package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.SearchFieldFilter;
import cn.ibizlab.odoo.util.enums.SearchFieldType;
import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * 数据集范围（VALUERANGE）
 *
 * @param <T>
 */

@Slf4j
@Data
@IBIZLog
public class VRValueRangeCondition<T> extends VRSingleCondition<T> {

    //当前值规则对应属性。
    private String fieldName;
    //数据查询名称
    private String dataQuery;
    //当前实体对象
    private Class<?> curEntity;
    //数据集实体对象
    private Class<?> majorEntity;
    //数据集实体属性
    private String majorEntityField;
    //附加属性
    private String exFieldName;

    public VRValueRangeCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);

    }

    public VRValueRangeCondition<T> init(String fieldName, Class<?> curEntity, String dataQuery, Class<?> majorEntity, String majorEntityField, String exFieldName) {
        this.fieldName = fieldName;
        this.curEntity = curEntity;
        this.dataQuery = dataQuery;
        this.majorEntity = majorEntity;
        this.majorEntityField = majorEntityField;
        this.exFieldName = exFieldName;
        return this;
    }

    @Override
    public boolean validate() {
        try {
            if (majorEntityField != null && majorEntityField.trim().length() != 0) {
                throw new RuntimeException("模板暂不支持的配置实体附加约束属性。");
            }

            //获取实体对应查询上下文
            String searchContextName = majorEntity.getName().replace("domain", "filter") + "SearchContext";
            Object searchContext = Class.forName(searchContextName).getConstructor().newInstance();

            //添加约束条件
            SearchFieldFilter fieldFilter = new SearchFieldFilter();
            fieldFilter.setParam(fieldName);
            fieldFilter.setCondition(SearchFieldType.EQ);
            fieldFilter.setValue(value);
            if (searchContext instanceof SearchContext) {
                ((SearchContext) searchContext).getCondition().add(fieldFilter);
            }

            //获取实体对应的Service及对应的数据查询
            Class<?> beanSearviceClass = Class.forName( majorEntity.getName().replace("domain","service.impl")+"ServiceImpl");
            Object service = SpringContextHolder.getBean(beanSearviceClass);
            Object dataset = service.getClass().getMethod("search" + dataQuery, searchContext.getClass()).invoke(service, searchContext);

            //如果查询结果不为空，返回true
            if (dataset instanceof Page) {
                Page datas = (Page) dataset;
                if (datas.getTotalElements() == 0) {
                    return false;
                } else {
                    return true;
                }
            }
            return false;

        } catch (Exception e) {
            e.printStackTrace();
            log.info("数据集【" + dataQuery + "】对应的值规则校验异常。");
            throw new RuntimeException("数据集【" + dataQuery + "】对应的值规则校验异常。");
        }
    }

}

package cn.ibizlab.odoo.util.serialize;

import java.io.IOException;
import java.lang.reflect.Method;

import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializer;
import com.fasterxml.jackson.databind.ser.std.BeanSerializerBase;

/**
 * Created by Felix on 2019/05/10.
 */
@Slf4j
public class DomainJsonSerializer extends BeanSerializer {

	BeanSerializer BeanSerializer = null ;

    protected DomainJsonSerializer(BeanSerializerBase src) {
		super(src);
	}


	protected void serializeFields(Object bean, JsonGenerator gen, SerializerProvider provider)
            throws IOException
        {
            final BeanPropertyWriter[] props;
            if (_filteredProps != null && provider.getActiveView() != null) {
                props = _filteredProps;
            } else {
                props = _props;
            }
            int i = 0;
            try {
                for (final int len = props.length; i < len; ++i) {
                    BeanPropertyWriter prop = props[i];
                    if(prop.getName().contains("DirtyFlag"))
                    	continue ;
                    String strMethod = prop.getMember().getName()+"DirtyFlag" ;
                    Method dirtyFlagMethod = null ;
                    try{
                    	dirtyFlagMethod = bean.getClass().getMethod(strMethod);
                    }catch (Exception e){

                    }
                    if(dirtyFlagMethod!=null){
                    	if(!(boolean) dirtyFlagMethod.invoke(bean))
                    		continue ;
                    }
                    if (prop != null) { // can have nulls in filtered list
                        prop.serializeAsField(bean, gen, provider);
                    }
                }
                if (_anyGetterWriter != null) {
                    _anyGetterWriter.getAndSerialize(bean, gen, provider);
                }
            } catch (Exception e) {
                String name = (i == props.length) ? "[anySetter]" : props[i].getName();
                wrapAndThrow(provider, e, bean, name);
            } catch (StackOverflowError e) {
                // 04-Sep-2009, tatu: Dealing with this is tricky, since we don't have many
                //   stack frames to spare... just one or two; can't make many calls.

                // 10-Dec-2015, tatu: and due to above, avoid "from" method, call ctor directly:
                //JsonMappingException mapE = JsonMappingException.from(gen, "Infinite recursion (StackOverflowError)", e);
                JsonMappingException mapE = new JsonMappingException(gen, "Infinite recursion (StackOverflowError)", e);

                 String name = (i == props.length) ? "[anySetter]" : props[i].getName();
                mapE.prependPath(new JsonMappingException.Reference(bean, name));
                throw mapE;
            }
        }

}

package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;
import lombok.extern.slf4j.Slf4j;

/**
 * 字符长度（STRINGLENGTH）条件
 *
 * @param <T> 当前成员变量类型
 */
@Slf4j
@Data
@IBIZLog
public class VRStringLengthCondition<T> extends VRSingleCondition<T> {
    //最小值
    private Integer minValue;
    //最大值
    private Integer maxValue;
    //是否包含最小值
    private Boolean isIncludeMinValue;
    //是否包含最大值
    private Boolean isIncludeMaxValue;

    public VRStringLengthCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }

    public VRStringLengthCondition<T> init(Integer minValue, Integer maxValue, Boolean isIncludeMinValue, Boolean isIncludeMaxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.isIncludeMinValue = isIncludeMinValue;
        this.isIncludeMaxValue = isIncludeMaxValue;
        return this;
    }

    @Override
    public boolean validate() {
        Integer valueLength = String.valueOf(value == null ? "" : value).length();

        boolean isInrange = true;
        //最小值比较
        if (!(minValue == null)) {
            boolean minRange = valueLength > minValue;
            if (isIncludeMinValue) {
                minRange = minRange || valueLength == minValue;
            }
            isInrange = isInrange && minRange;
        }

        //最大值比较。
        if (!(maxValue == null)) {
            boolean maxRange = valueLength < maxValue;
            if (isIncludeMaxValue) {
                maxRange = maxRange || valueLength == maxValue;
            }
            isInrange = isInrange && maxRange;
        }
        return isInrange;
    }
}

package cn.ibizlab.odoo.util.annotation;


import cn.ibizlab.odoo.util.enums.FillMode;
import cn.ibizlab.odoo.util.enums.PredefinedType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD})
public @interface PreField
{
	/**
	 * 填充模式
	 * @return
	 */
	FillMode fill() default FillMode.INSERT_UPDATE;

	/**
	 * 预置属性类型
	 * @return
	 */
	PredefinedType preType() default PredefinedType.DEFAULT;
}


package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.Data;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 * 值范围（值清单）条件
 *
 * @param <T>
 */
@Slf4j
@Data
@IBIZLog
public class VRValueRange3Condition<T> extends VRSingleCondition<T> {
    //值清单
    private String [] valuelist;

    public VRValueRange3Condition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);

    }

    public VRValueRange3Condition<T> init(String... valuelist) {
        this.valuelist = valuelist;
        return this;
    }

    @Override
    public boolean validate() {
        List<String> list = Arrays.asList(valuelist);

        return list.contains(String.valueOf(value));
    }

    public static void main(String[] args) {
        Integer a = null;
        String s = String.valueOf(a);
        System.out.println(s);
    }
}

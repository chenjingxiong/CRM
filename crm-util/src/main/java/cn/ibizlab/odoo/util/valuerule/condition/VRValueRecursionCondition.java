package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 递归查询条件（VALUERECURSION）
 * 暂未开放。
 *
 * @param <T>
 */
@Slf4j
@Data
@IBIZLog
public class VRValueRecursionCondition<T> extends VRSingleCondition<T> {
    public VRValueRecursionCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }

    @Override
    public boolean validate() {
        return true;
    }
}

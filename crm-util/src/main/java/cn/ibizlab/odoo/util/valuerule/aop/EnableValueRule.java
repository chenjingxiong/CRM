package cn.ibizlab.odoo.util.valuerule.aop;

import java.lang.annotation.*;

/**
 * 开启值规则校验注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EnableValueRule {
    /**
     * flag为值验证标记，true时验证，false时不验证
     * @return
     */
    boolean flag() default true;
}

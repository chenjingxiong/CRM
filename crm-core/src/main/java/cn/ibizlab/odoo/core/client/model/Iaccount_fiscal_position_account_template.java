package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_fiscal_position_account_template] 对象
 */
public interface Iaccount_fiscal_position_account_template {

    /**
     * 获取 [科目目标]
     */
    public void setAccount_dest_id(Integer account_dest_id);
    
    /**
     * 设置 [科目目标]
     */
    public Integer getAccount_dest_id();

    /**
     * 获取 [科目目标]脏标记
     */
    public boolean getAccount_dest_idDirtyFlag();
    /**
     * 获取 [科目目标]
     */
    public void setAccount_dest_id_text(String account_dest_id_text);
    
    /**
     * 设置 [科目目标]
     */
    public String getAccount_dest_id_text();

    /**
     * 获取 [科目目标]脏标记
     */
    public boolean getAccount_dest_id_textDirtyFlag();
    /**
     * 获取 [科目来源]
     */
    public void setAccount_src_id(Integer account_src_id);
    
    /**
     * 设置 [科目来源]
     */
    public Integer getAccount_src_id();

    /**
     * 获取 [科目来源]脏标记
     */
    public boolean getAccount_src_idDirtyFlag();
    /**
     * 获取 [科目来源]
     */
    public void setAccount_src_id_text(String account_src_id_text);
    
    /**
     * 设置 [科目来源]
     */
    public String getAccount_src_id_text();

    /**
     * 获取 [科目来源]脏标记
     */
    public boolean getAccount_src_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [财务映射]
     */
    public void setPosition_id(Integer position_id);
    
    /**
     * 设置 [财务映射]
     */
    public Integer getPosition_id();

    /**
     * 获取 [财务映射]脏标记
     */
    public boolean getPosition_idDirtyFlag();
    /**
     * 获取 [财务映射]
     */
    public void setPosition_id_text(String position_id_text);
    
    /**
     * 设置 [财务映射]
     */
    public String getPosition_id_text();

    /**
     * 获取 [财务映射]脏标记
     */
    public boolean getPosition_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

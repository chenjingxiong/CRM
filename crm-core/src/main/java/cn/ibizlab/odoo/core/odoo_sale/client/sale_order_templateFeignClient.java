package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_order_template] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-order-template", fallback = sale_order_templateFallback.class)
public interface sale_order_templateFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates")
    Sale_order_template create(@RequestBody Sale_order_template sale_order_template);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/batch")
    Boolean createBatch(@RequestBody List<Sale_order_template> sale_order_templates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_templates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_templates/{id}")
    Sale_order_template update(@PathVariable("id") Integer id,@RequestBody Sale_order_template sale_order_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_templates/batch")
    Boolean updateBatch(@RequestBody List<Sale_order_template> sale_order_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_templates/{id}")
    Sale_order_template get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/searchdefault")
    Page<Sale_order_template> searchDefault(@RequestBody Sale_order_templateSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_templates/select")
    Page<Sale_order_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_templates/getdraft")
    Sale_order_template getDraft();


}

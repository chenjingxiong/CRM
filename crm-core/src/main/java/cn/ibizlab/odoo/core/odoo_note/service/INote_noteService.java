package cn.ibizlab.odoo.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;


/**
 * 实体[Note_note] 服务对象接口
 */
public interface INote_noteService{

    Note_note get(Integer key) ;
    Note_note getDraft(Note_note et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Note_note et) ;
    void createBatch(List<Note_note> list) ;
    boolean update(Note_note et) ;
    void updateBatch(List<Note_note> list) ;
    Page<Note_note> searchDefault(Note_noteSearchContext context) ;

}




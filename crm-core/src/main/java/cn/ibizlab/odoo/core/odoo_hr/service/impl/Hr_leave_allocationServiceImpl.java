package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_allocationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_allocationFeignClient;

/**
 * 实体[休假分配] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_allocationServiceImpl implements IHr_leave_allocationService {

    @Autowired
    hr_leave_allocationFeignClient hr_leave_allocationFeignClient;


    @Override
    public Hr_leave_allocation getDraft(Hr_leave_allocation et) {
        et=hr_leave_allocationFeignClient.getDraft();
        return et;
    }

    @Override
    public Hr_leave_allocation get(Integer id) {
		Hr_leave_allocation et=hr_leave_allocationFeignClient.get(id);
        if(et==null){
            et=new Hr_leave_allocation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_leave_allocationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_leave_allocationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_leave_allocation et) {
        Hr_leave_allocation rt = hr_leave_allocationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_leave_allocation> list){
        hr_leave_allocationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Hr_leave_allocation et) {
        Hr_leave_allocation rt = hr_leave_allocationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_allocation> list){
        hr_leave_allocationFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_allocation> searchDefault(Hr_leave_allocationSearchContext context) {
        Page<Hr_leave_allocation> hr_leave_allocations=hr_leave_allocationFeignClient.searchDefault(context);
        return hr_leave_allocations;
    }


}



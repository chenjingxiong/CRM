package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;


/**
 * 实体[Account_cash_rounding] 服务对象接口
 */
public interface IAccount_cash_roundingService{

    Account_cash_rounding getDraft(Account_cash_rounding et) ;
    boolean create(Account_cash_rounding et) ;
    void createBatch(List<Account_cash_rounding> list) ;
    Account_cash_rounding get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_cash_rounding et) ;
    void updateBatch(List<Account_cash_rounding> list) ;
    Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_chart_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_chart_templateFeignClient;

/**
 * 实体[科目表模版] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_chart_templateServiceImpl implements IAccount_chart_templateService {

    @Autowired
    account_chart_templateFeignClient account_chart_templateFeignClient;


    @Override
    public Account_chart_template getDraft(Account_chart_template et) {
        et=account_chart_templateFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_chart_template get(Integer id) {
		Account_chart_template et=account_chart_templateFeignClient.get(id);
        if(et==null){
            et=new Account_chart_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_chart_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_chart_templateFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_chart_template et) {
        Account_chart_template rt = account_chart_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_chart_template> list){
        account_chart_templateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_chart_template et) {
        Account_chart_template rt = account_chart_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_chart_template> list){
        account_chart_templateFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context) {
        Page<Account_chart_template> account_chart_templates=account_chart_templateFeignClient.searchDefault(context);
        return account_chart_templates;
    }


}



package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_activity_report] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-activity-report", fallback = crm_activity_reportFallback.class)
public interface crm_activity_reportFeignClient {





    @RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports/searchdefault")
    Page<Crm_activity_report> searchDefault(@RequestBody Crm_activity_reportSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports")
    Crm_activity_report create(@RequestBody Crm_activity_report crm_activity_report);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports/batch")
    Boolean createBatch(@RequestBody List<Crm_activity_report> crm_activity_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/{id}")
    Crm_activity_report get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/{id}")
    Crm_activity_report update(@PathVariable("id") Integer id,@RequestBody Crm_activity_report crm_activity_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/batch")
    Boolean updateBatch(@RequestBody List<Crm_activity_report> crm_activity_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/select")
    Page<Crm_activity_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/getdraft")
    Crm_activity_report getDraft();


}

package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;


/**
 * 实体[Fleet_vehicle_log_contract] 服务对象接口
 */
public interface IFleet_vehicle_log_contractService{

    Fleet_vehicle_log_contract getDraft(Fleet_vehicle_log_contract et) ;
    Fleet_vehicle_log_contract get(Integer key) ;
    boolean update(Fleet_vehicle_log_contract et) ;
    void updateBatch(List<Fleet_vehicle_log_contract> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Fleet_vehicle_log_contract et) ;
    void createBatch(List<Fleet_vehicle_log_contract> list) ;
    Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_department] 服务对象接口
 */
@Component
public class hr_departmentFallback implements hr_departmentFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Hr_department get(Integer id){
            return null;
     }


    public Hr_department update(Integer id, Hr_department hr_department){
            return null;
     }
    public Boolean updateBatch(List<Hr_department> hr_departments){
            return false;
     }




    public Hr_department create(Hr_department hr_department){
            return null;
     }
    public Boolean createBatch(List<Hr_department> hr_departments){
            return false;
     }

    public Page<Hr_department> searchDefault(Hr_departmentSearchContext context){
            return null;
     }



    public Page<Hr_department> select(){
            return null;
     }

    public Hr_department getDraft(){
            return null;
    }



}

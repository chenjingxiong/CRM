package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_price_list] 对象
 */
public interface Iproduct_price_list {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPrice_list(Integer price_list);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPrice_list();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPrice_listDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPrice_list_text(String price_list_text);
    
    /**
     * 设置 [价格表]
     */
    public String getPrice_list_text();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPrice_list_textDirtyFlag();
    /**
     * 获取 [数量-1]
     */
    public void setQty1(Integer qty1);
    
    /**
     * 设置 [数量-1]
     */
    public Integer getQty1();

    /**
     * 获取 [数量-1]脏标记
     */
    public boolean getQty1DirtyFlag();
    /**
     * 获取 [数量-2]
     */
    public void setQty2(Integer qty2);
    
    /**
     * 设置 [数量-2]
     */
    public Integer getQty2();

    /**
     * 获取 [数量-2]脏标记
     */
    public boolean getQty2DirtyFlag();
    /**
     * 获取 [数量-3]
     */
    public void setQty3(Integer qty3);
    
    /**
     * 设置 [数量-3]
     */
    public Integer getQty3();

    /**
     * 获取 [数量-3]脏标记
     */
    public boolean getQty3DirtyFlag();
    /**
     * 获取 [数量-4]
     */
    public void setQty4(Integer qty4);
    
    /**
     * 设置 [数量-4]
     */
    public Integer getQty4();

    /**
     * 获取 [数量-4]脏标记
     */
    public boolean getQty4DirtyFlag();
    /**
     * 获取 [数量-5]
     */
    public void setQty5(Integer qty5);
    
    /**
     * 设置 [数量-5]
     */
    public Integer getQty5();

    /**
     * 获取 [数量-5]脏标记
     */
    public boolean getQty5DirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

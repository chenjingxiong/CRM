package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;


/**
 * 实体[Stock_move] 服务对象接口
 */
public interface IStock_moveService{

    Stock_move get(Integer key) ;
    Stock_move getDraft(Stock_move et) ;
    boolean update(Stock_move et) ;
    void updateBatch(List<Stock_move> list) ;
    boolean create(Stock_move et) ;
    void createBatch(List<Stock_move> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_move> searchDefault(Stock_moveSearchContext context) ;

}




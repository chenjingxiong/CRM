package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_account_template] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-account-template", fallback = account_account_templateFallback.class)
public interface account_account_templateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_templates/{id}")
    Account_account_template update(@PathVariable("id") Integer id,@RequestBody Account_account_template account_account_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_templates/batch")
    Boolean updateBatch(@RequestBody List<Account_account_template> account_account_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_templates/{id}")
    Account_account_template get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/searchdefault")
    Page<Account_account_template> searchDefault(@RequestBody Account_account_templateSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_templates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_account_templates")
    Account_account_template create(@RequestBody Account_account_template account_account_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/batch")
    Boolean createBatch(@RequestBody List<Account_account_template> account_account_templates);



    @RequestMapping(method = RequestMethod.GET, value = "/account_account_templates/select")
    Page<Account_account_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_templates/getdraft")
    Account_account_template getDraft();


}

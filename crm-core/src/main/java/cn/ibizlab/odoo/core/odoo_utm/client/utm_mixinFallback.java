package cn.ibizlab.odoo.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[utm_mixin] 服务对象接口
 */
@Component
public class utm_mixinFallback implements utm_mixinFeignClient{



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Utm_mixin create(Utm_mixin utm_mixin){
            return null;
     }
    public Boolean createBatch(List<Utm_mixin> utm_mixins){
            return false;
     }

    public Utm_mixin get(Integer id){
            return null;
     }


    public Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context){
            return null;
     }


    public Utm_mixin update(Integer id, Utm_mixin utm_mixin){
            return null;
     }
    public Boolean updateBatch(List<Utm_mixin> utm_mixins){
            return false;
     }


    public Page<Utm_mixin> select(){
            return null;
     }

    public Utm_mixin getDraft(){
            return null;
    }



}

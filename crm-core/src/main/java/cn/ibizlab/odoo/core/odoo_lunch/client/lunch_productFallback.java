package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_product] 服务对象接口
 */
@Component
public class lunch_productFallback implements lunch_productFeignClient{

    public Lunch_product create(Lunch_product lunch_product){
            return null;
     }
    public Boolean createBatch(List<Lunch_product> lunch_products){
            return false;
     }

    public Lunch_product get(Integer id){
            return null;
     }


    public Page<Lunch_product> searchDefault(Lunch_productSearchContext context){
            return null;
     }





    public Lunch_product update(Integer id, Lunch_product lunch_product){
            return null;
     }
    public Boolean updateBatch(List<Lunch_product> lunch_products){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Lunch_product> select(){
            return null;
     }

    public Lunch_product getDraft(){
            return null;
    }



}

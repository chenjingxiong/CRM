package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_event] 服务对象接口
 */
public interface Ievent_eventClientService{

    public Ievent_event createModel() ;

    public void create(Ievent_event event_event);

    public void get(Ievent_event event_event);

    public Page<Ievent_event> fetchDefault(SearchContext context);

    public void createBatch(List<Ievent_event> event_events);

    public void updateBatch(List<Ievent_event> event_events);

    public void remove(Ievent_event event_event);

    public void update(Ievent_event event_event);

    public void removeBatch(List<Ievent_event> event_events);

    public Page<Ievent_event> select(SearchContext context);

    public void getDraft(Ievent_event event_event);

}

package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
@Component
public class maintenance_stageFallback implements maintenance_stageFeignClient{

    public Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context){
            return null;
     }


    public Maintenance_stage get(Integer id){
            return null;
     }


    public Maintenance_stage create(Maintenance_stage maintenance_stage){
            return null;
     }
    public Boolean createBatch(List<Maintenance_stage> maintenance_stages){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Maintenance_stage update(Integer id, Maintenance_stage maintenance_stage){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_stage> maintenance_stages){
            return false;
     }





    public Page<Maintenance_stage> select(){
            return null;
     }

    public Maintenance_stage getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_production;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_production] 服务对象接口
 */
public interface Imrp_productionClientService{

    public Imrp_production createModel() ;

    public void removeBatch(List<Imrp_production> mrp_productions);

    public void createBatch(List<Imrp_production> mrp_productions);

    public void create(Imrp_production mrp_production);

    public Page<Imrp_production> fetchDefault(SearchContext context);

    public void updateBatch(List<Imrp_production> mrp_productions);

    public void get(Imrp_production mrp_production);

    public void remove(Imrp_production mrp_production);

    public void update(Imrp_production mrp_production);

    public Page<Imrp_production> select(SearchContext context);

    public void getDraft(Imrp_production mrp_production);

}

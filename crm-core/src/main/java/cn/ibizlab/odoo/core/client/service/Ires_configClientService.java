package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_config;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config] 服务对象接口
 */
public interface Ires_configClientService{

    public Ires_config createModel() ;

    public void remove(Ires_config res_config);

    public void create(Ires_config res_config);

    public void updateBatch(List<Ires_config> res_configs);

    public void get(Ires_config res_config);

    public void update(Ires_config res_config);

    public Page<Ires_config> fetchDefault(SearchContext context);

    public void createBatch(List<Ires_config> res_configs);

    public void removeBatch(List<Ires_config> res_configs);

    public Page<Ires_config> select(SearchContext context);

    public void getDraft(Ires_config res_config);

}

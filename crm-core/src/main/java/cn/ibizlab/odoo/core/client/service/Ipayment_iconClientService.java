package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipayment_icon;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_icon] 服务对象接口
 */
public interface Ipayment_iconClientService{

    public Ipayment_icon createModel() ;

    public void removeBatch(List<Ipayment_icon> payment_icons);

    public Page<Ipayment_icon> fetchDefault(SearchContext context);

    public void create(Ipayment_icon payment_icon);

    public void updateBatch(List<Ipayment_icon> payment_icons);

    public void remove(Ipayment_icon payment_icon);

    public void createBatch(List<Ipayment_icon> payment_icons);

    public void get(Ipayment_icon payment_icon);

    public void update(Ipayment_icon payment_icon);

    public Page<Ipayment_icon> select(SearchContext context);

    public void getDraft(Ipayment_icon payment_icon);

}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_department] 对象
 */
public interface Ihr_department {

    /**
     * 获取 [今日缺勤]
     */
    public void setAbsence_of_today(Integer absence_of_today);
    
    /**
     * 设置 [今日缺勤]
     */
    public Integer getAbsence_of_today();

    /**
     * 获取 [今日缺勤]脏标记
     */
    public boolean getAbsence_of_todayDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [待批准的分配]
     */
    public void setAllocation_to_approve_count(Integer allocation_to_approve_count);
    
    /**
     * 设置 [待批准的分配]
     */
    public Integer getAllocation_to_approve_count();

    /**
     * 获取 [待批准的分配]脏标记
     */
    public boolean getAllocation_to_approve_countDirtyFlag();
    /**
     * 获取 [子部门]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [子部门]
     */
    public String getChild_ids();

    /**
     * 获取 [子部门]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [完整名称]
     */
    public void setComplete_name(String complete_name);
    
    /**
     * 设置 [完整名称]
     */
    public String getComplete_name();

    /**
     * 获取 [完整名称]脏标记
     */
    public boolean getComplete_nameDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [预期的员工]
     */
    public void setExpected_employee(Integer expected_employee);
    
    /**
     * 设置 [预期的员工]
     */
    public Integer getExpected_employee();

    /**
     * 获取 [预期的员工]脏标记
     */
    public boolean getExpected_employeeDirtyFlag();
    /**
     * 获取 [待批准的费用报告]
     */
    public void setExpense_sheets_to_approve_count(Integer expense_sheets_to_approve_count);
    
    /**
     * 设置 [待批准的费用报告]
     */
    public Integer getExpense_sheets_to_approve_count();

    /**
     * 获取 [待批准的费用报告]脏标记
     */
    public boolean getExpense_sheets_to_approve_countDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [工作]
     */
    public void setJobs_ids(String jobs_ids);
    
    /**
     * 设置 [工作]
     */
    public String getJobs_ids();

    /**
     * 获取 [工作]脏标记
     */
    public boolean getJobs_idsDirtyFlag();
    /**
     * 获取 [待批准休假]
     */
    public void setLeave_to_approve_count(Integer leave_to_approve_count);
    
    /**
     * 设置 [待批准休假]
     */
    public Integer getLeave_to_approve_count();

    /**
     * 获取 [待批准休假]脏标记
     */
    public boolean getLeave_to_approve_countDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setManager_id(Integer manager_id);
    
    /**
     * 设置 [经理]
     */
    public Integer getManager_id();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getManager_idDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setManager_id_text(String manager_id_text);
    
    /**
     * 设置 [经理]
     */
    public String getManager_id_text();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getManager_id_textDirtyFlag();
    /**
     * 获取 [会员]
     */
    public void setMember_ids(String member_ids);
    
    /**
     * 设置 [会员]
     */
    public String getMember_ids();

    /**
     * 获取 [会员]脏标记
     */
    public boolean getMember_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [部门名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [部门名称]
     */
    public String getName();

    /**
     * 获取 [部门名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [新申请]
     */
    public void setNew_applicant_count(Integer new_applicant_count);
    
    /**
     * 设置 [新申请]
     */
    public Integer getNew_applicant_count();

    /**
     * 获取 [新申请]脏标记
     */
    public boolean getNew_applicant_countDirtyFlag();
    /**
     * 获取 [新雇用的员工]
     */
    public void setNew_hired_employee(Integer new_hired_employee);
    
    /**
     * 设置 [新雇用的员工]
     */
    public Integer getNew_hired_employee();

    /**
     * 获取 [新雇用的员工]脏标记
     */
    public boolean getNew_hired_employeeDirtyFlag();
    /**
     * 获取 [笔记]
     */
    public void setNote(String note);
    
    /**
     * 设置 [笔记]
     */
    public String getNote();

    /**
     * 获取 [笔记]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [上级部门]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级部门]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级部门]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级部门]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级部门]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级部门]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [员工总数]
     */
    public void setTotal_employee(Integer total_employee);
    
    /**
     * 设置 [员工总数]
     */
    public Integer getTotal_employee();

    /**
     * 获取 [员工总数]脏标记
     */
    public boolean getTotal_employeeDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

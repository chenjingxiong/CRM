package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_m2o_relatedService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_m2o_relatedFeignClient;

/**
 * 实体[测试:基本导入模型，多对一关系] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_m2o_relatedServiceImpl implements IBase_import_tests_models_m2o_relatedService {

    @Autowired
    base_import_tests_models_m2o_relatedFeignClient base_import_tests_models_m2o_relatedFeignClient;


    @Override
    public boolean update(Base_import_tests_models_m2o_related et) {
        Base_import_tests_models_m2o_related rt = base_import_tests_models_m2o_relatedFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_m2o_related> list){
        base_import_tests_models_m2o_relatedFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_import_tests_models_m2o_related get(Integer id) {
		Base_import_tests_models_m2o_related et=base_import_tests_models_m2o_relatedFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_m2o_related();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Base_import_tests_models_m2o_related getDraft(Base_import_tests_models_m2o_related et) {
        et=base_import_tests_models_m2o_relatedFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Base_import_tests_models_m2o_related et) {
        Base_import_tests_models_m2o_related rt = base_import_tests_models_m2o_relatedFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_m2o_related> list){
        base_import_tests_models_m2o_relatedFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_m2o_relatedFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_m2o_relatedFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_m2o_related> searchDefault(Base_import_tests_models_m2o_relatedSearchContext context) {
        Page<Base_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds=base_import_tests_models_m2o_relatedFeignClient.searchDefault(context);
        return base_import_tests_models_m2o_relateds;
    }


}



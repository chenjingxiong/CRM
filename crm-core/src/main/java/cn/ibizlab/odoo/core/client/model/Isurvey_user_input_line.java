package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [survey_user_input_line] 对象
 */
public interface Isurvey_user_input_line {

    /**
     * 获取 [回复类型]
     */
    public void setAnswer_type(String answer_type);
    
    /**
     * 设置 [回复类型]
     */
    public String getAnswer_type();

    /**
     * 获取 [回复类型]脏标记
     */
    public boolean getAnswer_typeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setDate_create(Timestamp date_create);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getDate_create();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getDate_createDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [页]
     */
    public void setPage_id(Integer page_id);
    
    /**
     * 设置 [页]
     */
    public Integer getPage_id();

    /**
     * 获取 [页]脏标记
     */
    public boolean getPage_idDirtyFlag();
    /**
     * 获取 [疑问]
     */
    public void setQuestion_id(Integer question_id);
    
    /**
     * 设置 [疑问]
     */
    public Integer getQuestion_id();

    /**
     * 获取 [疑问]脏标记
     */
    public boolean getQuestion_idDirtyFlag();
    /**
     * 获取 [这个选项分配的分数]
     */
    public void setQuizz_mark(Double quizz_mark);
    
    /**
     * 设置 [这个选项分配的分数]
     */
    public Double getQuizz_mark();

    /**
     * 获取 [这个选项分配的分数]脏标记
     */
    public boolean getQuizz_markDirtyFlag();
    /**
     * 获取 [忽略]
     */
    public void setSkipped(String skipped);
    
    /**
     * 设置 [忽略]
     */
    public String getSkipped();

    /**
     * 获取 [忽略]脏标记
     */
    public boolean getSkippedDirtyFlag();
    /**
     * 获取 [问卷]
     */
    public void setSurvey_id(Integer survey_id);
    
    /**
     * 设置 [问卷]
     */
    public Integer getSurvey_id();

    /**
     * 获取 [问卷]脏标记
     */
    public boolean getSurvey_idDirtyFlag();
    /**
     * 获取 [用户输入]
     */
    public void setUser_input_id(Integer user_input_id);
    
    /**
     * 设置 [用户输入]
     */
    public Integer getUser_input_id();

    /**
     * 获取 [用户输入]脏标记
     */
    public boolean getUser_input_idDirtyFlag();
    /**
     * 获取 [回复日期]
     */
    public void setValue_date(Timestamp value_date);
    
    /**
     * 设置 [回复日期]
     */
    public Timestamp getValue_date();

    /**
     * 获取 [回复日期]脏标记
     */
    public boolean getValue_dateDirtyFlag();
    /**
     * 获取 [自由文本答案]
     */
    public void setValue_free_text(String value_free_text);
    
    /**
     * 设置 [自由文本答案]
     */
    public String getValue_free_text();

    /**
     * 获取 [自由文本答案]脏标记
     */
    public boolean getValue_free_textDirtyFlag();
    /**
     * 获取 [数字答案]
     */
    public void setValue_number(Double value_number);
    
    /**
     * 设置 [数字答案]
     */
    public Double getValue_number();

    /**
     * 获取 [数字答案]脏标记
     */
    public boolean getValue_numberDirtyFlag();
    /**
     * 获取 [建议答案]
     */
    public void setValue_suggested(Integer value_suggested);
    
    /**
     * 设置 [建议答案]
     */
    public Integer getValue_suggested();

    /**
     * 获取 [建议答案]脏标记
     */
    public boolean getValue_suggestedDirtyFlag();
    /**
     * 获取 [答案行]
     */
    public void setValue_suggested_row(Integer value_suggested_row);
    
    /**
     * 设置 [答案行]
     */
    public Integer getValue_suggested_row();

    /**
     * 获取 [答案行]脏标记
     */
    public boolean getValue_suggested_rowDirtyFlag();
    /**
     * 获取 [文本答案]
     */
    public void setValue_text(String value_text);
    
    /**
     * 设置 [文本答案]
     */
    public String getValue_text();

    /**
     * 获取 [文本答案]脏标记
     */
    public boolean getValue_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_tax_group;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_tax_group] 服务对象接口
 */
public interface Iaccount_tax_groupClientService{

    public Iaccount_tax_group createModel() ;

    public void createBatch(List<Iaccount_tax_group> account_tax_groups);

    public void create(Iaccount_tax_group account_tax_group);

    public Page<Iaccount_tax_group> fetchDefault(SearchContext context);

    public void get(Iaccount_tax_group account_tax_group);

    public void updateBatch(List<Iaccount_tax_group> account_tax_groups);

    public void remove(Iaccount_tax_group account_tax_group);

    public void update(Iaccount_tax_group account_tax_group);

    public void removeBatch(List<Iaccount_tax_group> account_tax_groups);

    public Page<Iaccount_tax_group> select(SearchContext context);

    public void getDraft(Iaccount_tax_group account_tax_group);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproject_project;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_project] 服务对象接口
 */
public interface Iproject_projectClientService{

    public Iproject_project createModel() ;

    public Page<Iproject_project> fetchDefault(SearchContext context);

    public void updateBatch(List<Iproject_project> project_projects);

    public void createBatch(List<Iproject_project> project_projects);

    public void removeBatch(List<Iproject_project> project_projects);

    public void remove(Iproject_project project_project);

    public void create(Iproject_project project_project);

    public void get(Iproject_project project_project);

    public void update(Iproject_project project_project);

    public Page<Iproject_project> select(SearchContext context);

    public void getDraft(Iproject_project project_project);

}

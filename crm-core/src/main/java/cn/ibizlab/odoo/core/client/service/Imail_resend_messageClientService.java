package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_resend_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_resend_message] 服务对象接口
 */
public interface Imail_resend_messageClientService{

    public Imail_resend_message createModel() ;

    public void updateBatch(List<Imail_resend_message> mail_resend_messages);

    public void update(Imail_resend_message mail_resend_message);

    public void remove(Imail_resend_message mail_resend_message);

    public void createBatch(List<Imail_resend_message> mail_resend_messages);

    public void get(Imail_resend_message mail_resend_message);

    public Page<Imail_resend_message> fetchDefault(SearchContext context);

    public void removeBatch(List<Imail_resend_message> mail_resend_messages);

    public void create(Imail_resend_message mail_resend_message);

    public Page<Imail_resend_message> select(SearchContext context);

    public void getDraft(Imail_resend_message mail_resend_message);

}

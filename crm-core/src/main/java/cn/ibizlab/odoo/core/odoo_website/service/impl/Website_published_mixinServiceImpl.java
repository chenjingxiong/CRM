package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_published_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_published_mixinFeignClient;

/**
 * 实体[网站发布Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_published_mixinServiceImpl implements IWebsite_published_mixinService {

    @Autowired
    website_published_mixinFeignClient website_published_mixinFeignClient;


    @Override
    public Website_published_mixin get(Integer id) {
		Website_published_mixin et=website_published_mixinFeignClient.get(id);
        if(et==null){
            et=new Website_published_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_published_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_published_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Website_published_mixin getDraft(Website_published_mixin et) {
        et=website_published_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Website_published_mixin et) {
        Website_published_mixin rt = website_published_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_published_mixin> list){
        website_published_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Website_published_mixin et) {
        Website_published_mixin rt = website_published_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_published_mixin> list){
        website_published_mixinFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_published_mixin> searchDefault(Website_published_mixinSearchContext context) {
        Page<Website_published_mixin> website_published_mixins=website_published_mixinFeignClient.searchDefault(context);
        return website_published_mixins;
    }


}



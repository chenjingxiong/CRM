package cn.ibizlab.odoo.core.odoo_bus.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[bus_bus] 服务对象接口
 */
@Component
public class bus_busFallback implements bus_busFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Bus_bus update(Integer id, Bus_bus bus_bus){
            return null;
     }
    public Boolean updateBatch(List<Bus_bus> bus_buses){
            return false;
     }


    public Bus_bus create(Bus_bus bus_bus){
            return null;
     }
    public Boolean createBatch(List<Bus_bus> bus_buses){
            return false;
     }


    public Bus_bus get(Integer id){
            return null;
     }


    public Page<Bus_bus> searchDefault(Bus_busSearchContext context){
            return null;
     }




    public Page<Bus_bus> select(){
            return null;
     }

    public Bus_bus getDraft(){
            return null;
    }



}

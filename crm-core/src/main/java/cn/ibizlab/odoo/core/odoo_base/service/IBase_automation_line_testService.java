package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_line_testSearchContext;


/**
 * 实体[Base_automation_line_test] 服务对象接口
 */
public interface IBase_automation_line_testService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Base_automation_line_test et) ;
    void updateBatch(List<Base_automation_line_test> list) ;
    Base_automation_line_test get(Integer key) ;
    Base_automation_line_test getDraft(Base_automation_line_test et) ;
    boolean create(Base_automation_line_test et) ;
    void createBatch(List<Base_automation_line_test> list) ;
    Page<Base_automation_line_test> searchDefault(Base_automation_line_testSearchContext context) ;

}




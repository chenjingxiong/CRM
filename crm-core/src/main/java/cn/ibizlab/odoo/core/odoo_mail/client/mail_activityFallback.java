package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_activity] 服务对象接口
 */
@Component
public class mail_activityFallback implements mail_activityFeignClient{


    public Mail_activity get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Mail_activity update(Integer id, Mail_activity mail_activity){
            return null;
     }
    public Boolean updateBatch(List<Mail_activity> mail_activities){
            return false;
     }


    public Mail_activity create(Mail_activity mail_activity){
            return null;
     }
    public Boolean createBatch(List<Mail_activity> mail_activities){
            return false;
     }


    public Page<Mail_activity> searchDefault(Mail_activitySearchContext context){
            return null;
     }


    public Page<Mail_activity> select(){
            return null;
     }

    public Mail_activity getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_activity mail_activity){
            return false;
     }


    public Boolean save(Mail_activity mail_activity){
            return false;
     }
    public Boolean saveBatch(List<Mail_activity> mail_activities){
            return false;
     }

}

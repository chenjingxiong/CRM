package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_exclusion;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
public interface Iproduct_template_attribute_exclusionClientService{

    public Iproduct_template_attribute_exclusion createModel() ;

    public void updateBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions);

    public Page<Iproduct_template_attribute_exclusion> fetchDefault(SearchContext context);

    public void get(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

    public void create(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

    public void update(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

    public void remove(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

    public void removeBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions);

    public void createBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions);

    public Page<Iproduct_template_attribute_exclusion> select(SearchContext context);

    public void getDraft(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

}

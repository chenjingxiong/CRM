package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_bank] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-bank", fallback = res_bankFallback.class)
public interface res_bankFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/res_banks/searchdefault")
    Page<Res_bank> searchDefault(@RequestBody Res_bankSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/res_banks/{id}")
    Res_bank update(@PathVariable("id") Integer id,@RequestBody Res_bank res_bank);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_banks/batch")
    Boolean updateBatch(@RequestBody List<Res_bank> res_banks);



    @RequestMapping(method = RequestMethod.GET, value = "/res_banks/{id}")
    Res_bank get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/res_banks")
    Res_bank create(@RequestBody Res_bank res_bank);

    @RequestMapping(method = RequestMethod.POST, value = "/res_banks/batch")
    Boolean createBatch(@RequestBody List<Res_bank> res_banks);


    @RequestMapping(method = RequestMethod.GET, value = "/res_banks/select")
    Page<Res_bank> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_banks/getdraft")
    Res_bank getDraft();


}

package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_bom_line;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_bom_line.Mrp_bom_lineWrite_dateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_bom_line
 * 属性：Write_date
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_bom_lineWrite_dateDefaultValidator.class})
public @interface Mrp_bom_lineWrite_dateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_style] 服务对象接口
 */
@Component
public class product_styleFallback implements product_styleFeignClient{

    public Product_style update(Integer id, Product_style product_style){
            return null;
     }
    public Boolean updateBatch(List<Product_style> product_styles){
            return false;
     }



    public Page<Product_style> searchDefault(Product_styleSearchContext context){
            return null;
     }


    public Product_style create(Product_style product_style){
            return null;
     }
    public Boolean createBatch(List<Product_style> product_styles){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Product_style get(Integer id){
            return null;
     }



    public Page<Product_style> select(){
            return null;
     }

    public Product_style getDraft(){
            return null;
    }



}

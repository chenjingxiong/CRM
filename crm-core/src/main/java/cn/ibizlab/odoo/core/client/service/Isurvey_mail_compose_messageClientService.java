package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_mail_compose_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
public interface Isurvey_mail_compose_messageClientService{

    public Isurvey_mail_compose_message createModel() ;

    public void remove(Isurvey_mail_compose_message survey_mail_compose_message);

    public void createBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages);

    public void update(Isurvey_mail_compose_message survey_mail_compose_message);

    public void get(Isurvey_mail_compose_message survey_mail_compose_message);

    public void removeBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages);

    public void updateBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages);

    public void create(Isurvey_mail_compose_message survey_mail_compose_message);

    public Page<Isurvey_mail_compose_message> fetchDefault(SearchContext context);

    public Page<Isurvey_mail_compose_message> select(SearchContext context);

    public void getDraft(Isurvey_mail_compose_message survey_mail_compose_message);

}

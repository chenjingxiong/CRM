package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_track_line] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-track-line", fallback = stock_track_lineFallback.class)
public interface stock_track_lineFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/{id}")
    Stock_track_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines")
    Stock_track_line create(@RequestBody Stock_track_line stock_track_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/batch")
    Boolean createBatch(@RequestBody List<Stock_track_line> stock_track_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/searchdefault")
    Page<Stock_track_line> searchDefault(@RequestBody Stock_track_lineSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/{id}")
    Stock_track_line update(@PathVariable("id") Integer id,@RequestBody Stock_track_line stock_track_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/batch")
    Boolean updateBatch(@RequestBody List<Stock_track_line> stock_track_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/select")
    Page<Stock_track_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/getdraft")
    Stock_track_line getDraft();


}

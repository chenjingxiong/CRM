package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_full_reconcile] 服务对象接口
 */
@Component
public class account_full_reconcileFallback implements account_full_reconcileFeignClient{

    public Account_full_reconcile create(Account_full_reconcile account_full_reconcile){
            return null;
     }
    public Boolean createBatch(List<Account_full_reconcile> account_full_reconciles){
            return false;
     }

    public Account_full_reconcile get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_full_reconcile> searchDefault(Account_full_reconcileSearchContext context){
            return null;
     }



    public Account_full_reconcile update(Integer id, Account_full_reconcile account_full_reconcile){
            return null;
     }
    public Boolean updateBatch(List<Account_full_reconcile> account_full_reconciles){
            return false;
     }



    public Page<Account_full_reconcile> select(){
            return null;
     }

    public Account_full_reconcile getDraft(){
            return null;
    }



}

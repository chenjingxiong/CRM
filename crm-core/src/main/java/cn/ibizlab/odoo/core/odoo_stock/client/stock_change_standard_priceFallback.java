package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
@Component
public class stock_change_standard_priceFallback implements stock_change_standard_priceFeignClient{

    public Stock_change_standard_price get(Integer id){
            return null;
     }


    public Stock_change_standard_price create(Stock_change_standard_price stock_change_standard_price){
            return null;
     }
    public Boolean createBatch(List<Stock_change_standard_price> stock_change_standard_prices){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context){
            return null;
     }


    public Stock_change_standard_price update(Integer id, Stock_change_standard_price stock_change_standard_price){
            return null;
     }
    public Boolean updateBatch(List<Stock_change_standard_price> stock_change_standard_prices){
            return false;
     }




    public Page<Stock_change_standard_price> select(){
            return null;
     }

    public Stock_change_standard_price getDraft(){
            return null;
    }



}

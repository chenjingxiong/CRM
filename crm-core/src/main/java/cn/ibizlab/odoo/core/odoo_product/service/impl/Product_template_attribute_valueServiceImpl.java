package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_template_attribute_valueFeignClient;

/**
 * 实体[产品属性值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_template_attribute_valueServiceImpl implements IProduct_template_attribute_valueService {

    @Autowired
    product_template_attribute_valueFeignClient product_template_attribute_valueFeignClient;


    @Override
    public boolean update(Product_template_attribute_value et) {
        Product_template_attribute_value rt = product_template_attribute_valueFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_template_attribute_value> list){
        product_template_attribute_valueFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Product_template_attribute_value et) {
        Product_template_attribute_value rt = product_template_attribute_valueFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template_attribute_value> list){
        product_template_attribute_valueFeignClient.createBatch(list) ;
    }

    @Override
    public Product_template_attribute_value getDraft(Product_template_attribute_value et) {
        et=product_template_attribute_valueFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_template_attribute_valueFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_template_attribute_valueFeignClient.removeBatch(idList);
    }

    @Override
    public Product_template_attribute_value get(Integer id) {
		Product_template_attribute_value et=product_template_attribute_valueFeignClient.get(id);
        if(et==null){
            et=new Product_template_attribute_value();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template_attribute_value> searchDefault(Product_template_attribute_valueSearchContext context) {
        Page<Product_template_attribute_value> product_template_attribute_values=product_template_attribute_valueFeignClient.searchDefault(context);
        return product_template_attribute_values;
    }


}



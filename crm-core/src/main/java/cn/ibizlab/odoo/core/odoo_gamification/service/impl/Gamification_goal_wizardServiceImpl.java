package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goal_wizardFeignClient;

/**
 * 实体[游戏化目标向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goal_wizardServiceImpl implements IGamification_goal_wizardService {

    @Autowired
    gamification_goal_wizardFeignClient gamification_goal_wizardFeignClient;


    @Override
    public Gamification_goal_wizard getDraft(Gamification_goal_wizard et) {
        et=gamification_goal_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_goal_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_goal_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Gamification_goal_wizard et) {
        Gamification_goal_wizard rt = gamification_goal_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_goal_wizard> list){
        gamification_goal_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Gamification_goal_wizard et) {
        Gamification_goal_wizard rt = gamification_goal_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal_wizard> list){
        gamification_goal_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public Gamification_goal_wizard get(Integer id) {
		Gamification_goal_wizard et=gamification_goal_wizardFeignClient.get(id);
        if(et==null){
            et=new Gamification_goal_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal_wizard> searchDefault(Gamification_goal_wizardSearchContext context) {
        Page<Gamification_goal_wizard> gamification_goal_wizards=gamification_goal_wizardFeignClient.searchDefault(context);
        return gamification_goal_wizards;
    }


}



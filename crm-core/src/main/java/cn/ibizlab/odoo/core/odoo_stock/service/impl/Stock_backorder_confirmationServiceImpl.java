package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_backorder_confirmationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_backorder_confirmationFeignClient;

/**
 * 实体[欠单确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_backorder_confirmationServiceImpl implements IStock_backorder_confirmationService {

    @Autowired
    stock_backorder_confirmationFeignClient stock_backorder_confirmationFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_backorder_confirmationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_backorder_confirmationFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_backorder_confirmation get(Integer id) {
		Stock_backorder_confirmation et=stock_backorder_confirmationFeignClient.get(id);
        if(et==null){
            et=new Stock_backorder_confirmation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_backorder_confirmation getDraft(Stock_backorder_confirmation et) {
        et=stock_backorder_confirmationFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_backorder_confirmation et) {
        Stock_backorder_confirmation rt = stock_backorder_confirmationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_backorder_confirmation> list){
        stock_backorder_confirmationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_backorder_confirmation et) {
        Stock_backorder_confirmation rt = stock_backorder_confirmationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_backorder_confirmation> list){
        stock_backorder_confirmationFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context) {
        Page<Stock_backorder_confirmation> stock_backorder_confirmations=stock_backorder_confirmationFeignClient.searchDefault(context);
        return stock_backorder_confirmations;
    }


}



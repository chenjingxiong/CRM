package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
@FeignClient(value = "odoo-maintenance", contextId = "maintenance-equipment-category", fallback = maintenance_equipment_categoryFallback.class)
public interface maintenance_equipment_categoryFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories")
    Maintenance_equipment_category create(@RequestBody Maintenance_equipment_category maintenance_equipment_category);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/batch")
    Boolean createBatch(@RequestBody List<Maintenance_equipment_category> maintenance_equipment_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/{id}")
    Maintenance_equipment_category update(@PathVariable("id") Integer id,@RequestBody Maintenance_equipment_category maintenance_equipment_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/batch")
    Boolean updateBatch(@RequestBody List<Maintenance_equipment_category> maintenance_equipment_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/{id}")
    Maintenance_equipment_category get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/searchdefault")
    Page<Maintenance_equipment_category> searchDefault(@RequestBody Maintenance_equipment_categorySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/select")
    Page<Maintenance_equipment_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/getdraft")
    Maintenance_equipment_category getDraft();


}

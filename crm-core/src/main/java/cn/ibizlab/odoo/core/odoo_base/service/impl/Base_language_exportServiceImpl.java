package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_exportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_language_exportFeignClient;

/**
 * 实体[语言输出] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_language_exportServiceImpl implements IBase_language_exportService {

    @Autowired
    base_language_exportFeignClient base_language_exportFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=base_language_exportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_language_exportFeignClient.removeBatch(idList);
    }

    @Override
    public Base_language_export get(Integer id) {
		Base_language_export et=base_language_exportFeignClient.get(id);
        if(et==null){
            et=new Base_language_export();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_language_export et) {
        Base_language_export rt = base_language_exportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_language_export> list){
        base_language_exportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_language_export et) {
        Base_language_export rt = base_language_exportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_language_export> list){
        base_language_exportFeignClient.createBatch(list) ;
    }

    @Override
    public Base_language_export getDraft(Base_language_export et) {
        et=base_language_exportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_language_export> searchDefault(Base_language_exportSearchContext context) {
        Page<Base_language_export> base_language_exports=base_language_exportFeignClient.searchDefault(context);
        return base_language_exports;
    }


}



package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
@Component
public class mail_blacklist_mixinFallback implements mail_blacklist_mixinFeignClient{

    public Mail_blacklist_mixin create(Mail_blacklist_mixin mail_blacklist_mixin){
            return null;
     }
    public Boolean createBatch(List<Mail_blacklist_mixin> mail_blacklist_mixins){
            return false;
     }

    public Page<Mail_blacklist_mixin> searchDefault(Mail_blacklist_mixinSearchContext context){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Mail_blacklist_mixin update(Integer id, Mail_blacklist_mixin mail_blacklist_mixin){
            return null;
     }
    public Boolean updateBatch(List<Mail_blacklist_mixin> mail_blacklist_mixins){
            return false;
     }


    public Mail_blacklist_mixin get(Integer id){
            return null;
     }


    public Page<Mail_blacklist_mixin> select(){
            return null;
     }

    public Mail_blacklist_mixin getDraft(){
            return null;
    }



}

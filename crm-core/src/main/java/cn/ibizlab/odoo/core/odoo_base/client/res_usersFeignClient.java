package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_users] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-users", fallback = res_usersFallback.class)
public interface res_usersFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_users/{id}")
    Res_users get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users")
    Res_users create(@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users/batch")
    Boolean createBatch(@RequestBody List<Res_users> res_users);



    @RequestMapping(method = RequestMethod.POST, value = "/res_users/searchdefault")
    Page<Res_users> searchDefault(@RequestBody Res_usersSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/res_users/{id}")
    Res_users update(@PathVariable("id") Integer id,@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_users/batch")
    Boolean updateBatch(@RequestBody List<Res_users> res_users);


    @RequestMapping(method = RequestMethod.GET, value = "/res_users/select")
    Page<Res_users> select();


    @RequestMapping(method = RequestMethod.POST, value = "/res_users/save")
    Boolean save(@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users/save")
    Boolean saveBatch(@RequestBody List<Res_users> res_users);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users/checkkey")
    Boolean checkKey(@RequestBody Res_users res_users);


    @RequestMapping(method = RequestMethod.GET, value = "/res_users/getdraft")
    Res_users getDraft();


}

package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_templateFeignClient;

/**
 * 实体[报价单模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_templateServiceImpl implements ISale_order_templateService {

    @Autowired
    sale_order_templateFeignClient sale_order_templateFeignClient;


    @Override
    public boolean create(Sale_order_template et) {
        Sale_order_template rt = sale_order_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_template> list){
        sale_order_templateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Sale_order_template et) {
        Sale_order_template rt = sale_order_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_order_template> list){
        sale_order_templateFeignClient.updateBatch(list) ;
    }

    @Override
    public Sale_order_template get(Integer id) {
		Sale_order_template et=sale_order_templateFeignClient.get(id);
        if(et==null){
            et=new Sale_order_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sale_order_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_order_templateFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_order_template getDraft(Sale_order_template et) {
        et=sale_order_templateFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context) {
        Page<Sale_order_template> sale_order_templates=sale_order_templateFeignClient.searchDefault(context);
        return sale_order_templates;
    }


}



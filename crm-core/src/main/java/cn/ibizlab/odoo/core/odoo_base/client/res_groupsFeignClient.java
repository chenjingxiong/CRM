package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_groups] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-groups", fallback = res_groupsFallback.class)
public interface res_groupsFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_groups")
    Res_groups create(@RequestBody Res_groups res_groups);

    @RequestMapping(method = RequestMethod.POST, value = "/res_groups/batch")
    Boolean createBatch(@RequestBody List<Res_groups> res_groups);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_groups/{id}")
    Res_groups update(@PathVariable("id") Integer id,@RequestBody Res_groups res_groups);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_groups/batch")
    Boolean updateBatch(@RequestBody List<Res_groups> res_groups);



    @RequestMapping(method = RequestMethod.GET, value = "/res_groups/{id}")
    Res_groups get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/res_groups/searchdefault")
    Page<Res_groups> searchDefault(@RequestBody Res_groupsSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/res_groups/select")
    Page<Res_groups> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_groups/getdraft")
    Res_groups getDraft();


}

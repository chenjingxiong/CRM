package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_leave_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave_type] 服务对象接口
 */
public interface Ihr_leave_typeClientService{

    public Ihr_leave_type createModel() ;

    public void updateBatch(List<Ihr_leave_type> hr_leave_types);

    public void remove(Ihr_leave_type hr_leave_type);

    public void create(Ihr_leave_type hr_leave_type);

    public Page<Ihr_leave_type> fetchDefault(SearchContext context);

    public void get(Ihr_leave_type hr_leave_type);

    public void update(Ihr_leave_type hr_leave_type);

    public void createBatch(List<Ihr_leave_type> hr_leave_types);

    public void removeBatch(List<Ihr_leave_type> hr_leave_types);

    public Page<Ihr_leave_type> select(SearchContext context);

    public void getDraft(Ihr_leave_type hr_leave_type);

}

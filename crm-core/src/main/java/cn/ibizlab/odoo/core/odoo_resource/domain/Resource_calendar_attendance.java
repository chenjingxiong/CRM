package cn.ibizlab.odoo.core.odoo_resource.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作细节] 对象
 */
@Data
public class Resource_calendar_attendance extends EntityClient implements Serializable {

    /**
     * 星期
     */
    @JSONField(name = "dayofweek")
    @JsonProperty("dayofweek")
    private String dayofweek;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 工作截止
     */
    @DEField(name = "hour_to")
    @JSONField(name = "hour_to")
    @JsonProperty("hour_to")
    private Double hourTo;

    /**
     * 日期
     */
    @DEField(name = "day_period")
    @JSONField(name = "day_period")
    @JsonProperty("day_period")
    private String dayPeriod;

    /**
     * 工作起始
     */
    @DEField(name = "hour_from")
    @JSONField(name = "hour_from")
    @JsonProperty("hour_from")
    private Double hourFrom;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 结束日期
     */
    @DEField(name = "date_to")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd")
    @JsonProperty("date_to")
    private Timestamp dateTo;

    /**
     * 起始日期
     */
    @DEField(name = "date_from")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd")
    @JsonProperty("date_from")
    private Timestamp dateFrom;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 资源的日历
     */
    @JSONField(name = "calendar_id_text")
    @JsonProperty("calendar_id_text")
    private String calendarIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 资源的日历
     */
    @DEField(name = "calendar_id")
    @JSONField(name = "calendar_id")
    @JsonProperty("calendar_id")
    private Integer calendarId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocalendar")
    @JsonProperty("odoocalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooCalendar;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [星期]
     */
    public void setDayofweek(String dayofweek){
        this.dayofweek = dayofweek ;
        this.modify("dayofweek",dayofweek);
    }
    /**
     * 设置 [工作截止]
     */
    public void setHourTo(Double hourTo){
        this.hourTo = hourTo ;
        this.modify("hour_to",hourTo);
    }
    /**
     * 设置 [日期]
     */
    public void setDayPeriod(String dayPeriod){
        this.dayPeriod = dayPeriod ;
        this.modify("day_period",dayPeriod);
    }
    /**
     * 设置 [工作起始]
     */
    public void setHourFrom(Double hourFrom){
        this.hourFrom = hourFrom ;
        this.modify("hour_from",hourFrom);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateTo(Timestamp dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }
    /**
     * 设置 [起始日期]
     */
    public void setDateFrom(Timestamp dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }
    /**
     * 设置 [资源的日历]
     */
    public void setCalendarId(Integer calendarId){
        this.calendarId = calendarId ;
        this.modify("calendar_id",calendarId);
    }

}



package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_request_reject] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-request-reject", fallback = mro_request_rejectFallback.class)
public interface mro_request_rejectFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/{id}")
    Mro_request_reject update(@PathVariable("id") Integer id,@RequestBody Mro_request_reject mro_request_reject);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/batch")
    Boolean updateBatch(@RequestBody List<Mro_request_reject> mro_request_rejects);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects")
    Mro_request_reject create(@RequestBody Mro_request_reject mro_request_reject);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/batch")
    Boolean createBatch(@RequestBody List<Mro_request_reject> mro_request_rejects);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/searchdefault")
    Page<Mro_request_reject> searchDefault(@RequestBody Mro_request_rejectSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/{id}")
    Mro_request_reject get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/select")
    Page<Mro_request_reject> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/getdraft")
    Mro_request_reject getDraft();


}

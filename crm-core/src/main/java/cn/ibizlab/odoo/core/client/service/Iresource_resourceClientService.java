package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iresource_resource;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_resource] 服务对象接口
 */
public interface Iresource_resourceClientService{

    public Iresource_resource createModel() ;

    public void updateBatch(List<Iresource_resource> resource_resources);

    public void remove(Iresource_resource resource_resource);

    public void removeBatch(List<Iresource_resource> resource_resources);

    public void get(Iresource_resource resource_resource);

    public void createBatch(List<Iresource_resource> resource_resources);

    public Page<Iresource_resource> fetchDefault(SearchContext context);

    public void update(Iresource_resource resource_resource);

    public void create(Iresource_resource resource_resource);

    public Page<Iresource_resource> select(SearchContext context);

    public void getDraft(Iresource_resource resource_resource);

}

package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
@Component
public class mrp_unbuildFallback implements mrp_unbuildFeignClient{



    public Mrp_unbuild create(Mrp_unbuild mrp_unbuild){
            return null;
     }
    public Boolean createBatch(List<Mrp_unbuild> mrp_unbuilds){
            return false;
     }


    public Mrp_unbuild update(Integer id, Mrp_unbuild mrp_unbuild){
            return null;
     }
    public Boolean updateBatch(List<Mrp_unbuild> mrp_unbuilds){
            return false;
     }


    public Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_unbuild get(Integer id){
            return null;
     }


    public Page<Mrp_unbuild> select(){
            return null;
     }

    public Mrp_unbuild getDraft(){
            return null;
    }



}

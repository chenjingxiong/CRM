package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_location_route;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_location_route] 服务对象接口
 */
public interface Istock_location_routeClientService{

    public Istock_location_route createModel() ;

    public Page<Istock_location_route> fetchDefault(SearchContext context);

    public void create(Istock_location_route stock_location_route);

    public void createBatch(List<Istock_location_route> stock_location_routes);

    public void removeBatch(List<Istock_location_route> stock_location_routes);

    public void updateBatch(List<Istock_location_route> stock_location_routes);

    public void get(Istock_location_route stock_location_route);

    public void update(Istock_location_route stock_location_route);

    public void remove(Istock_location_route stock_location_route);

    public Page<Istock_location_route> select(SearchContext context);

    public void getDraft(Istock_location_route stock_location_route);

}

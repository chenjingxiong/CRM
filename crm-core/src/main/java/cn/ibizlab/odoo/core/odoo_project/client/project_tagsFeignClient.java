package cn.ibizlab.odoo.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[project_tags] 服务对象接口
 */
@FeignClient(value = "odoo-project", contextId = "project-tags", fallback = project_tagsFallback.class)
public interface project_tagsFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/searchdefault")
    Page<Project_tags> searchDefault(@RequestBody Project_tagsSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/project_tags/{id}")
    Project_tags update(@PathVariable("id") Integer id,@RequestBody Project_tags project_tags);

    @RequestMapping(method = RequestMethod.PUT, value = "/project_tags/batch")
    Boolean updateBatch(@RequestBody List<Project_tags> project_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/project_tags")
    Project_tags create(@RequestBody Project_tags project_tags);

    @RequestMapping(method = RequestMethod.POST, value = "/project_tags/batch")
    Boolean createBatch(@RequestBody List<Project_tags> project_tags);



    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/{id}")
    Project_tags get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/select")
    Page<Project_tags> select();


    @RequestMapping(method = RequestMethod.GET, value = "/project_tags/getdraft")
    Project_tags getDraft();


}

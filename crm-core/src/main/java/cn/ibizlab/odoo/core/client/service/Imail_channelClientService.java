package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_channel] 服务对象接口
 */
public interface Imail_channelClientService{

    public Imail_channel createModel() ;

    public void updateBatch(List<Imail_channel> mail_channels);

    public void create(Imail_channel mail_channel);

    public Page<Imail_channel> fetchDefault(SearchContext context);

    public void get(Imail_channel mail_channel);

    public void removeBatch(List<Imail_channel> mail_channels);

    public void createBatch(List<Imail_channel> mail_channels);

    public void update(Imail_channel mail_channel);

    public void remove(Imail_channel mail_channel);

    public Page<Imail_channel> select(SearchContext context);

    public void getDraft(Imail_channel mail_channel);

}

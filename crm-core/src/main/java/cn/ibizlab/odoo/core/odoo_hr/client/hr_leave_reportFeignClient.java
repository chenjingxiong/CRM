package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_leave_report] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-leave-report", fallback = hr_leave_reportFallback.class)
public interface hr_leave_reportFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/{id}")
    Hr_leave_report update(@PathVariable("id") Integer id,@RequestBody Hr_leave_report hr_leave_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/batch")
    Boolean updateBatch(@RequestBody List<Hr_leave_report> hr_leave_reports);





    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/searchdefault")
    Page<Hr_leave_report> searchDefault(@RequestBody Hr_leave_reportSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports")
    Hr_leave_report create(@RequestBody Hr_leave_report hr_leave_report);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/batch")
    Boolean createBatch(@RequestBody List<Hr_leave_report> hr_leave_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/{id}")
    Hr_leave_report get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/select")
    Page<Hr_leave_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/getdraft")
    Hr_leave_report getDraft();


}

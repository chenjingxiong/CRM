package cn.ibizlab.odoo.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[portal_wizard_user] 服务对象接口
 */
@Component
public class portal_wizard_userFallback implements portal_wizard_userFeignClient{

    public Portal_wizard_user get(Integer id){
            return null;
     }


    public Portal_wizard_user create(Portal_wizard_user portal_wizard_user){
            return null;
     }
    public Boolean createBatch(List<Portal_wizard_user> portal_wizard_users){
            return false;
     }




    public Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Portal_wizard_user update(Integer id, Portal_wizard_user portal_wizard_user){
            return null;
     }
    public Boolean updateBatch(List<Portal_wizard_user> portal_wizard_users){
            return false;
     }


    public Page<Portal_wizard_user> select(){
            return null;
     }

    public Portal_wizard_user getDraft(){
            return null;
    }



}

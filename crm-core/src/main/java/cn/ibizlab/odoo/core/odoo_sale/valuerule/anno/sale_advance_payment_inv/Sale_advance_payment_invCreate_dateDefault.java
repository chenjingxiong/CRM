package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_advance_payment_inv;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_advance_payment_inv.Sale_advance_payment_invCreate_dateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_advance_payment_inv
 * 属性：Create_date
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_advance_payment_invCreate_dateDefaultValidator.class})
public @interface Sale_advance_payment_invCreate_dateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

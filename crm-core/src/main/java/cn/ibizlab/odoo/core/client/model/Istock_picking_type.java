package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_picking_type] 对象
 */
public interface Istock_picking_type {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [条码]
     */
    public void setBarcode(String barcode);
    
    /**
     * 设置 [条码]
     */
    public String getBarcode();

    /**
     * 获取 [条码]脏标记
     */
    public boolean getBarcodeDirtyFlag();
    /**
     * 获取 [作业的类型]
     */
    public void setCode(String code);
    
    /**
     * 设置 [作业的类型]
     */
    public String getCode();

    /**
     * 获取 [作业的类型]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色]
     */
    public Integer getColor();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [生产单数量延迟]
     */
    public void setCount_mo_late(Integer count_mo_late);
    
    /**
     * 设置 [生产单数量延迟]
     */
    public Integer getCount_mo_late();

    /**
     * 获取 [生产单数量延迟]脏标记
     */
    public boolean getCount_mo_lateDirtyFlag();
    /**
     * 获取 [生产单的数量]
     */
    public void setCount_mo_todo(Integer count_mo_todo);
    
    /**
     * 设置 [生产单的数量]
     */
    public Integer getCount_mo_todo();

    /**
     * 获取 [生产单的数量]脏标记
     */
    public boolean getCount_mo_todoDirtyFlag();
    /**
     * 获取 [生产单等待的数量]
     */
    public void setCount_mo_waiting(Integer count_mo_waiting);
    
    /**
     * 设置 [生产单等待的数量]
     */
    public Integer getCount_mo_waiting();

    /**
     * 获取 [生产单等待的数量]脏标记
     */
    public boolean getCount_mo_waitingDirtyFlag();
    /**
     * 获取 [拣货个数]
     */
    public void setCount_picking(Integer count_picking);
    
    /**
     * 设置 [拣货个数]
     */
    public Integer getCount_picking();

    /**
     * 获取 [拣货个数]脏标记
     */
    public boolean getCount_pickingDirtyFlag();
    /**
     * 获取 [拣货欠单个数]
     */
    public void setCount_picking_backorders(Integer count_picking_backorders);
    
    /**
     * 设置 [拣货欠单个数]
     */
    public Integer getCount_picking_backorders();

    /**
     * 获取 [拣货欠单个数]脏标记
     */
    public boolean getCount_picking_backordersDirtyFlag();
    /**
     * 获取 [草稿拣货个数]
     */
    public void setCount_picking_draft(Integer count_picking_draft);
    
    /**
     * 设置 [草稿拣货个数]
     */
    public Integer getCount_picking_draft();

    /**
     * 获取 [草稿拣货个数]脏标记
     */
    public boolean getCount_picking_draftDirtyFlag();
    /**
     * 获取 [迟到拣货个数]
     */
    public void setCount_picking_late(Integer count_picking_late);
    
    /**
     * 设置 [迟到拣货个数]
     */
    public Integer getCount_picking_late();

    /**
     * 获取 [迟到拣货个数]脏标记
     */
    public boolean getCount_picking_lateDirtyFlag();
    /**
     * 获取 [拣货个数准备好]
     */
    public void setCount_picking_ready(Integer count_picking_ready);
    
    /**
     * 设置 [拣货个数准备好]
     */
    public Integer getCount_picking_ready();

    /**
     * 获取 [拣货个数准备好]脏标记
     */
    public boolean getCount_picking_readyDirtyFlag();
    /**
     * 获取 [等待拣货个数]
     */
    public void setCount_picking_waiting(Integer count_picking_waiting);
    
    /**
     * 设置 [等待拣货个数]
     */
    public Integer getCount_picking_waiting();

    /**
     * 获取 [等待拣货个数]脏标记
     */
    public boolean getCount_picking_waitingDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [默认目的位置]
     */
    public void setDefault_location_dest_id(Integer default_location_dest_id);
    
    /**
     * 设置 [默认目的位置]
     */
    public Integer getDefault_location_dest_id();

    /**
     * 获取 [默认目的位置]脏标记
     */
    public boolean getDefault_location_dest_idDirtyFlag();
    /**
     * 获取 [默认目的位置]
     */
    public void setDefault_location_dest_id_text(String default_location_dest_id_text);
    
    /**
     * 设置 [默认目的位置]
     */
    public String getDefault_location_dest_id_text();

    /**
     * 获取 [默认目的位置]脏标记
     */
    public boolean getDefault_location_dest_id_textDirtyFlag();
    /**
     * 获取 [默认源位置]
     */
    public void setDefault_location_src_id(Integer default_location_src_id);
    
    /**
     * 设置 [默认源位置]
     */
    public Integer getDefault_location_src_id();

    /**
     * 获取 [默认源位置]脏标记
     */
    public boolean getDefault_location_src_idDirtyFlag();
    /**
     * 获取 [默认源位置]
     */
    public void setDefault_location_src_id_text(String default_location_src_id_text);
    
    /**
     * 设置 [默认源位置]
     */
    public String getDefault_location_src_id_text();

    /**
     * 获取 [默认源位置]脏标记
     */
    public boolean getDefault_location_src_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最近10笔完成的拣货]
     */
    public void setLast_done_picking(String last_done_picking);
    
    /**
     * 设置 [最近10笔完成的拣货]
     */
    public String getLast_done_picking();

    /**
     * 获取 [最近10笔完成的拣货]脏标记
     */
    public boolean getLast_done_pickingDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setName(String name);
    
    /**
     * 设置 [作业类型]
     */
    public String getName();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [欠单比率]
     */
    public void setRate_picking_backorders(Integer rate_picking_backorders);
    
    /**
     * 设置 [欠单比率]
     */
    public Integer getRate_picking_backorders();

    /**
     * 获取 [欠单比率]脏标记
     */
    public boolean getRate_picking_backordersDirtyFlag();
    /**
     * 获取 [延迟比率]
     */
    public void setRate_picking_late(Integer rate_picking_late);
    
    /**
     * 设置 [延迟比率]
     */
    public Integer getRate_picking_late();

    /**
     * 获取 [延迟比率]脏标记
     */
    public boolean getRate_picking_lateDirtyFlag();
    /**
     * 获取 [退回的作业类型]
     */
    public void setReturn_picking_type_id(Integer return_picking_type_id);
    
    /**
     * 设置 [退回的作业类型]
     */
    public Integer getReturn_picking_type_id();

    /**
     * 获取 [退回的作业类型]脏标记
     */
    public boolean getReturn_picking_type_idDirtyFlag();
    /**
     * 获取 [退回的作业类型]
     */
    public void setReturn_picking_type_id_text(String return_picking_type_id_text);
    
    /**
     * 设置 [退回的作业类型]
     */
    public String getReturn_picking_type_id_text();

    /**
     * 获取 [退回的作业类型]脏标记
     */
    public boolean getReturn_picking_type_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [参考序列]
     */
    public void setSequence_id(Integer sequence_id);
    
    /**
     * 设置 [参考序列]
     */
    public Integer getSequence_id();

    /**
     * 获取 [参考序列]脏标记
     */
    public boolean getSequence_idDirtyFlag();
    /**
     * 获取 [移动整个包裹]
     */
    public void setShow_entire_packs(String show_entire_packs);
    
    /**
     * 设置 [移动整个包裹]
     */
    public String getShow_entire_packs();

    /**
     * 获取 [移动整个包裹]脏标记
     */
    public boolean getShow_entire_packsDirtyFlag();
    /**
     * 获取 [显示详细作业]
     */
    public void setShow_operations(String show_operations);
    
    /**
     * 设置 [显示详细作业]
     */
    public String getShow_operations();

    /**
     * 获取 [显示详细作业]脏标记
     */
    public boolean getShow_operationsDirtyFlag();
    /**
     * 获取 [显示预留]
     */
    public void setShow_reserved(String show_reserved);
    
    /**
     * 设置 [显示预留]
     */
    public String getShow_reserved();

    /**
     * 获取 [显示预留]脏标记
     */
    public boolean getShow_reservedDirtyFlag();
    /**
     * 获取 [创建新批次/序列号码]
     */
    public void setUse_create_lots(String use_create_lots);
    
    /**
     * 设置 [创建新批次/序列号码]
     */
    public String getUse_create_lots();

    /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    public boolean getUse_create_lotsDirtyFlag();
    /**
     * 获取 [使用已有批次/序列号码]
     */
    public void setUse_existing_lots(String use_existing_lots);
    
    /**
     * 设置 [使用已有批次/序列号码]
     */
    public String getUse_existing_lots();

    /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    public boolean getUse_existing_lotsDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id_text(String warehouse_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

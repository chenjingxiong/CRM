package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_calendar_leaves] 服务对象接口
 */
@FeignClient(value = "odoo-resource", contextId = "resource-calendar-leaves", fallback = resource_calendar_leavesFallback.class)
public interface resource_calendar_leavesFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/{id}")
    Resource_calendar_leaves update(@PathVariable("id") Integer id,@RequestBody Resource_calendar_leaves resource_calendar_leaves);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/batch")
    Boolean updateBatch(@RequestBody List<Resource_calendar_leaves> resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/{id}")
    Resource_calendar_leaves get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/searchdefault")
    Page<Resource_calendar_leaves> searchDefault(@RequestBody Resource_calendar_leavesSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves")
    Resource_calendar_leaves create(@RequestBody Resource_calendar_leaves resource_calendar_leaves);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/batch")
    Boolean createBatch(@RequestBody List<Resource_calendar_leaves> resource_calendar_leaves);




    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/select")
    Page<Resource_calendar_leaves> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/getdraft")
    Resource_calendar_leaves getDraft();


}

package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;


/**
 * 实体[Hr_expense] 服务对象接口
 */
public interface IHr_expenseService{

    boolean update(Hr_expense et) ;
    void updateBatch(List<Hr_expense> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_expense get(Integer key) ;
    boolean create(Hr_expense et) ;
    void createBatch(List<Hr_expense> list) ;
    Hr_expense getDraft(Hr_expense et) ;
    Page<Hr_expense> searchDefault(Hr_expenseSearchContext context) ;

}




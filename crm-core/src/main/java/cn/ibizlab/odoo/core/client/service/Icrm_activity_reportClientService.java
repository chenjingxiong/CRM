package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_activity_report] 服务对象接口
 */
public interface Icrm_activity_reportClientService{

    public Icrm_activity_report createModel() ;

    public void removeBatch(List<Icrm_activity_report> crm_activity_reports);

    public void createBatch(List<Icrm_activity_report> crm_activity_reports);

    public void updateBatch(List<Icrm_activity_report> crm_activity_reports);

    public Page<Icrm_activity_report> fetchDefault(SearchContext context);

    public void create(Icrm_activity_report crm_activity_report);

    public void get(Icrm_activity_report crm_activity_report);

    public void update(Icrm_activity_report crm_activity_report);

    public void remove(Icrm_activity_report crm_activity_report);

    public Page<Icrm_activity_report> select(SearchContext context);

    public void getDraft(Icrm_activity_report crm_activity_report);

}

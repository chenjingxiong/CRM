package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_tax_template;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_tax_template.Account_tax_templateType_tax_useDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_tax_template
 * 属性：Type_tax_use
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_tax_templateType_tax_useDefaultValidator.class})
public @interface Account_tax_templateType_tax_useDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

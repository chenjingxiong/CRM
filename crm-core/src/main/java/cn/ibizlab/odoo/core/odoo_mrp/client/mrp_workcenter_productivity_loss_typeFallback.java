package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
@Component
public class mrp_workcenter_productivity_loss_typeFallback implements mrp_workcenter_productivity_loss_typeFeignClient{


    public Mrp_workcenter_productivity_loss_type create(Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
            return null;
     }
    public Boolean createBatch(List<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
            return false;
     }



    public Mrp_workcenter_productivity_loss_type get(Integer id){
            return null;
     }


    public Mrp_workcenter_productivity_loss_type update(Integer id, Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type){
            return null;
     }
    public Boolean updateBatch(List<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types){
            return false;
     }


    public Page<Mrp_workcenter_productivity_loss_type> searchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mrp_workcenter_productivity_loss_type> select(){
            return null;
     }

    public Mrp_workcenter_productivity_loss_type getDraft(){
            return null;
    }



}

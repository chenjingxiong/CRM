package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;


/**
 * 实体[Crm_lead2opportunity_partner] 服务对象接口
 */
public interface ICrm_lead2opportunity_partnerService{

    boolean create(Crm_lead2opportunity_partner et) ;
    void createBatch(List<Crm_lead2opportunity_partner> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Crm_lead2opportunity_partner get(Integer key) ;
    boolean update(Crm_lead2opportunity_partner et) ;
    void updateBatch(List<Crm_lead2opportunity_partner> list) ;
    Crm_lead2opportunity_partner getDraft(Crm_lead2opportunity_partner et) ;
    Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_bus.valuerule.anno.bus_presence;

import cn.ibizlab.odoo.core.odoo_bus.valuerule.validator.bus_presence.Bus_presenceLast_pollDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Bus_presence
 * 属性：Last_poll
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Bus_presenceLast_pollDefaultValidator.class})
public @interface Bus_presenceLast_pollDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

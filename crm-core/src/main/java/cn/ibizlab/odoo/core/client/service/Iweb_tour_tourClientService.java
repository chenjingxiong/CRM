package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iweb_tour_tour;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[web_tour_tour] 服务对象接口
 */
public interface Iweb_tour_tourClientService{

    public Iweb_tour_tour createModel() ;

    public void createBatch(List<Iweb_tour_tour> web_tour_tours);

    public void get(Iweb_tour_tour web_tour_tour);

    public void removeBatch(List<Iweb_tour_tour> web_tour_tours);

    public void updateBatch(List<Iweb_tour_tour> web_tour_tours);

    public void update(Iweb_tour_tour web_tour_tour);

    public Page<Iweb_tour_tour> fetchDefault(SearchContext context);

    public void remove(Iweb_tour_tour web_tour_tour);

    public void create(Iweb_tour_tour web_tour_tour);

    public Page<Iweb_tour_tour> select(SearchContext context);

    public void getDraft(Iweb_tour_tour web_tour_tour);

}

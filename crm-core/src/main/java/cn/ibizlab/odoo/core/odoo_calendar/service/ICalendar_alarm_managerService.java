package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;


/**
 * 实体[Calendar_alarm_manager] 服务对象接口
 */
public interface ICalendar_alarm_managerService{

    Calendar_alarm_manager getDraft(Calendar_alarm_manager et) ;
    Calendar_alarm_manager get(Integer key) ;
    boolean update(Calendar_alarm_manager et) ;
    void updateBatch(List<Calendar_alarm_manager> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Calendar_alarm_manager et) ;
    void createBatch(List<Calendar_alarm_manager> list) ;
    Page<Calendar_alarm_manager> searchDefault(Calendar_alarm_managerSearchContext context) ;

}




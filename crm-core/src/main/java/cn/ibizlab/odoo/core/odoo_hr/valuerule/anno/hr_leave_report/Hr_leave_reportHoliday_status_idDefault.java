package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave_report;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_leave_report.Hr_leave_reportHoliday_status_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_leave_report
 * 属性：Holiday_status_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_leave_reportHoliday_status_idDefaultValidator.class})
public @interface Hr_leave_reportHoliday_status_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

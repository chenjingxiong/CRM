package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position] 服务对象接口
 */
public interface Iaccount_fiscal_positionClientService{

    public Iaccount_fiscal_position createModel() ;

    public void update(Iaccount_fiscal_position account_fiscal_position);

    public void removeBatch(List<Iaccount_fiscal_position> account_fiscal_positions);

    public void remove(Iaccount_fiscal_position account_fiscal_position);

    public void get(Iaccount_fiscal_position account_fiscal_position);

    public Page<Iaccount_fiscal_position> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_fiscal_position> account_fiscal_positions);

    public void create(Iaccount_fiscal_position account_fiscal_position);

    public void createBatch(List<Iaccount_fiscal_position> account_fiscal_positions);

    public Page<Iaccount_fiscal_position> select(SearchContext context);

    public void getDraft(Iaccount_fiscal_position account_fiscal_position);

}

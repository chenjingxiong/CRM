package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workorder;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_workorder.Mrp_workorderIs_user_workingDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_workorder
 * 属性：Is_user_working
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_workorderIs_user_workingDefaultValidator.class})
public @interface Mrp_workorderIs_user_workingDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

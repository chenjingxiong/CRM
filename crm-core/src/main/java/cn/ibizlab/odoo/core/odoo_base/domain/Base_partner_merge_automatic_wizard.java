package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [合并业务伙伴向导] 对象
 */
@Data
public class Base_partner_merge_automatic_wizard extends EntityClient implements Serializable {

    /**
     * 联系人组的最多联系人数量
     */
    @DEField(name = "maximum_group")
    @JSONField(name = "maximum_group")
    @JsonProperty("maximum_group")
    private Integer maximumGroup;

    /**
     * 上级公司
     */
    @DEField(name = "group_by_parent_id")
    @JSONField(name = "group_by_parent_id")
    @JsonProperty("group_by_parent_id")
    private String groupByParentId;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 联系人组
     */
    @DEField(name = "number_group")
    @JSONField(name = "number_group")
    @JsonProperty("number_group")
    private Integer numberGroup;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 明细行
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;

    /**
     * 增值税
     */
    @DEField(name = "group_by_vat")
    @JSONField(name = "group_by_vat")
    @JsonProperty("group_by_vat")
    private String groupByVat;

    /**
     * 与系统用户相关的联系人
     */
    @DEField(name = "exclude_contact")
    @JSONField(name = "exclude_contact")
    @JsonProperty("exclude_contact")
    private String excludeContact;

    /**
     * 联系人
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * EMail
     */
    @DEField(name = "group_by_email")
    @JSONField(name = "group_by_email")
    @JsonProperty("group_by_email")
    private String groupByEmail;

    /**
     * 是公司
     */
    @DEField(name = "group_by_is_company")
    @JSONField(name = "group_by_is_company")
    @JsonProperty("group_by_is_company")
    private String groupByIsCompany;

    /**
     * 省/ 州
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 与日记账相关的联系人
     */
    @DEField(name = "exclude_journal_item")
    @JSONField(name = "exclude_journal_item")
    @JsonProperty("exclude_journal_item")
    private String excludeJournalItem;

    /**
     * 名称
     */
    @DEField(name = "group_by_name")
    @JSONField(name = "group_by_name")
    @JsonProperty("group_by_name")
    private String groupByName;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 目的地之联系人
     */
    @JSONField(name = "dst_partner_id_text")
    @JsonProperty("dst_partner_id_text")
    private String dstPartnerIdText;

    /**
     * 目的地之联系人
     */
    @DEField(name = "dst_partner_id")
    @JSONField(name = "dst_partner_id")
    @JsonProperty("dst_partner_id")
    private Integer dstPartnerId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 当前行
     */
    @DEField(name = "current_line_id")
    @JSONField(name = "current_line_id")
    @JsonProperty("current_line_id")
    private Integer currentLineId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocurrentline")
    @JsonProperty("odoocurrentline")
    private cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line odooCurrentLine;

    /**
     * 
     */
    @JSONField(name = "odoodstpartner")
    @JsonProperty("odoodstpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooDstPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [联系人组的最多联系人数量]
     */
    public void setMaximumGroup(Integer maximumGroup){
        this.maximumGroup = maximumGroup ;
        this.modify("maximum_group",maximumGroup);
    }
    /**
     * 设置 [上级公司]
     */
    public void setGroupByParentId(String groupByParentId){
        this.groupByParentId = groupByParentId ;
        this.modify("group_by_parent_id",groupByParentId);
    }
    /**
     * 设置 [联系人组]
     */
    public void setNumberGroup(Integer numberGroup){
        this.numberGroup = numberGroup ;
        this.modify("number_group",numberGroup);
    }
    /**
     * 设置 [增值税]
     */
    public void setGroupByVat(String groupByVat){
        this.groupByVat = groupByVat ;
        this.modify("group_by_vat",groupByVat);
    }
    /**
     * 设置 [与系统用户相关的联系人]
     */
    public void setExcludeContact(String excludeContact){
        this.excludeContact = excludeContact ;
        this.modify("exclude_contact",excludeContact);
    }
    /**
     * 设置 [EMail]
     */
    public void setGroupByEmail(String groupByEmail){
        this.groupByEmail = groupByEmail ;
        this.modify("group_by_email",groupByEmail);
    }
    /**
     * 设置 [是公司]
     */
    public void setGroupByIsCompany(String groupByIsCompany){
        this.groupByIsCompany = groupByIsCompany ;
        this.modify("group_by_is_company",groupByIsCompany);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [与日记账相关的联系人]
     */
    public void setExcludeJournalItem(String excludeJournalItem){
        this.excludeJournalItem = excludeJournalItem ;
        this.modify("exclude_journal_item",excludeJournalItem);
    }
    /**
     * 设置 [名称]
     */
    public void setGroupByName(String groupByName){
        this.groupByName = groupByName ;
        this.modify("group_by_name",groupByName);
    }
    /**
     * 设置 [目的地之联系人]
     */
    public void setDstPartnerId(Integer dstPartnerId){
        this.dstPartnerId = dstPartnerId ;
        this.modify("dst_partner_id",dstPartnerId);
    }
    /**
     * 设置 [当前行]
     */
    public void setCurrentLineId(Integer currentLineId){
        this.currentLineId = currentLineId ;
        this.modify("current_line_id",currentLineId);
    }

}



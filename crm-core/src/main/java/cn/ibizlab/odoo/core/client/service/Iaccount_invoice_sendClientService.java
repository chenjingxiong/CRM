package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_send;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_send] 服务对象接口
 */
public interface Iaccount_invoice_sendClientService{

    public Iaccount_invoice_send createModel() ;

    public Page<Iaccount_invoice_send> fetchDefault(SearchContext context);

    public void create(Iaccount_invoice_send account_invoice_send);

    public void remove(Iaccount_invoice_send account_invoice_send);

    public void createBatch(List<Iaccount_invoice_send> account_invoice_sends);

    public void removeBatch(List<Iaccount_invoice_send> account_invoice_sends);

    public void get(Iaccount_invoice_send account_invoice_send);

    public void updateBatch(List<Iaccount_invoice_send> account_invoice_sends);

    public void update(Iaccount_invoice_send account_invoice_send);

    public Page<Iaccount_invoice_send> select(SearchContext context);

    public void getDraft(Iaccount_invoice_send account_invoice_send);

}

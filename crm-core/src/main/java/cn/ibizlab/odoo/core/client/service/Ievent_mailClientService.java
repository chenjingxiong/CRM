package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_mail] 服务对象接口
 */
public interface Ievent_mailClientService{

    public Ievent_mail createModel() ;

    public void removeBatch(List<Ievent_mail> event_mails);

    public void remove(Ievent_mail event_mail);

    public void create(Ievent_mail event_mail);

    public void createBatch(List<Ievent_mail> event_mails);

    public void updateBatch(List<Ievent_mail> event_mails);

    public Page<Ievent_mail> fetchDefault(SearchContext context);

    public void get(Ievent_mail event_mail);

    public void update(Ievent_mail event_mail);

    public Page<Ievent_mail> select(SearchContext context);

    public void getDraft(Ievent_mail event_mail);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_category] 服务对象接口
 */
public interface Ires_partner_categoryClientService{

    public Ires_partner_category createModel() ;

    public void get(Ires_partner_category res_partner_category);

    public void createBatch(List<Ires_partner_category> res_partner_categories);

    public void updateBatch(List<Ires_partner_category> res_partner_categories);

    public Page<Ires_partner_category> fetchDefault(SearchContext context);

    public void update(Ires_partner_category res_partner_category);

    public void removeBatch(List<Ires_partner_category> res_partner_categories);

    public void create(Ires_partner_category res_partner_category);

    public void remove(Ires_partner_category res_partner_category);

    public Page<Ires_partner_category> select(SearchContext context);

    public void getDraft(Ires_partner_category res_partner_category);

}

package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;


/**
 * 实体[Mail_mail_statistics] 服务对象接口
 */
public interface IMail_mail_statisticsService{

    Mail_mail_statistics getDraft(Mail_mail_statistics et) ;
    boolean update(Mail_mail_statistics et) ;
    void updateBatch(List<Mail_mail_statistics> list) ;
    Mail_mail_statistics get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_mail_statistics et) ;
    void createBatch(List<Mail_mail_statistics> list) ;
    Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context) ;

}




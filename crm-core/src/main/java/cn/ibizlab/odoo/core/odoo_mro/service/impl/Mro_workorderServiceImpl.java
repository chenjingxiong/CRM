package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_workorderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_workorderFeignClient;

/**
 * 实体[工单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_workorderServiceImpl implements IMro_workorderService {

    @Autowired
    mro_workorderFeignClient mro_workorderFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mro_workorderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_workorderFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mro_workorder et) {
        Mro_workorder rt = mro_workorderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_workorder> list){
        mro_workorderFeignClient.updateBatch(list) ;
    }

    @Override
    public Mro_workorder getDraft(Mro_workorder et) {
        et=mro_workorderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mro_workorder et) {
        Mro_workorder rt = mro_workorderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_workorder> list){
        mro_workorderFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_workorder get(Integer id) {
		Mro_workorder et=mro_workorderFeignClient.get(id);
        if(et==null){
            et=new Mro_workorder();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_workorder> searchDefault(Mro_workorderSearchContext context) {
        Page<Mro_workorder> mro_workorders=mro_workorderFeignClient.searchDefault(context);
        return mro_workorders;
    }


}



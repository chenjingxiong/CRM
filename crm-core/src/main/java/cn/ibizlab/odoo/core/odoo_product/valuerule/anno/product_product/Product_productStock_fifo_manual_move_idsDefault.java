package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_product;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_product.Product_productStock_fifo_manual_move_idsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_product
 * 属性：Stock_fifo_manual_move_ids
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_productStock_fifo_manual_move_idsDefaultValidator.class})
public @interface Product_productStock_fifo_manual_move_idsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

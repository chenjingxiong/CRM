package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_parameter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
public interface Imro_pm_parameterClientService{

    public Imro_pm_parameter createModel() ;

    public Page<Imro_pm_parameter> fetchDefault(SearchContext context);

    public void removeBatch(List<Imro_pm_parameter> mro_pm_parameters);

    public void get(Imro_pm_parameter mro_pm_parameter);

    public void create(Imro_pm_parameter mro_pm_parameter);

    public void updateBatch(List<Imro_pm_parameter> mro_pm_parameters);

    public void createBatch(List<Imro_pm_parameter> mro_pm_parameters);

    public void remove(Imro_pm_parameter mro_pm_parameter);

    public void update(Imro_pm_parameter mro_pm_parameter);

    public Page<Imro_pm_parameter> select(SearchContext context);

    public void getDraft(Imro_pm_parameter mro_pm_parameter);

}

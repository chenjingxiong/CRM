package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;


/**
 * 实体[Survey_stage] 服务对象接口
 */
public interface ISurvey_stageService{

    Survey_stage getDraft(Survey_stage et) ;
    boolean update(Survey_stage et) ;
    void updateBatch(List<Survey_stage> list) ;
    Survey_stage get(Integer key) ;
    boolean create(Survey_stage et) ;
    void createBatch(List<Survey_stage> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Survey_stage> searchDefault(Survey_stageSearchContext context) ;

}




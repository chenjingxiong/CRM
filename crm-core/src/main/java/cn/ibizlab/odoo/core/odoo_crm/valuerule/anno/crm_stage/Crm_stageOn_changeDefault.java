package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_stage;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_stage.Crm_stageOn_changeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_stage
 * 属性：On_change
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_stageOn_changeDefaultValidator.class})
public @interface Crm_stageOn_changeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_journal;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_journal.Account_journalBank_acc_numberDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_journal
 * 属性：Bank_acc_number
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_journalBank_acc_numberDefaultValidator.class})
public @interface Account_journalBank_acc_numberDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

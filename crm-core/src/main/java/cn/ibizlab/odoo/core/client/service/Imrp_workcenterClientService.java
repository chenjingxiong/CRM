package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
public interface Imrp_workcenterClientService{

    public Imrp_workcenter createModel() ;

    public void updateBatch(List<Imrp_workcenter> mrp_workcenters);

    public Page<Imrp_workcenter> fetchDefault(SearchContext context);

    public void createBatch(List<Imrp_workcenter> mrp_workcenters);

    public void create(Imrp_workcenter mrp_workcenter);

    public void removeBatch(List<Imrp_workcenter> mrp_workcenters);

    public void get(Imrp_workcenter mrp_workcenter);

    public void remove(Imrp_workcenter mrp_workcenter);

    public void update(Imrp_workcenter mrp_workcenter);

    public Page<Imrp_workcenter> select(SearchContext context);

    public void getDraft(Imrp_workcenter mrp_workcenter);

}

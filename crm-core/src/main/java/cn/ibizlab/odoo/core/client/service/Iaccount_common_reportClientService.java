package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_common_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_common_report] 服务对象接口
 */
public interface Iaccount_common_reportClientService{

    public Iaccount_common_report createModel() ;

    public void get(Iaccount_common_report account_common_report);

    public void updateBatch(List<Iaccount_common_report> account_common_reports);

    public void create(Iaccount_common_report account_common_report);

    public void createBatch(List<Iaccount_common_report> account_common_reports);

    public void removeBatch(List<Iaccount_common_report> account_common_reports);

    public void remove(Iaccount_common_report account_common_report);

    public Page<Iaccount_common_report> fetchDefault(SearchContext context);

    public void update(Iaccount_common_report account_common_report);

    public Page<Iaccount_common_report> select(SearchContext context);

    public void getDraft(Iaccount_common_report account_common_report);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
public interface Imrp_workcenter_productivity_loss_typeClientService{

    public Imrp_workcenter_productivity_loss_type createModel() ;

    public void createBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);

    public void create(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    public void removeBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);

    public void updateBatch(List<Imrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);

    public void get(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    public void update(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    public Page<Imrp_workcenter_productivity_loss_type> fetchDefault(SearchContext context);

    public void remove(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    public Page<Imrp_workcenter_productivity_loss_type> select(SearchContext context);

    public void getDraft(Imrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

}

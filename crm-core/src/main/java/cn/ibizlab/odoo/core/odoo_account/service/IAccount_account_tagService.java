package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;


/**
 * 实体[Account_account_tag] 服务对象接口
 */
public interface IAccount_account_tagService{

    boolean update(Account_account_tag et) ;
    void updateBatch(List<Account_account_tag> list) ;
    boolean create(Account_account_tag et) ;
    void createBatch(List<Account_account_tag> list) ;
    Account_account_tag get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_account_tag getDraft(Account_account_tag et) ;
    Page<Account_account_tag> searchDefault(Account_account_tagSearchContext context) ;

}




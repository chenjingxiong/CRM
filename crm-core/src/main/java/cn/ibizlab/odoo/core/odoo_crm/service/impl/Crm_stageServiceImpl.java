package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_stageFeignClient;

/**
 * 实体[CRM 阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_stageServiceImpl implements ICrm_stageService {

    @Autowired
    crm_stageFeignClient crm_stageFeignClient;


    @Override
    public boolean create(Crm_stage et) {
        Crm_stage rt = crm_stageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_stage> list){
        crm_stageFeignClient.createBatch(list) ;
    }

    @Override
    public Crm_stage getDraft(Crm_stage et) {
        et=crm_stageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_stageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_stageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Crm_stage et) {
        Crm_stage rt = crm_stageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_stage> list){
        crm_stageFeignClient.updateBatch(list) ;
    }

    @Override
    public Crm_stage get(Integer id) {
		Crm_stage et=crm_stageFeignClient.get(id);
        if(et==null){
            et=new Crm_stage();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_stage> searchDefault(Crm_stageSearchContext context) {
        Page<Crm_stage> crm_stages=crm_stageFeignClient.searchDefault(context);
        return crm_stages;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_page] 服务对象接口
 */
public interface Iwebsite_pageClientService{

    public Iwebsite_page createModel() ;

    public Page<Iwebsite_page> fetchDefault(SearchContext context);

    public void updateBatch(List<Iwebsite_page> website_pages);

    public void update(Iwebsite_page website_page);

    public void get(Iwebsite_page website_page);

    public void create(Iwebsite_page website_page);

    public void createBatch(List<Iwebsite_page> website_pages);

    public void removeBatch(List<Iwebsite_page> website_pages);

    public void remove(Iwebsite_page website_page);

    public Page<Iwebsite_page> select(SearchContext context);

    public void getDraft(Iwebsite_page website_page);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_equipment] 服务对象接口
 */
public interface Imaintenance_equipmentClientService{

    public Imaintenance_equipment createModel() ;

    public void remove(Imaintenance_equipment maintenance_equipment);

    public void removeBatch(List<Imaintenance_equipment> maintenance_equipments);

    public Page<Imaintenance_equipment> fetchDefault(SearchContext context);

    public void create(Imaintenance_equipment maintenance_equipment);

    public void updateBatch(List<Imaintenance_equipment> maintenance_equipments);

    public void update(Imaintenance_equipment maintenance_equipment);

    public void get(Imaintenance_equipment maintenance_equipment);

    public void createBatch(List<Imaintenance_equipment> maintenance_equipments);

    public Page<Imaintenance_equipment> select(SearchContext context);

    public void getDraft(Imaintenance_equipment maintenance_equipment);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_category] 服务对象接口
 */
public interface Iproduct_categoryClientService{

    public Iproduct_category createModel() ;

    public void update(Iproduct_category product_category);

    public void removeBatch(List<Iproduct_category> product_categories);

    public void create(Iproduct_category product_category);

    public void createBatch(List<Iproduct_category> product_categories);

    public void remove(Iproduct_category product_category);

    public void get(Iproduct_category product_category);

    public Page<Iproduct_category> fetchDefault(SearchContext context);

    public void updateBatch(List<Iproduct_category> product_categories);

    public Page<Iproduct_category> select(SearchContext context);

    public void save(Iproduct_category product_category);

    public void getDraft(Iproduct_category product_category);

    public void checkKey(Iproduct_category product_category);

}

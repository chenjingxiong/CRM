package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [website_page] 对象
 */
public interface Iwebsite_page {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [视图结构]
     */
    public void setArch(String arch);
    
    /**
     * 设置 [视图结构]
     */
    public String getArch();

    /**
     * 获取 [视图结构]脏标记
     */
    public boolean getArchDirtyFlag();
    /**
     * 获取 [基础视图结构]
     */
    public void setArch_base(String arch_base);
    
    /**
     * 设置 [基础视图结构]
     */
    public String getArch_base();

    /**
     * 获取 [基础视图结构]脏标记
     */
    public boolean getArch_baseDirtyFlag();
    /**
     * 获取 [Arch Blob]
     */
    public void setArch_db(String arch_db);
    
    /**
     * 设置 [Arch Blob]
     */
    public String getArch_db();

    /**
     * 获取 [Arch Blob]脏标记
     */
    public boolean getArch_dbDirtyFlag();
    /**
     * 获取 [Arch 文件名]
     */
    public void setArch_fs(String arch_fs);
    
    /**
     * 设置 [Arch 文件名]
     */
    public String getArch_fs();

    /**
     * 获取 [Arch 文件名]脏标记
     */
    public boolean getArch_fsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [作为可选继承显示]
     */
    public void setCustomize_show(String customize_show);
    
    /**
     * 设置 [作为可选继承显示]
     */
    public String getCustomize_show();

    /**
     * 获取 [作为可选继承显示]脏标记
     */
    public boolean getCustomize_showDirtyFlag();
    /**
     * 获取 [发布日期]
     */
    public void setDate_publish(Timestamp date_publish);
    
    /**
     * 设置 [发布日期]
     */
    public Timestamp getDate_publish();

    /**
     * 获取 [发布日期]脏标记
     */
    public boolean getDate_publishDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [下级字段]
     */
    public void setField_parent(String field_parent);
    
    /**
     * 设置 [下级字段]
     */
    public String getField_parent();

    /**
     * 获取 [下级字段]脏标记
     */
    public boolean getField_parentDirtyFlag();
    /**
     * 获取 [网站页面]
     */
    public void setFirst_page_id(Integer first_page_id);
    
    /**
     * 设置 [网站页面]
     */
    public Integer getFirst_page_id();

    /**
     * 获取 [网站页面]脏标记
     */
    public boolean getFirst_page_idDirtyFlag();
    /**
     * 获取 [群组]
     */
    public void setGroups_id(String groups_id);
    
    /**
     * 设置 [群组]
     */
    public String getGroups_id();

    /**
     * 获取 [群组]脏标记
     */
    public boolean getGroups_idDirtyFlag();
    /**
     * 获取 [标题颜色]
     */
    public void setHeader_color(String header_color);
    
    /**
     * 设置 [标题颜色]
     */
    public String getHeader_color();

    /**
     * 获取 [标题颜色]脏标记
     */
    public boolean getHeader_colorDirtyFlag();
    /**
     * 获取 [标题覆盖层]
     */
    public void setHeader_overlay(String header_overlay);
    
    /**
     * 设置 [标题覆盖层]
     */
    public String getHeader_overlay();

    /**
     * 获取 [标题覆盖层]脏标记
     */
    public boolean getHeader_overlayDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [继承于此的视图]
     */
    public void setInherit_children_ids(String inherit_children_ids);
    
    /**
     * 设置 [继承于此的视图]
     */
    public String getInherit_children_ids();

    /**
     * 获取 [继承于此的视图]脏标记
     */
    public boolean getInherit_children_idsDirtyFlag();
    /**
     * 获取 [继承的视图]
     */
    public void setInherit_id(Integer inherit_id);
    
    /**
     * 设置 [继承的视图]
     */
    public Integer getInherit_id();

    /**
     * 获取 [继承的视图]脏标记
     */
    public boolean getInherit_idDirtyFlag();
    /**
     * 获取 [主页]
     */
    public void setIs_homepage(String is_homepage);
    
    /**
     * 设置 [主页]
     */
    public String getIs_homepage();

    /**
     * 获取 [主页]脏标记
     */
    public boolean getIs_homepageDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [SEO优化]
     */
    public void setIs_seo_optimized(String is_seo_optimized);
    
    /**
     * 设置 [SEO优化]
     */
    public String getIs_seo_optimized();

    /**
     * 获取 [SEO优化]脏标记
     */
    public boolean getIs_seo_optimizedDirtyFlag();
    /**
     * 获取 [可见]
     */
    public void setIs_visible(String is_visible);
    
    /**
     * 设置 [可见]
     */
    public String getIs_visible();

    /**
     * 获取 [可见]脏标记
     */
    public boolean getIs_visibleDirtyFlag();
    /**
     * 获取 [键]
     */
    public void setKey(String key);
    
    /**
     * 设置 [键]
     */
    public String getKey();

    /**
     * 获取 [键]脏标记
     */
    public boolean getKeyDirtyFlag();
    /**
     * 获取 [相关菜单]
     */
    public void setMenu_ids(String menu_ids);
    
    /**
     * 设置 [相关菜单]
     */
    public String getMenu_ids();

    /**
     * 获取 [相关菜单]脏标记
     */
    public boolean getMenu_idsDirtyFlag();
    /**
     * 获取 [视图继承模式]
     */
    public void setMode(String mode);
    
    /**
     * 设置 [视图继承模式]
     */
    public String getMode();

    /**
     * 获取 [视图继承模式]脏标记
     */
    public boolean getModeDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [模型]
     */
    public String getModel();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [模型数据]
     */
    public void setModel_data_id(Integer model_data_id);
    
    /**
     * 设置 [模型数据]
     */
    public Integer getModel_data_id();

    /**
     * 获取 [模型数据]脏标记
     */
    public boolean getModel_data_idDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel_ids(String model_ids);
    
    /**
     * 设置 [模型]
     */
    public String getModel_ids();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModel_idsDirtyFlag();
    /**
     * 获取 [视图名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [视图名称]
     */
    public String getName();

    /**
     * 获取 [视图名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [页]
     */
    public void setPage_ids(String page_ids);
    
    /**
     * 设置 [页]
     */
    public String getPage_ids();

    /**
     * 获取 [页]脏标记
     */
    public boolean getPage_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setPriority(Integer priority);
    
    /**
     * 设置 [序号]
     */
    public Integer getPriority();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [主题模板]
     */
    public void setTheme_template_id(Integer theme_template_id);
    
    /**
     * 设置 [主题模板]
     */
    public Integer getTheme_template_id();

    /**
     * 获取 [主题模板]脏标记
     */
    public boolean getTheme_template_idDirtyFlag();
    /**
     * 获取 [视图类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [视图类型]
     */
    public String getType();

    /**
     * 获取 [视图类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [页面 URL]
     */
    public void setUrl(String url);
    
    /**
     * 设置 [页面 URL]
     */
    public String getUrl();

    /**
     * 获取 [页面 URL]脏标记
     */
    public boolean getUrlDirtyFlag();
    /**
     * 获取 [视图]
     */
    public void setView_id(Integer view_id);
    
    /**
     * 设置 [视图]
     */
    public Integer getView_id();

    /**
     * 获取 [视图]脏标记
     */
    public boolean getView_idDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [页面已索引]
     */
    public void setWebsite_indexed(String website_indexed);
    
    /**
     * 设置 [页面已索引]
     */
    public String getWebsite_indexed();

    /**
     * 获取 [页面已索引]脏标记
     */
    public boolean getWebsite_indexedDirtyFlag();
    /**
     * 获取 [网站元说明]
     */
    public void setWebsite_meta_description(String website_meta_description);
    
    /**
     * 设置 [网站元说明]
     */
    public String getWebsite_meta_description();

    /**
     * 获取 [网站元说明]脏标记
     */
    public boolean getWebsite_meta_descriptionDirtyFlag();
    /**
     * 获取 [网站meta关键词]
     */
    public void setWebsite_meta_keywords(String website_meta_keywords);
    
    /**
     * 设置 [网站meta关键词]
     */
    public String getWebsite_meta_keywords();

    /**
     * 获取 [网站meta关键词]脏标记
     */
    public boolean getWebsite_meta_keywordsDirtyFlag();
    /**
     * 获取 [网站opengraph图像]
     */
    public void setWebsite_meta_og_img(String website_meta_og_img);
    
    /**
     * 设置 [网站opengraph图像]
     */
    public String getWebsite_meta_og_img();

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    public boolean getWebsite_meta_og_imgDirtyFlag();
    /**
     * 获取 [网站meta标题]
     */
    public void setWebsite_meta_title(String website_meta_title);
    
    /**
     * 设置 [网站meta标题]
     */
    public String getWebsite_meta_title();

    /**
     * 获取 [网站meta标题]脏标记
     */
    public boolean getWebsite_meta_titleDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [外部 ID]
     */
    public void setXml_id(String xml_id);
    
    /**
     * 设置 [外部 ID]
     */
    public String getXml_id();

    /**
     * 获取 [外部 ID]脏标记
     */
    public boolean getXml_idDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel_rule;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
public interface Iim_livechat_channel_ruleClientService{

    public Iim_livechat_channel_rule createModel() ;

    public void createBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules);

    public Page<Iim_livechat_channel_rule> fetchDefault(SearchContext context);

    public void removeBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules);

    public void get(Iim_livechat_channel_rule im_livechat_channel_rule);

    public void update(Iim_livechat_channel_rule im_livechat_channel_rule);

    public void create(Iim_livechat_channel_rule im_livechat_channel_rule);

    public void remove(Iim_livechat_channel_rule im_livechat_channel_rule);

    public void updateBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules);

    public Page<Iim_livechat_channel_rule> select(SearchContext context);

    public void getDraft(Iim_livechat_channel_rule im_livechat_channel_rule);

}

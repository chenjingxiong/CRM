package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [lunch_alert] 对象
 */
public interface Ilunch_alert {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [重新提起]
     */
    public void setAlert_type(String alert_type);
    
    /**
     * 设置 [重新提起]
     */
    public String getAlert_type();

    /**
     * 获取 [重新提起]脏标记
     */
    public boolean getAlert_typeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示]
     */
    public void setDisplay(String display);
    
    /**
     * 设置 [显示]
     */
    public String getDisplay();

    /**
     * 获取 [显示]脏标记
     */
    public boolean getDisplayDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [和]
     */
    public void setEnd_hour(Double end_hour);
    
    /**
     * 设置 [和]
     */
    public Double getEnd_hour();

    /**
     * 获取 [和]脏标记
     */
    public boolean getEnd_hourDirtyFlag();
    /**
     * 获取 [周五]
     */
    public void setFriday(String friday);
    
    /**
     * 设置 [周五]
     */
    public String getFriday();

    /**
     * 获取 [周五]脏标记
     */
    public boolean getFridayDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage(String message);
    
    /**
     * 设置 [消息]
     */
    public String getMessage();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessageDirtyFlag();
    /**
     * 获取 [周一]
     */
    public void setMonday(String monday);
    
    /**
     * 设置 [周一]
     */
    public String getMonday();

    /**
     * 获取 [周一]脏标记
     */
    public boolean getMondayDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getPartner_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getPartner_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [周六]
     */
    public void setSaturday(String saturday);
    
    /**
     * 设置 [周六]
     */
    public String getSaturday();

    /**
     * 获取 [周六]脏标记
     */
    public boolean getSaturdayDirtyFlag();
    /**
     * 获取 [日]
     */
    public void setSpecific_day(Timestamp specific_day);
    
    /**
     * 设置 [日]
     */
    public Timestamp getSpecific_day();

    /**
     * 获取 [日]脏标记
     */
    public boolean getSpecific_dayDirtyFlag();
    /**
     * 获取 [介于]
     */
    public void setStart_hour(Double start_hour);
    
    /**
     * 设置 [介于]
     */
    public Double getStart_hour();

    /**
     * 获取 [介于]脏标记
     */
    public boolean getStart_hourDirtyFlag();
    /**
     * 获取 [周日]
     */
    public void setSunday(String sunday);
    
    /**
     * 设置 [周日]
     */
    public String getSunday();

    /**
     * 获取 [周日]脏标记
     */
    public boolean getSundayDirtyFlag();
    /**
     * 获取 [周四]
     */
    public void setThursday(String thursday);
    
    /**
     * 设置 [周四]
     */
    public String getThursday();

    /**
     * 获取 [周四]脏标记
     */
    public boolean getThursdayDirtyFlag();
    /**
     * 获取 [周二]
     */
    public void setTuesday(String tuesday);
    
    /**
     * 设置 [周二]
     */
    public String getTuesday();

    /**
     * 获取 [周二]脏标记
     */
    public boolean getTuesdayDirtyFlag();
    /**
     * 获取 [周三]
     */
    public void setWednesday(String wednesday);
    
    /**
     * 设置 [周三]
     */
    public String getWednesday();

    /**
     * 获取 [周三]脏标记
     */
    public boolean getWednesdayDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

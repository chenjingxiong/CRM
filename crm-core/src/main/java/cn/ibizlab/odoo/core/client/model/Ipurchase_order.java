package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [purchase_order] 对象
 */
public interface Ipurchase_order {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问网址]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问网址]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问网址]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setAmount_tax(Double amount_tax);
    
    /**
     * 设置 [税率]
     */
    public Double getAmount_tax();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getAmount_taxDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [总计]
     */
    public Double getAmount_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [未税金额]
     */
    public void setAmount_untaxed(Double amount_untaxed);
    
    /**
     * 设置 [未税金额]
     */
    public Double getAmount_untaxed();

    /**
     * 获取 [未税金额]脏标记
     */
    public boolean getAmount_untaxedDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [审批日期]
     */
    public void setDate_approve(Timestamp date_approve);
    
    /**
     * 设置 [审批日期]
     */
    public Timestamp getDate_approve();

    /**
     * 获取 [审批日期]脏标记
     */
    public boolean getDate_approveDirtyFlag();
    /**
     * 获取 [单据日期]
     */
    public void setDate_order(Timestamp date_order);
    
    /**
     * 设置 [单据日期]
     */
    public Timestamp getDate_order();

    /**
     * 获取 [单据日期]脏标记
     */
    public boolean getDate_orderDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setDate_planned(Timestamp date_planned);
    
    /**
     * 设置 [计划日期]
     */
    public Timestamp getDate_planned();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getDate_plannedDirtyFlag();
    /**
     * 获取 [目标位置类型]
     */
    public void setDefault_location_dest_id_usage(String default_location_dest_id_usage);
    
    /**
     * 设置 [目标位置类型]
     */
    public String getDefault_location_dest_id_usage();

    /**
     * 获取 [目标位置类型]脏标记
     */
    public boolean getDefault_location_dest_id_usageDirtyFlag();
    /**
     * 获取 [代发货地址]
     */
    public void setDest_address_id(Integer dest_address_id);
    
    /**
     * 设置 [代发货地址]
     */
    public Integer getDest_address_id();

    /**
     * 获取 [代发货地址]脏标记
     */
    public boolean getDest_address_idDirtyFlag();
    /**
     * 获取 [代发货地址]
     */
    public void setDest_address_id_text(String dest_address_id_text);
    
    /**
     * 设置 [代发货地址]
     */
    public String getDest_address_id_text();

    /**
     * 获取 [代发货地址]脏标记
     */
    public boolean getDest_address_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id(Integer fiscal_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getFiscal_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id_text(String fiscal_position_id_text);
    
    /**
     * 设置 [税科目调整]
     */
    public String getFiscal_position_id_text();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_id_textDirtyFlag();
    /**
     * 获取 [补货组]
     */
    public void setGroup_id(Integer group_id);
    
    /**
     * 设置 [补货组]
     */
    public Integer getGroup_id();

    /**
     * 获取 [补货组]脏标记
     */
    public boolean getGroup_idDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [国际贸易术语]
     */
    public void setIncoterm_id(Integer incoterm_id);
    
    /**
     * 设置 [国际贸易术语]
     */
    public Integer getIncoterm_id();

    /**
     * 获取 [国际贸易术语]脏标记
     */
    public boolean getIncoterm_idDirtyFlag();
    /**
     * 获取 [国际贸易术语]
     */
    public void setIncoterm_id_text(String incoterm_id_text);
    
    /**
     * 设置 [国际贸易术语]
     */
    public String getIncoterm_id_text();

    /**
     * 获取 [国际贸易术语]脏标记
     */
    public boolean getIncoterm_id_textDirtyFlag();
    /**
     * 获取 [账单数量]
     */
    public void setInvoice_count(Integer invoice_count);
    
    /**
     * 设置 [账单数量]
     */
    public Integer getInvoice_count();

    /**
     * 获取 [账单数量]脏标记
     */
    public boolean getInvoice_countDirtyFlag();
    /**
     * 获取 [账单]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [账单]
     */
    public String getInvoice_ids();

    /**
     * 获取 [账单]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [账单状态]
     */
    public void setInvoice_status(String invoice_status);
    
    /**
     * 设置 [账单状态]
     */
    public String getInvoice_status();

    /**
     * 获取 [账单状态]脏标记
     */
    public boolean getInvoice_statusDirtyFlag();
    /**
     * 获取 [是否要运送]
     */
    public void setIs_shipped(String is_shipped);
    
    /**
     * 设置 [是否要运送]
     */
    public String getIs_shipped();

    /**
     * 获取 [是否要运送]脏标记
     */
    public boolean getIs_shippedDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息传输错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息传输错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息传输错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [前置操作]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [前置操作]
     */
    public String getMessage_needaction();

    /**
     * 获取 [前置操作]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [动作编号]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [动作编号]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [动作编号]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setName(String name);
    
    /**
     * 设置 [订单关联]
     */
    public String getName();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [条款和条件]
     */
    public void setNotes(String notes);
    
    /**
     * 设置 [条款和条件]
     */
    public String getNotes();

    /**
     * 获取 [条款和条件]脏标记
     */
    public boolean getNotesDirtyFlag();
    /**
     * 获取 [订单行]
     */
    public void setOrder_line(String order_line);
    
    /**
     * 设置 [订单行]
     */
    public String getOrder_line();

    /**
     * 获取 [订单行]脏标记
     */
    public boolean getOrder_lineDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getPartner_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getPartner_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [供应商参考]
     */
    public void setPartner_ref(String partner_ref);
    
    /**
     * 设置 [供应商参考]
     */
    public String getPartner_ref();

    /**
     * 获取 [供应商参考]脏标记
     */
    public boolean getPartner_refDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id(Integer payment_term_id);
    
    /**
     * 设置 [付款条款]
     */
    public Integer getPayment_term_id();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_idDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id_text(String payment_term_id_text);
    
    /**
     * 设置 [付款条款]
     */
    public String getPayment_term_id_text();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_id_textDirtyFlag();
    /**
     * 获取 [拣货数]
     */
    public void setPicking_count(Integer picking_count);
    
    /**
     * 设置 [拣货数]
     */
    public Integer getPicking_count();

    /**
     * 获取 [拣货数]脏标记
     */
    public boolean getPicking_countDirtyFlag();
    /**
     * 获取 [接收]
     */
    public void setPicking_ids(String picking_ids);
    
    /**
     * 设置 [接收]
     */
    public String getPicking_ids();

    /**
     * 获取 [接收]脏标记
     */
    public boolean getPicking_idsDirtyFlag();
    /**
     * 获取 [交货到]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [交货到]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [交货到]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [交货到]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [交货到]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [交货到]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [采购员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [采购员]
     */
    public Integer getUser_id();

    /**
     * 获取 [采购员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [采购员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [采购员]
     */
    public String getUser_id_text();

    /**
     * 获取 [采购员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

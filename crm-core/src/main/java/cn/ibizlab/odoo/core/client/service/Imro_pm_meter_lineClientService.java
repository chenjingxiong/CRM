package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
public interface Imro_pm_meter_lineClientService{

    public Imro_pm_meter_line createModel() ;

    public void create(Imro_pm_meter_line mro_pm_meter_line);

    public void get(Imro_pm_meter_line mro_pm_meter_line);

    public void removeBatch(List<Imro_pm_meter_line> mro_pm_meter_lines);

    public void update(Imro_pm_meter_line mro_pm_meter_line);

    public Page<Imro_pm_meter_line> fetchDefault(SearchContext context);

    public void updateBatch(List<Imro_pm_meter_line> mro_pm_meter_lines);

    public void remove(Imro_pm_meter_line mro_pm_meter_line);

    public void createBatch(List<Imro_pm_meter_line> mro_pm_meter_lines);

    public Page<Imro_pm_meter_line> select(SearchContext context);

    public void getDraft(Imro_pm_meter_line mro_pm_meter_line);

}

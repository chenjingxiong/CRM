package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;


/**
 * 实体[Survey_mail_compose_message] 服务对象接口
 */
public interface ISurvey_mail_compose_messageService{

    boolean create(Survey_mail_compose_message et) ;
    void createBatch(List<Survey_mail_compose_message> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Survey_mail_compose_message get(Integer key) ;
    boolean update(Survey_mail_compose_message et) ;
    void updateBatch(List<Survey_mail_compose_message> list) ;
    Survey_mail_compose_message getDraft(Survey_mail_compose_message et) ;
    Page<Survey_mail_compose_message> searchDefault(Survey_mail_compose_messageSearchContext context) ;

}




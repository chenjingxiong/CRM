package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_removalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_removalFeignClient;

/**
 * 实体[下架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_removalServiceImpl implements IProduct_removalService {

    @Autowired
    product_removalFeignClient product_removalFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_removalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_removalFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Product_removal et) {
        Product_removal rt = product_removalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_removal> list){
        product_removalFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Product_removal et) {
        Product_removal rt = product_removalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_removal> list){
        product_removalFeignClient.createBatch(list) ;
    }

    @Override
    public Product_removal getDraft(Product_removal et) {
        et=product_removalFeignClient.getDraft();
        return et;
    }

    @Override
    public Product_removal get(Integer id) {
		Product_removal et=product_removalFeignClient.get(id);
        if(et==null){
            et=new Product_removal();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_removal> searchDefault(Product_removalSearchContext context) {
        Page<Product_removal> product_removals=product_removalFeignClient.searchDefault(context);
        return product_removals;
    }


}



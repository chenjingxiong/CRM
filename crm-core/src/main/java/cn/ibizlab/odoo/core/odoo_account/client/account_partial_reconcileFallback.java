package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
@Component
public class account_partial_reconcileFallback implements account_partial_reconcileFeignClient{

    public Account_partial_reconcile get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_partial_reconcile update(Integer id, Account_partial_reconcile account_partial_reconcile){
            return null;
     }
    public Boolean updateBatch(List<Account_partial_reconcile> account_partial_reconciles){
            return false;
     }



    public Account_partial_reconcile create(Account_partial_reconcile account_partial_reconcile){
            return null;
     }
    public Boolean createBatch(List<Account_partial_reconcile> account_partial_reconciles){
            return false;
     }

    public Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context){
            return null;
     }




    public Page<Account_partial_reconcile> select(){
            return null;
     }

    public Account_partial_reconcile getDraft(){
            return null;
    }



}

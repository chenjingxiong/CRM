package cn.ibizlab.odoo.core.odoo_barcodes.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;


/**
 * 实体[Barcodes_barcode_events_mixin] 服务对象接口
 */
public interface IBarcodes_barcode_events_mixinService{

    boolean create(Barcodes_barcode_events_mixin et) ;
    void createBatch(List<Barcodes_barcode_events_mixin> list) ;
    boolean update(Barcodes_barcode_events_mixin et) ;
    void updateBatch(List<Barcodes_barcode_events_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Barcodes_barcode_events_mixin get(Integer key) ;
    Barcodes_barcode_events_mixin getDraft(Barcodes_barcode_events_mixin et) ;
    Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context) ;

}




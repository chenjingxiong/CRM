package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_attendee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_attendee] 服务对象接口
 */
public interface Icalendar_attendeeClientService{

    public Icalendar_attendee createModel() ;

    public void update(Icalendar_attendee calendar_attendee);

    public void createBatch(List<Icalendar_attendee> calendar_attendees);

    public void create(Icalendar_attendee calendar_attendee);

    public void removeBatch(List<Icalendar_attendee> calendar_attendees);

    public void get(Icalendar_attendee calendar_attendee);

    public Page<Icalendar_attendee> fetchDefault(SearchContext context);

    public void updateBatch(List<Icalendar_attendee> calendar_attendees);

    public void remove(Icalendar_attendee calendar_attendee);

    public Page<Icalendar_attendee> select(SearchContext context);

    public void getDraft(Icalendar_attendee calendar_attendee);

}

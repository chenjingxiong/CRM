package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_optionSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_optionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_optionFeignClient;

/**
 * 实体[销售选项] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_optionServiceImpl implements ISale_order_optionService {

    @Autowired
    sale_order_optionFeignClient sale_order_optionFeignClient;


    @Override
    public Sale_order_option getDraft(Sale_order_option et) {
        et=sale_order_optionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sale_order_optionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_order_optionFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_order_option get(Integer id) {
		Sale_order_option et=sale_order_optionFeignClient.get(id);
        if(et==null){
            et=new Sale_order_option();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Sale_order_option et) {
        Sale_order_option rt = sale_order_optionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_option> list){
        sale_order_optionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Sale_order_option et) {
        Sale_order_option rt = sale_order_optionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_order_option> list){
        sale_order_optionFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_option> searchDefault(Sale_order_optionSearchContext context) {
        Page<Sale_order_option> sale_order_options=sale_order_optionFeignClient.searchDefault(context);
        return sale_order_options;
    }


}



package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_seo_metadataService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_seo_metadataFeignClient;

/**
 * 实体[SEO元数据] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_seo_metadataServiceImpl implements IWebsite_seo_metadataService {

    @Autowired
    website_seo_metadataFeignClient website_seo_metadataFeignClient;


    @Override
    public boolean create(Website_seo_metadata et) {
        Website_seo_metadata rt = website_seo_metadataFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_seo_metadata> list){
        website_seo_metadataFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Website_seo_metadata et) {
        Website_seo_metadata rt = website_seo_metadataFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_seo_metadata> list){
        website_seo_metadataFeignClient.updateBatch(list) ;
    }

    @Override
    public Website_seo_metadata get(Integer id) {
		Website_seo_metadata et=website_seo_metadataFeignClient.get(id);
        if(et==null){
            et=new Website_seo_metadata();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Website_seo_metadata getDraft(Website_seo_metadata et) {
        et=website_seo_metadataFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_seo_metadataFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_seo_metadataFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context) {
        Page<Website_seo_metadata> website_seo_metadatas=website_seo_metadataFeignClient.searchDefault(context);
        return website_seo_metadatas;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave_report] 服务对象接口
 */
public interface Ihr_leave_reportClientService{

    public Ihr_leave_report createModel() ;

    public void remove(Ihr_leave_report hr_leave_report);

    public void update(Ihr_leave_report hr_leave_report);

    public void removeBatch(List<Ihr_leave_report> hr_leave_reports);

    public void createBatch(List<Ihr_leave_report> hr_leave_reports);

    public Page<Ihr_leave_report> fetchDefault(SearchContext context);

    public void create(Ihr_leave_report hr_leave_report);

    public void get(Ihr_leave_report hr_leave_report);

    public void updateBatch(List<Ihr_leave_report> hr_leave_reports);

    public Page<Ihr_leave_report> select(SearchContext context);

    public void getDraft(Ihr_leave_report hr_leave_report);

}

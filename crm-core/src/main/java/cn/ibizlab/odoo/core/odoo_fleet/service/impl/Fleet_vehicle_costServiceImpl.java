package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_costService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_costFeignClient;

/**
 * 实体[车辆相关的费用] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_costServiceImpl implements IFleet_vehicle_costService {

    @Autowired
    fleet_vehicle_costFeignClient fleet_vehicle_costFeignClient;


    @Override
    public Fleet_vehicle_cost getDraft(Fleet_vehicle_cost et) {
        et=fleet_vehicle_costFeignClient.getDraft();
        return et;
    }

    @Override
    public Fleet_vehicle_cost get(Integer id) {
		Fleet_vehicle_cost et=fleet_vehicle_costFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_cost();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Fleet_vehicle_cost et) {
        Fleet_vehicle_cost rt = fleet_vehicle_costFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_cost> list){
        fleet_vehicle_costFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_costFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_costFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Fleet_vehicle_cost et) {
        Fleet_vehicle_cost rt = fleet_vehicle_costFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_cost> list){
        fleet_vehicle_costFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context) {
        Page<Fleet_vehicle_cost> fleet_vehicle_costs=fleet_vehicle_costFeignClient.searchDefault(context);
        return fleet_vehicle_costs;
    }


}



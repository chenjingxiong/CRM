package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;


/**
 * 实体[Stock_warehouse] 服务对象接口
 */
public interface IStock_warehouseService{

    Stock_warehouse get(Integer key) ;
    Stock_warehouse getDraft(Stock_warehouse et) ;
    boolean create(Stock_warehouse et) ;
    void createBatch(List<Stock_warehouse> list) ;
    boolean update(Stock_warehouse et) ;
    void updateBatch(List<Stock_warehouse> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context) ;

}




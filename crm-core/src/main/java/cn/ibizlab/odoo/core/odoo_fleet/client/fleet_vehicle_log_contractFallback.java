package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
@Component
public class fleet_vehicle_log_contractFallback implements fleet_vehicle_log_contractFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context){
            return null;
     }


    public Fleet_vehicle_log_contract update(Integer id, Fleet_vehicle_log_contract fleet_vehicle_log_contract){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts){
            return false;
     }


    public Fleet_vehicle_log_contract get(Integer id){
            return null;
     }




    public Fleet_vehicle_log_contract create(Fleet_vehicle_log_contract fleet_vehicle_log_contract){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts){
            return false;
     }


    public Page<Fleet_vehicle_log_contract> select(){
            return null;
     }

    public Fleet_vehicle_log_contract getDraft(){
            return null;
    }



}

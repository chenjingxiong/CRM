package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_pricelist_item] 服务对象接口
 */
public interface Iproduct_pricelist_itemClientService{

    public Iproduct_pricelist_item createModel() ;

    public void removeBatch(List<Iproduct_pricelist_item> product_pricelist_items);

    public void remove(Iproduct_pricelist_item product_pricelist_item);

    public void update(Iproduct_pricelist_item product_pricelist_item);

    public void get(Iproduct_pricelist_item product_pricelist_item);

    public void create(Iproduct_pricelist_item product_pricelist_item);

    public void updateBatch(List<Iproduct_pricelist_item> product_pricelist_items);

    public Page<Iproduct_pricelist_item> fetchDefault(SearchContext context);

    public void createBatch(List<Iproduct_pricelist_item> product_pricelist_items);

    public Page<Iproduct_pricelist_item> select(SearchContext context);

    public void getDraft(Iproduct_pricelist_item product_pricelist_item);

    public void checkKey(Iproduct_pricelist_item product_pricelist_item);

    public void save(Iproduct_pricelist_item product_pricelist_item);

}

package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_expense] 服务对象接口
 */
@Component
public class hr_expenseFallback implements hr_expenseFeignClient{


    public Hr_expense create(Hr_expense hr_expense){
            return null;
     }
    public Boolean createBatch(List<Hr_expense> hr_expenses){
            return false;
     }

    public Hr_expense get(Integer id){
            return null;
     }


    public Page<Hr_expense> searchDefault(Hr_expenseSearchContext context){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Hr_expense update(Integer id, Hr_expense hr_expense){
            return null;
     }
    public Boolean updateBatch(List<Hr_expense> hr_expenses){
            return false;
     }


    public Page<Hr_expense> select(){
            return null;
     }

    public Hr_expense getDraft(){
            return null;
    }



}

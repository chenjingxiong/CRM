package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_activity_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
public interface Imail_activity_mixinClientService{

    public Imail_activity_mixin createModel() ;

    public void create(Imail_activity_mixin mail_activity_mixin);

    public Page<Imail_activity_mixin> fetchDefault(SearchContext context);

    public void get(Imail_activity_mixin mail_activity_mixin);

    public void createBatch(List<Imail_activity_mixin> mail_activity_mixins);

    public void update(Imail_activity_mixin mail_activity_mixin);

    public void updateBatch(List<Imail_activity_mixin> mail_activity_mixins);

    public void remove(Imail_activity_mixin mail_activity_mixin);

    public void removeBatch(List<Imail_activity_mixin> mail_activity_mixins);

    public Page<Imail_activity_mixin> select(SearchContext context);

    public void getDraft(Imail_activity_mixin mail_activity_mixin);

}

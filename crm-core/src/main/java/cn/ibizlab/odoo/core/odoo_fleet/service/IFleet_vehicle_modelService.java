package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;


/**
 * 实体[Fleet_vehicle_model] 服务对象接口
 */
public interface IFleet_vehicle_modelService{

    boolean update(Fleet_vehicle_model et) ;
    void updateBatch(List<Fleet_vehicle_model> list) ;
    Fleet_vehicle_model get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Fleet_vehicle_model et) ;
    void createBatch(List<Fleet_vehicle_model> list) ;
    Fleet_vehicle_model getDraft(Fleet_vehicle_model et) ;
    Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context) ;

}




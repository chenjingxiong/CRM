package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_public_category] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-public-category", fallback = product_public_categoryFallback.class)
public interface product_public_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/{id}")
    Product_public_category get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/product_public_categories")
    Product_public_category create(@RequestBody Product_public_category product_public_category);

    @RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/batch")
    Boolean createBatch(@RequestBody List<Product_public_category> product_public_categories);




    @RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/searchdefault")
    Page<Product_public_category> searchDefault(@RequestBody Product_public_categorySearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/{id}")
    Product_public_category update(@PathVariable("id") Integer id,@RequestBody Product_public_category product_public_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/batch")
    Boolean updateBatch(@RequestBody List<Product_public_category> product_public_categories);



    @RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/select")
    Page<Product_public_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/getdraft")
    Product_public_category getDraft();


}

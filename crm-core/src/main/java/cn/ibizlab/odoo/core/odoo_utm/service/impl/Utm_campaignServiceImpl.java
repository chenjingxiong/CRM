package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_campaignService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_utm.client.utm_campaignFeignClient;

/**
 * 实体[UTM 营销活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_campaignServiceImpl implements IUtm_campaignService {

    @Autowired
    utm_campaignFeignClient utm_campaignFeignClient;


    @Override
    public boolean update(Utm_campaign et) {
        Utm_campaign rt = utm_campaignFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Utm_campaign> list){
        utm_campaignFeignClient.updateBatch(list) ;
    }

    @Override
    public Utm_campaign get(Integer id) {
		Utm_campaign et=utm_campaignFeignClient.get(id);
        if(et==null){
            et=new Utm_campaign();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=utm_campaignFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        utm_campaignFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Utm_campaign et) {
        Utm_campaign rt = utm_campaignFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_campaign> list){
        utm_campaignFeignClient.createBatch(list) ;
    }

    @Override
    public Utm_campaign getDraft(Utm_campaign et) {
        et=utm_campaignFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_campaign> searchDefault(Utm_campaignSearchContext context) {
        Page<Utm_campaign> utm_campaigns=utm_campaignFeignClient.searchDefault(context);
        return utm_campaigns;
    }


}



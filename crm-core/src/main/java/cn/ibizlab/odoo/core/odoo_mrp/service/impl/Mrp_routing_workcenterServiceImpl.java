package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routing_workcenterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_routing_workcenterFeignClient;

/**
 * 实体[工作中心使用情况] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_routing_workcenterServiceImpl implements IMrp_routing_workcenterService {

    @Autowired
    mrp_routing_workcenterFeignClient mrp_routing_workcenterFeignClient;


    @Override
    public Mrp_routing_workcenter getDraft(Mrp_routing_workcenter et) {
        et=mrp_routing_workcenterFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_routing_workcenterFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_routing_workcenterFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mrp_routing_workcenter et) {
        Mrp_routing_workcenter rt = mrp_routing_workcenterFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_routing_workcenter> list){
        mrp_routing_workcenterFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mrp_routing_workcenter et) {
        Mrp_routing_workcenter rt = mrp_routing_workcenterFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_routing_workcenter> list){
        mrp_routing_workcenterFeignClient.updateBatch(list) ;
    }

    @Override
    public Mrp_routing_workcenter get(Integer id) {
		Mrp_routing_workcenter et=mrp_routing_workcenterFeignClient.get(id);
        if(et==null){
            et=new Mrp_routing_workcenter();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_routing_workcenter> searchDefault(Mrp_routing_workcenterSearchContext context) {
        Page<Mrp_routing_workcenter> mrp_routing_workcenters=mrp_routing_workcenterFeignClient.searchDefault(context);
        return mrp_routing_workcenters;
    }


}



package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_portal.client.portal_mixinFeignClient;

/**
 * 实体[门户Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_mixinServiceImpl implements IPortal_mixinService {

    @Autowired
    portal_mixinFeignClient portal_mixinFeignClient;


    @Override
    public Portal_mixin get(Integer id) {
		Portal_mixin et=portal_mixinFeignClient.get(id);
        if(et==null){
            et=new Portal_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=portal_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        portal_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Portal_mixin et) {
        Portal_mixin rt = portal_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Portal_mixin> list){
        portal_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public Portal_mixin getDraft(Portal_mixin et) {
        et=portal_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Portal_mixin et) {
        Portal_mixin rt = portal_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_mixin> list){
        portal_mixinFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context) {
        Page<Portal_mixin> portal_mixins=portal_mixinFeignClient.searchDefault(context);
        return portal_mixins;
    }


}



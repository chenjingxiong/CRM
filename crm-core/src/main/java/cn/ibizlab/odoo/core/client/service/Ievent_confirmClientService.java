package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_confirm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_confirm] 服务对象接口
 */
public interface Ievent_confirmClientService{

    public Ievent_confirm createModel() ;

    public void create(Ievent_confirm event_confirm);

    public void update(Ievent_confirm event_confirm);

    public Page<Ievent_confirm> fetchDefault(SearchContext context);

    public void removeBatch(List<Ievent_confirm> event_confirms);

    public void updateBatch(List<Ievent_confirm> event_confirms);

    public void get(Ievent_confirm event_confirm);

    public void remove(Ievent_confirm event_confirm);

    public void createBatch(List<Ievent_confirm> event_confirms);

    public Page<Ievent_confirm> select(SearchContext context);

    public void getDraft(Ievent_confirm event_confirm);

}

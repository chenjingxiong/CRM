package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_paymentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_paymentFeignClient;

/**
 * 实体[付款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_paymentServiceImpl implements IAccount_paymentService {

    @Autowired
    account_paymentFeignClient account_paymentFeignClient;


    @Override
    public boolean checkKey(Account_payment et) {
        return account_paymentFeignClient.checkKey(et);
    }
    @Override
    public boolean update(Account_payment et) {
        Account_payment rt = account_paymentFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_payment> list){
        account_paymentFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_payment getDraft(Account_payment et) {
        et=account_paymentFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_paymentFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_paymentFeignClient.removeBatch(idList);
    }

    @Override
    @Transactional
    public boolean save(Account_payment et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!account_paymentFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Account_payment> list) {
        account_paymentFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean create(Account_payment et) {
        Account_payment rt = account_paymentFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment> list){
        account_paymentFeignClient.createBatch(list) ;
    }

    @Override
    public Account_payment get(Integer id) {
		Account_payment et=account_paymentFeignClient.get(id);
        if(et==null){
            et=new Account_payment();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment> searchDefault(Account_paymentSearchContext context) {
        Page<Account_payment> account_payments=account_paymentFeignClient.searchDefault(context);
        return account_payments;
    }


}



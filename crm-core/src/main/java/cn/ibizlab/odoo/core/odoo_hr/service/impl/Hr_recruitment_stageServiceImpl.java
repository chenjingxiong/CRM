package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_recruitment_stageFeignClient;

/**
 * 实体[招聘阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_recruitment_stageServiceImpl implements IHr_recruitment_stageService {

    @Autowired
    hr_recruitment_stageFeignClient hr_recruitment_stageFeignClient;


    @Override
    public Hr_recruitment_stage get(Integer id) {
		Hr_recruitment_stage et=hr_recruitment_stageFeignClient.get(id);
        if(et==null){
            et=new Hr_recruitment_stage();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_recruitment_stage et) {
        Hr_recruitment_stage rt = hr_recruitment_stageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_recruitment_stage> list){
        hr_recruitment_stageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Hr_recruitment_stage et) {
        Hr_recruitment_stage rt = hr_recruitment_stageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_recruitment_stage> list){
        hr_recruitment_stageFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_recruitment_stage getDraft(Hr_recruitment_stage et) {
        et=hr_recruitment_stageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_recruitment_stageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_recruitment_stageFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context) {
        Page<Hr_recruitment_stage> hr_recruitment_stages=hr_recruitment_stageFeignClient.searchDefault(context);
        return hr_recruitment_stages;
    }


}



package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_packaging] 服务对象接口
 */
@Component
public class product_packagingFallback implements product_packagingFeignClient{



    public Product_packaging get(Integer id){
            return null;
     }



    public Product_packaging create(Product_packaging product_packaging){
            return null;
     }
    public Boolean createBatch(List<Product_packaging> product_packagings){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Product_packaging update(Integer id, Product_packaging product_packaging){
            return null;
     }
    public Boolean updateBatch(List<Product_packaging> product_packagings){
            return false;
     }


    public Page<Product_packaging> searchDefault(Product_packagingSearchContext context){
            return null;
     }


    public Page<Product_packaging> select(){
            return null;
     }

    public Product_packaging getDraft(){
            return null;
    }



}

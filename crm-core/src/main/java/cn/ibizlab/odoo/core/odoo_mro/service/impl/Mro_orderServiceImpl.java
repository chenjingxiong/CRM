package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_orderFeignClient;

/**
 * 实体[Maintenance Order] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_orderServiceImpl implements IMro_orderService {

    @Autowired
    mro_orderFeignClient mro_orderFeignClient;


    @Override
    public Mro_order getDraft(Mro_order et) {
        et=mro_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mro_order et) {
        Mro_order rt = mro_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_order> list){
        mro_orderFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_orderFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_order get(Integer id) {
		Mro_order et=mro_orderFeignClient.get(id);
        if(et==null){
            et=new Mro_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mro_order et) {
        Mro_order rt = mro_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_order> list){
        mro_orderFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_order> searchDefault(Mro_orderSearchContext context) {
        Page<Mro_order> mro_orders=mro_orderFeignClient.searchDefault(context);
        return mro_orders;
    }


}



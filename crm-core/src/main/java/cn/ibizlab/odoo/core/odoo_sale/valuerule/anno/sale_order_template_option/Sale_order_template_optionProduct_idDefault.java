package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order_template_option;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_order_template_option.Sale_order_template_optionProduct_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_order_template_option
 * 属性：Product_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_order_template_optionProduct_idDefaultValidator.class})
public @interface Sale_order_template_optionProduct_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproject_task_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_task_type] 服务对象接口
 */
public interface Iproject_task_typeClientService{

    public Iproject_task_type createModel() ;

    public void createBatch(List<Iproject_task_type> project_task_types);

    public void get(Iproject_task_type project_task_type);

    public void update(Iproject_task_type project_task_type);

    public Page<Iproject_task_type> fetchDefault(SearchContext context);

    public void removeBatch(List<Iproject_task_type> project_task_types);

    public void create(Iproject_task_type project_task_type);

    public void remove(Iproject_task_type project_task_type);

    public void updateBatch(List<Iproject_task_type> project_task_types);

    public Page<Iproject_task_type> select(SearchContext context);

    public void getDraft(Iproject_task_type project_task_type);

}

package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;


/**
 * 实体[Stock_move_line] 服务对象接口
 */
public interface IStock_move_lineService{

    Stock_move_line get(Integer key) ;
    boolean create(Stock_move_line et) ;
    void createBatch(List<Stock_move_line> list) ;
    boolean update(Stock_move_line et) ;
    void updateBatch(List<Stock_move_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_move_line getDraft(Stock_move_line et) ;
    Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context) ;

}




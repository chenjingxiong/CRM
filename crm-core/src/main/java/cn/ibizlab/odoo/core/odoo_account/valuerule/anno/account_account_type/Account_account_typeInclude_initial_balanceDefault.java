package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_account_type;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_account_type.Account_account_typeInclude_initial_balanceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_account_type
 * 属性：Include_initial_balance
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_account_typeInclude_initial_balanceDefaultValidator.class})
public @interface Account_account_typeInclude_initial_balanceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

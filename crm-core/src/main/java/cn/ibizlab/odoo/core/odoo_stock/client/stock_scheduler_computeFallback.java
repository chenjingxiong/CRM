package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
@Component
public class stock_scheduler_computeFallback implements stock_scheduler_computeFeignClient{

    public Page<Stock_scheduler_compute> searchDefault(Stock_scheduler_computeSearchContext context){
            return null;
     }





    public Stock_scheduler_compute create(Stock_scheduler_compute stock_scheduler_compute){
            return null;
     }
    public Boolean createBatch(List<Stock_scheduler_compute> stock_scheduler_computes){
            return false;
     }

    public Stock_scheduler_compute get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_scheduler_compute update(Integer id, Stock_scheduler_compute stock_scheduler_compute){
            return null;
     }
    public Boolean updateBatch(List<Stock_scheduler_compute> stock_scheduler_computes){
            return false;
     }


    public Page<Stock_scheduler_compute> select(){
            return null;
     }

    public Stock_scheduler_compute getDraft(){
            return null;
    }



}

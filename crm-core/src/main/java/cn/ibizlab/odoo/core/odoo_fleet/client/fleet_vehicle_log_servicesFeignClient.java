package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
@FeignClient(value = "odoo-fleet", contextId = "fleet-vehicle-log-services", fallback = fleet_vehicle_log_servicesFallback.class)
public interface fleet_vehicle_log_servicesFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services")
    Fleet_vehicle_log_services create(@RequestBody Fleet_vehicle_log_services fleet_vehicle_log_services);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_log_services> fleet_vehicle_log_services);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/{id}")
    Fleet_vehicle_log_services get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/searchdefault")
    Page<Fleet_vehicle_log_services> searchDefault(@RequestBody Fleet_vehicle_log_servicesSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/{id}")
    Fleet_vehicle_log_services update(@PathVariable("id") Integer id,@RequestBody Fleet_vehicle_log_services fleet_vehicle_log_services);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_log_services> fleet_vehicle_log_services);



    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/select")
    Page<Fleet_vehicle_log_services> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/getdraft")
    Fleet_vehicle_log_services getDraft();


}

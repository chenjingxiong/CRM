package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproject_task;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_task] 服务对象接口
 */
public interface Iproject_taskClientService{

    public Iproject_task createModel() ;

    public void updateBatch(List<Iproject_task> project_tasks);

    public void createBatch(List<Iproject_task> project_tasks);

    public void create(Iproject_task project_task);

    public void remove(Iproject_task project_task);

    public void get(Iproject_task project_task);

    public Page<Iproject_task> fetchDefault(SearchContext context);

    public void update(Iproject_task project_task);

    public void removeBatch(List<Iproject_task> project_tasks);

    public Page<Iproject_task> select(SearchContext context);

    public void getDraft(Iproject_task project_task);

}

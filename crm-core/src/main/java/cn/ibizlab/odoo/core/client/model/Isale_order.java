package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_order] 对象
 */
public interface Isale_order {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问网址]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问网址]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问网址]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [按组分配税额]
     */
    public void setAmount_by_group(byte[] amount_by_group);
    
    /**
     * 设置 [按组分配税额]
     */
    public byte[] getAmount_by_group();

    /**
     * 获取 [按组分配税额]脏标记
     */
    public boolean getAmount_by_groupDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setAmount_tax(Double amount_tax);
    
    /**
     * 设置 [税率]
     */
    public Double getAmount_tax();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getAmount_taxDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [总计]
     */
    public Double getAmount_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [折扣前金额]
     */
    public void setAmount_undiscounted(Double amount_undiscounted);
    
    /**
     * 设置 [折扣前金额]
     */
    public Double getAmount_undiscounted();

    /**
     * 获取 [折扣前金额]脏标记
     */
    public boolean getAmount_undiscountedDirtyFlag();
    /**
     * 获取 [未税金额]
     */
    public void setAmount_untaxed(Double amount_untaxed);
    
    /**
     * 设置 [未税金额]
     */
    public Double getAmount_untaxed();

    /**
     * 获取 [未税金额]脏标记
     */
    public boolean getAmount_untaxedDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id(Integer analytic_account_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAnalytic_account_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id_text(String analytic_account_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAnalytic_account_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_id_textDirtyFlag();
    /**
     * 获取 [已授权的交易]
     */
    public void setAuthorized_transaction_ids(String authorized_transaction_ids);
    
    /**
     * 设置 [已授权的交易]
     */
    public String getAuthorized_transaction_ids();

    /**
     * 获取 [已授权的交易]脏标记
     */
    public boolean getAuthorized_transaction_idsDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [购物车数量]
     */
    public void setCart_quantity(Integer cart_quantity);
    
    /**
     * 设置 [购物车数量]
     */
    public Integer getCart_quantity();

    /**
     * 获取 [购物车数量]脏标记
     */
    public boolean getCart_quantityDirtyFlag();
    /**
     * 获取 [购物车恢复EMail已发送]
     */
    public void setCart_recovery_email_sent(String cart_recovery_email_sent);
    
    /**
     * 设置 [购物车恢复EMail已发送]
     */
    public String getCart_recovery_email_sent();

    /**
     * 获取 [购物车恢复EMail已发送]脏标记
     */
    public boolean getCart_recovery_email_sentDirtyFlag();
    /**
     * 获取 [客户订单号]
     */
    public void setClient_order_ref(String client_order_ref);
    
    /**
     * 设置 [客户订单号]
     */
    public String getClient_order_ref();

    /**
     * 获取 [客户订单号]脏标记
     */
    public boolean getClient_order_refDirtyFlag();
    /**
     * 获取 [承诺日期]
     */
    public void setCommitment_date(Timestamp commitment_date);
    
    /**
     * 设置 [承诺日期]
     */
    public Timestamp getCommitment_date();

    /**
     * 获取 [承诺日期]脏标记
     */
    public boolean getCommitment_dateDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [确认日期]
     */
    public void setConfirmation_date(Timestamp confirmation_date);
    
    /**
     * 设置 [确认日期]
     */
    public Timestamp getConfirmation_date();

    /**
     * 获取 [确认日期]脏标记
     */
    public boolean getConfirmation_dateDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [汇率]
     */
    public void setCurrency_rate(Double currency_rate);
    
    /**
     * 设置 [汇率]
     */
    public Double getCurrency_rate();

    /**
     * 获取 [汇率]脏标记
     */
    public boolean getCurrency_rateDirtyFlag();
    /**
     * 获取 [单据日期]
     */
    public void setDate_order(Timestamp date_order);
    
    /**
     * 设置 [单据日期]
     */
    public Timestamp getDate_order();

    /**
     * 获取 [单据日期]脏标记
     */
    public boolean getDate_orderDirtyFlag();
    /**
     * 获取 [出库单]
     */
    public void setDelivery_count(Integer delivery_count);
    
    /**
     * 设置 [出库单]
     */
    public Integer getDelivery_count();

    /**
     * 获取 [出库单]脏标记
     */
    public boolean getDelivery_countDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [实际日期]
     */
    public void setEffective_date(Timestamp effective_date);
    
    /**
     * 设置 [实际日期]
     */
    public Timestamp getEffective_date();

    /**
     * 获取 [实际日期]脏标记
     */
    public boolean getEffective_dateDirtyFlag();
    /**
     * 获取 [预计日期]
     */
    public void setExpected_date(Timestamp expected_date);
    
    /**
     * 设置 [预计日期]
     */
    public Timestamp getExpected_date();

    /**
     * 获取 [预计日期]脏标记
     */
    public boolean getExpected_dateDirtyFlag();
    /**
     * 获取 [# 费用]
     */
    public void setExpense_count(Integer expense_count);
    
    /**
     * 设置 [# 费用]
     */
    public Integer getExpense_count();

    /**
     * 获取 [# 费用]脏标记
     */
    public boolean getExpense_countDirtyFlag();
    /**
     * 获取 [费用]
     */
    public void setExpense_ids(String expense_ids);
    
    /**
     * 设置 [费用]
     */
    public String getExpense_ids();

    /**
     * 获取 [费用]脏标记
     */
    public boolean getExpense_idsDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id(Integer fiscal_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getFiscal_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id_text(String fiscal_position_id_text);
    
    /**
     * 设置 [税科目调整]
     */
    public String getFiscal_position_id_text();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [贸易条款]
     */
    public void setIncoterm(Integer incoterm);
    
    /**
     * 设置 [贸易条款]
     */
    public Integer getIncoterm();

    /**
     * 获取 [贸易条款]脏标记
     */
    public boolean getIncotermDirtyFlag();
    /**
     * 获取 [贸易条款]
     */
    public void setIncoterm_text(String incoterm_text);
    
    /**
     * 设置 [贸易条款]
     */
    public String getIncoterm_text();

    /**
     * 获取 [贸易条款]脏标记
     */
    public boolean getIncoterm_textDirtyFlag();
    /**
     * 获取 [发票数]
     */
    public void setInvoice_count(Integer invoice_count);
    
    /**
     * 设置 [发票数]
     */
    public Integer getInvoice_count();

    /**
     * 获取 [发票数]脏标记
     */
    public boolean getInvoice_countDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_ids();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [发票状态]
     */
    public void setInvoice_status(String invoice_status);
    
    /**
     * 设置 [发票状态]
     */
    public String getInvoice_status();

    /**
     * 获取 [发票状态]脏标记
     */
    public boolean getInvoice_statusDirtyFlag();
    /**
     * 获取 [遗弃的购物车]
     */
    public void setIs_abandoned_cart(String is_abandoned_cart);
    
    /**
     * 设置 [遗弃的购物车]
     */
    public String getIs_abandoned_cart();

    /**
     * 获取 [遗弃的购物车]脏标记
     */
    public boolean getIs_abandoned_cartDirtyFlag();
    /**
     * 获取 [过期了]
     */
    public void setIs_expired(String is_expired);
    
    /**
     * 设置 [过期了]
     */
    public String getIs_expired();

    /**
     * 获取 [过期了]脏标记
     */
    public boolean getIs_expiredDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setName(String name);
    
    /**
     * 设置 [订单关联]
     */
    public String getName();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [条款和条件]
     */
    public void setNote(String note);
    
    /**
     * 设置 [条款和条件]
     */
    public String getNote();

    /**
     * 获取 [条款和条件]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [只是服务]
     */
    public void setOnly_services(String only_services);
    
    /**
     * 设置 [只是服务]
     */
    public String getOnly_services();

    /**
     * 获取 [只是服务]脏标记
     */
    public boolean getOnly_servicesDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_id(Integer opportunity_id);
    
    /**
     * 设置 [商机]
     */
    public Integer getOpportunity_id();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_idDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_id_text(String opportunity_id_text);
    
    /**
     * 设置 [商机]
     */
    public String getOpportunity_id_text();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_id_textDirtyFlag();
    /**
     * 获取 [订单行]
     */
    public void setOrder_line(String order_line);
    
    /**
     * 设置 [订单行]
     */
    public String getOrder_line();

    /**
     * 获取 [订单行]脏标记
     */
    public boolean getOrder_lineDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [发票地址]
     */
    public void setPartner_invoice_id(Integer partner_invoice_id);
    
    /**
     * 设置 [发票地址]
     */
    public Integer getPartner_invoice_id();

    /**
     * 获取 [发票地址]脏标记
     */
    public boolean getPartner_invoice_idDirtyFlag();
    /**
     * 获取 [发票地址]
     */
    public void setPartner_invoice_id_text(String partner_invoice_id_text);
    
    /**
     * 设置 [发票地址]
     */
    public String getPartner_invoice_id_text();

    /**
     * 获取 [发票地址]脏标记
     */
    public boolean getPartner_invoice_id_textDirtyFlag();
    /**
     * 获取 [送货地址]
     */
    public void setPartner_shipping_id(Integer partner_shipping_id);
    
    /**
     * 设置 [送货地址]
     */
    public Integer getPartner_shipping_id();

    /**
     * 获取 [送货地址]脏标记
     */
    public boolean getPartner_shipping_idDirtyFlag();
    /**
     * 获取 [送货地址]
     */
    public void setPartner_shipping_id_text(String partner_shipping_id_text);
    
    /**
     * 设置 [送货地址]
     */
    public String getPartner_shipping_id_text();

    /**
     * 获取 [送货地址]脏标记
     */
    public boolean getPartner_shipping_id_textDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id(Integer payment_term_id);
    
    /**
     * 设置 [付款条款]
     */
    public Integer getPayment_term_id();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_idDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id_text(String payment_term_id_text);
    
    /**
     * 设置 [付款条款]
     */
    public String getPayment_term_id_text();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_id_textDirtyFlag();
    /**
     * 获取 [拣货]
     */
    public void setPicking_ids(String picking_ids);
    
    /**
     * 设置 [拣货]
     */
    public String getPicking_ids();

    /**
     * 获取 [拣货]脏标记
     */
    public boolean getPicking_idsDirtyFlag();
    /**
     * 获取 [送货策略]
     */
    public void setPicking_policy(String picking_policy);
    
    /**
     * 设置 [送货策略]
     */
    public String getPicking_policy();

    /**
     * 获取 [送货策略]脏标记
     */
    public boolean getPicking_policyDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id(Integer pricelist_id);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPricelist_id();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_idDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id_text(String pricelist_id_text);
    
    /**
     * 设置 [价格表]
     */
    public String getPricelist_id_text();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_id_textDirtyFlag();
    /**
     * 获取 [补货组]
     */
    public void setProcurement_group_id(Integer procurement_group_id);
    
    /**
     * 设置 [补货组]
     */
    public Integer getProcurement_group_id();

    /**
     * 获取 [补货组]脏标记
     */
    public boolean getProcurement_group_idDirtyFlag();
    /**
     * 获取 [采购订单号]
     */
    public void setPurchase_order_count(Integer purchase_order_count);
    
    /**
     * 设置 [采购订单号]
     */
    public Integer getPurchase_order_count();

    /**
     * 获取 [采购订单号]脏标记
     */
    public boolean getPurchase_order_countDirtyFlag();
    /**
     * 获取 [付款参考:]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [付款参考:]
     */
    public String getReference();

    /**
     * 获取 [付款参考:]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [剩余确认天数]
     */
    public void setRemaining_validity_days(Integer remaining_validity_days);
    
    /**
     * 设置 [剩余确认天数]
     */
    public Integer getRemaining_validity_days();

    /**
     * 获取 [剩余确认天数]脏标记
     */
    public boolean getRemaining_validity_daysDirtyFlag();
    /**
     * 获取 [在线支付]
     */
    public void setRequire_payment(String require_payment);
    
    /**
     * 设置 [在线支付]
     */
    public String getRequire_payment();

    /**
     * 获取 [在线支付]脏标记
     */
    public boolean getRequire_paymentDirtyFlag();
    /**
     * 获取 [在线签名]
     */
    public void setRequire_signature(String require_signature);
    
    /**
     * 设置 [在线签名]
     */
    public String getRequire_signature();

    /**
     * 获取 [在线签名]脏标记
     */
    public boolean getRequire_signatureDirtyFlag();
    /**
     * 获取 [可选产品行]
     */
    public void setSale_order_option_ids(String sale_order_option_ids);
    
    /**
     * 设置 [可选产品行]
     */
    public String getSale_order_option_ids();

    /**
     * 获取 [可选产品行]脏标记
     */
    public boolean getSale_order_option_idsDirtyFlag();
    /**
     * 获取 [报价单模板]
     */
    public void setSale_order_template_id(Integer sale_order_template_id);
    
    /**
     * 设置 [报价单模板]
     */
    public Integer getSale_order_template_id();

    /**
     * 获取 [报价单模板]脏标记
     */
    public boolean getSale_order_template_idDirtyFlag();
    /**
     * 获取 [报价单模板]
     */
    public void setSale_order_template_id_text(String sale_order_template_id_text);
    
    /**
     * 设置 [报价单模板]
     */
    public String getSale_order_template_id_text();

    /**
     * 获取 [报价单模板]脏标记
     */
    public boolean getSale_order_template_id_textDirtyFlag();
    /**
     * 获取 [签名]
     */
    public void setSignature(byte[] signature);
    
    /**
     * 设置 [签名]
     */
    public byte[] getSignature();

    /**
     * 获取 [签名]脏标记
     */
    public boolean getSignatureDirtyFlag();
    /**
     * 获取 [已签核]
     */
    public void setSigned_by(String signed_by);
    
    /**
     * 设置 [已签核]
     */
    public String getSigned_by();

    /**
     * 获取 [已签核]脏标记
     */
    public boolean getSigned_byDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [标签]
     */
    public String getTag_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [交易]
     */
    public void setTransaction_ids(String transaction_ids);
    
    /**
     * 设置 [交易]
     */
    public String getTransaction_ids();

    /**
     * 获取 [交易]脏标记
     */
    public boolean getTransaction_idsDirtyFlag();
    /**
     * 获取 [类型名称]
     */
    public void setType_name(String type_name);
    
    /**
     * 设置 [类型名称]
     */
    public String getType_name();

    /**
     * 获取 [类型名称]脏标记
     */
    public boolean getType_nameDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [验证]
     */
    public void setValidity_date(Timestamp validity_date);
    
    /**
     * 设置 [验证]
     */
    public Timestamp getValidity_date();

    /**
     * 获取 [验证]脏标记
     */
    public boolean getValidity_dateDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id_text(String warehouse_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_id_textDirtyFlag();
    /**
     * 获取 [警告]
     */
    public void setWarning_stock(String warning_stock);
    
    /**
     * 设置 [警告]
     */
    public String getWarning_stock();

    /**
     * 获取 [警告]脏标记
     */
    public boolean getWarning_stockDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [网页显示订单明细]
     */
    public void setWebsite_order_line(String website_order_line);
    
    /**
     * 设置 [网页显示订单明细]
     */
    public String getWebsite_order_line();

    /**
     * 获取 [网页显示订单明细]脏标记
     */
    public boolean getWebsite_order_lineDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
	
    /**
     * 获取 [sale_order_lines]
     */
    public List<cn.ibizlab.odoo.core.client.model.Isale_order_line> getSale_order_lines();

    /**
     * 获取 [sale_order_lines]
     */
    public void setSale_order_lines(List <cn.ibizlab.odoo.core.client.model.Isale_order_line> sale_order_lines);

}

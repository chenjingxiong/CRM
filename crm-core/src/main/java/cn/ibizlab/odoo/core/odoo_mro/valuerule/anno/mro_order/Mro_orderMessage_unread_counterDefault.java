package cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_order;

import cn.ibizlab.odoo.core.odoo_mro.valuerule.validator.mro_order.Mro_orderMessage_unread_counterDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mro_order
 * 属性：Message_unread_counter
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mro_orderMessage_unread_counterDefaultValidator.class})
public @interface Mro_orderMessage_unread_counterDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

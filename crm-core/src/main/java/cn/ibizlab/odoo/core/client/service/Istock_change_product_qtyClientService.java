package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_change_product_qty;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
public interface Istock_change_product_qtyClientService{

    public Istock_change_product_qty createModel() ;

    public void createBatch(List<Istock_change_product_qty> stock_change_product_qties);

    public void removeBatch(List<Istock_change_product_qty> stock_change_product_qties);

    public void update(Istock_change_product_qty stock_change_product_qty);

    public Page<Istock_change_product_qty> fetchDefault(SearchContext context);

    public void remove(Istock_change_product_qty stock_change_product_qty);

    public void updateBatch(List<Istock_change_product_qty> stock_change_product_qties);

    public void get(Istock_change_product_qty stock_change_product_qty);

    public void create(Istock_change_product_qty stock_change_product_qty);

    public Page<Istock_change_product_qty> select(SearchContext context);

    public void getDraft(Istock_change_product_qty stock_change_product_qty);

}

package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_package_level;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_package_level.Stock_package_levelShow_lots_m2oDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_package_level
 * 属性：Show_lots_m2o
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_package_levelShow_lots_m2oDefaultValidator.class})
public @interface Stock_package_levelShow_lots_m2oDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

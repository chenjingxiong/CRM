package cn.ibizlab.odoo.core.odoo_calendar.valuerule.anno.calendar_attendee;

import cn.ibizlab.odoo.core.odoo_calendar.valuerule.validator.calendar_attendee.Calendar_attendeeAccess_tokenDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Calendar_attendee
 * 属性：Access_token
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Calendar_attendeeAccess_tokenDefaultValidator.class})
public @interface Calendar_attendeeAccess_tokenDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_task_parts_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_task_parts_lineFeignClient;

/**
 * 实体[Maintenance Planned Parts] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_task_parts_lineServiceImpl implements IMro_task_parts_lineService {

    @Autowired
    mro_task_parts_lineFeignClient mro_task_parts_lineFeignClient;


    @Override
    public Mro_task_parts_line getDraft(Mro_task_parts_line et) {
        et=mro_task_parts_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mro_task_parts_line et) {
        Mro_task_parts_line rt = mro_task_parts_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_task_parts_line> list){
        mro_task_parts_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_task_parts_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_task_parts_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mro_task_parts_line et) {
        Mro_task_parts_line rt = mro_task_parts_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_task_parts_line> list){
        mro_task_parts_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_task_parts_line get(Integer id) {
		Mro_task_parts_line et=mro_task_parts_lineFeignClient.get(id);
        if(et==null){
            et=new Mro_task_parts_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context) {
        Page<Mro_task_parts_line> mro_task_parts_lines=mro_task_parts_lineFeignClient.searchDefault(context);
        return mro_task_parts_lines;
    }


}



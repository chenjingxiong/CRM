package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_payment] 对象
 */
public interface Iaccount_payment {

    /**
     * 获取 [付款金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [付款金额]
     */
    public Double getAmount();

    /**
     * 获取 [付款金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [备忘]
     */
    public void setCommunication(String communication);
    
    /**
     * 设置 [备忘]
     */
    public String getCommunication();

    /**
     * 获取 [备忘]脏标记
     */
    public boolean getCommunicationDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [目标账户]
     */
    public void setDestination_account_id(Integer destination_account_id);
    
    /**
     * 设置 [目标账户]
     */
    public Integer getDestination_account_id();

    /**
     * 获取 [目标账户]脏标记
     */
    public boolean getDestination_account_idDirtyFlag();
    /**
     * 获取 [转账到]
     */
    public void setDestination_journal_id(Integer destination_journal_id);
    
    /**
     * 设置 [转账到]
     */
    public Integer getDestination_journal_id();

    /**
     * 获取 [转账到]脏标记
     */
    public boolean getDestination_journal_idDirtyFlag();
    /**
     * 获取 [转账到]
     */
    public void setDestination_journal_id_text(String destination_journal_id_text);
    
    /**
     * 设置 [转账到]
     */
    public String getDestination_journal_id_text();

    /**
     * 获取 [转账到]脏标记
     */
    public boolean getDestination_journal_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [有发票]
     */
    public void setHas_invoices(String has_invoices);
    
    /**
     * 设置 [有发票]
     */
    public String getHas_invoices();

    /**
     * 获取 [有发票]脏标记
     */
    public boolean getHas_invoicesDirtyFlag();
    /**
     * 获取 [隐藏付款方式]
     */
    public void setHide_payment_method(String hide_payment_method);
    
    /**
     * 设置 [隐藏付款方式]
     */
    public String getHide_payment_method();

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    public boolean getHide_payment_methodDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_ids();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [付款日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [付款日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [付款日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [付款日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [付款日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [付款日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [需要一个动作消息的编码]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [需要一个动作消息的编码]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作编号]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作编号]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作编号]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [分录行]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [分录行]
     */
    public String getMove_line_ids();

    /**
     * 获取 [分录行]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [日记账分录名称]
     */
    public void setMove_name(String move_name);
    
    /**
     * 设置 [日记账分录名称]
     */
    public String getMove_name();

    /**
     * 获取 [日记账分录名称]脏标记
     */
    public boolean getMove_nameDirtyFlag();
    /**
     * 获取 [凭证已核销]
     */
    public void setMove_reconciled(String move_reconciled);
    
    /**
     * 设置 [凭证已核销]
     */
    public String getMove_reconciled();

    /**
     * 获取 [凭证已核销]脏标记
     */
    public boolean getMove_reconciledDirtyFlag();
    /**
     * 获取 [多]
     */
    public void setMulti(String multi);
    
    /**
     * 设置 [多]
     */
    public String getMulti();

    /**
     * 获取 [多]脏标记
     */
    public boolean getMultiDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [收款银行账号]
     */
    public void setPartner_bank_account_id(Integer partner_bank_account_id);
    
    /**
     * 设置 [收款银行账号]
     */
    public Integer getPartner_bank_account_id();

    /**
     * 获取 [收款银行账号]脏标记
     */
    public boolean getPartner_bank_account_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [业务伙伴类型]
     */
    public void setPartner_type(String partner_type);
    
    /**
     * 设置 [业务伙伴类型]
     */
    public String getPartner_type();

    /**
     * 获取 [业务伙伴类型]脏标记
     */
    public boolean getPartner_typeDirtyFlag();
    /**
     * 获取 [付款日期]
     */
    public void setPayment_date(Timestamp payment_date);
    
    /**
     * 设置 [付款日期]
     */
    public Timestamp getPayment_date();

    /**
     * 获取 [付款日期]脏标记
     */
    public boolean getPayment_dateDirtyFlag();
    /**
     * 获取 [付款差异]
     */
    public void setPayment_difference(Double payment_difference);
    
    /**
     * 设置 [付款差异]
     */
    public Double getPayment_difference();

    /**
     * 获取 [付款差异]脏标记
     */
    public boolean getPayment_differenceDirtyFlag();
    /**
     * 获取 [付款差异处理]
     */
    public void setPayment_difference_handling(String payment_difference_handling);
    
    /**
     * 设置 [付款差异处理]
     */
    public String getPayment_difference_handling();

    /**
     * 获取 [付款差异处理]脏标记
     */
    public boolean getPayment_difference_handlingDirtyFlag();
    /**
     * 获取 [代码]
     */
    public void setPayment_method_code(String payment_method_code);
    
    /**
     * 设置 [代码]
     */
    public String getPayment_method_code();

    /**
     * 获取 [代码]脏标记
     */
    public boolean getPayment_method_codeDirtyFlag();
    /**
     * 获取 [付款方法类型]
     */
    public void setPayment_method_id(Integer payment_method_id);
    
    /**
     * 设置 [付款方法类型]
     */
    public Integer getPayment_method_id();

    /**
     * 获取 [付款方法类型]脏标记
     */
    public boolean getPayment_method_idDirtyFlag();
    /**
     * 获取 [付款方法类型]
     */
    public void setPayment_method_id_text(String payment_method_id_text);
    
    /**
     * 设置 [付款方法类型]
     */
    public String getPayment_method_id_text();

    /**
     * 获取 [付款方法类型]脏标记
     */
    public boolean getPayment_method_id_textDirtyFlag();
    /**
     * 获取 [付款参考]
     */
    public void setPayment_reference(String payment_reference);
    
    /**
     * 设置 [付款参考]
     */
    public String getPayment_reference();

    /**
     * 获取 [付款参考]脏标记
     */
    public boolean getPayment_referenceDirtyFlag();
    /**
     * 获取 [保存的付款令牌]
     */
    public void setPayment_token_id(Integer payment_token_id);
    
    /**
     * 设置 [保存的付款令牌]
     */
    public Integer getPayment_token_id();

    /**
     * 获取 [保存的付款令牌]脏标记
     */
    public boolean getPayment_token_idDirtyFlag();
    /**
     * 获取 [保存的付款令牌]
     */
    public void setPayment_token_id_text(String payment_token_id_text);
    
    /**
     * 设置 [保存的付款令牌]
     */
    public String getPayment_token_id_text();

    /**
     * 获取 [保存的付款令牌]脏标记
     */
    public boolean getPayment_token_id_textDirtyFlag();
    /**
     * 获取 [付款交易]
     */
    public void setPayment_transaction_id(Integer payment_transaction_id);
    
    /**
     * 设置 [付款交易]
     */
    public Integer getPayment_transaction_id();

    /**
     * 获取 [付款交易]脏标记
     */
    public boolean getPayment_transaction_idDirtyFlag();
    /**
     * 获取 [付款类型]
     */
    public void setPayment_type(String payment_type);
    
    /**
     * 设置 [付款类型]
     */
    public String getPayment_type();

    /**
     * 获取 [付款类型]脏标记
     */
    public boolean getPayment_typeDirtyFlag();
    /**
     * 获取 [已核销的发票]
     */
    public void setReconciled_invoice_ids(String reconciled_invoice_ids);
    
    /**
     * 设置 [已核销的发票]
     */
    public String getReconciled_invoice_ids();

    /**
     * 获取 [已核销的发票]脏标记
     */
    public boolean getReconciled_invoice_idsDirtyFlag();
    /**
     * 获取 [显示合作伙伴银行账户]
     */
    public void setShow_partner_bank_account(String show_partner_bank_account);
    
    /**
     * 设置 [显示合作伙伴银行账户]
     */
    public String getShow_partner_bank_account();

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    public boolean getShow_partner_bank_accountDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [差异科目]
     */
    public void setWriteoff_account_id(Integer writeoff_account_id);
    
    /**
     * 设置 [差异科目]
     */
    public Integer getWriteoff_account_id();

    /**
     * 获取 [差异科目]脏标记
     */
    public boolean getWriteoff_account_idDirtyFlag();
    /**
     * 获取 [差异科目]
     */
    public void setWriteoff_account_id_text(String writeoff_account_id_text);
    
    /**
     * 设置 [差异科目]
     */
    public String getWriteoff_account_id_text();

    /**
     * 获取 [差异科目]脏标记
     */
    public boolean getWriteoff_account_id_textDirtyFlag();
    /**
     * 获取 [日记账项目标签]
     */
    public void setWriteoff_label(String writeoff_label);
    
    /**
     * 设置 [日记账项目标签]
     */
    public String getWriteoff_label();

    /**
     * 获取 [日记账项目标签]脏标记
     */
    public boolean getWriteoff_labelDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

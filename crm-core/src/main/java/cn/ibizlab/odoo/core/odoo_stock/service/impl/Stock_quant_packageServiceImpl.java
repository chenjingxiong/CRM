package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quant_packageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_quant_packageFeignClient;

/**
 * 实体[包裹] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_quant_packageServiceImpl implements IStock_quant_packageService {

    @Autowired
    stock_quant_packageFeignClient stock_quant_packageFeignClient;


    @Override
    public boolean update(Stock_quant_package et) {
        Stock_quant_package rt = stock_quant_packageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_quant_package> list){
        stock_quant_packageFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_quant_packageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_quant_packageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_quant_package et) {
        Stock_quant_package rt = stock_quant_packageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_quant_package> list){
        stock_quant_packageFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_quant_package getDraft(Stock_quant_package et) {
        et=stock_quant_packageFeignClient.getDraft();
        return et;
    }

    @Override
    public Stock_quant_package get(Integer id) {
		Stock_quant_package et=stock_quant_packageFeignClient.get(id);
        if(et==null){
            et=new Stock_quant_package();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context) {
        Page<Stock_quant_package> stock_quant_packages=stock_quant_packageFeignClient.searchDefault(context);
        return stock_quant_packages;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_inventory_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_inventory_line] 服务对象接口
 */
public interface Istock_inventory_lineClientService{

    public Istock_inventory_line createModel() ;

    public void updateBatch(List<Istock_inventory_line> stock_inventory_lines);

    public void removeBatch(List<Istock_inventory_line> stock_inventory_lines);

    public void createBatch(List<Istock_inventory_line> stock_inventory_lines);

    public void update(Istock_inventory_line stock_inventory_line);

    public void create(Istock_inventory_line stock_inventory_line);

    public Page<Istock_inventory_line> fetchDefault(SearchContext context);

    public void remove(Istock_inventory_line stock_inventory_line);

    public void get(Istock_inventory_line stock_inventory_line);

    public Page<Istock_inventory_line> select(SearchContext context);

    public void getDraft(Istock_inventory_line stock_inventory_line);

}

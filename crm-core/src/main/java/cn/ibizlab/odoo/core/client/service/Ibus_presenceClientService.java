package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibus_presence;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[bus_presence] 服务对象接口
 */
public interface Ibus_presenceClientService{

    public Ibus_presence createModel() ;

    public void updateBatch(List<Ibus_presence> bus_presences);

    public void get(Ibus_presence bus_presence);

    public Page<Ibus_presence> fetchDefault(SearchContext context);

    public void create(Ibus_presence bus_presence);

    public void createBatch(List<Ibus_presence> bus_presences);

    public void removeBatch(List<Ibus_presence> bus_presences);

    public void update(Ibus_presence bus_presence);

    public void remove(Ibus_presence bus_presence);

    public Page<Ibus_presence> select(SearchContext context);

    public void getDraft(Ibus_presence bus_presence);

}

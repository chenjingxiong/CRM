package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_term_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_payment_term_lineFeignClient;

/**
 * 实体[付款条款行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_payment_term_lineServiceImpl implements IAccount_payment_term_lineService {

    @Autowired
    account_payment_term_lineFeignClient account_payment_term_lineFeignClient;


    @Override
    public boolean update(Account_payment_term_line et) {
        Account_payment_term_line rt = account_payment_term_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_payment_term_line> list){
        account_payment_term_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_payment_term_line et) {
        Account_payment_term_line rt = account_payment_term_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment_term_line> list){
        account_payment_term_lineFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_payment_term_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_payment_term_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Account_payment_term_line get(Integer id) {
		Account_payment_term_line et=account_payment_term_lineFeignClient.get(id);
        if(et==null){
            et=new Account_payment_term_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_payment_term_line getDraft(Account_payment_term_line et) {
        et=account_payment_term_lineFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment_term_line> searchDefault(Account_payment_term_lineSearchContext context) {
        Page<Account_payment_term_line> account_payment_term_lines=account_payment_term_lineFeignClient.searchDefault(context);
        return account_payment_term_lines;
    }


}



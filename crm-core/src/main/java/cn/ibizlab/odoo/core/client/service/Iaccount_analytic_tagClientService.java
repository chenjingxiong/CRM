package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_tag] 服务对象接口
 */
public interface Iaccount_analytic_tagClientService{

    public Iaccount_analytic_tag createModel() ;

    public void createBatch(List<Iaccount_analytic_tag> account_analytic_tags);

    public Page<Iaccount_analytic_tag> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_analytic_tag> account_analytic_tags);

    public void remove(Iaccount_analytic_tag account_analytic_tag);

    public void update(Iaccount_analytic_tag account_analytic_tag);

    public void get(Iaccount_analytic_tag account_analytic_tag);

    public void create(Iaccount_analytic_tag account_analytic_tag);

    public void removeBatch(List<Iaccount_analytic_tag> account_analytic_tags);

    public Page<Iaccount_analytic_tag> select(SearchContext context);

    public void getDraft(Iaccount_analytic_tag account_analytic_tag);

}

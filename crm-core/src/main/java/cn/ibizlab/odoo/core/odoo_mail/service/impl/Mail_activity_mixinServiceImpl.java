package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_activity_mixinFeignClient;

/**
 * 实体[活动Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activity_mixinServiceImpl implements IMail_activity_mixinService {

    @Autowired
    mail_activity_mixinFeignClient mail_activity_mixinFeignClient;


    @Override
    public Mail_activity_mixin get(Integer id) {
		Mail_activity_mixin et=mail_activity_mixinFeignClient.get(id);
        if(et==null){
            et=new Mail_activity_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_activity_mixin et) {
        Mail_activity_mixin rt = mail_activity_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_activity_mixin> list){
        mail_activity_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_activity_mixin et) {
        Mail_activity_mixin rt = mail_activity_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity_mixin> list){
        mail_activity_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_activity_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_activity_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_activity_mixin getDraft(Mail_activity_mixin et) {
        et=mail_activity_mixinFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context) {
        Page<Mail_activity_mixin> mail_activity_mixins=mail_activity_mixinFeignClient.searchDefault(context);
        return mail_activity_mixins;
    }


}



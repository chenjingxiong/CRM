package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_print_journal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_print_journal] 服务对象接口
 */
public interface Iaccount_print_journalClientService{

    public Iaccount_print_journal createModel() ;

    public void create(Iaccount_print_journal account_print_journal);

    public void createBatch(List<Iaccount_print_journal> account_print_journals);

    public void update(Iaccount_print_journal account_print_journal);

    public void remove(Iaccount_print_journal account_print_journal);

    public Page<Iaccount_print_journal> fetchDefault(SearchContext context);

    public void removeBatch(List<Iaccount_print_journal> account_print_journals);

    public void get(Iaccount_print_journal account_print_journal);

    public void updateBatch(List<Iaccount_print_journal> account_print_journals);

    public Page<Iaccount_print_journal> select(SearchContext context);

    public void getDraft(Iaccount_print_journal account_print_journal);

}

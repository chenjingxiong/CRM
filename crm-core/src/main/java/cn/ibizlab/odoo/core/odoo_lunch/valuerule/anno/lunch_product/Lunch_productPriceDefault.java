package cn.ibizlab.odoo.core.odoo_lunch.valuerule.anno.lunch_product;

import cn.ibizlab.odoo.core.odoo_lunch.valuerule.validator.lunch_product.Lunch_productPriceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Lunch_product
 * 属性：Price
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Lunch_productPriceDefaultValidator.class})
public @interface Lunch_productPriceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [游戏化目标定义] 对象
 */
@Data
public class Gamification_goal_definition extends EntityClient implements Serializable {

    /**
     * 后缀
     */
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    private String suffix;

    /**
     * 批量用户的特有字段
     */
    @DEField(name = "batch_distinctive_field")
    @JSONField(name = "batch_distinctive_field")
    @JsonProperty("batch_distinctive_field")
    private Integer batchDistinctiveField;

    /**
     * 金钱值
     */
    @JSONField(name = "monetary")
    @JsonProperty("monetary")
    private String monetary;

    /**
     * 目标绩效
     */
    @JSONField(name = "condition")
    @JsonProperty("condition")
    private String condition;

    /**
     * Python 代码
     */
    @DEField(name = "compute_code")
    @JSONField(name = "compute_code")
    @JsonProperty("compute_code")
    private String computeCode;

    /**
     * 计算模式
     */
    @DEField(name = "computation_mode")
    @JSONField(name = "computation_mode")
    @JsonProperty("computation_mode")
    private String computationMode;

    /**
     * 动作
     */
    @DEField(name = "action_id")
    @JSONField(name = "action_id")
    @JsonProperty("action_id")
    private Integer actionId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 筛选域
     */
    @JSONField(name = "domain")
    @JsonProperty("domain")
    private String domain;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 字段总计
     */
    @DEField(name = "field_id")
    @JSONField(name = "field_id")
    @JsonProperty("field_id")
    private Integer fieldId;

    /**
     * 用户ID字段
     */
    @DEField(name = "res_id_field")
    @JSONField(name = "res_id_field")
    @JsonProperty("res_id_field")
    private String resIdField;

    /**
     * 批量模式
     */
    @DEField(name = "batch_mode")
    @JSONField(name = "batch_mode")
    @JsonProperty("batch_mode")
    private String batchMode;

    /**
     * 模型
     */
    @DEField(name = "model_id")
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 目标说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 日期字段
     */
    @DEField(name = "field_date_id")
    @JSONField(name = "field_date_id")
    @JsonProperty("field_date_id")
    private Integer fieldDateId;

    /**
     * 显示为
     */
    @DEField(name = "display_mode")
    @JSONField(name = "display_mode")
    @JsonProperty("display_mode")
    private String displayMode;

    /**
     * 完整的后缀
     */
    @JSONField(name = "full_suffix")
    @JsonProperty("full_suffix")
    private String fullSuffix;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 批处理模式的求值表达式
     */
    @DEField(name = "batch_user_expression")
    @JSONField(name = "batch_user_expression")
    @JsonProperty("batch_user_expression")
    private String batchUserExpression;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 目标定义
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [后缀]
     */
    public void setSuffix(String suffix){
        this.suffix = suffix ;
        this.modify("suffix",suffix);
    }
    /**
     * 设置 [批量用户的特有字段]
     */
    public void setBatchDistinctiveField(Integer batchDistinctiveField){
        this.batchDistinctiveField = batchDistinctiveField ;
        this.modify("batch_distinctive_field",batchDistinctiveField);
    }
    /**
     * 设置 [金钱值]
     */
    public void setMonetary(String monetary){
        this.monetary = monetary ;
        this.modify("monetary",monetary);
    }
    /**
     * 设置 [目标绩效]
     */
    public void setCondition(String condition){
        this.condition = condition ;
        this.modify("condition",condition);
    }
    /**
     * 设置 [Python 代码]
     */
    public void setComputeCode(String computeCode){
        this.computeCode = computeCode ;
        this.modify("compute_code",computeCode);
    }
    /**
     * 设置 [计算模式]
     */
    public void setComputationMode(String computationMode){
        this.computationMode = computationMode ;
        this.modify("computation_mode",computationMode);
    }
    /**
     * 设置 [动作]
     */
    public void setActionId(Integer actionId){
        this.actionId = actionId ;
        this.modify("action_id",actionId);
    }
    /**
     * 设置 [筛选域]
     */
    public void setDomain(String domain){
        this.domain = domain ;
        this.modify("domain",domain);
    }
    /**
     * 设置 [字段总计]
     */
    public void setFieldId(Integer fieldId){
        this.fieldId = fieldId ;
        this.modify("field_id",fieldId);
    }
    /**
     * 设置 [用户ID字段]
     */
    public void setResIdField(String resIdField){
        this.resIdField = resIdField ;
        this.modify("res_id_field",resIdField);
    }
    /**
     * 设置 [批量模式]
     */
    public void setBatchMode(String batchMode){
        this.batchMode = batchMode ;
        this.modify("batch_mode",batchMode);
    }
    /**
     * 设置 [模型]
     */
    public void setModelId(Integer modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }
    /**
     * 设置 [目标说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [日期字段]
     */
    public void setFieldDateId(Integer fieldDateId){
        this.fieldDateId = fieldDateId ;
        this.modify("field_date_id",fieldDateId);
    }
    /**
     * 设置 [显示为]
     */
    public void setDisplayMode(String displayMode){
        this.displayMode = displayMode ;
        this.modify("display_mode",displayMode);
    }
    /**
     * 设置 [批处理模式的求值表达式]
     */
    public void setBatchUserExpression(String batchUserExpression){
        this.batchUserExpression = batchUserExpression ;
        this.modify("batch_user_expression",batchUserExpression);
    }
    /**
     * 设置 [目标定义]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

}



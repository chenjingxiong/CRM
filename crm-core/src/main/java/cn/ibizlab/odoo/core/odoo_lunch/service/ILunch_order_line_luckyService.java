package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;


/**
 * 实体[Lunch_order_line_lucky] 服务对象接口
 */
public interface ILunch_order_line_luckyService{

    Lunch_order_line_lucky get(Integer key) ;
    boolean create(Lunch_order_line_lucky et) ;
    void createBatch(List<Lunch_order_line_lucky> list) ;
    boolean update(Lunch_order_line_lucky et) ;
    void updateBatch(List<Lunch_order_line_lucky> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Lunch_order_line_lucky getDraft(Lunch_order_line_lucky et) ;
    Page<Lunch_order_line_lucky> searchDefault(Lunch_order_line_luckySearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;


/**
 * 实体[Account_account_template] 服务对象接口
 */
public interface IAccount_account_templateService{

    Account_account_template get(Integer key) ;
    boolean create(Account_account_template et) ;
    void createBatch(List<Account_account_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_account_template getDraft(Account_account_template et) ;
    boolean update(Account_account_template et) ;
    void updateBatch(List<Account_account_template> list) ;
    Page<Account_account_template> searchDefault(Account_account_templateSearchContext context) ;

}




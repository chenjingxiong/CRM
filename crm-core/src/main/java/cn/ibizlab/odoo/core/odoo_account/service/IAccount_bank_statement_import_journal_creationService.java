package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;


/**
 * 实体[Account_bank_statement_import_journal_creation] 服务对象接口
 */
public interface IAccount_bank_statement_import_journal_creationService{

    boolean create(Account_bank_statement_import_journal_creation et) ;
    void createBatch(List<Account_bank_statement_import_journal_creation> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_bank_statement_import_journal_creation et) ;
    void updateBatch(List<Account_bank_statement_import_journal_creation> list) ;
    Account_bank_statement_import_journal_creation getDraft(Account_bank_statement_import_journal_creation et) ;
    Account_bank_statement_import_journal_creation get(Integer key) ;
    Page<Account_bank_statement_import_journal_creation> searchDefault(Account_bank_statement_import_journal_creationSearchContext context) ;

}




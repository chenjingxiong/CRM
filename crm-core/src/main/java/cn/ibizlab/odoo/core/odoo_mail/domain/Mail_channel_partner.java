package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [渠道指南] 对象
 */
@Data
public class Mail_channel_partner extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 黑名单
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 是否置顶
     */
    @DEField(name = "is_pinned")
    @JSONField(name = "is_pinned")
    @JsonProperty("is_pinned")
    private String isPinned;

    /**
     * 对话收拢状态
     */
    @DEField(name = "fold_state")
    @JSONField(name = "fold_state")
    @JsonProperty("fold_state")
    private String foldState;

    /**
     * 对话已最小化
     */
    @DEField(name = "is_minimized")
    @JSONField(name = "is_minimized")
    @JsonProperty("is_minimized")
    private String isMinimized;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 收件人
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 渠道
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;

    /**
     * EMail
     */
    @JSONField(name = "partner_email")
    @JsonProperty("partner_email")
    private String partnerEmail;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 渠道
     */
    @DEField(name = "channel_id")
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 收件人
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 最近一次查阅
     */
    @DEField(name = "seen_message_id")
    @JSONField(name = "seen_message_id")
    @JsonProperty("seen_message_id")
    private Integer seenMessageId;


    /**
     * 
     */
    @JSONField(name = "odoochannel")
    @JsonProperty("odoochannel")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel odooChannel;

    /**
     * 
     */
    @JSONField(name = "odooseenmessage")
    @JsonProperty("odooseenmessage")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message odooSeenMessage;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [是否置顶]
     */
    public void setIsPinned(String isPinned){
        this.isPinned = isPinned ;
        this.modify("is_pinned",isPinned);
    }
    /**
     * 设置 [对话收拢状态]
     */
    public void setFoldState(String foldState){
        this.foldState = foldState ;
        this.modify("fold_state",foldState);
    }
    /**
     * 设置 [对话已最小化]
     */
    public void setIsMinimized(String isMinimized){
        this.isMinimized = isMinimized ;
        this.modify("is_minimized",isMinimized);
    }
    /**
     * 设置 [渠道]
     */
    public void setChannelId(Integer channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }
    /**
     * 设置 [收件人]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [最近一次查阅]
     */
    public void setSeenMessageId(Integer seenMessageId){
        this.seenMessageId = seenMessageId ;
        this.modify("seen_message_id",seenMessageId);
    }

}



package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-activity-mixin", fallback = mail_activity_mixinFallback.class)
public interface mail_activity_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins")
    Mail_activity_mixin create(@RequestBody Mail_activity_mixin mail_activity_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/batch")
    Boolean createBatch(@RequestBody List<Mail_activity_mixin> mail_activity_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/searchdefault")
    Page<Mail_activity_mixin> searchDefault(@RequestBody Mail_activity_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/{id}")
    Mail_activity_mixin get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/{id}")
    Mail_activity_mixin update(@PathVariable("id") Integer id,@RequestBody Mail_activity_mixin mail_activity_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/batch")
    Boolean updateBatch(@RequestBody List<Mail_activity_mixin> mail_activity_mixins);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/select")
    Page<Mail_activity_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/getdraft")
    Mail_activity_mixin getDraft();


}

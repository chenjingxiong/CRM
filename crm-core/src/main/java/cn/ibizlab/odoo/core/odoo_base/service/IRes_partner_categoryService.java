package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;


/**
 * 实体[Res_partner_category] 服务对象接口
 */
public interface IRes_partner_categoryService{

    Res_partner_category get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_partner_category et) ;
    void createBatch(List<Res_partner_category> list) ;
    boolean update(Res_partner_category et) ;
    void updateBatch(List<Res_partner_category> list) ;
    Res_partner_category getDraft(Res_partner_category et) ;
    Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context) ;

}




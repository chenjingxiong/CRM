package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_challenge;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_challenge] 服务对象接口
 */
public interface Igamification_challengeClientService{

    public Igamification_challenge createModel() ;

    public void create(Igamification_challenge gamification_challenge);

    public void createBatch(List<Igamification_challenge> gamification_challenges);

    public void update(Igamification_challenge gamification_challenge);

    public void get(Igamification_challenge gamification_challenge);

    public Page<Igamification_challenge> fetchDefault(SearchContext context);

    public void remove(Igamification_challenge gamification_challenge);

    public void updateBatch(List<Igamification_challenge> gamification_challenges);

    public void removeBatch(List<Igamification_challenge> gamification_challenges);

    public Page<Igamification_challenge> select(SearchContext context);

    public void getDraft(Igamification_challenge gamification_challenge);

}

package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_merge_opportunityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_merge_opportunityFeignClient;

/**
 * 实体[合并商机] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_merge_opportunityServiceImpl implements ICrm_merge_opportunityService {

    @Autowired
    crm_merge_opportunityFeignClient crm_merge_opportunityFeignClient;


    @Override
    public Crm_merge_opportunity getDraft(Crm_merge_opportunity et) {
        et=crm_merge_opportunityFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Crm_merge_opportunity et) {
        Crm_merge_opportunity rt = crm_merge_opportunityFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_merge_opportunity> list){
        crm_merge_opportunityFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Crm_merge_opportunity et) {
        Crm_merge_opportunity rt = crm_merge_opportunityFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_merge_opportunity> list){
        crm_merge_opportunityFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_merge_opportunityFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_merge_opportunityFeignClient.removeBatch(idList);
    }

    @Override
    public Crm_merge_opportunity get(Integer id) {
		Crm_merge_opportunity et=crm_merge_opportunityFeignClient.get(id);
        if(et==null){
            et=new Crm_merge_opportunity();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_merge_opportunity> searchDefault(Crm_merge_opportunitySearchContext context) {
        Page<Crm_merge_opportunity> crm_merge_opportunitys=crm_merge_opportunityFeignClient.searchDefault(context);
        return crm_merge_opportunitys;
    }


}



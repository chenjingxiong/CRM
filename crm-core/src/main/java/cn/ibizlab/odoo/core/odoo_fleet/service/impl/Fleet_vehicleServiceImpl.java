package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicleFeignClient;

/**
 * 实体[车辆] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicleServiceImpl implements IFleet_vehicleService {

    @Autowired
    fleet_vehicleFeignClient fleet_vehicleFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicleFeignClient.removeBatch(idList);
    }

    @Override
    public Fleet_vehicle getDraft(Fleet_vehicle et) {
        et=fleet_vehicleFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Fleet_vehicle et) {
        Fleet_vehicle rt = fleet_vehicleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle> list){
        fleet_vehicleFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Fleet_vehicle et) {
        Fleet_vehicle rt = fleet_vehicleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle> list){
        fleet_vehicleFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_vehicle get(Integer id) {
		Fleet_vehicle et=fleet_vehicleFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context) {
        Page<Fleet_vehicle> fleet_vehicles=fleet_vehicleFeignClient.searchDefault(context);
        return fleet_vehicles;
    }


}



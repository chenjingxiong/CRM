package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_template] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-template", fallback = mail_templateFallback.class)
public interface mail_templateFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_templates")
    Mail_template create(@RequestBody Mail_template mail_template);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_templates/batch")
    Boolean createBatch(@RequestBody List<Mail_template> mail_templates);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_templates/searchdefault")
    Page<Mail_template> searchDefault(@RequestBody Mail_templateSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/{id}")
    Mail_template update(@PathVariable("id") Integer id,@RequestBody Mail_template mail_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/batch")
    Boolean updateBatch(@RequestBody List<Mail_template> mail_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_templates/{id}")
    Mail_template get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_templates/select")
    Page<Mail_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_templates/getdraft")
    Mail_template getDraft();


}

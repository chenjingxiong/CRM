package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_advance_payment_inv;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_advance_payment_inv.Sale_advance_payment_invDeposit_account_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_advance_payment_inv
 * 属性：Deposit_account_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_advance_payment_invDeposit_account_id_textDefaultValidator.class})
public @interface Sale_advance_payment_invDeposit_account_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_invoice_tax] 对象
 */
public interface Iaccount_invoice_tax {

    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id(Integer account_analytic_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAccount_analytic_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id_text(String account_analytic_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAccount_analytic_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_id_textDirtyFlag();
    /**
     * 获取 [税率科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [税率科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [税率科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [税率科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [税率科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [税率科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [税率金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [税率金额]
     */
    public Double getAmount();

    /**
     * 获取 [税率金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [金额差异]
     */
    public void setAmount_rounding(Double amount_rounding);
    
    /**
     * 设置 [金额差异]
     */
    public Double getAmount_rounding();

    /**
     * 获取 [金额差异]脏标记
     */
    public boolean getAmount_roundingDirtyFlag();
    /**
     * 获取 [总金额]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [总金额]
     */
    public Double getAmount_total();

    /**
     * 获取 [总金额]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [基础]
     */
    public void setBase(Double base);
    
    /**
     * 设置 [基础]
     */
    public Double getBase();

    /**
     * 获取 [基础]脏标记
     */
    public boolean getBaseDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id(Integer invoice_id);
    
    /**
     * 设置 [发票]
     */
    public Integer getInvoice_id();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id_text(String invoice_id_text);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_id_text();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_id_textDirtyFlag();
    /**
     * 获取 [手动]
     */
    public void setManual(String manual);
    
    /**
     * 设置 [手动]
     */
    public String getManual();

    /**
     * 获取 [手动]脏标记
     */
    public boolean getManualDirtyFlag();
    /**
     * 获取 [税说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [税说明]
     */
    public String getName();

    /**
     * 获取 [税说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTax_id(Integer tax_id);
    
    /**
     * 设置 [税率]
     */
    public Integer getTax_id();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTax_idDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTax_id_text(String tax_id_text);
    
    /**
     * 设置 [税率]
     */
    public String getTax_id_text();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTax_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

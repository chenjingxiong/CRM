package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_charSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
@Component
public class base_import_tests_models_charFallback implements base_import_tests_models_charFeignClient{

    public Base_import_tests_models_char create(Base_import_tests_models_char base_import_tests_models_char){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char> base_import_tests_models_chars){
            return false;
     }


    public Page<Base_import_tests_models_char> searchDefault(Base_import_tests_models_charSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_import_tests_models_char get(Integer id){
            return null;
     }



    public Base_import_tests_models_char update(Integer id, Base_import_tests_models_char base_import_tests_models_char){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char> base_import_tests_models_chars){
            return false;
     }



    public Page<Base_import_tests_models_char> select(){
            return null;
     }

    public Base_import_tests_models_char getDraft(){
            return null;
    }



}

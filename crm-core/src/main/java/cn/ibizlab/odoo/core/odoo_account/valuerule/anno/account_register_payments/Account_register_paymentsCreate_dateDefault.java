package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_register_payments;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_register_payments.Account_register_paymentsCreate_dateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_register_payments
 * 属性：Create_date
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_register_paymentsCreate_dateDefaultValidator.class})
public @interface Account_register_paymentsCreate_dateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

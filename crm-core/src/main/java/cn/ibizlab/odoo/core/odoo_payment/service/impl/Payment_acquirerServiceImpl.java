package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_payment.client.payment_acquirerFeignClient;

/**
 * 实体[付款收单方] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_acquirerServiceImpl implements IPayment_acquirerService {

    @Autowired
    payment_acquirerFeignClient payment_acquirerFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=payment_acquirerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        payment_acquirerFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Payment_acquirer et) {
        Payment_acquirer rt = payment_acquirerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Payment_acquirer> list){
        payment_acquirerFeignClient.updateBatch(list) ;
    }

    @Override
    public Payment_acquirer get(Integer id) {
		Payment_acquirer et=payment_acquirerFeignClient.get(id);
        if(et==null){
            et=new Payment_acquirer();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Payment_acquirer getDraft(Payment_acquirer et) {
        et=payment_acquirerFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Payment_acquirer et) {
        Payment_acquirer rt = payment_acquirerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_acquirer> list){
        payment_acquirerFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context) {
        Page<Payment_acquirer> payment_acquirers=payment_acquirerFeignClient.searchDefault(context);
        return payment_acquirers;
    }


}



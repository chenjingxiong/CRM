package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-immediate-transfer", fallback = stock_immediate_transferFallback.class)
public interface stock_immediate_transferFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers")
    Stock_immediate_transfer create(@RequestBody Stock_immediate_transfer stock_immediate_transfer);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/batch")
    Boolean createBatch(@RequestBody List<Stock_immediate_transfer> stock_immediate_transfers);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_immediate_transfers/{id}")
    Stock_immediate_transfer update(@PathVariable("id") Integer id,@RequestBody Stock_immediate_transfer stock_immediate_transfer);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_immediate_transfers/batch")
    Boolean updateBatch(@RequestBody List<Stock_immediate_transfer> stock_immediate_transfers);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_immediate_transfers/{id}")
    Stock_immediate_transfer get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_immediate_transfers/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_immediate_transfers/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/searchdefault")
    Page<Stock_immediate_transfer> searchDefault(@RequestBody Stock_immediate_transferSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_immediate_transfers/select")
    Page<Stock_immediate_transfer> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_immediate_transfers/getdraft")
    Stock_immediate_transfer getDraft();


}

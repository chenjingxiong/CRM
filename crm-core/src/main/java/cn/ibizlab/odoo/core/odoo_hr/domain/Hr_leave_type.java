package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [休假类型] 对象
 */
@Data
public class Hr_leave_type extends EntityClient implements Serializable {

    /**
     * 有效
     */
    @JSONField(name = "valid")
    @JsonProperty("valid")
    private String valid;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 虚拟剩余休假
     */
    @JSONField(name = "virtual_remaining_leaves")
    @JsonProperty("virtual_remaining_leaves")
    private Double virtualRemainingLeaves;

    /**
     * 休假早已使用
     */
    @JSONField(name = "leaves_taken")
    @JsonProperty("leaves_taken")
    private Double leavesTaken;

    /**
     * 分配天数
     */
    @JSONField(name = "group_days_allocation")
    @JsonProperty("group_days_allocation")
    private Double groupDaysAllocation;

    /**
     * 应用双重验证
     */
    @JSONField(name = "double_validation")
    @JsonProperty("double_validation")
    private String doubleValidation;

    /**
     * 模式
     */
    @DEField(name = "allocation_type")
    @JSONField(name = "allocation_type")
    @JsonProperty("allocation_type")
    private String allocationType;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 没付款
     */
    @JSONField(name = "unpaid")
    @JsonProperty("unpaid")
    private String unpaid;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最大允许
     */
    @JSONField(name = "max_leaves")
    @JsonProperty("max_leaves")
    private Double maxLeaves;

    /**
     * 结束日期
     */
    @DEField(name = "validity_stop")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_stop" , format="yyyy-MM-dd")
    @JsonProperty("validity_stop")
    private Timestamp validityStop;

    /**
     * 验证人
     */
    @DEField(name = "validation_type")
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    private String validationType;

    /**
     * 请假
     */
    @DEField(name = "time_type")
    @JSONField(name = "time_type")
    @JsonProperty("time_type")
    private String timeType;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 休假
     */
    @DEField(name = "request_unit")
    @JSONField(name = "request_unit")
    @JsonProperty("request_unit")
    private String requestUnit;

    /**
     * 集团假期
     */
    @JSONField(name = "group_days_leave")
    @JsonProperty("group_days_leave")
    private Double groupDaysLeave;

    /**
     * 报表中的颜色
     */
    @DEField(name = "color_name")
    @JSONField(name = "color_name")
    @JsonProperty("color_name")
    private String colorName;

    /**
     * 剩余休假
     */
    @JSONField(name = "remaining_leaves")
    @JsonProperty("remaining_leaves")
    private Double remainingLeaves;

    /**
     * 休假类型
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 开始日期
     */
    @DEField(name = "validity_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_start" , format="yyyy-MM-dd")
    @JsonProperty("validity_start")
    private Timestamp validityStart;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 会议类型
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 会议类型
     */
    @DEField(name = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocateg")
    @JsonProperty("odoocateg")
    private cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type odooCateg;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [模式]
     */
    public void setAllocationType(String allocationType){
        this.allocationType = allocationType ;
        this.modify("allocation_type",allocationType);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [没付款]
     */
    public void setUnpaid(String unpaid){
        this.unpaid = unpaid ;
        this.modify("unpaid",unpaid);
    }
    /**
     * 设置 [结束日期]
     */
    public void setValidityStop(Timestamp validityStop){
        this.validityStop = validityStop ;
        this.modify("validity_stop",validityStop);
    }
    /**
     * 设置 [验证人]
     */
    public void setValidationType(String validationType){
        this.validationType = validationType ;
        this.modify("validation_type",validationType);
    }
    /**
     * 设置 [请假]
     */
    public void setTimeType(String timeType){
        this.timeType = timeType ;
        this.modify("time_type",timeType);
    }
    /**
     * 设置 [休假]
     */
    public void setRequestUnit(String requestUnit){
        this.requestUnit = requestUnit ;
        this.modify("request_unit",requestUnit);
    }
    /**
     * 设置 [报表中的颜色]
     */
    public void setColorName(String colorName){
        this.colorName = colorName ;
        this.modify("color_name",colorName);
    }
    /**
     * 设置 [休假类型]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [开始日期]
     */
    public void setValidityStart(Timestamp validityStart){
        this.validityStart = validityStart ;
        this.modify("validity_start",validityStart);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [会议类型]
     */
    public void setCategId(Integer categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

}



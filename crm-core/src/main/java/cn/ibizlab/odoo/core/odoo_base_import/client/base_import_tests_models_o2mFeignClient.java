package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2mSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_o2m] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-o2m", fallback = base_import_tests_models_o2mFallback.class)
public interface base_import_tests_models_o2mFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/{id}")
    Base_import_tests_models_o2m update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_o2m base_import_tests_models_o2m);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_o2m> base_import_tests_models_o2ms);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/{id}")
    Base_import_tests_models_o2m get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/searchdefault")
    Page<Base_import_tests_models_o2m> searchDefault(@RequestBody Base_import_tests_models_o2mSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms")
    Base_import_tests_models_o2m create(@RequestBody Base_import_tests_models_o2m base_import_tests_models_o2m);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_o2m> base_import_tests_models_o2ms);





    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/select")
    Page<Base_import_tests_models_o2m> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/getdraft")
    Base_import_tests_models_o2m getDraft();


}

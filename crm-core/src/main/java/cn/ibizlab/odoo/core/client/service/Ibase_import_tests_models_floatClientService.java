package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_float;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
public interface Ibase_import_tests_models_floatClientService{

    public Ibase_import_tests_models_float createModel() ;

    public void create(Ibase_import_tests_models_float base_import_tests_models_float);

    public void remove(Ibase_import_tests_models_float base_import_tests_models_float);

    public void get(Ibase_import_tests_models_float base_import_tests_models_float);

    public void createBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats);

    public void removeBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats);

    public void update(Ibase_import_tests_models_float base_import_tests_models_float);

    public void updateBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats);

    public Page<Ibase_import_tests_models_float> fetchDefault(SearchContext context);

    public Page<Ibase_import_tests_models_float> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_float base_import_tests_models_float);

}

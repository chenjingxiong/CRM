package cn.ibizlab.odoo.core.odoo_im_livechat.valuerule.anno.im_livechat_channel_rule;

import cn.ibizlab.odoo.core.odoo_im_livechat.valuerule.validator.im_livechat_channel_rule.Im_livechat_channel_ruleWrite_dateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Im_livechat_channel_rule
 * 属性：Write_date
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Im_livechat_channel_ruleWrite_dateDefaultValidator.class})
public @interface Im_livechat_channel_ruleWrite_dateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_payment_method;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment_method] 服务对象接口
 */
public interface Iaccount_payment_methodClientService{

    public Iaccount_payment_method createModel() ;

    public void get(Iaccount_payment_method account_payment_method);

    public void updateBatch(List<Iaccount_payment_method> account_payment_methods);

    public void remove(Iaccount_payment_method account_payment_method);

    public void create(Iaccount_payment_method account_payment_method);

    public void removeBatch(List<Iaccount_payment_method> account_payment_methods);

    public Page<Iaccount_payment_method> fetchDefault(SearchContext context);

    public void update(Iaccount_payment_method account_payment_method);

    public void createBatch(List<Iaccount_payment_method> account_payment_methods);

    public Page<Iaccount_payment_method> select(SearchContext context);

    public void getDraft(Iaccount_payment_method account_payment_method);

}

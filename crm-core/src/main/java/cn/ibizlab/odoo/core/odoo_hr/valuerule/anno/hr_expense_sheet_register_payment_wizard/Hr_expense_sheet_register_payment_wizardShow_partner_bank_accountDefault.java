package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_expense_sheet_register_payment_wizard;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_expense_sheet_register_payment_wizard.Hr_expense_sheet_register_payment_wizardShow_partner_bank_accountDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_expense_sheet_register_payment_wizard
 * 属性：Show_partner_bank_account
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_expense_sheet_register_payment_wizardShow_partner_bank_accountDefaultValidator.class})
public @interface Hr_expense_sheet_register_payment_wizardShow_partner_bank_accountDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

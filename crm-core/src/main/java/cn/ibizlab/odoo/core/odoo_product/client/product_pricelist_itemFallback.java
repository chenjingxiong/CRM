package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_pricelist_item] 服务对象接口
 */
@Component
public class product_pricelist_itemFallback implements product_pricelist_itemFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Product_pricelist_item update(Integer id, Product_pricelist_item product_pricelist_item){
            return null;
     }
    public Boolean updateBatch(List<Product_pricelist_item> product_pricelist_items){
            return false;
     }


    public Product_pricelist_item get(Integer id){
            return null;
     }


    public Product_pricelist_item create(Product_pricelist_item product_pricelist_item){
            return null;
     }
    public Boolean createBatch(List<Product_pricelist_item> product_pricelist_items){
            return false;
     }


    public Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context){
            return null;
     }



    public Page<Product_pricelist_item> select(){
            return null;
     }

    public Product_pricelist_item getDraft(){
            return null;
    }



    public Boolean checkKey(Product_pricelist_item product_pricelist_item){
            return false;
     }


    public Boolean save(Product_pricelist_item product_pricelist_item){
            return false;
     }
    public Boolean saveBatch(List<Product_pricelist_item> product_pricelist_items){
            return false;
     }

}

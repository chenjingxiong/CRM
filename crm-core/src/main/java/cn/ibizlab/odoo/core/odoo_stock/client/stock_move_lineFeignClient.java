package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_move_line] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-move-line", fallback = stock_move_lineFallback.class)
public interface stock_move_lineFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_move_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_move_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines")
    Stock_move_line create(@RequestBody Stock_move_line stock_move_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/batch")
    Boolean createBatch(@RequestBody List<Stock_move_line> stock_move_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_move_lines/searchdefault")
    Page<Stock_move_line> searchDefault(@RequestBody Stock_move_lineSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_move_lines/{id}")
    Stock_move_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_move_lines/{id}")
    Stock_move_line update(@PathVariable("id") Integer id,@RequestBody Stock_move_line stock_move_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_move_lines/batch")
    Boolean updateBatch(@RequestBody List<Stock_move_line> stock_move_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_move_lines/select")
    Page<Stock_move_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_move_lines/getdraft")
    Stock_move_line getDraft();


}

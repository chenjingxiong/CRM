package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_report] 对象
 */
public interface Isale_report {

    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id(Integer analytic_account_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAnalytic_account_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id_text(String analytic_account_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAnalytic_account_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_id_textDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id(Integer categ_id);
    
    /**
     * 设置 [产品种类]
     */
    public Integer getCateg_id();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_idDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id_text(String categ_id_text);
    
    /**
     * 设置 [产品种类]
     */
    public String getCateg_id_text();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_id_textDirtyFlag();
    /**
     * 获取 [客户实体]
     */
    public void setCommercial_partner_id(Integer commercial_partner_id);
    
    /**
     * 设置 [客户实体]
     */
    public Integer getCommercial_partner_id();

    /**
     * 获取 [客户实体]脏标记
     */
    public boolean getCommercial_partner_idDirtyFlag();
    /**
     * 获取 [客户实体]
     */
    public void setCommercial_partner_id_text(String commercial_partner_id_text);
    
    /**
     * 设置 [客户实体]
     */
    public String getCommercial_partner_id_text();

    /**
     * 获取 [客户实体]脏标记
     */
    public boolean getCommercial_partner_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [确认日期]
     */
    public void setConfirmation_date(Timestamp confirmation_date);
    
    /**
     * 设置 [确认日期]
     */
    public Timestamp getConfirmation_date();

    /**
     * 获取 [确认日期]脏标记
     */
    public boolean getConfirmation_dateDirtyFlag();
    /**
     * 获取 [客户国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [客户国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [客户国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [客户国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [客户国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [客户国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [单据日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [单据日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [单据日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [折扣 %]
     */
    public void setDiscount(Double discount);
    
    /**
     * 设置 [折扣 %]
     */
    public Double getDiscount();

    /**
     * 获取 [折扣 %]脏标记
     */
    public boolean getDiscountDirtyFlag();
    /**
     * 获取 [折扣金额]
     */
    public void setDiscount_amount(Double discount_amount);
    
    /**
     * 设置 [折扣金额]
     */
    public Double getDiscount_amount();

    /**
     * 获取 [折扣金额]脏标记
     */
    public boolean getDiscount_amountDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setName(String name);
    
    /**
     * 设置 [订单关联]
     */
    public String getName();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [# 明细行]
     */
    public void setNbr(Integer nbr);
    
    /**
     * 设置 [# 明细行]
     */
    public Integer getNbr();

    /**
     * 获取 [# 明细行]脏标记
     */
    public boolean getNbrDirtyFlag();
    /**
     * 获取 [订单 #]
     */
    public void setOrder_id(Integer order_id);
    
    /**
     * 设置 [订单 #]
     */
    public Integer getOrder_id();

    /**
     * 获取 [订单 #]脏标记
     */
    public boolean getOrder_idDirtyFlag();
    /**
     * 获取 [订单 #]
     */
    public void setOrder_id_text(String order_id_text);
    
    /**
     * 设置 [订单 #]
     */
    public String getOrder_id_text();

    /**
     * 获取 [订单 #]脏标记
     */
    public boolean getOrder_id_textDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id(Integer pricelist_id);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPricelist_id();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_idDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id_text(String pricelist_id_text);
    
    /**
     * 设置 [价格表]
     */
    public String getPricelist_id_text();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_id_textDirtyFlag();
    /**
     * 获取 [不含税总计]
     */
    public void setPrice_subtotal(Double price_subtotal);
    
    /**
     * 设置 [不含税总计]
     */
    public Double getPrice_subtotal();

    /**
     * 获取 [不含税总计]脏标记
     */
    public boolean getPrice_subtotalDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setPrice_total(Double price_total);
    
    /**
     * 设置 [总计]
     */
    public Double getPrice_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getPrice_totalDirtyFlag();
    /**
     * 获取 [产品变体]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品变体]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品变体]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品变体]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品变体]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品变体]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [订购数量]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [订购数量]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [订购数量]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [已送货数量]
     */
    public void setQty_delivered(Double qty_delivered);
    
    /**
     * 设置 [已送货数量]
     */
    public Double getQty_delivered();

    /**
     * 获取 [已送货数量]脏标记
     */
    public boolean getQty_deliveredDirtyFlag();
    /**
     * 获取 [已开票数量]
     */
    public void setQty_invoiced(Double qty_invoiced);
    
    /**
     * 设置 [已开票数量]
     */
    public Double getQty_invoiced();

    /**
     * 获取 [已开票数量]脏标记
     */
    public boolean getQty_invoicedDirtyFlag();
    /**
     * 获取 [待开票数量]
     */
    public void setQty_to_invoice(Double qty_to_invoice);
    
    /**
     * 设置 [待开票数量]
     */
    public Double getQty_to_invoice();

    /**
     * 获取 [待开票数量]脏标记
     */
    public boolean getQty_to_invoiceDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [不含税已开票金额]
     */
    public void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);
    
    /**
     * 设置 [不含税已开票金额]
     */
    public Double getUntaxed_amount_invoiced();

    /**
     * 获取 [不含税已开票金额]脏标记
     */
    public boolean getUntaxed_amount_invoicedDirtyFlag();
    /**
     * 获取 [不含税待开票金额]
     */
    public void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);
    
    /**
     * 设置 [不含税待开票金额]
     */
    public Double getUntaxed_amount_to_invoice();

    /**
     * 获取 [不含税待开票金额]脏标记
     */
    public boolean getUntaxed_amount_to_invoiceDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [体积]
     */
    public void setVolume(Double volume);
    
    /**
     * 设置 [体积]
     */
    public Double getVolume();

    /**
     * 获取 [体积]脏标记
     */
    public boolean getVolumeDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id_text(String warehouse_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_id_textDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [毛重]
     */
    public void setWeight(Double weight);
    
    /**
     * 设置 [毛重]
     */
    public Double getWeight();

    /**
     * 获取 [毛重]脏标记
     */
    public boolean getWeightDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

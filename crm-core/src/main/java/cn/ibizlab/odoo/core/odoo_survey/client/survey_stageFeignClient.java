package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_stage] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-stage", fallback = survey_stageFallback.class)
public interface survey_stageFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/survey_stages")
    Survey_stage create(@RequestBody Survey_stage survey_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_stages/batch")
    Boolean createBatch(@RequestBody List<Survey_stage> survey_stages);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_stages/searchdefault")
    Page<Survey_stage> searchDefault(@RequestBody Survey_stageSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/{id}")
    Survey_stage update(@PathVariable("id") Integer id,@RequestBody Survey_stage survey_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/batch")
    Boolean updateBatch(@RequestBody List<Survey_stage> survey_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_stages/{id}")
    Survey_stage get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_stages/select")
    Page<Survey_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_stages/getdraft")
    Survey_stage getDraft();


}

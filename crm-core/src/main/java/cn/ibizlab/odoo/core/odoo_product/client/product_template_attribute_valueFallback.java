package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_valueSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_template_attribute_value] 服务对象接口
 */
@Component
public class product_template_attribute_valueFallback implements product_template_attribute_valueFeignClient{



    public Product_template_attribute_value get(Integer id){
            return null;
     }



    public Product_template_attribute_value create(Product_template_attribute_value product_template_attribute_value){
            return null;
     }
    public Boolean createBatch(List<Product_template_attribute_value> product_template_attribute_values){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Product_template_attribute_value> searchDefault(Product_template_attribute_valueSearchContext context){
            return null;
     }


    public Product_template_attribute_value update(Integer id, Product_template_attribute_value product_template_attribute_value){
            return null;
     }
    public Boolean updateBatch(List<Product_template_attribute_value> product_template_attribute_values){
            return false;
     }


    public Page<Product_template_attribute_value> select(){
            return null;
     }

    public Product_template_attribute_value getDraft(){
            return null;
    }



}

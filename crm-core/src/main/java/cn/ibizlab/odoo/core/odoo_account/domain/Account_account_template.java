package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [科目模板] 对象
 */
@Data
public class Account_account_template extends EntityClient implements Serializable {

    /**
     * 代码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 科目标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 选项创建
     */
    @JSONField(name = "nocreate")
    @JsonProperty("nocreate")
    private String nocreate;

    /**
     * 默认税
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 备注
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 允许发票和付款匹配
     */
    @JSONField(name = "reconcile")
    @JsonProperty("reconcile")
    private String reconcile;

    /**
     * 表模板
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 科目币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 类型
     */
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    private String userTypeIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 组
     */
    @JSONField(name = "group_id_text")
    @JsonProperty("group_id_text")
    private String groupIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 科目币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 类型
     */
    @DEField(name = "user_type_id")
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    private Integer userTypeId;

    /**
     * 组
     */
    @DEField(name = "group_id")
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;


    /**
     * 
     */
    @JSONField(name = "odoousertype")
    @JsonProperty("odoousertype")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type odooUserType;

    /**
     * 
     */
    @JSONField(name = "odoocharttemplate")
    @JsonProperty("odoocharttemplate")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JSONField(name = "odoogroup")
    @JsonProperty("odoogroup")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_group odooGroup;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [选项创建]
     */
    public void setNocreate(String nocreate){
        this.nocreate = nocreate ;
        this.modify("nocreate",nocreate);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [允许发票和付款匹配]
     */
    public void setReconcile(String reconcile){
        this.reconcile = reconcile ;
        this.modify("reconcile",reconcile);
    }
    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Integer chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }
    /**
     * 设置 [科目币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [类型]
     */
    public void setUserTypeId(Integer userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }
    /**
     * 设置 [组]
     */
    public void setGroupId(Integer groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

}



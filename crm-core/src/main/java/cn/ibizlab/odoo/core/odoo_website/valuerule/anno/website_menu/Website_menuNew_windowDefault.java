package cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_menu;

import cn.ibizlab.odoo.core.odoo_website.valuerule.validator.website_menu.Website_menuNew_windowDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Website_menu
 * 属性：New_window
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Website_menuNew_windowDefaultValidator.class})
public @interface Website_menuNew_windowDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

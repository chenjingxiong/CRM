package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_tax_group] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-tax-group", fallback = account_tax_groupFallback.class)
public interface account_tax_groupFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups")
    Account_tax_group create(@RequestBody Account_tax_group account_tax_group);

    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/batch")
    Boolean createBatch(@RequestBody List<Account_tax_group> account_tax_groups);



    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/searchdefault")
    Page<Account_tax_group> searchDefault(@RequestBody Account_tax_groupSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/{id}")
    Account_tax_group get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/{id}")
    Account_tax_group update(@PathVariable("id") Integer id,@RequestBody Account_tax_group account_tax_group);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/batch")
    Boolean updateBatch(@RequestBody List<Account_tax_group> account_tax_groups);



    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/select")
    Page<Account_tax_group> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/getdraft")
    Account_tax_group getDraft();


}

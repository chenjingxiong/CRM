package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_expense_sheet_register_payment_wizard] 对象
 */
public interface Ihr_expense_sheet_register_payment_wizard {

    /**
     * 获取 [付款金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [付款金额]
     */
    public Double getAmount();

    /**
     * 获取 [付款金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [备忘]
     */
    public void setCommunication(String communication);
    
    /**
     * 设置 [备忘]
     */
    public String getCommunication();

    /**
     * 获取 [备忘]脏标记
     */
    public boolean getCommunicationDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [隐藏付款方式]
     */
    public void setHide_payment_method(String hide_payment_method);
    
    /**
     * 设置 [隐藏付款方式]
     */
    public String getHide_payment_method();

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    public boolean getHide_payment_methodDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [付款方法]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [付款方法]
     */
    public Integer getJournal_id();

    /**
     * 获取 [付款方法]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [付款方法]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [付款方法]
     */
    public String getJournal_id_text();

    /**
     * 获取 [付款方法]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [收款银行账号]
     */
    public void setPartner_bank_account_id(Integer partner_bank_account_id);
    
    /**
     * 设置 [收款银行账号]
     */
    public Integer getPartner_bank_account_id();

    /**
     * 获取 [收款银行账号]脏标记
     */
    public boolean getPartner_bank_account_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [付款日期]
     */
    public void setPayment_date(Timestamp payment_date);
    
    /**
     * 设置 [付款日期]
     */
    public Timestamp getPayment_date();

    /**
     * 获取 [付款日期]脏标记
     */
    public boolean getPayment_dateDirtyFlag();
    /**
     * 获取 [付款类型]
     */
    public void setPayment_method_id(Integer payment_method_id);
    
    /**
     * 设置 [付款类型]
     */
    public Integer getPayment_method_id();

    /**
     * 获取 [付款类型]脏标记
     */
    public boolean getPayment_method_idDirtyFlag();
    /**
     * 获取 [付款类型]
     */
    public void setPayment_method_id_text(String payment_method_id_text);
    
    /**
     * 设置 [付款类型]
     */
    public String getPayment_method_id_text();

    /**
     * 获取 [付款类型]脏标记
     */
    public boolean getPayment_method_id_textDirtyFlag();
    /**
     * 获取 [显示合作伙伴银行账户]
     */
    public void setShow_partner_bank_account(String show_partner_bank_account);
    
    /**
     * 设置 [显示合作伙伴银行账户]
     */
    public String getShow_partner_bank_account();

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    public boolean getShow_partner_bank_accountDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

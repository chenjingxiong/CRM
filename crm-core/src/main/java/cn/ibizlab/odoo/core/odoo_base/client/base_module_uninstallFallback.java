package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_module_uninstall] 服务对象接口
 */
@Component
public class base_module_uninstallFallback implements base_module_uninstallFeignClient{

    public Base_module_uninstall update(Integer id, Base_module_uninstall base_module_uninstall){
            return null;
     }
    public Boolean updateBatch(List<Base_module_uninstall> base_module_uninstalls){
            return false;
     }




    public Base_module_uninstall get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_module_uninstall create(Base_module_uninstall base_module_uninstall){
            return null;
     }
    public Boolean createBatch(List<Base_module_uninstall> base_module_uninstalls){
            return false;
     }

    public Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context){
            return null;
     }


    public Page<Base_module_uninstall> select(){
            return null;
     }

    public Base_module_uninstall getDraft(){
            return null;
    }



}

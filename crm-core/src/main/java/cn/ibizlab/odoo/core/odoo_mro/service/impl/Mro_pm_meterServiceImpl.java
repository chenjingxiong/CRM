package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meterFeignClient;

/**
 * 实体[Asset Meters] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meterServiceImpl implements IMro_pm_meterService {

    @Autowired
    mro_pm_meterFeignClient mro_pm_meterFeignClient;


    @Override
    public boolean update(Mro_pm_meter et) {
        Mro_pm_meter rt = mro_pm_meterFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_meter> list){
        mro_pm_meterFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_meterFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_meterFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_pm_meter get(Integer id) {
		Mro_pm_meter et=mro_pm_meterFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_meter();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mro_pm_meter getDraft(Mro_pm_meter et) {
        et=mro_pm_meterFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mro_pm_meter et) {
        Mro_pm_meter rt = mro_pm_meterFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter> list){
        mro_pm_meterFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context) {
        Page<Mro_pm_meter> mro_pm_meters=mro_pm_meterFeignClient.searchDefault(context);
        return mro_pm_meters;
    }


}



package cn.ibizlab.odoo.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Res_users] 查询条件对象
 */
@Slf4j
@Data
public class Res_usersSearchContext extends SearchContextBase {
	private String n_notification_type_eq;//[通知管理]

	private String n_state_eq;//[状态]

	private String n_odoobot_state_eq;//[OdooBot 状态]

	private String n_sale_team_id_text_eq;//[用户的销售团队]

	private String n_sale_team_id_text_like;//[用户的销售团队]

	private String n_write_uid_text_eq;//[最后更新者]

	private String n_write_uid_text_like;//[最后更新者]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_name_eq;//[名称]

	private String n_name_like;//[名称]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private Integer n_company_id_eq;//[公司]

	private Integer n_partner_id_eq;//[相关的业务伙伴]

	private Integer n_alias_id_eq;//[别名]

	private Integer n_sale_team_id_eq;//[用户的销售团队]

	private Integer n_write_uid_eq;//[最后更新者]

	private Integer n_create_uid_eq;//[创建人]

}




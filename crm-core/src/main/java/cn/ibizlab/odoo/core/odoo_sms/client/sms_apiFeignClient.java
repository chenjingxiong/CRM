package cn.ibizlab.odoo.core.odoo_sms.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sms_api] 服务对象接口
 */
@FeignClient(value = "odoo-sms", contextId = "sms-api", fallback = sms_apiFallback.class)
public interface sms_apiFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/sms_apis")
    Sms_api create(@RequestBody Sms_api sms_api);

    @RequestMapping(method = RequestMethod.POST, value = "/sms_apis/batch")
    Boolean createBatch(@RequestBody List<Sms_api> sms_apis);





    @RequestMapping(method = RequestMethod.POST, value = "/sms_apis/searchdefault")
    Page<Sms_api> searchDefault(@RequestBody Sms_apiSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/sms_apis/{id}")
    Sms_api get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/{id}")
    Sms_api update(@PathVariable("id") Integer id,@RequestBody Sms_api sms_api);

    @RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/batch")
    Boolean updateBatch(@RequestBody List<Sms_api> sms_apis);



    @RequestMapping(method = RequestMethod.GET, value = "/sms_apis/select")
    Page<Sms_api> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sms_apis/getdraft")
    Sms_api getDraft();


}

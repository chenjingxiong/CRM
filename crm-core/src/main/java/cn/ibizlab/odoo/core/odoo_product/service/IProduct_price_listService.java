package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;


/**
 * 实体[Product_price_list] 服务对象接口
 */
public interface IProduct_price_listService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Product_price_list et) ;
    void updateBatch(List<Product_price_list> list) ;
    boolean create(Product_price_list et) ;
    void createBatch(List<Product_price_list> list) ;
    Product_price_list get(Integer key) ;
    Product_price_list getDraft(Product_price_list et) ;
    Page<Product_price_list> searchDefault(Product_price_listSearchContext context) ;

}




package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_event_ticket] 服务对象接口
 */
public interface Ievent_event_ticketClientService{

    public Ievent_event_ticket createModel() ;

    public void create(Ievent_event_ticket event_event_ticket);

    public void get(Ievent_event_ticket event_event_ticket);

    public void update(Ievent_event_ticket event_event_ticket);

    public void removeBatch(List<Ievent_event_ticket> event_event_tickets);

    public Page<Ievent_event_ticket> fetchDefault(SearchContext context);

    public void remove(Ievent_event_ticket event_event_ticket);

    public void createBatch(List<Ievent_event_ticket> event_event_tickets);

    public void updateBatch(List<Ievent_event_ticket> event_event_tickets);

    public Page<Ievent_event_ticket> select(SearchContext context);

    public void getDraft(Ievent_event_ticket event_event_ticket);

}

package cn.ibizlab.odoo.core.odoo_rating.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_ratingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_rating.client.rating_ratingFeignClient;

/**
 * 实体[评级] 服务对象接口实现
 */
@Slf4j
@Service
public class Rating_ratingServiceImpl implements IRating_ratingService {

    @Autowired
    rating_ratingFeignClient rating_ratingFeignClient;


    @Override
    public boolean update(Rating_rating et) {
        Rating_rating rt = rating_ratingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Rating_rating> list){
        rating_ratingFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Rating_rating et) {
        Rating_rating rt = rating_ratingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Rating_rating> list){
        rating_ratingFeignClient.createBatch(list) ;
    }

    @Override
    public Rating_rating get(Integer id) {
		Rating_rating et=rating_ratingFeignClient.get(id);
        if(et==null){
            et=new Rating_rating();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Rating_rating getDraft(Rating_rating et) {
        et=rating_ratingFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=rating_ratingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        rating_ratingFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Rating_rating> searchDefault(Rating_ratingSearchContext context) {
        Page<Rating_rating> rating_ratings=rating_ratingFeignClient.searchDefault(context);
        return rating_ratings;
    }


}



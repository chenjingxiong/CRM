package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_template] 对象
 */
public interface Imail_template {

    /**
     * 获取 [附件]
     */
    public void setAttachment_ids(String attachment_ids);
    
    /**
     * 设置 [附件]
     */
    public String getAttachment_ids();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getAttachment_idsDirtyFlag();
    /**
     * 获取 [自动删除]
     */
    public void setAuto_delete(String auto_delete);
    
    /**
     * 设置 [自动删除]
     */
    public String getAuto_delete();

    /**
     * 获取 [自动删除]脏标记
     */
    public boolean getAuto_deleteDirtyFlag();
    /**
     * 获取 [正文]
     */
    public void setBody_html(String body_html);
    
    /**
     * 设置 [正文]
     */
    public String getBody_html();

    /**
     * 获取 [正文]脏标记
     */
    public boolean getBody_htmlDirtyFlag();
    /**
     * 获取 [占位符表达式]
     */
    public void setCopyvalue(String copyvalue);
    
    /**
     * 设置 [占位符表达式]
     */
    public String getCopyvalue();

    /**
     * 获取 [占位符表达式]脏标记
     */
    public boolean getCopyvalueDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [抄送]
     */
    public void setEmail_cc(String email_cc);
    
    /**
     * 设置 [抄送]
     */
    public String getEmail_cc();

    /**
     * 获取 [抄送]脏标记
     */
    public boolean getEmail_ccDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [从]
     */
    public String getEmail_from();

    /**
     * 获取 [从]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [至（EMail）]
     */
    public void setEmail_to(String email_to);
    
    /**
     * 设置 [至（EMail）]
     */
    public String getEmail_to();

    /**
     * 获取 [至（EMail）]脏标记
     */
    public boolean getEmail_toDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [语言]
     */
    public void setLang(String lang);
    
    /**
     * 设置 [语言]
     */
    public String getLang();

    /**
     * 获取 [语言]脏标记
     */
    public boolean getLangDirtyFlag();
    /**
     * 获取 [邮件发送服务器]
     */
    public void setMail_server_id(Integer mail_server_id);
    
    /**
     * 设置 [邮件发送服务器]
     */
    public Integer getMail_server_id();

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    public boolean getMail_server_idDirtyFlag();
    /**
     * 获取 [相关的文档模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [相关的文档模型]
     */
    public String getModel();

    /**
     * 获取 [相关的文档模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [应用于]
     */
    public void setModel_id(Integer model_id);
    
    /**
     * 设置 [应用于]
     */
    public Integer getModel_id();

    /**
     * 获取 [应用于]脏标记
     */
    public boolean getModel_idDirtyFlag();
    /**
     * 获取 [字段]
     */
    public void setModel_object_field(Integer model_object_field);
    
    /**
     * 设置 [字段]
     */
    public Integer getModel_object_field();

    /**
     * 获取 [字段]脏标记
     */
    public boolean getModel_object_fieldDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setNull_value(String null_value);
    
    /**
     * 设置 [默认值]
     */
    public String getNull_value();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getNull_valueDirtyFlag();
    /**
     * 获取 [至（合作伙伴）]
     */
    public void setPartner_to(String partner_to);
    
    /**
     * 设置 [至（合作伙伴）]
     */
    public String getPartner_to();

    /**
     * 获取 [至（合作伙伴）]脏标记
     */
    public boolean getPartner_toDirtyFlag();
    /**
     * 获取 [边栏操作]
     */
    public void setRef_ir_act_window(Integer ref_ir_act_window);
    
    /**
     * 设置 [边栏操作]
     */
    public Integer getRef_ir_act_window();

    /**
     * 获取 [边栏操作]脏标记
     */
    public boolean getRef_ir_act_windowDirtyFlag();
    /**
     * 获取 [回复 至]
     */
    public void setReply_to(String reply_to);
    
    /**
     * 设置 [回复 至]
     */
    public String getReply_to();

    /**
     * 获取 [回复 至]脏标记
     */
    public boolean getReply_toDirtyFlag();
    /**
     * 获取 [报告文件名]
     */
    public void setReport_name(String report_name);
    
    /**
     * 设置 [报告文件名]
     */
    public String getReport_name();

    /**
     * 获取 [报告文件名]脏标记
     */
    public boolean getReport_nameDirtyFlag();
    /**
     * 获取 [可选的打印和附加报表]
     */
    public void setReport_template(Integer report_template);
    
    /**
     * 设置 [可选的打印和附加报表]
     */
    public Integer getReport_template();

    /**
     * 获取 [可选的打印和附加报表]脏标记
     */
    public boolean getReport_templateDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setScheduled_date(String scheduled_date);
    
    /**
     * 设置 [计划日期]
     */
    public String getScheduled_date();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getScheduled_dateDirtyFlag();
    /**
     * 获取 [主题]
     */
    public void setSubject(String subject);
    
    /**
     * 设置 [主题]
     */
    public String getSubject();

    /**
     * 获取 [主题]脏标记
     */
    public boolean getSubjectDirtyFlag();
    /**
     * 获取 [子字段]
     */
    public void setSub_model_object_field(Integer sub_model_object_field);
    
    /**
     * 设置 [子字段]
     */
    public Integer getSub_model_object_field();

    /**
     * 获取 [子字段]脏标记
     */
    public boolean getSub_model_object_fieldDirtyFlag();
    /**
     * 获取 [子模型]
     */
    public void setSub_object(Integer sub_object);
    
    /**
     * 设置 [子模型]
     */
    public Integer getSub_object();

    /**
     * 获取 [子模型]脏标记
     */
    public boolean getSub_objectDirtyFlag();
    /**
     * 获取 [添加签名]
     */
    public void setUser_signature(String user_signature);
    
    /**
     * 设置 [添加签名]
     */
    public String getUser_signature();

    /**
     * 获取 [添加签名]脏标记
     */
    public boolean getUser_signatureDirtyFlag();
    /**
     * 获取 [默认收件人]
     */
    public void setUse_default_to(String use_default_to);
    
    /**
     * 设置 [默认收件人]
     */
    public String getUse_default_to();

    /**
     * 获取 [默认收件人]脏标记
     */
    public boolean getUse_default_toDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

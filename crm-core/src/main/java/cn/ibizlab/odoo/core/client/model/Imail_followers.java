package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_followers] 对象
 */
public interface Imail_followers {

    /**
     * 获取 [监听器]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [监听器]
     */
    public Integer getChannel_id();

    /**
     * 获取 [监听器]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [监听器]
     */
    public void setChannel_id_text(String channel_id_text);
    
    /**
     * 设置 [监听器]
     */
    public String getChannel_id_text();

    /**
     * 获取 [监听器]脏标记
     */
    public boolean getChannel_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [相关的业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [相关的业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [相关的业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [相关的业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [相关文档编号]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [相关文档编号]
     */
    public Integer getRes_id();

    /**
     * 获取 [相关文档编号]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [相关的文档模型名称]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [相关的文档模型名称]
     */
    public String getRes_model();

    /**
     * 获取 [相关的文档模型名称]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [子类型]
     */
    public void setSubtype_ids(String subtype_ids);
    
    /**
     * 设置 [子类型]
     */
    public String getSubtype_ids();

    /**
     * 获取 [子类型]脏标记
     */
    public boolean getSubtype_idsDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

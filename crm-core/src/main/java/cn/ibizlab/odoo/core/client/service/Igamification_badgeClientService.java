package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_badge;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge] 服务对象接口
 */
public interface Igamification_badgeClientService{

    public Igamification_badge createModel() ;

    public void remove(Igamification_badge gamification_badge);

    public void get(Igamification_badge gamification_badge);

    public void removeBatch(List<Igamification_badge> gamification_badges);

    public void createBatch(List<Igamification_badge> gamification_badges);

    public void updateBatch(List<Igamification_badge> gamification_badges);

    public void update(Igamification_badge gamification_badge);

    public Page<Igamification_badge> fetchDefault(SearchContext context);

    public void create(Igamification_badge gamification_badge);

    public Page<Igamification_badge> select(SearchContext context);

    public void getDraft(Igamification_badge gamification_badge);

}

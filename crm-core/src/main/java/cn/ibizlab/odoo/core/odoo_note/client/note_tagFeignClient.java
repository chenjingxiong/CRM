package cn.ibizlab.odoo.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[note_tag] 服务对象接口
 */
@FeignClient(value = "odoo-note", contextId = "note-tag", fallback = note_tagFallback.class)
public interface note_tagFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/note_tags/{id}")
    Note_tag get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/note_tags")
    Note_tag create(@RequestBody Note_tag note_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/note_tags/batch")
    Boolean createBatch(@RequestBody List<Note_tag> note_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/note_tags/searchdefault")
    Page<Note_tag> searchDefault(@RequestBody Note_tagSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/note_tags/{id}")
    Note_tag update(@PathVariable("id") Integer id,@RequestBody Note_tag note_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/note_tags/batch")
    Boolean updateBatch(@RequestBody List<Note_tag> note_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/note_tags/select")
    Page<Note_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/note_tags/getdraft")
    Note_tag getDraft();


}

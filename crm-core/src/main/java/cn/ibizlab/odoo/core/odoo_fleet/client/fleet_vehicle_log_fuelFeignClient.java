package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
@FeignClient(value = "odoo-fleet", contextId = "fleet-vehicle-log-fuel", fallback = fleet_vehicle_log_fuelFallback.class)
public interface fleet_vehicle_log_fuelFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/{id}")
    Fleet_vehicle_log_fuel update(@PathVariable("id") Integer id,@RequestBody Fleet_vehicle_log_fuel fleet_vehicle_log_fuel);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_log_fuel> fleet_vehicle_log_fuels);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels")
    Fleet_vehicle_log_fuel create(@RequestBody Fleet_vehicle_log_fuel fleet_vehicle_log_fuel);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_log_fuel> fleet_vehicle_log_fuels);



    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/{id}")
    Fleet_vehicle_log_fuel get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/searchdefault")
    Page<Fleet_vehicle_log_fuel> searchDefault(@RequestBody Fleet_vehicle_log_fuelSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/select")
    Page<Fleet_vehicle_log_fuel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/getdraft")
    Fleet_vehicle_log_fuel getDraft();


}

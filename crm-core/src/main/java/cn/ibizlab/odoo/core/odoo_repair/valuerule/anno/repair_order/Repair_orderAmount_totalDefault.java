package cn.ibizlab.odoo.core.odoo_repair.valuerule.anno.repair_order;

import cn.ibizlab.odoo.core.odoo_repair.valuerule.validator.repair_order.Repair_orderAmount_totalDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Repair_order
 * 属性：Amount_total
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Repair_orderAmount_totalDefaultValidator.class})
public @interface Repair_orderAmount_totalDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

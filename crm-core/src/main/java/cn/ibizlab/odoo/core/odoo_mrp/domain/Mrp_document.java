package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [生产文档] 对象
 */
@Data
public class Mrp_document extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 资源字段
     */
    @JSONField(name = "res_field")
    @JsonProperty("res_field")
    private String resField;

    /**
     * 资源名称
     */
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;

    /**
     * RES型号名称
     */
    @JSONField(name = "res_model_name")
    @JsonProperty("res_model_name")
    private String resModelName;

    /**
     * 文件名
     */
    @JSONField(name = "datas_fname")
    @JsonProperty("datas_fname")
    private String datasFname;

    /**
     * 主题模板
     */
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;

    /**
     * MIME 类型
     */
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;

    /**
     * 资源ID
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 存储的文件名
     */
    @JSONField(name = "store_fname")
    @JsonProperty("store_fname")
    private String storeFname;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 附件网址
     */
    @JSONField(name = "local_url")
    @JsonProperty("local_url")
    private String localUrl;

    /**
     * 键
     */
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;

    /**
     * 是公开文档
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;

    /**
     * 资源模型
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 略所图
     */
    @JSONField(name = "thumbnail")
    @JsonProperty("thumbnail")
    private byte[] thumbnail;

    /**
     * Url网址
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;

    /**
     * 文件大小
     */
    @JSONField(name = "file_size")
    @JsonProperty("file_size")
    private Integer fileSize;

    /**
     * 访问令牌
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 公司
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 相关的附件
     */
    @DEField(name = "ir_attachment_id")
    @JSONField(name = "ir_attachment_id")
    @JsonProperty("ir_attachment_id")
    private Integer irAttachmentId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 校验和/SHA1
     */
    @JSONField(name = "checksum")
    @JsonProperty("checksum")
    private String checksum;

    /**
     * 数据库数据
     */
    @JSONField(name = "db_datas")
    @JsonProperty("db_datas")
    private byte[] dbDatas;

    /**
     * 索引的内容
     */
    @JSONField(name = "index_content")
    @JsonProperty("index_content")
    private String indexContent;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 网站
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 文件内容
     */
    @JSONField(name = "datas")
    @JsonProperty("datas")
    private byte[] datas;

    /**
     * 优先级
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [相关的附件]
     */
    public void setIrAttachmentId(Integer irAttachmentId){
        this.irAttachmentId = irAttachmentId ;
        this.modify("ir_attachment_id",irAttachmentId);
    }
    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

}



package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_user_input] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-user-input", fallback = survey_user_inputFallback.class)
public interface survey_user_inputFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/searchdefault")
    Page<Survey_user_input> searchDefault(@RequestBody Survey_user_inputSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs")
    Survey_user_input create(@RequestBody Survey_user_input survey_user_input);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/batch")
    Boolean createBatch(@RequestBody List<Survey_user_input> survey_user_inputs);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/{id}")
    Survey_user_input update(@PathVariable("id") Integer id,@RequestBody Survey_user_input survey_user_input);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/batch")
    Boolean updateBatch(@RequestBody List<Survey_user_input> survey_user_inputs);



    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/{id}")
    Survey_user_input get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/select")
    Page<Survey_user_input> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/getdraft")
    Survey_user_input getDraft();


}

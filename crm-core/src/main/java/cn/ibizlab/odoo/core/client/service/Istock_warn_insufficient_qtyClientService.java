package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warn_insufficient_qty] 服务对象接口
 */
public interface Istock_warn_insufficient_qtyClientService{

    public Istock_warn_insufficient_qty createModel() ;

    public void removeBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties);

    public void update(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

    public void updateBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties);

    public Page<Istock_warn_insufficient_qty> fetchDefault(SearchContext context);

    public void remove(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

    public void createBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties);

    public void get(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

    public void create(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

    public Page<Istock_warn_insufficient_qty> select(SearchContext context);

    public void getDraft(Istock_warn_insufficient_qty stock_warn_insufficient_qty);

}

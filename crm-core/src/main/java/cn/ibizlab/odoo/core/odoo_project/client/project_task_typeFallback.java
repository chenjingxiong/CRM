package cn.ibizlab.odoo.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[project_task_type] 服务对象接口
 */
@Component
public class project_task_typeFallback implements project_task_typeFeignClient{


    public Project_task_type get(Integer id){
            return null;
     }


    public Project_task_type update(Integer id, Project_task_type project_task_type){
            return null;
     }
    public Boolean updateBatch(List<Project_task_type> project_task_types){
            return false;
     }


    public Page<Project_task_type> searchDefault(Project_task_typeSearchContext context){
            return null;
     }



    public Project_task_type create(Project_task_type project_task_type){
            return null;
     }
    public Boolean createBatch(List<Project_task_type> project_task_types){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Project_task_type> select(){
            return null;
     }

    public Project_task_type getDraft(){
            return null;
    }



}

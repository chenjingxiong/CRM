package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_channelFeignClient;

/**
 * 实体[即时聊天] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_channelServiceImpl implements IIm_livechat_channelService {

    @Autowired
    im_livechat_channelFeignClient im_livechat_channelFeignClient;


    @Override
    public Im_livechat_channel get(Integer id) {
		Im_livechat_channel et=im_livechat_channelFeignClient.get(id);
        if(et==null){
            et=new Im_livechat_channel();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Im_livechat_channel et) {
        Im_livechat_channel rt = im_livechat_channelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Im_livechat_channel> list){
        im_livechat_channelFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=im_livechat_channelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        im_livechat_channelFeignClient.removeBatch(idList);
    }

    @Override
    public Im_livechat_channel getDraft(Im_livechat_channel et) {
        et=im_livechat_channelFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Im_livechat_channel et) {
        Im_livechat_channel rt = im_livechat_channelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_channel> list){
        im_livechat_channelFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_channel> searchDefault(Im_livechat_channelSearchContext context) {
        Page<Im_livechat_channel> im_livechat_channels=im_livechat_channelFeignClient.searchDefault(context);
        return im_livechat_channels;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_stage] 服务对象接口
 */
public interface Icrm_stageClientService{

    public Icrm_stage createModel() ;

    public void get(Icrm_stage crm_stage);

    public void createBatch(List<Icrm_stage> crm_stages);

    public void update(Icrm_stage crm_stage);

    public void remove(Icrm_stage crm_stage);

    public void updateBatch(List<Icrm_stage> crm_stages);

    public void create(Icrm_stage crm_stage);

    public Page<Icrm_stage> fetchDefault(SearchContext context);

    public void removeBatch(List<Icrm_stage> crm_stages);

    public Page<Icrm_stage> select(SearchContext context);

    public void getDraft(Icrm_stage crm_stage);

}

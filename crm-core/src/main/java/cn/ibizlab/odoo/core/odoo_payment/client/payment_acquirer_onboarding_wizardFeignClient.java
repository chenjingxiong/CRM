package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-payment", contextId = "payment-acquirer-onboarding-wizard", fallback = payment_acquirer_onboarding_wizardFallback.class)
public interface payment_acquirer_onboarding_wizardFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards")
    Payment_acquirer_onboarding_wizard create(@RequestBody Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/batch")
    Boolean createBatch(@RequestBody List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/searchdefault")
    Page<Payment_acquirer_onboarding_wizard> searchDefault(@RequestBody Payment_acquirer_onboarding_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/{id}")
    Payment_acquirer_onboarding_wizard update(@PathVariable("id") Integer id,@RequestBody Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/batch")
    Boolean updateBatch(@RequestBody List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards);



    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/{id}")
    Payment_acquirer_onboarding_wizard get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/select")
    Page<Payment_acquirer_onboarding_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/getdraft")
    Payment_acquirer_onboarding_wizard getDraft();


}

package cn.ibizlab.odoo.core.odoo_uom.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[uom_uom] 服务对象接口
 */
@FeignClient(value = "odoo-uom", contextId = "uom-uom", fallback = uom_uomFallback.class)
public interface uom_uomFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/uom_uoms")
    Uom_uom create(@RequestBody Uom_uom uom_uom);

    @RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/batch")
    Boolean createBatch(@RequestBody List<Uom_uom> uom_uoms);



    @RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/searchdefault")
    Page<Uom_uom> searchDefault(@RequestBody Uom_uomSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/{id}")
    Uom_uom get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/{id}")
    Uom_uom update(@PathVariable("id") Integer id,@RequestBody Uom_uom uom_uom);

    @RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/batch")
    Boolean updateBatch(@RequestBody List<Uom_uom> uom_uoms);


    @RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/select")
    Page<Uom_uom> select();


    @RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/getdraft")
    Uom_uom getDraft();


}

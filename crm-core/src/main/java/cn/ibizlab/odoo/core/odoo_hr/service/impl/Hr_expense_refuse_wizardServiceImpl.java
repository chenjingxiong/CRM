package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_refuse_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_expense_refuse_wizardFeignClient;

/**
 * 实体[费用拒绝原因向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_expense_refuse_wizardServiceImpl implements IHr_expense_refuse_wizardService {

    @Autowired
    hr_expense_refuse_wizardFeignClient hr_expense_refuse_wizardFeignClient;


    @Override
    public boolean create(Hr_expense_refuse_wizard et) {
        Hr_expense_refuse_wizard rt = hr_expense_refuse_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_expense_refuse_wizard> list){
        hr_expense_refuse_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Hr_expense_refuse_wizard et) {
        Hr_expense_refuse_wizard rt = hr_expense_refuse_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_expense_refuse_wizard> list){
        hr_expense_refuse_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_expense_refuse_wizard get(Integer id) {
		Hr_expense_refuse_wizard et=hr_expense_refuse_wizardFeignClient.get(id);
        if(et==null){
            et=new Hr_expense_refuse_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Hr_expense_refuse_wizard getDraft(Hr_expense_refuse_wizard et) {
        et=hr_expense_refuse_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_expense_refuse_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_expense_refuse_wizardFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_expense_refuse_wizard> searchDefault(Hr_expense_refuse_wizardSearchContext context) {
        Page<Hr_expense_refuse_wizard> hr_expense_refuse_wizards=hr_expense_refuse_wizardFeignClient.searchDefault(context);
        return hr_expense_refuse_wizards;
    }


}



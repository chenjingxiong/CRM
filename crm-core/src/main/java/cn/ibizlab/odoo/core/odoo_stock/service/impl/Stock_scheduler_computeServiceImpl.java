package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scheduler_computeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_scheduler_computeFeignClient;

/**
 * 实体[手动运行调度器] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_scheduler_computeServiceImpl implements IStock_scheduler_computeService {

    @Autowired
    stock_scheduler_computeFeignClient stock_scheduler_computeFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_scheduler_computeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_scheduler_computeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_scheduler_compute et) {
        Stock_scheduler_compute rt = stock_scheduler_computeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_scheduler_compute> list){
        stock_scheduler_computeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_scheduler_compute et) {
        Stock_scheduler_compute rt = stock_scheduler_computeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_scheduler_compute> list){
        stock_scheduler_computeFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_scheduler_compute getDraft(Stock_scheduler_compute et) {
        et=stock_scheduler_computeFeignClient.getDraft();
        return et;
    }

    @Override
    public Stock_scheduler_compute get(Integer id) {
		Stock_scheduler_compute et=stock_scheduler_computeFeignClient.get(id);
        if(et==null){
            et=new Stock_scheduler_compute();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_scheduler_compute> searchDefault(Stock_scheduler_computeSearchContext context) {
        Page<Stock_scheduler_compute> stock_scheduler_computes=stock_scheduler_computeFeignClient.searchDefault(context);
        return stock_scheduler_computes;
    }


}



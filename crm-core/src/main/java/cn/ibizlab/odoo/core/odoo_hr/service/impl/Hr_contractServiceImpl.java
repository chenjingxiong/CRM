package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contractService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_contractFeignClient;

/**
 * 实体[Contract] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_contractServiceImpl implements IHr_contractService {

    @Autowired
    hr_contractFeignClient hr_contractFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=hr_contractFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_contractFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_contract et) {
        Hr_contract rt = hr_contractFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_contract> list){
        hr_contractFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Hr_contract et) {
        Hr_contract rt = hr_contractFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_contract> list){
        hr_contractFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_contract get(Integer id) {
		Hr_contract et=hr_contractFeignClient.get(id);
        if(et==null){
            et=new Hr_contract();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Hr_contract getDraft(Hr_contract et) {
        et=hr_contractFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_contract> searchDefault(Hr_contractSearchContext context) {
        Page<Hr_contract> hr_contracts=hr_contractFeignClient.searchDefault(context);
        return hr_contracts;
    }


}



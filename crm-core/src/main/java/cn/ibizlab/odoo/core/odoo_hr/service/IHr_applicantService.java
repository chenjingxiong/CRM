package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;


/**
 * 实体[Hr_applicant] 服务对象接口
 */
public interface IHr_applicantService{

    boolean create(Hr_applicant et) ;
    void createBatch(List<Hr_applicant> list) ;
    Hr_applicant getDraft(Hr_applicant et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_applicant et) ;
    void updateBatch(List<Hr_applicant> list) ;
    Hr_applicant get(Integer key) ;
    Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context) ;

}




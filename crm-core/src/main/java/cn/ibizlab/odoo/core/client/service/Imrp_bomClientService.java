package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_bom;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_bom] 服务对象接口
 */
public interface Imrp_bomClientService{

    public Imrp_bom createModel() ;

    public void create(Imrp_bom mrp_bom);

    public Page<Imrp_bom> fetchDefault(SearchContext context);

    public void remove(Imrp_bom mrp_bom);

    public void update(Imrp_bom mrp_bom);

    public void get(Imrp_bom mrp_bom);

    public void createBatch(List<Imrp_bom> mrp_boms);

    public void updateBatch(List<Imrp_bom> mrp_boms);

    public void removeBatch(List<Imrp_bom> mrp_boms);

    public Page<Imrp_bom> select(SearchContext context);

    public void getDraft(Imrp_bom mrp_bom);

}

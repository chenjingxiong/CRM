package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
public interface Iproduct_template_attribute_lineClientService{

    public Iproduct_template_attribute_line createModel() ;

    public void removeBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines);

    public Page<Iproduct_template_attribute_line> fetchDefault(SearchContext context);

    public void createBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines);

    public void update(Iproduct_template_attribute_line product_template_attribute_line);

    public void remove(Iproduct_template_attribute_line product_template_attribute_line);

    public void create(Iproduct_template_attribute_line product_template_attribute_line);

    public void updateBatch(List<Iproduct_template_attribute_line> product_template_attribute_lines);

    public void get(Iproduct_template_attribute_line product_template_attribute_line);

    public Page<Iproduct_template_attribute_line> select(SearchContext context);

    public void getDraft(Iproduct_template_attribute_line product_template_attribute_line);

}

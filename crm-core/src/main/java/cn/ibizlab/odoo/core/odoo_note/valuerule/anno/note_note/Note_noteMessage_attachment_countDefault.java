package cn.ibizlab.odoo.core.odoo_note.valuerule.anno.note_note;

import cn.ibizlab.odoo.core.odoo_note.valuerule.validator.note_note.Note_noteMessage_attachment_countDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Note_note
 * 属性：Message_attachment_count
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Note_noteMessage_attachment_countDefaultValidator.class})
public @interface Note_noteMessage_attachment_countDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

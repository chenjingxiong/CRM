package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[purchase_bill_union] 服务对象接口
 */
@FeignClient(value = "odoo-purchase", contextId = "purchase-bill-union", fallback = purchase_bill_unionFallback.class)
public interface purchase_bill_unionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions")
    Purchase_bill_union create(@RequestBody Purchase_bill_union purchase_bill_union);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/batch")
    Boolean createBatch(@RequestBody List<Purchase_bill_union> purchase_bill_unions);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/searchdefault")
    Page<Purchase_bill_union> searchDefault(@RequestBody Purchase_bill_unionSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/{id}")
    Purchase_bill_union get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/{id}")
    Purchase_bill_union update(@PathVariable("id") Integer id,@RequestBody Purchase_bill_union purchase_bill_union);

    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/batch")
    Boolean updateBatch(@RequestBody List<Purchase_bill_union> purchase_bill_unions);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/select")
    Page<Purchase_bill_union> select();


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/getdraft")
    Purchase_bill_union getDraft();


}

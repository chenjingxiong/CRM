package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_task] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-task", fallback = mro_taskFallback.class)
public interface mro_taskFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/searchdefault")
    Page<Mro_task> searchDefault(@RequestBody Mro_taskSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/{id}")
    Mro_task get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_tasks")
    Mro_task create(@RequestBody Mro_task mro_task);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/batch")
    Boolean createBatch(@RequestBody List<Mro_task> mro_tasks);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/{id}")
    Mro_task update(@PathVariable("id") Integer id,@RequestBody Mro_task mro_task);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/batch")
    Boolean updateBatch(@RequestBody List<Mro_task> mro_tasks);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/select")
    Page<Mro_task> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/getdraft")
    Mro_task getDraft();


}

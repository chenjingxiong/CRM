package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_noreadonlySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象接口
 */
@Component
public class base_import_tests_models_char_noreadonlyFallback implements base_import_tests_models_char_noreadonlyFeignClient{

    public Base_import_tests_models_char_noreadonly update(Integer id, Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
            return false;
     }




    public Page<Base_import_tests_models_char_noreadonly> searchDefault(Base_import_tests_models_char_noreadonlySearchContext context){
            return null;
     }



    public Base_import_tests_models_char_noreadonly get(Integer id){
            return null;
     }


    public Base_import_tests_models_char_noreadonly create(Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Base_import_tests_models_char_noreadonly> select(){
            return null;
     }

    public Base_import_tests_models_char_noreadonly getDraft(){
            return null;
    }



}

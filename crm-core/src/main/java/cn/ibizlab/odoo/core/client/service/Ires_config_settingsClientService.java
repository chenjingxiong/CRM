package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_config_settings;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config_settings] 服务对象接口
 */
public interface Ires_config_settingsClientService{

    public Ires_config_settings createModel() ;

    public void createBatch(List<Ires_config_settings> res_config_settings);

    public void updateBatch(List<Ires_config_settings> res_config_settings);

    public Page<Ires_config_settings> fetchDefault(SearchContext context);

    public void removeBatch(List<Ires_config_settings> res_config_settings);

    public void update(Ires_config_settings res_config_settings);

    public void remove(Ires_config_settings res_config_settings);

    public void create(Ires_config_settings res_config_settings);

    public void get(Ires_config_settings res_config_settings);

    public Page<Ires_config_settings> select(SearchContext context);

    public void getDraft(Ires_config_settings res_config_settings);

}

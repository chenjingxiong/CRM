package cn.ibizlab.odoo.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;


/**
 * 实体[Purchase_bill_union] 服务对象接口
 */
public interface IPurchase_bill_unionService{

    Purchase_bill_union getDraft(Purchase_bill_union et) ;
    boolean create(Purchase_bill_union et) ;
    void createBatch(List<Purchase_bill_union> list) ;
    Purchase_bill_union get(Integer key) ;
    boolean update(Purchase_bill_union et) ;
    void updateBatch(List<Purchase_bill_union> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context) ;

}




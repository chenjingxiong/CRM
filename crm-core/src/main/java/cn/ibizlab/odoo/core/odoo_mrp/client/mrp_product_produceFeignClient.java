package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_product_produce] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-product-produce", fallback = mrp_product_produceFallback.class)
public interface mrp_product_produceFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/searchdefault")
    Page<Mrp_product_produce> searchDefault(@RequestBody Mrp_product_produceSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/{id}")
    Mrp_product_produce get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces")
    Mrp_product_produce create(@RequestBody Mrp_product_produce mrp_product_produce);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/batch")
    Boolean createBatch(@RequestBody List<Mrp_product_produce> mrp_product_produces);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/{id}")
    Mrp_product_produce update(@PathVariable("id") Integer id,@RequestBody Mrp_product_produce mrp_product_produce);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/batch")
    Boolean updateBatch(@RequestBody List<Mrp_product_produce> mrp_product_produces);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/select")
    Page<Mrp_product_produce> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/getdraft")
    Mrp_product_produce getDraft();


}

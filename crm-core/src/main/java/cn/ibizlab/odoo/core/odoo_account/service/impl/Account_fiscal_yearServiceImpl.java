package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_yearService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_yearFeignClient;

/**
 * 实体[会计年度] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_yearServiceImpl implements IAccount_fiscal_yearService {

    @Autowired
    account_fiscal_yearFeignClient account_fiscal_yearFeignClient;


    @Override
    public Account_fiscal_year getDraft(Account_fiscal_year et) {
        et=account_fiscal_yearFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_fiscal_yearFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_fiscal_yearFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_fiscal_year et) {
        Account_fiscal_year rt = account_fiscal_yearFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_fiscal_year> list){
        account_fiscal_yearFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_fiscal_year get(Integer id) {
		Account_fiscal_year et=account_fiscal_yearFeignClient.get(id);
        if(et==null){
            et=new Account_fiscal_year();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_fiscal_year et) {
        Account_fiscal_year rt = account_fiscal_yearFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_year> list){
        account_fiscal_yearFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_year> searchDefault(Account_fiscal_yearSearchContext context) {
        Page<Account_fiscal_year> account_fiscal_years=account_fiscal_yearFeignClient.searchDefault(context);
        return account_fiscal_years;
    }


}



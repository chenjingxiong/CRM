package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_packagingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_packagingFeignClient;

/**
 * 实体[产品包装] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_packagingServiceImpl implements IProduct_packagingService {

    @Autowired
    product_packagingFeignClient product_packagingFeignClient;


    @Override
    public Product_packaging getDraft(Product_packaging et) {
        et=product_packagingFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_packagingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_packagingFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Product_packaging et) {
        Product_packaging rt = product_packagingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_packaging> list){
        product_packagingFeignClient.createBatch(list) ;
    }

    @Override
    public Product_packaging get(Integer id) {
		Product_packaging et=product_packagingFeignClient.get(id);
        if(et==null){
            et=new Product_packaging();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Product_packaging et) {
        Product_packaging rt = product_packagingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_packaging> list){
        product_packagingFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_packaging> searchDefault(Product_packagingSearchContext context) {
        Page<Product_packaging> product_packagings=product_packagingFeignClient.searchDefault(context);
        return product_packagings;
    }


}



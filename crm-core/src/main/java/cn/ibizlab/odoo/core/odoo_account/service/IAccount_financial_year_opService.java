package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;


/**
 * 实体[Account_financial_year_op] 服务对象接口
 */
public interface IAccount_financial_year_opService{

    boolean update(Account_financial_year_op et) ;
    void updateBatch(List<Account_financial_year_op> list) ;
    Account_financial_year_op getDraft(Account_financial_year_op et) ;
    boolean create(Account_financial_year_op et) ;
    void createBatch(List<Account_financial_year_op> list) ;
    Account_financial_year_op get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context) ;

}




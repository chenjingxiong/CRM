package cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_goal;

import cn.ibizlab.odoo.core.odoo_gamification.valuerule.validator.gamification_goal.Gamification_goalCompletenessDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Gamification_goal
 * 属性：Completeness
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Gamification_goalCompletenessDefaultValidator.class})
public @interface Gamification_goalCompletenessDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

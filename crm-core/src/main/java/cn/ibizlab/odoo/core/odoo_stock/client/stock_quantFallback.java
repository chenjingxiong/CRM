package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_quant] 服务对象接口
 */
@Component
public class stock_quantFallback implements stock_quantFeignClient{

    public Stock_quant update(Integer id, Stock_quant stock_quant){
            return null;
     }
    public Boolean updateBatch(List<Stock_quant> stock_quants){
            return false;
     }



    public Stock_quant get(Integer id){
            return null;
     }


    public Page<Stock_quant> searchDefault(Stock_quantSearchContext context){
            return null;
     }


    public Stock_quant create(Stock_quant stock_quant){
            return null;
     }
    public Boolean createBatch(List<Stock_quant> stock_quants){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Stock_quant> select(){
            return null;
     }

    public Stock_quant getDraft(){
            return null;
    }



}

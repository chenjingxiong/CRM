package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_partner_binding;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_partner_binding.Crm_partner_bindingCreate_uidDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_partner_binding
 * 属性：Create_uid
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_partner_bindingCreate_uidDefaultValidator.class})
public @interface Crm_partner_bindingCreate_uidDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-change-product-qty", fallback = stock_change_product_qtyFallback.class)
public interface stock_change_product_qtyFeignClient {



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/{id}")
    Stock_change_product_qty update(@PathVariable("id") Integer id,@RequestBody Stock_change_product_qty stock_change_product_qty);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/batch")
    Boolean updateBatch(@RequestBody List<Stock_change_product_qty> stock_change_product_qties);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/searchdefault")
    Page<Stock_change_product_qty> searchDefault(@RequestBody Stock_change_product_qtySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/{id}")
    Stock_change_product_qty get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties")
    Stock_change_product_qty create(@RequestBody Stock_change_product_qty stock_change_product_qty);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/batch")
    Boolean createBatch(@RequestBody List<Stock_change_product_qty> stock_change_product_qties);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/select")
    Page<Stock_change_product_qty> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/getdraft")
    Stock_change_product_qty getDraft();


}

package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_attribute] 服务对象接口
 */
@Component
public class product_attributeFallback implements product_attributeFeignClient{

    public Product_attribute get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Product_attribute> searchDefault(Product_attributeSearchContext context){
            return null;
     }


    public Product_attribute update(Integer id, Product_attribute product_attribute){
            return null;
     }
    public Boolean updateBatch(List<Product_attribute> product_attributes){
            return false;
     }



    public Product_attribute create(Product_attribute product_attribute){
            return null;
     }
    public Boolean createBatch(List<Product_attribute> product_attributes){
            return false;
     }


    public Page<Product_attribute> select(){
            return null;
     }

    public Product_attribute getDraft(){
            return null;
    }



}

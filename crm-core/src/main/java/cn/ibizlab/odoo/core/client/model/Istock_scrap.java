package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_scrap] 对象
 */
public interface Istock_scrap {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [预计日期]
     */
    public void setDate_expected(Timestamp date_expected);
    
    /**
     * 设置 [预计日期]
     */
    public Timestamp getDate_expected();

    /**
     * 获取 [预计日期]脏标记
     */
    public boolean getDate_expectedDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [批次]
     */
    public void setLot_id(Integer lot_id);
    
    /**
     * 设置 [批次]
     */
    public Integer getLot_id();

    /**
     * 获取 [批次]脏标记
     */
    public boolean getLot_idDirtyFlag();
    /**
     * 获取 [批次]
     */
    public void setLot_id_text(String lot_id_text);
    
    /**
     * 设置 [批次]
     */
    public String getLot_id_text();

    /**
     * 获取 [批次]脏标记
     */
    public boolean getLot_id_textDirtyFlag();
    /**
     * 获取 [报废移动]
     */
    public void setMove_id(Integer move_id);
    
    /**
     * 设置 [报废移动]
     */
    public Integer getMove_id();

    /**
     * 获取 [报废移动]脏标记
     */
    public boolean getMove_idDirtyFlag();
    /**
     * 获取 [报废移动]
     */
    public void setMove_id_text(String move_id_text);
    
    /**
     * 设置 [报废移动]
     */
    public String getMove_id_text();

    /**
     * 获取 [报废移动]脏标记
     */
    public boolean getMove_id_textDirtyFlag();
    /**
     * 获取 [编号]
     */
    public void setName(String name);
    
    /**
     * 设置 [编号]
     */
    public String getName();

    /**
     * 获取 [编号]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id(Integer owner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getOwner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id_text(String owner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getOwner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_id_textDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id(Integer package_id);
    
    /**
     * 设置 [包裹]
     */
    public Integer getPackage_id();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_idDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id_text(String package_id_text);
    
    /**
     * 设置 [包裹]
     */
    public String getPackage_id_text();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_id_textDirtyFlag();
    /**
     * 获取 [分拣]
     */
    public void setPicking_id(Integer picking_id);
    
    /**
     * 设置 [分拣]
     */
    public Integer getPicking_id();

    /**
     * 获取 [分拣]脏标记
     */
    public boolean getPicking_idDirtyFlag();
    /**
     * 获取 [分拣]
     */
    public void setPicking_id_text(String picking_id_text);
    
    /**
     * 设置 [分拣]
     */
    public String getPicking_id_text();

    /**
     * 获取 [分拣]脏标记
     */
    public boolean getPicking_id_textDirtyFlag();
    /**
     * 获取 [制造订单]
     */
    public void setProduction_id(Integer production_id);
    
    /**
     * 设置 [制造订单]
     */
    public Integer getProduction_id();

    /**
     * 获取 [制造订单]脏标记
     */
    public boolean getProduction_idDirtyFlag();
    /**
     * 获取 [制造订单]
     */
    public void setProduction_id_text(String production_id_text);
    
    /**
     * 设置 [制造订单]
     */
    public String getProduction_id_text();

    /**
     * 获取 [制造订单]脏标记
     */
    public boolean getProduction_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [报废位置]
     */
    public void setScrap_location_id(Integer scrap_location_id);
    
    /**
     * 设置 [报废位置]
     */
    public Integer getScrap_location_id();

    /**
     * 获取 [报废位置]脏标记
     */
    public boolean getScrap_location_idDirtyFlag();
    /**
     * 获取 [报废位置]
     */
    public void setScrap_location_id_text(String scrap_location_id_text);
    
    /**
     * 设置 [报废位置]
     */
    public String getScrap_location_id_text();

    /**
     * 获取 [报废位置]脏标记
     */
    public boolean getScrap_location_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setScrap_qty(Double scrap_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getScrap_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getScrap_qtyDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setTracking(String tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getTracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getTrackingDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_id(Integer workorder_id);
    
    /**
     * 设置 [工单]
     */
    public Integer getWorkorder_id();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_idDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_id_text(String workorder_id_text);
    
    /**
     * 设置 [工单]
     */
    public String getWorkorder_id_text();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

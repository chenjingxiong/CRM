package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warehouse] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-warehouse", fallback = stock_warehouseFallback.class)
public interface stock_warehouseFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/searchdefault")
    Page<Stock_warehouse> searchDefault(@RequestBody Stock_warehouseSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/{id}")
    Stock_warehouse update(@PathVariable("id") Integer id,@RequestBody Stock_warehouse stock_warehouse);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/batch")
    Boolean updateBatch(@RequestBody List<Stock_warehouse> stock_warehouses);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses")
    Stock_warehouse create(@RequestBody Stock_warehouse stock_warehouse);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/batch")
    Boolean createBatch(@RequestBody List<Stock_warehouse> stock_warehouses);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/{id}")
    Stock_warehouse get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/select")
    Page<Stock_warehouse> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/getdraft")
    Stock_warehouse getDraft();


}

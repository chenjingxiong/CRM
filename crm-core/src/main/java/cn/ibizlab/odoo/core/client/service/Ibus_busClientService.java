package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibus_bus;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[bus_bus] 服务对象接口
 */
public interface Ibus_busClientService{

    public Ibus_bus createModel() ;

    public void remove(Ibus_bus bus_bus);

    public void update(Ibus_bus bus_bus);

    public void create(Ibus_bus bus_bus);

    public void createBatch(List<Ibus_bus> bus_buses);

    public void get(Ibus_bus bus_bus);

    public Page<Ibus_bus> fetchDefault(SearchContext context);

    public void updateBatch(List<Ibus_bus> bus_buses);

    public void removeBatch(List<Ibus_bus> bus_buses);

    public Page<Ibus_bus> select(SearchContext context);

    public void getDraft(Ibus_bus bus_bus);

}

package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;


/**
 * 实体[Gamification_challenge_line] 服务对象接口
 */
public interface IGamification_challenge_lineService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Gamification_challenge_line et) ;
    void createBatch(List<Gamification_challenge_line> list) ;
    Gamification_challenge_line get(Integer key) ;
    boolean update(Gamification_challenge_line et) ;
    void updateBatch(List<Gamification_challenge_line> list) ;
    Gamification_challenge_line getDraft(Gamification_challenge_line et) ;
    Page<Gamification_challenge_line> searchDefault(Gamification_challenge_lineSearchContext context) ;

}




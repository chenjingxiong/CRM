package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_request_reject] 服务对象接口
 */
@Component
public class mro_request_rejectFallback implements mro_request_rejectFeignClient{

    public Mro_request_reject update(Integer id, Mro_request_reject mro_request_reject){
            return null;
     }
    public Boolean updateBatch(List<Mro_request_reject> mro_request_rejects){
            return false;
     }



    public Mro_request_reject create(Mro_request_reject mro_request_reject){
            return null;
     }
    public Boolean createBatch(List<Mro_request_reject> mro_request_rejects){
            return false;
     }

    public Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Mro_request_reject get(Integer id){
            return null;
     }


    public Page<Mro_request_reject> select(){
            return null;
     }

    public Mro_request_reject getDraft(){
            return null;
    }



}

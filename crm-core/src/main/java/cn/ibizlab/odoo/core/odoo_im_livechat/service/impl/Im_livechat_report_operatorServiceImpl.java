package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_report_operatorFeignClient;

/**
 * 实体[实时聊天支持操作员报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_report_operatorServiceImpl implements IIm_livechat_report_operatorService {

    @Autowired
    im_livechat_report_operatorFeignClient im_livechat_report_operatorFeignClient;


    @Override
    public boolean create(Im_livechat_report_operator et) {
        Im_livechat_report_operator rt = im_livechat_report_operatorFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_report_operator> list){
        im_livechat_report_operatorFeignClient.createBatch(list) ;
    }

    @Override
    public Im_livechat_report_operator get(Integer id) {
		Im_livechat_report_operator et=im_livechat_report_operatorFeignClient.get(id);
        if(et==null){
            et=new Im_livechat_report_operator();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Im_livechat_report_operator et) {
        Im_livechat_report_operator rt = im_livechat_report_operatorFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Im_livechat_report_operator> list){
        im_livechat_report_operatorFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=im_livechat_report_operatorFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        im_livechat_report_operatorFeignClient.removeBatch(idList);
    }

    @Override
    public Im_livechat_report_operator getDraft(Im_livechat_report_operator et) {
        et=im_livechat_report_operatorFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_report_operator> searchDefault(Im_livechat_report_operatorSearchContext context) {
        Page<Im_livechat_report_operator> im_livechat_report_operators=im_livechat_report_operatorFeignClient.searchDefault(context);
        return im_livechat_report_operators;
    }


}



package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [purchase_report] 对象
 */
public interface Ipurchase_report {

    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id(Integer account_analytic_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAccount_analytic_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id_text(String account_analytic_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAccount_analytic_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_id_textDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [产品种类]
     */
    public Integer getCategory_id();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [产品种类]
     */
    public String getCategory_id_text();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id(Integer commercial_partner_id);
    
    /**
     * 设置 [商业实体]
     */
    public Integer getCommercial_partner_id();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_idDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id_text(String commercial_partner_id_text);
    
    /**
     * 设置 [商业实体]
     */
    public String getCommercial_partner_id_text();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [业务伙伴国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [业务伙伴国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [业务伙伴国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [业务伙伴国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [业务伙伴国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [业务伙伴国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [批准日期]
     */
    public void setDate_approve(Timestamp date_approve);
    
    /**
     * 设置 [批准日期]
     */
    public Timestamp getDate_approve();

    /**
     * 获取 [批准日期]脏标记
     */
    public boolean getDate_approveDirtyFlag();
    /**
     * 获取 [单据日期]
     */
    public void setDate_order(Timestamp date_order);
    
    /**
     * 设置 [单据日期]
     */
    public Timestamp getDate_order();

    /**
     * 获取 [单据日期]脏标记
     */
    public boolean getDate_orderDirtyFlag();
    /**
     * 获取 [验证天数]
     */
    public void setDelay(Double delay);
    
    /**
     * 设置 [验证天数]
     */
    public Double getDelay();

    /**
     * 获取 [验证天数]脏标记
     */
    public boolean getDelayDirtyFlag();
    /**
     * 获取 [交货天数]
     */
    public void setDelay_pass(Double delay_pass);
    
    /**
     * 设置 [交货天数]
     */
    public Double getDelay_pass();

    /**
     * 获取 [交货天数]脏标记
     */
    public boolean getDelay_passDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id(Integer fiscal_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getFiscal_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id_text(String fiscal_position_id_text);
    
    /**
     * 设置 [税科目调整]
     */
    public String getFiscal_position_id_text();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [# 明细行]
     */
    public void setNbr_lines(Integer nbr_lines);
    
    /**
     * 设置 [# 明细行]
     */
    public Integer getNbr_lines();

    /**
     * 获取 [# 明细行]脏标记
     */
    public boolean getNbr_linesDirtyFlag();
    /**
     * 获取 [采购 - 标准单价]
     */
    public void setNegociation(Double negociation);
    
    /**
     * 设置 [采购 - 标准单价]
     */
    public Double getNegociation();

    /**
     * 获取 [采购 - 标准单价]脏标记
     */
    public boolean getNegociationDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getPartner_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getPartner_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [平均价格]
     */
    public void setPrice_average(Double price_average);
    
    /**
     * 设置 [平均价格]
     */
    public Double getPrice_average();

    /**
     * 获取 [平均价格]脏标记
     */
    public boolean getPrice_averageDirtyFlag();
    /**
     * 获取 [产品价值]
     */
    public void setPrice_standard(Double price_standard);
    
    /**
     * 设置 [产品价值]
     */
    public Double getPrice_standard();

    /**
     * 获取 [产品价值]脏标记
     */
    public boolean getPrice_standardDirtyFlag();
    /**
     * 获取 [总价]
     */
    public void setPrice_total(Double price_total);
    
    /**
     * 设置 [总价]
     */
    public Double getPrice_total();

    /**
     * 获取 [总价]脏标记
     */
    public boolean getPrice_totalDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [产品模板]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [参考计量单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [参考计量单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [参考计量单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [参考计量单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [参考计量单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [参考计量单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [订单状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [订单状态]
     */
    public String getState();

    /**
     * 获取 [订单状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setUnit_quantity(Double unit_quantity);
    
    /**
     * 设置 [数量]
     */
    public Double getUnit_quantity();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getUnit_quantityDirtyFlag();
    /**
     * 获取 [采购员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [采购员]
     */
    public Integer getUser_id();

    /**
     * 获取 [采购员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [采购员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [采购员]
     */
    public String getUser_id_text();

    /**
     * 获取 [采购员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [体积]
     */
    public void setVolume(Double volume);
    
    /**
     * 设置 [体积]
     */
    public Double getVolume();

    /**
     * 获取 [体积]脏标记
     */
    public boolean getVolumeDirtyFlag();
    /**
     * 获取 [毛重]
     */
    public void setWeight(Double weight);
    
    /**
     * 设置 [毛重]
     */
    public Double getWeight();

    /**
     * 获取 [毛重]脏标记
     */
    public boolean getWeightDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_order;

import cn.ibizlab.odoo.core.odoo_purchase.valuerule.validator.purchase_order.Purchase_orderAccess_warningDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Purchase_order
 * 属性：Access_warning
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Purchase_orderAccess_warningDefaultValidator.class})
public @interface Purchase_orderAccess_warningDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

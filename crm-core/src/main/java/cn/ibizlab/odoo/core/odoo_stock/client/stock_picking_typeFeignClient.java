package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_picking_type] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-picking-type", fallback = stock_picking_typeFallback.class)
public interface stock_picking_typeFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/searchdefault")
    Page<Stock_picking_type> searchDefault(@RequestBody Stock_picking_typeSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types")
    Stock_picking_type create(@RequestBody Stock_picking_type stock_picking_type);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/batch")
    Boolean createBatch(@RequestBody List<Stock_picking_type> stock_picking_types);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/{id}")
    Stock_picking_type update(@PathVariable("id") Integer id,@RequestBody Stock_picking_type stock_picking_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/batch")
    Boolean updateBatch(@RequestBody List<Stock_picking_type> stock_picking_types);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/{id}")
    Stock_picking_type get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/select")
    Page<Stock_picking_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/getdraft")
    Stock_picking_type getDraft();


}

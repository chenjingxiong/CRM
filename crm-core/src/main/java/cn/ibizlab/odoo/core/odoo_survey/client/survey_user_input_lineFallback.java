package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_user_input_line] 服务对象接口
 */
@Component
public class survey_user_input_lineFallback implements survey_user_input_lineFeignClient{

    public Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Survey_user_input_line get(Integer id){
            return null;
     }


    public Survey_user_input_line update(Integer id, Survey_user_input_line survey_user_input_line){
            return null;
     }
    public Boolean updateBatch(List<Survey_user_input_line> survey_user_input_lines){
            return false;
     }



    public Survey_user_input_line create(Survey_user_input_line survey_user_input_line){
            return null;
     }
    public Boolean createBatch(List<Survey_user_input_line> survey_user_input_lines){
            return false;
     }


    public Page<Survey_user_input_line> select(){
            return null;
     }

    public Survey_user_input_line getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [销售订单] 对象
 */
@Data
public class Sale_order extends EntityClient implements Serializable {

    /**
     * 单据日期
     */
    @DEField(name = "date_order")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private Double amountUntaxed;

    /**
     * 验证
     */
    @DEField(name = "validity_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_date" , format="yyyy-MM-dd")
    @JsonProperty("validity_date")
    private Timestamp validityDate;

    /**
     * 折扣前金额
     */
    @JSONField(name = "amount_undiscounted")
    @JsonProperty("amount_undiscounted")
    private Double amountUndiscounted;

    /**
     * 发票数
     */
    @JSONField(name = "invoice_count")
    @JsonProperty("invoice_count")
    private Integer invoiceCount;

    /**
     * 访问警告
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;

    /**
     * 警告
     */
    @DEField(name = "warning_stock")
    @JSONField(name = "warning_stock")
    @JsonProperty("warning_stock")
    private String warningStock;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 实际日期
     */
    @DEField(name = "effective_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    private Timestamp effectiveDate;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 在线签名
     */
    @DEField(name = "require_signature")
    @JSONField(name = "require_signature")
    @JsonProperty("require_signature")
    private String requireSignature;

    /**
     * 汇率
     */
    @DEField(name = "currency_rate")
    @JSONField(name = "currency_rate")
    @JsonProperty("currency_rate")
    private Double currencyRate;

    /**
     * 送货策略
     */
    @DEField(name = "picking_policy")
    @JSONField(name = "picking_policy")
    @JsonProperty("picking_policy")
    private String pickingPolicy;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 签名
     */
    @JSONField(name = "signature")
    @JsonProperty("signature")
    private byte[] signature;

    /**
     * 补货组
     */
    @DEField(name = "procurement_group_id")
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 购物车数量
     */
    @JSONField(name = "cart_quantity")
    @JsonProperty("cart_quantity")
    private Integer cartQuantity;

    /**
     * 类型名称
     */
    @JSONField(name = "type_name")
    @JsonProperty("type_name")
    private String typeName;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 出库单
     */
    @JSONField(name = "delivery_count")
    @JsonProperty("delivery_count")
    private Integer deliveryCount;

    /**
     * 已签核
     */
    @DEField(name = "signed_by")
    @JSONField(name = "signed_by")
    @JsonProperty("signed_by")
    private String signedBy;

    /**
     * 订单行
     */
    @JSONField(name = "order_line")
    @JsonProperty("order_line")
    private String orderLine;

    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 在线支付
     */
    @DEField(name = "require_payment")
    @JSONField(name = "require_payment")
    @JsonProperty("require_payment")
    private String requirePayment;

    /**
     * 只是服务
     */
    @JSONField(name = "only_services")
    @JsonProperty("only_services")
    private String onlyServices;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 网页显示订单明细
     */
    @JSONField(name = "website_order_line")
    @JsonProperty("website_order_line")
    private String websiteOrderLine;

    /**
     * 付款参考:
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 发票状态
     */
    @DEField(name = "invoice_status")
    @JSONField(name = "invoice_status")
    @JsonProperty("invoice_status")
    private String invoiceStatus;

    /**
     * 标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 交易
     */
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    private String transactionIds;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 遗弃的购物车
     */
    @JSONField(name = "is_abandoned_cart")
    @JsonProperty("is_abandoned_cart")
    private String isAbandonedCart;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * # 费用
     */
    @JSONField(name = "expense_count")
    @JsonProperty("expense_count")
    private Integer expenseCount;

    /**
     * 已授权的交易
     */
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    private String authorizedTransactionIds;

    /**
     * 承诺日期
     */
    @DEField(name = "commitment_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "commitment_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("commitment_date")
    private Timestamp commitmentDate;

    /**
     * 过期了
     */
    @JSONField(name = "is_expired")
    @JsonProperty("is_expired")
    private String isExpired;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 剩余确认天数
     */
    @JSONField(name = "remaining_validity_days")
    @JsonProperty("remaining_validity_days")
    private Integer remainingValidityDays;

    /**
     * 预计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expected_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expected_date")
    private Timestamp expectedDate;

    /**
     * 采购订单号
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 门户访问网址
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;

    /**
     * 订单关联
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 可选产品行
     */
    @JSONField(name = "sale_order_option_ids")
    @JsonProperty("sale_order_option_ids")
    private String saleOrderOptionIds;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 确认日期
     */
    @DEField(name = "confirmation_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "confirmation_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("confirmation_date")
    private Timestamp confirmationDate;

    /**
     * 条款和条件
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 按组分配税额
     */
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;

    /**
     * 拣货
     */
    @JSONField(name = "picking_ids")
    @JsonProperty("picking_ids")
    private String pickingIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 总计
     */
    @DEField(name = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 客户订单号
     */
    @DEField(name = "client_order_ref")
    @JSONField(name = "client_order_ref")
    @JsonProperty("client_order_ref")
    private String clientOrderRef;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 购物车恢复EMail已发送
     */
    @DEField(name = "cart_recovery_email_sent")
    @JSONField(name = "cart_recovery_email_sent")
    @JsonProperty("cart_recovery_email_sent")
    private String cartRecoveryEmailSent;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 税率
     */
    @DEField(name = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private Double amountTax;

    /**
     * 费用
     */
    @JSONField(name = "expense_ids")
    @JsonProperty("expense_ids")
    private String expenseIds;

    /**
     * 发票地址
     */
    @JSONField(name = "partner_invoice_id_text")
    @JsonProperty("partner_invoice_id_text")
    private String partnerInvoiceIdText;

    /**
     * 贸易条款
     */
    @JSONField(name = "incoterm_text")
    @JsonProperty("incoterm_text")
    private String incotermText;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;

    /**
     * 客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 营销
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 送货地址
     */
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    private String partnerShippingIdText;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    private String opportunityIdText;

    /**
     * 来源
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;

    /**
     * 媒体
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 税科目调整
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 价格表
     */
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;

    /**
     * 付款条款
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    private String paymentTermIdText;

    /**
     * 报价单模板
     */
    @JSONField(name = "sale_order_template_id_text")
    @JsonProperty("sale_order_template_id_text")
    private String saleOrderTemplateIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 发票地址
     */
    @DEField(name = "partner_invoice_id")
    @JSONField(name = "partner_invoice_id")
    @JsonProperty("partner_invoice_id")
    private Integer partnerInvoiceId;

    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Integer fiscalPositionId;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;

    /**
     * 来源
     */
    @DEField(name = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    /**
     * 报价单模板
     */
    @DEField(name = "sale_order_template_id")
    @JSONField(name = "sale_order_template_id")
    @JsonProperty("sale_order_template_id")
    private Integer saleOrderTemplateId;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Integer analyticAccountId;

    /**
     * 付款条款
     */
    @DEField(name = "payment_term_id")
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    private Integer paymentTermId;

    /**
     * 送货地址
     */
    @DEField(name = "partner_shipping_id")
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    private Integer partnerShippingId;

    /**
     * 商机
     */
    @DEField(name = "opportunity_id")
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    private Integer opportunityId;

    /**
     * 贸易条款
     */
    @JSONField(name = "incoterm")
    @JsonProperty("incoterm")
    private Integer incoterm;

    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;


    /**
     * 
     */
    @JSONField(name = "odooanalyticaccount")
    @JsonProperty("odooanalyticaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JSONField(name = "odoofiscalposition")
    @JsonProperty("odoofiscalposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JSONField(name = "odooincoterm")
    @JsonProperty("odooincoterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JSONField(name = "odoopaymentterm")
    @JsonProperty("odoopaymentterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term odooPaymentTerm;

    /**
     * 
     */
    @JSONField(name = "odooopportunity")
    @JsonProperty("odooopportunity")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead odooOpportunity;

    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoopricelist")
    @JsonProperty("odoopricelist")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartnerinvoice")
    @JsonProperty("odoopartnerinvoice")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartnerInvoice;

    /**
     * 
     */
    @JSONField(name = "odoopartnershipping")
    @JsonProperty("odoopartnershipping")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartnerShipping;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoosaleordertemplate")
    @JsonProperty("odoosaleordertemplate")
    private cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template odooSaleOrderTemplate;

    /**
     * 
     */
    @JSONField(name = "odoowarehouse")
    @JsonProperty("odoowarehouse")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooWarehouse;

    /**
     * 
     */
    @JSONField(name = "odoocampaign")
    @JsonProperty("odoocampaign")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomedium")
    @JsonProperty("odoomedium")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JSONField(name = "odoosource")
    @JsonProperty("odoosource")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source odooSource;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLines;



    /**
     * 设置 [单据日期]
     */
    public void setDateOrder(Timestamp dateOrder){
        this.dateOrder = dateOrder ;
        this.modify("date_order",dateOrder);
    }
    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(Double amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }
    /**
     * 设置 [验证]
     */
    public void setValidityDate(Timestamp validityDate){
        this.validityDate = validityDate ;
        this.modify("validity_date",validityDate);
    }
    /**
     * 设置 [警告]
     */
    public void setWarningStock(String warningStock){
        this.warningStock = warningStock ;
        this.modify("warning_stock",warningStock);
    }
    /**
     * 设置 [实际日期]
     */
    public void setEffectiveDate(Timestamp effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [在线签名]
     */
    public void setRequireSignature(String requireSignature){
        this.requireSignature = requireSignature ;
        this.modify("require_signature",requireSignature);
    }
    /**
     * 设置 [汇率]
     */
    public void setCurrencyRate(Double currencyRate){
        this.currencyRate = currencyRate ;
        this.modify("currency_rate",currencyRate);
    }
    /**
     * 设置 [送货策略]
     */
    public void setPickingPolicy(String pickingPolicy){
        this.pickingPolicy = pickingPolicy ;
        this.modify("picking_policy",pickingPolicy);
    }
    /**
     * 设置 [补货组]
     */
    public void setProcurementGroupId(Integer procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [已签核]
     */
    public void setSignedBy(String signedBy){
        this.signedBy = signedBy ;
        this.modify("signed_by",signedBy);
    }
    /**
     * 设置 [在线支付]
     */
    public void setRequirePayment(String requirePayment){
        this.requirePayment = requirePayment ;
        this.modify("require_payment",requirePayment);
    }
    /**
     * 设置 [付款参考:]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }
    /**
     * 设置 [发票状态]
     */
    public void setInvoiceStatus(String invoiceStatus){
        this.invoiceStatus = invoiceStatus ;
        this.modify("invoice_status",invoiceStatus);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [承诺日期]
     */
    public void setCommitmentDate(Timestamp commitmentDate){
        this.commitmentDate = commitmentDate ;
        this.modify("commitment_date",commitmentDate);
    }
    /**
     * 设置 [订单关联]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [确认日期]
     */
    public void setConfirmationDate(Timestamp confirmationDate){
        this.confirmationDate = confirmationDate ;
        this.modify("confirmation_date",confirmationDate);
    }
    /**
     * 设置 [条款和条件]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }
    /**
     * 设置 [总计]
     */
    public void setAmountTotal(Double amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }
    /**
     * 设置 [客户订单号]
     */
    public void setClientOrderRef(String clientOrderRef){
        this.clientOrderRef = clientOrderRef ;
        this.modify("client_order_ref",clientOrderRef);
    }
    /**
     * 设置 [购物车恢复EMail已发送]
     */
    public void setCartRecoveryEmailSent(String cartRecoveryEmailSent){
        this.cartRecoveryEmailSent = cartRecoveryEmailSent ;
        this.modify("cart_recovery_email_sent",cartRecoveryEmailSent);
    }
    /**
     * 设置 [税率]
     */
    public void setAmountTax(Double amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }
    /**
     * 设置 [发票地址]
     */
    public void setPartnerInvoiceId(Integer partnerInvoiceId){
        this.partnerInvoiceId = partnerInvoiceId ;
        this.modify("partner_invoice_id",partnerInvoiceId);
    }
    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Integer fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [营销]
     */
    public void setCampaignId(Integer campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }
    /**
     * 设置 [来源]
     */
    public void setSourceId(Integer sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }
    /**
     * 设置 [报价单模板]
     */
    public void setSaleOrderTemplateId(Integer saleOrderTemplateId){
        this.saleOrderTemplateId = saleOrderTemplateId ;
        this.modify("sale_order_template_id",saleOrderTemplateId);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [媒体]
     */
    public void setMediumId(Integer mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Integer analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }
    /**
     * 设置 [付款条款]
     */
    public void setPaymentTermId(Integer paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }
    /**
     * 设置 [送货地址]
     */
    public void setPartnerShippingId(Integer partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }
    /**
     * 设置 [商机]
     */
    public void setOpportunityId(Integer opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }
    /**
     * 设置 [贸易条款]
     */
    public void setIncoterm(Integer incoterm){
        this.incoterm = incoterm ;
        this.modify("incoterm",incoterm);
    }
    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Integer pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }
    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Integer warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

}



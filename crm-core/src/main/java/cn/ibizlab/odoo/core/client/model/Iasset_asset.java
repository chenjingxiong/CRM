package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [asset_asset] 对象
 */
public interface Iasset_asset {

    /**
     * 获取 [省/ 州]
     */
    public void setAccounting_state_id(Integer accounting_state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getAccounting_state_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getAccounting_state_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setAccounting_state_id_text(String accounting_state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getAccounting_state_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getAccounting_state_id_textDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [Asset Number]
     */
    public void setAsset_number(String asset_number);
    
    /**
     * 设置 [Asset Number]
     */
    public String getAsset_number();

    /**
     * 获取 [Asset Number]脏标记
     */
    public boolean getAsset_numberDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setCategory_ids(String category_ids);
    
    /**
     * 设置 [标签]
     */
    public String getCategory_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getCategory_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [Criticality]
     */
    public void setCriticality(String criticality);
    
    /**
     * 设置 [Criticality]
     */
    public String getCriticality();

    /**
     * 获取 [Criticality]脏标记
     */
    public boolean getCriticalityDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setFinance_state_id(Integer finance_state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getFinance_state_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getFinance_state_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setFinance_state_id_text(String finance_state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getFinance_state_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getFinance_state_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸图像]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸图像]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸图像]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸图像]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸图像]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [Maintenance Date]
     */
    public void setMaintenance_date(Timestamp maintenance_date);
    
    /**
     * 设置 [Maintenance Date]
     */
    public Timestamp getMaintenance_date();

    /**
     * 获取 [Maintenance Date]脏标记
     */
    public boolean getMaintenance_dateDirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setMaintenance_state_color(String maintenance_state_color);
    
    /**
     * 设置 [颜色]
     */
    public String getMaintenance_state_color();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getMaintenance_state_colorDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setMaintenance_state_id(Integer maintenance_state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getMaintenance_state_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getMaintenance_state_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setMaintenance_state_id_text(String maintenance_state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getMaintenance_state_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getMaintenance_state_id_textDirtyFlag();
    /**
     * 获取 [Manufacturer]
     */
    public void setManufacturer_id(Integer manufacturer_id);
    
    /**
     * 设置 [Manufacturer]
     */
    public Integer getManufacturer_id();

    /**
     * 获取 [Manufacturer]脏标记
     */
    public boolean getManufacturer_idDirtyFlag();
    /**
     * 获取 [Manufacturer]
     */
    public void setManufacturer_id_text(String manufacturer_id_text);
    
    /**
     * 设置 [Manufacturer]
     */
    public String getManufacturer_id_text();

    /**
     * 获取 [Manufacturer]脏标记
     */
    public boolean getManufacturer_id_textDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setManufacture_state_id(Integer manufacture_state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getManufacture_state_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getManufacture_state_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setManufacture_state_id_text(String manufacture_state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getManufacture_state_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getManufacture_state_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误个数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误个数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误个数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [前置操作]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [前置操作]
     */
    public String getMessage_needaction();

    /**
     * 获取 [前置操作]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作次数]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作次数]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作次数]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [Meter]
     */
    public void setMeter_ids(String meter_ids);
    
    /**
     * 设置 [Meter]
     */
    public String getMeter_ids();

    /**
     * 获取 [Meter]脏标记
     */
    public boolean getMeter_idsDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [模型]
     */
    public String getModel();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [# Maintenance]
     */
    public void setMro_count(Integer mro_count);
    
    /**
     * 设置 [# Maintenance]
     */
    public Integer getMro_count();

    /**
     * 获取 [# Maintenance]脏标记
     */
    public boolean getMro_countDirtyFlag();
    /**
     * 获取 [Asset Name]
     */
    public void setName(String name);
    
    /**
     * 设置 [Asset Name]
     */
    public String getName();

    /**
     * 获取 [Asset Name]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [Map]
     */
    public void setPosition(String position);
    
    /**
     * 设置 [Map]
     */
    public String getPosition();

    /**
     * 获取 [Map]脏标记
     */
    public boolean getPositionDirtyFlag();
    /**
     * 获取 [Asset Location]
     */
    public void setProperty_stock_asset(Integer property_stock_asset);
    
    /**
     * 设置 [Asset Location]
     */
    public Integer getProperty_stock_asset();

    /**
     * 获取 [Asset Location]脏标记
     */
    public boolean getProperty_stock_assetDirtyFlag();
    /**
     * 获取 [Purchase Date]
     */
    public void setPurchase_date(Timestamp purchase_date);
    
    /**
     * 设置 [Purchase Date]
     */
    public Timestamp getPurchase_date();

    /**
     * 获取 [Purchase Date]脏标记
     */
    public boolean getPurchase_dateDirtyFlag();
    /**
     * 获取 [Serial no.]
     */
    public void setSerial(String serial);
    
    /**
     * 设置 [Serial no.]
     */
    public String getSerial();

    /**
     * 获取 [Serial no.]脏标记
     */
    public boolean getSerialDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [分派给]
     */
    public Integer getUser_id();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [分派给]
     */
    public String getUser_id_text();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setVendor_id(Integer vendor_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getVendor_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getVendor_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setVendor_id_text(String vendor_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getVendor_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getVendor_id_textDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setWarehouse_state_id(Integer warehouse_state_id);
    
    /**
     * 设置 [省/ 州]
     */
    public Integer getWarehouse_state_id();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getWarehouse_state_idDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setWarehouse_state_id_text(String warehouse_state_id_text);
    
    /**
     * 设置 [省/ 州]
     */
    public String getWarehouse_state_id_text();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getWarehouse_state_id_textDirtyFlag();
    /**
     * 获取 [Warranty End]
     */
    public void setWarranty_end_date(Timestamp warranty_end_date);
    
    /**
     * 设置 [Warranty End]
     */
    public Timestamp getWarranty_end_date();

    /**
     * 获取 [Warranty End]脏标记
     */
    public boolean getWarranty_end_dateDirtyFlag();
    /**
     * 获取 [Warranty Start]
     */
    public void setWarranty_start_date(Timestamp warranty_start_date);
    
    /**
     * 设置 [Warranty Start]
     */
    public Timestamp getWarranty_start_date();

    /**
     * 获取 [Warranty Start]脏标记
     */
    public boolean getWarranty_start_dateDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

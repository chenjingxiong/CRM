package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
@Component
public class mro_pm_rule_lineFallback implements mro_pm_rule_lineFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Mro_pm_rule_line create(Mro_pm_rule_line mro_pm_rule_line){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_rule_line> mro_pm_rule_lines){
            return false;
     }


    public Page<Mro_pm_rule_line> searchDefault(Mro_pm_rule_lineSearchContext context){
            return null;
     }


    public Mro_pm_rule_line update(Integer id, Mro_pm_rule_line mro_pm_rule_line){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_rule_line> mro_pm_rule_lines){
            return false;
     }


    public Mro_pm_rule_line get(Integer id){
            return null;
     }


    public Page<Mro_pm_rule_line> select(){
            return null;
     }

    public Mro_pm_rule_line getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_employeeFeignClient;

/**
 * 实体[员工] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_employeeServiceImpl implements IHr_employeeService {

    @Autowired
    hr_employeeFeignClient hr_employeeFeignClient;


    @Override
    public boolean create(Hr_employee et) {
        Hr_employee rt = hr_employeeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_employee> list){
        hr_employeeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_employeeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_employeeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_employee et) {
        Hr_employee rt = hr_employeeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_employee> list){
        hr_employeeFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_employee get(Integer id) {
		Hr_employee et=hr_employeeFeignClient.get(id);
        if(et==null){
            et=new Hr_employee();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Hr_employee getDraft(Hr_employee et) {
        et=hr_employeeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_employee> searchDefault(Hr_employeeSearchContext context) {
        Page<Hr_employee> hr_employees=hr_employeeFeignClient.searchDefault(context);
        return hr_employees;
    }


}



package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_valueSearchContext;


/**
 * 实体[Product_attribute_value] 服务对象接口
 */
public interface IProduct_attribute_valueService{

    boolean create(Product_attribute_value et) ;
    void createBatch(List<Product_attribute_value> list) ;
    Product_attribute_value getDraft(Product_attribute_value et) ;
    Product_attribute_value get(Integer key) ;
    boolean update(Product_attribute_value et) ;
    void updateBatch(List<Product_attribute_value> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Product_attribute_value> searchDefault(Product_attribute_valueSearchContext context) ;

}




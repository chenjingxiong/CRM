package cn.ibizlab.odoo.core.odoo_payment.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [付款收单方] 对象
 */
@Data
public class Payment_acquirer extends EntityClient implements Serializable {

    /**
     * 支持费用计算
     */
    @JSONField(name = "fees_implemented")
    @JsonProperty("fees_implemented")
    private String feesImplemented;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 完成的信息
     */
    @DEField(name = "done_msg")
    @JSONField(name = "done_msg")
    @JsonProperty("done_msg")
    private String doneMsg;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 支持的支付图标
     */
    @JSONField(name = "payment_icon_ids")
    @JsonProperty("payment_icon_ids")
    private String paymentIconIds;

    /**
     * 在门户/网站可见
     */
    @DEField(name = "website_published")
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 可变的交易费用（百分比）
     */
    @DEField(name = "fees_int_var")
    @JSONField(name = "fees_int_var")
    @JsonProperty("fees_int_var")
    private Double feesIntVar;

    /**
     * 添加额外的费用
     */
    @DEField(name = "fees_active")
    @JSONField(name = "fees_active")
    @JsonProperty("fees_active")
    private String feesActive;

    /**
     * S2S表单模板
     */
    @DEField(name = "registration_view_template_id")
    @JSONField(name = "registration_view_template_id")
    @JsonProperty("registration_view_template_id")
    private Integer registrationViewTemplateId;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 感谢留言
     */
    @DEField(name = "post_msg")
    @JSONField(name = "post_msg")
    @JsonProperty("post_msg")
    private String postMsg;

    /**
     * 环境
     */
    @JSONField(name = "environment")
    @JsonProperty("environment")
    private String environment;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 支持保存卡片资料
     */
    @JSONField(name = "token_implemented")
    @JsonProperty("token_implemented")
    private String tokenImplemented;

    /**
     * 国内固定费用
     */
    @DEField(name = "fees_dom_fixed")
    @JSONField(name = "fees_dom_fixed")
    @JsonProperty("fees_dom_fixed")
    private Double feesDomFixed;

    /**
     * 固定的手续费
     */
    @DEField(name = "fees_int_fixed")
    @JSONField(name = "fees_int_fixed")
    @JsonProperty("fees_int_fixed")
    private Double feesIntFixed;

    /**
     * 对应模块
     */
    @DEField(name = "module_id")
    @JSONField(name = "module_id")
    @JsonProperty("module_id")
    private Integer moduleId;

    /**
     * 批准支持的机制
     */
    @JSONField(name = "authorize_implemented")
    @JsonProperty("authorize_implemented")
    private String authorizeImplemented;

    /**
     * 立即支付
     */
    @DEField(name = "payment_flow")
    @JSONField(name = "payment_flow")
    @JsonProperty("payment_flow")
    private String paymentFlow;

    /**
     * 特定国家/地区
     */
    @DEField(name = "specific_countries")
    @JSONField(name = "specific_countries")
    @JsonProperty("specific_countries")
    private String specificCountries;

    /**
     * 动态内部费用(百分比)
     */
    @DEField(name = "fees_dom_var")
    @JSONField(name = "fees_dom_var")
    @JsonProperty("fees_dom_var")
    private Double feesDomVar;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 帮助信息
     */
    @DEField(name = "pre_msg")
    @JSONField(name = "pre_msg")
    @JsonProperty("pre_msg")
    private String preMsg;

    /**
     * 国家
     */
    @JSONField(name = "country_ids")
    @JsonProperty("country_ids")
    private String countryIds;

    /**
     * 使用SEPA QR 二维码
     */
    @DEField(name = "qr_code")
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 待定消息
     */
    @DEField(name = "pending_msg")
    @JSONField(name = "pending_msg")
    @JsonProperty("pending_msg")
    private String pendingMsg;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 窗体按钮模板
     */
    @DEField(name = "view_template_id")
    @JSONField(name = "view_template_id")
    @JsonProperty("view_template_id")
    private Integer viewTemplateId;

    /**
     * 未收款
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;

    /**
     * 交流
     */
    @DEField(name = "so_reference_type")
    @JSONField(name = "so_reference_type")
    @JsonProperty("so_reference_type")
    private String soReferenceType;

    /**
     * 安装状态
     */
    @JSONField(name = "module_state")
    @JsonProperty("module_state")
    private String moduleState;

    /**
     * 取消消息
     */
    @DEField(name = "cancel_msg")
    @JSONField(name = "cancel_msg")
    @JsonProperty("cancel_msg")
    private String cancelMsg;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 服务商
     */
    @JSONField(name = "provider")
    @JsonProperty("provider")
    private String provider;

    /**
     * 保存卡
     */
    @DEField(name = "save_token")
    @JSONField(name = "save_token")
    @JsonProperty("save_token")
    private String saveToken;

    /**
     * 手动获取金额
     */
    @DEField(name = "capture_manually")
    @JSONField(name = "capture_manually")
    @JsonProperty("capture_manually")
    private String captureManually;

    /**
     * 错误消息
     */
    @DEField(name = "error_msg")
    @JSONField(name = "error_msg")
    @JsonProperty("error_msg")
    private String errorMsg;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 付款日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 付款日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [完成的信息]
     */
    public void setDoneMsg(String doneMsg){
        this.doneMsg = doneMsg ;
        this.modify("done_msg",doneMsg);
    }
    /**
     * 设置 [在门户/网站可见]
     */
    public void setWebsitePublished(String websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }
    /**
     * 设置 [可变的交易费用（百分比）]
     */
    public void setFeesIntVar(Double feesIntVar){
        this.feesIntVar = feesIntVar ;
        this.modify("fees_int_var",feesIntVar);
    }
    /**
     * 设置 [添加额外的费用]
     */
    public void setFeesActive(String feesActive){
        this.feesActive = feesActive ;
        this.modify("fees_active",feesActive);
    }
    /**
     * 设置 [S2S表单模板]
     */
    public void setRegistrationViewTemplateId(Integer registrationViewTemplateId){
        this.registrationViewTemplateId = registrationViewTemplateId ;
        this.modify("registration_view_template_id",registrationViewTemplateId);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [感谢留言]
     */
    public void setPostMsg(String postMsg){
        this.postMsg = postMsg ;
        this.modify("post_msg",postMsg);
    }
    /**
     * 设置 [环境]
     */
    public void setEnvironment(String environment){
        this.environment = environment ;
        this.modify("environment",environment);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [国内固定费用]
     */
    public void setFeesDomFixed(Double feesDomFixed){
        this.feesDomFixed = feesDomFixed ;
        this.modify("fees_dom_fixed",feesDomFixed);
    }
    /**
     * 设置 [固定的手续费]
     */
    public void setFeesIntFixed(Double feesIntFixed){
        this.feesIntFixed = feesIntFixed ;
        this.modify("fees_int_fixed",feesIntFixed);
    }
    /**
     * 设置 [对应模块]
     */
    public void setModuleId(Integer moduleId){
        this.moduleId = moduleId ;
        this.modify("module_id",moduleId);
    }
    /**
     * 设置 [立即支付]
     */
    public void setPaymentFlow(String paymentFlow){
        this.paymentFlow = paymentFlow ;
        this.modify("payment_flow",paymentFlow);
    }
    /**
     * 设置 [特定国家/地区]
     */
    public void setSpecificCountries(String specificCountries){
        this.specificCountries = specificCountries ;
        this.modify("specific_countries",specificCountries);
    }
    /**
     * 设置 [动态内部费用(百分比)]
     */
    public void setFeesDomVar(Double feesDomVar){
        this.feesDomVar = feesDomVar ;
        this.modify("fees_dom_var",feesDomVar);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [帮助信息]
     */
    public void setPreMsg(String preMsg){
        this.preMsg = preMsg ;
        this.modify("pre_msg",preMsg);
    }
    /**
     * 设置 [使用SEPA QR 二维码]
     */
    public void setQrCode(String qrCode){
        this.qrCode = qrCode ;
        this.modify("qr_code",qrCode);
    }
    /**
     * 设置 [待定消息]
     */
    public void setPendingMsg(String pendingMsg){
        this.pendingMsg = pendingMsg ;
        this.modify("pending_msg",pendingMsg);
    }
    /**
     * 设置 [窗体按钮模板]
     */
    public void setViewTemplateId(Integer viewTemplateId){
        this.viewTemplateId = viewTemplateId ;
        this.modify("view_template_id",viewTemplateId);
    }
    /**
     * 设置 [交流]
     */
    public void setSoReferenceType(String soReferenceType){
        this.soReferenceType = soReferenceType ;
        this.modify("so_reference_type",soReferenceType);
    }
    /**
     * 设置 [取消消息]
     */
    public void setCancelMsg(String cancelMsg){
        this.cancelMsg = cancelMsg ;
        this.modify("cancel_msg",cancelMsg);
    }
    /**
     * 设置 [服务商]
     */
    public void setProvider(String provider){
        this.provider = provider ;
        this.modify("provider",provider);
    }
    /**
     * 设置 [保存卡]
     */
    public void setSaveToken(String saveToken){
        this.saveToken = saveToken ;
        this.modify("save_token",saveToken);
    }
    /**
     * 设置 [手动获取金额]
     */
    public void setCaptureManually(String captureManually){
        this.captureManually = captureManually ;
        this.modify("capture_manually",captureManually);
    }
    /**
     * 设置 [错误消息]
     */
    public void setErrorMsg(String errorMsg){
        this.errorMsg = errorMsg ;
        this.modify("error_msg",errorMsg);
    }
    /**
     * 设置 [付款日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

}



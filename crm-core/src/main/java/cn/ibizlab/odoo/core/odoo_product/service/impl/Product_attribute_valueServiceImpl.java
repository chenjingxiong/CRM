package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_attribute_valueFeignClient;

/**
 * 实体[属性值] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attribute_valueServiceImpl implements IProduct_attribute_valueService {

    @Autowired
    product_attribute_valueFeignClient product_attribute_valueFeignClient;


    @Override
    public boolean create(Product_attribute_value et) {
        Product_attribute_value rt = product_attribute_valueFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute_value> list){
        product_attribute_valueFeignClient.createBatch(list) ;
    }

    @Override
    public Product_attribute_value getDraft(Product_attribute_value et) {
        et=product_attribute_valueFeignClient.getDraft();
        return et;
    }

    @Override
    public Product_attribute_value get(Integer id) {
		Product_attribute_value et=product_attribute_valueFeignClient.get(id);
        if(et==null){
            et=new Product_attribute_value();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Product_attribute_value et) {
        Product_attribute_value rt = product_attribute_valueFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_attribute_value> list){
        product_attribute_valueFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_attribute_valueFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_attribute_valueFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute_value> searchDefault(Product_attribute_valueSearchContext context) {
        Page<Product_attribute_value> product_attribute_values=product_attribute_valueFeignClient.searchDefault(context);
        return product_attribute_values;
    }


}



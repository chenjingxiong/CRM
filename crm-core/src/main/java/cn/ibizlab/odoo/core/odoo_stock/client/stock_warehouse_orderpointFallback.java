package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
@Component
public class stock_warehouse_orderpointFallback implements stock_warehouse_orderpointFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Stock_warehouse_orderpoint get(Integer id){
            return null;
     }



    public Stock_warehouse_orderpoint update(Integer id, Stock_warehouse_orderpoint stock_warehouse_orderpoint){
            return null;
     }
    public Boolean updateBatch(List<Stock_warehouse_orderpoint> stock_warehouse_orderpoints){
            return false;
     }


    public Stock_warehouse_orderpoint create(Stock_warehouse_orderpoint stock_warehouse_orderpoint){
            return null;
     }
    public Boolean createBatch(List<Stock_warehouse_orderpoint> stock_warehouse_orderpoints){
            return false;
     }

    public Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context){
            return null;
     }


    public Page<Stock_warehouse_orderpoint> select(){
            return null;
     }

    public Stock_warehouse_orderpoint getDraft(){
            return null;
    }



}

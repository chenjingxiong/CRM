package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;


/**
 * 实体[Mail_resend_message] 服务对象接口
 */
public interface IMail_resend_messageService{

    Mail_resend_message get(Integer key) ;
    Mail_resend_message getDraft(Mail_resend_message et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_resend_message et) ;
    void createBatch(List<Mail_resend_message> list) ;
    boolean update(Mail_resend_message et) ;
    void updateBatch(List<Mail_resend_message> list) ;
    Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context) ;

}




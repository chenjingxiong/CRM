package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_update_translations] 服务对象接口
 */
@Component
public class base_update_translationsFallback implements base_update_translationsFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_update_translations update(Integer id, Base_update_translations base_update_translations){
            return null;
     }
    public Boolean updateBatch(List<Base_update_translations> base_update_translations){
            return false;
     }


    public Base_update_translations create(Base_update_translations base_update_translations){
            return null;
     }
    public Boolean createBatch(List<Base_update_translations> base_update_translations){
            return false;
     }


    public Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context){
            return null;
     }




    public Base_update_translations get(Integer id){
            return null;
     }


    public Page<Base_update_translations> select(){
            return null;
     }

    public Base_update_translations getDraft(){
            return null;
    }



}

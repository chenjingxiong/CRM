package cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_equipment_category;

import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.validator.maintenance_equipment_category.Maintenance_equipment_categoryMaintenance_countDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Maintenance_equipment_category
 * 属性：Maintenance_count
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Maintenance_equipment_categoryMaintenance_countDefaultValidator.class})
public @interface Maintenance_equipment_categoryMaintenance_countDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

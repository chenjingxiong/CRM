package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_attendanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_attendanceFeignClient;

/**
 * 实体[出勤] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_attendanceServiceImpl implements IHr_attendanceService {

    @Autowired
    hr_attendanceFeignClient hr_attendanceFeignClient;


    @Override
    public Hr_attendance getDraft(Hr_attendance et) {
        et=hr_attendanceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_attendanceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_attendanceFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Hr_attendance et) {
        Hr_attendance rt = hr_attendanceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_attendance> list){
        hr_attendanceFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Hr_attendance et) {
        Hr_attendance rt = hr_attendanceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_attendance> list){
        hr_attendanceFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_attendance get(Integer id) {
		Hr_attendance et=hr_attendanceFeignClient.get(id);
        if(et==null){
            et=new Hr_attendance();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context) {
        Page<Hr_attendance> hr_attendances=hr_attendanceFeignClient.searchDefault(context);
        return hr_attendances;
    }


}



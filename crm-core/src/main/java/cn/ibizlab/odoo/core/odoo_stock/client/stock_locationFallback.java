package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_location] 服务对象接口
 */
@Component
public class stock_locationFallback implements stock_locationFeignClient{

    public Stock_location update(Integer id, Stock_location stock_location){
            return null;
     }
    public Boolean updateBatch(List<Stock_location> stock_locations){
            return false;
     }


    public Stock_location create(Stock_location stock_location){
            return null;
     }
    public Boolean createBatch(List<Stock_location> stock_locations){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Stock_location get(Integer id){
            return null;
     }



    public Page<Stock_location> searchDefault(Stock_locationSearchContext context){
            return null;
     }



    public Page<Stock_location> select(){
            return null;
     }

    public Stock_location getDraft(){
            return null;
    }



}

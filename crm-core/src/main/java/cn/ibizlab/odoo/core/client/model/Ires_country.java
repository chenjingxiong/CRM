package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_country] 对象
 */
public interface Ires_country {

    /**
     * 获取 [报表布局]
     */
    public void setAddress_format(String address_format);
    
    /**
     * 设置 [报表布局]
     */
    public String getAddress_format();

    /**
     * 获取 [报表布局]脏标记
     */
    public boolean getAddress_formatDirtyFlag();
    /**
     * 获取 [输入视图]
     */
    public void setAddress_view_id(Integer address_view_id);
    
    /**
     * 设置 [输入视图]
     */
    public Integer getAddress_view_id();

    /**
     * 获取 [输入视图]脏标记
     */
    public boolean getAddress_view_idDirtyFlag();
    /**
     * 获取 [国家/地区代码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [国家/地区代码]
     */
    public String getCode();

    /**
     * 获取 [国家/地区代码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [国家/地区分组]
     */
    public void setCountry_group_ids(String country_group_ids);
    
    /**
     * 设置 [国家/地区分组]
     */
    public String getCountry_group_ids();

    /**
     * 获取 [国家/地区分组]脏标记
     */
    public boolean getCountry_group_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [国家/地区名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [国家/地区名称]
     */
    public String getName();

    /**
     * 获取 [国家/地区名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [客户姓名位置]
     */
    public void setName_position(String name_position);
    
    /**
     * 设置 [客户姓名位置]
     */
    public String getName_position();

    /**
     * 获取 [客户姓名位置]脏标记
     */
    public boolean getName_positionDirtyFlag();
    /**
     * 获取 [国家/地区长途区号]
     */
    public void setPhone_code(Integer phone_code);
    
    /**
     * 设置 [国家/地区长途区号]
     */
    public Integer getPhone_code();

    /**
     * 获取 [国家/地区长途区号]脏标记
     */
    public boolean getPhone_codeDirtyFlag();
    /**
     * 获取 [省份]
     */
    public void setState_ids(String state_ids);
    
    /**
     * 设置 [省份]
     */
    public String getState_ids();

    /**
     * 获取 [省份]脏标记
     */
    public boolean getState_idsDirtyFlag();
    /**
     * 获取 [增值税标签]
     */
    public void setVat_label(String vat_label);
    
    /**
     * 设置 [增值税标签]
     */
    public String getVat_label();

    /**
     * 获取 [增值税标签]脏标记
     */
    public boolean getVat_labelDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品种类] 对象
 */
@Data
public class Product_category extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 库存计价科目
     */
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 下级类别
     */
    @JSONField(name = "child_id")
    @JsonProperty("child_id")
    private String childId;

    /**
     * # 产品
     */
    @JSONField(name = "product_count")
    @JsonProperty("product_count")
    private Integer productCount;

    /**
     * 路线
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 库存计价
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    private String propertyValuation;

    /**
     * 父级路径
     */
    @DEField(name = "parent_path")
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 完整名称
     */
    @DEField(name = "complete_name")
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    private String completeName;

    /**
     * 价格差异科目
     */
    @JSONField(name = "property_account_creditor_price_difference_categ")
    @JsonProperty("property_account_creditor_price_difference_categ")
    private Integer propertyAccountCreditorPriceDifferenceCateg;

    /**
     * 库存出货科目
     */
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 路线合计
     */
    @JSONField(name = "total_route_ids")
    @JsonProperty("total_route_ids")
    private String totalRouteIds;

    /**
     * 成本方法
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    private String propertyCostMethod;

    /**
     * 收入科目
     */
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    private Integer propertyAccountIncomeCategId;

    /**
     * 费用科目
     */
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    private Integer propertyAccountExpenseCategId;

    /**
     * 库存日记账
     */
    @JSONField(name = "property_stock_journal")
    @JsonProperty("property_stock_journal")
    private Integer propertyStockJournal;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 库存进货科目
     */
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 上级类别
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 强制下架策略
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    private String removalStrategyIdText;

    /**
     * 上级类别
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 强制下架策略
     */
    @DEField(name = "removal_strategy_id")
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    private Integer removalStrategyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_category odooParent;

    /**
     * 
     */
    @JSONField(name = "odooremovalstrategy")
    @JsonProperty("odooremovalstrategy")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_removal odooRemovalStrategy;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [父级路径]
     */
    public void setParentPath(String parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }
    /**
     * 设置 [完整名称]
     */
    public void setCompleteName(String completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }
    /**
     * 设置 [上级类别]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [强制下架策略]
     */
    public void setRemovalStrategyId(Integer removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }

}



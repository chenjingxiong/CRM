package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workcenter_productivity_loss;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_workcenter_productivity_loss.Mrp_workcenter_productivity_lossLoss_typeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_workcenter_productivity_loss
 * 属性：Loss_type
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_workcenter_productivity_lossLoss_typeDefaultValidator.class})
public @interface Mrp_workcenter_productivity_lossLoss_typeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

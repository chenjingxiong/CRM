package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_order_parts_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_order_parts_line] 服务对象接口
 */
public interface Imro_order_parts_lineClientService{

    public Imro_order_parts_line createModel() ;

    public void update(Imro_order_parts_line mro_order_parts_line);

    public Page<Imro_order_parts_line> fetchDefault(SearchContext context);

    public void createBatch(List<Imro_order_parts_line> mro_order_parts_lines);

    public void get(Imro_order_parts_line mro_order_parts_line);

    public void remove(Imro_order_parts_line mro_order_parts_line);

    public void create(Imro_order_parts_line mro_order_parts_line);

    public void updateBatch(List<Imro_order_parts_line> mro_order_parts_lines);

    public void removeBatch(List<Imro_order_parts_line> mro_order_parts_lines);

    public Page<Imro_order_parts_line> select(SearchContext context);

    public void getDraft(Imro_order_parts_line mro_order_parts_line);

}

package cn.ibizlab.odoo.core.odoo_asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_assetService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_asset.client.asset_assetFeignClient;

/**
 * 实体[Asset] 服务对象接口实现
 */
@Slf4j
@Service
public class Asset_assetServiceImpl implements IAsset_assetService {

    @Autowired
    asset_assetFeignClient asset_assetFeignClient;


    @Override
    public boolean update(Asset_asset et) {
        Asset_asset rt = asset_assetFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Asset_asset> list){
        asset_assetFeignClient.updateBatch(list) ;
    }

    @Override
    public Asset_asset get(Integer id) {
		Asset_asset et=asset_assetFeignClient.get(id);
        if(et==null){
            et=new Asset_asset();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=asset_assetFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        asset_assetFeignClient.removeBatch(idList);
    }

    @Override
    public Asset_asset getDraft(Asset_asset et) {
        et=asset_assetFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Asset_asset et) {
        Asset_asset rt = asset_assetFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Asset_asset> list){
        asset_assetFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Asset_asset> searchDefault(Asset_assetSearchContext context) {
        Page<Asset_asset> asset_assets=asset_assetFeignClient.searchDefault(context);
        return asset_assets;
    }


}



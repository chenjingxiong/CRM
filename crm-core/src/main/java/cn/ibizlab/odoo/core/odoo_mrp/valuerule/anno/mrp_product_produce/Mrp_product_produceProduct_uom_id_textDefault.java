package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_product_produce;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_product_produce.Mrp_product_produceProduct_uom_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_product_produce
 * 属性：Product_uom_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_product_produceProduct_uom_id_textDefaultValidator.class})
public @interface Mrp_product_produceProduct_uom_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_partnerSearchContext;


/**
 * 实体[Mail_resend_partner] 服务对象接口
 */
public interface IMail_resend_partnerService{

    Mail_resend_partner getDraft(Mail_resend_partner et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_resend_partner get(Integer key) ;
    boolean create(Mail_resend_partner et) ;
    void createBatch(List<Mail_resend_partner> list) ;
    boolean update(Mail_resend_partner et) ;
    void updateBatch(List<Mail_resend_partner> list) ;
    Page<Mail_resend_partner> searchDefault(Mail_resend_partnerSearchContext context) ;

}




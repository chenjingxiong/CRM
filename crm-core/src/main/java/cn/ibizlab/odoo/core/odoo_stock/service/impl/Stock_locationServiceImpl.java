package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_locationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_locationFeignClient;

/**
 * 实体[库存位置] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_locationServiceImpl implements IStock_locationService {

    @Autowired
    stock_locationFeignClient stock_locationFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_locationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_locationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_location et) {
        Stock_location rt = stock_locationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_location> list){
        stock_locationFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_location get(Integer id) {
		Stock_location et=stock_locationFeignClient.get(id);
        if(et==null){
            et=new Stock_location();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_location et) {
        Stock_location rt = stock_locationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_location> list){
        stock_locationFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_location getDraft(Stock_location et) {
        et=stock_locationFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_location> searchDefault(Stock_locationSearchContext context) {
        Page<Stock_location> stock_locations=stock_locationFeignClient.searchDefault(context);
        return stock_locations;
    }


}



package cn.ibizlab.odoo.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;


/**
 * 实体[Asset_state] 服务对象接口
 */
public interface IAsset_stateService{

    boolean create(Asset_state et) ;
    void createBatch(List<Asset_state> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Asset_state get(Integer key) ;
    boolean update(Asset_state et) ;
    void updateBatch(List<Asset_state> list) ;
    Asset_state getDraft(Asset_state et) ;
    Page<Asset_state> searchDefault(Asset_stateSearchContext context) ;

}




package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_product_configurator;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_product_configurator] 服务对象接口
 */
public interface Isale_product_configuratorClientService{

    public Isale_product_configurator createModel() ;

    public void create(Isale_product_configurator sale_product_configurator);

    public void createBatch(List<Isale_product_configurator> sale_product_configurators);

    public void removeBatch(List<Isale_product_configurator> sale_product_configurators);

    public void get(Isale_product_configurator sale_product_configurator);

    public void update(Isale_product_configurator sale_product_configurator);

    public void updateBatch(List<Isale_product_configurator> sale_product_configurators);

    public Page<Isale_product_configurator> fetchDefault(SearchContext context);

    public void remove(Isale_product_configurator sale_product_configurator);

    public Page<Isale_product_configurator> select(SearchContext context);

    public void getDraft(Isale_product_configurator sale_product_configurator);

}

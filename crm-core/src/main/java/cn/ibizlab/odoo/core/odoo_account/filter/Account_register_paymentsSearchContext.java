package cn.ibizlab.odoo.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Account_register_payments] 查询条件对象
 */
@Slf4j
@Data
public class Account_register_paymentsSearchContext extends SearchContextBase {
	private String n_payment_type_eq;//[付款类型]

	private String n_partner_type_eq;//[业务伙伴类型]

	private String n_payment_difference_handling_eq;//[付款差异处理]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_payment_method_id_text_eq;//[付款方法类型]

	private String n_payment_method_id_text_like;//[付款方法类型]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_journal_id_text_eq;//[付款日记账]

	private String n_journal_id_text_like;//[付款日记账]

	private String n_partner_id_text_eq;//[业务伙伴]

	private String n_partner_id_text_like;//[业务伙伴]

	private String n_writeoff_account_id_text_eq;//[差异科目]

	private String n_writeoff_account_id_text_like;//[差异科目]

	private Integer n_writeoff_account_id_eq;//[差异科目]

	private Integer n_partner_bank_account_id_eq;//[收款银行账号]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_partner_id_eq;//[业务伙伴]

	private Integer n_journal_id_eq;//[付款日记账]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_payment_method_id_eq;//[付款方法类型]

}




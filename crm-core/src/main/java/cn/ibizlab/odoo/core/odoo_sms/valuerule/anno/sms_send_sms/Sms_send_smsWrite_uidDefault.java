package cn.ibizlab.odoo.core.odoo_sms.valuerule.anno.sms_send_sms;

import cn.ibizlab.odoo.core.odoo_sms.valuerule.validator.sms_send_sms.Sms_send_smsWrite_uidDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sms_send_sms
 * 属性：Write_uid
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sms_send_smsWrite_uidDefaultValidator.class})
public @interface Sms_send_smsWrite_uidDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

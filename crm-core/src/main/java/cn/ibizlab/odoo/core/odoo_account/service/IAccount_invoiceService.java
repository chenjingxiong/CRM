package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;


/**
 * 实体[Account_invoice] 服务对象接口
 */
public interface IAccount_invoiceService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean checkKey(Account_invoice et) ;
    boolean create(Account_invoice et) ;
    void createBatch(List<Account_invoice> list) ;
    boolean update(Account_invoice et) ;
    void updateBatch(List<Account_invoice> list) ;
    Account_invoice get(Integer key) ;
    Account_invoice getDraft(Account_invoice et) ;
    boolean save(Account_invoice et) ;
    void saveBatch(List<Account_invoice> list) ;
    Page<Account_invoice> searchDefault(Account_invoiceSearchContext context) ;

}




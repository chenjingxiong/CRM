package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_inventory;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_inventory] 服务对象接口
 */
public interface Istock_inventoryClientService{

    public Istock_inventory createModel() ;

    public void update(Istock_inventory stock_inventory);

    public Page<Istock_inventory> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_inventory> stock_inventories);

    public void remove(Istock_inventory stock_inventory);

    public void create(Istock_inventory stock_inventory);

    public void removeBatch(List<Istock_inventory> stock_inventories);

    public void createBatch(List<Istock_inventory> stock_inventories);

    public void get(Istock_inventory stock_inventory);

    public Page<Istock_inventory> select(SearchContext context);

    public void getDraft(Istock_inventory stock_inventory);

}

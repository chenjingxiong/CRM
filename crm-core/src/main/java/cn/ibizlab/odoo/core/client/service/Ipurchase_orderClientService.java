package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipurchase_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_order] 服务对象接口
 */
public interface Ipurchase_orderClientService{

    public Ipurchase_order createModel() ;

    public void remove(Ipurchase_order purchase_order);

    public Page<Ipurchase_order> fetchDefault(SearchContext context);

    public void update(Ipurchase_order purchase_order);

    public void removeBatch(List<Ipurchase_order> purchase_orders);

    public void createBatch(List<Ipurchase_order> purchase_orders);

    public void updateBatch(List<Ipurchase_order> purchase_orders);

    public void get(Ipurchase_order purchase_order);

    public void create(Ipurchase_order purchase_order);

    public Page<Ipurchase_order> select(SearchContext context);

    public void getDraft(Ipurchase_order purchase_order);

}

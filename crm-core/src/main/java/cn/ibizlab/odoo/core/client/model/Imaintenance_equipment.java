package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [maintenance_equipment] 对象
 */
public interface Imaintenance_equipment {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [分配日期]
     */
    public void setAssign_date(Timestamp assign_date);
    
    /**
     * 设置 [分配日期]
     */
    public Timestamp getAssign_date();

    /**
     * 获取 [分配日期]脏标记
     */
    public boolean getAssign_dateDirtyFlag();
    /**
     * 获取 [设备类别]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [设备类别]
     */
    public Integer getCategory_id();

    /**
     * 获取 [设备类别]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [设备类别]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [设备类别]
     */
    public String getCategory_id_text();

    /**
     * 获取 [设备类别]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [成本]
     */
    public void setCost(Double cost);
    
    /**
     * 设置 [成本]
     */
    public Double getCost();

    /**
     * 获取 [成本]脏标记
     */
    public boolean getCostDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [分配到部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [分配到部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [分配到部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [分配到部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [分配到部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [分配到部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [实际日期]
     */
    public void setEffective_date(Timestamp effective_date);
    
    /**
     * 设置 [实际日期]
     */
    public Timestamp getEffective_date();

    /**
     * 获取 [实际日期]脏标记
     */
    public boolean getEffective_dateDirtyFlag();
    /**
     * 获取 [分配到员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [分配到员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [分配到员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [分配到员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [分配到员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [分配到员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [用于]
     */
    public void setEquipment_assign_to(String equipment_assign_to);
    
    /**
     * 设置 [用于]
     */
    public String getEquipment_assign_to();

    /**
     * 获取 [用于]脏标记
     */
    public boolean getEquipment_assign_toDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation(String location);
    
    /**
     * 设置 [地点]
     */
    public String getLocation();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocationDirtyFlag();
    /**
     * 获取 [维修统计]
     */
    public void setMaintenance_count(Integer maintenance_count);
    
    /**
     * 设置 [维修统计]
     */
    public Integer getMaintenance_count();

    /**
     * 获取 [维修统计]脏标记
     */
    public boolean getMaintenance_countDirtyFlag();
    /**
     * 获取 [保养时长]
     */
    public void setMaintenance_duration(Double maintenance_duration);
    
    /**
     * 设置 [保养时长]
     */
    public Double getMaintenance_duration();

    /**
     * 获取 [保养时长]脏标记
     */
    public boolean getMaintenance_durationDirtyFlag();
    /**
     * 获取 [保养]
     */
    public void setMaintenance_ids(String maintenance_ids);
    
    /**
     * 设置 [保养]
     */
    public String getMaintenance_ids();

    /**
     * 获取 [保养]脏标记
     */
    public boolean getMaintenance_idsDirtyFlag();
    /**
     * 获取 [当前维护]
     */
    public void setMaintenance_open_count(Integer maintenance_open_count);
    
    /**
     * 设置 [当前维护]
     */
    public Integer getMaintenance_open_count();

    /**
     * 获取 [当前维护]脏标记
     */
    public boolean getMaintenance_open_countDirtyFlag();
    /**
     * 获取 [保养团队]
     */
    public void setMaintenance_team_id(Integer maintenance_team_id);
    
    /**
     * 设置 [保养团队]
     */
    public Integer getMaintenance_team_id();

    /**
     * 获取 [保养团队]脏标记
     */
    public boolean getMaintenance_team_idDirtyFlag();
    /**
     * 获取 [保养团队]
     */
    public void setMaintenance_team_id_text(String maintenance_team_id_text);
    
    /**
     * 设置 [保养团队]
     */
    public String getMaintenance_team_id_text();

    /**
     * 获取 [保养团队]脏标记
     */
    public boolean getMaintenance_team_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [型号]
     */
    public void setModel(String model);
    
    /**
     * 设置 [型号]
     */
    public String getModel();

    /**
     * 获取 [型号]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [设备名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [设备名称]
     */
    public String getName();

    /**
     * 获取 [设备名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [下次预防维护日期]
     */
    public void setNext_action_date(Timestamp next_action_date);
    
    /**
     * 设置 [下次预防维护日期]
     */
    public Timestamp getNext_action_date();

    /**
     * 获取 [下次预防维护日期]脏标记
     */
    public boolean getNext_action_dateDirtyFlag();
    /**
     * 获取 [笔记]
     */
    public void setNote(String note);
    
    /**
     * 设置 [笔记]
     */
    public String getNote();

    /**
     * 获取 [笔记]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_user_id(Integer owner_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getOwner_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_user_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_user_id_text(String owner_user_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getOwner_user_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_user_id_textDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getPartner_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getPartner_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [供应商参考]
     */
    public void setPartner_ref(String partner_ref);
    
    /**
     * 设置 [供应商参考]
     */
    public String getPartner_ref();

    /**
     * 获取 [供应商参考]脏标记
     */
    public boolean getPartner_refDirtyFlag();
    /**
     * 获取 [预防维护间隔天数]
     */
    public void setPeriod(Integer period);
    
    /**
     * 设置 [预防维护间隔天数]
     */
    public Integer getPeriod();

    /**
     * 获取 [预防维护间隔天数]脏标记
     */
    public boolean getPeriodDirtyFlag();
    /**
     * 获取 [报废日期]
     */
    public void setScrap_date(Timestamp scrap_date);
    
    /**
     * 设置 [报废日期]
     */
    public Timestamp getScrap_date();

    /**
     * 获取 [报废日期]脏标记
     */
    public boolean getScrap_dateDirtyFlag();
    /**
     * 获取 [序列号]
     */
    public void setSerial_no(String serial_no);
    
    /**
     * 设置 [序列号]
     */
    public String getSerial_no();

    /**
     * 获取 [序列号]脏标记
     */
    public boolean getSerial_noDirtyFlag();
    /**
     * 获取 [技术员]
     */
    public void setTechnician_user_id(Integer technician_user_id);
    
    /**
     * 设置 [技术员]
     */
    public Integer getTechnician_user_id();

    /**
     * 获取 [技术员]脏标记
     */
    public boolean getTechnician_user_idDirtyFlag();
    /**
     * 获取 [技术员]
     */
    public void setTechnician_user_id_text(String technician_user_id_text);
    
    /**
     * 设置 [技术员]
     */
    public String getTechnician_user_id_text();

    /**
     * 获取 [技术员]脏标记
     */
    public boolean getTechnician_user_id_textDirtyFlag();
    /**
     * 获取 [保修截止日期]
     */
    public void setWarranty_date(Timestamp warranty_date);
    
    /**
     * 设置 [保修截止日期]
     */
    public Timestamp getWarranty_date();

    /**
     * 获取 [保修截止日期]脏标记
     */
    public boolean getWarranty_dateDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

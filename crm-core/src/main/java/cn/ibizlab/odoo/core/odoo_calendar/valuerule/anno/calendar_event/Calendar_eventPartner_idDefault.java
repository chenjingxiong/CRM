package cn.ibizlab.odoo.core.odoo_calendar.valuerule.anno.calendar_event;

import cn.ibizlab.odoo.core.odoo_calendar.valuerule.validator.calendar_event.Calendar_eventPartner_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Calendar_event
 * 属性：Partner_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Calendar_eventPartner_idDefaultValidator.class})
public @interface Calendar_eventPartner_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

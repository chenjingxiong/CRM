package cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_pm_rule_line;

import cn.ibizlab.odoo.core.odoo_mro.valuerule.validator.mro_pm_rule_line.Mro_pm_rule_lineMeter_interval_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mro_pm_rule_line
 * 属性：Meter_interval_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mro_pm_rule_lineMeter_interval_id_textDefaultValidator.class})
public @interface Mro_pm_rule_lineMeter_interval_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

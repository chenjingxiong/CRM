package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;


/**
 * 实体[Res_lang] 服务对象接口
 */
public interface IRes_langService{

    Res_lang getDraft(Res_lang et) ;
    boolean update(Res_lang et) ;
    void updateBatch(List<Res_lang> list) ;
    boolean create(Res_lang et) ;
    void createBatch(List<Res_lang> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_lang get(Integer key) ;
    Page<Res_lang> searchDefault(Res_langSearchContext context) ;

}




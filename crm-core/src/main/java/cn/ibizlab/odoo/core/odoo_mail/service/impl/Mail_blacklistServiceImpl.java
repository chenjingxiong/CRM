package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklistService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_blacklistFeignClient;

/**
 * 实体[邮件黑名单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_blacklistServiceImpl implements IMail_blacklistService {

    @Autowired
    mail_blacklistFeignClient mail_blacklistFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_blacklistFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_blacklistFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_blacklist get(Integer id) {
		Mail_blacklist et=mail_blacklistFeignClient.get(id);
        if(et==null){
            et=new Mail_blacklist();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_blacklist et) {
        Mail_blacklist rt = mail_blacklistFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_blacklist> list){
        mail_blacklistFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_blacklist et) {
        Mail_blacklist rt = mail_blacklistFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_blacklist> list){
        mail_blacklistFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_blacklist getDraft(Mail_blacklist et) {
        et=mail_blacklistFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_blacklist> searchDefault(Mail_blacklistSearchContext context) {
        Page<Mail_blacklist> mail_blacklists=mail_blacklistFeignClient.searchDefault(context);
        return mail_blacklists;
    }


}



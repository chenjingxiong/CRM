package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_position] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-fiscal-position", fallback = account_fiscal_positionFallback.class)
public interface account_fiscal_positionFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/{id}")
    Account_fiscal_position update(@PathVariable("id") Integer id,@RequestBody Account_fiscal_position account_fiscal_position);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_position> account_fiscal_positions);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/{id}")
    Account_fiscal_position get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/searchdefault")
    Page<Account_fiscal_position> searchDefault(@RequestBody Account_fiscal_positionSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions")
    Account_fiscal_position create(@RequestBody Account_fiscal_position account_fiscal_position);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_position> account_fiscal_positions);



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/select")
    Page<Account_fiscal_position> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/getdraft")
    Account_fiscal_position getDraft();


}

package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_test] 服务对象接口
 */
@FeignClient(value = "odoo-resource", contextId = "resource-test", fallback = resource_testFallback.class)
public interface resource_testFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_tests")
    Resource_test create(@RequestBody Resource_test resource_test);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_tests/batch")
    Boolean createBatch(@RequestBody List<Resource_test> resource_tests);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_tests/searchdefault")
    Page<Resource_test> searchDefault(@RequestBody Resource_testSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/{id}")
    Resource_test update(@PathVariable("id") Integer id,@RequestBody Resource_test resource_test);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/batch")
    Boolean updateBatch(@RequestBody List<Resource_test> resource_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_tests/{id}")
    Resource_test get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.GET, value = "/resource_tests/select")
    Page<Resource_test> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_tests/getdraft")
    Resource_test getDraft();


}

package cn.ibizlab.odoo.core.odoo_snailmail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[snailmail_letter] 服务对象接口
 */
@Component
public class snailmail_letterFallback implements snailmail_letterFeignClient{

    public Snailmail_letter create(Snailmail_letter snailmail_letter){
            return null;
     }
    public Boolean createBatch(List<Snailmail_letter> snailmail_letters){
            return false;
     }

    public Snailmail_letter get(Integer id){
            return null;
     }


    public Snailmail_letter update(Integer id, Snailmail_letter snailmail_letter){
            return null;
     }
    public Boolean updateBatch(List<Snailmail_letter> snailmail_letters){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context){
            return null;
     }





    public Page<Snailmail_letter> select(){
            return null;
     }

    public Snailmail_letter getDraft(){
            return null;
    }



}

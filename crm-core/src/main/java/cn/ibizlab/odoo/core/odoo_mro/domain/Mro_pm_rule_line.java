package cn.ibizlab.odoo.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Rule for Task] 对象
 */
@Data
public class Mro_pm_rule_line extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * PM Rule
     */
    @JSONField(name = "pm_rule_id_text")
    @JsonProperty("pm_rule_id_text")
    private String pmRuleIdText;

    /**
     * Meter Interval
     */
    @JSONField(name = "meter_interval_id_text")
    @JsonProperty("meter_interval_id_text")
    private String meterIntervalIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * Task
     */
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    private String taskIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * Task
     */
    @DEField(name = "task_id")
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    private Integer taskId;

    /**
     * Meter Interval
     */
    @DEField(name = "meter_interval_id")
    @JSONField(name = "meter_interval_id")
    @JsonProperty("meter_interval_id")
    private Integer meterIntervalId;

    /**
     * PM Rule
     */
    @DEField(name = "pm_rule_id")
    @JSONField(name = "pm_rule_id")
    @JsonProperty("pm_rule_id")
    private Integer pmRuleId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoometerinterval")
    @JsonProperty("odoometerinterval")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval odooMeterInterval;

    /**
     * 
     */
    @JSONField(name = "odoopmrule")
    @JsonProperty("odoopmrule")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule odooPmRule;

    /**
     * 
     */
    @JSONField(name = "odootask")
    @JsonProperty("odootask")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task odooTask;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [Task]
     */
    public void setTaskId(Integer taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }
    /**
     * 设置 [Meter Interval]
     */
    public void setMeterIntervalId(Integer meterIntervalId){
        this.meterIntervalId = meterIntervalId ;
        this.modify("meter_interval_id",meterIntervalId);
    }
    /**
     * 设置 [PM Rule]
     */
    public void setPmRuleId(Integer pmRuleId){
        this.pmRuleId = pmRuleId ;
        this.modify("pm_rule_id",pmRuleId);
    }

}



package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_stage] 服务对象接口
 */
@Component
public class crm_stageFallback implements crm_stageFeignClient{

    public Crm_stage get(Integer id){
            return null;
     }



    public Crm_stage update(Integer id, Crm_stage crm_stage){
            return null;
     }
    public Boolean updateBatch(List<Crm_stage> crm_stages){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Crm_stage create(Crm_stage crm_stage){
            return null;
     }
    public Boolean createBatch(List<Crm_stage> crm_stages){
            return false;
     }

    public Page<Crm_stage> searchDefault(Crm_stageSearchContext context){
            return null;
     }



    public Page<Crm_stage> select(){
            return null;
     }

    public Crm_stage getDraft(){
            return null;
    }



}

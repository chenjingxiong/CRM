package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_redirect] 服务对象接口
 */
@Component
public class website_redirectFallback implements website_redirectFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Website_redirect update(Integer id, Website_redirect website_redirect){
            return null;
     }
    public Boolean updateBatch(List<Website_redirect> website_redirects){
            return false;
     }


    public Page<Website_redirect> searchDefault(Website_redirectSearchContext context){
            return null;
     }


    public Website_redirect get(Integer id){
            return null;
     }




    public Website_redirect create(Website_redirect website_redirect){
            return null;
     }
    public Boolean createBatch(List<Website_redirect> website_redirects){
            return false;
     }


    public Page<Website_redirect> select(){
            return null;
     }

    public Website_redirect getDraft(){
            return null;
    }



}

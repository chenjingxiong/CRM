package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_mail_registration] 服务对象接口
 */
@FeignClient(value = "odoo-event", contextId = "event-mail-registration", fallback = event_mail_registrationFallback.class)
public interface event_mail_registrationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/{id}")
    Event_mail_registration update(@PathVariable("id") Integer id,@RequestBody Event_mail_registration event_mail_registration);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/batch")
    Boolean updateBatch(@RequestBody List<Event_mail_registration> event_mail_registrations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations")
    Event_mail_registration create(@RequestBody Event_mail_registration event_mail_registration);

    @RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/batch")
    Boolean createBatch(@RequestBody List<Event_mail_registration> event_mail_registrations);



    @RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/searchdefault")
    Page<Event_mail_registration> searchDefault(@RequestBody Event_mail_registrationSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/{id}")
    Event_mail_registration get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/select")
    Page<Event_mail_registration> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/getdraft")
    Event_mail_registration getDraft();


}

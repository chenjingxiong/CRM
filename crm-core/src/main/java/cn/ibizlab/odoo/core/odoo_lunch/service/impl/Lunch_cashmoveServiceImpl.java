package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_cashmoveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_cashmoveFeignClient;

/**
 * 实体[午餐费的现金划转] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_cashmoveServiceImpl implements ILunch_cashmoveService {

    @Autowired
    lunch_cashmoveFeignClient lunch_cashmoveFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_cashmoveFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_cashmoveFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Lunch_cashmove et) {
        Lunch_cashmove rt = lunch_cashmoveFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_cashmove> list){
        lunch_cashmoveFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Lunch_cashmove et) {
        Lunch_cashmove rt = lunch_cashmoveFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_cashmove> list){
        lunch_cashmoveFeignClient.createBatch(list) ;
    }

    @Override
    public Lunch_cashmove get(Integer id) {
		Lunch_cashmove et=lunch_cashmoveFeignClient.get(id);
        if(et==null){
            et=new Lunch_cashmove();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Lunch_cashmove getDraft(Lunch_cashmove et) {
        et=lunch_cashmoveFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context) {
        Page<Lunch_cashmove> lunch_cashmoves=lunch_cashmoveFeignClient.searchDefault(context);
        return lunch_cashmoves;
    }


}



package cn.ibizlab.odoo.core.odoo_rating.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;


/**
 * 实体[Rating_mixin] 服务对象接口
 */
public interface IRating_mixinService{

    Rating_mixin getDraft(Rating_mixin et) ;
    Rating_mixin get(Integer key) ;
    boolean create(Rating_mixin et) ;
    void createBatch(List<Rating_mixin> list) ;
    boolean update(Rating_mixin et) ;
    void updateBatch(List<Rating_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context) ;

}




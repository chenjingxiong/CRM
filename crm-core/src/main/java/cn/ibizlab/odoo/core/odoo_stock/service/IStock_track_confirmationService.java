package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;


/**
 * 实体[Stock_track_confirmation] 服务对象接口
 */
public interface IStock_track_confirmationService{

    boolean create(Stock_track_confirmation et) ;
    void createBatch(List<Stock_track_confirmation> list) ;
    Stock_track_confirmation get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_track_confirmation getDraft(Stock_track_confirmation et) ;
    boolean update(Stock_track_confirmation et) ;
    void updateBatch(List<Stock_track_confirmation> list) ;
    Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context) ;

}




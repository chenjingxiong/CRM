package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warn_insufficient_qty] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-warn-insufficient-qty", fallback = stock_warn_insufficient_qtyFallback.class)
public interface stock_warn_insufficient_qtyFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qties/{id}")
    Stock_warn_insufficient_qty update(@PathVariable("id") Integer id,@RequestBody Stock_warn_insufficient_qty stock_warn_insufficient_qty);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qties/batch")
    Boolean updateBatch(@RequestBody List<Stock_warn_insufficient_qty> stock_warn_insufficient_qties);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/searchdefault")
    Page<Stock_warn_insufficient_qty> searchDefault(@RequestBody Stock_warn_insufficient_qtySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qties/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qties/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qties/{id}")
    Stock_warn_insufficient_qty get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties")
    Stock_warn_insufficient_qty create(@RequestBody Stock_warn_insufficient_qty stock_warn_insufficient_qty);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/batch")
    Boolean createBatch(@RequestBody List<Stock_warn_insufficient_qty> stock_warn_insufficient_qties);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qties/select")
    Page<Stock_warn_insufficient_qty> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qties/getdraft")
    Stock_warn_insufficient_qty getDraft();


}

package cn.ibizlab.odoo.core.odoo_board.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_board.domain.Board_board;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;


/**
 * 实体[Board_board] 服务对象接口
 */
public interface IBoard_boardService{

    Board_board getDraft(Board_board et) ;
    boolean update(Board_board et) ;
    void updateBatch(List<Board_board> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Board_board et) ;
    void createBatch(List<Board_board> list) ;
    Board_board get(Integer key) ;
    Page<Board_board> searchDefault(Board_boardSearchContext context) ;

}




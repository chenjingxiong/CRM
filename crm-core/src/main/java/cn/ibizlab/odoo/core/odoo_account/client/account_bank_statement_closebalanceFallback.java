package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
@Component
public class account_bank_statement_closebalanceFallback implements account_bank_statement_closebalanceFeignClient{

    public Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context){
            return null;
     }


    public Account_bank_statement_closebalance update(Integer id, Account_bank_statement_closebalance account_bank_statement_closebalance){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement_closebalance> account_bank_statement_closebalances){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_bank_statement_closebalance get(Integer id){
            return null;
     }


    public Account_bank_statement_closebalance create(Account_bank_statement_closebalance account_bank_statement_closebalance){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement_closebalance> account_bank_statement_closebalances){
            return false;
     }




    public Page<Account_bank_statement_closebalance> select(){
            return null;
     }

    public Account_bank_statement_closebalance getDraft(){
            return null;
    }



}

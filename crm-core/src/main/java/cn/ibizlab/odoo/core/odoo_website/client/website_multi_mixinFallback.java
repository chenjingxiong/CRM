package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_multi_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_multi_mixin] 服务对象接口
 */
@Component
public class website_multi_mixinFallback implements website_multi_mixinFeignClient{

    public Website_multi_mixin get(Integer id){
            return null;
     }


    public Website_multi_mixin create(Website_multi_mixin website_multi_mixin){
            return null;
     }
    public Boolean createBatch(List<Website_multi_mixin> website_multi_mixins){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Website_multi_mixin> searchDefault(Website_multi_mixinSearchContext context){
            return null;
     }




    public Website_multi_mixin update(Integer id, Website_multi_mixin website_multi_mixin){
            return null;
     }
    public Boolean updateBatch(List<Website_multi_mixin> website_multi_mixins){
            return false;
     }



    public Page<Website_multi_mixin> select(){
            return null;
     }

    public Website_multi_mixin getDraft(){
            return null;
    }



}

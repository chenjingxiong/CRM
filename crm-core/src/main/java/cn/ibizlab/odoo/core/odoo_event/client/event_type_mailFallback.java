package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_type_mail] 服务对象接口
 */
@Component
public class event_type_mailFallback implements event_type_mailFeignClient{


    public Event_type_mail update(Integer id, Event_type_mail event_type_mail){
            return null;
     }
    public Boolean updateBatch(List<Event_type_mail> event_type_mails){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context){
            return null;
     }


    public Event_type_mail get(Integer id){
            return null;
     }


    public Event_type_mail create(Event_type_mail event_type_mail){
            return null;
     }
    public Boolean createBatch(List<Event_type_mail> event_type_mails){
            return false;
     }

    public Page<Event_type_mail> select(){
            return null;
     }

    public Event_type_mail getDraft(){
            return null;
    }



}

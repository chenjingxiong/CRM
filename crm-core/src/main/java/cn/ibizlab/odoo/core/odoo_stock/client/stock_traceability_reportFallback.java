package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_traceability_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_traceability_report] 服务对象接口
 */
@Component
public class stock_traceability_reportFallback implements stock_traceability_reportFeignClient{

    public Stock_traceability_report create(Stock_traceability_report stock_traceability_report){
            return null;
     }
    public Boolean createBatch(List<Stock_traceability_report> stock_traceability_reports){
            return false;
     }

    public Stock_traceability_report update(Integer id, Stock_traceability_report stock_traceability_report){
            return null;
     }
    public Boolean updateBatch(List<Stock_traceability_report> stock_traceability_reports){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Stock_traceability_report get(Integer id){
            return null;
     }


    public Page<Stock_traceability_report> searchDefault(Stock_traceability_reportSearchContext context){
            return null;
     }


    public Page<Stock_traceability_report> select(){
            return null;
     }

    public Stock_traceability_report getDraft(){
            return null;
    }



}

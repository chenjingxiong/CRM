package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scrapService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_scrapFeignClient;

/**
 * 实体[报废] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_scrapServiceImpl implements IStock_scrapService {

    @Autowired
    stock_scrapFeignClient stock_scrapFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_scrapFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_scrapFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_scrap et) {
        Stock_scrap rt = stock_scrapFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_scrap> list){
        stock_scrapFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_scrap getDraft(Stock_scrap et) {
        et=stock_scrapFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_scrap et) {
        Stock_scrap rt = stock_scrapFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_scrap> list){
        stock_scrapFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_scrap get(Integer id) {
		Stock_scrap et=stock_scrapFeignClient.get(id);
        if(et==null){
            et=new Stock_scrap();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context) {
        Page<Stock_scrap> stock_scraps=stock_scrapFeignClient.searchDefault(context);
        return stock_scraps;
    }


}



package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_common_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_common_reportFeignClient;

/**
 * 实体[账户通用报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_common_reportServiceImpl implements IAccount_common_reportService {

    @Autowired
    account_common_reportFeignClient account_common_reportFeignClient;


    @Override
    public Account_common_report get(Integer id) {
		Account_common_report et=account_common_reportFeignClient.get(id);
        if(et==null){
            et=new Account_common_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_common_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_common_reportFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_common_report et) {
        Account_common_report rt = account_common_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_common_report> list){
        account_common_reportFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_common_report et) {
        Account_common_report rt = account_common_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_common_report> list){
        account_common_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_common_report getDraft(Account_common_report et) {
        et=account_common_reportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_common_report> searchDefault(Account_common_reportSearchContext context) {
        Page<Account_common_report> account_common_reports=account_common_reportFeignClient.searchDefault(context);
        return account_common_reports;
    }


}



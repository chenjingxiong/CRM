package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;


/**
 * 实体[Mrp_unbuild] 服务对象接口
 */
public interface IMrp_unbuildService{

    boolean update(Mrp_unbuild et) ;
    void updateBatch(List<Mrp_unbuild> list) ;
    Mrp_unbuild getDraft(Mrp_unbuild et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_unbuild get(Integer key) ;
    boolean create(Mrp_unbuild et) ;
    void createBatch(List<Mrp_unbuild> list) ;
    Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_cancelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_resend_cancelFeignClient;

/**
 * 实体[重发请求被拒绝] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_resend_cancelServiceImpl implements IMail_resend_cancelService {

    @Autowired
    mail_resend_cancelFeignClient mail_resend_cancelFeignClient;


    @Override
    public Mail_resend_cancel getDraft(Mail_resend_cancel et) {
        et=mail_resend_cancelFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_resend_cancel et) {
        Mail_resend_cancel rt = mail_resend_cancelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_resend_cancel> list){
        mail_resend_cancelFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_resend_cancelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_resend_cancelFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_resend_cancel et) {
        Mail_resend_cancel rt = mail_resend_cancelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_resend_cancel> list){
        mail_resend_cancelFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_resend_cancel get(Integer id) {
		Mail_resend_cancel et=mail_resend_cancelFeignClient.get(id);
        if(et==null){
            et=new Mail_resend_cancel();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context) {
        Page<Mail_resend_cancel> mail_resend_cancels=mail_resend_cancelFeignClient.searchDefault(context);
        return mail_resend_cancels;
    }


}



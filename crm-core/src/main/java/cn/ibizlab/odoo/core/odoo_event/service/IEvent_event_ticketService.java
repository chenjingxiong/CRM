package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;


/**
 * 实体[Event_event_ticket] 服务对象接口
 */
public interface IEvent_event_ticketService{

    Event_event_ticket getDraft(Event_event_ticket et) ;
    boolean update(Event_event_ticket et) ;
    void updateBatch(List<Event_event_ticket> list) ;
    Event_event_ticket get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Event_event_ticket et) ;
    void createBatch(List<Event_event_ticket> list) ;
    Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_language_install] 服务对象接口
 */
@Component
public class base_language_installFallback implements base_language_installFeignClient{


    public Base_language_install update(Integer id, Base_language_install base_language_install){
            return null;
     }
    public Boolean updateBatch(List<Base_language_install> base_language_installs){
            return false;
     }


    public Page<Base_language_install> searchDefault(Base_language_installSearchContext context){
            return null;
     }




    public Base_language_install create(Base_language_install base_language_install){
            return null;
     }
    public Boolean createBatch(List<Base_language_install> base_language_installs){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_language_install get(Integer id){
            return null;
     }


    public Page<Base_language_install> select(){
            return null;
     }

    public Base_language_install getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[asset_category] 服务对象接口
 */
@FeignClient(value = "odoo-asset", contextId = "asset-category", fallback = asset_categoryFallback.class)
public interface asset_categoryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/asset_categories")
    Asset_category create(@RequestBody Asset_category asset_category);

    @RequestMapping(method = RequestMethod.POST, value = "/asset_categories/batch")
    Boolean createBatch(@RequestBody List<Asset_category> asset_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/asset_categories/{id}")
    Asset_category get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/asset_categories/searchdefault")
    Page<Asset_category> searchDefault(@RequestBody Asset_categorySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/{id}")
    Asset_category update(@PathVariable("id") Integer id,@RequestBody Asset_category asset_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/batch")
    Boolean updateBatch(@RequestBody List<Asset_category> asset_categories);



    @RequestMapping(method = RequestMethod.GET, value = "/asset_categories/select")
    Page<Asset_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/asset_categories/getdraft")
    Asset_category getDraft();


}

package cn.ibizlab.odoo.core.odoo_asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_asset.client.asset_categoryFeignClient;

/**
 * 实体[Asset Tags] 服务对象接口实现
 */
@Slf4j
@Service
public class Asset_categoryServiceImpl implements IAsset_categoryService {

    @Autowired
    asset_categoryFeignClient asset_categoryFeignClient;


    @Override
    public Asset_category get(Integer id) {
		Asset_category et=asset_categoryFeignClient.get(id);
        if(et==null){
            et=new Asset_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=asset_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        asset_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public Asset_category getDraft(Asset_category et) {
        et=asset_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Asset_category et) {
        Asset_category rt = asset_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Asset_category> list){
        asset_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Asset_category et) {
        Asset_category rt = asset_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Asset_category> list){
        asset_categoryFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Asset_category> searchDefault(Asset_categorySearchContext context) {
        Page<Asset_category> asset_categorys=asset_categoryFeignClient.searchDefault(context);
        return asset_categorys;
    }


}



package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_channel_ruleFeignClient;

/**
 * 实体[实时聊天频道规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_channel_ruleServiceImpl implements IIm_livechat_channel_ruleService {

    @Autowired
    im_livechat_channel_ruleFeignClient im_livechat_channel_ruleFeignClient;


    @Override
    public Im_livechat_channel_rule get(Integer id) {
		Im_livechat_channel_rule et=im_livechat_channel_ruleFeignClient.get(id);
        if(et==null){
            et=new Im_livechat_channel_rule();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Im_livechat_channel_rule et) {
        Im_livechat_channel_rule rt = im_livechat_channel_ruleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Im_livechat_channel_rule> list){
        im_livechat_channel_ruleFeignClient.updateBatch(list) ;
    }

    @Override
    public Im_livechat_channel_rule getDraft(Im_livechat_channel_rule et) {
        et=im_livechat_channel_ruleFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Im_livechat_channel_rule et) {
        Im_livechat_channel_rule rt = im_livechat_channel_ruleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_channel_rule> list){
        im_livechat_channel_ruleFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=im_livechat_channel_ruleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        im_livechat_channel_ruleFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context) {
        Page<Im_livechat_channel_rule> im_livechat_channel_rules=im_livechat_channel_ruleFeignClient.searchDefault(context);
        return im_livechat_channel_rules;
    }


}



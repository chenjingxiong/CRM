package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;


/**
 * 实体[Crm_stage] 服务对象接口
 */
public interface ICrm_stageService{

    boolean create(Crm_stage et) ;
    void createBatch(List<Crm_stage> list) ;
    Crm_stage getDraft(Crm_stage et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Crm_stage et) ;
    void updateBatch(List<Crm_stage> list) ;
    Crm_stage get(Integer key) ;
    Page<Crm_stage> searchDefault(Crm_stageSearchContext context) ;

}




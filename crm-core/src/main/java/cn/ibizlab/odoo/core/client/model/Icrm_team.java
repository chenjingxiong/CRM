package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [crm_team] 对象
 */
public interface Icrm_team {

    /**
     * 获取 [遗弃购物车数量]
     */
    public void setAbandoned_carts_amount(Integer abandoned_carts_amount);
    
    /**
     * 设置 [遗弃购物车数量]
     */
    public Integer getAbandoned_carts_amount();

    /**
     * 获取 [遗弃购物车数量]脏标记
     */
    public boolean getAbandoned_carts_amountDirtyFlag();
    /**
     * 获取 [遗弃购物车数量]
     */
    public void setAbandoned_carts_count(Integer abandoned_carts_count);
    
    /**
     * 设置 [遗弃购物车数量]
     */
    public Integer getAbandoned_carts_count();

    /**
     * 获取 [遗弃购物车数量]脏标记
     */
    public boolean getAbandoned_carts_countDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [安全联系人别名]
     */
    public void setAlias_contact(String alias_contact);
    
    /**
     * 设置 [安全联系人别名]
     */
    public String getAlias_contact();

    /**
     * 获取 [安全联系人别名]脏标记
     */
    public boolean getAlias_contactDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setAlias_defaults(String alias_defaults);
    
    /**
     * 设置 [默认值]
     */
    public String getAlias_defaults();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getAlias_defaultsDirtyFlag();
    /**
     * 获取 [网域别名]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [网域别名]
     */
    public String getAlias_domain();

    /**
     * 获取 [网域别名]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [记录线索ID]
     */
    public void setAlias_force_thread_id(Integer alias_force_thread_id);
    
    /**
     * 设置 [记录线索ID]
     */
    public Integer getAlias_force_thread_id();

    /**
     * 获取 [记录线索ID]脏标记
     */
    public boolean getAlias_force_thread_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [模型别名]
     */
    public void setAlias_model_id(Integer alias_model_id);
    
    /**
     * 设置 [模型别名]
     */
    public Integer getAlias_model_id();

    /**
     * 获取 [模型别名]脏标记
     */
    public boolean getAlias_model_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [别名]
     */
    public String getAlias_name();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [上级模型]
     */
    public void setAlias_parent_model_id(Integer alias_parent_model_id);
    
    /**
     * 设置 [上级模型]
     */
    public Integer getAlias_parent_model_id();

    /**
     * 获取 [上级模型]脏标记
     */
    public boolean getAlias_parent_model_idDirtyFlag();
    /**
     * 获取 [上级记录ID]
     */
    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);
    
    /**
     * 设置 [上级记录ID]
     */
    public Integer getAlias_parent_thread_id();

    /**
     * 获取 [上级记录ID]脏标记
     */
    public boolean getAlias_parent_thread_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setAlias_user_id(Integer alias_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getAlias_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getAlias_user_idDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [仪表板按钮]
     */
    public void setDashboard_button_name(String dashboard_button_name);
    
    /**
     * 设置 [仪表板按钮]
     */
    public String getDashboard_button_name();

    /**
     * 获取 [仪表板按钮]脏标记
     */
    public boolean getDashboard_button_nameDirtyFlag();
    /**
     * 获取 [数据仪表图]
     */
    public void setDashboard_graph_data(String dashboard_graph_data);
    
    /**
     * 设置 [数据仪表图]
     */
    public String getDashboard_graph_data();

    /**
     * 获取 [数据仪表图]脏标记
     */
    public boolean getDashboard_graph_dataDirtyFlag();
    /**
     * 获取 [分组]
     */
    public void setDashboard_graph_group(String dashboard_graph_group);
    
    /**
     * 设置 [分组]
     */
    public String getDashboard_graph_group();

    /**
     * 获取 [分组]脏标记
     */
    public boolean getDashboard_graph_groupDirtyFlag();
    /**
     * 获取 [分组方式]
     */
    public void setDashboard_graph_group_pipeline(String dashboard_graph_group_pipeline);
    
    /**
     * 设置 [分组方式]
     */
    public String getDashboard_graph_group_pipeline();

    /**
     * 获取 [分组方式]脏标记
     */
    public boolean getDashboard_graph_group_pipelineDirtyFlag();
    /**
     * 获取 [POS分组]
     */
    public void setDashboard_graph_group_pos(String dashboard_graph_group_pos);
    
    /**
     * 设置 [POS分组]
     */
    public String getDashboard_graph_group_pos();

    /**
     * 获取 [POS分组]脏标记
     */
    public boolean getDashboard_graph_group_posDirtyFlag();
    /**
     * 获取 [内容]
     */
    public void setDashboard_graph_model(String dashboard_graph_model);
    
    /**
     * 设置 [内容]
     */
    public String getDashboard_graph_model();

    /**
     * 获取 [内容]脏标记
     */
    public boolean getDashboard_graph_modelDirtyFlag();
    /**
     * 获取 [比例]
     */
    public void setDashboard_graph_period(String dashboard_graph_period);
    
    /**
     * 设置 [比例]
     */
    public String getDashboard_graph_period();

    /**
     * 获取 [比例]脏标记
     */
    public boolean getDashboard_graph_periodDirtyFlag();
    /**
     * 获取 [预计关闭]
     */
    public void setDashboard_graph_period_pipeline(String dashboard_graph_period_pipeline);
    
    /**
     * 设置 [预计关闭]
     */
    public String getDashboard_graph_period_pipeline();

    /**
     * 获取 [预计关闭]脏标记
     */
    public boolean getDashboard_graph_period_pipelineDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setDashboard_graph_type(String dashboard_graph_type);
    
    /**
     * 设置 [类型]
     */
    public String getDashboard_graph_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getDashboard_graph_typeDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [最喜欢的成员]
     */
    public void setFavorite_user_ids(String favorite_user_ids);
    
    /**
     * 设置 [最喜欢的成员]
     */
    public String getFavorite_user_ids();

    /**
     * 获取 [最喜欢的成员]脏标记
     */
    public boolean getFavorite_user_idsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [本月已开发票]
     */
    public void setInvoiced(Integer invoiced);
    
    /**
     * 设置 [本月已开发票]
     */
    public Integer getInvoiced();

    /**
     * 获取 [本月已开发票]脏标记
     */
    public boolean getInvoicedDirtyFlag();
    /**
     * 获取 [发票目标]
     */
    public void setInvoiced_target(Integer invoiced_target);
    
    /**
     * 设置 [发票目标]
     */
    public Integer getInvoiced_target();

    /**
     * 获取 [发票目标]脏标记
     */
    public boolean getInvoiced_targetDirtyFlag();
    /**
     * 获取 [显示仪表]
     */
    public void setIs_favorite(String is_favorite);
    
    /**
     * 设置 [显示仪表]
     */
    public String getIs_favorite();

    /**
     * 获取 [显示仪表]脏标记
     */
    public boolean getIs_favoriteDirtyFlag();
    /**
     * 获取 [渠道人员]
     */
    public void setMember_ids(String member_ids);
    
    /**
     * 设置 [渠道人员]
     */
    public String getMember_ids();

    /**
     * 获取 [渠道人员]脏标记
     */
    public boolean getMember_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setName(String name);
    
    /**
     * 设置 [销售团队]
     */
    public String getName();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [商机收入]
     */
    public void setOpportunities_amount(Integer opportunities_amount);
    
    /**
     * 设置 [商机收入]
     */
    public Integer getOpportunities_amount();

    /**
     * 获取 [商机收入]脏标记
     */
    public boolean getOpportunities_amountDirtyFlag();
    /**
     * 获取 [开启的商机数]
     */
    public void setOpportunities_count(Integer opportunities_count);
    
    /**
     * 设置 [开启的商机数]
     */
    public Integer getOpportunities_count();

    /**
     * 获取 [开启的商机数]脏标记
     */
    public boolean getOpportunities_countDirtyFlag();
    /**
     * 获取 [POS]
     */
    public void setPos_config_ids(String pos_config_ids);
    
    /**
     * 设置 [POS]
     */
    public String getPos_config_ids();

    /**
     * 获取 [POS]脏标记
     */
    public boolean getPos_config_idsDirtyFlag();
    /**
     * 获取 [会议销售金额]
     */
    public void setPos_order_amount_total(Double pos_order_amount_total);
    
    /**
     * 设置 [会议销售金额]
     */
    public Double getPos_order_amount_total();

    /**
     * 获取 [会议销售金额]脏标记
     */
    public boolean getPos_order_amount_totalDirtyFlag();
    /**
     * 获取 [开放POS会议]
     */
    public void setPos_sessions_open_count(Integer pos_sessions_open_count);
    
    /**
     * 设置 [开放POS会议]
     */
    public Integer getPos_sessions_open_count();

    /**
     * 获取 [开放POS会议]脏标记
     */
    public boolean getPos_sessions_open_countDirtyFlag();
    /**
     * 获取 [发票金额]
     */
    public void setQuotations_amount(Integer quotations_amount);
    
    /**
     * 设置 [发票金额]
     */
    public Integer getQuotations_amount();

    /**
     * 获取 [发票金额]脏标记
     */
    public boolean getQuotations_amountDirtyFlag();
    /**
     * 获取 [发票报价单]
     */
    public void setQuotations_count(Integer quotations_count);
    
    /**
     * 设置 [发票报价单]
     */
    public Integer getQuotations_count();

    /**
     * 获取 [发票报价单]脏标记
     */
    public boolean getQuotations_countDirtyFlag();
    /**
     * 获取 [回复 至]
     */
    public void setReply_to(String reply_to);
    
    /**
     * 设置 [回复 至]
     */
    public String getReply_to();

    /**
     * 获取 [回复 至]脏标记
     */
    public boolean getReply_toDirtyFlag();
    /**
     * 获取 [发票销售单]
     */
    public void setSales_to_invoice_count(Integer sales_to_invoice_count);
    
    /**
     * 设置 [发票销售单]
     */
    public Integer getSales_to_invoice_count();

    /**
     * 获取 [发票销售单]脏标记
     */
    public boolean getSales_to_invoice_countDirtyFlag();
    /**
     * 获取 [团队类型]
     */
    public void setTeam_type(String team_type);
    
    /**
     * 设置 [团队类型]
     */
    public String getTeam_type();

    /**
     * 获取 [团队类型]脏标记
     */
    public boolean getTeam_typeDirtyFlag();
    /**
     * 获取 [未分派线索]
     */
    public void setUnassigned_leads_count(Integer unassigned_leads_count);
    
    /**
     * 设置 [未分派线索]
     */
    public Integer getUnassigned_leads_count();

    /**
     * 获取 [未分派线索]脏标记
     */
    public boolean getUnassigned_leads_countDirtyFlag();
    /**
     * 获取 [团队负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [团队负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [团队负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [团队负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [团队负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [团队负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [设定开票目标]
     */
    public void setUse_invoices(String use_invoices);
    
    /**
     * 设置 [设定开票目标]
     */
    public String getUse_invoices();

    /**
     * 获取 [设定开票目标]脏标记
     */
    public boolean getUse_invoicesDirtyFlag();
    /**
     * 获取 [线索]
     */
    public void setUse_leads(String use_leads);
    
    /**
     * 设置 [线索]
     */
    public String getUse_leads();

    /**
     * 获取 [线索]脏标记
     */
    public boolean getUse_leadsDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setUse_opportunities(String use_opportunities);
    
    /**
     * 设置 [渠道]
     */
    public String getUse_opportunities();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getUse_opportunitiesDirtyFlag();
    /**
     * 获取 [报价单]
     */
    public void setUse_quotations(String use_quotations);
    
    /**
     * 设置 [报价单]
     */
    public String getUse_quotations();

    /**
     * 获取 [报价单]脏标记
     */
    public boolean getUse_quotationsDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_ids(String website_ids);
    
    /**
     * 设置 [网站]
     */
    public String getWebsite_ids();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idsDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

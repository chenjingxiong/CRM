package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [报废] 对象
 */
@Data
public class Stock_scrap extends EntityClient implements Serializable {

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 预计日期
     */
    @DEField(name = "date_expected")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    private Timestamp dateExpected;

    /**
     * 数量
     */
    @DEField(name = "scrap_qty")
    @JSONField(name = "scrap_qty")
    @JsonProperty("scrap_qty")
    private Double scrapQty;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 编号
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 工单
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 报废位置
     */
    @JSONField(name = "scrap_location_id_text")
    @JsonProperty("scrap_location_id_text")
    private String scrapLocationIdText;

    /**
     * 批次
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 报废移动
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 分拣
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 制造订单
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 追踪
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 包裹
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 所有者
     */
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 批次
     */
    @DEField(name = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;

    /**
     * 制造订单
     */
    @DEField(name = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 分拣
     */
    @DEField(name = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 报废位置
     */
    @DEField(name = "scrap_location_id")
    @JSONField(name = "scrap_location_id")
    @JsonProperty("scrap_location_id")
    private Integer scrapLocationId;

    /**
     * 报废移动
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 工单
     */
    @DEField(name = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 包裹
     */
    @DEField(name = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Integer packageId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 所有者
     */
    @DEField(name = "owner_id")
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Integer ownerId;


    /**
     * 
     */
    @JSONField(name = "odooproduction")
    @JsonProperty("odooproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JSONField(name = "odooworkorder")
    @JsonProperty("odooworkorder")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odooowner")
    @JsonProperty("odooowner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooOwner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odooscraplocation")
    @JsonProperty("odooscraplocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooScrapLocation;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoopicking")
    @JsonProperty("odoopicking")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JSONField(name = "odoolot")
    @JsonProperty("odoolot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JSONField(name = "odoopackage")
    @JsonProperty("odoopackage")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package odooPackage;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [预计日期]
     */
    public void setDateExpected(Timestamp dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }
    /**
     * 设置 [数量]
     */
    public void setScrapQty(Double scrapQty){
        this.scrapQty = scrapQty ;
        this.modify("scrap_qty",scrapQty);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [编号]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [批次]
     */
    public void setLotId(Integer lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }
    /**
     * 设置 [制造订单]
     */
    public void setProductionId(Integer productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }
    /**
     * 设置 [分拣]
     */
    public void setPickingId(Integer pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }
    /**
     * 设置 [报废位置]
     */
    public void setScrapLocationId(Integer scrapLocationId){
        this.scrapLocationId = scrapLocationId ;
        this.modify("scrap_location_id",scrapLocationId);
    }
    /**
     * 设置 [报废移动]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [工单]
     */
    public void setWorkorderId(Integer workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }
    /**
     * 设置 [单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }
    /**
     * 设置 [包裹]
     */
    public void setPackageId(Integer packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [所有者]
     */
    public void setOwnerId(Integer ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }

}



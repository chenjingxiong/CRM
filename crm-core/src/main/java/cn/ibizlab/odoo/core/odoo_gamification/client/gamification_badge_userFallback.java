package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_badge_user] 服务对象接口
 */
@Component
public class gamification_badge_userFallback implements gamification_badge_userFeignClient{


    public Gamification_badge_user get(Integer id){
            return null;
     }



    public Gamification_badge_user update(Integer id, Gamification_badge_user gamification_badge_user){
            return null;
     }
    public Boolean updateBatch(List<Gamification_badge_user> gamification_badge_users){
            return false;
     }



    public Gamification_badge_user create(Gamification_badge_user gamification_badge_user){
            return null;
     }
    public Boolean createBatch(List<Gamification_badge_user> gamification_badge_users){
            return false;
     }

    public Page<Gamification_badge_user> searchDefault(Gamification_badge_userSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Gamification_badge_user> select(){
            return null;
     }

    public Gamification_badge_user getDraft(){
            return null;
    }



}

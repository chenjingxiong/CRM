package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner] 服务对象接口
 */
@Component
public class res_partnerFallback implements res_partnerFeignClient{

    public Res_partner get(Integer id){
            return null;
     }


    public Res_partner update(Integer id, Res_partner res_partner){
            return null;
     }
    public Boolean updateBatch(List<Res_partner> res_partners){
            return false;
     }


    public Res_partner create(Res_partner res_partner){
            return null;
     }
    public Boolean createBatch(List<Res_partner> res_partners){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Res_partner> searchDefault(Res_partnerSearchContext context){
            return null;
     }


    public Page<Res_partner> select(){
            return null;
     }

    public Boolean checkKey(Res_partner res_partner){
            return false;
     }


    public Boolean save(Res_partner res_partner){
            return false;
     }
    public Boolean saveBatch(List<Res_partner> res_partners){
            return false;
     }

    public Res_partner getDraft(){
            return null;
    }



    public Page<Res_partner> searchContacts(Res_partnerSearchContext context){
            return null;
     }


    public Page<Res_partner> searchCompany(Res_partnerSearchContext context){
            return null;
     }


}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_goal_definition] 对象
 */
public interface Igamification_goal_definition {

    /**
     * 获取 [动作]
     */
    public void setAction_id(Integer action_id);
    
    /**
     * 设置 [动作]
     */
    public Integer getAction_id();

    /**
     * 获取 [动作]脏标记
     */
    public boolean getAction_idDirtyFlag();
    /**
     * 获取 [批量用户的特有字段]
     */
    public void setBatch_distinctive_field(Integer batch_distinctive_field);
    
    /**
     * 设置 [批量用户的特有字段]
     */
    public Integer getBatch_distinctive_field();

    /**
     * 获取 [批量用户的特有字段]脏标记
     */
    public boolean getBatch_distinctive_fieldDirtyFlag();
    /**
     * 获取 [批量模式]
     */
    public void setBatch_mode(String batch_mode);
    
    /**
     * 设置 [批量模式]
     */
    public String getBatch_mode();

    /**
     * 获取 [批量模式]脏标记
     */
    public boolean getBatch_modeDirtyFlag();
    /**
     * 获取 [批处理模式的求值表达式]
     */
    public void setBatch_user_expression(String batch_user_expression);
    
    /**
     * 设置 [批处理模式的求值表达式]
     */
    public String getBatch_user_expression();

    /**
     * 获取 [批处理模式的求值表达式]脏标记
     */
    public boolean getBatch_user_expressionDirtyFlag();
    /**
     * 获取 [计算模式]
     */
    public void setComputation_mode(String computation_mode);
    
    /**
     * 设置 [计算模式]
     */
    public String getComputation_mode();

    /**
     * 获取 [计算模式]脏标记
     */
    public boolean getComputation_modeDirtyFlag();
    /**
     * 获取 [Python 代码]
     */
    public void setCompute_code(String compute_code);
    
    /**
     * 设置 [Python 代码]
     */
    public String getCompute_code();

    /**
     * 获取 [Python 代码]脏标记
     */
    public boolean getCompute_codeDirtyFlag();
    /**
     * 获取 [目标绩效]
     */
    public void setCondition(String condition);
    
    /**
     * 设置 [目标绩效]
     */
    public String getCondition();

    /**
     * 获取 [目标绩效]脏标记
     */
    public boolean getConditionDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [目标说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [目标说明]
     */
    public String getDescription();

    /**
     * 获取 [目标说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示为]
     */
    public void setDisplay_mode(String display_mode);
    
    /**
     * 设置 [显示为]
     */
    public String getDisplay_mode();

    /**
     * 获取 [显示为]脏标记
     */
    public boolean getDisplay_modeDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [筛选域]
     */
    public void setDomain(String domain);
    
    /**
     * 设置 [筛选域]
     */
    public String getDomain();

    /**
     * 获取 [筛选域]脏标记
     */
    public boolean getDomainDirtyFlag();
    /**
     * 获取 [日期字段]
     */
    public void setField_date_id(Integer field_date_id);
    
    /**
     * 设置 [日期字段]
     */
    public Integer getField_date_id();

    /**
     * 获取 [日期字段]脏标记
     */
    public boolean getField_date_idDirtyFlag();
    /**
     * 获取 [字段总计]
     */
    public void setField_id(Integer field_id);
    
    /**
     * 设置 [字段总计]
     */
    public Integer getField_id();

    /**
     * 获取 [字段总计]脏标记
     */
    public boolean getField_idDirtyFlag();
    /**
     * 获取 [完整的后缀]
     */
    public void setFull_suffix(String full_suffix);
    
    /**
     * 设置 [完整的后缀]
     */
    public String getFull_suffix();

    /**
     * 获取 [完整的后缀]脏标记
     */
    public boolean getFull_suffixDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel_id(Integer model_id);
    
    /**
     * 设置 [模型]
     */
    public Integer getModel_id();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModel_idDirtyFlag();
    /**
     * 获取 [金钱值]
     */
    public void setMonetary(String monetary);
    
    /**
     * 设置 [金钱值]
     */
    public String getMonetary();

    /**
     * 获取 [金钱值]脏标记
     */
    public boolean getMonetaryDirtyFlag();
    /**
     * 获取 [目标定义]
     */
    public void setName(String name);
    
    /**
     * 设置 [目标定义]
     */
    public String getName();

    /**
     * 获取 [目标定义]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [用户ID字段]
     */
    public void setRes_id_field(String res_id_field);
    
    /**
     * 设置 [用户ID字段]
     */
    public String getRes_id_field();

    /**
     * 获取 [用户ID字段]脏标记
     */
    public boolean getRes_id_fieldDirtyFlag();
    /**
     * 获取 [后缀]
     */
    public void setSuffix(String suffix);
    
    /**
     * 设置 [后缀]
     */
    public String getSuffix();

    /**
     * 获取 [后缀]脏标记
     */
    public boolean getSuffixDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_note.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_note.service.INote_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_note.client.note_tagFeignClient;

/**
 * 实体[便签标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Note_tagServiceImpl implements INote_tagService {

    @Autowired
    note_tagFeignClient note_tagFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=note_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        note_tagFeignClient.removeBatch(idList);
    }

    @Override
    public Note_tag getDraft(Note_tag et) {
        et=note_tagFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Note_tag et) {
        Note_tag rt = note_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Note_tag> list){
        note_tagFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Note_tag et) {
        Note_tag rt = note_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Note_tag> list){
        note_tagFeignClient.createBatch(list) ;
    }

    @Override
    public Note_tag get(Integer id) {
		Note_tag et=note_tagFeignClient.get(id);
        if(et==null){
            et=new Note_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Note_tag> searchDefault(Note_tagSearchContext context) {
        Page<Note_tag> note_tags=note_tagFeignClient.searchDefault(context);
        return note_tags;
    }


}



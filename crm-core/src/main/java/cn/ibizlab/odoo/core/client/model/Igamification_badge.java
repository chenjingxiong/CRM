package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_badge] 对象
 */
public interface Igamification_badge {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [挑战的奖励]
     */
    public void setChallenge_ids(String challenge_ids);
    
    /**
     * 设置 [挑战的奖励]
     */
    public String getChallenge_ids();

    /**
     * 获取 [挑战的奖励]脏标记
     */
    public boolean getChallenge_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [奖励按照]
     */
    public void setGoal_definition_ids(String goal_definition_ids);
    
    /**
     * 设置 [奖励按照]
     */
    public String getGoal_definition_ids();

    /**
     * 获取 [奖励按照]脏标记
     */
    public boolean getGoal_definition_idsDirtyFlag();
    /**
     * 获取 [授予的员工人数]
     */
    public void setGranted_employees_count(Integer granted_employees_count);
    
    /**
     * 设置 [授予的员工人数]
     */
    public Integer getGranted_employees_count();

    /**
     * 获取 [授予的员工人数]脏标记
     */
    public boolean getGranted_employees_countDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [论坛徽章等级]
     */
    public void setLevel(String level);
    
    /**
     * 设置 [论坛徽章等级]
     */
    public String getLevel();

    /**
     * 获取 [论坛徽章等级]脏标记
     */
    public boolean getLevelDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [徽章]
     */
    public void setName(String name);
    
    /**
     * 设置 [徽章]
     */
    public String getName();

    /**
     * 获取 [徽章]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_ids(String owner_ids);
    
    /**
     * 设置 [所有者]
     */
    public String getOwner_ids();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_idsDirtyFlag();
    /**
     * 获取 [其他的允许发送]
     */
    public void setRemaining_sending(Integer remaining_sending);
    
    /**
     * 设置 [其他的允许发送]
     */
    public Integer getRemaining_sending();

    /**
     * 获取 [其他的允许发送]脏标记
     */
    public boolean getRemaining_sendingDirtyFlag();
    /**
     * 获取 [允许授予]
     */
    public void setRule_auth(String rule_auth);
    
    /**
     * 设置 [允许授予]
     */
    public String getRule_auth();

    /**
     * 获取 [允许授予]脏标记
     */
    public boolean getRule_authDirtyFlag();
    /**
     * 获取 [需要徽章]
     */
    public void setRule_auth_badge_ids(String rule_auth_badge_ids);
    
    /**
     * 设置 [需要徽章]
     */
    public String getRule_auth_badge_ids();

    /**
     * 获取 [需要徽章]脏标记
     */
    public boolean getRule_auth_badge_idsDirtyFlag();
    /**
     * 获取 [授权用户]
     */
    public void setRule_auth_user_ids(String rule_auth_user_ids);
    
    /**
     * 设置 [授权用户]
     */
    public String getRule_auth_user_ids();

    /**
     * 获取 [授权用户]脏标记
     */
    public boolean getRule_auth_user_idsDirtyFlag();
    /**
     * 获取 [月度限额发放]
     */
    public void setRule_max(String rule_max);
    
    /**
     * 设置 [月度限额发放]
     */
    public String getRule_max();

    /**
     * 获取 [月度限额发放]脏标记
     */
    public boolean getRule_maxDirtyFlag();
    /**
     * 获取 [限制数量]
     */
    public void setRule_max_number(Integer rule_max_number);
    
    /**
     * 设置 [限制数量]
     */
    public Integer getRule_max_number();

    /**
     * 获取 [限制数量]脏标记
     */
    public boolean getRule_max_numberDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setStat_count(Integer stat_count);
    
    /**
     * 设置 [总计]
     */
    public Integer getStat_count();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getStat_countDirtyFlag();
    /**
     * 获取 [用户数量]
     */
    public void setStat_count_distinct(Integer stat_count_distinct);
    
    /**
     * 设置 [用户数量]
     */
    public Integer getStat_count_distinct();

    /**
     * 获取 [用户数量]脏标记
     */
    public boolean getStat_count_distinctDirtyFlag();
    /**
     * 获取 [我的总计]
     */
    public void setStat_my(Integer stat_my);
    
    /**
     * 设置 [我的总计]
     */
    public Integer getStat_my();

    /**
     * 获取 [我的总计]脏标记
     */
    public boolean getStat_myDirtyFlag();
    /**
     * 获取 [月度发放总数]
     */
    public void setStat_my_monthly_sending(Integer stat_my_monthly_sending);
    
    /**
     * 设置 [月度发放总数]
     */
    public Integer getStat_my_monthly_sending();

    /**
     * 获取 [月度发放总数]脏标记
     */
    public boolean getStat_my_monthly_sendingDirtyFlag();
    /**
     * 获取 [我的月份总计]
     */
    public void setStat_my_this_month(Integer stat_my_this_month);
    
    /**
     * 设置 [我的月份总计]
     */
    public Integer getStat_my_this_month();

    /**
     * 获取 [我的月份总计]脏标记
     */
    public boolean getStat_my_this_monthDirtyFlag();
    /**
     * 获取 [每月总数]
     */
    public void setStat_this_month(Integer stat_this_month);
    
    /**
     * 设置 [每月总数]
     */
    public Integer getStat_this_month();

    /**
     * 获取 [每月总数]脏标记
     */
    public boolean getStat_this_monthDirtyFlag();
    /**
     * 获取 [唯一的所有者]
     */
    public void setUnique_owner_ids(String unique_owner_ids);
    
    /**
     * 设置 [唯一的所有者]
     */
    public String getUnique_owner_ids();

    /**
     * 获取 [唯一的所有者]脏标记
     */
    public boolean getUnique_owner_idsDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_replenish;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_replenish] 服务对象接口
 */
public interface Iproduct_replenishClientService{

    public Iproduct_replenish createModel() ;

    public void removeBatch(List<Iproduct_replenish> product_replenishes);

    public void createBatch(List<Iproduct_replenish> product_replenishes);

    public void updateBatch(List<Iproduct_replenish> product_replenishes);

    public Page<Iproduct_replenish> fetchDefault(SearchContext context);

    public void remove(Iproduct_replenish product_replenish);

    public void update(Iproduct_replenish product_replenish);

    public void get(Iproduct_replenish product_replenish);

    public void create(Iproduct_replenish product_replenish);

    public Page<Iproduct_replenish> select(SearchContext context);

    public void getDraft(Iproduct_replenish product_replenish);

}

package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_installService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_language_installFeignClient;

/**
 * 实体[安装语言] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_language_installServiceImpl implements IBase_language_installService {

    @Autowired
    base_language_installFeignClient base_language_installFeignClient;


    @Override
    public boolean create(Base_language_install et) {
        Base_language_install rt = base_language_installFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_language_install> list){
        base_language_installFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Base_language_install et) {
        Base_language_install rt = base_language_installFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_language_install> list){
        base_language_installFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_language_install getDraft(Base_language_install et) {
        et=base_language_installFeignClient.getDraft();
        return et;
    }

    @Override
    public Base_language_install get(Integer id) {
		Base_language_install et=base_language_installFeignClient.get(id);
        if(et==null){
            et=new Base_language_install();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_language_installFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_language_installFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_language_install> searchDefault(Base_language_installSearchContext context) {
        Page<Base_language_install> base_language_installs=base_language_installFeignClient.searchDefault(context);
        return base_language_installs;
    }


}



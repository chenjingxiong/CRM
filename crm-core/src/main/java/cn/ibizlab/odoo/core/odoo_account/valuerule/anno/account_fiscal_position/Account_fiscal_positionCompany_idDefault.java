package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_fiscal_position;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_fiscal_position.Account_fiscal_positionCompany_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_fiscal_position
 * 属性：Company_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_fiscal_positionCompany_idDefaultValidator.class})
public @interface Account_fiscal_positionCompany_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Res_partner] 查询条件对象
 */
@Slf4j
@Data
public class Res_partnerSearchContext extends SearchContextBase {
	private String n_type_eq;//[地址类型]

	private String n_trust_eq;//[对此债务人的信任度]

	private String n_invoice_warn_eq;//[发票]

	private String n_is_company_eq;//[公司]

	private String n_tz_eq;//[时区]

	private String n_sale_warn_eq;//[销售警告]

	private String n_picking_warn_eq;//[库存拣货]

	private String n_purchase_warn_eq;//[采购订单]

	private String n_activity_state_eq;//[活动状态]

	private String n_name_like;//[名称]

	private String n_company_type_eq;//[公司类别]

	private String n_lang_eq;//[语言]

	private String n_write_uid_text_eq;//[最后更新者]

	private String n_write_uid_text_like;//[最后更新者]

	private String n_title_text_eq;//[称谓]

	private String n_title_text_like;//[称谓]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_country_id_text_eq;//[国家/地区]

	private String n_country_id_text_like;//[国家/地区]

	private String n_state_id_text_eq;//[省/ 州]

	private String n_state_id_text_like;//[省/ 州]

	private String n_commercial_partner_id_text_eq;//[商业实体]

	private String n_commercial_partner_id_text_like;//[商业实体]

	private String n_parent_name_eq;//[上级名称]

	private String n_parent_name_like;//[上级名称]

	private String n_user_id_text_eq;//[销售员]

	private String n_user_id_text_like;//[销售员]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_industry_id_text_eq;//[工业]

	private String n_industry_id_text_like;//[工业]

	private String n_team_id_text_eq;//[销售团队]

	private String n_team_id_text_like;//[销售团队]

	private Integer n_team_id_eq;//[销售团队]

	private Integer n_state_id_eq;//[省/ 州]

	private Integer n_user_id_eq;//[销售员]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_parent_id_eq;//[关联公司]

	private Integer n_title_eq;//[称谓]

	private Integer n_write_uid_eq;//[最后更新者]

	private Integer n_commercial_partner_id_eq;//[商业实体]

	private Integer n_industry_id_eq;//[工业]

	private Integer n_company_id_eq;//[公司]

	private Integer n_country_id_eq;//[国家/地区]

}




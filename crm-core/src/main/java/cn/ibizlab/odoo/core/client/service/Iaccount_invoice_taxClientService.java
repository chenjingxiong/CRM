package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_tax;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_tax] 服务对象接口
 */
public interface Iaccount_invoice_taxClientService{

    public Iaccount_invoice_tax createModel() ;

    public void updateBatch(List<Iaccount_invoice_tax> account_invoice_taxes);

    public void removeBatch(List<Iaccount_invoice_tax> account_invoice_taxes);

    public void createBatch(List<Iaccount_invoice_tax> account_invoice_taxes);

    public void update(Iaccount_invoice_tax account_invoice_tax);

    public void get(Iaccount_invoice_tax account_invoice_tax);

    public void remove(Iaccount_invoice_tax account_invoice_tax);

    public void create(Iaccount_invoice_tax account_invoice_tax);

    public Page<Iaccount_invoice_tax> fetchDefault(SearchContext context);

    public Page<Iaccount_invoice_tax> select(SearchContext context);

    public void getDraft(Iaccount_invoice_tax account_invoice_tax);

}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_employee] 对象
 */
public interface Ihr_employee {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [附加说明]
     */
    public void setAdditional_note(String additional_note);
    
    /**
     * 设置 [附加说明]
     */
    public String getAdditional_note();

    /**
     * 获取 [附加说明]脏标记
     */
    public boolean getAdditional_noteDirtyFlag();
    /**
     * 获取 [家庭住址]
     */
    public void setAddress_home_id(Integer address_home_id);
    
    /**
     * 设置 [家庭住址]
     */
    public Integer getAddress_home_id();

    /**
     * 获取 [家庭住址]脏标记
     */
    public boolean getAddress_home_idDirtyFlag();
    /**
     * 获取 [家庭住址]
     */
    public void setAddress_home_id_text(String address_home_id_text);
    
    /**
     * 设置 [家庭住址]
     */
    public String getAddress_home_id_text();

    /**
     * 获取 [家庭住址]脏标记
     */
    public boolean getAddress_home_id_textDirtyFlag();
    /**
     * 获取 [工作地址]
     */
    public void setAddress_id(Integer address_id);
    
    /**
     * 设置 [工作地址]
     */
    public Integer getAddress_id();

    /**
     * 获取 [工作地址]脏标记
     */
    public boolean getAddress_idDirtyFlag();
    /**
     * 获取 [工作地址]
     */
    public void setAddress_id_text(String address_id_text);
    
    /**
     * 设置 [工作地址]
     */
    public String getAddress_id_text();

    /**
     * 获取 [工作地址]脏标记
     */
    public boolean getAddress_id_textDirtyFlag();
    /**
     * 获取 [出勤]
     */
    public void setAttendance_ids(String attendance_ids);
    
    /**
     * 设置 [出勤]
     */
    public String getAttendance_ids();

    /**
     * 获取 [出勤]脏标记
     */
    public boolean getAttendance_idsDirtyFlag();
    /**
     * 获取 [出勤状态]
     */
    public void setAttendance_state(String attendance_state);
    
    /**
     * 设置 [出勤状态]
     */
    public String getAttendance_state();

    /**
     * 获取 [出勤状态]脏标记
     */
    public boolean getAttendance_stateDirtyFlag();
    /**
     * 获取 [员工徽章]
     */
    public void setBadge_ids(String badge_ids);
    
    /**
     * 设置 [员工徽章]
     */
    public String getBadge_ids();

    /**
     * 获取 [员工徽章]脏标记
     */
    public boolean getBadge_idsDirtyFlag();
    /**
     * 获取 [银行账户号码]
     */
    public void setBank_account_id(Integer bank_account_id);
    
    /**
     * 设置 [银行账户号码]
     */
    public Integer getBank_account_id();

    /**
     * 获取 [银行账户号码]脏标记
     */
    public boolean getBank_account_idDirtyFlag();
    /**
     * 获取 [工牌 ID]
     */
    public void setBarcode(String barcode);
    
    /**
     * 设置 [工牌 ID]
     */
    public String getBarcode();

    /**
     * 获取 [工牌 ID]脏标记
     */
    public boolean getBarcodeDirtyFlag();
    /**
     * 获取 [出生日期]
     */
    public void setBirthday(Timestamp birthday);
    
    /**
     * 设置 [出生日期]
     */
    public Timestamp getBirthday();

    /**
     * 获取 [出生日期]脏标记
     */
    public boolean getBirthdayDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setCategory_ids(String category_ids);
    
    /**
     * 设置 [标签]
     */
    public String getCategory_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getCategory_idsDirtyFlag();
    /**
     * 获取 [证书等级]
     */
    public void setCertificate(String certificate);
    
    /**
     * 设置 [证书等级]
     */
    public String getCertificate();

    /**
     * 获取 [证书等级]脏标记
     */
    public boolean getCertificateDirtyFlag();
    /**
     * 获取 [子女数目]
     */
    public void setChildren(Integer children);
    
    /**
     * 设置 [子女数目]
     */
    public Integer getChildren();

    /**
     * 获取 [子女数目]脏标记
     */
    public boolean getChildrenDirtyFlag();
    /**
     * 获取 [下属]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [下属]
     */
    public String getChild_ids();

    /**
     * 获取 [下属]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [教练]
     */
    public void setCoach_id(Integer coach_id);
    
    /**
     * 设置 [教练]
     */
    public Integer getCoach_id();

    /**
     * 获取 [教练]脏标记
     */
    public boolean getCoach_idDirtyFlag();
    /**
     * 获取 [教练]
     */
    public void setCoach_id_text(String coach_id_text);
    
    /**
     * 设置 [教练]
     */
    public String getCoach_id_text();

    /**
     * 获取 [教练]脏标记
     */
    public boolean getCoach_id_textDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [合同统计]
     */
    public void setContracts_count(Integer contracts_count);
    
    /**
     * 设置 [合同统计]
     */
    public Integer getContracts_count();

    /**
     * 获取 [合同统计]脏标记
     */
    public boolean getContracts_countDirtyFlag();
    /**
     * 获取 [当前合同]
     */
    public void setContract_id(Integer contract_id);
    
    /**
     * 设置 [当前合同]
     */
    public Integer getContract_id();

    /**
     * 获取 [当前合同]脏标记
     */
    public boolean getContract_idDirtyFlag();
    /**
     * 获取 [员工合同]
     */
    public void setContract_ids(String contract_ids);
    
    /**
     * 设置 [员工合同]
     */
    public String getContract_ids();

    /**
     * 获取 [员工合同]脏标记
     */
    public boolean getContract_idsDirtyFlag();
    /**
     * 获取 [国籍(国家)]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国籍(国家)]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国籍(国家)]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国籍(国家)]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国籍(国家)]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国籍(国家)]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [国籍]
     */
    public void setCountry_of_birth(Integer country_of_birth);
    
    /**
     * 设置 [国籍]
     */
    public Integer getCountry_of_birth();

    /**
     * 获取 [国籍]脏标记
     */
    public boolean getCountry_of_birthDirtyFlag();
    /**
     * 获取 [国籍]
     */
    public void setCountry_of_birth_text(String country_of_birth_text);
    
    /**
     * 设置 [国籍]
     */
    public String getCountry_of_birth_text();

    /**
     * 获取 [国籍]脏标记
     */
    public boolean getCountry_of_birth_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [当前休假类型]
     */
    public void setCurrent_leave_id(Integer current_leave_id);
    
    /**
     * 设置 [当前休假类型]
     */
    public Integer getCurrent_leave_id();

    /**
     * 获取 [当前休假类型]脏标记
     */
    public boolean getCurrent_leave_idDirtyFlag();
    /**
     * 获取 [当前休假状态]
     */
    public void setCurrent_leave_state(String current_leave_state);
    
    /**
     * 设置 [当前休假状态]
     */
    public String getCurrent_leave_state();

    /**
     * 获取 [当前休假状态]脏标记
     */
    public boolean getCurrent_leave_stateDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [直接徽章]
     */
    public void setDirect_badge_ids(String direct_badge_ids);
    
    /**
     * 设置 [直接徽章]
     */
    public String getDirect_badge_ids();

    /**
     * 获取 [直接徽章]脏标记
     */
    public boolean getDirect_badge_idsDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [紧急联系人]
     */
    public void setEmergency_contact(String emergency_contact);
    
    /**
     * 设置 [紧急联系人]
     */
    public String getEmergency_contact();

    /**
     * 获取 [紧急联系人]脏标记
     */
    public boolean getEmergency_contactDirtyFlag();
    /**
     * 获取 [紧急电话]
     */
    public void setEmergency_phone(String emergency_phone);
    
    /**
     * 设置 [紧急电话]
     */
    public String getEmergency_phone();

    /**
     * 获取 [紧急电话]脏标记
     */
    public boolean getEmergency_phoneDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setExpense_manager_id(Integer expense_manager_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getExpense_manager_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getExpense_manager_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setExpense_manager_id_text(String expense_manager_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getExpense_manager_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getExpense_manager_id_textDirtyFlag();
    /**
     * 获取 [性别]
     */
    public void setGender(String gender);
    
    /**
     * 设置 [性别]
     */
    public String getGender();

    /**
     * 获取 [性别]脏标记
     */
    public boolean getGenderDirtyFlag();
    /**
     * 获取 [员工人力资源目标]
     */
    public void setGoal_ids(String goal_ids);
    
    /**
     * 设置 [员工人力资源目标]
     */
    public String getGoal_ids();

    /**
     * 获取 [员工人力资源目标]脏标记
     */
    public boolean getGoal_idsDirtyFlag();
    /**
     * 获取 [员工文档]
     */
    public void setGoogle_drive_link(String google_drive_link);
    
    /**
     * 设置 [员工文档]
     */
    public String getGoogle_drive_link();

    /**
     * 获取 [员工文档]脏标记
     */
    public boolean getGoogle_drive_linkDirtyFlag();
    /**
     * 获取 [拥有徽章]
     */
    public void setHas_badges(String has_badges);
    
    /**
     * 设置 [拥有徽章]
     */
    public String getHas_badges();

    /**
     * 获取 [拥有徽章]脏标记
     */
    public boolean getHas_badgesDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [身份证号]
     */
    public void setIdentification_id(String identification_id);
    
    /**
     * 设置 [身份证号]
     */
    public String getIdentification_id();

    /**
     * 获取 [身份证号]脏标记
     */
    public boolean getIdentification_idDirtyFlag();
    /**
     * 获取 [照片]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [照片]
     */
    public byte[] getImage();

    /**
     * 获取 [照片]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸照片]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸照片]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸照片]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸照片]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸照片]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸照片]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [今日缺勤]
     */
    public void setIs_absent_totay(String is_absent_totay);
    
    /**
     * 设置 [今日缺勤]
     */
    public String getIs_absent_totay();

    /**
     * 获取 [今日缺勤]脏标记
     */
    public boolean getIs_absent_totayDirtyFlag();
    /**
     * 获取 [已与公司地址相关联的员工住址]
     */
    public void setIs_address_home_a_company(String is_address_home_a_company);
    
    /**
     * 设置 [已与公司地址相关联的员工住址]
     */
    public String getIs_address_home_a_company();

    /**
     * 获取 [已与公司地址相关联的员工住址]脏标记
     */
    public boolean getIs_address_home_a_companyDirtyFlag();
    /**
     * 获取 [工作岗位]
     */
    public void setJob_id(Integer job_id);
    
    /**
     * 设置 [工作岗位]
     */
    public Integer getJob_id();

    /**
     * 获取 [工作岗位]脏标记
     */
    public boolean getJob_idDirtyFlag();
    /**
     * 获取 [工作岗位]
     */
    public void setJob_id_text(String job_id_text);
    
    /**
     * 设置 [工作岗位]
     */
    public String getJob_id_text();

    /**
     * 获取 [工作岗位]脏标记
     */
    public boolean getJob_id_textDirtyFlag();
    /**
     * 获取 [工作头衔]
     */
    public void setJob_title(String job_title);
    
    /**
     * 设置 [工作头衔]
     */
    public String getJob_title();

    /**
     * 获取 [工作头衔]脏标记
     */
    public boolean getJob_titleDirtyFlag();
    /**
     * 获取 [上班距离]
     */
    public void setKm_home_work(Integer km_home_work);
    
    /**
     * 设置 [上班距离]
     */
    public Integer getKm_home_work();

    /**
     * 获取 [上班距离]脏标记
     */
    public boolean getKm_home_workDirtyFlag();
    /**
     * 获取 [上次出勤]
     */
    public void setLast_attendance_id(Integer last_attendance_id);
    
    /**
     * 设置 [上次出勤]
     */
    public Integer getLast_attendance_id();

    /**
     * 获取 [上次出勤]脏标记
     */
    public boolean getLast_attendance_idDirtyFlag();
    /**
     * 获取 [休假天数]
     */
    public void setLeaves_count(Double leaves_count);
    
    /**
     * 设置 [休假天数]
     */
    public Double getLeaves_count();

    /**
     * 获取 [休假天数]脏标记
     */
    public boolean getLeaves_countDirtyFlag();
    /**
     * 获取 [起始日期]
     */
    public void setLeave_date_from(Timestamp leave_date_from);
    
    /**
     * 设置 [起始日期]
     */
    public Timestamp getLeave_date_from();

    /**
     * 获取 [起始日期]脏标记
     */
    public boolean getLeave_date_fromDirtyFlag();
    /**
     * 获取 [至日期]
     */
    public void setLeave_date_to(Timestamp leave_date_to);
    
    /**
     * 设置 [至日期]
     */
    public Timestamp getLeave_date_to();

    /**
     * 获取 [至日期]脏标记
     */
    public boolean getLeave_date_toDirtyFlag();
    /**
     * 获取 [是管理者]
     */
    public void setManager(String manager);
    
    /**
     * 设置 [是管理者]
     */
    public String getManager();

    /**
     * 获取 [是管理者]脏标记
     */
    public boolean getManagerDirtyFlag();
    /**
     * 获取 [手动出勤]
     */
    public void setManual_attendance(String manual_attendance);
    
    /**
     * 设置 [手动出勤]
     */
    public String getManual_attendance();

    /**
     * 获取 [手动出勤]脏标记
     */
    public boolean getManual_attendanceDirtyFlag();
    /**
     * 获取 [婚姻状况]
     */
    public void setMarital(String marital);
    
    /**
     * 设置 [婚姻状况]
     */
    public String getMarital();

    /**
     * 获取 [婚姻状况]脏标记
     */
    public boolean getMaritalDirtyFlag();
    /**
     * 获取 [体检日期]
     */
    public void setMedic_exam(Timestamp medic_exam);
    
    /**
     * 设置 [体检日期]
     */
    public Timestamp getMedic_exam();

    /**
     * 获取 [体检日期]脏标记
     */
    public boolean getMedic_examDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [办公手机]
     */
    public void setMobile_phone(String mobile_phone);
    
    /**
     * 设置 [办公手机]
     */
    public String getMobile_phone();

    /**
     * 获取 [办公手机]脏标记
     */
    public boolean getMobile_phoneDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [新近雇用的员工]
     */
    public void setNewly_hired_employee(String newly_hired_employee);
    
    /**
     * 设置 [新近雇用的员工]
     */
    public String getNewly_hired_employee();

    /**
     * 获取 [新近雇用的员工]脏标记
     */
    public boolean getNewly_hired_employeeDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNotes(String notes);
    
    /**
     * 设置 [备注]
     */
    public String getNotes();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNotesDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [经理]
     */
    public Integer getParent_id();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [经理]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [经理]
     */
    public String getParent_id_text();

    /**
     * 获取 [经理]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [护照号]
     */
    public void setPassport_id(String passport_id);
    
    /**
     * 设置 [护照号]
     */
    public String getPassport_id();

    /**
     * 获取 [护照号]脏标记
     */
    public boolean getPassport_idDirtyFlag();
    /**
     * 获取 [工作许可编号]
     */
    public void setPermit_no(String permit_no);
    
    /**
     * 设置 [工作许可编号]
     */
    public String getPermit_no();

    /**
     * 获取 [工作许可编号]脏标记
     */
    public boolean getPermit_noDirtyFlag();
    /**
     * 获取 [PIN]
     */
    public void setPin(String pin);
    
    /**
     * 设置 [PIN]
     */
    public String getPin();

    /**
     * 获取 [PIN]脏标记
     */
    public boolean getPinDirtyFlag();
    /**
     * 获取 [出生地]
     */
    public void setPlace_of_birth(String place_of_birth);
    
    /**
     * 设置 [出生地]
     */
    public String getPlace_of_birth();

    /**
     * 获取 [出生地]脏标记
     */
    public boolean getPlace_of_birthDirtyFlag();
    /**
     * 获取 [剩余的法定休假]
     */
    public void setRemaining_leaves(Double remaining_leaves);
    
    /**
     * 设置 [剩余的法定休假]
     */
    public Double getRemaining_leaves();

    /**
     * 获取 [剩余的法定休假]脏标记
     */
    public boolean getRemaining_leavesDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [工作时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id_text(String resource_calendar_id_text);
    
    /**
     * 设置 [工作时间]
     */
    public String getResource_calendar_id_text();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_id_textDirtyFlag();
    /**
     * 获取 [资源]
     */
    public void setResource_id(Integer resource_id);
    
    /**
     * 设置 [资源]
     */
    public Integer getResource_id();

    /**
     * 获取 [资源]脏标记
     */
    public boolean getResource_idDirtyFlag();
    /**
     * 获取 [能查看剩余的休假]
     */
    public void setShow_leaves(String show_leaves);
    
    /**
     * 设置 [能查看剩余的休假]
     */
    public String getShow_leaves();

    /**
     * 获取 [能查看剩余的休假]脏标记
     */
    public boolean getShow_leavesDirtyFlag();
    /**
     * 获取 [社会保险号SIN]
     */
    public void setSinid(String sinid);
    
    /**
     * 设置 [社会保险号SIN]
     */
    public String getSinid();

    /**
     * 获取 [社会保险号SIN]脏标记
     */
    public boolean getSinidDirtyFlag();
    /**
     * 获取 [配偶生日]
     */
    public void setSpouse_birthdate(Timestamp spouse_birthdate);
    
    /**
     * 设置 [配偶生日]
     */
    public Timestamp getSpouse_birthdate();

    /**
     * 获取 [配偶生日]脏标记
     */
    public boolean getSpouse_birthdateDirtyFlag();
    /**
     * 获取 [配偶全名]
     */
    public void setSpouse_complete_name(String spouse_complete_name);
    
    /**
     * 设置 [配偶全名]
     */
    public String getSpouse_complete_name();

    /**
     * 获取 [配偶全名]脏标记
     */
    public boolean getSpouse_complete_nameDirtyFlag();
    /**
     * 获取 [社会保障号SSN]
     */
    public void setSsnid(String ssnid);
    
    /**
     * 设置 [社会保障号SSN]
     */
    public String getSsnid();

    /**
     * 获取 [社会保障号SSN]脏标记
     */
    public boolean getSsnidDirtyFlag();
    /**
     * 获取 [研究领域]
     */
    public void setStudy_field(String study_field);
    
    /**
     * 设置 [研究领域]
     */
    public String getStudy_field();

    /**
     * 获取 [研究领域]脏标记
     */
    public boolean getStudy_fieldDirtyFlag();
    /**
     * 获取 [毕业院校]
     */
    public void setStudy_school(String study_school);
    
    /**
     * 设置 [毕业院校]
     */
    public String getStudy_school();

    /**
     * 获取 [毕业院校]脏标记
     */
    public boolean getStudy_schoolDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setTz(String tz);
    
    /**
     * 设置 [时区]
     */
    public String getTz();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getTzDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [公司汽车]
     */
    public void setVehicle(String vehicle);
    
    /**
     * 设置 [公司汽车]
     */
    public String getVehicle();

    /**
     * 获取 [公司汽车]脏标记
     */
    public boolean getVehicleDirtyFlag();
    /**
     * 获取 [签证到期日期]
     */
    public void setVisa_expire(Timestamp visa_expire);
    
    /**
     * 设置 [签证到期日期]
     */
    public Timestamp getVisa_expire();

    /**
     * 获取 [签证到期日期]脏标记
     */
    public boolean getVisa_expireDirtyFlag();
    /**
     * 获取 [签证号]
     */
    public void setVisa_no(String visa_no);
    
    /**
     * 设置 [签证号]
     */
    public String getVisa_no();

    /**
     * 获取 [签证号]脏标记
     */
    public boolean getVisa_noDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [工作EMail]
     */
    public void setWork_email(String work_email);
    
    /**
     * 设置 [工作EMail]
     */
    public String getWork_email();

    /**
     * 获取 [工作EMail]脏标记
     */
    public boolean getWork_emailDirtyFlag();
    /**
     * 获取 [工作地点]
     */
    public void setWork_location(String work_location);
    
    /**
     * 设置 [工作地点]
     */
    public String getWork_location();

    /**
     * 获取 [工作地点]脏标记
     */
    public boolean getWork_locationDirtyFlag();
    /**
     * 获取 [办公电话]
     */
    public void setWork_phone(String work_phone);
    
    /**
     * 设置 [办公电话]
     */
    public String getWork_phone();

    /**
     * 获取 [办公电话]脏标记
     */
    public boolean getWork_phoneDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

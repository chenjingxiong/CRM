package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_document] 对象
 */
public interface Imrp_document {

    /**
     * 获取 [访问令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [访问令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [访问令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [校验和/SHA1]
     */
    public void setChecksum(String checksum);
    
    /**
     * 设置 [校验和/SHA1]
     */
    public String getChecksum();

    /**
     * 获取 [校验和/SHA1]脏标记
     */
    public boolean getChecksumDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [文件内容]
     */
    public void setDatas(byte[] datas);
    
    /**
     * 设置 [文件内容]
     */
    public byte[] getDatas();

    /**
     * 获取 [文件内容]脏标记
     */
    public boolean getDatasDirtyFlag();
    /**
     * 获取 [文件名]
     */
    public void setDatas_fname(String datas_fname);
    
    /**
     * 设置 [文件名]
     */
    public String getDatas_fname();

    /**
     * 获取 [文件名]脏标记
     */
    public boolean getDatas_fnameDirtyFlag();
    /**
     * 获取 [数据库数据]
     */
    public void setDb_datas(byte[] db_datas);
    
    /**
     * 设置 [数据库数据]
     */
    public byte[] getDb_datas();

    /**
     * 获取 [数据库数据]脏标记
     */
    public boolean getDb_datasDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [文件大小]
     */
    public void setFile_size(Integer file_size);
    
    /**
     * 设置 [文件大小]
     */
    public Integer getFile_size();

    /**
     * 获取 [文件大小]脏标记
     */
    public boolean getFile_sizeDirtyFlag();
    /**
     * 获取 [是公开文档]
     */
    public void setIbizpublic(String ibizpublic);
    
    /**
     * 设置 [是公开文档]
     */
    public String getIbizpublic();

    /**
     * 获取 [是公开文档]脏标记
     */
    public boolean getIbizpublicDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [索引的内容]
     */
    public void setIndex_content(String index_content);
    
    /**
     * 设置 [索引的内容]
     */
    public String getIndex_content();

    /**
     * 获取 [索引的内容]脏标记
     */
    public boolean getIndex_contentDirtyFlag();
    /**
     * 获取 [相关的附件]
     */
    public void setIr_attachment_id(Integer ir_attachment_id);
    
    /**
     * 设置 [相关的附件]
     */
    public Integer getIr_attachment_id();

    /**
     * 获取 [相关的附件]脏标记
     */
    public boolean getIr_attachment_idDirtyFlag();
    /**
     * 获取 [键]
     */
    public void setKey(String key);
    
    /**
     * 设置 [键]
     */
    public String getKey();

    /**
     * 获取 [键]脏标记
     */
    public boolean getKeyDirtyFlag();
    /**
     * 获取 [附件网址]
     */
    public void setLocal_url(String local_url);
    
    /**
     * 设置 [附件网址]
     */
    public String getLocal_url();

    /**
     * 获取 [附件网址]脏标记
     */
    public boolean getLocal_urlDirtyFlag();
    /**
     * 获取 [MIME 类型]
     */
    public void setMimetype(String mimetype);
    
    /**
     * 设置 [MIME 类型]
     */
    public String getMimetype();

    /**
     * 获取 [MIME 类型]脏标记
     */
    public boolean getMimetypeDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [资源字段]
     */
    public void setRes_field(String res_field);
    
    /**
     * 设置 [资源字段]
     */
    public String getRes_field();

    /**
     * 获取 [资源字段]脏标记
     */
    public boolean getRes_fieldDirtyFlag();
    /**
     * 获取 [资源ID]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [资源ID]
     */
    public Integer getRes_id();

    /**
     * 获取 [资源ID]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [资源模型]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [资源模型]
     */
    public String getRes_model();

    /**
     * 获取 [资源模型]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [RES型号名称]
     */
    public void setRes_model_name(String res_model_name);
    
    /**
     * 设置 [RES型号名称]
     */
    public String getRes_model_name();

    /**
     * 获取 [RES型号名称]脏标记
     */
    public boolean getRes_model_nameDirtyFlag();
    /**
     * 获取 [资源名称]
     */
    public void setRes_name(String res_name);
    
    /**
     * 设置 [资源名称]
     */
    public String getRes_name();

    /**
     * 获取 [资源名称]脏标记
     */
    public boolean getRes_nameDirtyFlag();
    /**
     * 获取 [存储的文件名]
     */
    public void setStore_fname(String store_fname);
    
    /**
     * 设置 [存储的文件名]
     */
    public String getStore_fname();

    /**
     * 获取 [存储的文件名]脏标记
     */
    public boolean getStore_fnameDirtyFlag();
    /**
     * 获取 [主题模板]
     */
    public void setTheme_template_id(Integer theme_template_id);
    
    /**
     * 设置 [主题模板]
     */
    public Integer getTheme_template_id();

    /**
     * 获取 [主题模板]脏标记
     */
    public boolean getTheme_template_idDirtyFlag();
    /**
     * 获取 [略所图]
     */
    public void setThumbnail(byte[] thumbnail);
    
    /**
     * 设置 [略所图]
     */
    public byte[] getThumbnail();

    /**
     * 获取 [略所图]脏标记
     */
    public boolean getThumbnailDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [Url网址]
     */
    public void setUrl(String url);
    
    /**
     * 设置 [Url网址]
     */
    public String getUrl();

    /**
     * 获取 [Url网址]脏标记
     */
    public boolean getUrlDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

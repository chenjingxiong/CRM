package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_assignation_logService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_assignation_logFeignClient;

/**
 * 实体[交通工具驾驶历史] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_assignation_logServiceImpl implements IFleet_vehicle_assignation_logService {

    @Autowired
    fleet_vehicle_assignation_logFeignClient fleet_vehicle_assignation_logFeignClient;


    @Override
    public Fleet_vehicle_assignation_log get(Integer id) {
		Fleet_vehicle_assignation_log et=fleet_vehicle_assignation_logFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_assignation_log();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Fleet_vehicle_assignation_log et) {
        Fleet_vehicle_assignation_log rt = fleet_vehicle_assignation_logFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_assignation_log> list){
        fleet_vehicle_assignation_logFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_assignation_logFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_assignation_logFeignClient.removeBatch(idList);
    }

    @Override
    public Fleet_vehicle_assignation_log getDraft(Fleet_vehicle_assignation_log et) {
        et=fleet_vehicle_assignation_logFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Fleet_vehicle_assignation_log et) {
        Fleet_vehicle_assignation_log rt = fleet_vehicle_assignation_logFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_assignation_log> list){
        fleet_vehicle_assignation_logFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_assignation_log> searchDefault(Fleet_vehicle_assignation_logSearchContext context) {
        Page<Fleet_vehicle_assignation_log> fleet_vehicle_assignation_logs=fleet_vehicle_assignation_logFeignClient.searchDefault(context);
        return fleet_vehicle_assignation_logs;
    }


}



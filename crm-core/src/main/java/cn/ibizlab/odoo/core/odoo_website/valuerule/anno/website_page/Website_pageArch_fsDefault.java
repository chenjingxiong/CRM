package cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_page;

import cn.ibizlab.odoo.core.odoo_website.valuerule.validator.website_page.Website_pageArch_fsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Website_page
 * 属性：Arch_fs
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Website_pageArch_fsDefaultValidator.class})
public @interface Website_pageArch_fsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

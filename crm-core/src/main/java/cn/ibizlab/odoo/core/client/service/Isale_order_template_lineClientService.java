package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_order_template_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_template_line] 服务对象接口
 */
public interface Isale_order_template_lineClientService{

    public Isale_order_template_line createModel() ;

    public Page<Isale_order_template_line> fetchDefault(SearchContext context);

    public void removeBatch(List<Isale_order_template_line> sale_order_template_lines);

    public void update(Isale_order_template_line sale_order_template_line);

    public void get(Isale_order_template_line sale_order_template_line);

    public void create(Isale_order_template_line sale_order_template_line);

    public void createBatch(List<Isale_order_template_line> sale_order_template_lines);

    public void updateBatch(List<Isale_order_template_line> sale_order_template_lines);

    public void remove(Isale_order_template_line sale_order_template_line);

    public Page<Isale_order_template_line> select(SearchContext context);

    public void getDraft(Isale_order_template_line sale_order_template_line);

}

package cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_mail_compose_message;

import cn.ibizlab.odoo.core.odoo_survey.valuerule.validator.survey_mail_compose_message.Survey_mail_compose_messageMessage_typeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Survey_mail_compose_message
 * 属性：Message_type
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Survey_mail_compose_messageMessage_typeDefaultValidator.class})
public @interface Survey_mail_compose_messageMessage_typeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

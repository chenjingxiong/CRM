package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作中心生产力日志] 对象
 */
@Data
public class Mrp_workcenter_productivity extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 时长
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 工作中心
     */
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;

    /**
     * 用户
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 有效性类别
     */
    @JSONField(name = "loss_type")
    @JsonProperty("loss_type")
    private String lossType;

    /**
     * 工单
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 损失原因
     */
    @JSONField(name = "loss_id_text")
    @JsonProperty("loss_id_text")
    private String lossIdText;

    /**
     * 制造订单
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 用户
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 工单
     */
    @DEField(name = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 工作中心
     */
    @DEField(name = "workcenter_id")
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Integer workcenterId;

    /**
     * 损失原因
     */
    @DEField(name = "loss_id")
    @JSONField(name = "loss_id")
    @JsonProperty("loss_id")
    private Integer lossId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odooloss")
    @JsonProperty("odooloss")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss odooLoss;

    /**
     * 
     */
    @JSONField(name = "odooworkcenter")
    @JsonProperty("odooworkcenter")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter;

    /**
     * 
     */
    @JSONField(name = "odooworkorder")
    @JsonProperty("odooworkorder")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [时长]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }
    /**
     * 设置 [用户]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [工单]
     */
    public void setWorkorderId(Integer workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }
    /**
     * 设置 [工作中心]
     */
    public void setWorkcenterId(Integer workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }
    /**
     * 设置 [损失原因]
     */
    public void setLossId(Integer lossId){
        this.lossId = lossId ;
        this.modify("loss_id",lossId);
    }

}



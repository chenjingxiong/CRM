package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_shareService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_portal.client.portal_shareFeignClient;

/**
 * 实体[门户分享] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_shareServiceImpl implements IPortal_shareService {

    @Autowired
    portal_shareFeignClient portal_shareFeignClient;


    @Override
    public boolean update(Portal_share et) {
        Portal_share rt = portal_shareFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Portal_share> list){
        portal_shareFeignClient.updateBatch(list) ;
    }

    @Override
    public Portal_share get(Integer id) {
		Portal_share et=portal_shareFeignClient.get(id);
        if(et==null){
            et=new Portal_share();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Portal_share et) {
        Portal_share rt = portal_shareFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_share> list){
        portal_shareFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=portal_shareFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        portal_shareFeignClient.removeBatch(idList);
    }

    @Override
    public Portal_share getDraft(Portal_share et) {
        et=portal_shareFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_share> searchDefault(Portal_shareSearchContext context) {
        Page<Portal_share> portal_shares=portal_shareFeignClient.searchDefault(context);
        return portal_shares;
    }


}



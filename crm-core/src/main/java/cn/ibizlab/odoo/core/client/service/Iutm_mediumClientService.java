package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_medium] 服务对象接口
 */
public interface Iutm_mediumClientService{

    public Iutm_medium createModel() ;

    public void removeBatch(List<Iutm_medium> utm_media);

    public void createBatch(List<Iutm_medium> utm_media);

    public void create(Iutm_medium utm_medium);

    public void remove(Iutm_medium utm_medium);

    public void updateBatch(List<Iutm_medium> utm_media);

    public void update(Iutm_medium utm_medium);

    public void get(Iutm_medium utm_medium);

    public Page<Iutm_medium> fetchDefault(SearchContext context);

    public Page<Iutm_medium> select(SearchContext context);

    public void getDraft(Iutm_medium utm_medium);

}

package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [消息通知] 对象
 */
@Data
public class Mail_notification extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 失败原因
     */
    @DEField(name = "failure_reason")
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;

    /**
     * 以邮件发送
     */
    @DEField(name = "is_email")
    @JSONField(name = "is_email")
    @JsonProperty("is_email")
    private String isEmail;

    /**
     * 已读
     */
    @DEField(name = "is_read")
    @JSONField(name = "is_read")
    @JsonProperty("is_read")
    private String isRead;

    /**
     * EMail状态
     */
    @DEField(name = "email_status")
    @JSONField(name = "email_status")
    @JsonProperty("email_status")
    private String emailStatus;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 失败类型
     */
    @DEField(name = "failure_type")
    @JSONField(name = "failure_type")
    @JsonProperty("failure_type")
    private String failureType;

    /**
     * 需收件人
     */
    @JSONField(name = "res_partner_id_text")
    @JsonProperty("res_partner_id_text")
    private String resPartnerIdText;

    /**
     * 邮件
     */
    @DEField(name = "mail_id")
    @JSONField(name = "mail_id")
    @JsonProperty("mail_id")
    private Integer mailId;

    /**
     * 消息
     */
    @DEField(name = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Integer mailMessageId;

    /**
     * 需收件人
     */
    @DEField(name = "res_partner_id")
    @JSONField(name = "res_partner_id")
    @JsonProperty("res_partner_id")
    private Integer resPartnerId;


    /**
     * 
     */
    @JSONField(name = "odoomail")
    @JsonProperty("odoomail")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail odooMail;

    /**
     * 
     */
    @JSONField(name = "odoomailmessage")
    @JsonProperty("odoomailmessage")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JSONField(name = "odoorespartner")
    @JsonProperty("odoorespartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooResPartner;




    /**
     * 设置 [失败原因]
     */
    public void setFailureReason(String failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }
    /**
     * 设置 [以邮件发送]
     */
    public void setIsEmail(String isEmail){
        this.isEmail = isEmail ;
        this.modify("is_email",isEmail);
    }
    /**
     * 设置 [已读]
     */
    public void setIsRead(String isRead){
        this.isRead = isRead ;
        this.modify("is_read",isRead);
    }
    /**
     * 设置 [EMail状态]
     */
    public void setEmailStatus(String emailStatus){
        this.emailStatus = emailStatus ;
        this.modify("email_status",emailStatus);
    }
    /**
     * 设置 [失败类型]
     */
    public void setFailureType(String failureType){
        this.failureType = failureType ;
        this.modify("failure_type",failureType);
    }
    /**
     * 设置 [邮件]
     */
    public void setMailId(Integer mailId){
        this.mailId = mailId ;
        this.modify("mail_id",mailId);
    }
    /**
     * 设置 [消息]
     */
    public void setMailMessageId(Integer mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }
    /**
     * 设置 [需收件人]
     */
    public void setResPartnerId(Integer resPartnerId){
        this.resPartnerId = resPartnerId ;
        this.modify("res_partner_id",resPartnerId);
    }

}



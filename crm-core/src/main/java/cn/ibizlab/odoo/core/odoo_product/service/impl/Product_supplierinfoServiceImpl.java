package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_supplierinfoService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_supplierinfoFeignClient;

/**
 * 实体[供应商价格表] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_supplierinfoServiceImpl implements IProduct_supplierinfoService {

    @Autowired
    product_supplierinfoFeignClient product_supplierinfoFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_supplierinfoFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_supplierinfoFeignClient.removeBatch(idList);
    }

    @Override
    public Product_supplierinfo getDraft(Product_supplierinfo et) {
        et=product_supplierinfoFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Product_supplierinfo et) {
        Product_supplierinfo rt = product_supplierinfoFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_supplierinfo> list){
        product_supplierinfoFeignClient.createBatch(list) ;
    }

    @Override
    public Product_supplierinfo get(Integer id) {
		Product_supplierinfo et=product_supplierinfoFeignClient.get(id);
        if(et==null){
            et=new Product_supplierinfo();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Product_supplierinfo et) {
        Product_supplierinfo rt = product_supplierinfoFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_supplierinfo> list){
        product_supplierinfoFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_supplierinfo> searchDefault(Product_supplierinfoSearchContext context) {
        Page<Product_supplierinfo> product_supplierinfos=product_supplierinfoFeignClient.searchDefault(context);
        return product_supplierinfos;
    }


}



package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;


/**
 * 实体[Hr_contract] 服务对象接口
 */
public interface IHr_contractService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_contract et) ;
    void updateBatch(List<Hr_contract> list) ;
    boolean create(Hr_contract et) ;
    void createBatch(List<Hr_contract> list) ;
    Hr_contract get(Integer key) ;
    Hr_contract getDraft(Hr_contract et) ;
    Page<Hr_contract> searchDefault(Hr_contractSearchContext context) ;

}




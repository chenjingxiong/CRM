package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;


/**
 * 实体[Stock_production_lot] 服务对象接口
 */
public interface IStock_production_lotService{

    boolean update(Stock_production_lot et) ;
    void updateBatch(List<Stock_production_lot> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_production_lot get(Integer key) ;
    boolean create(Stock_production_lot et) ;
    void createBatch(List<Stock_production_lot> list) ;
    Stock_production_lot getDraft(Stock_production_lot et) ;
    Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context) ;

}




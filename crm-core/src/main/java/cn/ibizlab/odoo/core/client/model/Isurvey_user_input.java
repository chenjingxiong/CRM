package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [survey_user_input] 对象
 */
public interface Isurvey_user_input {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setDate_create(Timestamp date_create);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getDate_create();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getDate_createDirtyFlag();
    /**
     * 获取 [截止日期]
     */
    public void setDeadline(Timestamp deadline);
    
    /**
     * 设置 [截止日期]
     */
    public Timestamp getDeadline();

    /**
     * 获取 [截止日期]脏标记
     */
    public boolean getDeadlineDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最后显示页面]
     */
    public void setLast_displayed_page_id(Integer last_displayed_page_id);
    
    /**
     * 设置 [最后显示页面]
     */
    public Integer getLast_displayed_page_id();

    /**
     * 获取 [最后显示页面]脏标记
     */
    public boolean getLast_displayed_page_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [空白调查的公开链接]
     */
    public void setPrint_url(String print_url);
    
    /**
     * 设置 [空白调查的公开链接]
     */
    public String getPrint_url();

    /**
     * 获取 [空白调查的公开链接]脏标记
     */
    public boolean getPrint_urlDirtyFlag();
    /**
     * 获取 [测试成绩]
     */
    public void setQuizz_score(Double quizz_score);
    
    /**
     * 设置 [测试成绩]
     */
    public Double getQuizz_score();

    /**
     * 获取 [测试成绩]脏标记
     */
    public boolean getQuizz_scoreDirtyFlag();
    /**
     * 获取 [调查结果的公开链接]
     */
    public void setResult_url(String result_url);
    
    /**
     * 设置 [调查结果的公开链接]
     */
    public String getResult_url();

    /**
     * 获取 [调查结果的公开链接]脏标记
     */
    public boolean getResult_urlDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [问卷]
     */
    public void setSurvey_id(Integer survey_id);
    
    /**
     * 设置 [问卷]
     */
    public Integer getSurvey_id();

    /**
     * 获取 [问卷]脏标记
     */
    public boolean getSurvey_idDirtyFlag();
    /**
     * 获取 [测试问卷]
     */
    public void setTest_entry(String test_entry);
    
    /**
     * 设置 [测试问卷]
     */
    public String getTest_entry();

    /**
     * 获取 [测试问卷]脏标记
     */
    public boolean getTest_entryDirtyFlag();
    /**
     * 获取 [标识令牌]
     */
    public void setToken(String token);
    
    /**
     * 设置 [标识令牌]
     */
    public String getToken();

    /**
     * 获取 [标识令牌]脏标记
     */
    public boolean getTokenDirtyFlag();
    /**
     * 获取 [回复类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [回复类型]
     */
    public String getType();

    /**
     * 获取 [回复类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [答案]
     */
    public void setUser_input_line_ids(String user_input_line_ids);
    
    /**
     * 设置 [答案]
     */
    public String getUser_input_line_ids();

    /**
     * 获取 [答案]脏标记
     */
    public boolean getUser_input_line_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

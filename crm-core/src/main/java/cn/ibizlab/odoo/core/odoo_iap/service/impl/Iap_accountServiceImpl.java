package cn.ibizlab.odoo.core.odoo_iap.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_iap.service.IIap_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_iap.client.iap_accountFeignClient;

/**
 * 实体[IAP 账户] 服务对象接口实现
 */
@Slf4j
@Service
public class Iap_accountServiceImpl implements IIap_accountService {

    @Autowired
    iap_accountFeignClient iap_accountFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=iap_accountFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        iap_accountFeignClient.removeBatch(idList);
    }

    @Override
    public Iap_account get(Integer id) {
		Iap_account et=iap_accountFeignClient.get(id);
        if(et==null){
            et=new Iap_account();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Iap_account getDraft(Iap_account et) {
        et=iap_accountFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Iap_account et) {
        Iap_account rt = iap_accountFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Iap_account> list){
        iap_accountFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Iap_account et) {
        Iap_account rt = iap_accountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Iap_account> list){
        iap_accountFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Iap_account> searchDefault(Iap_accountSearchContext context) {
        Page<Iap_account> iap_accounts=iap_accountFeignClient.searchDefault(context);
        return iap_accounts;
    }


}



package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;


/**
 * 实体[Hr_leave] 服务对象接口
 */
public interface IHr_leaveService{

    boolean update(Hr_leave et) ;
    void updateBatch(List<Hr_leave> list) ;
    boolean create(Hr_leave et) ;
    void createBatch(List<Hr_leave> list) ;
    Hr_leave getDraft(Hr_leave et) ;
    Hr_leave get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_leave> searchDefault(Hr_leaveSearchContext context) ;

}




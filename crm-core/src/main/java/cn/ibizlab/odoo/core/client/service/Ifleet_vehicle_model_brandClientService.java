package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model_brand;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
public interface Ifleet_vehicle_model_brandClientService{

    public Ifleet_vehicle_model_brand createModel() ;

    public void updateBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands);

    public void createBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands);

    public void removeBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands);

    public Page<Ifleet_vehicle_model_brand> fetchDefault(SearchContext context);

    public void get(Ifleet_vehicle_model_brand fleet_vehicle_model_brand);

    public void remove(Ifleet_vehicle_model_brand fleet_vehicle_model_brand);

    public void update(Ifleet_vehicle_model_brand fleet_vehicle_model_brand);

    public void create(Ifleet_vehicle_model_brand fleet_vehicle_model_brand);

    public Page<Ifleet_vehicle_model_brand> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_model_brand fleet_vehicle_model_brand);

}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mro_order] 对象
 */
public interface Imro_order {

    /**
     * 获取 [Asset]
     */
    public void setAsset_id(Integer asset_id);
    
    /**
     * 设置 [Asset]
     */
    public Integer getAsset_id();

    /**
     * 获取 [Asset]脏标记
     */
    public boolean getAsset_idDirtyFlag();
    /**
     * 获取 [Asset]
     */
    public void setAsset_id_text(String asset_id_text);
    
    /**
     * 设置 [Asset]
     */
    public String getAsset_id_text();

    /**
     * 获取 [Asset]脏标记
     */
    public boolean getAsset_id_textDirtyFlag();
    /**
     * 获取 [Asset Category]
     */
    public void setCategory_ids(String category_ids);
    
    /**
     * 设置 [Asset Category]
     */
    public String getCategory_ids();

    /**
     * 获取 [Asset Category]脏标记
     */
    public boolean getCategory_idsDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [Execution Date]
     */
    public void setDate_execution(Timestamp date_execution);
    
    /**
     * 设置 [Execution Date]
     */
    public Timestamp getDate_execution();

    /**
     * 获取 [Execution Date]脏标记
     */
    public boolean getDate_executionDirtyFlag();
    /**
     * 获取 [Planned Date]
     */
    public void setDate_planned(Timestamp date_planned);
    
    /**
     * 设置 [Planned Date]
     */
    public Timestamp getDate_planned();

    /**
     * 获取 [Planned Date]脏标记
     */
    public boolean getDate_plannedDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setDate_scheduled(Timestamp date_scheduled);
    
    /**
     * 设置 [计划日期]
     */
    public Timestamp getDate_scheduled();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getDate_scheduledDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [Documentation Description]
     */
    public void setDocumentation_description(String documentation_description);
    
    /**
     * 设置 [Documentation Description]
     */
    public String getDocumentation_description();

    /**
     * 获取 [Documentation Description]脏标记
     */
    public boolean getDocumentation_descriptionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [Labor Description]
     */
    public void setLabor_description(String labor_description);
    
    /**
     * 设置 [Labor Description]
     */
    public String getLabor_description();

    /**
     * 获取 [Labor Description]脏标记
     */
    public boolean getLabor_descriptionDirtyFlag();
    /**
     * 获取 [保养类型]
     */
    public void setMaintenance_type(String maintenance_type);
    
    /**
     * 设置 [保养类型]
     */
    public String getMaintenance_type();

    /**
     * 获取 [保养类型]脏标记
     */
    public boolean getMaintenance_typeDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误个数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误个数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误个数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [前置操作]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [前置操作]
     */
    public String getMessage_needaction();

    /**
     * 获取 [前置操作]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作次数]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作次数]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作次数]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [编号]
     */
    public void setName(String name);
    
    /**
     * 设置 [编号]
     */
    public String getName();

    /**
     * 获取 [编号]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [Operations Description]
     */
    public void setOperations_description(String operations_description);
    
    /**
     * 设置 [Operations Description]
     */
    public String getOperations_description();

    /**
     * 获取 [Operations Description]脏标记
     */
    public boolean getOperations_descriptionDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [Planned parts]
     */
    public void setParts_lines(String parts_lines);
    
    /**
     * 设置 [Planned parts]
     */
    public String getParts_lines();

    /**
     * 获取 [Planned parts]脏标记
     */
    public boolean getParts_linesDirtyFlag();
    /**
     * 获取 [Parts Moved Lines]
     */
    public void setParts_moved_lines(String parts_moved_lines);
    
    /**
     * 设置 [Parts Moved Lines]
     */
    public String getParts_moved_lines();

    /**
     * 获取 [Parts Moved Lines]脏标记
     */
    public boolean getParts_moved_linesDirtyFlag();
    /**
     * 获取 [Parts Move Lines]
     */
    public void setParts_move_lines(String parts_move_lines);
    
    /**
     * 设置 [Parts Move Lines]
     */
    public String getParts_move_lines();

    /**
     * 获取 [Parts Move Lines]脏标记
     */
    public boolean getParts_move_linesDirtyFlag();
    /**
     * 获取 [Parts Ready Lines]
     */
    public void setParts_ready_lines(String parts_ready_lines);
    
    /**
     * 设置 [Parts Ready Lines]
     */
    public String getParts_ready_lines();

    /**
     * 获取 [Parts Ready Lines]脏标记
     */
    public boolean getParts_ready_linesDirtyFlag();
    /**
     * 获取 [Problem Description]
     */
    public void setProblem_description(String problem_description);
    
    /**
     * 设置 [Problem Description]
     */
    public String getProblem_description();

    /**
     * 获取 [Problem Description]脏标记
     */
    public boolean getProblem_descriptionDirtyFlag();
    /**
     * 获取 [Procurement group]
     */
    public void setProcurement_group_id(Integer procurement_group_id);
    
    /**
     * 设置 [Procurement group]
     */
    public Integer getProcurement_group_id();

    /**
     * 获取 [Procurement group]脏标记
     */
    public boolean getProcurement_group_idDirtyFlag();
    /**
     * 获取 [请求]
     */
    public void setRequest_id(Integer request_id);
    
    /**
     * 设置 [请求]
     */
    public Integer getRequest_id();

    /**
     * 获取 [请求]脏标记
     */
    public boolean getRequest_idDirtyFlag();
    /**
     * 获取 [请求]
     */
    public void setRequest_id_text(String request_id_text);
    
    /**
     * 设置 [请求]
     */
    public String getRequest_id_text();

    /**
     * 获取 [请求]脏标记
     */
    public boolean getRequest_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [Task]
     */
    public void setTask_id(Integer task_id);
    
    /**
     * 设置 [Task]
     */
    public Integer getTask_id();

    /**
     * 获取 [Task]脏标记
     */
    public boolean getTask_idDirtyFlag();
    /**
     * 获取 [Task]
     */
    public void setTask_id_text(String task_id_text);
    
    /**
     * 设置 [Task]
     */
    public String getTask_id_text();

    /**
     * 获取 [Task]脏标记
     */
    public boolean getTask_id_textDirtyFlag();
    /**
     * 获取 [Tools Description]
     */
    public void setTools_description(String tools_description);
    
    /**
     * 设置 [Tools Description]
     */
    public String getTools_description();

    /**
     * 获取 [Tools Description]脏标记
     */
    public boolean getTools_descriptionDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWo_id(Integer wo_id);
    
    /**
     * 设置 [工单]
     */
    public Integer getWo_id();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWo_idDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWo_id_text(String wo_id_text);
    
    /**
     * 设置 [工单]
     */
    public String getWo_id_text();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWo_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

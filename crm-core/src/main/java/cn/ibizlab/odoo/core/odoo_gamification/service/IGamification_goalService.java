package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;


/**
 * 实体[Gamification_goal] 服务对象接口
 */
public interface IGamification_goalService{

    boolean create(Gamification_goal et) ;
    void createBatch(List<Gamification_goal> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Gamification_goal et) ;
    void updateBatch(List<Gamification_goal> list) ;
    Gamification_goal get(Integer key) ;
    Gamification_goal getDraft(Gamification_goal et) ;
    Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context) ;

}




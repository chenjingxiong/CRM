package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_line] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-invoice-line", fallback = account_invoice_lineFallback.class)
public interface account_invoice_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/{id}")
    Account_invoice_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines")
    Account_invoice_line create(@RequestBody Account_invoice_line account_invoice_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_line> account_invoice_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/searchdefault")
    Page<Account_invoice_line> searchDefault(@RequestBody Account_invoice_lineSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/{id}")
    Account_invoice_line update(@PathVariable("id") Integer id,@RequestBody Account_invoice_line account_invoice_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_line> account_invoice_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/select")
    Page<Account_invoice_line> select();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/checkkey")
    Boolean checkKey(@RequestBody Account_invoice_line account_invoice_line);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/getdraft")
    Account_invoice_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/save")
    Boolean save(@RequestBody Account_invoice_line account_invoice_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/save")
    Boolean saveBatch(@RequestBody List<Account_invoice_line> account_invoice_lines);


}

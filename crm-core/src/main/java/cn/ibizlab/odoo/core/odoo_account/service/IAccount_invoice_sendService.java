package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;


/**
 * 实体[Account_invoice_send] 服务对象接口
 */
public interface IAccount_invoice_sendService{

    Account_invoice_send getDraft(Account_invoice_send et) ;
    Account_invoice_send get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_invoice_send et) ;
    void updateBatch(List<Account_invoice_send> list) ;
    boolean create(Account_invoice_send et) ;
    void createBatch(List<Account_invoice_send> list) ;
    Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context) ;

}




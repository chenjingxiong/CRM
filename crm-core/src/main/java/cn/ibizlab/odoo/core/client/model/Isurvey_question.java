package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [survey_question] 对象
 */
public interface Isurvey_question {

    /**
     * 获取 [栏位数]
     */
    public void setColumn_nb(String column_nb);
    
    /**
     * 设置 [栏位数]
     */
    public String getColumn_nb();

    /**
     * 获取 [栏位数]脏标记
     */
    public boolean getColumn_nbDirtyFlag();
    /**
     * 获取 [显示评论字段]
     */
    public void setComments_allowed(String comments_allowed);
    
    /**
     * 设置 [显示评论字段]
     */
    public String getComments_allowed();

    /**
     * 获取 [显示评论字段]脏标记
     */
    public boolean getComments_allowedDirtyFlag();
    /**
     * 获取 [评论消息]
     */
    public void setComments_message(String comments_message);
    
    /**
     * 设置 [评论消息]
     */
    public String getComments_message();

    /**
     * 获取 [评论消息]脏标记
     */
    public boolean getComments_messageDirtyFlag();
    /**
     * 获取 [评论字段是答案选项]
     */
    public void setComment_count_as_answer(String comment_count_as_answer);
    
    /**
     * 设置 [评论字段是答案选项]
     */
    public String getComment_count_as_answer();

    /**
     * 获取 [评论字段是答案选项]脏标记
     */
    public boolean getComment_count_as_answerDirtyFlag();
    /**
     * 获取 [错误消息]
     */
    public void setConstr_error_msg(String constr_error_msg);
    
    /**
     * 设置 [错误消息]
     */
    public String getConstr_error_msg();

    /**
     * 获取 [错误消息]脏标记
     */
    public boolean getConstr_error_msgDirtyFlag();
    /**
     * 获取 [必答问题]
     */
    public void setConstr_mandatory(String constr_mandatory);
    
    /**
     * 设置 [必答问题]
     */
    public String getConstr_mandatory();

    /**
     * 获取 [必答问题]脏标记
     */
    public boolean getConstr_mandatoryDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示模式]
     */
    public void setDisplay_mode(String display_mode);
    
    /**
     * 设置 [显示模式]
     */
    public String getDisplay_mode();

    /**
     * 获取 [显示模式]脏标记
     */
    public boolean getDisplay_modeDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [答案类型]
     */
    public void setLabels_ids(String labels_ids);
    
    /**
     * 设置 [答案类型]
     */
    public String getLabels_ids();

    /**
     * 获取 [答案类型]脏标记
     */
    public boolean getLabels_idsDirtyFlag();
    /**
     * 获取 [表格行数]
     */
    public void setLabels_ids_2(String labels_ids_2);
    
    /**
     * 设置 [表格行数]
     */
    public String getLabels_ids_2();

    /**
     * 获取 [表格行数]脏标记
     */
    public boolean getLabels_ids_2DirtyFlag();
    /**
     * 获取 [表格类型]
     */
    public void setMatrix_subtype(String matrix_subtype);
    
    /**
     * 设置 [表格类型]
     */
    public String getMatrix_subtype();

    /**
     * 获取 [表格类型]脏标记
     */
    public boolean getMatrix_subtypeDirtyFlag();
    /**
     * 获取 [调查页面]
     */
    public void setPage_id(Integer page_id);
    
    /**
     * 设置 [调查页面]
     */
    public Integer getPage_id();

    /**
     * 获取 [调查页面]脏标记
     */
    public boolean getPage_idDirtyFlag();
    /**
     * 获取 [问题名称]
     */
    public void setQuestion(String question);
    
    /**
     * 设置 [问题名称]
     */
    public String getQuestion();

    /**
     * 获取 [问题名称]脏标记
     */
    public boolean getQuestionDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [问卷]
     */
    public void setSurvey_id(Integer survey_id);
    
    /**
     * 设置 [问卷]
     */
    public Integer getSurvey_id();

    /**
     * 获取 [问卷]脏标记
     */
    public boolean getSurvey_idDirtyFlag();
    /**
     * 获取 [问题类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [问题类型]
     */
    public String getType();

    /**
     * 获取 [问题类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [答案]
     */
    public void setUser_input_line_ids(String user_input_line_ids);
    
    /**
     * 设置 [答案]
     */
    public String getUser_input_line_ids();

    /**
     * 获取 [答案]脏标记
     */
    public boolean getUser_input_line_idsDirtyFlag();
    /**
     * 获取 [输入必须是EMail]
     */
    public void setValidation_email(String validation_email);
    
    /**
     * 设置 [输入必须是EMail]
     */
    public String getValidation_email();

    /**
     * 获取 [输入必须是EMail]脏标记
     */
    public boolean getValidation_emailDirtyFlag();
    /**
     * 获取 [信息：验证错误]
     */
    public void setValidation_error_msg(String validation_error_msg);
    
    /**
     * 设置 [信息：验证错误]
     */
    public String getValidation_error_msg();

    /**
     * 获取 [信息：验证错误]脏标记
     */
    public boolean getValidation_error_msgDirtyFlag();
    /**
     * 获取 [最大文本长度]
     */
    public void setValidation_length_max(Integer validation_length_max);
    
    /**
     * 设置 [最大文本长度]
     */
    public Integer getValidation_length_max();

    /**
     * 获取 [最大文本长度]脏标记
     */
    public boolean getValidation_length_maxDirtyFlag();
    /**
     * 获取 [最小文本长度]
     */
    public void setValidation_length_min(Integer validation_length_min);
    
    /**
     * 设置 [最小文本长度]
     */
    public Integer getValidation_length_min();

    /**
     * 获取 [最小文本长度]脏标记
     */
    public boolean getValidation_length_minDirtyFlag();
    /**
     * 获取 [最大日期]
     */
    public void setValidation_max_date(Timestamp validation_max_date);
    
    /**
     * 设置 [最大日期]
     */
    public Timestamp getValidation_max_date();

    /**
     * 获取 [最大日期]脏标记
     */
    public boolean getValidation_max_dateDirtyFlag();
    /**
     * 获取 [最大值]
     */
    public void setValidation_max_float_value(Double validation_max_float_value);
    
    /**
     * 设置 [最大值]
     */
    public Double getValidation_max_float_value();

    /**
     * 获取 [最大值]脏标记
     */
    public boolean getValidation_max_float_valueDirtyFlag();
    /**
     * 获取 [最小日期]
     */
    public void setValidation_min_date(Timestamp validation_min_date);
    
    /**
     * 设置 [最小日期]
     */
    public Timestamp getValidation_min_date();

    /**
     * 获取 [最小日期]脏标记
     */
    public boolean getValidation_min_dateDirtyFlag();
    /**
     * 获取 [最小值]
     */
    public void setValidation_min_float_value(Double validation_min_float_value);
    
    /**
     * 设置 [最小值]
     */
    public Double getValidation_min_float_value();

    /**
     * 获取 [最小值]脏标记
     */
    public boolean getValidation_min_float_valueDirtyFlag();
    /**
     * 获取 [验证文本]
     */
    public void setValidation_required(String validation_required);
    
    /**
     * 设置 [验证文本]
     */
    public String getValidation_required();

    /**
     * 获取 [验证文本]脏标记
     */
    public boolean getValidation_requiredDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

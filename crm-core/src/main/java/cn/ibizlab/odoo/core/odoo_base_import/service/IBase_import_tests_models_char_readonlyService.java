package cn.ibizlab.odoo.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;


/**
 * 实体[Base_import_tests_models_char_readonly] 服务对象接口
 */
public interface IBase_import_tests_models_char_readonlyService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Base_import_tests_models_char_readonly getDraft(Base_import_tests_models_char_readonly et) ;
    boolean update(Base_import_tests_models_char_readonly et) ;
    void updateBatch(List<Base_import_tests_models_char_readonly> list) ;
    boolean create(Base_import_tests_models_char_readonly et) ;
    void createBatch(List<Base_import_tests_models_char_readonly> list) ;
    Base_import_tests_models_char_readonly get(Integer key) ;
    Page<Base_import_tests_models_char_readonly> searchDefault(Base_import_tests_models_char_readonlySearchContext context) ;

}




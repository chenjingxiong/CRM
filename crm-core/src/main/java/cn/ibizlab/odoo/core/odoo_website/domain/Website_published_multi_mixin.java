package cn.ibizlab.odoo.core.odoo_website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [多网站发布的Mixin] 对象
 */
@Data
public class Website_published_multi_mixin extends EntityClient implements Serializable {

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;





    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

}



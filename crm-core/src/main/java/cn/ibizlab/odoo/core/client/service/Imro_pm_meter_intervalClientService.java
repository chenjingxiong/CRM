package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_interval;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
public interface Imro_pm_meter_intervalClientService{

    public Imro_pm_meter_interval createModel() ;

    public void update(Imro_pm_meter_interval mro_pm_meter_interval);

    public Page<Imro_pm_meter_interval> fetchDefault(SearchContext context);

    public void removeBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals);

    public void remove(Imro_pm_meter_interval mro_pm_meter_interval);

    public void createBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals);

    public void create(Imro_pm_meter_interval mro_pm_meter_interval);

    public void get(Imro_pm_meter_interval mro_pm_meter_interval);

    public void updateBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals);

    public Page<Imro_pm_meter_interval> select(SearchContext context);

    public void getDraft(Imro_pm_meter_interval mro_pm_meter_interval);

}

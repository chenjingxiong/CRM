package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;


/**
 * 实体[Survey_page] 服务对象接口
 */
public interface ISurvey_pageService{

    boolean update(Survey_page et) ;
    void updateBatch(List<Survey_page> list) ;
    Survey_page getDraft(Survey_page et) ;
    Survey_page get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Survey_page et) ;
    void createBatch(List<Survey_page> list) ;
    Page<Survey_page> searchDefault(Survey_pageSearchContext context) ;

}




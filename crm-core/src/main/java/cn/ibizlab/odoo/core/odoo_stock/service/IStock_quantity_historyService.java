package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;


/**
 * 实体[Stock_quantity_history] 服务对象接口
 */
public interface IStock_quantity_historyService{

    Stock_quantity_history get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_quantity_history et) ;
    void createBatch(List<Stock_quantity_history> list) ;
    boolean update(Stock_quantity_history et) ;
    void updateBatch(List<Stock_quantity_history> list) ;
    Stock_quantity_history getDraft(Stock_quantity_history et) ;
    Page<Stock_quantity_history> searchDefault(Stock_quantity_historySearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
@Component
public class im_livechat_channel_ruleFallback implements im_livechat_channel_ruleFeignClient{


    public Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context){
            return null;
     }



    public Im_livechat_channel_rule get(Integer id){
            return null;
     }


    public Im_livechat_channel_rule update(Integer id, Im_livechat_channel_rule im_livechat_channel_rule){
            return null;
     }
    public Boolean updateBatch(List<Im_livechat_channel_rule> im_livechat_channel_rules){
            return false;
     }


    public Im_livechat_channel_rule create(Im_livechat_channel_rule im_livechat_channel_rule){
            return null;
     }
    public Boolean createBatch(List<Im_livechat_channel_rule> im_livechat_channel_rules){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Im_livechat_channel_rule> select(){
            return null;
     }

    public Im_livechat_channel_rule getDraft(){
            return null;
    }



}

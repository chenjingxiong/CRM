package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_accountFeignClient;

/**
 * 实体[科目] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_accountServiceImpl implements IAccount_accountService {

    @Autowired
    account_accountFeignClient account_accountFeignClient;


    @Override
    public Account_account get(Integer id) {
		Account_account et=account_accountFeignClient.get(id);
        if(et==null){
            et=new Account_account();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_account getDraft(Account_account et) {
        et=account_accountFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_account et) {
        Account_account rt = account_accountFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_account> list){
        account_accountFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_accountFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_accountFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_account et) {
        Account_account rt = account_accountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account> list){
        account_accountFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account> searchDefault(Account_accountSearchContext context) {
        Page<Account_account> account_accounts=account_accountFeignClient.searchDefault(context);
        return account_accounts;
    }


}



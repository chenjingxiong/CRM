package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;


/**
 * 实体[Survey_user_input] 服务对象接口
 */
public interface ISurvey_user_inputService{

    boolean update(Survey_user_input et) ;
    void updateBatch(List<Survey_user_input> list) ;
    boolean create(Survey_user_input et) ;
    void createBatch(List<Survey_user_input> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Survey_user_input getDraft(Survey_user_input et) ;
    Survey_user_input get(Integer key) ;
    Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context) ;

}




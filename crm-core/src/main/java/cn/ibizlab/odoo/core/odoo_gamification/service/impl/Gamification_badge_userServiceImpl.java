package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_userService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_badge_userFeignClient;

/**
 * 实体[游戏化用户徽章] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_badge_userServiceImpl implements IGamification_badge_userService {

    @Autowired
    gamification_badge_userFeignClient gamification_badge_userFeignClient;


    @Override
    public Gamification_badge_user getDraft(Gamification_badge_user et) {
        et=gamification_badge_userFeignClient.getDraft();
        return et;
    }

    @Override
    public Gamification_badge_user get(Integer id) {
		Gamification_badge_user et=gamification_badge_userFeignClient.get(id);
        if(et==null){
            et=new Gamification_badge_user();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Gamification_badge_user et) {
        Gamification_badge_user rt = gamification_badge_userFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_badge_user> list){
        gamification_badge_userFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_badge_userFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_badge_userFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Gamification_badge_user et) {
        Gamification_badge_user rt = gamification_badge_userFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_badge_user> list){
        gamification_badge_userFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_badge_user> searchDefault(Gamification_badge_userSearchContext context) {
        Page<Gamification_badge_user> gamification_badge_users=gamification_badge_userFeignClient.searchDefault(context);
        return gamification_badge_users;
    }


}



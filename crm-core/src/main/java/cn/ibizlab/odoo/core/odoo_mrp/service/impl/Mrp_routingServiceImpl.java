package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_routingFeignClient;

/**
 * 实体[工艺] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_routingServiceImpl implements IMrp_routingService {

    @Autowired
    mrp_routingFeignClient mrp_routingFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_routingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_routingFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_routing get(Integer id) {
		Mrp_routing et=mrp_routingFeignClient.get(id);
        if(et==null){
            et=new Mrp_routing();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mrp_routing getDraft(Mrp_routing et) {
        et=mrp_routingFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mrp_routing et) {
        Mrp_routing rt = mrp_routingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_routing> list){
        mrp_routingFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mrp_routing et) {
        Mrp_routing rt = mrp_routingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_routing> list){
        mrp_routingFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context) {
        Page<Mrp_routing> mrp_routings=mrp_routingFeignClient.searchDefault(context);
        return mrp_routings;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_type] 服务对象接口
 */
public interface Ievent_typeClientService{

    public Ievent_type createModel() ;

    public void updateBatch(List<Ievent_type> event_types);

    public void createBatch(List<Ievent_type> event_types);

    public void removeBatch(List<Ievent_type> event_types);

    public void get(Ievent_type event_type);

    public Page<Ievent_type> fetchDefault(SearchContext context);

    public void create(Ievent_type event_type);

    public void remove(Ievent_type event_type);

    public void update(Ievent_type event_type);

    public Page<Ievent_type> select(SearchContext context);

    public void getDraft(Ievent_type event_type);

}

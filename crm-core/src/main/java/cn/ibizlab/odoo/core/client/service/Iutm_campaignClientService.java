package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iutm_campaign;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_campaign] 服务对象接口
 */
public interface Iutm_campaignClientService{

    public Iutm_campaign createModel() ;

    public void remove(Iutm_campaign utm_campaign);

    public void get(Iutm_campaign utm_campaign);

    public void update(Iutm_campaign utm_campaign);

    public void createBatch(List<Iutm_campaign> utm_campaigns);

    public void create(Iutm_campaign utm_campaign);

    public void removeBatch(List<Iutm_campaign> utm_campaigns);

    public void updateBatch(List<Iutm_campaign> utm_campaigns);

    public Page<Iutm_campaign> fetchDefault(SearchContext context);

    public Page<Iutm_campaign> select(SearchContext context);

    public void getDraft(Iutm_campaign utm_campaign);

}

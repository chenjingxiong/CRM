package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_statistics_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_statistics_reportFeignClient;

/**
 * 实体[群发邮件阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_statistics_reportServiceImpl implements IMail_statistics_reportService {

    @Autowired
    mail_statistics_reportFeignClient mail_statistics_reportFeignClient;


    @Override
    public boolean update(Mail_statistics_report et) {
        Mail_statistics_report rt = mail_statistics_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_statistics_report> list){
        mail_statistics_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_statistics_report getDraft(Mail_statistics_report et) {
        et=mail_statistics_reportFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_statistics_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_statistics_reportFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_statistics_report get(Integer id) {
		Mail_statistics_report et=mail_statistics_reportFeignClient.get(id);
        if(et==null){
            et=new Mail_statistics_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_statistics_report et) {
        Mail_statistics_report rt = mail_statistics_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_statistics_report> list){
        mail_statistics_reportFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context) {
        Page<Mail_statistics_report> mail_statistics_reports=mail_statistics_reportFeignClient.searchDefault(context);
        return mail_statistics_reports;
    }


}



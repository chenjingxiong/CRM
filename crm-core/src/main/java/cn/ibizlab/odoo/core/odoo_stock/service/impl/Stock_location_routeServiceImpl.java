package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_location_routeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_location_routeFeignClient;

/**
 * 实体[库存路线] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_location_routeServiceImpl implements IStock_location_routeService {

    @Autowired
    stock_location_routeFeignClient stock_location_routeFeignClient;


    @Override
    public boolean create(Stock_location_route et) {
        Stock_location_route rt = stock_location_routeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_location_route> list){
        stock_location_routeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_location_routeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_location_routeFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_location_route get(Integer id) {
		Stock_location_route et=stock_location_routeFeignClient.get(id);
        if(et==null){
            et=new Stock_location_route();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_location_route getDraft(Stock_location_route et) {
        et=stock_location_routeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_location_route et) {
        Stock_location_route rt = stock_location_routeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_location_route> list){
        stock_location_routeFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context) {
        Page<Stock_location_route> stock_location_routes=stock_location_routeFeignClient.searchDefault(context);
        return stock_location_routes;
    }


}



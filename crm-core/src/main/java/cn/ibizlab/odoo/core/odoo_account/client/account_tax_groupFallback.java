package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_tax_group] 服务对象接口
 */
@Component
public class account_tax_groupFallback implements account_tax_groupFeignClient{


    public Account_tax_group create(Account_tax_group account_tax_group){
            return null;
     }
    public Boolean createBatch(List<Account_tax_group> account_tax_groups){
            return false;
     }

    public Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context){
            return null;
     }


    public Account_tax_group get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_tax_group update(Integer id, Account_tax_group account_tax_group){
            return null;
     }
    public Boolean updateBatch(List<Account_tax_group> account_tax_groups){
            return false;
     }



    public Page<Account_tax_group> select(){
            return null;
     }

    public Account_tax_group getDraft(){
            return null;
    }



}

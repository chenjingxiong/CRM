package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_update_translations;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_update_translations] 服务对象接口
 */
public interface Ibase_update_translationsClientService{

    public Ibase_update_translations createModel() ;

    public void remove(Ibase_update_translations base_update_translations);

    public void update(Ibase_update_translations base_update_translations);

    public void create(Ibase_update_translations base_update_translations);

    public void updateBatch(List<Ibase_update_translations> base_update_translations);

    public Page<Ibase_update_translations> fetchDefault(SearchContext context);

    public void createBatch(List<Ibase_update_translations> base_update_translations);

    public void removeBatch(List<Ibase_update_translations> base_update_translations);

    public void get(Ibase_update_translations base_update_translations);

    public Page<Ibase_update_translations> select(SearchContext context);

    public void getDraft(Ibase_update_translations base_update_translations);

}

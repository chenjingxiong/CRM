package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_leave_type] 对象
 */
public interface Ihr_leave_type {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [模式]
     */
    public void setAllocation_type(String allocation_type);
    
    /**
     * 设置 [模式]
     */
    public String getAllocation_type();

    /**
     * 获取 [模式]脏标记
     */
    public boolean getAllocation_typeDirtyFlag();
    /**
     * 获取 [会议类型]
     */
    public void setCateg_id(Integer categ_id);
    
    /**
     * 设置 [会议类型]
     */
    public Integer getCateg_id();

    /**
     * 获取 [会议类型]脏标记
     */
    public boolean getCateg_idDirtyFlag();
    /**
     * 获取 [会议类型]
     */
    public void setCateg_id_text(String categ_id_text);
    
    /**
     * 设置 [会议类型]
     */
    public String getCateg_id_text();

    /**
     * 获取 [会议类型]脏标记
     */
    public boolean getCateg_id_textDirtyFlag();
    /**
     * 获取 [报表中的颜色]
     */
    public void setColor_name(String color_name);
    
    /**
     * 设置 [报表中的颜色]
     */
    public String getColor_name();

    /**
     * 获取 [报表中的颜色]脏标记
     */
    public boolean getColor_nameDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [应用双重验证]
     */
    public void setDouble_validation(String double_validation);
    
    /**
     * 设置 [应用双重验证]
     */
    public String getDouble_validation();

    /**
     * 获取 [应用双重验证]脏标记
     */
    public boolean getDouble_validationDirtyFlag();
    /**
     * 获取 [分配天数]
     */
    public void setGroup_days_allocation(Double group_days_allocation);
    
    /**
     * 设置 [分配天数]
     */
    public Double getGroup_days_allocation();

    /**
     * 获取 [分配天数]脏标记
     */
    public boolean getGroup_days_allocationDirtyFlag();
    /**
     * 获取 [集团假期]
     */
    public void setGroup_days_leave(Double group_days_leave);
    
    /**
     * 设置 [集团假期]
     */
    public Double getGroup_days_leave();

    /**
     * 获取 [集团假期]脏标记
     */
    public boolean getGroup_days_leaveDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [休假早已使用]
     */
    public void setLeaves_taken(Double leaves_taken);
    
    /**
     * 设置 [休假早已使用]
     */
    public Double getLeaves_taken();

    /**
     * 获取 [休假早已使用]脏标记
     */
    public boolean getLeaves_takenDirtyFlag();
    /**
     * 获取 [最大允许]
     */
    public void setMax_leaves(Double max_leaves);
    
    /**
     * 设置 [最大允许]
     */
    public Double getMax_leaves();

    /**
     * 获取 [最大允许]脏标记
     */
    public boolean getMax_leavesDirtyFlag();
    /**
     * 获取 [休假类型]
     */
    public void setName(String name);
    
    /**
     * 设置 [休假类型]
     */
    public String getName();

    /**
     * 获取 [休假类型]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [剩余休假]
     */
    public void setRemaining_leaves(Double remaining_leaves);
    
    /**
     * 设置 [剩余休假]
     */
    public Double getRemaining_leaves();

    /**
     * 获取 [剩余休假]脏标记
     */
    public boolean getRemaining_leavesDirtyFlag();
    /**
     * 获取 [休假]
     */
    public void setRequest_unit(String request_unit);
    
    /**
     * 设置 [休假]
     */
    public String getRequest_unit();

    /**
     * 获取 [休假]脏标记
     */
    public boolean getRequest_unitDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [请假]
     */
    public void setTime_type(String time_type);
    
    /**
     * 设置 [请假]
     */
    public String getTime_type();

    /**
     * 获取 [请假]脏标记
     */
    public boolean getTime_typeDirtyFlag();
    /**
     * 获取 [没付款]
     */
    public void setUnpaid(String unpaid);
    
    /**
     * 设置 [没付款]
     */
    public String getUnpaid();

    /**
     * 获取 [没付款]脏标记
     */
    public boolean getUnpaidDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setValid(String valid);
    
    /**
     * 设置 [有效]
     */
    public String getValid();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getValidDirtyFlag();
    /**
     * 获取 [验证人]
     */
    public void setValidation_type(String validation_type);
    
    /**
     * 设置 [验证人]
     */
    public String getValidation_type();

    /**
     * 获取 [验证人]脏标记
     */
    public boolean getValidation_typeDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setValidity_start(Timestamp validity_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getValidity_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getValidity_startDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setValidity_stop(Timestamp validity_stop);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getValidity_stop();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getValidity_stopDirtyFlag();
    /**
     * 获取 [虚拟剩余休假]
     */
    public void setVirtual_remaining_leaves(Double virtual_remaining_leaves);
    
    /**
     * 设置 [虚拟剩余休假]
     */
    public Double getVirtual_remaining_leaves();

    /**
     * 获取 [虚拟剩余休假]脏标记
     */
    public boolean getVirtual_remaining_leavesDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

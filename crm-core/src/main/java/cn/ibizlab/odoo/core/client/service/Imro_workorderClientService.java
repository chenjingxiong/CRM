package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_workorder;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_workorder] 服务对象接口
 */
public interface Imro_workorderClientService{

    public Imro_workorder createModel() ;

    public void create(Imro_workorder mro_workorder);

    public void update(Imro_workorder mro_workorder);

    public void get(Imro_workorder mro_workorder);

    public void removeBatch(List<Imro_workorder> mro_workorders);

    public void remove(Imro_workorder mro_workorder);

    public void updateBatch(List<Imro_workorder> mro_workorders);

    public void createBatch(List<Imro_workorder> mro_workorders);

    public Page<Imro_workorder> fetchDefault(SearchContext context);

    public Page<Imro_workorder> select(SearchContext context);

    public void getDraft(Imro_workorder mro_workorder);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_rules_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_rules_report] 服务对象接口
 */
public interface Istock_rules_reportClientService{

    public Istock_rules_report createModel() ;

    public Page<Istock_rules_report> fetchDefault(SearchContext context);

    public void createBatch(List<Istock_rules_report> stock_rules_reports);

    public void update(Istock_rules_report stock_rules_report);

    public void removeBatch(List<Istock_rules_report> stock_rules_reports);

    public void get(Istock_rules_report stock_rules_report);

    public void updateBatch(List<Istock_rules_report> stock_rules_reports);

    public void create(Istock_rules_report stock_rules_report);

    public void remove(Istock_rules_report stock_rules_report);

    public Page<Istock_rules_report> select(SearchContext context);

    public void getDraft(Istock_rules_report stock_rules_report);

}

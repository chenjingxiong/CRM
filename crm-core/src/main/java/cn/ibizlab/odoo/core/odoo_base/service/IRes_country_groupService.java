package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_groupSearchContext;


/**
 * 实体[Res_country_group] 服务对象接口
 */
public interface IRes_country_groupService{

    Res_country_group getDraft(Res_country_group et) ;
    Res_country_group get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Res_country_group et) ;
    void updateBatch(List<Res_country_group> list) ;
    boolean create(Res_country_group et) ;
    void createBatch(List<Res_country_group> list) ;
    Page<Res_country_group> searchDefault(Res_country_groupSearchContext context) ;

}




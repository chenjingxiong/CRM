package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_location_route] 服务对象接口
 */
@Component
public class stock_location_routeFallback implements stock_location_routeFeignClient{

    public Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context){
            return null;
     }


    public Stock_location_route create(Stock_location_route stock_location_route){
            return null;
     }
    public Boolean createBatch(List<Stock_location_route> stock_location_routes){
            return false;
     }




    public Stock_location_route get(Integer id){
            return null;
     }


    public Stock_location_route update(Integer id, Stock_location_route stock_location_route){
            return null;
     }
    public Boolean updateBatch(List<Stock_location_route> stock_location_routes){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_location_route> select(){
            return null;
     }

    public Stock_location_route getDraft(){
            return null;
    }



}

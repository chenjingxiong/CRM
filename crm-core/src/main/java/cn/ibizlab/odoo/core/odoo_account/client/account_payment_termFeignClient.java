package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_payment_term] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-payment-term", fallback = account_payment_termFallback.class)
public interface account_payment_termFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms")
    Account_payment_term create(@RequestBody Account_payment_term account_payment_term);

    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/batch")
    Boolean createBatch(@RequestBody List<Account_payment_term> account_payment_terms);





    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/searchdefault")
    Page<Account_payment_term> searchDefault(@RequestBody Account_payment_termSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/{id}")
    Account_payment_term update(@PathVariable("id") Integer id,@RequestBody Account_payment_term account_payment_term);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/batch")
    Boolean updateBatch(@RequestBody List<Account_payment_term> account_payment_terms);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/{id}")
    Account_payment_term get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/select")
    Page<Account_payment_term> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/getdraft")
    Account_payment_term getDraft();


}

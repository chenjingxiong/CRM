package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_product_category] 服务对象接口
 */
@FeignClient(value = "odoo-lunch", contextId = "lunch-product-category", fallback = lunch_product_categoryFallback.class)
public interface lunch_product_categoryFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/searchdefault")
    Page<Lunch_product_category> searchDefault(@RequestBody Lunch_product_categorySearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories")
    Lunch_product_category create(@RequestBody Lunch_product_category lunch_product_category);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/batch")
    Boolean createBatch(@RequestBody List<Lunch_product_category> lunch_product_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/{id}")
    Lunch_product_category update(@PathVariable("id") Integer id,@RequestBody Lunch_product_category lunch_product_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/batch")
    Boolean updateBatch(@RequestBody List<Lunch_product_category> lunch_product_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/{id}")
    Lunch_product_category get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/select")
    Page<Lunch_product_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/getdraft")
    Lunch_product_category getDraft();


}

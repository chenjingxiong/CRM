package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_charSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-char", fallback = base_import_tests_models_charFallback.class)
public interface base_import_tests_models_charFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars")
    Base_import_tests_models_char create(@RequestBody Base_import_tests_models_char base_import_tests_models_char);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char> base_import_tests_models_chars);




    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/searchdefault")
    Page<Base_import_tests_models_char> searchDefault(@RequestBody Base_import_tests_models_charSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_chars/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_chars/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_chars/{id}")
    Base_import_tests_models_char get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_chars/{id}")
    Base_import_tests_models_char update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_char base_import_tests_models_char);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_chars/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char> base_import_tests_models_chars);



    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_chars/select")
    Page<Base_import_tests_models_char> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_chars/getdraft")
    Base_import_tests_models_char getDraft();


}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_packaging;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_packaging] 服务对象接口
 */
public interface Iproduct_packagingClientService{

    public Iproduct_packaging createModel() ;

    public void updateBatch(List<Iproduct_packaging> product_packagings);

    public void createBatch(List<Iproduct_packaging> product_packagings);

    public void get(Iproduct_packaging product_packaging);

    public void removeBatch(List<Iproduct_packaging> product_packagings);

    public void create(Iproduct_packaging product_packaging);

    public void remove(Iproduct_packaging product_packaging);

    public void update(Iproduct_packaging product_packaging);

    public Page<Iproduct_packaging> fetchDefault(SearchContext context);

    public Page<Iproduct_packaging> select(SearchContext context);

    public void getDraft(Iproduct_packaging product_packaging);

}

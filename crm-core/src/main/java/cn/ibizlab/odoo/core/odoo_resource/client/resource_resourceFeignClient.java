package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_resource] 服务对象接口
 */
@FeignClient(value = "odoo-resource", contextId = "resource-resource", fallback = resource_resourceFallback.class)
public interface resource_resourceFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/resource_resources/{id}")
    Resource_resource get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/resource_resources/searchdefault")
    Page<Resource_resource> searchDefault(@RequestBody Resource_resourceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/{id}")
    Resource_resource update(@PathVariable("id") Integer id,@RequestBody Resource_resource resource_resource);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/batch")
    Boolean updateBatch(@RequestBody List<Resource_resource> resource_resources);


    @RequestMapping(method = RequestMethod.POST, value = "/resource_resources")
    Resource_resource create(@RequestBody Resource_resource resource_resource);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_resources/batch")
    Boolean createBatch(@RequestBody List<Resource_resource> resource_resources);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_resources/select")
    Page<Resource_resource> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_resources/getdraft")
    Resource_resource getDraft();


}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [event_event_ticket] 对象
 */
public interface Ievent_event_ticket {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [销售结束]
     */
    public void setDeadline(Timestamp deadline);
    
    /**
     * 设置 [销售结束]
     */
    public Timestamp getDeadline();

    /**
     * 获取 [销售结束]脏标记
     */
    public boolean getDeadlineDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id(Integer event_id);
    
    /**
     * 设置 [活动]
     */
    public Integer getEvent_id();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_idDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id_text(String event_id_text);
    
    /**
     * 设置 [活动]
     */
    public String getEvent_id_text();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_id_textDirtyFlag();
    /**
     * 获取 [活动类别]
     */
    public void setEvent_type_id(Integer event_type_id);
    
    /**
     * 设置 [活动类别]
     */
    public Integer getEvent_type_id();

    /**
     * 获取 [活动类别]脏标记
     */
    public boolean getEvent_type_idDirtyFlag();
    /**
     * 获取 [活动类别]
     */
    public void setEvent_type_id_text(String event_type_id_text);
    
    /**
     * 设置 [活动类别]
     */
    public String getEvent_type_id_text();

    /**
     * 获取 [活动类别]脏标记
     */
    public boolean getEvent_type_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [已结束]
     */
    public void setIs_expired(String is_expired);
    
    /**
     * 设置 [已结束]
     */
    public String getIs_expired();

    /**
     * 获取 [已结束]脏标记
     */
    public boolean getIs_expiredDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [价格]
     */
    public void setPrice(Double price);
    
    /**
     * 设置 [价格]
     */
    public Double getPrice();

    /**
     * 获取 [价格]脏标记
     */
    public boolean getPriceDirtyFlag();
    /**
     * 获取 [降价]
     */
    public void setPrice_reduce(Double price_reduce);
    
    /**
     * 设置 [降价]
     */
    public Double getPrice_reduce();

    /**
     * 获取 [降价]脏标记
     */
    public boolean getPrice_reduceDirtyFlag();
    /**
     * 获取 [减税后价格]
     */
    public void setPrice_reduce_taxinc(Double price_reduce_taxinc);
    
    /**
     * 设置 [减税后价格]
     */
    public Double getPrice_reduce_taxinc();

    /**
     * 获取 [减税后价格]脏标记
     */
    public boolean getPrice_reduce_taxincDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [登记]
     */
    public void setRegistration_ids(String registration_ids);
    
    /**
     * 设置 [登记]
     */
    public String getRegistration_ids();

    /**
     * 获取 [登记]脏标记
     */
    public boolean getRegistration_idsDirtyFlag();
    /**
     * 获取 [可用席位]
     */
    public void setSeats_availability(String seats_availability);
    
    /**
     * 设置 [可用席位]
     */
    public String getSeats_availability();

    /**
     * 获取 [可用席位]脏标记
     */
    public boolean getSeats_availabilityDirtyFlag();
    /**
     * 获取 [可用席位]
     */
    public void setSeats_available(Integer seats_available);
    
    /**
     * 设置 [可用席位]
     */
    public Integer getSeats_available();

    /**
     * 获取 [可用席位]脏标记
     */
    public boolean getSeats_availableDirtyFlag();
    /**
     * 获取 [最多可用席位]
     */
    public void setSeats_max(Integer seats_max);
    
    /**
     * 设置 [最多可用席位]
     */
    public Integer getSeats_max();

    /**
     * 获取 [最多可用席位]脏标记
     */
    public boolean getSeats_maxDirtyFlag();
    /**
     * 获取 [预订席位]
     */
    public void setSeats_reserved(Integer seats_reserved);
    
    /**
     * 设置 [预订席位]
     */
    public Integer getSeats_reserved();

    /**
     * 获取 [预订席位]脏标记
     */
    public boolean getSeats_reservedDirtyFlag();
    /**
     * 获取 [未确认的席位预订]
     */
    public void setSeats_unconfirmed(Integer seats_unconfirmed);
    
    /**
     * 设置 [未确认的席位预订]
     */
    public Integer getSeats_unconfirmed();

    /**
     * 获取 [未确认的席位预订]脏标记
     */
    public boolean getSeats_unconfirmedDirtyFlag();
    /**
     * 获取 [座椅]
     */
    public void setSeats_used(Integer seats_used);
    
    /**
     * 设置 [座椅]
     */
    public Integer getSeats_used();

    /**
     * 获取 [座椅]脏标记
     */
    public boolean getSeats_usedDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [purchase_order_line] 对象
 */
public interface Ipurchase_order_line {

    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id(Integer account_analytic_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAccount_analytic_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAccount_analytic_id_text(String account_analytic_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAccount_analytic_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAccount_analytic_id_textDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [单据日期]
     */
    public void setDate_order(Timestamp date_order);
    
    /**
     * 设置 [单据日期]
     */
    public Timestamp getDate_order();

    /**
     * 获取 [单据日期]脏标记
     */
    public boolean getDate_orderDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setDate_planned(Timestamp date_planned);
    
    /**
     * 设置 [计划日期]
     */
    public Timestamp getDate_planned();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getDate_plannedDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [账单明细行]
     */
    public void setInvoice_lines(String invoice_lines);
    
    /**
     * 设置 [账单明细行]
     */
    public String getInvoice_lines();

    /**
     * 获取 [账单明细行]脏标记
     */
    public boolean getInvoice_linesDirtyFlag();
    /**
     * 获取 [下游移动]
     */
    public void setMove_dest_ids(String move_dest_ids);
    
    /**
     * 设置 [下游移动]
     */
    public String getMove_dest_ids();

    /**
     * 获取 [下游移动]脏标记
     */
    public boolean getMove_dest_idsDirtyFlag();
    /**
     * 获取 [保留]
     */
    public void setMove_ids(String move_ids);
    
    /**
     * 设置 [保留]
     */
    public String getMove_ids();

    /**
     * 获取 [保留]脏标记
     */
    public boolean getMove_idsDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [订货点]
     */
    public void setOrderpoint_id(Integer orderpoint_id);
    
    /**
     * 设置 [订货点]
     */
    public Integer getOrderpoint_id();

    /**
     * 获取 [订货点]脏标记
     */
    public boolean getOrderpoint_idDirtyFlag();
    /**
     * 获取 [订货点]
     */
    public void setOrderpoint_id_text(String orderpoint_id_text);
    
    /**
     * 设置 [订货点]
     */
    public String getOrderpoint_id_text();

    /**
     * 获取 [订货点]脏标记
     */
    public boolean getOrderpoint_id_textDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setOrder_id(Integer order_id);
    
    /**
     * 设置 [订单关联]
     */
    public Integer getOrder_id();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getOrder_idDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setOrder_id_text(String order_id_text);
    
    /**
     * 设置 [订单关联]
     */
    public String getOrder_id_text();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getOrder_id_textDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [小计]
     */
    public void setPrice_subtotal(Double price_subtotal);
    
    /**
     * 设置 [小计]
     */
    public Double getPrice_subtotal();

    /**
     * 获取 [小计]脏标记
     */
    public boolean getPrice_subtotalDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setPrice_tax(Double price_tax);
    
    /**
     * 设置 [税率]
     */
    public Double getPrice_tax();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getPrice_taxDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setPrice_total(Double price_total);
    
    /**
     * 设置 [总计]
     */
    public Double getPrice_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getPrice_totalDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setPrice_unit(Double price_unit);
    
    /**
     * 设置 [单价]
     */
    public Double getPrice_unit();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getPrice_unitDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品图片]
     */
    public void setProduct_image(byte[] product_image);
    
    /**
     * 设置 [产品图片]
     */
    public byte[] getProduct_image();

    /**
     * 获取 [产品图片]脏标记
     */
    public boolean getProduct_imageDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [产品类型]
     */
    public void setProduct_type(String product_type);
    
    /**
     * 设置 [产品类型]
     */
    public String getProduct_type();

    /**
     * 获取 [产品类型]脏标记
     */
    public boolean getProduct_typeDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [数量总计]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [数量总计]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [数量总计]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [开票数量]
     */
    public void setQty_invoiced(Double qty_invoiced);
    
    /**
     * 设置 [开票数量]
     */
    public Double getQty_invoiced();

    /**
     * 获取 [开票数量]脏标记
     */
    public boolean getQty_invoicedDirtyFlag();
    /**
     * 获取 [已接收数量]
     */
    public void setQty_received(Double qty_received);
    
    /**
     * 设置 [已接收数量]
     */
    public Double getQty_received();

    /**
     * 获取 [已接收数量]脏标记
     */
    public boolean getQty_receivedDirtyFlag();
    /**
     * 获取 [原销售项]
     */
    public void setSale_line_id(Integer sale_line_id);
    
    /**
     * 设置 [原销售项]
     */
    public Integer getSale_line_id();

    /**
     * 获取 [原销售项]脏标记
     */
    public boolean getSale_line_idDirtyFlag();
    /**
     * 获取 [原销售项]
     */
    public void setSale_line_id_text(String sale_line_id_text);
    
    /**
     * 设置 [原销售项]
     */
    public String getSale_line_id_text();

    /**
     * 获取 [原销售项]脏标记
     */
    public boolean getSale_line_id_textDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_id(Integer sale_order_id);
    
    /**
     * 设置 [销售订单]
     */
    public Integer getSale_order_id();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_idDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_id_text(String sale_order_id_text);
    
    /**
     * 设置 [销售订单]
     */
    public String getSale_order_id_text();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_id_textDirtyFlag();
    /**
     * 获取 [序列]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序列]
     */
    public Integer getSequence();

    /**
     * 获取 [序列]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTaxes_id(String taxes_id);
    
    /**
     * 设置 [税率]
     */
    public String getTaxes_id();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTaxes_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_states;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_statesSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char_states] 服务对象接口
 */
@Component
public class base_import_tests_models_char_statesFallback implements base_import_tests_models_char_statesFeignClient{




    public Base_import_tests_models_char_states create(Base_import_tests_models_char_states base_import_tests_models_char_states){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char_states> base_import_tests_models_char_states){
            return false;
     }

    public Base_import_tests_models_char_states update(Integer id, Base_import_tests_models_char_states base_import_tests_models_char_states){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char_states> base_import_tests_models_char_states){
            return false;
     }


    public Base_import_tests_models_char_states get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Base_import_tests_models_char_states> searchDefault(Base_import_tests_models_char_statesSearchContext context){
            return null;
     }


    public Page<Base_import_tests_models_char_states> select(){
            return null;
     }

    public Base_import_tests_models_char_states getDraft(){
            return null;
    }



}

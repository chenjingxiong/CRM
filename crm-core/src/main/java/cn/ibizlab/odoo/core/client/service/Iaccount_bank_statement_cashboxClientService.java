package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_cashbox;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
public interface Iaccount_bank_statement_cashboxClientService{

    public Iaccount_bank_statement_cashbox createModel() ;

    public void update(Iaccount_bank_statement_cashbox account_bank_statement_cashbox);

    public void get(Iaccount_bank_statement_cashbox account_bank_statement_cashbox);

    public void removeBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes);

    public Page<Iaccount_bank_statement_cashbox> fetchDefault(SearchContext context);

    public void createBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes);

    public void remove(Iaccount_bank_statement_cashbox account_bank_statement_cashbox);

    public void updateBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes);

    public void create(Iaccount_bank_statement_cashbox account_bank_statement_cashbox);

    public Page<Iaccount_bank_statement_cashbox> select(SearchContext context);

    public void getDraft(Iaccount_bank_statement_cashbox account_bank_statement_cashbox);

}

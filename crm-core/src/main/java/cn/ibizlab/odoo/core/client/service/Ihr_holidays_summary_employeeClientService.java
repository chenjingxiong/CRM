package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_employee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
public interface Ihr_holidays_summary_employeeClientService{

    public Ihr_holidays_summary_employee createModel() ;

    public void remove(Ihr_holidays_summary_employee hr_holidays_summary_employee);

    public Page<Ihr_holidays_summary_employee> fetchDefault(SearchContext context);

    public void updateBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees);

    public void removeBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees);

    public void createBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees);

    public void update(Ihr_holidays_summary_employee hr_holidays_summary_employee);

    public void get(Ihr_holidays_summary_employee hr_holidays_summary_employee);

    public void create(Ihr_holidays_summary_employee hr_holidays_summary_employee);

    public Page<Ihr_holidays_summary_employee> select(SearchContext context);

    public void getDraft(Ihr_holidays_summary_employee hr_holidays_summary_employee);

}

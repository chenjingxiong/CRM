package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_documentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_documentFeignClient;

/**
 * 实体[生产文档] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_documentServiceImpl implements IMrp_documentService {

    @Autowired
    mrp_documentFeignClient mrp_documentFeignClient;


    @Override
    public Mrp_document getDraft(Mrp_document et) {
        et=mrp_documentFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mrp_document et) {
        Mrp_document rt = mrp_documentFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_document> list){
        mrp_documentFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mrp_document et) {
        Mrp_document rt = mrp_documentFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_document> list){
        mrp_documentFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_documentFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_documentFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_document get(Integer id) {
		Mrp_document et=mrp_documentFeignClient.get(id);
        if(et==null){
            et=new Mrp_document();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_document> searchDefault(Mrp_documentSearchContext context) {
        Page<Mrp_document> mrp_documents=mrp_documentFeignClient.searchDefault(context);
        return mrp_documents;
    }


}



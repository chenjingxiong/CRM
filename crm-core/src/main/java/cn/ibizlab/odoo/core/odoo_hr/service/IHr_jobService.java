package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;


/**
 * 实体[Hr_job] 服务对象接口
 */
public interface IHr_jobService{

    boolean update(Hr_job et) ;
    void updateBatch(List<Hr_job> list) ;
    Hr_job getDraft(Hr_job et) ;
    boolean create(Hr_job et) ;
    void createBatch(List<Hr_job> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_job get(Integer key) ;
    Page<Hr_job> searchDefault(Hr_jobSearchContext context) ;

}




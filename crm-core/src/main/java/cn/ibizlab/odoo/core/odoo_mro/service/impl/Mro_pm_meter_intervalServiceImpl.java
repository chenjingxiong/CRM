package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_intervalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meter_intervalFeignClient;

/**
 * 实体[Meter interval] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meter_intervalServiceImpl implements IMro_pm_meter_intervalService {

    @Autowired
    mro_pm_meter_intervalFeignClient mro_pm_meter_intervalFeignClient;


    @Override
    public Mro_pm_meter_interval getDraft(Mro_pm_meter_interval et) {
        et=mro_pm_meter_intervalFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mro_pm_meter_interval et) {
        Mro_pm_meter_interval rt = mro_pm_meter_intervalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter_interval> list){
        mro_pm_meter_intervalFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_pm_meter_interval get(Integer id) {
		Mro_pm_meter_interval et=mro_pm_meter_intervalFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_meter_interval();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_meter_intervalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_meter_intervalFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mro_pm_meter_interval et) {
        Mro_pm_meter_interval rt = mro_pm_meter_intervalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_meter_interval> list){
        mro_pm_meter_intervalFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter_interval> searchDefault(Mro_pm_meter_intervalSearchContext context) {
        Page<Mro_pm_meter_interval> mro_pm_meter_intervals=mro_pm_meter_intervalFeignClient.searchDefault(context);
        return mro_pm_meter_intervals;
    }


}



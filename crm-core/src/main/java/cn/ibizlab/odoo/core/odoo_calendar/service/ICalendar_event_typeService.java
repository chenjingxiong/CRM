package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;


/**
 * 实体[Calendar_event_type] 服务对象接口
 */
public interface ICalendar_event_typeService{

    Calendar_event_type get(Integer key) ;
    boolean create(Calendar_event_type et) ;
    void createBatch(List<Calendar_event_type> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Calendar_event_type et) ;
    void updateBatch(List<Calendar_event_type> list) ;
    Calendar_event_type getDraft(Calendar_event_type et) ;
    Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context) ;

}




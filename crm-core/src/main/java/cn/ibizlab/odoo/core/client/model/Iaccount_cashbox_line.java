package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_cashbox_line] 对象
 */
public interface Iaccount_cashbox_line {

    /**
     * 获取 [钱箱]
     */
    public void setCashbox_id(Integer cashbox_id);
    
    /**
     * 设置 [钱箱]
     */
    public Integer getCashbox_id();

    /**
     * 获取 [钱箱]脏标记
     */
    public boolean getCashbox_idDirtyFlag();
    /**
     * 获取 [硬币／账单　价值]
     */
    public void setCoin_value(Double coin_value);
    
    /**
     * 设置 [硬币／账单　价值]
     */
    public Double getCoin_value();

    /**
     * 获取 [硬币／账单　价值]脏标记
     */
    public boolean getCoin_valueDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    public void setDefault_pos_id(Integer default_pos_id);
    
    /**
     * 设置 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    public Integer getDefault_pos_id();

    /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]脏标记
     */
    public boolean getDefault_pos_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [货币和账单编号]
     */
    public void setNumber(Integer number);
    
    /**
     * 设置 [货币和账单编号]
     */
    public Integer getNumber();

    /**
     * 获取 [货币和账单编号]脏标记
     */
    public boolean getNumberDirtyFlag();
    /**
     * 获取 [小计]
     */
    public void setSubtotal(Double subtotal);
    
    /**
     * 设置 [小计]
     */
    public Double getSubtotal();

    /**
     * 获取 [小计]脏标记
     */
    public boolean getSubtotalDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_readonly;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
public interface Ibase_import_tests_models_char_readonlyClientService{

    public Ibase_import_tests_models_char_readonly createModel() ;

    public void update(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    public void createBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies);

    public void removeBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies);

    public void create(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    public void updateBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies);

    public void remove(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    public Page<Ibase_import_tests_models_char_readonly> fetchDefault(SearchContext context);

    public void get(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    public Page<Ibase_import_tests_models_char_readonly> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_custom_value;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
public interface Iproduct_attribute_custom_valueClientService{

    public Iproduct_attribute_custom_value createModel() ;

    public void get(Iproduct_attribute_custom_value product_attribute_custom_value);

    public Page<Iproduct_attribute_custom_value> fetchDefault(SearchContext context);

    public void createBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values);

    public void removeBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values);

    public void create(Iproduct_attribute_custom_value product_attribute_custom_value);

    public void updateBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values);

    public void update(Iproduct_attribute_custom_value product_attribute_custom_value);

    public void remove(Iproduct_attribute_custom_value product_attribute_custom_value);

    public Page<Iproduct_attribute_custom_value> select(SearchContext context);

    public void getDraft(Iproduct_attribute_custom_value product_attribute_custom_value);

}

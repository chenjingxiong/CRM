package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qtyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qtyFeignClient;

/**
 * 实体[库存不足时发出警告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qtyServiceImpl implements IStock_warn_insufficient_qtyService {

    @Autowired
    stock_warn_insufficient_qtyFeignClient stock_warn_insufficient_qtyFeignClient;


    @Override
    public Stock_warn_insufficient_qty getDraft(Stock_warn_insufficient_qty et) {
        et=stock_warn_insufficient_qtyFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_warn_insufficient_qty et) {
        Stock_warn_insufficient_qty rt = stock_warn_insufficient_qtyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warn_insufficient_qty> list){
        stock_warn_insufficient_qtyFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_warn_insufficient_qty get(Integer id) {
		Stock_warn_insufficient_qty et=stock_warn_insufficient_qtyFeignClient.get(id);
        if(et==null){
            et=new Stock_warn_insufficient_qty();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warn_insufficient_qtyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warn_insufficient_qtyFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty et) {
        Stock_warn_insufficient_qty rt = stock_warn_insufficient_qtyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty> list){
        stock_warn_insufficient_qtyFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty> searchDefault(Stock_warn_insufficient_qtySearchContext context) {
        Page<Stock_warn_insufficient_qty> stock_warn_insufficient_qtys=stock_warn_insufficient_qtyFeignClient.searchDefault(context);
        return stock_warn_insufficient_qtys;
    }


}



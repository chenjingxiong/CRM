package cn.ibizlab.odoo.core.odoo_uom.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;


/**
 * 实体[Uom_category] 服务对象接口
 */
public interface IUom_categoryService{

    boolean create(Uom_category et) ;
    void createBatch(List<Uom_category> list) ;
    boolean update(Uom_category et) ;
    void updateBatch(List<Uom_category> list) ;
    Uom_category get(Integer key) ;
    Uom_category getDraft(Uom_category et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Uom_category> searchDefault(Uom_categorySearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-pm-parameter", fallback = mro_pm_parameterFallback.class)
public interface mro_pm_parameterFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/searchdefault")
    Page<Mro_pm_parameter> searchDefault(@RequestBody Mro_pm_parameterSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/{id}")
    Mro_pm_parameter get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters")
    Mro_pm_parameter create(@RequestBody Mro_pm_parameter mro_pm_parameter);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_parameter> mro_pm_parameters);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/{id}")
    Mro_pm_parameter update(@PathVariable("id") Integer id,@RequestBody Mro_pm_parameter mro_pm_parameter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_parameter> mro_pm_parameters);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/select")
    Page<Mro_pm_parameter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/getdraft")
    Mro_pm_parameter getDraft();


}

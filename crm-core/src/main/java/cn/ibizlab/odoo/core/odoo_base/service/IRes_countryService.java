package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;


/**
 * 实体[Res_country] 服务对象接口
 */
public interface IRes_countryService{

    Res_country getDraft(Res_country et) ;
    boolean update(Res_country et) ;
    void updateBatch(List<Res_country> list) ;
    Res_country get(Integer key) ;
    boolean create(Res_country et) ;
    void createBatch(List<Res_country> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Res_country> searchDefault(Res_countrySearchContext context) ;

}




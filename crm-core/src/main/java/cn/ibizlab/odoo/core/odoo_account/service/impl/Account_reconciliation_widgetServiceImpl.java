package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconciliation_widgetService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_reconciliation_widgetFeignClient;

/**
 * 实体[会计核销挂账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconciliation_widgetServiceImpl implements IAccount_reconciliation_widgetService {

    @Autowired
    account_reconciliation_widgetFeignClient account_reconciliation_widgetFeignClient;


    @Override
    public Account_reconciliation_widget getDraft(Account_reconciliation_widget et) {
        et=account_reconciliation_widgetFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_reconciliation_widgetFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_reconciliation_widgetFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_reconciliation_widget et) {
        Account_reconciliation_widget rt = account_reconciliation_widgetFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconciliation_widget> list){
        account_reconciliation_widgetFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_reconciliation_widget et) {
        Account_reconciliation_widget rt = account_reconciliation_widgetFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_reconciliation_widget> list){
        account_reconciliation_widgetFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_reconciliation_widget get(Integer id) {
		Account_reconciliation_widget et=account_reconciliation_widgetFeignClient.get(id);
        if(et==null){
            et=new Account_reconciliation_widget();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context) {
        Page<Account_reconciliation_widget> account_reconciliation_widgets=account_reconciliation_widgetFeignClient.searchDefault(context);
        return account_reconciliation_widgets;
    }


}



package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-mass-mailing-test", fallback = mail_mass_mailing_testFallback.class)
public interface mail_mass_mailing_testFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/{id}")
    Mail_mass_mailing_test update(@PathVariable("id") Integer id,@RequestBody Mail_mass_mailing_test mail_mass_mailing_test);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_test> mail_mass_mailing_tests);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests")
    Mail_mass_mailing_test create(@RequestBody Mail_mass_mailing_test mail_mass_mailing_test);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_test> mail_mass_mailing_tests);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/searchdefault")
    Page<Mail_mass_mailing_test> searchDefault(@RequestBody Mail_mass_mailing_testSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/{id}")
    Mail_mass_mailing_test get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/select")
    Page<Mail_mass_mailing_test> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/getdraft")
    Mail_mass_mailing_test getDraft();


}

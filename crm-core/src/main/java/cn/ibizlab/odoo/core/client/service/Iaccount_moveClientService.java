package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_move;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move] 服务对象接口
 */
public interface Iaccount_moveClientService{

    public Iaccount_move createModel() ;

    public Page<Iaccount_move> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_move> account_moves);

    public void update(Iaccount_move account_move);

    public void get(Iaccount_move account_move);

    public void create(Iaccount_move account_move);

    public void remove(Iaccount_move account_move);

    public void removeBatch(List<Iaccount_move> account_moves);

    public void createBatch(List<Iaccount_move> account_moves);

    public Page<Iaccount_move> select(SearchContext context);

    public void getDraft(Iaccount_move account_move);

}

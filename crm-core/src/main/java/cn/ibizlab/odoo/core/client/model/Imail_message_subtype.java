package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_message_subtype] 对象
 */
public interface Imail_message_subtype {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [默认]
     */
    public void setIbizdefault(String ibizdefault);
    
    /**
     * 设置 [默认]
     */
    public String getIbizdefault();

    /**
     * 获取 [默认]脏标记
     */
    public boolean getIbizdefaultDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [隐藏]
     */
    public void setHidden(String hidden);
    
    /**
     * 设置 [隐藏]
     */
    public String getHidden();

    /**
     * 获取 [隐藏]脏标记
     */
    public boolean getHiddenDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [仅内部的]
     */
    public void setInternal(String internal);
    
    /**
     * 设置 [仅内部的]
     */
    public String getInternal();

    /**
     * 获取 [仅内部的]脏标记
     */
    public boolean getInternalDirtyFlag();
    /**
     * 获取 [消息类型]
     */
    public void setName(String name);
    
    /**
     * 设置 [消息类型]
     */
    public String getName();

    /**
     * 获取 [消息类型]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [关系字段]
     */
    public void setRelation_field(String relation_field);
    
    /**
     * 设置 [关系字段]
     */
    public String getRelation_field();

    /**
     * 获取 [关系字段]脏标记
     */
    public boolean getRelation_fieldDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [模型]
     */
    public String getRes_model();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

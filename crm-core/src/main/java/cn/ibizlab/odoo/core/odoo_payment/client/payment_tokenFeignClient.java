package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[payment_token] 服务对象接口
 */
@FeignClient(value = "odoo-payment", contextId = "payment-token", fallback = payment_tokenFallback.class)
public interface payment_tokenFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/{id}")
    Payment_token get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_tokens")
    Payment_token create(@RequestBody Payment_token payment_token);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/batch")
    Boolean createBatch(@RequestBody List<Payment_token> payment_tokens);




    @RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/searchdefault")
    Page<Payment_token> searchDefault(@RequestBody Payment_tokenSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/{id}")
    Payment_token update(@PathVariable("id") Integer id,@RequestBody Payment_token payment_token);

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/batch")
    Boolean updateBatch(@RequestBody List<Payment_token> payment_tokens);


    @RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/select")
    Page<Payment_token> select();


    @RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/getdraft")
    Payment_token getDraft();


}

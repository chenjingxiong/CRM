package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-partner", fallback = res_partnerFallback.class)
public interface res_partnerFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/{id}")
    Res_partner get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{id}")
    Res_partner update(@PathVariable("id") Integer id,@RequestBody Res_partner res_partner);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partners/batch")
    Boolean updateBatch(@RequestBody List<Res_partner> res_partners);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partners")
    Res_partner create(@RequestBody Res_partner res_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/batch")
    Boolean createBatch(@RequestBody List<Res_partner> res_partners);





    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/searchdefault")
    Page<Res_partner> searchDefault(@RequestBody Res_partnerSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/select")
    Page<Res_partner> select();


    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/checkkey")
    Boolean checkKey(@RequestBody Res_partner res_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/save")
    Boolean save(@RequestBody Res_partner res_partner);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/save")
    Boolean saveBatch(@RequestBody List<Res_partner> res_partners);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/getdraft")
    Res_partner getDraft();



    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/searchcontacts")
    Page<Res_partner> searchContacts(@RequestBody Res_partnerSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partners/searchcompany")
    Page<Res_partner> searchCompany(@RequestBody Res_partnerSearchContext context);


}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment_term] 服务对象接口
 */
public interface Iaccount_payment_termClientService{

    public Iaccount_payment_term createModel() ;

    public void create(Iaccount_payment_term account_payment_term);

    public void createBatch(List<Iaccount_payment_term> account_payment_terms);

    public void updateBatch(List<Iaccount_payment_term> account_payment_terms);

    public Page<Iaccount_payment_term> fetchDefault(SearchContext context);

    public void remove(Iaccount_payment_term account_payment_term);

    public void removeBatch(List<Iaccount_payment_term> account_payment_terms);

    public void update(Iaccount_payment_term account_payment_term);

    public void get(Iaccount_payment_term account_payment_term);

    public Page<Iaccount_payment_term> select(SearchContext context);

    public void getDraft(Iaccount_payment_term account_payment_term);

}

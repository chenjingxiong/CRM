package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_backorder_confirmation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
public interface Istock_backorder_confirmationClientService{

    public Istock_backorder_confirmation createModel() ;

    public void create(Istock_backorder_confirmation stock_backorder_confirmation);

    public Page<Istock_backorder_confirmation> fetchDefault(SearchContext context);

    public void get(Istock_backorder_confirmation stock_backorder_confirmation);

    public void removeBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations);

    public void remove(Istock_backorder_confirmation stock_backorder_confirmation);

    public void updateBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations);

    public void update(Istock_backorder_confirmation stock_backorder_confirmation);

    public void createBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations);

    public Page<Istock_backorder_confirmation> select(SearchContext context);

    public void getDraft(Istock_backorder_confirmation stock_backorder_confirmation);

}

package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_threadService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_threadFeignClient;

/**
 * 实体[EMail线程] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_threadServiceImpl implements IMail_threadService {

    @Autowired
    mail_threadFeignClient mail_threadFeignClient;


    @Override
    public Mail_thread getDraft(Mail_thread et) {
        et=mail_threadFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_threadFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_threadFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_thread et) {
        Mail_thread rt = mail_threadFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_thread> list){
        mail_threadFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_thread get(Integer id) {
		Mail_thread et=mail_threadFeignClient.get(id);
        if(et==null){
            et=new Mail_thread();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_thread et) {
        Mail_thread rt = mail_threadFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_thread> list){
        mail_threadFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_thread> searchDefault(Mail_threadSearchContext context) {
        Page<Mail_thread> mail_threads=mail_threadFeignClient.searchDefault(context);
        return mail_threads;
    }


}



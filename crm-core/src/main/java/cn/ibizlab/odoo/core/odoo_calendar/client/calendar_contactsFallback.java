package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_contacts] 服务对象接口
 */
@Component
public class calendar_contactsFallback implements calendar_contactsFeignClient{

    public Calendar_contacts update(Integer id, Calendar_contacts calendar_contacts){
            return null;
     }
    public Boolean updateBatch(List<Calendar_contacts> calendar_contacts){
            return false;
     }


    public Calendar_contacts create(Calendar_contacts calendar_contacts){
            return null;
     }
    public Boolean createBatch(List<Calendar_contacts> calendar_contacts){
            return false;
     }

    public Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Calendar_contacts get(Integer id){
            return null;
     }





    public Page<Calendar_contacts> select(){
            return null;
     }

    public Calendar_contacts getDraft(){
            return null;
    }



}

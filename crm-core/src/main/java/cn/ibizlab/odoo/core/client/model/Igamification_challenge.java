package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_challenge] 对象
 */
public interface Igamification_challenge {

    /**
     * 获取 [出现在]
     */
    public void setCategory(String category);
    
    /**
     * 设置 [出现在]
     */
    public String getCategory();

    /**
     * 获取 [出现在]脏标记
     */
    public boolean getCategoryDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setEnd_date(Timestamp end_date);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getEnd_date();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getEnd_dateDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [建议用户]
     */
    public void setInvited_user_ids(String invited_user_ids);
    
    /**
     * 设置 [建议用户]
     */
    public String getInvited_user_ids();

    /**
     * 获取 [建议用户]脏标记
     */
    public boolean getInvited_user_idsDirtyFlag();
    /**
     * 获取 [最新报告日期]
     */
    public void setLast_report_date(Timestamp last_report_date);
    
    /**
     * 设置 [最新报告日期]
     */
    public Timestamp getLast_report_date();

    /**
     * 获取 [最新报告日期]脏标记
     */
    public boolean getLast_report_dateDirtyFlag();
    /**
     * 获取 [明细行]
     */
    public void setLine_ids(String line_ids);
    
    /**
     * 设置 [明细行]
     */
    public String getLine_ids();

    /**
     * 获取 [明细行]脏标记
     */
    public boolean getLine_idsDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setManager_id(Integer manager_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getManager_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getManager_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setManager_id_text(String manager_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getManager_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getManager_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [挑战名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [挑战名称]
     */
    public String getName();

    /**
     * 获取 [挑战名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [下次报告日期]
     */
    public void setNext_report_date(Timestamp next_report_date);
    
    /**
     * 设置 [下次报告日期]
     */
    public Timestamp getNext_report_date();

    /**
     * 获取 [下次报告日期]脏标记
     */
    public boolean getNext_report_dateDirtyFlag();
    /**
     * 获取 [周期]
     */
    public void setPeriod(String period);
    
    /**
     * 设置 [周期]
     */
    public String getPeriod();

    /**
     * 获取 [周期]脏标记
     */
    public boolean getPeriodDirtyFlag();
    /**
     * 获取 [未更新的手动目标稍后将被提醒]
     */
    public void setRemind_update_delay(Integer remind_update_delay);
    
    /**
     * 设置 [未更新的手动目标稍后将被提醒]
     */
    public Integer getRemind_update_delay();

    /**
     * 获取 [未更新的手动目标稍后将被提醒]脏标记
     */
    public boolean getRemind_update_delayDirtyFlag();
    /**
     * 获取 [报告的频率]
     */
    public void setReport_message_frequency(String report_message_frequency);
    
    /**
     * 设置 [报告的频率]
     */
    public String getReport_message_frequency();

    /**
     * 获取 [报告的频率]脏标记
     */
    public boolean getReport_message_frequencyDirtyFlag();
    /**
     * 获取 [抄送]
     */
    public void setReport_message_group_id(Integer report_message_group_id);
    
    /**
     * 设置 [抄送]
     */
    public Integer getReport_message_group_id();

    /**
     * 获取 [抄送]脏标记
     */
    public boolean getReport_message_group_idDirtyFlag();
    /**
     * 获取 [抄送]
     */
    public void setReport_message_group_id_text(String report_message_group_id_text);
    
    /**
     * 设置 [抄送]
     */
    public String getReport_message_group_id_text();

    /**
     * 获取 [抄送]脏标记
     */
    public boolean getReport_message_group_id_textDirtyFlag();
    /**
     * 获取 [报告模板]
     */
    public void setReport_template_id(Integer report_template_id);
    
    /**
     * 设置 [报告模板]
     */
    public Integer getReport_template_id();

    /**
     * 获取 [报告模板]脏标记
     */
    public boolean getReport_template_idDirtyFlag();
    /**
     * 获取 [报告模板]
     */
    public void setReport_template_id_text(String report_template_id_text);
    
    /**
     * 设置 [报告模板]
     */
    public String getReport_template_id_text();

    /**
     * 获取 [报告模板]脏标记
     */
    public boolean getReport_template_id_textDirtyFlag();
    /**
     * 获取 [奖励未达成目标的最优者?]
     */
    public void setReward_failure(String reward_failure);
    
    /**
     * 设置 [奖励未达成目标的最优者?]
     */
    public String getReward_failure();

    /**
     * 获取 [奖励未达成目标的最优者?]脏标记
     */
    public boolean getReward_failureDirtyFlag();
    /**
     * 获取 [第一位用户]
     */
    public void setReward_first_id(Integer reward_first_id);
    
    /**
     * 设置 [第一位用户]
     */
    public Integer getReward_first_id();

    /**
     * 获取 [第一位用户]脏标记
     */
    public boolean getReward_first_idDirtyFlag();
    /**
     * 获取 [第一位用户]
     */
    public void setReward_first_id_text(String reward_first_id_text);
    
    /**
     * 设置 [第一位用户]
     */
    public String getReward_first_id_text();

    /**
     * 获取 [第一位用户]脏标记
     */
    public boolean getReward_first_id_textDirtyFlag();
    /**
     * 获取 [每位获得成功的用户]
     */
    public void setReward_id(Integer reward_id);
    
    /**
     * 设置 [每位获得成功的用户]
     */
    public Integer getReward_id();

    /**
     * 获取 [每位获得成功的用户]脏标记
     */
    public boolean getReward_idDirtyFlag();
    /**
     * 获取 [每位获得成功的用户]
     */
    public void setReward_id_text(String reward_id_text);
    
    /**
     * 设置 [每位获得成功的用户]
     */
    public String getReward_id_text();

    /**
     * 获取 [每位获得成功的用户]脏标记
     */
    public boolean getReward_id_textDirtyFlag();
    /**
     * 获取 [每完成一个目标就马上奖励]
     */
    public void setReward_realtime(String reward_realtime);
    
    /**
     * 设置 [每完成一个目标就马上奖励]
     */
    public String getReward_realtime();

    /**
     * 获取 [每完成一个目标就马上奖励]脏标记
     */
    public boolean getReward_realtimeDirtyFlag();
    /**
     * 获取 [第二位用户]
     */
    public void setReward_second_id(Integer reward_second_id);
    
    /**
     * 设置 [第二位用户]
     */
    public Integer getReward_second_id();

    /**
     * 获取 [第二位用户]脏标记
     */
    public boolean getReward_second_idDirtyFlag();
    /**
     * 获取 [第二位用户]
     */
    public void setReward_second_id_text(String reward_second_id_text);
    
    /**
     * 设置 [第二位用户]
     */
    public String getReward_second_id_text();

    /**
     * 获取 [第二位用户]脏标记
     */
    public boolean getReward_second_id_textDirtyFlag();
    /**
     * 获取 [第三位用户]
     */
    public void setReward_third_id(Integer reward_third_id);
    
    /**
     * 设置 [第三位用户]
     */
    public Integer getReward_third_id();

    /**
     * 获取 [第三位用户]脏标记
     */
    public boolean getReward_third_idDirtyFlag();
    /**
     * 获取 [第三位用户]
     */
    public void setReward_third_id_text(String reward_third_id_text);
    
    /**
     * 设置 [第三位用户]
     */
    public String getReward_third_id_text();

    /**
     * 获取 [第三位用户]脏标记
     */
    public boolean getReward_third_id_textDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [用户领域]
     */
    public void setUser_domain(String user_domain);
    
    /**
     * 设置 [用户领域]
     */
    public String getUser_domain();

    /**
     * 获取 [用户领域]脏标记
     */
    public boolean getUser_domainDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [用户]
     */
    public String getUser_ids();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [显示模式]
     */
    public void setVisibility_mode(String visibility_mode);
    
    /**
     * 设置 [显示模式]
     */
    public String getVisibility_mode();

    /**
     * 获取 [显示模式]脏标记
     */
    public boolean getVisibility_modeDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

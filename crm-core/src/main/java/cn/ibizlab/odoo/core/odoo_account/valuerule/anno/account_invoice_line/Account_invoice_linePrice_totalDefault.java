package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_invoice_line;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_invoice_line.Account_invoice_linePrice_totalDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_invoice_line
 * 属性：Price_total
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_invoice_linePrice_totalDefaultValidator.class})
public @interface Account_invoice_linePrice_totalDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

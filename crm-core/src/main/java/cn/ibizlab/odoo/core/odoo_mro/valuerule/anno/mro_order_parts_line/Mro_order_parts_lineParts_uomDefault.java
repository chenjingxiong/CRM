package cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_order_parts_line;

import cn.ibizlab.odoo.core.odoo_mro.valuerule.validator.mro_order_parts_line.Mro_order_parts_lineParts_uomDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mro_order_parts_line
 * 属性：Parts_uom
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mro_order_parts_lineParts_uomDefaultValidator.class})
public @interface Mro_order_parts_lineParts_uomDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

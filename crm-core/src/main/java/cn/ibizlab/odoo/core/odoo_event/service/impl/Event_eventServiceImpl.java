package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_eventService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_eventFeignClient;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_eventServiceImpl implements IEvent_eventService {

    @Autowired
    event_eventFeignClient event_eventFeignClient;


    @Override
    public Event_event get(Integer id) {
		Event_event et=event_eventFeignClient.get(id);
        if(et==null){
            et=new Event_event();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Event_event et) {
        Event_event rt = event_eventFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_event> list){
        event_eventFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Event_event et) {
        Event_event rt = event_eventFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_event> list){
        event_eventFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_event getDraft(Event_event et) {
        et=event_eventFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_eventFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_eventFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_event> searchDefault(Event_eventSearchContext context) {
        Page<Event_event> event_events=event_eventFeignClient.searchDefault(context);
        return event_events;
    }


}



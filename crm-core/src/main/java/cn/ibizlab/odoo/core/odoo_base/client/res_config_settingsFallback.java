package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_config_settings] 服务对象接口
 */
@Component
public class res_config_settingsFallback implements res_config_settingsFeignClient{



    public Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context){
            return null;
     }



    public Res_config_settings update(Integer id, Res_config_settings res_config_settings){
            return null;
     }
    public Boolean updateBatch(List<Res_config_settings> res_config_settings){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Res_config_settings create(Res_config_settings res_config_settings){
            return null;
     }
    public Boolean createBatch(List<Res_config_settings> res_config_settings){
            return false;
     }

    public Res_config_settings get(Integer id){
            return null;
     }


    public Page<Res_config_settings> select(){
            return null;
     }

    public Res_config_settings getDraft(){
            return null;
    }



}

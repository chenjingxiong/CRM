package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_asset] 服务对象接口
 */
public interface Iasset_assetClientService{

    public Iasset_asset createModel() ;

    public void get(Iasset_asset asset_asset);

    public void create(Iasset_asset asset_asset);

    public void update(Iasset_asset asset_asset);

    public void createBatch(List<Iasset_asset> asset_assets);

    public void updateBatch(List<Iasset_asset> asset_assets);

    public void remove(Iasset_asset asset_asset);

    public void removeBatch(List<Iasset_asset> asset_assets);

    public Page<Iasset_asset> fetchDefault(SearchContext context);

    public Page<Iasset_asset> select(SearchContext context);

    public void getDraft(Iasset_asset asset_asset);

}

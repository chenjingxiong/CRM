package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_lead2opportunity_partner_massFeignClient;

/**
 * 实体[转化线索为商机（批量）] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lead2opportunity_partner_massServiceImpl implements ICrm_lead2opportunity_partner_massService {

    @Autowired
    crm_lead2opportunity_partner_massFeignClient crm_lead2opportunity_partner_massFeignClient;


    @Override
    public Crm_lead2opportunity_partner_mass getDraft(Crm_lead2opportunity_partner_mass et) {
        et=crm_lead2opportunity_partner_massFeignClient.getDraft();
        return et;
    }

    @Override
    public Crm_lead2opportunity_partner_mass get(Integer id) {
		Crm_lead2opportunity_partner_mass et=crm_lead2opportunity_partner_massFeignClient.get(id);
        if(et==null){
            et=new Crm_lead2opportunity_partner_mass();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Crm_lead2opportunity_partner_mass et) {
        Crm_lead2opportunity_partner_mass rt = crm_lead2opportunity_partner_massFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead2opportunity_partner_mass> list){
        crm_lead2opportunity_partner_massFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_lead2opportunity_partner_massFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_lead2opportunity_partner_massFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Crm_lead2opportunity_partner_mass et) {
        Crm_lead2opportunity_partner_mass rt = crm_lead2opportunity_partner_massFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_lead2opportunity_partner_mass> list){
        crm_lead2opportunity_partner_massFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead2opportunity_partner_mass> searchDefault(Crm_lead2opportunity_partner_massSearchContext context) {
        Page<Crm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masss=crm_lead2opportunity_partner_massFeignClient.searchDefault(context);
        return crm_lead2opportunity_partner_masss;
    }


}



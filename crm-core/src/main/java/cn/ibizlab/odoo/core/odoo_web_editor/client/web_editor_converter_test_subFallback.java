package cn.ibizlab.odoo.core.odoo_web_editor.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
@Component
public class web_editor_converter_test_subFallback implements web_editor_converter_test_subFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Web_editor_converter_test_sub create(Web_editor_converter_test_sub web_editor_converter_test_sub){
            return null;
     }
    public Boolean createBatch(List<Web_editor_converter_test_sub> web_editor_converter_test_subs){
            return false;
     }

    public Page<Web_editor_converter_test_sub> searchDefault(Web_editor_converter_test_subSearchContext context){
            return null;
     }




    public Web_editor_converter_test_sub update(Integer id, Web_editor_converter_test_sub web_editor_converter_test_sub){
            return null;
     }
    public Boolean updateBatch(List<Web_editor_converter_test_sub> web_editor_converter_test_subs){
            return false;
     }



    public Web_editor_converter_test_sub get(Integer id){
            return null;
     }


    public Page<Web_editor_converter_test_sub> select(){
            return null;
     }

    public Web_editor_converter_test_sub getDraft(){
            return null;
    }



}

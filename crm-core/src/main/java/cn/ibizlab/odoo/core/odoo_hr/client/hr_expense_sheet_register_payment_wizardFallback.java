package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
@Component
public class hr_expense_sheet_register_payment_wizardFallback implements hr_expense_sheet_register_payment_wizardFeignClient{


    public Hr_expense_sheet_register_payment_wizard update(Integer id, Hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
            return null;
     }
    public Boolean updateBatch(List<Hr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
            return false;
     }



    public Hr_expense_sheet_register_payment_wizard create(Hr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
            return null;
     }
    public Boolean createBatch(List<Hr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
            return false;
     }

    public Page<Hr_expense_sheet_register_payment_wizard> searchDefault(Hr_expense_sheet_register_payment_wizardSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Hr_expense_sheet_register_payment_wizard get(Integer id){
            return null;
     }


    public Page<Hr_expense_sheet_register_payment_wizard> select(){
            return null;
     }

    public Hr_expense_sheet_register_payment_wizard getDraft(){
            return null;
    }



}

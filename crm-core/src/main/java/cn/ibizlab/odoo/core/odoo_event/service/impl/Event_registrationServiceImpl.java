package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_registrationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_registrationFeignClient;

/**
 * 实体[事件记录] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_registrationServiceImpl implements IEvent_registrationService {

    @Autowired
    event_registrationFeignClient event_registrationFeignClient;


    @Override
    public boolean create(Event_registration et) {
        Event_registration rt = event_registrationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_registration> list){
        event_registrationFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_registrationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_registrationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Event_registration et) {
        Event_registration rt = event_registrationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_registration> list){
        event_registrationFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_registration get(Integer id) {
		Event_registration et=event_registrationFeignClient.get(id);
        if(et==null){
            et=new Event_registration();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Event_registration getDraft(Event_registration et) {
        et=event_registrationFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_registration> searchDefault(Event_registrationSearchContext context) {
        Page<Event_registration> event_registrations=event_registrationFeignClient.searchDefault(context);
        return event_registrations;
    }


}



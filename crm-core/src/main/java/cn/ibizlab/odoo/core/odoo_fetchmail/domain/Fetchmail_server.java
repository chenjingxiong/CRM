package cn.ibizlab.odoo.core.odoo_fetchmail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Incoming Mail Server] 对象
 */
@Data
public class Fetchmail_server extends EntityClient implements Serializable {

    /**
     * 用户名
     */
    @JSONField(name = "user")
    @JsonProperty("user")
    private String user;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 脚本
     */
    @JSONField(name = "script")
    @JsonProperty("script")
    private String script;

    /**
     * 配置
     */
    @JSONField(name = "configuration")
    @JsonProperty("configuration")
    private String configuration;

    /**
     * 最后收取日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 服务器类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 密码
     */
    @JSONField(name = "password")
    @JsonProperty("password")
    private String password;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 服务器名称
     */
    @JSONField(name = "server")
    @JsonProperty("server")
    private String server;

    /**
     * 端口
     */
    @JSONField(name = "port")
    @JsonProperty("port")
    private Integer port;

    /**
     * SSL/TLS
     */
    @DEField(name = "is_ssl")
    @JSONField(name = "is_ssl")
    @JsonProperty("is_ssl")
    private String isSsl;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 服务器优先级
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建新记录
     */
    @DEField(name = "object_id")
    @JSONField(name = "object_id")
    @JsonProperty("object_id")
    private Integer objectId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 保存附件
     */
    @JSONField(name = "attach")
    @JsonProperty("attach")
    private String attach;

    /**
     * 保留原件
     */
    @JSONField(name = "original")
    @JsonProperty("original")
    private String original;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [用户名]
     */
    public void setUser(String user){
        this.user = user ;
        this.modify("user",user);
    }
    /**
     * 设置 [脚本]
     */
    public void setScript(String script){
        this.script = script ;
        this.modify("script",script);
    }
    /**
     * 设置 [配置]
     */
    public void setConfiguration(String configuration){
        this.configuration = configuration ;
        this.modify("configuration",configuration);
    }
    /**
     * 设置 [最后收取日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [服务器类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [密码]
     */
    public void setPassword(String password){
        this.password = password ;
        this.modify("password",password);
    }
    /**
     * 设置 [服务器名称]
     */
    public void setServer(String server){
        this.server = server ;
        this.modify("server",server);
    }
    /**
     * 设置 [端口]
     */
    public void setPort(Integer port){
        this.port = port ;
        this.modify("port",port);
    }
    /**
     * 设置 [SSL/TLS]
     */
    public void setIsSsl(String isSsl){
        this.isSsl = isSsl ;
        this.modify("is_ssl",isSsl);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [服务器优先级]
     */
    public void setPriority(Integer priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }
    /**
     * 设置 [创建新记录]
     */
    public void setObjectId(Integer objectId){
        this.objectId = objectId ;
        this.modify("object_id",objectId);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [保存附件]
     */
    public void setAttach(String attach){
        this.attach = attach ;
        this.modify("attach",attach);
    }
    /**
     * 设置 [保留原件]
     */
    public void setOriginal(String original){
        this.original = original ;
        this.modify("original",original);
    }

}



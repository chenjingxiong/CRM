package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_attendee] 服务对象接口
 */
@FeignClient(value = "odoo-calendar", contextId = "calendar-attendee", fallback = calendar_attendeeFallback.class)
public interface calendar_attendeeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/{id}")
    Calendar_attendee update(@PathVariable("id") Integer id,@RequestBody Calendar_attendee calendar_attendee);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/batch")
    Boolean updateBatch(@RequestBody List<Calendar_attendee> calendar_attendees);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees")
    Calendar_attendee create(@RequestBody Calendar_attendee calendar_attendee);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/batch")
    Boolean createBatch(@RequestBody List<Calendar_attendee> calendar_attendees);



    @RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/{id}")
    Calendar_attendee get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/searchdefault")
    Page<Calendar_attendee> searchDefault(@RequestBody Calendar_attendeeSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/select")
    Page<Calendar_attendee> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/getdraft")
    Calendar_attendee getDraft();


}

package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workorder;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_workorder.Mrp_workorderProduct_trackingDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_workorder
 * 属性：Product_tracking
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_workorderProduct_trackingDefaultValidator.class})
public @interface Mrp_workorderProduct_trackingDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

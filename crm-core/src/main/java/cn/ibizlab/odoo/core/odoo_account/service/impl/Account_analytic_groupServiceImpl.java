package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_groupFeignClient;

/**
 * 实体[分析类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_groupServiceImpl implements IAccount_analytic_groupService {

    @Autowired
    account_analytic_groupFeignClient account_analytic_groupFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_analytic_groupFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_analytic_groupFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_analytic_group et) {
        Account_analytic_group rt = account_analytic_groupFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_group> list){
        account_analytic_groupFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_analytic_group et) {
        Account_analytic_group rt = account_analytic_groupFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_analytic_group> list){
        account_analytic_groupFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_analytic_group getDraft(Account_analytic_group et) {
        et=account_analytic_groupFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_analytic_group get(Integer id) {
		Account_analytic_group et=account_analytic_groupFeignClient.get(id);
        if(et==null){
            et=new Account_analytic_group();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_group> searchDefault(Account_analytic_groupSearchContext context) {
        Page<Account_analytic_group> account_analytic_groups=account_analytic_groupFeignClient.searchDefault(context);
        return account_analytic_groups;
    }


}



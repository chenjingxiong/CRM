package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;


/**
 * 实体[Hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
public interface IHr_expense_sheet_register_payment_wizardService{

    boolean create(Hr_expense_sheet_register_payment_wizard et) ;
    void createBatch(List<Hr_expense_sheet_register_payment_wizard> list) ;
    Hr_expense_sheet_register_payment_wizard get(Integer key) ;
    boolean update(Hr_expense_sheet_register_payment_wizard et) ;
    void updateBatch(List<Hr_expense_sheet_register_payment_wizard> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_expense_sheet_register_payment_wizard getDraft(Hr_expense_sheet_register_payment_wizard et) ;
    Page<Hr_expense_sheet_register_payment_wizard> searchDefault(Hr_expense_sheet_register_payment_wizardSearchContext context) ;

}




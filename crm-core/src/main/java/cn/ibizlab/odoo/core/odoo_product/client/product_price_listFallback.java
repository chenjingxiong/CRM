package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_price_list] 服务对象接口
 */
@Component
public class product_price_listFallback implements product_price_listFeignClient{


    public Product_price_list create(Product_price_list product_price_list){
            return null;
     }
    public Boolean createBatch(List<Product_price_list> product_price_lists){
            return false;
     }


    public Product_price_list get(Integer id){
            return null;
     }


    public Product_price_list update(Integer id, Product_price_list product_price_list){
            return null;
     }
    public Boolean updateBatch(List<Product_price_list> product_price_lists){
            return false;
     }


    public Page<Product_price_list> searchDefault(Product_price_listSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Product_price_list> select(){
            return null;
     }

    public Product_price_list getDraft(){
            return null;
    }



}

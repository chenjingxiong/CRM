package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;


/**
 * 实体[Stock_quant_package] 服务对象接口
 */
public interface IStock_quant_packageService{

    boolean update(Stock_quant_package et) ;
    void updateBatch(List<Stock_quant_package> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_quant_package et) ;
    void createBatch(List<Stock_quant_package> list) ;
    Stock_quant_package getDraft(Stock_quant_package et) ;
    Stock_quant_package get(Integer key) ;
    Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context) ;

}




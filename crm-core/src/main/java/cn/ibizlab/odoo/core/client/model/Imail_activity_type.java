package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_activity_type] 对象
 */
public interface Imail_activity_type {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setCategory(String category);
    
    /**
     * 设置 [类别]
     */
    public String getCategory();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getCategoryDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [排版类型]
     */
    public void setDecoration_type(String decoration_type);
    
    /**
     * 设置 [排版类型]
     */
    public String getDecoration_type();

    /**
     * 获取 [排版类型]脏标记
     */
    public boolean getDecoration_typeDirtyFlag();
    /**
     * 获取 [设置默认下一个活动]
     */
    public void setDefault_next_type_id(Integer default_next_type_id);
    
    /**
     * 设置 [设置默认下一个活动]
     */
    public Integer getDefault_next_type_id();

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    public boolean getDefault_next_type_idDirtyFlag();
    /**
     * 获取 [设置默认下一个活动]
     */
    public void setDefault_next_type_id_text(String default_next_type_id_text);
    
    /**
     * 设置 [设置默认下一个活动]
     */
    public String getDefault_next_type_id_text();

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    public boolean getDefault_next_type_id_textDirtyFlag();
    /**
     * 获取 [之后]
     */
    public void setDelay_count(Integer delay_count);
    
    /**
     * 设置 [之后]
     */
    public Integer getDelay_count();

    /**
     * 获取 [之后]脏标记
     */
    public boolean getDelay_countDirtyFlag();
    /**
     * 获取 [延迟类型]
     */
    public void setDelay_from(String delay_from);
    
    /**
     * 设置 [延迟类型]
     */
    public String getDelay_from();

    /**
     * 获取 [延迟类型]脏标记
     */
    public boolean getDelay_fromDirtyFlag();
    /**
     * 获取 [延迟单位]
     */
    public void setDelay_unit(String delay_unit);
    
    /**
     * 设置 [延迟单位]
     */
    public String getDelay_unit();

    /**
     * 获取 [延迟单位]脏标记
     */
    public boolean getDelay_unitDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [自动安排下一个活动]
     */
    public void setForce_next(String force_next);
    
    /**
     * 设置 [自动安排下一个活动]
     */
    public String getForce_next();

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    public boolean getForce_nextDirtyFlag();
    /**
     * 获取 [图标]
     */
    public void setIcon(String icon);
    
    /**
     * 设置 [图标]
     */
    public String getIcon();

    /**
     * 获取 [图标]脏标记
     */
    public boolean getIconDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [初始模型]
     */
    public void setInitial_res_model_id(Integer initial_res_model_id);
    
    /**
     * 设置 [初始模型]
     */
    public Integer getInitial_res_model_id();

    /**
     * 获取 [初始模型]脏标记
     */
    public boolean getInitial_res_model_idDirtyFlag();
    /**
     * 获取 [邮件模板]
     */
    public void setMail_template_ids(String mail_template_ids);
    
    /**
     * 设置 [邮件模板]
     */
    public String getMail_template_ids();

    /**
     * 获取 [邮件模板]脏标记
     */
    public boolean getMail_template_idsDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [推荐的下一活动]
     */
    public void setNext_type_ids(String next_type_ids);
    
    /**
     * 设置 [推荐的下一活动]
     */
    public String getNext_type_ids();

    /**
     * 获取 [推荐的下一活动]脏标记
     */
    public boolean getNext_type_idsDirtyFlag();
    /**
     * 获取 [预先活动]
     */
    public void setPrevious_type_ids(String previous_type_ids);
    
    /**
     * 设置 [预先活动]
     */
    public String getPrevious_type_ids();

    /**
     * 获取 [预先活动]脏标记
     */
    public boolean getPrevious_type_idsDirtyFlag();
    /**
     * 获取 [模型已更改]
     */
    public void setRes_model_change(String res_model_change);
    
    /**
     * 设置 [模型已更改]
     */
    public String getRes_model_change();

    /**
     * 获取 [模型已更改]脏标记
     */
    public boolean getRes_model_changeDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setRes_model_id(Integer res_model_id);
    
    /**
     * 设置 [模型]
     */
    public Integer getRes_model_id();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getRes_model_idDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [摘要]
     */
    public void setSummary(String summary);
    
    /**
     * 设置 [摘要]
     */
    public String getSummary();

    /**
     * 获取 [摘要]脏标记
     */
    public boolean getSummaryDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_calendar] 服务对象接口
 */
@Component
public class resource_calendarFallback implements resource_calendarFeignClient{

    public Resource_calendar update(Integer id, Resource_calendar resource_calendar){
            return null;
     }
    public Boolean updateBatch(List<Resource_calendar> resource_calendars){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context){
            return null;
     }


    public Resource_calendar get(Integer id){
            return null;
     }




    public Resource_calendar create(Resource_calendar resource_calendar){
            return null;
     }
    public Boolean createBatch(List<Resource_calendar> resource_calendars){
            return false;
     }

    public Page<Resource_calendar> select(){
            return null;
     }

    public Resource_calendar getDraft(){
            return null;
    }



}

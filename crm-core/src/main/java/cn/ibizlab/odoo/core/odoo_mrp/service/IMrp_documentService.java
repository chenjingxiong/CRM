package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;


/**
 * 实体[Mrp_document] 服务对象接口
 */
public interface IMrp_documentService{

    Mrp_document getDraft(Mrp_document et) ;
    boolean update(Mrp_document et) ;
    void updateBatch(List<Mrp_document> list) ;
    boolean create(Mrp_document et) ;
    void createBatch(List<Mrp_document> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_document get(Integer key) ;
    Page<Mrp_document> searchDefault(Mrp_documentSearchContext context) ;

}




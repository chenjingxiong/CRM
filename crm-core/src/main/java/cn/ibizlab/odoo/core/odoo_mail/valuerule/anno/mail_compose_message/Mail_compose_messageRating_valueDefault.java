package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_compose_message;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_compose_message.Mail_compose_messageRating_valueDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_compose_message
 * 属性：Rating_value
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_compose_messageRating_valueDefaultValidator.class})
public @interface Mail_compose_messageRating_valueDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

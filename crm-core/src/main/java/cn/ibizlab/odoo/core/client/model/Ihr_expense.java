package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_expense] 对象
 */
public interface Ihr_expense {

    /**
     * 获取 [科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id(Integer analytic_account_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAnalytic_account_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id_text(String analytic_account_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAnalytic_account_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_id_textDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setAttachment_number(Integer attachment_number);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getAttachment_number();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getAttachment_numberDirtyFlag();
    /**
     * 获取 [公司货币报告]
     */
    public void setCompany_currency_id(Integer company_currency_id);
    
    /**
     * 设置 [公司货币报告]
     */
    public Integer getCompany_currency_id();

    /**
     * 获取 [公司货币报告]脏标记
     */
    public boolean getCompany_currency_idDirtyFlag();
    /**
     * 获取 [公司货币报告]
     */
    public void setCompany_currency_id_text(String company_currency_id_text);
    
    /**
     * 设置 [公司货币报告]
     */
    public String getCompany_currency_id_text();

    /**
     * 获取 [公司货币报告]脏标记
     */
    public boolean getCompany_currency_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [备注...]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [备注...]
     */
    public String getDescription();

    /**
     * 获取 [备注...]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [由经理或会计人员明确地拒绝]
     */
    public void setIs_refused(String is_refused);
    
    /**
     * 设置 [由经理或会计人员明确地拒绝]
     */
    public String getIs_refused();

    /**
     * 获取 [由经理或会计人员明确地拒绝]脏标记
     */
    public boolean getIs_refusedDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [支付]
     */
    public void setPayment_mode(String payment_mode);
    
    /**
     * 设置 [支付]
     */
    public String getPayment_mode();

    /**
     * 获取 [支付]脏标记
     */
    public boolean getPayment_modeDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setQuantity(Double quantity);
    
    /**
     * 设置 [数量]
     */
    public Double getQuantity();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getQuantityDirtyFlag();
    /**
     * 获取 [账单参照]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [账单参照]
     */
    public String getReference();

    /**
     * 获取 [账单参照]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_id(Integer sale_order_id);
    
    /**
     * 设置 [销售订单]
     */
    public Integer getSale_order_id();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_idDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_order_id_text(String sale_order_id_text);
    
    /**
     * 设置 [销售订单]
     */
    public String getSale_order_id_text();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_order_id_textDirtyFlag();
    /**
     * 获取 [费用报表]
     */
    public void setSheet_id(Integer sheet_id);
    
    /**
     * 设置 [费用报表]
     */
    public Integer getSheet_id();

    /**
     * 获取 [费用报表]脏标记
     */
    public boolean getSheet_idDirtyFlag();
    /**
     * 获取 [费用报表]
     */
    public void setSheet_id_text(String sheet_id_text);
    
    /**
     * 设置 [费用报表]
     */
    public String getSheet_id_text();

    /**
     * 获取 [费用报表]脏标记
     */
    public boolean getSheet_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [税率设置]
     */
    public void setTax_ids(String tax_ids);
    
    /**
     * 设置 [税率设置]
     */
    public String getTax_ids();

    /**
     * 获取 [税率设置]脏标记
     */
    public boolean getTax_idsDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setTotal_amount(Double total_amount);
    
    /**
     * 设置 [总计]
     */
    public Double getTotal_amount();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getTotal_amountDirtyFlag();
    /**
     * 获取 [合计 (公司货币)]
     */
    public void setTotal_amount_company(Double total_amount_company);
    
    /**
     * 设置 [合计 (公司货币)]
     */
    public Double getTotal_amount_company();

    /**
     * 获取 [合计 (公司货币)]脏标记
     */
    public boolean getTotal_amount_companyDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setUnit_amount(Double unit_amount);
    
    /**
     * 设置 [单价]
     */
    public Double getUnit_amount();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getUnit_amountDirtyFlag();
    /**
     * 获取 [小计]
     */
    public void setUntaxed_amount(Double untaxed_amount);
    
    /**
     * 设置 [小计]
     */
    public Double getUntaxed_amount();

    /**
     * 获取 [小计]脏标记
     */
    public boolean getUntaxed_amountDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

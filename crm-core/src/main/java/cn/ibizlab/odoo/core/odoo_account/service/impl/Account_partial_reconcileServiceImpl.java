package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_partial_reconcileService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_partial_reconcileFeignClient;

/**
 * 实体[部分核销] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_partial_reconcileServiceImpl implements IAccount_partial_reconcileService {

    @Autowired
    account_partial_reconcileFeignClient account_partial_reconcileFeignClient;


    @Override
    public boolean create(Account_partial_reconcile et) {
        Account_partial_reconcile rt = account_partial_reconcileFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_partial_reconcile> list){
        account_partial_reconcileFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_partial_reconcileFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_partial_reconcileFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_partial_reconcile et) {
        Account_partial_reconcile rt = account_partial_reconcileFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_partial_reconcile> list){
        account_partial_reconcileFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_partial_reconcile get(Integer id) {
		Account_partial_reconcile et=account_partial_reconcileFeignClient.get(id);
        if(et==null){
            et=new Account_partial_reconcile();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_partial_reconcile getDraft(Account_partial_reconcile et) {
        et=account_partial_reconcileFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context) {
        Page<Account_partial_reconcile> account_partial_reconciles=account_partial_reconcileFeignClient.searchDefault(context);
        return account_partial_reconciles;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_currency_rate;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_currency_rate] 服务对象接口
 */
public interface Ires_currency_rateClientService{

    public Ires_currency_rate createModel() ;

    public void get(Ires_currency_rate res_currency_rate);

    public void createBatch(List<Ires_currency_rate> res_currency_rates);

    public void create(Ires_currency_rate res_currency_rate);

    public void update(Ires_currency_rate res_currency_rate);

    public void removeBatch(List<Ires_currency_rate> res_currency_rates);

    public void remove(Ires_currency_rate res_currency_rate);

    public void updateBatch(List<Ires_currency_rate> res_currency_rates);

    public Page<Ires_currency_rate> fetchDefault(SearchContext context);

    public Page<Ires_currency_rate> select(SearchContext context);

    public void getDraft(Ires_currency_rate res_currency_rate);

}

package cn.ibizlab.odoo.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[utm_source] 服务对象接口
 */
@FeignClient(value = "odoo-utm", contextId = "utm-source", fallback = utm_sourceFallback.class)
public interface utm_sourceFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/utm_sources")
    Utm_source create(@RequestBody Utm_source utm_source);

    @RequestMapping(method = RequestMethod.POST, value = "/utm_sources/batch")
    Boolean createBatch(@RequestBody List<Utm_source> utm_sources);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_sources/{id}")
    Utm_source get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/utm_sources/searchdefault")
    Page<Utm_source> searchDefault(@RequestBody Utm_sourceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/{id}")
    Utm_source update(@PathVariable("id") Integer id,@RequestBody Utm_source utm_source);

    @RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/batch")
    Boolean updateBatch(@RequestBody List<Utm_source> utm_sources);


    @RequestMapping(method = RequestMethod.GET, value = "/utm_sources/select")
    Page<Utm_source> select();


    @RequestMapping(method = RequestMethod.GET, value = "/utm_sources/getdraft")
    Utm_source getDraft();


}

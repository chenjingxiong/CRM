package cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_task;

import cn.ibizlab.odoo.core.odoo_project.valuerule.validator.project_task.Project_taskMessage_has_error_counterDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Project_task
 * 属性：Message_has_error_counter
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Project_taskMessage_has_error_counterDefaultValidator.class})
public @interface Project_taskMessage_has_error_counterDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Product_pricelist] 查询条件对象
 */
@Slf4j
@Data
public class Product_pricelistSearchContext extends SearchContextBase {
	private String n_name_like;//[价格表名称]

	private String n_discount_policy_eq;//[折扣政策]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_company_id_eq;//[公司]

}




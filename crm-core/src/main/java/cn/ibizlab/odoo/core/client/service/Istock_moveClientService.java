package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_move;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_move] 服务对象接口
 */
public interface Istock_moveClientService{

    public Istock_move createModel() ;

    public void createBatch(List<Istock_move> stock_moves);

    public void updateBatch(List<Istock_move> stock_moves);

    public void get(Istock_move stock_move);

    public void create(Istock_move stock_move);

    public void update(Istock_move stock_move);

    public void removeBatch(List<Istock_move> stock_moves);

    public Page<Istock_move> fetchDefault(SearchContext context);

    public void remove(Istock_move stock_move);

    public Page<Istock_move> select(SearchContext context);

    public void getDraft(Istock_move stock_move);

}

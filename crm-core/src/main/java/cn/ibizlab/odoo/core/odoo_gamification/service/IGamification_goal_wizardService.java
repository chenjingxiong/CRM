package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;


/**
 * 实体[Gamification_goal_wizard] 服务对象接口
 */
public interface IGamification_goal_wizardService{

    Gamification_goal_wizard getDraft(Gamification_goal_wizard et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Gamification_goal_wizard et) ;
    void updateBatch(List<Gamification_goal_wizard> list) ;
    boolean create(Gamification_goal_wizard et) ;
    void createBatch(List<Gamification_goal_wizard> list) ;
    Gamification_goal_wizard get(Integer key) ;
    Page<Gamification_goal_wizard> searchDefault(Gamification_goal_wizardSearchContext context) ;

}




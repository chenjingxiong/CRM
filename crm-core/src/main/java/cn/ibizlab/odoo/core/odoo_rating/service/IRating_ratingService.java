package cn.ibizlab.odoo.core.odoo_rating.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;


/**
 * 实体[Rating_rating] 服务对象接口
 */
public interface IRating_ratingService{

    boolean update(Rating_rating et) ;
    void updateBatch(List<Rating_rating> list) ;
    boolean create(Rating_rating et) ;
    void createBatch(List<Rating_rating> list) ;
    Rating_rating get(Integer key) ;
    Rating_rating getDraft(Rating_rating et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Rating_rating> searchDefault(Rating_ratingSearchContext context) ;

}




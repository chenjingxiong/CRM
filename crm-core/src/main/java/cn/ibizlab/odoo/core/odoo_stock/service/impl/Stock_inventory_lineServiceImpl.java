package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_inventory_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_inventory_lineFeignClient;

/**
 * 实体[库存明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_inventory_lineServiceImpl implements IStock_inventory_lineService {

    @Autowired
    stock_inventory_lineFeignClient stock_inventory_lineFeignClient;


    @Override
    public boolean create(Stock_inventory_line et) {
        Stock_inventory_line rt = stock_inventory_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_inventory_line> list){
        stock_inventory_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_inventory_line get(Integer id) {
		Stock_inventory_line et=stock_inventory_lineFeignClient.get(id);
        if(et==null){
            et=new Stock_inventory_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_inventory_line et) {
        Stock_inventory_line rt = stock_inventory_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_inventory_line> list){
        stock_inventory_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_inventory_line getDraft(Stock_inventory_line et) {
        et=stock_inventory_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_inventory_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_inventory_lineFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_inventory_line> searchDefault(Stock_inventory_lineSearchContext context) {
        Page<Stock_inventory_line> stock_inventory_lines=stock_inventory_lineFeignClient.searchDefault(context);
        return stock_inventory_lines;
    }


}



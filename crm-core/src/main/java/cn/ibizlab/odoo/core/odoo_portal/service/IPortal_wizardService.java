package cn.ibizlab.odoo.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;


/**
 * 实体[Portal_wizard] 服务对象接口
 */
public interface IPortal_wizardService{

    boolean update(Portal_wizard et) ;
    void updateBatch(List<Portal_wizard> list) ;
    Portal_wizard getDraft(Portal_wizard et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Portal_wizard et) ;
    void createBatch(List<Portal_wizard> list) ;
    Portal_wizard get(Integer key) ;
    Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context) ;

}




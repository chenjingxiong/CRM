package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_applicant_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_applicant_category] 服务对象接口
 */
public interface Ihr_applicant_categoryClientService{

    public Ihr_applicant_category createModel() ;

    public void updateBatch(List<Ihr_applicant_category> hr_applicant_categories);

    public void get(Ihr_applicant_category hr_applicant_category);

    public void create(Ihr_applicant_category hr_applicant_category);

    public void removeBatch(List<Ihr_applicant_category> hr_applicant_categories);

    public void remove(Ihr_applicant_category hr_applicant_category);

    public Page<Ihr_applicant_category> fetchDefault(SearchContext context);

    public void update(Ihr_applicant_category hr_applicant_category);

    public void createBatch(List<Ihr_applicant_category> hr_applicant_categories);

    public Page<Ihr_applicant_category> select(SearchContext context);

    public void getDraft(Ihr_applicant_category hr_applicant_category);

}

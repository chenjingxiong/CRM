package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_replan;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_replan] 服务对象接口
 */
public interface Imro_pm_replanClientService{

    public Imro_pm_replan createModel() ;

    public void remove(Imro_pm_replan mro_pm_replan);

    public void update(Imro_pm_replan mro_pm_replan);

    public void get(Imro_pm_replan mro_pm_replan);

    public void updateBatch(List<Imro_pm_replan> mro_pm_replans);

    public Page<Imro_pm_replan> fetchDefault(SearchContext context);

    public void createBatch(List<Imro_pm_replan> mro_pm_replans);

    public void removeBatch(List<Imro_pm_replan> mro_pm_replans);

    public void create(Imro_pm_replan mro_pm_replan);

    public Page<Imro_pm_replan> select(SearchContext context);

    public void getDraft(Imro_pm_replan mro_pm_replan);

}

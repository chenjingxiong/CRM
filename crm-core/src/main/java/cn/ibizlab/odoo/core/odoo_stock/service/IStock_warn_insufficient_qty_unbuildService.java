package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;


/**
 * 实体[Stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
public interface IStock_warn_insufficient_qty_unbuildService{

    boolean update(Stock_warn_insufficient_qty_unbuild et) ;
    void updateBatch(List<Stock_warn_insufficient_qty_unbuild> list) ;
    boolean create(Stock_warn_insufficient_qty_unbuild et) ;
    void createBatch(List<Stock_warn_insufficient_qty_unbuild> list) ;
    Stock_warn_insufficient_qty_unbuild get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_warn_insufficient_qty_unbuild getDraft(Stock_warn_insufficient_qty_unbuild et) ;
    Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) ;

}




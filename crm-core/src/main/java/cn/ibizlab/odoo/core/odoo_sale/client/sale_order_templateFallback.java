package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_order_template] 服务对象接口
 */
@Component
public class sale_order_templateFallback implements sale_order_templateFeignClient{

    public Sale_order_template create(Sale_order_template sale_order_template){
            return null;
     }
    public Boolean createBatch(List<Sale_order_template> sale_order_templates){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Sale_order_template update(Integer id, Sale_order_template sale_order_template){
            return null;
     }
    public Boolean updateBatch(List<Sale_order_template> sale_order_templates){
            return false;
     }


    public Sale_order_template get(Integer id){
            return null;
     }



    public Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context){
            return null;
     }



    public Page<Sale_order_template> select(){
            return null;
     }

    public Sale_order_template getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[repair_order] 服务对象接口
 */
@FeignClient(value = "odoo-repair", contextId = "repair-order", fallback = repair_orderFallback.class)
public interface repair_orderFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/{id}")
    Repair_order update(@PathVariable("id") Integer id,@RequestBody Repair_order repair_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/batch")
    Boolean updateBatch(@RequestBody List<Repair_order> repair_orders);




    @RequestMapping(method = RequestMethod.POST, value = "/repair_orders")
    Repair_order create(@RequestBody Repair_order repair_order);

    @RequestMapping(method = RequestMethod.POST, value = "/repair_orders/batch")
    Boolean createBatch(@RequestBody List<Repair_order> repair_orders);



    @RequestMapping(method = RequestMethod.POST, value = "/repair_orders/searchdefault")
    Page<Repair_order> searchDefault(@RequestBody Repair_orderSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_orders/{id}")
    Repair_order get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_orders/select")
    Page<Repair_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/repair_orders/getdraft")
    Repair_order getDraft();


}

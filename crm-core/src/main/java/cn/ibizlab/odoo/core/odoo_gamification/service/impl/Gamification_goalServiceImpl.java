package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goalFeignClient;

/**
 * 实体[游戏化目标] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goalServiceImpl implements IGamification_goalService {

    @Autowired
    gamification_goalFeignClient gamification_goalFeignClient;


    @Override
    public boolean create(Gamification_goal et) {
        Gamification_goal rt = gamification_goalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal> list){
        gamification_goalFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_goalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_goalFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Gamification_goal et) {
        Gamification_goal rt = gamification_goalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_goal> list){
        gamification_goalFeignClient.updateBatch(list) ;
    }

    @Override
    public Gamification_goal get(Integer id) {
		Gamification_goal et=gamification_goalFeignClient.get(id);
        if(et==null){
            et=new Gamification_goal();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Gamification_goal getDraft(Gamification_goal et) {
        et=gamification_goalFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context) {
        Page<Gamification_goal> gamification_goals=gamification_goalFeignClient.searchDefault(context);
        return gamification_goals;
    }


}



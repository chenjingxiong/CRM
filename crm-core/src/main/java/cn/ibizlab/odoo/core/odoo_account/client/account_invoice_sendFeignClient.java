package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_send] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-invoice-send", fallback = account_invoice_sendFallback.class)
public interface account_invoice_sendFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/searchdefault")
    Page<Account_invoice_send> searchDefault(@RequestBody Account_invoice_sendSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends")
    Account_invoice_send create(@RequestBody Account_invoice_send account_invoice_send);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_send> account_invoice_sends);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/{id}")
    Account_invoice_send get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/{id}")
    Account_invoice_send update(@PathVariable("id") Integer id,@RequestBody Account_invoice_send account_invoice_send);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_send> account_invoice_sends);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/select")
    Page<Account_invoice_send> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/getdraft")
    Account_invoice_send getDraft();


}

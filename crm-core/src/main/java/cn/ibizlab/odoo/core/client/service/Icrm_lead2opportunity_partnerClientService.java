package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead2opportunity_partner] 服务对象接口
 */
public interface Icrm_lead2opportunity_partnerClientService{

    public Icrm_lead2opportunity_partner createModel() ;

    public void createBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners);

    public void update(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

    public void removeBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners);

    public void get(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

    public void create(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

    public void updateBatch(List<Icrm_lead2opportunity_partner> crm_lead2opportunity_partners);

    public void remove(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

    public Page<Icrm_lead2opportunity_partner> fetchDefault(SearchContext context);

    public Page<Icrm_lead2opportunity_partner> select(SearchContext context);

    public void getDraft(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

}

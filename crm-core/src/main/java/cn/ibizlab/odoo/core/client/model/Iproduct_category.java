package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_category] 对象
 */
public interface Iproduct_category {

    /**
     * 获取 [下级类别]
     */
    public void setChild_id(String child_id);
    
    /**
     * 设置 [下级类别]
     */
    public String getChild_id();

    /**
     * 获取 [下级类别]脏标记
     */
    public boolean getChild_idDirtyFlag();
    /**
     * 获取 [完整名称]
     */
    public void setComplete_name(String complete_name);
    
    /**
     * 设置 [完整名称]
     */
    public String getComplete_name();

    /**
     * 获取 [完整名称]脏标记
     */
    public boolean getComplete_nameDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [上级类别]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级类别]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级类别]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级类别]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级类别]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级类别]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [父级路径]
     */
    public void setParent_path(String parent_path);
    
    /**
     * 设置 [父级路径]
     */
    public String getParent_path();

    /**
     * 获取 [父级路径]脏标记
     */
    public boolean getParent_pathDirtyFlag();
    /**
     * 获取 [# 产品]
     */
    public void setProduct_count(Integer product_count);
    
    /**
     * 设置 [# 产品]
     */
    public Integer getProduct_count();

    /**
     * 获取 [# 产品]脏标记
     */
    public boolean getProduct_countDirtyFlag();
    /**
     * 获取 [价格差异科目]
     */
    public void setProperty_account_creditor_price_difference_categ(Integer property_account_creditor_price_difference_categ);
    
    /**
     * 设置 [价格差异科目]
     */
    public Integer getProperty_account_creditor_price_difference_categ();

    /**
     * 获取 [价格差异科目]脏标记
     */
    public boolean getProperty_account_creditor_price_difference_categDirtyFlag();
    /**
     * 获取 [费用科目]
     */
    public void setProperty_account_expense_categ_id(Integer property_account_expense_categ_id);
    
    /**
     * 设置 [费用科目]
     */
    public Integer getProperty_account_expense_categ_id();

    /**
     * 获取 [费用科目]脏标记
     */
    public boolean getProperty_account_expense_categ_idDirtyFlag();
    /**
     * 获取 [收入科目]
     */
    public void setProperty_account_income_categ_id(Integer property_account_income_categ_id);
    
    /**
     * 设置 [收入科目]
     */
    public Integer getProperty_account_income_categ_id();

    /**
     * 获取 [收入科目]脏标记
     */
    public boolean getProperty_account_income_categ_idDirtyFlag();
    /**
     * 获取 [成本方法]
     */
    public void setProperty_cost_method(String property_cost_method);
    
    /**
     * 设置 [成本方法]
     */
    public String getProperty_cost_method();

    /**
     * 获取 [成本方法]脏标记
     */
    public boolean getProperty_cost_methodDirtyFlag();
    /**
     * 获取 [库存进货科目]
     */
    public void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);
    
    /**
     * 设置 [库存进货科目]
     */
    public Integer getProperty_stock_account_input_categ_id();

    /**
     * 获取 [库存进货科目]脏标记
     */
    public boolean getProperty_stock_account_input_categ_idDirtyFlag();
    /**
     * 获取 [库存出货科目]
     */
    public void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);
    
    /**
     * 设置 [库存出货科目]
     */
    public Integer getProperty_stock_account_output_categ_id();

    /**
     * 获取 [库存出货科目]脏标记
     */
    public boolean getProperty_stock_account_output_categ_idDirtyFlag();
    /**
     * 获取 [库存日记账]
     */
    public void setProperty_stock_journal(Integer property_stock_journal);
    
    /**
     * 设置 [库存日记账]
     */
    public Integer getProperty_stock_journal();

    /**
     * 获取 [库存日记账]脏标记
     */
    public boolean getProperty_stock_journalDirtyFlag();
    /**
     * 获取 [库存计价科目]
     */
    public void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);
    
    /**
     * 设置 [库存计价科目]
     */
    public Integer getProperty_stock_valuation_account_id();

    /**
     * 获取 [库存计价科目]脏标记
     */
    public boolean getProperty_stock_valuation_account_idDirtyFlag();
    /**
     * 获取 [库存计价]
     */
    public void setProperty_valuation(String property_valuation);
    
    /**
     * 设置 [库存计价]
     */
    public String getProperty_valuation();

    /**
     * 获取 [库存计价]脏标记
     */
    public boolean getProperty_valuationDirtyFlag();
    /**
     * 获取 [强制下架策略]
     */
    public void setRemoval_strategy_id(Integer removal_strategy_id);
    
    /**
     * 设置 [强制下架策略]
     */
    public Integer getRemoval_strategy_id();

    /**
     * 获取 [强制下架策略]脏标记
     */
    public boolean getRemoval_strategy_idDirtyFlag();
    /**
     * 获取 [强制下架策略]
     */
    public void setRemoval_strategy_id_text(String removal_strategy_id_text);
    
    /**
     * 设置 [强制下架策略]
     */
    public String getRemoval_strategy_id_text();

    /**
     * 获取 [强制下架策略]脏标记
     */
    public boolean getRemoval_strategy_id_textDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_ids(String route_ids);
    
    /**
     * 设置 [路线]
     */
    public String getRoute_ids();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_idsDirtyFlag();
    /**
     * 获取 [路线合计]
     */
    public void setTotal_route_ids(String total_route_ids);
    
    /**
     * 设置 [路线合计]
     */
    public String getTotal_route_ids();

    /**
     * 获取 [路线合计]脏标记
     */
    public boolean getTotal_route_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

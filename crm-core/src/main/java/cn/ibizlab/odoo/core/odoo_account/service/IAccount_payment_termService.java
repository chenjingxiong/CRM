package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;


/**
 * 实体[Account_payment_term] 服务对象接口
 */
public interface IAccount_payment_termService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_payment_term et) ;
    void createBatch(List<Account_payment_term> list) ;
    Account_payment_term get(Integer key) ;
    Account_payment_term getDraft(Account_payment_term et) ;
    boolean update(Account_payment_term et) ;
    void updateBatch(List<Account_payment_term> list) ;
    Page<Account_payment_term> searchDefault(Account_payment_termSearchContext context) ;

}




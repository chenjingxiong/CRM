package cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_order;

import cn.ibizlab.odoo.core.odoo_purchase.valuerule.validator.purchase_order.Purchase_orderCompany_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Purchase_order
 * 属性：Company_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Purchase_orderCompany_idDefaultValidator.class})
public @interface Purchase_orderCompany_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

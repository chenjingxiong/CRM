package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_mass_mailing_campaign] 对象
 */
public interface Imail_mass_mailing_campaign {

    /**
     * 获取 [被退回]
     */
    public void setBounced(Integer bounced);
    
    /**
     * 设置 [被退回]
     */
    public Integer getBounced();

    /**
     * 获取 [被退回]脏标记
     */
    public boolean getBouncedDirtyFlag();
    /**
     * 获取 [被退回的比率]
     */
    public void setBounced_ratio(Integer bounced_ratio);
    
    /**
     * 设置 [被退回的比率]
     */
    public Integer getBounced_ratio();

    /**
     * 获取 [被退回的比率]脏标记
     */
    public boolean getBounced_ratioDirtyFlag();
    /**
     * 获取 [运动_ ID]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [运动_ ID]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [运动_ ID]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [点击数]
     */
    public void setClicks_ratio(Integer clicks_ratio);
    
    /**
     * 设置 [点击数]
     */
    public Integer getClicks_ratio();

    /**
     * 获取 [点击数]脏标记
     */
    public boolean getClicks_ratioDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [已送货]
     */
    public void setDelivered(Integer delivered);
    
    /**
     * 设置 [已送货]
     */
    public Integer getDelivered();

    /**
     * 获取 [已送货]脏标记
     */
    public boolean getDeliveredDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [失败的]
     */
    public void setFailed(Integer failed);
    
    /**
     * 设置 [失败的]
     */
    public Integer getFailed();

    /**
     * 获取 [失败的]脏标记
     */
    public boolean getFailedDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [忽略]
     */
    public void setIgnored(Integer ignored);
    
    /**
     * 设置 [忽略]
     */
    public Integer getIgnored();

    /**
     * 获取 [忽略]脏标记
     */
    public boolean getIgnoredDirtyFlag();
    /**
     * 获取 [群发邮件]
     */
    public void setMass_mailing_ids(String mass_mailing_ids);
    
    /**
     * 设置 [群发邮件]
     */
    public String getMass_mailing_ids();

    /**
     * 获取 [群发邮件]脏标记
     */
    public boolean getMass_mailing_idsDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [营销名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [营销名称]
     */
    public String getName();

    /**
     * 获取 [营销名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [已开启]
     */
    public void setOpened(Integer opened);
    
    /**
     * 设置 [已开启]
     */
    public Integer getOpened();

    /**
     * 获取 [已开启]脏标记
     */
    public boolean getOpenedDirtyFlag();
    /**
     * 获取 [打开比例]
     */
    public void setOpened_ratio(Integer opened_ratio);
    
    /**
     * 设置 [打开比例]
     */
    public Integer getOpened_ratio();

    /**
     * 获取 [打开比例]脏标记
     */
    public boolean getOpened_ratioDirtyFlag();
    /**
     * 获取 [已接收比例]
     */
    public void setReceived_ratio(Integer received_ratio);
    
    /**
     * 设置 [已接收比例]
     */
    public Integer getReceived_ratio();

    /**
     * 获取 [已接收比例]脏标记
     */
    public boolean getReceived_ratioDirtyFlag();
    /**
     * 获取 [已回复]
     */
    public void setReplied(Integer replied);
    
    /**
     * 设置 [已回复]
     */
    public Integer getReplied();

    /**
     * 获取 [已回复]脏标记
     */
    public boolean getRepliedDirtyFlag();
    /**
     * 获取 [回复比例]
     */
    public void setReplied_ratio(Integer replied_ratio);
    
    /**
     * 设置 [回复比例]
     */
    public Integer getReplied_ratio();

    /**
     * 获取 [回复比例]脏标记
     */
    public boolean getReplied_ratioDirtyFlag();
    /**
     * 获取 [安排]
     */
    public void setScheduled(Integer scheduled);
    
    /**
     * 设置 [安排]
     */
    public Integer getScheduled();

    /**
     * 获取 [安排]脏标记
     */
    public boolean getScheduledDirtyFlag();
    /**
     * 获取 [发送邮件]
     */
    public void setSent(Integer sent);
    
    /**
     * 设置 [发送邮件]
     */
    public Integer getSent();

    /**
     * 获取 [发送邮件]脏标记
     */
    public boolean getSentDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [标签]
     */
    public String getTag_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setTotal(Integer total);
    
    /**
     * 设置 [总计]
     */
    public Integer getTotal();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getTotalDirtyFlag();
    /**
     * 获取 [邮件]
     */
    public void setTotal_mailings(Integer total_mailings);
    
    /**
     * 设置 [邮件]
     */
    public Integer getTotal_mailings();

    /**
     * 获取 [邮件]脏标记
     */
    public boolean getTotal_mailingsDirtyFlag();
    /**
     * 获取 [支持 A/B 测试]
     */
    public void setUnique_ab_testing(String unique_ab_testing);
    
    /**
     * 设置 [支持 A/B 测试]
     */
    public String getUnique_ab_testing();

    /**
     * 获取 [支持 A/B 测试]脏标记
     */
    public boolean getUnique_ab_testingDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

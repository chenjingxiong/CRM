package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_partial_reconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
public interface Iaccount_partial_reconcileClientService{

    public Iaccount_partial_reconcile createModel() ;

    public void get(Iaccount_partial_reconcile account_partial_reconcile);

    public void remove(Iaccount_partial_reconcile account_partial_reconcile);

    public void update(Iaccount_partial_reconcile account_partial_reconcile);

    public void updateBatch(List<Iaccount_partial_reconcile> account_partial_reconciles);

    public void create(Iaccount_partial_reconcile account_partial_reconcile);

    public Page<Iaccount_partial_reconcile> fetchDefault(SearchContext context);

    public void removeBatch(List<Iaccount_partial_reconcile> account_partial_reconciles);

    public void createBatch(List<Iaccount_partial_reconcile> account_partial_reconciles);

    public Page<Iaccount_partial_reconcile> select(SearchContext context);

    public void getDraft(Iaccount_partial_reconcile account_partial_reconcile);

}

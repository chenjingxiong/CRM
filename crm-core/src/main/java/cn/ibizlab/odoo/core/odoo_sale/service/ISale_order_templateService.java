package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;


/**
 * 实体[Sale_order_template] 服务对象接口
 */
public interface ISale_order_templateService{

    boolean create(Sale_order_template et) ;
    void createBatch(List<Sale_order_template> list) ;
    boolean update(Sale_order_template et) ;
    void updateBatch(List<Sale_order_template> list) ;
    Sale_order_template get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_order_template getDraft(Sale_order_template et) ;
    Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context) ;

}




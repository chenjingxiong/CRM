package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_language_export;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.base_language_export.Base_language_exportDataDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_language_export
 * 属性：Data
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_language_exportDataDefaultValidator.class})
public @interface Base_language_exportDataDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

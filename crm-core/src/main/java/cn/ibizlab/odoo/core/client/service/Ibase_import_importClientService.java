package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_import;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_import] 服务对象接口
 */
public interface Ibase_import_importClientService{

    public Ibase_import_import createModel() ;

    public void remove(Ibase_import_import base_import_import);

    public Page<Ibase_import_import> fetchDefault(SearchContext context);

    public void update(Ibase_import_import base_import_import);

    public void create(Ibase_import_import base_import_import);

    public void get(Ibase_import_import base_import_import);

    public void updateBatch(List<Ibase_import_import> base_import_imports);

    public void removeBatch(List<Ibase_import_import> base_import_imports);

    public void createBatch(List<Ibase_import_import> base_import_imports);

    public Page<Ibase_import_import> select(SearchContext context);

    public void getDraft(Ibase_import_import base_import_import);

}

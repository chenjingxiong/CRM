package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface Ipayment_acquirer_onboarding_wizardClientService{

    public Ipayment_acquirer_onboarding_wizard createModel() ;

    public void updateBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards);

    public void create(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    public Page<Ipayment_acquirer_onboarding_wizard> fetchDefault(SearchContext context);

    public void update(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    public void createBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards);

    public void get(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    public void remove(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

    public void removeBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards);

    public Page<Ipayment_acquirer_onboarding_wizard> select(SearchContext context);

    public void getDraft(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

}

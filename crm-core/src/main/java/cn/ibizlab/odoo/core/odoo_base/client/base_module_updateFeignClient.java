package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_module_update] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-module-update", fallback = base_module_updateFallback.class)
public interface base_module_updateFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/{id}")
    Base_module_update update(@PathVariable("id") Integer id,@RequestBody Base_module_update base_module_update);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/batch")
    Boolean updateBatch(@RequestBody List<Base_module_update> base_module_updates);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_updates")
    Base_module_update create(@RequestBody Base_module_update base_module_update);

    @RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/batch")
    Boolean createBatch(@RequestBody List<Base_module_update> base_module_updates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/searchdefault")
    Page<Base_module_update> searchDefault(@RequestBody Base_module_updateSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/{id}")
    Base_module_update get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/select")
    Page<Base_module_update> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/getdraft")
    Base_module_update getDraft();


}

package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_repair.client.repair_orderFeignClient;

/**
 * 实体[维修单] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_orderServiceImpl implements IRepair_orderService {

    @Autowired
    repair_orderFeignClient repair_orderFeignClient;


    @Override
    public Repair_order get(Integer id) {
		Repair_order et=repair_orderFeignClient.get(id);
        if(et==null){
            et=new Repair_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Repair_order getDraft(Repair_order et) {
        et=repair_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Repair_order et) {
        Repair_order rt = repair_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Repair_order> list){
        repair_orderFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=repair_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        repair_orderFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Repair_order et) {
        Repair_order rt = repair_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_order> list){
        repair_orderFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_order> searchDefault(Repair_orderSearchContext context) {
        Page<Repair_order> repair_orders=repair_orderFeignClient.searchDefault(context);
        return repair_orders;
    }


}



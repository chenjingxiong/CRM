package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [barcodes_barcode_events_mixin] 对象
 */
public interface Ibarcodes_barcode_events_mixin {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [条码扫描到]
     */
    public void set_barcode_scanned(String _barcode_scanned);
    
    /**
     * 设置 [条码扫描到]
     */
    public String get_barcode_scanned();

    /**
     * 获取 [条码扫描到]脏标记
     */
    public boolean get_barcode_scannedDirtyFlag();
    /**
     * 获取 [最后修改时间]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改时间]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改时间]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;


/**
 * 实体[Crm_lead_tag] 服务对象接口
 */
public interface ICrm_lead_tagService{

    Crm_lead_tag getDraft(Crm_lead_tag et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Crm_lead_tag et) ;
    void updateBatch(List<Crm_lead_tag> list) ;
    Crm_lead_tag get(Integer key) ;
    boolean create(Crm_lead_tag et) ;
    void createBatch(List<Crm_lead_tag> list) ;
    Page<Crm_lead_tag> searchDefault(Crm_lead_tagSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Asset Meters] 对象
 */
@Data
public class Mro_pm_meter extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * Min Utilization (per day)
     */
    @DEField(name = "min_utilization")
    @JSONField(name = "min_utilization")
    @JsonProperty("min_utilization")
    private Double minUtilization;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * Reading Type
     */
    @DEField(name = "reading_type")
    @JSONField(name = "reading_type")
    @JsonProperty("reading_type")
    private String readingType;

    /**
     * 值
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;

    /**
     * Meters
     */
    @JSONField(name = "meter_line_ids")
    @JsonProperty("meter_line_ids")
    private String meterLineIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * View Line
     */
    @JSONField(name = "view_line_ids")
    @JsonProperty("view_line_ids")
    private String viewLineIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * Utilization (per day)
     */
    @JSONField(name = "utilization")
    @JsonProperty("utilization")
    private Double utilization;

    /**
     * New value
     */
    @DEField(name = "new_value")
    @JSONField(name = "new_value")
    @JsonProperty("new_value")
    private Double newValue;

    /**
     * Averaging time (days)
     */
    @DEField(name = "av_time")
    @JSONField(name = "av_time")
    @JsonProperty("av_time")
    private Double avTime;

    /**
     * Total Value
     */
    @JSONField(name = "total_value")
    @JsonProperty("total_value")
    private Double totalValue;

    /**
     * Meter
     */
    @JSONField(name = "name_text")
    @JsonProperty("name_text")
    private String nameText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 单位
     */
    @JSONField(name = "meter_uom")
    @JsonProperty("meter_uom")
    private Integer meterUom;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * Asset
     */
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    private String assetIdText;

    /**
     * Ratio to Source
     */
    @JSONField(name = "parent_ratio_id_text")
    @JsonProperty("parent_ratio_id_text")
    private String parentRatioIdText;

    /**
     * Source Meter
     */
    @DEField(name = "parent_meter_id")
    @JSONField(name = "parent_meter_id")
    @JsonProperty("parent_meter_id")
    private Integer parentMeterId;

    /**
     * Ratio to Source
     */
    @DEField(name = "parent_ratio_id")
    @JSONField(name = "parent_ratio_id")
    @JsonProperty("parent_ratio_id")
    private Integer parentRatioId;

    /**
     * Meter
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private Integer name;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * Asset
     */
    @DEField(name = "asset_id")
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    private Integer assetId;


    /**
     * 
     */
    @JSONField(name = "odooasset")
    @JsonProperty("odooasset")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset odooAsset;

    /**
     * 
     */
    @JSONField(name = "odooparentratio")
    @JsonProperty("odooparentratio")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio odooParentRatio;

    /**
     * 
     */
    @JSONField(name = "odooparentmeter")
    @JsonProperty("odooparentmeter")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter odooParentMeter;

    /**
     * 
     */
    @JSONField(name = "odooname")
    @JsonProperty("odooname")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter odooName;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [Min Utilization (per day)]
     */
    public void setMinUtilization(Double minUtilization){
        this.minUtilization = minUtilization ;
        this.modify("min_utilization",minUtilization);
    }
    /**
     * 设置 [Reading Type]
     */
    public void setReadingType(String readingType){
        this.readingType = readingType ;
        this.modify("reading_type",readingType);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [New value]
     */
    public void setNewValue(Double newValue){
        this.newValue = newValue ;
        this.modify("new_value",newValue);
    }
    /**
     * 设置 [Averaging time (days)]
     */
    public void setAvTime(Double avTime){
        this.avTime = avTime ;
        this.modify("av_time",avTime);
    }
    /**
     * 设置 [Source Meter]
     */
    public void setParentMeterId(Integer parentMeterId){
        this.parentMeterId = parentMeterId ;
        this.modify("parent_meter_id",parentMeterId);
    }
    /**
     * 设置 [Ratio to Source]
     */
    public void setParentRatioId(Integer parentRatioId){
        this.parentRatioId = parentRatioId ;
        this.modify("parent_ratio_id",parentRatioId);
    }
    /**
     * 设置 [Meter]
     */
    public void setName(Integer name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [Asset]
     */
    public void setAssetId(Integer assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }

}



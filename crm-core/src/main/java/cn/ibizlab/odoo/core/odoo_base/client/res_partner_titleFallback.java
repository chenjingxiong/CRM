package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner_title] 服务对象接口
 */
@Component
public class res_partner_titleFallback implements res_partner_titleFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Res_partner_title update(Integer id, Res_partner_title res_partner_title){
            return null;
     }
    public Boolean updateBatch(List<Res_partner_title> res_partner_titles){
            return false;
     }


    public Page<Res_partner_title> searchDefault(Res_partner_titleSearchContext context){
            return null;
     }


    public Res_partner_title create(Res_partner_title res_partner_title){
            return null;
     }
    public Boolean createBatch(List<Res_partner_title> res_partner_titles){
            return false;
     }

    public Res_partner_title get(Integer id){
            return null;
     }




    public Page<Res_partner_title> select(){
            return null;
     }

    public Res_partner_title getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_event_type] 服务对象接口
 */
@Component
public class calendar_event_typeFallback implements calendar_event_typeFeignClient{


    public Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context){
            return null;
     }


    public Calendar_event_type update(Integer id, Calendar_event_type calendar_event_type){
            return null;
     }
    public Boolean updateBatch(List<Calendar_event_type> calendar_event_types){
            return false;
     }


    public Calendar_event_type get(Integer id){
            return null;
     }


    public Calendar_event_type create(Calendar_event_type calendar_event_type){
            return null;
     }
    public Boolean createBatch(List<Calendar_event_type> calendar_event_types){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Calendar_event_type> select(){
            return null;
     }

    public Calendar_event_type getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_message_subtype] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-message-subtype", fallback = mail_message_subtypeFallback.class)
public interface mail_message_subtypeFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/{id}")
    Mail_message_subtype get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/{id}")
    Mail_message_subtype update(@PathVariable("id") Integer id,@RequestBody Mail_message_subtype mail_message_subtype);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/batch")
    Boolean updateBatch(@RequestBody List<Mail_message_subtype> mail_message_subtypes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes")
    Mail_message_subtype create(@RequestBody Mail_message_subtype mail_message_subtype);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/batch")
    Boolean createBatch(@RequestBody List<Mail_message_subtype> mail_message_subtypes);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/searchdefault")
    Page<Mail_message_subtype> searchDefault(@RequestBody Mail_message_subtypeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/select")
    Page<Mail_message_subtype> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/getdraft")
    Mail_message_subtype getDraft();


}

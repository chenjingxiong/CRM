package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_type] 服务对象接口
 */
@Component
public class event_typeFallback implements event_typeFeignClient{




    public Event_type get(Integer id){
            return null;
     }


    public Page<Event_type> searchDefault(Event_typeSearchContext context){
            return null;
     }


    public Event_type create(Event_type event_type){
            return null;
     }
    public Boolean createBatch(List<Event_type> event_types){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Event_type update(Integer id, Event_type event_type){
            return null;
     }
    public Boolean updateBatch(List<Event_type> event_types){
            return false;
     }


    public Page<Event_type> select(){
            return null;
     }

    public Event_type getDraft(){
            return null;
    }



}

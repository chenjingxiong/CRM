package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_moderation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_moderation] 服务对象接口
 */
public interface Imail_moderationClientService{

    public Imail_moderation createModel() ;

    public void remove(Imail_moderation mail_moderation);

    public void removeBatch(List<Imail_moderation> mail_moderations);

    public Page<Imail_moderation> fetchDefault(SearchContext context);

    public void update(Imail_moderation mail_moderation);

    public void get(Imail_moderation mail_moderation);

    public void create(Imail_moderation mail_moderation);

    public void createBatch(List<Imail_moderation> mail_moderations);

    public void updateBatch(List<Imail_moderation> mail_moderations);

    public Page<Imail_moderation> select(SearchContext context);

    public void getDraft(Imail_moderation mail_moderation);

}

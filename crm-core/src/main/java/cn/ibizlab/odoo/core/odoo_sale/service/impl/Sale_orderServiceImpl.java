package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_orderFeignClient;

/**
 * 实体[销售订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_orderServiceImpl implements ISale_orderService {

    @Autowired
    sale_orderFeignClient sale_orderFeignClient;


    @Override
    public boolean update(Sale_order et) {
        Sale_order rt = sale_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_order> list){
        sale_orderFeignClient.updateBatch(list) ;
    }

    @Override
    public Sale_order get(Integer id) {
		Sale_order et=sale_orderFeignClient.get(id);
        if(et==null){
            et=new Sale_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    @Transactional
    public boolean save(Sale_order et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!sale_orderFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Sale_order> list) {
        sale_orderFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean checkKey(Sale_order et) {
        return sale_orderFeignClient.checkKey(et);
    }
    @Override
    public boolean remove(Integer id) {
        boolean result=sale_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_orderFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_order getDraft(Sale_order et) {
        et=sale_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Sale_order et) {
        Sale_order rt = sale_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order> list){
        sale_orderFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order> searchDefault(Sale_orderSearchContext context) {
        Page<Sale_order> sale_orders=sale_orderFeignClient.searchDefault(context);
        return sale_orders;
    }


}



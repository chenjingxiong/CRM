package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;


/**
 * 实体[Mrp_workcenter_productivity_loss] 服务对象接口
 */
public interface IMrp_workcenter_productivity_lossService{

    Mrp_workcenter_productivity_loss getDraft(Mrp_workcenter_productivity_loss et) ;
    boolean create(Mrp_workcenter_productivity_loss et) ;
    void createBatch(List<Mrp_workcenter_productivity_loss> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_workcenter_productivity_loss get(Integer key) ;
    boolean update(Mrp_workcenter_productivity_loss et) ;
    void updateBatch(List<Mrp_workcenter_productivity_loss> list) ;
    Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [游戏化挑战的一般目标] 对象
 */
@Data
public class Gamification_challenge_line extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 要达到的目标值
     */
    @DEField(name = "target_goal")
    @JSONField(name = "target_goal")
    @JsonProperty("target_goal")
    private Double targetGoal;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 货币
     */
    @JSONField(name = "definition_monetary")
    @JsonProperty("definition_monetary")
    private String definitionMonetary;

    /**
     * 目标绩效
     */
    @JSONField(name = "condition")
    @JsonProperty("condition")
    private String condition;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 后缀
     */
    @JSONField(name = "definition_full_suffix")
    @JsonProperty("definition_full_suffix")
    private String definitionFullSuffix;

    /**
     * 挑战
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    private String challengeIdText;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 单位
     */
    @JSONField(name = "definition_suffix")
    @JsonProperty("definition_suffix")
    private String definitionSuffix;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 挑战
     */
    @DEField(name = "challenge_id")
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    private Integer challengeId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 目标定义
     */
    @DEField(name = "definition_id")
    @JSONField(name = "definition_id")
    @JsonProperty("definition_id")
    private Integer definitionId;


    /**
     * 
     */
    @JSONField(name = "odoochallenge")
    @JsonProperty("odoochallenge")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge odooChallenge;

    /**
     * 
     */
    @JSONField(name = "odoodefinition")
    @JsonProperty("odoodefinition")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition odooDefinition;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [要达到的目标值]
     */
    public void setTargetGoal(Double targetGoal){
        this.targetGoal = targetGoal ;
        this.modify("target_goal",targetGoal);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [挑战]
     */
    public void setChallengeId(Integer challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }
    /**
     * 设置 [目标定义]
     */
    public void setDefinitionId(Integer definitionId){
        this.definitionId = definitionId ;
        this.modify("definition_id",definitionId);
    }

}



package cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_employee;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cn.ibizlab.odoo.util.valuerule.DefaultValueRule;
import cn.ibizlab.odoo.util.valuerule.VRCondition;
import cn.ibizlab.odoo.util.valuerule.condition.*;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_employee.Hr_employeeNewly_hired_employeeDefault;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 值规则注解解析类
 * 实体：Hr_employee
 * 属性：Newly_hired_employee
 * 值规则：Default
 * 值规则信息：内容长度必须小于等于[200]
 */
@Slf4j
@IBIZLog
@Component("Hr_employeeNewly_hired_employeeDefaultValidator")
public class Hr_employeeNewly_hired_employeeDefaultValidator implements ConstraintValidator<Hr_employeeNewly_hired_employeeDefault, String>,Validator {
    private static final String MESSAGE = "值规则校验失败：【内容长度必须小于等于[200]】";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if( o!=null && supports(o.getClass())){
            if (!doValidate((String) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(String value) {
        DefaultValueRule<String> valueRule = new DefaultValueRule<>("默认值规则",MESSAGE,"Newly_hired_employee",value)
                //字符串长度，重复检查模式，重复值范围，基础值规则，是否递归检查。
                .init(200,"NONE",null,null,false);
        return valueRule.isValid();
    }
}


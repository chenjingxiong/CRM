package cn.ibizlab.odoo.core.odoo_iap.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[iap_account] 服务对象接口
 */
@FeignClient(value = "odoo-iap", contextId = "iap-account", fallback = iap_accountFallback.class)
public interface iap_accountFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/searchdefault")
    Page<Iap_account> searchDefault(@RequestBody Iap_accountSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/iap_accounts")
    Iap_account create(@RequestBody Iap_account iap_account);

    @RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/batch")
    Boolean createBatch(@RequestBody List<Iap_account> iap_accounts);




    @RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/{id}")
    Iap_account update(@PathVariable("id") Integer id,@RequestBody Iap_account iap_account);

    @RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/batch")
    Boolean updateBatch(@RequestBody List<Iap_account> iap_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/{id}")
    Iap_account get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/select")
    Page<Iap_account> select();


    @RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/getdraft")
    Iap_account getDraft();


}

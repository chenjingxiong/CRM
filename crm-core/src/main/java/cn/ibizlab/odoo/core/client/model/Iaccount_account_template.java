package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_account_template] 对象
 */
public interface Iaccount_account_template {

    /**
     * 获取 [表模板]
     */
    public void setChart_template_id(Integer chart_template_id);
    
    /**
     * 设置 [表模板]
     */
    public Integer getChart_template_id();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_idDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id_text(String chart_template_id_text);
    
    /**
     * 设置 [表模板]
     */
    public String getChart_template_id_text();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_id_textDirtyFlag();
    /**
     * 获取 [代码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [代码]
     */
    public String getCode();

    /**
     * 获取 [代码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [科目币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [科目币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [科目币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [科目币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [科目币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [科目币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [组]
     */
    public void setGroup_id(Integer group_id);
    
    /**
     * 设置 [组]
     */
    public Integer getGroup_id();

    /**
     * 获取 [组]脏标记
     */
    public boolean getGroup_idDirtyFlag();
    /**
     * 获取 [组]
     */
    public void setGroup_id_text(String group_id_text);
    
    /**
     * 设置 [组]
     */
    public String getGroup_id_text();

    /**
     * 获取 [组]脏标记
     */
    public boolean getGroup_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [选项创建]
     */
    public void setNocreate(String nocreate);
    
    /**
     * 设置 [选项创建]
     */
    public String getNocreate();

    /**
     * 获取 [选项创建]脏标记
     */
    public boolean getNocreateDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [允许发票和付款匹配]
     */
    public void setReconcile(String reconcile);
    
    /**
     * 设置 [允许发票和付款匹配]
     */
    public String getReconcile();

    /**
     * 获取 [允许发票和付款匹配]脏标记
     */
    public boolean getReconcileDirtyFlag();
    /**
     * 获取 [科目标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [科目标签]
     */
    public String getTag_ids();

    /**
     * 获取 [科目标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [默认税]
     */
    public void setTax_ids(String tax_ids);
    
    /**
     * 设置 [默认税]
     */
    public String getTax_ids();

    /**
     * 获取 [默认税]脏标记
     */
    public boolean getTax_idsDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setUser_type_id(Integer user_type_id);
    
    /**
     * 设置 [类型]
     */
    public Integer getUser_type_id();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getUser_type_idDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setUser_type_id_text(String user_type_id_text);
    
    /**
     * 设置 [类型]
     */
    public String getUser_type_id_text();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getUser_type_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

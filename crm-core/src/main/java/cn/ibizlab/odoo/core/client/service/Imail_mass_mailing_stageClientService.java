package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
public interface Imail_mass_mailing_stageClientService{

    public Imail_mass_mailing_stage createModel() ;

    public void create(Imail_mass_mailing_stage mail_mass_mailing_stage);

    public void updateBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages);

    public void createBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages);

    public void removeBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages);

    public void update(Imail_mass_mailing_stage mail_mass_mailing_stage);

    public void remove(Imail_mass_mailing_stage mail_mass_mailing_stage);

    public void get(Imail_mass_mailing_stage mail_mass_mailing_stage);

    public Page<Imail_mass_mailing_stage> fetchDefault(SearchContext context);

    public Page<Imail_mass_mailing_stage> select(SearchContext context);

    public void getDraft(Imail_mass_mailing_stage mail_mass_mailing_stage);

}

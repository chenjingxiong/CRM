package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;


/**
 * 实体[Fleet_vehicle_log_services] 服务对象接口
 */
public interface IFleet_vehicle_log_servicesService{

    boolean create(Fleet_vehicle_log_services et) ;
    void createBatch(List<Fleet_vehicle_log_services> list) ;
    Fleet_vehicle_log_services getDraft(Fleet_vehicle_log_services et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Fleet_vehicle_log_services et) ;
    void updateBatch(List<Fleet_vehicle_log_services> list) ;
    Fleet_vehicle_log_services get(Integer key) ;
    Page<Fleet_vehicle_log_services> searchDefault(Fleet_vehicle_log_servicesSearchContext context) ;

}




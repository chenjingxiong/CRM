package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_report;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_report.Sale_reportDiscount_amountDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_report
 * 属性：Discount_amount
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_reportDiscount_amountDefaultValidator.class})
public @interface Sale_reportDiscount_amountDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

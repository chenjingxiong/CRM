package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [purchase_bill_union] 对象
 */
public interface Ipurchase_bill_union {

    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setName(String name);
    
    /**
     * 设置 [参考]
     */
    public String getName();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [供应商]
     */
    public Integer getPartner_id();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [供应商]
     */
    public String getPartner_id_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [采购订单]
     */
    public void setPurchase_order_id(Integer purchase_order_id);
    
    /**
     * 设置 [采购订单]
     */
    public Integer getPurchase_order_id();

    /**
     * 获取 [采购订单]脏标记
     */
    public boolean getPurchase_order_idDirtyFlag();
    /**
     * 获取 [采购订单]
     */
    public void setPurchase_order_id_text(String purchase_order_id_text);
    
    /**
     * 设置 [采购订单]
     */
    public String getPurchase_order_id_text();

    /**
     * 获取 [采购订单]脏标记
     */
    public boolean getPurchase_order_id_textDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [来源]
     */
    public String getReference();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [供应商账单]
     */
    public void setVendor_bill_id(Integer vendor_bill_id);
    
    /**
     * 设置 [供应商账单]
     */
    public Integer getVendor_bill_id();

    /**
     * 获取 [供应商账单]脏标记
     */
    public boolean getVendor_bill_idDirtyFlag();
    /**
     * 获取 [供应商账单]
     */
    public void setVendor_bill_id_text(String vendor_bill_id_text);
    
    /**
     * 设置 [供应商账单]
     */
    public String getVendor_bill_id_text();

    /**
     * 获取 [供应商账单]脏标记
     */
    public boolean getVendor_bill_id_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

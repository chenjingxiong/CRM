package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_multi_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_multi_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_multi_mixinFeignClient;

/**
 * 实体[多网站的Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_multi_mixinServiceImpl implements IWebsite_multi_mixinService {

    @Autowired
    website_multi_mixinFeignClient website_multi_mixinFeignClient;


    @Override
    public boolean update(Website_multi_mixin et) {
        Website_multi_mixin rt = website_multi_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_multi_mixin> list){
        website_multi_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public Website_multi_mixin get(Integer id) {
		Website_multi_mixin et=website_multi_mixinFeignClient.get(id);
        if(et==null){
            et=new Website_multi_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_multi_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_multi_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Website_multi_mixin getDraft(Website_multi_mixin et) {
        et=website_multi_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Website_multi_mixin et) {
        Website_multi_mixin rt = website_multi_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_multi_mixin> list){
        website_multi_mixinFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_multi_mixin> searchDefault(Website_multi_mixinSearchContext context) {
        Page<Website_multi_mixin> website_multi_mixins=website_multi_mixinFeignClient.searchDefault(context);
        return website_multi_mixins;
    }


}



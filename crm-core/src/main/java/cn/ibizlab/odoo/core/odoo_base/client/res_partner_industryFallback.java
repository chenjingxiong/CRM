package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner_industry] 服务对象接口
 */
@Component
public class res_partner_industryFallback implements res_partner_industryFeignClient{

    public Res_partner_industry create(Res_partner_industry res_partner_industry){
            return null;
     }
    public Boolean createBatch(List<Res_partner_industry> res_partner_industries){
            return false;
     }


    public Res_partner_industry update(Integer id, Res_partner_industry res_partner_industry){
            return null;
     }
    public Boolean updateBatch(List<Res_partner_industry> res_partner_industries){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context){
            return null;
     }


    public Res_partner_industry get(Integer id){
            return null;
     }


    public Page<Res_partner_industry> select(){
            return null;
     }

    public Res_partner_industry getDraft(){
            return null;
    }



}

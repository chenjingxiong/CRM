package cn.ibizlab.odoo.core.odoo_asset.valuerule.anno.asset_asset;

import cn.ibizlab.odoo.core.odoo_asset.valuerule.validator.asset_asset.Asset_assetWarehouse_state_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Asset_asset
 * 属性：Warehouse_state_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Asset_assetWarehouse_state_idDefaultValidator.class})
public @interface Asset_assetWarehouse_state_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

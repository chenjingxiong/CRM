package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;


/**
 * 实体[Res_partner] 服务对象接口
 */
public interface IRes_partnerService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_partner get(Integer key) ;
    boolean checkKey(Res_partner et) ;
    boolean save(Res_partner et) ;
    void saveBatch(List<Res_partner> list) ;
    boolean update(Res_partner et) ;
    void updateBatch(List<Res_partner> list) ;
    Res_partner getDraft(Res_partner et) ;
    boolean create(Res_partner et) ;
    void createBatch(List<Res_partner> list) ;
    Page<Res_partner> searchContacts(Res_partnerSearchContext context) ;
    Page<Res_partner> searchCompany(Res_partnerSearchContext context) ;
    Page<Res_partner> searchDefault(Res_partnerSearchContext context) ;

}




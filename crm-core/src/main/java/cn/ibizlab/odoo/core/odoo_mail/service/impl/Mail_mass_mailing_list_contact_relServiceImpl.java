package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_list_contact_relFeignClient;

/**
 * 实体[群发邮件订阅信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_list_contact_relServiceImpl implements IMail_mass_mailing_list_contact_relService {

    @Autowired
    mail_mass_mailing_list_contact_relFeignClient mail_mass_mailing_list_contact_relFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_list_contact_relFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_list_contact_relFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_mass_mailing_list_contact_rel et) {
        Mail_mass_mailing_list_contact_rel rt = mail_mass_mailing_list_contact_relFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_list_contact_rel> list){
        mail_mass_mailing_list_contact_relFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_mass_mailing_list_contact_rel getDraft(Mail_mass_mailing_list_contact_rel et) {
        et=mail_mass_mailing_list_contact_relFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_mass_mailing_list_contact_rel get(Integer id) {
		Mail_mass_mailing_list_contact_rel et=mail_mass_mailing_list_contact_relFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_list_contact_rel();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_mass_mailing_list_contact_rel et) {
        Mail_mass_mailing_list_contact_rel rt = mail_mass_mailing_list_contact_relFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_list_contact_rel> list){
        mail_mass_mailing_list_contact_relFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) {
        Page<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels=mail_mass_mailing_list_contact_relFeignClient.searchDefault(context);
        return mail_mass_mailing_list_contact_rels;
    }


}



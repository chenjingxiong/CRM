package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_journal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_journal] 服务对象接口
 */
public interface Iaccount_journalClientService{

    public Iaccount_journal createModel() ;

    public void updateBatch(List<Iaccount_journal> account_journals);

    public void get(Iaccount_journal account_journal);

    public void create(Iaccount_journal account_journal);

    public void createBatch(List<Iaccount_journal> account_journals);

    public Page<Iaccount_journal> fetchDefault(SearchContext context);

    public void update(Iaccount_journal account_journal);

    public void remove(Iaccount_journal account_journal);

    public void removeBatch(List<Iaccount_journal> account_journals);

    public Page<Iaccount_journal> select(SearchContext context);

    public void getDraft(Iaccount_journal account_journal);

}

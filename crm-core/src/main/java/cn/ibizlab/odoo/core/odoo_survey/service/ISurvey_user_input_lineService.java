package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;


/**
 * 实体[Survey_user_input_line] 服务对象接口
 */
public interface ISurvey_user_input_lineService{

    boolean update(Survey_user_input_line et) ;
    void updateBatch(List<Survey_user_input_line> list) ;
    Survey_user_input_line get(Integer key) ;
    Survey_user_input_line getDraft(Survey_user_input_line et) ;
    boolean create(Survey_user_input_line et) ;
    void createBatch(List<Survey_user_input_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context) ;

}




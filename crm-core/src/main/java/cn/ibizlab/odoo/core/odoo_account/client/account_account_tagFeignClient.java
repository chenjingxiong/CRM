package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_account_tag] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-account-tag", fallback = account_account_tagFallback.class)
public interface account_account_tagFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_account_tags")
    Account_account_tag create(@RequestBody Account_account_tag account_account_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/batch")
    Boolean createBatch(@RequestBody List<Account_account_tag> account_account_tags);





    @RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/searchdefault")
    Page<Account_account_tag> searchDefault(@RequestBody Account_account_tagSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/{id}")
    Account_account_tag update(@PathVariable("id") Integer id,@RequestBody Account_account_tag account_account_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/batch")
    Boolean updateBatch(@RequestBody List<Account_account_tag> account_account_tags);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/{id}")
    Account_account_tag get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/select")
    Page<Account_account_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/getdraft")
    Account_account_tag getDraft();


}

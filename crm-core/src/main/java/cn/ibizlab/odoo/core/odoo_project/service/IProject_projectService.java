package cn.ibizlab.odoo.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;


/**
 * 实体[Project_project] 服务对象接口
 */
public interface IProject_projectService{

    Project_project get(Integer key) ;
    boolean update(Project_project et) ;
    void updateBatch(List<Project_project> list) ;
    Project_project getDraft(Project_project et) ;
    boolean create(Project_project et) ;
    void createBatch(List<Project_project> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Project_project> searchDefault(Project_projectSearchContext context) ;

}




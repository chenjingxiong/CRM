package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_odometer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
public interface Ifleet_vehicle_odometerClientService{

    public Ifleet_vehicle_odometer createModel() ;

    public void removeBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers);

    public void createBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers);

    public void remove(Ifleet_vehicle_odometer fleet_vehicle_odometer);

    public void updateBatch(List<Ifleet_vehicle_odometer> fleet_vehicle_odometers);

    public void create(Ifleet_vehicle_odometer fleet_vehicle_odometer);

    public void get(Ifleet_vehicle_odometer fleet_vehicle_odometer);

    public void update(Ifleet_vehicle_odometer fleet_vehicle_odometer);

    public Page<Ifleet_vehicle_odometer> fetchDefault(SearchContext context);

    public Page<Ifleet_vehicle_odometer> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_odometer fleet_vehicle_odometer);

}

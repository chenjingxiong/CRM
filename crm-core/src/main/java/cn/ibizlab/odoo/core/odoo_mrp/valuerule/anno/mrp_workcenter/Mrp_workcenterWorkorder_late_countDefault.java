package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workcenter;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_workcenter.Mrp_workcenterWorkorder_late_countDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_workcenter
 * 属性：Workorder_late_count
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_workcenterWorkorder_late_countDefaultValidator.class})
public @interface Mrp_workcenterWorkorder_late_countDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_tagFeignClient;

/**
 * 实体[车辆标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_tagServiceImpl implements IFleet_vehicle_tagService {

    @Autowired
    fleet_vehicle_tagFeignClient fleet_vehicle_tagFeignClient;


    @Override
    public Fleet_vehicle_tag getDraft(Fleet_vehicle_tag et) {
        et=fleet_vehicle_tagFeignClient.getDraft();
        return et;
    }

    @Override
    public Fleet_vehicle_tag get(Integer id) {
		Fleet_vehicle_tag et=fleet_vehicle_tagFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Fleet_vehicle_tag et) {
        Fleet_vehicle_tag rt = fleet_vehicle_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_tag> list){
        fleet_vehicle_tagFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Fleet_vehicle_tag et) {
        Fleet_vehicle_tag rt = fleet_vehicle_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_tag> list){
        fleet_vehicle_tagFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_tagFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_tag> searchDefault(Fleet_vehicle_tagSearchContext context) {
        Page<Fleet_vehicle_tag> fleet_vehicle_tags=fleet_vehicle_tagFeignClient.searchDefault(context);
        return fleet_vehicle_tags;
    }


}



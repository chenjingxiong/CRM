package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-lead2opportunity-partner-mass", fallback = crm_lead2opportunity_partner_massFallback.class)
public interface crm_lead2opportunity_partner_massFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/searchdefault")
    Page<Crm_lead2opportunity_partner_mass> searchDefault(@RequestBody Crm_lead2opportunity_partner_massSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/{id}")
    Crm_lead2opportunity_partner_mass update(@PathVariable("id") Integer id,@RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/batch")
    Boolean updateBatch(@RequestBody List<Crm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/{id}")
    Crm_lead2opportunity_partner_mass get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses")
    Crm_lead2opportunity_partner_mass create(@RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/batch")
    Boolean createBatch(@RequestBody List<Crm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses);




    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/select")
    Page<Crm_lead2opportunity_partner_mass> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/getdraft")
    Crm_lead2opportunity_partner_mass getDraft();


}

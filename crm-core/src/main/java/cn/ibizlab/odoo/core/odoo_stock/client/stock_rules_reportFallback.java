package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_rules_report] 服务对象接口
 */
@Component
public class stock_rules_reportFallback implements stock_rules_reportFeignClient{

    public Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context){
            return null;
     }



    public Stock_rules_report update(Integer id, Stock_rules_report stock_rules_report){
            return null;
     }
    public Boolean updateBatch(List<Stock_rules_report> stock_rules_reports){
            return false;
     }



    public Stock_rules_report get(Integer id){
            return null;
     }



    public Stock_rules_report create(Stock_rules_report stock_rules_report){
            return null;
     }
    public Boolean createBatch(List<Stock_rules_report> stock_rules_reports){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_rules_report> select(){
            return null;
     }

    public Stock_rules_report getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_quantFeignClient;

/**
 * 实体[即时库存] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_quantServiceImpl implements IStock_quantService {

    @Autowired
    stock_quantFeignClient stock_quantFeignClient;


    @Override
    public boolean create(Stock_quant et) {
        Stock_quant rt = stock_quantFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_quant> list){
        stock_quantFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_quant getDraft(Stock_quant et) {
        et=stock_quantFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_quantFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_quantFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_quant et) {
        Stock_quant rt = stock_quantFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_quant> list){
        stock_quantFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_quant get(Integer id) {
		Stock_quant et=stock_quantFeignClient.get(id);
        if(et==null){
            et=new Stock_quant();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_quant> searchDefault(Stock_quantSearchContext context) {
        Page<Stock_quant> stock_quants=stock_quantFeignClient.searchDefault(context);
        return stock_quants;
    }


}



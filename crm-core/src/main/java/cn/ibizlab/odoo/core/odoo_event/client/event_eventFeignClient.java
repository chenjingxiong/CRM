package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_event] 服务对象接口
 */
@FeignClient(value = "odoo-event", contextId = "event-event", fallback = event_eventFallback.class)
public interface event_eventFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/event_events")
    Event_event create(@RequestBody Event_event event_event);

    @RequestMapping(method = RequestMethod.POST, value = "/event_events/batch")
    Boolean createBatch(@RequestBody List<Event_event> event_events);


    @RequestMapping(method = RequestMethod.GET, value = "/event_events/{id}")
    Event_event get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/event_events/searchdefault")
    Page<Event_event> searchDefault(@RequestBody Event_eventSearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/event_events/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_events/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/event_events/{id}")
    Event_event update(@PathVariable("id") Integer id,@RequestBody Event_event event_event);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_events/batch")
    Boolean updateBatch(@RequestBody List<Event_event> event_events);



    @RequestMapping(method = RequestMethod.GET, value = "/event_events/select")
    Page<Event_event> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_events/getdraft")
    Event_event getDraft();


}

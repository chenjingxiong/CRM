package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_account_typeFeignClient;

/**
 * 实体[科目类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_account_typeServiceImpl implements IAccount_account_typeService {

    @Autowired
    account_account_typeFeignClient account_account_typeFeignClient;


    @Override
    public Account_account_type get(Integer id) {
		Account_account_type et=account_account_typeFeignClient.get(id);
        if(et==null){
            et=new Account_account_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_account_type getDraft(Account_account_type et) {
        et=account_account_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_account_type et) {
        Account_account_type rt = account_account_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_account_type> list){
        account_account_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_account_type et) {
        Account_account_type rt = account_account_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account_type> list){
        account_account_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_account_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_account_typeFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account_type> searchDefault(Account_account_typeSearchContext context) {
        Page<Account_account_type> account_account_types=account_account_typeFeignClient.searchDefault(context);
        return account_account_types;
    }


}



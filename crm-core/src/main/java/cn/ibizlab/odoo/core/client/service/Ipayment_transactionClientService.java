package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_transaction] 服务对象接口
 */
public interface Ipayment_transactionClientService{

    public Ipayment_transaction createModel() ;

    public void removeBatch(List<Ipayment_transaction> payment_transactions);

    public void create(Ipayment_transaction payment_transaction);

    public Page<Ipayment_transaction> fetchDefault(SearchContext context);

    public void createBatch(List<Ipayment_transaction> payment_transactions);

    public void updateBatch(List<Ipayment_transaction> payment_transactions);

    public void get(Ipayment_transaction payment_transaction);

    public void remove(Ipayment_transaction payment_transaction);

    public void update(Ipayment_transaction payment_transaction);

    public Page<Ipayment_transaction> select(SearchContext context);

    public void getDraft(Ipayment_transaction payment_transaction);

}

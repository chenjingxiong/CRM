package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cashbox_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_cashbox_lineFeignClient;

/**
 * 实体[钱箱明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_cashbox_lineServiceImpl implements IAccount_cashbox_lineService {

    @Autowired
    account_cashbox_lineFeignClient account_cashbox_lineFeignClient;


    @Override
    public boolean create(Account_cashbox_line et) {
        Account_cashbox_line rt = account_cashbox_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_cashbox_line> list){
        account_cashbox_lineFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_cashbox_line et) {
        Account_cashbox_line rt = account_cashbox_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_cashbox_line> list){
        account_cashbox_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_cashbox_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_cashbox_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Account_cashbox_line get(Integer id) {
		Account_cashbox_line et=account_cashbox_lineFeignClient.get(id);
        if(et==null){
            et=new Account_cashbox_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_cashbox_line getDraft(Account_cashbox_line et) {
        et=account_cashbox_lineFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context) {
        Page<Account_cashbox_line> account_cashbox_lines=account_cashbox_lineFeignClient.searchDefault(context);
        return account_cashbox_lines;
    }


}



package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_alarmFeignClient;

/**
 * 实体[活动提醒] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_alarmServiceImpl implements ICalendar_alarmService {

    @Autowired
    calendar_alarmFeignClient calendar_alarmFeignClient;


    @Override
    public Calendar_alarm get(Integer id) {
		Calendar_alarm et=calendar_alarmFeignClient.get(id);
        if(et==null){
            et=new Calendar_alarm();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Calendar_alarm getDraft(Calendar_alarm et) {
        et=calendar_alarmFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_alarmFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_alarmFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Calendar_alarm et) {
        Calendar_alarm rt = calendar_alarmFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_alarm> list){
        calendar_alarmFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Calendar_alarm et) {
        Calendar_alarm rt = calendar_alarmFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_alarm> list){
        calendar_alarmFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context) {
        Page<Calendar_alarm> calendar_alarms=calendar_alarmFeignClient.searchDefault(context);
        return calendar_alarms;
    }


}



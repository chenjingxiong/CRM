package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_order_lineFeignClient;

/**
 * 实体[工作餐订单行] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_order_lineServiceImpl implements ILunch_order_lineService {

    @Autowired
    lunch_order_lineFeignClient lunch_order_lineFeignClient;


    @Override
    public boolean update(Lunch_order_line et) {
        Lunch_order_line rt = lunch_order_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_order_line> list){
        lunch_order_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Lunch_order_line get(Integer id) {
		Lunch_order_line et=lunch_order_lineFeignClient.get(id);
        if(et==null){
            et=new Lunch_order_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Lunch_order_line getDraft(Lunch_order_line et) {
        et=lunch_order_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_order_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_order_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Lunch_order_line et) {
        Lunch_order_line rt = lunch_order_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order_line> list){
        lunch_order_lineFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context) {
        Page<Lunch_order_line> lunch_order_lines=lunch_order_lineFeignClient.searchDefault(context);
        return lunch_order_lines;
    }


}



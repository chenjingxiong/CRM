package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_fixed_putaway_stratService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_fixed_putaway_stratFeignClient;

/**
 * 实体[位置固定上架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_fixed_putaway_stratServiceImpl implements IStock_fixed_putaway_stratService {

    @Autowired
    stock_fixed_putaway_stratFeignClient stock_fixed_putaway_stratFeignClient;


    @Override
    public Stock_fixed_putaway_strat getDraft(Stock_fixed_putaway_strat et) {
        et=stock_fixed_putaway_stratFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_fixed_putaway_stratFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_fixed_putaway_stratFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_fixed_putaway_strat get(Integer id) {
		Stock_fixed_putaway_strat et=stock_fixed_putaway_stratFeignClient.get(id);
        if(et==null){
            et=new Stock_fixed_putaway_strat();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_fixed_putaway_strat et) {
        Stock_fixed_putaway_strat rt = stock_fixed_putaway_stratFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_fixed_putaway_strat> list){
        stock_fixed_putaway_stratFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_fixed_putaway_strat et) {
        Stock_fixed_putaway_strat rt = stock_fixed_putaway_stratFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_fixed_putaway_strat> list){
        stock_fixed_putaway_stratFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context) {
        Page<Stock_fixed_putaway_strat> stock_fixed_putaway_strats=stock_fixed_putaway_stratFeignClient.searchDefault(context);
        return stock_fixed_putaway_strats;
    }


}



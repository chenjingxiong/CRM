package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_badge_user_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
public interface Igamification_badge_user_wizardClientService{

    public Igamification_badge_user_wizard createModel() ;

    public void updateBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards);

    public void removeBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards);

    public void get(Igamification_badge_user_wizard gamification_badge_user_wizard);

    public void remove(Igamification_badge_user_wizard gamification_badge_user_wizard);

    public void create(Igamification_badge_user_wizard gamification_badge_user_wizard);

    public void update(Igamification_badge_user_wizard gamification_badge_user_wizard);

    public Page<Igamification_badge_user_wizard> fetchDefault(SearchContext context);

    public void createBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards);

    public Page<Igamification_badge_user_wizard> select(SearchContext context);

    public void getDraft(Igamification_badge_user_wizard gamification_badge_user_wizard);

}

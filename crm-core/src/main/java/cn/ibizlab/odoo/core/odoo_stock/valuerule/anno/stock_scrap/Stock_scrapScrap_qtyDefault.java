package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_scrap;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_scrap.Stock_scrapScrap_qtyDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_scrap
 * 属性：Scrap_qty
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_scrapScrap_qtyDefaultValidator.class})
public @interface Stock_scrapScrap_qtyDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_report] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-invoice-report", fallback = account_invoice_reportFallback.class)
public interface account_invoice_reportFeignClient {




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/searchdefault")
    Page<Account_invoice_report> searchDefault(@RequestBody Account_invoice_reportSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/{id}")
    Account_invoice_report get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports")
    Account_invoice_report create(@RequestBody Account_invoice_report account_invoice_report);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_report> account_invoice_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/{id}")
    Account_invoice_report update(@PathVariable("id") Integer id,@RequestBody Account_invoice_report account_invoice_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_report> account_invoice_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/select")
    Page<Account_invoice_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/getdraft")
    Account_invoice_report getDraft();


}

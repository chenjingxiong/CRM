package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;


/**
 * 实体[Mrp_product_produce_line] 服务对象接口
 */
public interface IMrp_product_produce_lineService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_product_produce_line get(Integer key) ;
    boolean update(Mrp_product_produce_line et) ;
    void updateBatch(List<Mrp_product_produce_line> list) ;
    Mrp_product_produce_line getDraft(Mrp_product_produce_line et) ;
    boolean create(Mrp_product_produce_line et) ;
    void createBatch(List<Mrp_product_produce_line> list) ;
    Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context) ;

}




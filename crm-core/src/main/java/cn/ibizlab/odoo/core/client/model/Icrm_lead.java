package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [crm_lead] 对象
 */
public interface Icrm_lead {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [城市]
     */
    public void setCity(String city);
    
    /**
     * 设置 [城市]
     */
    public String getCity();

    /**
     * 获取 [城市]脏标记
     */
    public boolean getCityDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCompany_currency(Integer company_currency);
    
    /**
     * 设置 [币种]
     */
    public Integer getCompany_currency();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCompany_currencyDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [联系人姓名]
     */
    public void setContact_name(String contact_name);
    
    /**
     * 设置 [联系人姓名]
     */
    public String getContact_name();

    /**
     * 获取 [联系人姓名]脏标记
     */
    public boolean getContact_nameDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [最近行动]
     */
    public void setDate_action_last(Timestamp date_action_last);
    
    /**
     * 设置 [最近行动]
     */
    public Timestamp getDate_action_last();

    /**
     * 获取 [最近行动]脏标记
     */
    public boolean getDate_action_lastDirtyFlag();
    /**
     * 获取 [关闭日期]
     */
    public void setDate_closed(Timestamp date_closed);
    
    /**
     * 设置 [关闭日期]
     */
    public Timestamp getDate_closed();

    /**
     * 获取 [关闭日期]脏标记
     */
    public boolean getDate_closedDirtyFlag();
    /**
     * 获取 [转换日期]
     */
    public void setDate_conversion(Timestamp date_conversion);
    
    /**
     * 设置 [转换日期]
     */
    public Timestamp getDate_conversion();

    /**
     * 获取 [转换日期]脏标记
     */
    public boolean getDate_conversionDirtyFlag();
    /**
     * 获取 [预期结束]
     */
    public void setDate_deadline(Timestamp date_deadline);
    
    /**
     * 设置 [预期结束]
     */
    public Timestamp getDate_deadline();

    /**
     * 获取 [预期结束]脏标记
     */
    public boolean getDate_deadlineDirtyFlag();
    /**
     * 获取 [最后阶段更新]
     */
    public void setDate_last_stage_update(Timestamp date_last_stage_update);
    
    /**
     * 设置 [最后阶段更新]
     */
    public Timestamp getDate_last_stage_update();

    /**
     * 获取 [最后阶段更新]脏标记
     */
    public boolean getDate_last_stage_updateDirtyFlag();
    /**
     * 获取 [分配日期]
     */
    public void setDate_open(Timestamp date_open);
    
    /**
     * 设置 [分配日期]
     */
    public Timestamp getDate_open();

    /**
     * 获取 [分配日期]脏标记
     */
    public boolean getDate_openDirtyFlag();
    /**
     * 获取 [关闭日期]
     */
    public void setDay_close(Double day_close);
    
    /**
     * 设置 [关闭日期]
     */
    public Double getDay_close();

    /**
     * 获取 [关闭日期]脏标记
     */
    public boolean getDay_closeDirtyFlag();
    /**
     * 获取 [分配天数]
     */
    public void setDay_open(Double day_open);
    
    /**
     * 设置 [分配天数]
     */
    public Double getDay_open();

    /**
     * 获取 [分配天数]脏标记
     */
    public boolean getDay_openDirtyFlag();
    /**
     * 获取 [便签]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [便签]
     */
    public String getDescription();

    /**
     * 获取 [便签]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [全局抄送]
     */
    public void setEmail_cc(String email_cc);
    
    /**
     * 设置 [全局抄送]
     */
    public String getEmail_cc();

    /**
     * 获取 [全局抄送]脏标记
     */
    public boolean getEmail_ccDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail_from();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [按比例分摊收入]
     */
    public void setExpected_revenue(Double expected_revenue);
    
    /**
     * 设置 [按比例分摊收入]
     */
    public Double getExpected_revenue();

    /**
     * 获取 [按比例分摊收入]脏标记
     */
    public boolean getExpected_revenueDirtyFlag();
    /**
     * 获取 [工作岗位]
     */
    public void setIbizfunction(String ibizfunction);
    
    /**
     * 设置 [工作岗位]
     */
    public String getIbizfunction();

    /**
     * 获取 [工作岗位]脏标记
     */
    public boolean getIbizfunctionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [黑名单]
     */
    public void setIs_blacklisted(String is_blacklisted);
    
    /**
     * 设置 [黑名单]
     */
    public String getIs_blacklisted();

    /**
     * 获取 [黑名单]脏标记
     */
    public boolean getIs_blacklistedDirtyFlag();
    /**
     * 获取 [看板状态]
     */
    public void setKanban_state(String kanban_state);
    
    /**
     * 设置 [看板状态]
     */
    public String getKanban_state();

    /**
     * 获取 [看板状态]脏标记
     */
    public boolean getKanban_stateDirtyFlag();
    /**
     * 获取 [失去原因]
     */
    public void setLost_reason(Integer lost_reason);
    
    /**
     * 设置 [失去原因]
     */
    public Integer getLost_reason();

    /**
     * 获取 [失去原因]脏标记
     */
    public boolean getLost_reasonDirtyFlag();
    /**
     * 获取 [失去原因]
     */
    public void setLost_reason_text(String lost_reason_text);
    
    /**
     * 设置 [失去原因]
     */
    public String getLost_reason_text();

    /**
     * 获取 [失去原因]脏标记
     */
    public boolean getLost_reason_textDirtyFlag();
    /**
     * 获取 [媒介]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒介]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒介]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒介]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒介]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒介]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [#会议]
     */
    public void setMeeting_count(Integer meeting_count);
    
    /**
     * 设置 [#会议]
     */
    public Integer getMeeting_count();

    /**
     * 获取 [#会议]脏标记
     */
    public boolean getMeeting_countDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [退回]
     */
    public void setMessage_bounce(Integer message_bounce);
    
    /**
     * 设置 [退回]
     */
    public Integer getMessage_bounce();

    /**
     * 获取 [退回]脏标记
     */
    public boolean getMessage_bounceDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [手机]
     */
    public void setMobile(String mobile);
    
    /**
     * 设置 [手机]
     */
    public String getMobile();

    /**
     * 获取 [手机]脏标记
     */
    public boolean getMobileDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setName(String name);
    
    /**
     * 设置 [商机]
     */
    public String getName();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [订单]
     */
    public void setOrder_ids(String order_ids);
    
    /**
     * 设置 [订单]
     */
    public String getOrder_ids();

    /**
     * 获取 [订单]脏标记
     */
    public boolean getOrder_idsDirtyFlag();
    /**
     * 获取 [业务伙伴联系EMail]
     */
    public void setPartner_address_email(String partner_address_email);
    
    /**
     * 设置 [业务伙伴联系EMail]
     */
    public String getPartner_address_email();

    /**
     * 获取 [业务伙伴联系EMail]脏标记
     */
    public boolean getPartner_address_emailDirtyFlag();
    /**
     * 获取 [Partner Contact Mobile]
     */
    public void setPartner_address_mobile(String partner_address_mobile);
    
    /**
     * 设置 [Partner Contact Mobile]
     */
    public String getPartner_address_mobile();

    /**
     * 获取 [Partner Contact Mobile]脏标记
     */
    public boolean getPartner_address_mobileDirtyFlag();
    /**
     * 获取 [业务伙伴联系姓名]
     */
    public void setPartner_address_name(String partner_address_name);
    
    /**
     * 设置 [业务伙伴联系姓名]
     */
    public String getPartner_address_name();

    /**
     * 获取 [业务伙伴联系姓名]脏标记
     */
    public boolean getPartner_address_nameDirtyFlag();
    /**
     * 获取 [合作伙伴联系电话]
     */
    public void setPartner_address_phone(String partner_address_phone);
    
    /**
     * 设置 [合作伙伴联系电话]
     */
    public String getPartner_address_phone();

    /**
     * 获取 [合作伙伴联系电话]脏标记
     */
    public boolean getPartner_address_phoneDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [合作伙伴黑名单]
     */
    public void setPartner_is_blacklisted(String partner_is_blacklisted);
    
    /**
     * 设置 [合作伙伴黑名单]
     */
    public String getPartner_is_blacklisted();

    /**
     * 获取 [合作伙伴黑名单]脏标记
     */
    public boolean getPartner_is_blacklistedDirtyFlag();
    /**
     * 获取 [客户名称]
     */
    public void setPartner_name(String partner_name);
    
    /**
     * 设置 [客户名称]
     */
    public String getPartner_name();

    /**
     * 获取 [客户名称]脏标记
     */
    public boolean getPartner_nameDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPhone(String phone);
    
    /**
     * 设置 [电话]
     */
    public String getPhone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPhoneDirtyFlag();
    /**
     * 获取 [预期收益]
     */
    public void setPlanned_revenue(Double planned_revenue);
    
    /**
     * 设置 [预期收益]
     */
    public Double getPlanned_revenue();

    /**
     * 获取 [预期收益]脏标记
     */
    public boolean getPlanned_revenueDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [概率]
     */
    public void setProbability(Double probability);
    
    /**
     * 设置 [概率]
     */
    public Double getProbability();

    /**
     * 获取 [概率]脏标记
     */
    public boolean getProbabilityDirtyFlag();
    /**
     * 获取 [引荐于]
     */
    public void setReferred(String referred);
    
    /**
     * 设置 [引荐于]
     */
    public String getReferred();

    /**
     * 获取 [引荐于]脏标记
     */
    public boolean getReferredDirtyFlag();
    /**
     * 获取 [销售订单的总数]
     */
    public void setSale_amount_total(Double sale_amount_total);
    
    /**
     * 设置 [销售订单的总数]
     */
    public Double getSale_amount_total();

    /**
     * 获取 [销售订单的总数]脏标记
     */
    public boolean getSale_amount_totalDirtyFlag();
    /**
     * 获取 [报价单的数量]
     */
    public void setSale_number(Integer sale_number);
    
    /**
     * 设置 [报价单的数量]
     */
    public Integer getSale_number();

    /**
     * 获取 [报价单的数量]脏标记
     */
    public boolean getSale_numberDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [省份]
     */
    public void setState_id(Integer state_id);
    
    /**
     * 设置 [省份]
     */
    public Integer getState_id();

    /**
     * 获取 [省份]脏标记
     */
    public boolean getState_idDirtyFlag();
    /**
     * 获取 [省份]
     */
    public void setState_id_text(String state_id_text);
    
    /**
     * 设置 [省份]
     */
    public String getState_id_text();

    /**
     * 获取 [省份]脏标记
     */
    public boolean getState_id_textDirtyFlag();
    /**
     * 获取 [街道]
     */
    public void setStreet(String street);
    
    /**
     * 设置 [街道]
     */
    public String getStreet();

    /**
     * 获取 [街道]脏标记
     */
    public boolean getStreetDirtyFlag();
    /**
     * 获取 [街道 2]
     */
    public void setStreet2(String street2);
    
    /**
     * 设置 [街道 2]
     */
    public String getStreet2();

    /**
     * 获取 [街道 2]脏标记
     */
    public boolean getStreet2DirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [标签]
     */
    public String getTag_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setTitle(Integer title);
    
    /**
     * 设置 [称谓]
     */
    public Integer getTitle();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getTitleDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setTitle_text(String title_text);
    
    /**
     * 设置 [称谓]
     */
    public String getTitle_text();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getTitle_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [用户EMail]
     */
    public void setUser_email(String user_email);
    
    /**
     * 设置 [用户EMail]
     */
    public String getUser_email();

    /**
     * 获取 [用户EMail]脏标记
     */
    public boolean getUser_emailDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [用户 登录]
     */
    public void setUser_login(String user_login);
    
    /**
     * 设置 [用户 登录]
     */
    public String getUser_login();

    /**
     * 获取 [用户 登录]脏标记
     */
    public boolean getUser_loginDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite(String website);
    
    /**
     * 设置 [网站]
     */
    public String getWebsite();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsiteDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [邮政编码]
     */
    public void setZip(String zip);
    
    /**
     * 设置 [邮政编码]
     */
    public String getZip();

    /**
     * 获取 [邮政编码]脏标记
     */
    public boolean getZipDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

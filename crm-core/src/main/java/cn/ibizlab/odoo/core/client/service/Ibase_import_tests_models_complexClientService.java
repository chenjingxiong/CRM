package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_complex;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
public interface Ibase_import_tests_models_complexClientService{

    public Ibase_import_tests_models_complex createModel() ;

    public void update(Ibase_import_tests_models_complex base_import_tests_models_complex);

    public Page<Ibase_import_tests_models_complex> fetchDefault(SearchContext context);

    public void createBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices);

    public void remove(Ibase_import_tests_models_complex base_import_tests_models_complex);

    public void get(Ibase_import_tests_models_complex base_import_tests_models_complex);

    public void create(Ibase_import_tests_models_complex base_import_tests_models_complex);

    public void updateBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices);

    public void removeBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices);

    public Page<Ibase_import_tests_models_complex> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_complex base_import_tests_models_complex);

}

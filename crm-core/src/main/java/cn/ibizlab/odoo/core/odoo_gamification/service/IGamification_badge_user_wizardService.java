package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;


/**
 * 实体[Gamification_badge_user_wizard] 服务对象接口
 */
public interface IGamification_badge_user_wizardService{

    boolean update(Gamification_badge_user_wizard et) ;
    void updateBatch(List<Gamification_badge_user_wizard> list) ;
    Gamification_badge_user_wizard get(Integer key) ;
    boolean create(Gamification_badge_user_wizard et) ;
    void createBatch(List<Gamification_badge_user_wizard> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Gamification_badge_user_wizard getDraft(Gamification_badge_user_wizard et) ;
    Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context) ;

}




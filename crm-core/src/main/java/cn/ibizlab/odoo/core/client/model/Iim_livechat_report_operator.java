package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [im_livechat_report_operator] 对象
 */
public interface Iim_livechat_report_operator {

    /**
     * 获取 [对话]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [对话]
     */
    public Integer getChannel_id();

    /**
     * 获取 [对话]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [对话]
     */
    public void setChannel_id_text(String channel_id_text);
    
    /**
     * 设置 [对话]
     */
    public String getChannel_id_text();

    /**
     * 获取 [对话]脏标记
     */
    public boolean getChannel_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [平均时间]
     */
    public void setDuration(Double duration);
    
    /**
     * 设置 [平均时间]
     */
    public Double getDuration();

    /**
     * 获取 [平均时间]脏标记
     */
    public boolean getDurationDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id(Integer livechat_channel_id);
    
    /**
     * 设置 [渠道]
     */
    public Integer getLivechat_channel_id();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id_text(String livechat_channel_id_text);
    
    /**
     * 设置 [渠道]
     */
    public String getLivechat_channel_id_text();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_id_textDirtyFlag();
    /**
     * 获取 [# 会话]
     */
    public void setNbr_channel(Integer nbr_channel);
    
    /**
     * 设置 [# 会话]
     */
    public Integer getNbr_channel();

    /**
     * 获取 [# 会话]脏标记
     */
    public boolean getNbr_channelDirtyFlag();
    /**
     * 获取 [运算符]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [运算符]
     */
    public Integer getPartner_id();

    /**
     * 获取 [运算符]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [运算符]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [运算符]
     */
    public String getPartner_id_text();

    /**
     * 获取 [运算符]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [会话的开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [会话的开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [会话的开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [该回答了]
     */
    public void setTime_to_answer(Double time_to_answer);
    
    /**
     * 设置 [该回答了]
     */
    public Double getTime_to_answer();

    /**
     * 获取 [该回答了]脏标记
     */
    public boolean getTime_to_answerDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

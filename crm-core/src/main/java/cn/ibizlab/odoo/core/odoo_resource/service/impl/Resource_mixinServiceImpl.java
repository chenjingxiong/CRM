package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_mixinFeignClient;

/**
 * 实体[资源装饰] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_mixinServiceImpl implements IResource_mixinService {

    @Autowired
    resource_mixinFeignClient resource_mixinFeignClient;


    @Override
    public boolean update(Resource_mixin et) {
        Resource_mixin rt = resource_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_mixin> list){
        resource_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=resource_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Resource_mixin getDraft(Resource_mixin et) {
        et=resource_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public Resource_mixin get(Integer id) {
		Resource_mixin et=resource_mixinFeignClient.get(id);
        if(et==null){
            et=new Resource_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Resource_mixin et) {
        Resource_mixin rt = resource_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_mixin> list){
        resource_mixinFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context) {
        Page<Resource_mixin> resource_mixins=resource_mixinFeignClient.searchDefault(context);
        return resource_mixins;
    }


}



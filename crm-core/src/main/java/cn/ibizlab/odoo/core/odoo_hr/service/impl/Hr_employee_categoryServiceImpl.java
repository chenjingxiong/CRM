package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employee_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_employee_categoryFeignClient;

/**
 * 实体[员工类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_employee_categoryServiceImpl implements IHr_employee_categoryService {

    @Autowired
    hr_employee_categoryFeignClient hr_employee_categoryFeignClient;


    @Override
    public boolean create(Hr_employee_category et) {
        Hr_employee_category rt = hr_employee_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_employee_category> list){
        hr_employee_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_employee_category getDraft(Hr_employee_category et) {
        et=hr_employee_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_employee_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_employee_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_employee_category et) {
        Hr_employee_category rt = hr_employee_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_employee_category> list){
        hr_employee_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_employee_category get(Integer id) {
		Hr_employee_category et=hr_employee_categoryFeignClient.get(id);
        if(et==null){
            et=new Hr_employee_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context) {
        Page<Hr_employee_category> hr_employee_categorys=hr_employee_categoryFeignClient.searchDefault(context);
        return hr_employee_categorys;
    }


}



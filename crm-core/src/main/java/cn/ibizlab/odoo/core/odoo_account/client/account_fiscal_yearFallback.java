package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_fiscal_year] 服务对象接口
 */
@Component
public class account_fiscal_yearFallback implements account_fiscal_yearFeignClient{


    public Page<Account_fiscal_year> searchDefault(Account_fiscal_yearSearchContext context){
            return null;
     }



    public Account_fiscal_year get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Account_fiscal_year update(Integer id, Account_fiscal_year account_fiscal_year){
            return null;
     }
    public Boolean updateBatch(List<Account_fiscal_year> account_fiscal_years){
            return false;
     }


    public Account_fiscal_year create(Account_fiscal_year account_fiscal_year){
            return null;
     }
    public Boolean createBatch(List<Account_fiscal_year> account_fiscal_years){
            return false;
     }

    public Page<Account_fiscal_year> select(){
            return null;
     }

    public Account_fiscal_year getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_template;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_template.Product_templateEvent_okDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_template
 * 属性：Event_ok
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_templateEvent_okDefaultValidator.class})
public @interface Product_templateEvent_okDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

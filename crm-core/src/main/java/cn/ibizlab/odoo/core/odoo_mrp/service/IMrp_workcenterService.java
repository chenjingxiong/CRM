package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;


/**
 * 实体[Mrp_workcenter] 服务对象接口
 */
public interface IMrp_workcenterService{

    Mrp_workcenter getDraft(Mrp_workcenter et) ;
    boolean create(Mrp_workcenter et) ;
    void createBatch(List<Mrp_workcenter> list) ;
    Mrp_workcenter get(Integer key) ;
    boolean update(Mrp_workcenter et) ;
    void updateBatch(List<Mrp_workcenter> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context) ;

}




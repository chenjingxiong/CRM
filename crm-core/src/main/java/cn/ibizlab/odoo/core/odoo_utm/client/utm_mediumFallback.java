package cn.ibizlab.odoo.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[utm_medium] 服务对象接口
 */
@Component
public class utm_mediumFallback implements utm_mediumFeignClient{



    public Utm_medium create(Utm_medium utm_medium){
            return null;
     }
    public Boolean createBatch(List<Utm_medium> utm_media){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Utm_medium update(Integer id, Utm_medium utm_medium){
            return null;
     }
    public Boolean updateBatch(List<Utm_medium> utm_media){
            return false;
     }


    public Utm_medium get(Integer id){
            return null;
     }


    public Page<Utm_medium> searchDefault(Utm_mediumSearchContext context){
            return null;
     }


    public Page<Utm_medium> select(){
            return null;
     }

    public Utm_medium getDraft(){
            return null;
    }



}

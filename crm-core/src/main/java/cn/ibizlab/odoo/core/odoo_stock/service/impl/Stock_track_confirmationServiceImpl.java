package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_confirmationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_track_confirmationFeignClient;

/**
 * 实体[库存追溯确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_track_confirmationServiceImpl implements IStock_track_confirmationService {

    @Autowired
    stock_track_confirmationFeignClient stock_track_confirmationFeignClient;


    @Override
    public boolean create(Stock_track_confirmation et) {
        Stock_track_confirmation rt = stock_track_confirmationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_track_confirmation> list){
        stock_track_confirmationFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_track_confirmation get(Integer id) {
		Stock_track_confirmation et=stock_track_confirmationFeignClient.get(id);
        if(et==null){
            et=new Stock_track_confirmation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_track_confirmationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_track_confirmationFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_track_confirmation getDraft(Stock_track_confirmation et) {
        et=stock_track_confirmationFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_track_confirmation et) {
        Stock_track_confirmation rt = stock_track_confirmationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_track_confirmation> list){
        stock_track_confirmationFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context) {
        Page<Stock_track_confirmation> stock_track_confirmations=stock_track_confirmationFeignClient.searchDefault(context);
        return stock_track_confirmations;
    }


}



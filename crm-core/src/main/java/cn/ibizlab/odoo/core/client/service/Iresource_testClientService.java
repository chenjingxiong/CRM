package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iresource_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_test] 服务对象接口
 */
public interface Iresource_testClientService{

    public Iresource_test createModel() ;

    public void remove(Iresource_test resource_test);

    public void createBatch(List<Iresource_test> resource_tests);

    public void create(Iresource_test resource_test);

    public Page<Iresource_test> fetchDefault(SearchContext context);

    public void update(Iresource_test resource_test);

    public void get(Iresource_test resource_test);

    public void removeBatch(List<Iresource_test> resource_tests);

    public void updateBatch(List<Iresource_test> resource_tests);

    public Page<Iresource_test> select(SearchContext context);

    public void getDraft(Iresource_test resource_test);

}

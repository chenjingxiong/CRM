package cn.ibizlab.odoo.core.odoo_digest.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_digestService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_digest.client.digest_digestFeignClient;

/**
 * 实体[摘要] 服务对象接口实现
 */
@Slf4j
@Service
public class Digest_digestServiceImpl implements IDigest_digestService {

    @Autowired
    digest_digestFeignClient digest_digestFeignClient;


    @Override
    public boolean create(Digest_digest et) {
        Digest_digest rt = digest_digestFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Digest_digest> list){
        digest_digestFeignClient.createBatch(list) ;
    }

    @Override
    public Digest_digest get(Integer id) {
		Digest_digest et=digest_digestFeignClient.get(id);
        if(et==null){
            et=new Digest_digest();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Digest_digest getDraft(Digest_digest et) {
        et=digest_digestFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=digest_digestFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        digest_digestFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Digest_digest et) {
        Digest_digest rt = digest_digestFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Digest_digest> list){
        digest_digestFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Digest_digest> searchDefault(Digest_digestSearchContext context) {
        Page<Digest_digest> digest_digests=digest_digestFeignClient.searchDefault(context);
        return digest_digests;
    }


}



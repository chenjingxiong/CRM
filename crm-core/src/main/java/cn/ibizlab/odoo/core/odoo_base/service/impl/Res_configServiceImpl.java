package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_configService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_configFeignClient;

/**
 * 实体[配置] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_configServiceImpl implements IRes_configService {

    @Autowired
    res_configFeignClient res_configFeignClient;


    @Override
    public Res_config get(Integer id) {
		Res_config et=res_configFeignClient.get(id);
        if(et==null){
            et=new Res_config();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_configFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_configFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Res_config et) {
        Res_config rt = res_configFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_config> list){
        res_configFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_config et) {
        Res_config rt = res_configFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_config> list){
        res_configFeignClient.createBatch(list) ;
    }

    @Override
    public Res_config getDraft(Res_config et) {
        et=res_configFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_config> searchDefault(Res_configSearchContext context) {
        Page<Res_config> res_configs=res_configFeignClient.searchDefault(context);
        return res_configs;
    }


}



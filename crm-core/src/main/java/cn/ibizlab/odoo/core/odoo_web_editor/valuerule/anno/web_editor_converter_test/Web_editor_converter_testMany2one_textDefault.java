package cn.ibizlab.odoo.core.odoo_web_editor.valuerule.anno.web_editor_converter_test;

import cn.ibizlab.odoo.core.odoo_web_editor.valuerule.validator.web_editor_converter_test.Web_editor_converter_testMany2one_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Web_editor_converter_test
 * 属性：Many2one_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Web_editor_converter_testMany2one_textDefaultValidator.class})
public @interface Web_editor_converter_testMany2one_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

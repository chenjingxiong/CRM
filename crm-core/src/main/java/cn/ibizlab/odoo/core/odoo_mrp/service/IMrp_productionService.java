package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;


/**
 * 实体[Mrp_production] 服务对象接口
 */
public interface IMrp_productionService{

    boolean create(Mrp_production et) ;
    void createBatch(List<Mrp_production> list) ;
    boolean update(Mrp_production et) ;
    void updateBatch(List<Mrp_production> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_production get(Integer key) ;
    Mrp_production getDraft(Mrp_production et) ;
    Page<Mrp_production> searchDefault(Mrp_productionSearchContext context) ;

}




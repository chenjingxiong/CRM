package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_groups;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_groups] 服务对象接口
 */
public interface Ires_groupsClientService{

    public Ires_groups createModel() ;

    public void removeBatch(List<Ires_groups> res_groups);

    public void remove(Ires_groups res_groups);

    public void create(Ires_groups res_groups);

    public void update(Ires_groups res_groups);

    public void createBatch(List<Ires_groups> res_groups);

    public void get(Ires_groups res_groups);

    public void updateBatch(List<Ires_groups> res_groups);

    public Page<Ires_groups> fetchDefault(SearchContext context);

    public Page<Ires_groups> select(SearchContext context);

    public void getDraft(Ires_groups res_groups);

}

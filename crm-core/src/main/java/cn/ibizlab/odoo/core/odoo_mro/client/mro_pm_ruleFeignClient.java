package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_rule] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-pm-rule", fallback = mro_pm_ruleFallback.class)
public interface mro_pm_ruleFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules")
    Mro_pm_rule create(@RequestBody Mro_pm_rule mro_pm_rule);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_rule> mro_pm_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/{id}")
    Mro_pm_rule get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/{id}")
    Mro_pm_rule update(@PathVariable("id") Integer id,@RequestBody Mro_pm_rule mro_pm_rule);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_rule> mro_pm_rules);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/searchdefault")
    Page<Mro_pm_rule> searchDefault(@RequestBody Mro_pm_ruleSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/select")
    Page<Mro_pm_rule> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/getdraft")
    Mro_pm_rule getDraft();


}

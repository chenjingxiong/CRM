package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_m2o_required] 服务对象接口
 */
public interface Ibase_import_tests_models_m2o_requiredClientService{

    public Ibase_import_tests_models_m2o_required createModel() ;

    public void updateBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds);

    public void create(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    public void removeBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds);

    public void remove(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    public Page<Ibase_import_tests_models_m2o_required> fetchDefault(SearchContext context);

    public void createBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds);

    public void update(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    public void get(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    public Page<Ibase_import_tests_models_m2o_required> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required);

}

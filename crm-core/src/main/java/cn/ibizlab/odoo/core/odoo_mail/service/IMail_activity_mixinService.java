package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;


/**
 * 实体[Mail_activity_mixin] 服务对象接口
 */
public interface IMail_activity_mixinService{

    Mail_activity_mixin get(Integer key) ;
    boolean update(Mail_activity_mixin et) ;
    void updateBatch(List<Mail_activity_mixin> list) ;
    boolean create(Mail_activity_mixin et) ;
    void createBatch(List<Mail_activity_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_activity_mixin getDraft(Mail_activity_mixin et) ;
    Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context) ;

}




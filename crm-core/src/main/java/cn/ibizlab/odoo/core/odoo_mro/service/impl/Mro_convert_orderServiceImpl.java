package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_convert_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_convert_orderFeignClient;

/**
 * 实体[Convert Order to Task] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_convert_orderServiceImpl implements IMro_convert_orderService {

    @Autowired
    mro_convert_orderFeignClient mro_convert_orderFeignClient;


    @Override
    public boolean create(Mro_convert_order et) {
        Mro_convert_order rt = mro_convert_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_convert_order> list){
        mro_convert_orderFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_convert_order get(Integer id) {
		Mro_convert_order et=mro_convert_orderFeignClient.get(id);
        if(et==null){
            et=new Mro_convert_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mro_convert_order et) {
        Mro_convert_order rt = mro_convert_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_convert_order> list){
        mro_convert_orderFeignClient.updateBatch(list) ;
    }

    @Override
    public Mro_convert_order getDraft(Mro_convert_order et) {
        et=mro_convert_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_convert_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_convert_orderFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context) {
        Page<Mro_convert_order> mro_convert_orders=mro_convert_orderFeignClient.searchDefault(context);
        return mro_convert_orders;
    }


}



package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_payment_method;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_payment_method.Account_payment_methodPayment_typeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_payment_method
 * 属性：Payment_type
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_payment_methodPayment_typeDefaultValidator.class})
public @interface Account_payment_methodPayment_typeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

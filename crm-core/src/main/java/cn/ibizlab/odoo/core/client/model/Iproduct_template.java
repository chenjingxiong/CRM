package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_template] 对象
 */
public interface Iproduct_template {

    /**
     * 获取 [附件产品]
     */
    public void setAccessory_product_ids(String accessory_product_ids);
    
    /**
     * 设置 [附件产品]
     */
    public String getAccessory_product_ids();

    /**
     * 获取 [附件产品]脏标记
     */
    public boolean getAccessory_product_idsDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [替代产品]
     */
    public void setAlternative_product_ids(String alternative_product_ids);
    
    /**
     * 设置 [替代产品]
     */
    public String getAlternative_product_ids();

    /**
     * 获取 [替代产品]脏标记
     */
    public boolean getAlternative_product_idsDirtyFlag();
    /**
     * 获取 [产品属性]
     */
    public void setAttribute_line_ids(String attribute_line_ids);
    
    /**
     * 设置 [产品属性]
     */
    public String getAttribute_line_ids();

    /**
     * 获取 [产品属性]脏标记
     */
    public boolean getAttribute_line_idsDirtyFlag();
    /**
     * 获取 [POS可用]
     */
    public void setAvailable_in_pos(String available_in_pos);
    
    /**
     * 设置 [POS可用]
     */
    public String getAvailable_in_pos();

    /**
     * 获取 [POS可用]脏标记
     */
    public boolean getAvailable_in_posDirtyFlag();
    /**
     * 获取 [可用阈值]
     */
    public void setAvailable_threshold(Double available_threshold);
    
    /**
     * 设置 [可用阈值]
     */
    public Double getAvailable_threshold();

    /**
     * 获取 [可用阈值]脏标记
     */
    public boolean getAvailable_thresholdDirtyFlag();
    /**
     * 获取 [条码]
     */
    public void setBarcode(String barcode);
    
    /**
     * 设置 [条码]
     */
    public String getBarcode();

    /**
     * 获取 [条码]脏标记
     */
    public boolean getBarcodeDirtyFlag();
    /**
     * 获取 [# 物料清单]
     */
    public void setBom_count(Integer bom_count);
    
    /**
     * 设置 [# 物料清单]
     */
    public Integer getBom_count();

    /**
     * 获取 [# 物料清单]脏标记
     */
    public boolean getBom_countDirtyFlag();
    /**
     * 获取 [物料清单]
     */
    public void setBom_ids(String bom_ids);
    
    /**
     * 设置 [物料清单]
     */
    public String getBom_ids();

    /**
     * 获取 [物料清单]脏标记
     */
    public boolean getBom_idsDirtyFlag();
    /**
     * 获取 [BOM组件]
     */
    public void setBom_line_ids(String bom_line_ids);
    
    /**
     * 设置 [BOM组件]
     */
    public String getBom_line_ids();

    /**
     * 获取 [BOM组件]脏标记
     */
    public boolean getBom_line_idsDirtyFlag();
    /**
     * 获取 [报销]
     */
    public void setCan_be_expensed(String can_be_expensed);
    
    /**
     * 设置 [报销]
     */
    public String getCan_be_expensed();

    /**
     * 获取 [报销]脏标记
     */
    public boolean getCan_be_expensedDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id(Integer categ_id);
    
    /**
     * 设置 [产品种类]
     */
    public Integer getCateg_id();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_idDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id_text(String categ_id_text);
    
    /**
     * 设置 [产品种类]
     */
    public String getCateg_id_text();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_id_textDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [成本币种]
     */
    public void setCost_currency_id(Integer cost_currency_id);
    
    /**
     * 设置 [成本币种]
     */
    public Integer getCost_currency_id();

    /**
     * 获取 [成本币种]脏标记
     */
    public boolean getCost_currency_idDirtyFlag();
    /**
     * 获取 [成本方法]
     */
    public void setCost_method(String cost_method);
    
    /**
     * 设置 [成本方法]
     */
    public String getCost_method();

    /**
     * 获取 [成本方法]脏标记
     */
    public boolean getCost_methodDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [自定义消息]
     */
    public void setCustom_message(String custom_message);
    
    /**
     * 设置 [自定义消息]
     */
    public String getCustom_message();

    /**
     * 获取 [自定义消息]脏标记
     */
    public boolean getCustom_messageDirtyFlag();
    /**
     * 获取 [内部参考]
     */
    public void setDefault_code(String default_code);
    
    /**
     * 设置 [内部参考]
     */
    public String getDefault_code();

    /**
     * 获取 [内部参考]脏标记
     */
    public boolean getDefault_codeDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [拣货说明]
     */
    public void setDescription_picking(String description_picking);
    
    /**
     * 设置 [拣货说明]
     */
    public String getDescription_picking();

    /**
     * 获取 [拣货说明]脏标记
     */
    public boolean getDescription_pickingDirtyFlag();
    /**
     * 获取 [收货说明]
     */
    public void setDescription_pickingin(String description_pickingin);
    
    /**
     * 设置 [收货说明]
     */
    public String getDescription_pickingin();

    /**
     * 获取 [收货说明]脏标记
     */
    public boolean getDescription_pickinginDirtyFlag();
    /**
     * 获取 [出库单说明]
     */
    public void setDescription_pickingout(String description_pickingout);
    
    /**
     * 设置 [出库单说明]
     */
    public String getDescription_pickingout();

    /**
     * 获取 [出库单说明]脏标记
     */
    public boolean getDescription_pickingoutDirtyFlag();
    /**
     * 获取 [采购说明]
     */
    public void setDescription_purchase(String description_purchase);
    
    /**
     * 设置 [采购说明]
     */
    public String getDescription_purchase();

    /**
     * 获取 [采购说明]脏标记
     */
    public boolean getDescription_purchaseDirtyFlag();
    /**
     * 获取 [销售说明]
     */
    public void setDescription_sale(String description_sale);
    
    /**
     * 设置 [销售说明]
     */
    public String getDescription_sale();

    /**
     * 获取 [销售说明]脏标记
     */
    public boolean getDescription_saleDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [是一张活动票吗？]
     */
    public void setEvent_ok(String event_ok);
    
    /**
     * 设置 [是一张活动票吗？]
     */
    public String getEvent_ok();

    /**
     * 获取 [是一张活动票吗？]脏标记
     */
    public boolean getEvent_okDirtyFlag();
    /**
     * 获取 [重开收据规则]
     */
    public void setExpense_policy(String expense_policy);
    
    /**
     * 设置 [重开收据规则]
     */
    public String getExpense_policy();

    /**
     * 获取 [重开收据规则]脏标记
     */
    public boolean getExpense_policyDirtyFlag();
    /**
     * 获取 [隐藏费用政策]
     */
    public void setHide_expense_policy(String hide_expense_policy);
    
    /**
     * 设置 [隐藏费用政策]
     */
    public String getHide_expense_policy();

    /**
     * 获取 [隐藏费用政策]脏标记
     */
    public boolean getHide_expense_policyDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸图像]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸图像]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸图像]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸图像]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸图像]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [入库]
     */
    public void setIncoming_qty(Double incoming_qty);
    
    /**
     * 设置 [入库]
     */
    public Double getIncoming_qty();

    /**
     * 获取 [入库]脏标记
     */
    public boolean getIncoming_qtyDirtyFlag();
    /**
     * 获取 [库存可用性]
     */
    public void setInventory_availability(String inventory_availability);
    
    /**
     * 设置 [库存可用性]
     */
    public String getInventory_availability();

    /**
     * 获取 [库存可用性]脏标记
     */
    public boolean getInventory_availabilityDirtyFlag();
    /**
     * 获取 [开票策略]
     */
    public void setInvoice_policy(String invoice_policy);
    
    /**
     * 设置 [开票策略]
     */
    public String getInvoice_policy();

    /**
     * 获取 [开票策略]脏标记
     */
    public boolean getInvoice_policyDirtyFlag();
    /**
     * 获取 [Can be Part]
     */
    public void setIsParts(String isParts);
    
    /**
     * 设置 [Can be Part]
     */
    public String getIsParts();

    /**
     * 获取 [Can be Part]脏标记
     */
    public boolean getIsPartsDirtyFlag();
    /**
     * 获取 [是产品变体]
     */
    public void setIs_product_variant(String is_product_variant);
    
    /**
     * 设置 [是产品变体]
     */
    public String getIs_product_variant();

    /**
     * 获取 [是产品变体]脏标记
     */
    public boolean getIs_product_variantDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [SEO优化]
     */
    public void setIs_seo_optimized(String is_seo_optimized);
    
    /**
     * 设置 [SEO优化]
     */
    public String getIs_seo_optimized();

    /**
     * 获取 [SEO优化]脏标记
     */
    public boolean getIs_seo_optimizedDirtyFlag();
    /**
     * 获取 [价格表项目]
     */
    public void setItem_ids(String item_ids);
    
    /**
     * 设置 [价格表项目]
     */
    public String getItem_ids();

    /**
     * 获取 [价格表项目]脏标记
     */
    public boolean getItem_idsDirtyFlag();
    /**
     * 获取 [销售价格]
     */
    public void setList_price(Double list_price);
    
    /**
     * 设置 [销售价格]
     */
    public Double getList_price();

    /**
     * 获取 [销售价格]脏标记
     */
    public boolean getList_priceDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [地点]
     */
    public Integer getLocation_id();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [公开价格]
     */
    public void setLst_price(Double lst_price);
    
    /**
     * 设置 [公开价格]
     */
    public Double getLst_price();

    /**
     * 获取 [公开价格]脏标记
     */
    public boolean getLst_priceDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [动作数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [动作数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [动作数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [已生产]
     */
    public void setMrp_product_qty(Double mrp_product_qty);
    
    /**
     * 设置 [已生产]
     */
    public Double getMrp_product_qty();

    /**
     * 获取 [已生产]脏标记
     */
    public boolean getMrp_product_qtyDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [订货规则]
     */
    public void setNbr_reordering_rules(Integer nbr_reordering_rules);
    
    /**
     * 设置 [订货规则]
     */
    public Integer getNbr_reordering_rules();

    /**
     * 获取 [订货规则]脏标记
     */
    public boolean getNbr_reordering_rulesDirtyFlag();
    /**
     * 获取 [可选产品]
     */
    public void setOptional_product_ids(String optional_product_ids);
    
    /**
     * 设置 [可选产品]
     */
    public String getOptional_product_ids();

    /**
     * 获取 [可选产品]脏标记
     */
    public boolean getOptional_product_idsDirtyFlag();
    /**
     * 获取 [出向]
     */
    public void setOutgoing_qty(Double outgoing_qty);
    
    /**
     * 设置 [出向]
     */
    public Double getOutgoing_qty();

    /**
     * 获取 [出向]脏标记
     */
    public boolean getOutgoing_qtyDirtyFlag();
    /**
     * 获取 [产品包裹]
     */
    public void setPackaging_ids(String packaging_ids);
    
    /**
     * 设置 [产品包裹]
     */
    public String getPackaging_ids();

    /**
     * 获取 [产品包裹]脏标记
     */
    public boolean getPackaging_idsDirtyFlag();
    /**
     * 获取 [POS类别]
     */
    public void setPos_categ_id(Integer pos_categ_id);
    
    /**
     * 设置 [POS类别]
     */
    public Integer getPos_categ_id();

    /**
     * 获取 [POS类别]脏标记
     */
    public boolean getPos_categ_idDirtyFlag();
    /**
     * 获取 [价格]
     */
    public void setPrice(Double price);
    
    /**
     * 设置 [价格]
     */
    public Double getPrice();

    /**
     * 获取 [价格]脏标记
     */
    public boolean getPriceDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id(Integer pricelist_id);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPricelist_id();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_idDirtyFlag();
    /**
     * 获取 [制造提前期(日)]
     */
    public void setProduce_delay(Double produce_delay);
    
    /**
     * 设置 [制造提前期(日)]
     */
    public Double getProduce_delay();

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    public boolean getProduce_delayDirtyFlag();
    /**
     * 获取 [图片]
     */
    public void setProduct_image_ids(String product_image_ids);
    
    /**
     * 设置 [图片]
     */
    public String getProduct_image_ids();

    /**
     * 获取 [图片]脏标记
     */
    public boolean getProduct_image_idsDirtyFlag();
    /**
     * 获取 [# 产品变体]
     */
    public void setProduct_variant_count(Integer product_variant_count);
    
    /**
     * 设置 [# 产品变体]
     */
    public Integer getProduct_variant_count();

    /**
     * 获取 [# 产品变体]脏标记
     */
    public boolean getProduct_variant_countDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_variant_id(Integer product_variant_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_variant_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_variant_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_variant_ids(String product_variant_ids);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_variant_ids();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_variant_idsDirtyFlag();
    /**
     * 获取 [价格差异科目]
     */
    public void setProperty_account_creditor_price_difference(Integer property_account_creditor_price_difference);
    
    /**
     * 设置 [价格差异科目]
     */
    public Integer getProperty_account_creditor_price_difference();

    /**
     * 获取 [价格差异科目]脏标记
     */
    public boolean getProperty_account_creditor_price_differenceDirtyFlag();
    /**
     * 获取 [费用科目]
     */
    public void setProperty_account_expense_id(Integer property_account_expense_id);
    
    /**
     * 设置 [费用科目]
     */
    public Integer getProperty_account_expense_id();

    /**
     * 获取 [费用科目]脏标记
     */
    public boolean getProperty_account_expense_idDirtyFlag();
    /**
     * 获取 [收入科目]
     */
    public void setProperty_account_income_id(Integer property_account_income_id);
    
    /**
     * 设置 [收入科目]
     */
    public Integer getProperty_account_income_id();

    /**
     * 获取 [收入科目]脏标记
     */
    public boolean getProperty_account_income_idDirtyFlag();
    /**
     * 获取 [成本方法]
     */
    public void setProperty_cost_method(String property_cost_method);
    
    /**
     * 设置 [成本方法]
     */
    public String getProperty_cost_method();

    /**
     * 获取 [成本方法]脏标记
     */
    public boolean getProperty_cost_methodDirtyFlag();
    /**
     * 获取 [库存进货科目]
     */
    public void setProperty_stock_account_input(Integer property_stock_account_input);
    
    /**
     * 设置 [库存进货科目]
     */
    public Integer getProperty_stock_account_input();

    /**
     * 获取 [库存进货科目]脏标记
     */
    public boolean getProperty_stock_account_inputDirtyFlag();
    /**
     * 获取 [库存出货科目]
     */
    public void setProperty_stock_account_output(Integer property_stock_account_output);
    
    /**
     * 设置 [库存出货科目]
     */
    public Integer getProperty_stock_account_output();

    /**
     * 获取 [库存出货科目]脏标记
     */
    public boolean getProperty_stock_account_outputDirtyFlag();
    /**
     * 获取 [库存位置]
     */
    public void setProperty_stock_inventory(Integer property_stock_inventory);
    
    /**
     * 设置 [库存位置]
     */
    public Integer getProperty_stock_inventory();

    /**
     * 获取 [库存位置]脏标记
     */
    public boolean getProperty_stock_inventoryDirtyFlag();
    /**
     * 获取 [生产位置]
     */
    public void setProperty_stock_production(Integer property_stock_production);
    
    /**
     * 设置 [生产位置]
     */
    public Integer getProperty_stock_production();

    /**
     * 获取 [生产位置]脏标记
     */
    public boolean getProperty_stock_productionDirtyFlag();
    /**
     * 获取 [库存计价]
     */
    public void setProperty_valuation(String property_valuation);
    
    /**
     * 设置 [库存计价]
     */
    public String getProperty_valuation();

    /**
     * 获取 [库存计价]脏标记
     */
    public boolean getProperty_valuationDirtyFlag();
    /**
     * 获取 [网站产品目录]
     */
    public void setPublic_categ_ids(String public_categ_ids);
    
    /**
     * 设置 [网站产品目录]
     */
    public String getPublic_categ_ids();

    /**
     * 获取 [网站产品目录]脏标记
     */
    public boolean getPublic_categ_idsDirtyFlag();
    /**
     * 获取 [已采购]
     */
    public void setPurchased_product_qty(Double purchased_product_qty);
    
    /**
     * 设置 [已采购]
     */
    public Double getPurchased_product_qty();

    /**
     * 获取 [已采购]脏标记
     */
    public boolean getPurchased_product_qtyDirtyFlag();
    /**
     * 获取 [采购订单行]
     */
    public void setPurchase_line_warn(String purchase_line_warn);
    
    /**
     * 设置 [采购订单行]
     */
    public String getPurchase_line_warn();

    /**
     * 获取 [采购订单行]脏标记
     */
    public boolean getPurchase_line_warnDirtyFlag();
    /**
     * 获取 [采购订单明细的消息]
     */
    public void setPurchase_line_warn_msg(String purchase_line_warn_msg);
    
    /**
     * 设置 [采购订单明细的消息]
     */
    public String getPurchase_line_warn_msg();

    /**
     * 获取 [采购订单明细的消息]脏标记
     */
    public boolean getPurchase_line_warn_msgDirtyFlag();
    /**
     * 获取 [控制策略]
     */
    public void setPurchase_method(String purchase_method);
    
    /**
     * 设置 [控制策略]
     */
    public String getPurchase_method();

    /**
     * 获取 [控制策略]脏标记
     */
    public boolean getPurchase_methodDirtyFlag();
    /**
     * 获取 [采购]
     */
    public void setPurchase_ok(String purchase_ok);
    
    /**
     * 设置 [采购]
     */
    public String getPurchase_ok();

    /**
     * 获取 [采购]脏标记
     */
    public boolean getPurchase_okDirtyFlag();
    /**
     * 获取 [在手数量]
     */
    public void setQty_available(Double qty_available);
    
    /**
     * 设置 [在手数量]
     */
    public Double getQty_available();

    /**
     * 获取 [在手数量]脏标记
     */
    public boolean getQty_availableDirtyFlag();
    /**
     * 获取 [评级数]
     */
    public void setRating_count(Integer rating_count);
    
    /**
     * 设置 [评级数]
     */
    public Integer getRating_count();

    /**
     * 获取 [评级数]脏标记
     */
    public boolean getRating_countDirtyFlag();
    /**
     * 获取 [评级]
     */
    public void setRating_ids(String rating_ids);
    
    /**
     * 设置 [评级]
     */
    public String getRating_ids();

    /**
     * 获取 [评级]脏标记
     */
    public boolean getRating_idsDirtyFlag();
    /**
     * 获取 [最新反馈评级]
     */
    public void setRating_last_feedback(String rating_last_feedback);
    
    /**
     * 设置 [最新反馈评级]
     */
    public String getRating_last_feedback();

    /**
     * 获取 [最新反馈评级]脏标记
     */
    public boolean getRating_last_feedbackDirtyFlag();
    /**
     * 获取 [最新图像评级]
     */
    public void setRating_last_image(byte[] rating_last_image);
    
    /**
     * 设置 [最新图像评级]
     */
    public byte[] getRating_last_image();

    /**
     * 获取 [最新图像评级]脏标记
     */
    public boolean getRating_last_imageDirtyFlag();
    /**
     * 获取 [最新值评级]
     */
    public void setRating_last_value(Double rating_last_value);
    
    /**
     * 设置 [最新值评级]
     */
    public Double getRating_last_value();

    /**
     * 获取 [最新值评级]脏标记
     */
    public boolean getRating_last_valueDirtyFlag();
    /**
     * 获取 [出租]
     */
    public void setRental(String rental);
    
    /**
     * 设置 [出租]
     */
    public String getRental();

    /**
     * 获取 [出租]脏标记
     */
    public boolean getRentalDirtyFlag();
    /**
     * 获取 [重订货最大数量]
     */
    public void setReordering_max_qty(Double reordering_max_qty);
    
    /**
     * 设置 [重订货最大数量]
     */
    public Double getReordering_max_qty();

    /**
     * 获取 [重订货最大数量]脏标记
     */
    public boolean getReordering_max_qtyDirtyFlag();
    /**
     * 获取 [重订货最小数量]
     */
    public void setReordering_min_qty(Double reordering_min_qty);
    
    /**
     * 设置 [重订货最小数量]
     */
    public Double getReordering_min_qty();

    /**
     * 获取 [重订货最小数量]脏标记
     */
    public boolean getReordering_min_qtyDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setResponsible_id(Integer responsible_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getResponsible_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getResponsible_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setResponsible_id_text(String responsible_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getResponsible_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getResponsible_id_textDirtyFlag();
    /**
     * 获取 [类别路线]
     */
    public void setRoute_from_categ_ids(String route_from_categ_ids);
    
    /**
     * 设置 [类别路线]
     */
    public String getRoute_from_categ_ids();

    /**
     * 获取 [类别路线]脏标记
     */
    public boolean getRoute_from_categ_idsDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_ids(String route_ids);
    
    /**
     * 设置 [路线]
     */
    public String getRoute_ids();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_idsDirtyFlag();
    /**
     * 获取 [已售出]
     */
    public void setSales_count(Double sales_count);
    
    /**
     * 设置 [已售出]
     */
    public Double getSales_count();

    /**
     * 获取 [已售出]脏标记
     */
    public boolean getSales_countDirtyFlag();
    /**
     * 获取 [客户前置时间]
     */
    public void setSale_delay(Double sale_delay);
    
    /**
     * 设置 [客户前置时间]
     */
    public Double getSale_delay();

    /**
     * 获取 [客户前置时间]脏标记
     */
    public boolean getSale_delayDirtyFlag();
    /**
     * 获取 [销售订单行]
     */
    public void setSale_line_warn(String sale_line_warn);
    
    /**
     * 设置 [销售订单行]
     */
    public String getSale_line_warn();

    /**
     * 获取 [销售订单行]脏标记
     */
    public boolean getSale_line_warnDirtyFlag();
    /**
     * 获取 [销售订单行消息]
     */
    public void setSale_line_warn_msg(String sale_line_warn_msg);
    
    /**
     * 设置 [销售订单行消息]
     */
    public String getSale_line_warn_msg();

    /**
     * 获取 [销售订单行消息]脏标记
     */
    public boolean getSale_line_warn_msgDirtyFlag();
    /**
     * 获取 [销售]
     */
    public void setSale_ok(String sale_ok);
    
    /**
     * 设置 [销售]
     */
    public String getSale_ok();

    /**
     * 获取 [销售]脏标记
     */
    public boolean getSale_okDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setSeller_ids(String seller_ids);
    
    /**
     * 设置 [供应商]
     */
    public String getSeller_ids();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getSeller_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [自动采购]
     */
    public void setService_to_purchase(String service_to_purchase);
    
    /**
     * 设置 [自动采购]
     */
    public String getService_to_purchase();

    /**
     * 获取 [自动采购]脏标记
     */
    public boolean getService_to_purchaseDirtyFlag();
    /**
     * 获取 [跟踪服务]
     */
    public void setService_type(String service_type);
    
    /**
     * 设置 [跟踪服务]
     */
    public String getService_type();

    /**
     * 获取 [跟踪服务]脏标记
     */
    public boolean getService_typeDirtyFlag();
    /**
     * 获取 [成本]
     */
    public void setStandard_price(Double standard_price);
    
    /**
     * 设置 [成本]
     */
    public Double getStandard_price();

    /**
     * 获取 [成本]脏标记
     */
    public boolean getStandard_priceDirtyFlag();
    /**
     * 获取 [进项税]
     */
    public void setSupplier_taxes_id(String supplier_taxes_id);
    
    /**
     * 设置 [进项税]
     */
    public String getSupplier_taxes_id();

    /**
     * 获取 [进项税]脏标记
     */
    public boolean getSupplier_taxes_idDirtyFlag();
    /**
     * 获取 [销项税]
     */
    public void setTaxes_id(String taxes_id);
    
    /**
     * 设置 [销项税]
     */
    public String getTaxes_id();

    /**
     * 获取 [销项税]脏标记
     */
    public boolean getTaxes_idDirtyFlag();
    /**
     * 获取 [称重]
     */
    public void setTo_weight(String to_weight);
    
    /**
     * 设置 [称重]
     */
    public String getTo_weight();

    /**
     * 获取 [称重]脏标记
     */
    public boolean getTo_weightDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setTracking(String tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getTracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getTrackingDirtyFlag();
    /**
     * 获取 [产品类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [产品类型]
     */
    public String getType();

    /**
     * 获取 [产品类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setUom_id(Integer uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getUom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getUom_idDirtyFlag();
    /**
     * 获取 [单位名称]
     */
    public void setUom_name(String uom_name);
    
    /**
     * 设置 [单位名称]
     */
    public String getUom_name();

    /**
     * 获取 [单位名称]脏标记
     */
    public boolean getUom_nameDirtyFlag();
    /**
     * 获取 [采购计量单位]
     */
    public void setUom_po_id(Integer uom_po_id);
    
    /**
     * 设置 [采购计量单位]
     */
    public Integer getUom_po_id();

    /**
     * 获取 [采购计量单位]脏标记
     */
    public boolean getUom_po_idDirtyFlag();
    /**
     * 获取 [采购计量单位]
     */
    public void setUom_po_id_text(String uom_po_id_text);
    
    /**
     * 设置 [采购计量单位]
     */
    public String getUom_po_id_text();

    /**
     * 获取 [采购计量单位]脏标记
     */
    public boolean getUom_po_id_textDirtyFlag();
    /**
     * 获取 [# BOM 使用的地方]
     */
    public void setUsed_in_bom_count(Integer used_in_bom_count);
    
    /**
     * 设置 [# BOM 使用的地方]
     */
    public Integer getUsed_in_bom_count();

    /**
     * 获取 [# BOM 使用的地方]脏标记
     */
    public boolean getUsed_in_bom_countDirtyFlag();
    /**
     * 获取 [Valid Archived Variants]
     */
    public void setValid_archived_variant_ids(String valid_archived_variant_ids);
    
    /**
     * 设置 [Valid Archived Variants]
     */
    public String getValid_archived_variant_ids();

    /**
     * 获取 [Valid Archived Variants]脏标记
     */
    public boolean getValid_archived_variant_idsDirtyFlag();
    /**
     * 获取 [Valid Existing Variants]
     */
    public void setValid_existing_variant_ids(String valid_existing_variant_ids);
    
    /**
     * 设置 [Valid Existing Variants]
     */
    public String getValid_existing_variant_ids();

    /**
     * 获取 [Valid Existing Variants]脏标记
     */
    public boolean getValid_existing_variant_idsDirtyFlag();
    /**
     * 获取 [有效的产品属性]
     */
    public void setValid_product_attribute_ids(String valid_product_attribute_ids);
    
    /**
     * 设置 [有效的产品属性]
     */
    public String getValid_product_attribute_ids();

    /**
     * 获取 [有效的产品属性]脏标记
     */
    public boolean getValid_product_attribute_idsDirtyFlag();
    /**
     * 获取 [有效的产品属性值]
     */
    public void setValid_product_attribute_value_ids(String valid_product_attribute_value_ids);
    
    /**
     * 设置 [有效的产品属性值]
     */
    public String getValid_product_attribute_value_ids();

    /**
     * 获取 [有效的产品属性值]脏标记
     */
    public boolean getValid_product_attribute_value_idsDirtyFlag();
    /**
     * 获取 [Valid Product Attribute Values Without No Variant Attributes]
     */
    public void setValid_product_attribute_value_wnva_ids(String valid_product_attribute_value_wnva_ids);
    
    /**
     * 设置 [Valid Product Attribute Values Without No Variant Attributes]
     */
    public String getValid_product_attribute_value_wnva_ids();

    /**
     * 获取 [Valid Product Attribute Values Without No Variant Attributes]脏标记
     */
    public boolean getValid_product_attribute_value_wnva_idsDirtyFlag();
    /**
     * 获取 [Valid Product Attributes Without No Variant Attributes]
     */
    public void setValid_product_attribute_wnva_ids(String valid_product_attribute_wnva_ids);
    
    /**
     * 设置 [Valid Product Attributes Without No Variant Attributes]
     */
    public String getValid_product_attribute_wnva_ids();

    /**
     * 获取 [Valid Product Attributes Without No Variant Attributes]脏标记
     */
    public boolean getValid_product_attribute_wnva_idsDirtyFlag();
    /**
     * 获取 [Valid Product Attribute Lines]
     */
    public void setValid_product_template_attribute_line_ids(String valid_product_template_attribute_line_ids);
    
    /**
     * 设置 [Valid Product Attribute Lines]
     */
    public String getValid_product_template_attribute_line_ids();

    /**
     * 获取 [Valid Product Attribute Lines]脏标记
     */
    public boolean getValid_product_template_attribute_line_idsDirtyFlag();
    /**
     * 获取 [Valid Product Attribute Lines Without No Variant Attributes]
     */
    public void setValid_product_template_attribute_line_wnva_ids(String valid_product_template_attribute_line_wnva_ids);
    
    /**
     * 设置 [Valid Product Attribute Lines Without No Variant Attributes]
     */
    public String getValid_product_template_attribute_line_wnva_ids();

    /**
     * 获取 [Valid Product Attribute Lines Without No Variant Attributes]脏标记
     */
    public boolean getValid_product_template_attribute_line_wnva_idsDirtyFlag();
    /**
     * 获取 [计价]
     */
    public void setValuation(String valuation);
    
    /**
     * 设置 [计价]
     */
    public String getValuation();

    /**
     * 获取 [计价]脏标记
     */
    public boolean getValuationDirtyFlag();
    /**
     * 获取 [变种卖家]
     */
    public void setVariant_seller_ids(String variant_seller_ids);
    
    /**
     * 设置 [变种卖家]
     */
    public String getVariant_seller_ids();

    /**
     * 获取 [变种卖家]脏标记
     */
    public boolean getVariant_seller_idsDirtyFlag();
    /**
     * 获取 [预测数量]
     */
    public void setVirtual_available(Double virtual_available);
    
    /**
     * 设置 [预测数量]
     */
    public Double getVirtual_available();

    /**
     * 获取 [预测数量]脏标记
     */
    public boolean getVirtual_availableDirtyFlag();
    /**
     * 获取 [体积]
     */
    public void setVolume(Double volume);
    
    /**
     * 设置 [体积]
     */
    public Double getVolume();

    /**
     * 获取 [体积]脏标记
     */
    public boolean getVolumeDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [网站的说明]
     */
    public void setWebsite_description(String website_description);
    
    /**
     * 设置 [网站的说明]
     */
    public String getWebsite_description();

    /**
     * 获取 [网站的说明]脏标记
     */
    public boolean getWebsite_descriptionDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [网站元说明]
     */
    public void setWebsite_meta_description(String website_meta_description);
    
    /**
     * 设置 [网站元说明]
     */
    public String getWebsite_meta_description();

    /**
     * 获取 [网站元说明]脏标记
     */
    public boolean getWebsite_meta_descriptionDirtyFlag();
    /**
     * 获取 [网站meta关键词]
     */
    public void setWebsite_meta_keywords(String website_meta_keywords);
    
    /**
     * 设置 [网站meta关键词]
     */
    public String getWebsite_meta_keywords();

    /**
     * 获取 [网站meta关键词]脏标记
     */
    public boolean getWebsite_meta_keywordsDirtyFlag();
    /**
     * 获取 [网站opengraph图像]
     */
    public void setWebsite_meta_og_img(String website_meta_og_img);
    
    /**
     * 设置 [网站opengraph图像]
     */
    public String getWebsite_meta_og_img();

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    public boolean getWebsite_meta_og_imgDirtyFlag();
    /**
     * 获取 [网站meta标题]
     */
    public void setWebsite_meta_title(String website_meta_title);
    
    /**
     * 设置 [网站meta标题]
     */
    public String getWebsite_meta_title();

    /**
     * 获取 [网站meta标题]脏标记
     */
    public boolean getWebsite_meta_titleDirtyFlag();
    /**
     * 获取 [网站价格]
     */
    public void setWebsite_price(Double website_price);
    
    /**
     * 设置 [网站价格]
     */
    public Double getWebsite_price();

    /**
     * 获取 [网站价格]脏标记
     */
    public boolean getWebsite_priceDirtyFlag();
    /**
     * 获取 [网站价格差异]
     */
    public void setWebsite_price_difference(String website_price_difference);
    
    /**
     * 设置 [网站价格差异]
     */
    public String getWebsite_price_difference();

    /**
     * 获取 [网站价格差异]脏标记
     */
    public boolean getWebsite_price_differenceDirtyFlag();
    /**
     * 获取 [网站公开价格]
     */
    public void setWebsite_public_price(Double website_public_price);
    
    /**
     * 设置 [网站公开价格]
     */
    public Double getWebsite_public_price();

    /**
     * 获取 [网站公开价格]脏标记
     */
    public boolean getWebsite_public_priceDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站序列]
     */
    public void setWebsite_sequence(Integer website_sequence);
    
    /**
     * 设置 [网站序列]
     */
    public Integer getWebsite_sequence();

    /**
     * 获取 [网站序列]脏标记
     */
    public boolean getWebsite_sequenceDirtyFlag();
    /**
     * 获取 [尺寸 X]
     */
    public void setWebsite_size_x(Integer website_size_x);
    
    /**
     * 设置 [尺寸 X]
     */
    public Integer getWebsite_size_x();

    /**
     * 获取 [尺寸 X]脏标记
     */
    public boolean getWebsite_size_xDirtyFlag();
    /**
     * 获取 [尺寸 Y]
     */
    public void setWebsite_size_y(Integer website_size_y);
    
    /**
     * 设置 [尺寸 Y]
     */
    public Integer getWebsite_size_y();

    /**
     * 获取 [尺寸 Y]脏标记
     */
    public boolean getWebsite_size_yDirtyFlag();
    /**
     * 获取 [样式]
     */
    public void setWebsite_style_ids(String website_style_ids);
    
    /**
     * 设置 [样式]
     */
    public String getWebsite_style_ids();

    /**
     * 获取 [样式]脏标记
     */
    public boolean getWebsite_style_idsDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [重量]
     */
    public void setWeight(Double weight);
    
    /**
     * 设置 [重量]
     */
    public Double getWeight();

    /**
     * 获取 [重量]脏标记
     */
    public boolean getWeightDirtyFlag();
    /**
     * 获取 [测量的重量单位]
     */
    public void setWeight_uom_id(Integer weight_uom_id);
    
    /**
     * 设置 [测量的重量单位]
     */
    public Integer getWeight_uom_id();

    /**
     * 获取 [测量的重量单位]脏标记
     */
    public boolean getWeight_uom_idDirtyFlag();
    /**
     * 获取 [重量计量单位标签]
     */
    public void setWeight_uom_name(String weight_uom_name);
    
    /**
     * 设置 [重量计量单位标签]
     */
    public String getWeight_uom_name();

    /**
     * 获取 [重量计量单位标签]脏标记
     */
    public boolean getWeight_uom_nameDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

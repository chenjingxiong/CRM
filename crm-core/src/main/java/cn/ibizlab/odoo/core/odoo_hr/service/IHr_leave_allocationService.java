package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;


/**
 * 实体[Hr_leave_allocation] 服务对象接口
 */
public interface IHr_leave_allocationService{

    Hr_leave_allocation getDraft(Hr_leave_allocation et) ;
    Hr_leave_allocation get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_leave_allocation et) ;
    void updateBatch(List<Hr_leave_allocation> list) ;
    boolean create(Hr_leave_allocation et) ;
    void createBatch(List<Hr_leave_allocation> list) ;
    Page<Hr_leave_allocation> searchDefault(Hr_leave_allocationSearchContext context) ;

}




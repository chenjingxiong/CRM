package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicant_categorySearchContext;


/**
 * 实体[Hr_applicant_category] 服务对象接口
 */
public interface IHr_applicant_categoryService{

    boolean update(Hr_applicant_category et) ;
    void updateBatch(List<Hr_applicant_category> list) ;
    Hr_applicant_category get(Integer key) ;
    boolean create(Hr_applicant_category et) ;
    void createBatch(List<Hr_applicant_category> list) ;
    Hr_applicant_category getDraft(Hr_applicant_category et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_applicant_category> searchDefault(Hr_applicant_categorySearchContext context) ;

}




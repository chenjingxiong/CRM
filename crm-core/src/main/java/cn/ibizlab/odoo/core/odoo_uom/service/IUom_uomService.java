package cn.ibizlab.odoo.core.odoo_uom.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;


/**
 * 实体[Uom_uom] 服务对象接口
 */
public interface IUom_uomService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Uom_uom getDraft(Uom_uom et) ;
    boolean update(Uom_uom et) ;
    void updateBatch(List<Uom_uom> list) ;
    Uom_uom get(Integer key) ;
    boolean create(Uom_uom et) ;
    void createBatch(List<Uom_uom> list) ;
    Page<Uom_uom> searchDefault(Uom_uomSearchContext context) ;

}




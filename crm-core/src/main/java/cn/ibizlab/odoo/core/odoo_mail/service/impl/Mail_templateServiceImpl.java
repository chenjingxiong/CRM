package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_templateFeignClient;

/**
 * 实体[EMail模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_templateServiceImpl implements IMail_templateService {

    @Autowired
    mail_templateFeignClient mail_templateFeignClient;


    @Override
    public Mail_template get(Integer id) {
		Mail_template et=mail_templateFeignClient.get(id);
        if(et==null){
            et=new Mail_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_template getDraft(Mail_template et) {
        et=mail_templateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_template et) {
        Mail_template rt = mail_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_template> list){
        mail_templateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_template et) {
        Mail_template rt = mail_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_template> list){
        mail_templateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_templateFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_template> searchDefault(Mail_templateSearchContext context) {
        Page<Mail_template> mail_templates=mail_templateFeignClient.searchDefault(context);
        return mail_templates;
    }


}



package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
@Component
public class mro_pm_meter_intervalFallback implements mro_pm_meter_intervalFeignClient{

    public Mro_pm_meter_interval update(Integer id, Mro_pm_meter_interval mro_pm_meter_interval){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_meter_interval> mro_pm_meter_intervals){
            return false;
     }


    public Page<Mro_pm_meter_interval> searchDefault(Mro_pm_meter_intervalSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Mro_pm_meter_interval create(Mro_pm_meter_interval mro_pm_meter_interval){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_meter_interval> mro_pm_meter_intervals){
            return false;
     }

    public Mro_pm_meter_interval get(Integer id){
            return null;
     }



    public Page<Mro_pm_meter_interval> select(){
            return null;
     }

    public Mro_pm_meter_interval getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_inventory_line] 对象
 */
public interface Istock_inventory_line {

    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [库存]
     */
    public void setInventory_id(Integer inventory_id);
    
    /**
     * 设置 [库存]
     */
    public Integer getInventory_id();

    /**
     * 获取 [库存]脏标记
     */
    public boolean getInventory_idDirtyFlag();
    /**
     * 获取 [库存]
     */
    public void setInventory_id_text(String inventory_id_text);
    
    /**
     * 设置 [库存]
     */
    public String getInventory_id_text();

    /**
     * 获取 [库存]脏标记
     */
    public boolean getInventory_id_textDirtyFlag();
    /**
     * 获取 [库存位置]
     */
    public void setInventory_location_id(Integer inventory_location_id);
    
    /**
     * 设置 [库存位置]
     */
    public Integer getInventory_location_id();

    /**
     * 获取 [库存位置]脏标记
     */
    public boolean getInventory_location_idDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id(Integer package_id);
    
    /**
     * 设置 [包裹]
     */
    public Integer getPackage_id();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_idDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id_text(String package_id_text);
    
    /**
     * 设置 [包裹]
     */
    public String getPackage_id_text();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_id_textDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getPartner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getPartner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [已检查的数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [已检查的数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [已检查的数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setProduct_tracking(String product_tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getProduct_tracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getProduct_trackingDirtyFlag();
    /**
     * 获取 [度量单位分类]
     */
    public void setProduct_uom_category_id(Integer product_uom_category_id);
    
    /**
     * 设置 [度量单位分类]
     */
    public Integer getProduct_uom_category_id();

    /**
     * 获取 [度量单位分类]脏标记
     */
    public boolean getProduct_uom_category_idDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [产品量度单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [产品量度单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setProd_lot_id(Integer prod_lot_id);
    
    /**
     * 设置 [批次/序列号码]
     */
    public Integer getProd_lot_id();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getProd_lot_idDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setProd_lot_id_text(String prod_lot_id_text);
    
    /**
     * 设置 [批次/序列号码]
     */
    public String getProd_lot_id_text();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getProd_lot_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [理论数量]
     */
    public void setTheoretical_qty(Double theoretical_qty);
    
    /**
     * 设置 [理论数量]
     */
    public Double getTheoretical_qty();

    /**
     * 获取 [理论数量]脏标记
     */
    public boolean getTheoretical_qtyDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

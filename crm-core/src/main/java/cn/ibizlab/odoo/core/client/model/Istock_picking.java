package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_picking] 对象
 */
public interface Istock_picking {

    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [欠单]
     */
    public void setBackorder_id(Integer backorder_id);
    
    /**
     * 设置 [欠单]
     */
    public Integer getBackorder_id();

    /**
     * 获取 [欠单]脏标记
     */
    public boolean getBackorder_idDirtyFlag();
    /**
     * 获取 [欠单]
     */
    public void setBackorder_ids(String backorder_ids);
    
    /**
     * 设置 [欠单]
     */
    public String getBackorder_ids();

    /**
     * 获取 [欠单]脏标记
     */
    public boolean getBackorder_idsDirtyFlag();
    /**
     * 获取 [欠单]
     */
    public void setBackorder_id_text(String backorder_id_text);
    
    /**
     * 设置 [欠单]
     */
    public String getBackorder_id_text();

    /**
     * 获取 [欠单]脏标记
     */
    public boolean getBackorder_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [调拨日期]
     */
    public void setDate_done(Timestamp date_done);
    
    /**
     * 设置 [调拨日期]
     */
    public Timestamp getDate_done();

    /**
     * 获取 [调拨日期]脏标记
     */
    public boolean getDate_doneDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [补货组]
     */
    public void setGroup_id(Integer group_id);
    
    /**
     * 设置 [补货组]
     */
    public Integer getGroup_id();

    /**
     * 获取 [补货组]脏标记
     */
    public boolean getGroup_idDirtyFlag();
    /**
     * 获取 [有包裹]
     */
    public void setHas_packages(String has_packages);
    
    /**
     * 设置 [有包裹]
     */
    public String getHas_packages();

    /**
     * 获取 [有包裹]脏标记
     */
    public boolean getHas_packagesDirtyFlag();
    /**
     * 获取 [有报废移动]
     */
    public void setHas_scrap_move(String has_scrap_move);
    
    /**
     * 设置 [有报废移动]
     */
    public String getHas_scrap_move();

    /**
     * 获取 [有报废移动]脏标记
     */
    public boolean getHas_scrap_moveDirtyFlag();
    /**
     * 获取 [有跟踪]
     */
    public void setHas_tracking(String has_tracking);
    
    /**
     * 设置 [有跟踪]
     */
    public String getHas_tracking();

    /**
     * 获取 [有跟踪]脏标记
     */
    public boolean getHas_trackingDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [立即调拨]
     */
    public void setImmediate_transfer(String immediate_transfer);
    
    /**
     * 设置 [立即调拨]
     */
    public String getImmediate_transfer();

    /**
     * 获取 [立即调拨]脏标记
     */
    public boolean getImmediate_transferDirtyFlag();
    /**
     * 获取 [是锁定]
     */
    public void setIs_locked(String is_locked);
    
    /**
     * 设置 [是锁定]
     */
    public String getIs_locked();

    /**
     * 获取 [是锁定]脏标记
     */
    public boolean getIs_lockedDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_dest_id(Integer location_dest_id);
    
    /**
     * 设置 [目的位置]
     */
    public Integer getLocation_dest_id();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_dest_idDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_dest_id_text(String location_dest_id_text);
    
    /**
     * 设置 [目的位置]
     */
    public String getLocation_dest_id_text();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_dest_id_textDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [源位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [源位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [库存移动不在包裹里]
     */
    public void setMove_ids_without_package(String move_ids_without_package);
    
    /**
     * 设置 [库存移动不在包裹里]
     */
    public String getMove_ids_without_package();

    /**
     * 获取 [库存移动不在包裹里]脏标记
     */
    public boolean getMove_ids_without_packageDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setMove_lines(String move_lines);
    
    /**
     * 设置 [库存移动]
     */
    public String getMove_lines();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getMove_linesDirtyFlag();
    /**
     * 获取 [有包裹作业]
     */
    public void setMove_line_exist(String move_line_exist);
    
    /**
     * 设置 [有包裹作业]
     */
    public String getMove_line_exist();

    /**
     * 获取 [有包裹作业]脏标记
     */
    public boolean getMove_line_existDirtyFlag();
    /**
     * 获取 [作业]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [作业]
     */
    public String getMove_line_ids();

    /**
     * 获取 [作业]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [无包裹作业]
     */
    public void setMove_line_ids_without_package(String move_line_ids_without_package);
    
    /**
     * 设置 [无包裹作业]
     */
    public String getMove_line_ids_without_package();

    /**
     * 获取 [无包裹作业]脏标记
     */
    public boolean getMove_line_ids_without_packageDirtyFlag();
    /**
     * 获取 [送货策略]
     */
    public void setMove_type(String move_type);
    
    /**
     * 设置 [送货策略]
     */
    public String getMove_type();

    /**
     * 获取 [送货策略]脏标记
     */
    public boolean getMove_typeDirtyFlag();
    /**
     * 获取 [编号]
     */
    public void setName(String name);
    
    /**
     * 设置 [编号]
     */
    public String getName();

    /**
     * 获取 [编号]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id(Integer owner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getOwner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id_text(String owner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getOwner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_id_textDirtyFlag();
    /**
     * 获取 [包裹层级]
     */
    public void setPackage_level_ids(String package_level_ids);
    
    /**
     * 设置 [包裹层级]
     */
    public String getPackage_level_ids();

    /**
     * 获取 [包裹层级]脏标记
     */
    public boolean getPackage_level_idsDirtyFlag();
    /**
     * 获取 [包裹层级ids 详情]
     */
    public void setPackage_level_ids_details(String package_level_ids_details);
    
    /**
     * 设置 [包裹层级ids 详情]
     */
    public String getPackage_level_ids_details();

    /**
     * 获取 [包裹层级ids 详情]脏标记
     */
    public boolean getPackage_level_ids_detailsDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [作业的类型]
     */
    public void setPicking_type_code(String picking_type_code);
    
    /**
     * 设置 [作业的类型]
     */
    public String getPicking_type_code();

    /**
     * 获取 [作业的类型]脏标记
     */
    public boolean getPicking_type_codeDirtyFlag();
    /**
     * 获取 [移动整个包裹]
     */
    public void setPicking_type_entire_packs(String picking_type_entire_packs);
    
    /**
     * 设置 [移动整个包裹]
     */
    public String getPicking_type_entire_packs();

    /**
     * 获取 [移动整个包裹]脏标记
     */
    public boolean getPicking_type_entire_packsDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [作业类型]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [作业类型]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [已打印]
     */
    public void setPrinted(String printed);
    
    /**
     * 设置 [已打印]
     */
    public String getPrinted();

    /**
     * 获取 [已打印]脏标记
     */
    public boolean getPrintedDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [采购订单]
     */
    public void setPurchase_id(Integer purchase_id);
    
    /**
     * 设置 [采购订单]
     */
    public Integer getPurchase_id();

    /**
     * 获取 [采购订单]脏标记
     */
    public boolean getPurchase_idDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_id(Integer sale_id);
    
    /**
     * 设置 [销售订单]
     */
    public Integer getSale_id();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_idDirtyFlag();
    /**
     * 获取 [销售订单]
     */
    public void setSale_id_text(String sale_id_text);
    
    /**
     * 设置 [销售订单]
     */
    public String getSale_id_text();

    /**
     * 获取 [销售订单]脏标记
     */
    public boolean getSale_id_textDirtyFlag();
    /**
     * 获取 [预定交货日期]
     */
    public void setScheduled_date(Timestamp scheduled_date);
    
    /**
     * 设置 [预定交货日期]
     */
    public Timestamp getScheduled_date();

    /**
     * 获取 [预定交货日期]脏标记
     */
    public boolean getScheduled_dateDirtyFlag();
    /**
     * 获取 [显示检查可用]
     */
    public void setShow_check_availability(String show_check_availability);
    
    /**
     * 设置 [显示检查可用]
     */
    public String getShow_check_availability();

    /**
     * 获取 [显示检查可用]脏标记
     */
    public boolean getShow_check_availabilityDirtyFlag();
    /**
     * 获取 [显示批次文本]
     */
    public void setShow_lots_text(String show_lots_text);
    
    /**
     * 设置 [显示批次文本]
     */
    public String getShow_lots_text();

    /**
     * 获取 [显示批次文本]脏标记
     */
    public boolean getShow_lots_textDirtyFlag();
    /**
     * 获取 [显示标记为代办]
     */
    public void setShow_mark_as_todo(String show_mark_as_todo);
    
    /**
     * 设置 [显示标记为代办]
     */
    public String getShow_mark_as_todo();

    /**
     * 获取 [显示标记为代办]脏标记
     */
    public boolean getShow_mark_as_todoDirtyFlag();
    /**
     * 获取 [显示作业]
     */
    public void setShow_operations(String show_operations);
    
    /**
     * 设置 [显示作业]
     */
    public String getShow_operations();

    /**
     * 获取 [显示作业]脏标记
     */
    public boolean getShow_operationsDirtyFlag();
    /**
     * 获取 [显示验证]
     */
    public void setShow_validate(String show_validate);
    
    /**
     * 设置 [显示验证]
     */
    public String getShow_validate();

    /**
     * 获取 [显示验证]脏标记
     */
    public boolean getShow_validateDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

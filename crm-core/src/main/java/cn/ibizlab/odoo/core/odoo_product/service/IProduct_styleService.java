package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;


/**
 * 实体[Product_style] 服务对象接口
 */
public interface IProduct_styleService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_style get(Integer key) ;
    Product_style getDraft(Product_style et) ;
    boolean create(Product_style et) ;
    void createBatch(List<Product_style> list) ;
    boolean update(Product_style et) ;
    void updateBatch(List<Product_style> list) ;
    Page<Product_style> searchDefault(Product_styleSearchContext context) ;

}




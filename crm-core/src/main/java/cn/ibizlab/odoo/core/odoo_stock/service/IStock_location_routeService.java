package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;


/**
 * 实体[Stock_location_route] 服务对象接口
 */
public interface IStock_location_routeService{

    boolean create(Stock_location_route et) ;
    void createBatch(List<Stock_location_route> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_location_route get(Integer key) ;
    Stock_location_route getDraft(Stock_location_route et) ;
    boolean update(Stock_location_route et) ;
    void updateBatch(List<Stock_location_route> list) ;
    Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context) ;

}




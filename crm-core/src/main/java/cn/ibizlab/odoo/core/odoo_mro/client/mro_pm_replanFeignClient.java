package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_replan] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-pm-replan", fallback = mro_pm_replanFallback.class)
public interface mro_pm_replanFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/{id}")
    Mro_pm_replan update(@PathVariable("id") Integer id,@RequestBody Mro_pm_replan mro_pm_replan);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_replan> mro_pm_replans);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/{id}")
    Mro_pm_replan get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/searchdefault")
    Page<Mro_pm_replan> searchDefault(@RequestBody Mro_pm_replanSearchContext context);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans")
    Mro_pm_replan create(@RequestBody Mro_pm_replan mro_pm_replan);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_replan> mro_pm_replans);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/select")
    Page<Mro_pm_replan> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/getdraft")
    Mro_pm_replan getDraft();


}

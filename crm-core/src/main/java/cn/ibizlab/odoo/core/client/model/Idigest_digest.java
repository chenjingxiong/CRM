package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [digest_digest] 对象
 */
public interface Idigest_digest {

    /**
     * 获取 [可用字段]
     */
    public void setAvailable_fields(String available_fields);
    
    /**
     * 设置 [可用字段]
     */
    public String getAvailable_fields();

    /**
     * 获取 [可用字段]脏标记
     */
    public boolean getAvailable_fieldsDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [已订阅]
     */
    public void setIs_subscribed(String is_subscribed);
    
    /**
     * 设置 [已订阅]
     */
    public String getIs_subscribed();

    /**
     * 获取 [已订阅]脏标记
     */
    public boolean getIs_subscribedDirtyFlag();
    /**
     * 获取 [收入]
     */
    public void setKpi_account_total_revenue(String kpi_account_total_revenue);
    
    /**
     * 设置 [收入]
     */
    public String getKpi_account_total_revenue();

    /**
     * 获取 [收入]脏标记
     */
    public boolean getKpi_account_total_revenueDirtyFlag();
    /**
     * 获取 [KPI账户总收入]
     */
    public void setKpi_account_total_revenue_value(Double kpi_account_total_revenue_value);
    
    /**
     * 设置 [KPI账户总收入]
     */
    public Double getKpi_account_total_revenue_value();

    /**
     * 获取 [KPI账户总收入]脏标记
     */
    public boolean getKpi_account_total_revenue_valueDirtyFlag();
    /**
     * 获取 [所有销售]
     */
    public void setKpi_all_sale_total(String kpi_all_sale_total);
    
    /**
     * 设置 [所有销售]
     */
    public String getKpi_all_sale_total();

    /**
     * 获取 [所有销售]脏标记
     */
    public boolean getKpi_all_sale_totalDirtyFlag();
    /**
     * 获取 [所有销售总价值KPI]
     */
    public void setKpi_all_sale_total_value(Double kpi_all_sale_total_value);
    
    /**
     * 设置 [所有销售总价值KPI]
     */
    public Double getKpi_all_sale_total_value();

    /**
     * 获取 [所有销售总价值KPI]脏标记
     */
    public boolean getKpi_all_sale_total_valueDirtyFlag();
    /**
     * 获取 [新的线索/商机]
     */
    public void setKpi_crm_lead_created(String kpi_crm_lead_created);
    
    /**
     * 设置 [新的线索/商机]
     */
    public String getKpi_crm_lead_created();

    /**
     * 获取 [新的线索/商机]脏标记
     */
    public boolean getKpi_crm_lead_createdDirtyFlag();
    /**
     * 获取 [KPI CRM预期收益]
     */
    public void setKpi_crm_lead_created_value(Integer kpi_crm_lead_created_value);
    
    /**
     * 设置 [KPI CRM预期收益]
     */
    public Integer getKpi_crm_lead_created_value();

    /**
     * 获取 [KPI CRM预期收益]脏标记
     */
    public boolean getKpi_crm_lead_created_valueDirtyFlag();
    /**
     * 获取 [已签单商机]
     */
    public void setKpi_crm_opportunities_won(String kpi_crm_opportunities_won);
    
    /**
     * 设置 [已签单商机]
     */
    public String getKpi_crm_opportunities_won();

    /**
     * 获取 [已签单商机]脏标记
     */
    public boolean getKpi_crm_opportunities_wonDirtyFlag();
    /**
     * 获取 [KPI CRM签单金额]
     */
    public void setKpi_crm_opportunities_won_value(Integer kpi_crm_opportunities_won_value);
    
    /**
     * 设置 [KPI CRM签单金额]
     */
    public Integer getKpi_crm_opportunities_won_value();

    /**
     * 获取 [KPI CRM签单金额]脏标记
     */
    public boolean getKpi_crm_opportunities_won_valueDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setKpi_hr_recruitment_new_colleagues(String kpi_hr_recruitment_new_colleagues);
    
    /**
     * 设置 [员工]
     */
    public String getKpi_hr_recruitment_new_colleagues();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getKpi_hr_recruitment_new_colleaguesDirtyFlag();
    /**
     * 获取 [人力资源新聘员工KPI指标]
     */
    public void setKpi_hr_recruitment_new_colleagues_value(Integer kpi_hr_recruitment_new_colleagues_value);
    
    /**
     * 设置 [人力资源新聘员工KPI指标]
     */
    public Integer getKpi_hr_recruitment_new_colleagues_value();

    /**
     * 获取 [人力资源新聘员工KPI指标]脏标记
     */
    public boolean getKpi_hr_recruitment_new_colleagues_valueDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setKpi_mail_message_total(String kpi_mail_message_total);
    
    /**
     * 设置 [消息]
     */
    public String getKpi_mail_message_total();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getKpi_mail_message_totalDirtyFlag();
    /**
     * 获取 [Kpi 邮件信息总计]
     */
    public void setKpi_mail_message_total_value(Integer kpi_mail_message_total_value);
    
    /**
     * 设置 [Kpi 邮件信息总计]
     */
    public Integer getKpi_mail_message_total_value();

    /**
     * 获取 [Kpi 邮件信息总计]脏标记
     */
    public boolean getKpi_mail_message_total_valueDirtyFlag();
    /**
     * 获取 [POS 销售]
     */
    public void setKpi_pos_total(String kpi_pos_total);
    
    /**
     * 设置 [POS 销售]
     */
    public String getKpi_pos_total();

    /**
     * 获取 [POS 销售]脏标记
     */
    public boolean getKpi_pos_totalDirtyFlag();
    /**
     * 获取 [终端销售总额的关键绩效指标]
     */
    public void setKpi_pos_total_value(Double kpi_pos_total_value);
    
    /**
     * 设置 [终端销售总额的关键绩效指标]
     */
    public Double getKpi_pos_total_value();

    /**
     * 获取 [终端销售总额的关键绩效指标]脏标记
     */
    public boolean getKpi_pos_total_valueDirtyFlag();
    /**
     * 获取 [开放任务]
     */
    public void setKpi_project_task_opened(String kpi_project_task_opened);
    
    /**
     * 设置 [开放任务]
     */
    public String getKpi_project_task_opened();

    /**
     * 获取 [开放任务]脏标记
     */
    public boolean getKpi_project_task_openedDirtyFlag();
    /**
     * 获取 [项目任务开放价值的关键绩效指标]
     */
    public void setKpi_project_task_opened_value(Integer kpi_project_task_opened_value);
    
    /**
     * 设置 [项目任务开放价值的关键绩效指标]
     */
    public Integer getKpi_project_task_opened_value();

    /**
     * 获取 [项目任务开放价值的关键绩效指标]脏标记
     */
    public boolean getKpi_project_task_opened_valueDirtyFlag();
    /**
     * 获取 [已连接用户]
     */
    public void setKpi_res_users_connected(String kpi_res_users_connected);
    
    /**
     * 设置 [已连接用户]
     */
    public String getKpi_res_users_connected();

    /**
     * 获取 [已连接用户]脏标记
     */
    public boolean getKpi_res_users_connectedDirtyFlag();
    /**
     * 获取 [Kpi Res 用户连接值]
     */
    public void setKpi_res_users_connected_value(Integer kpi_res_users_connected_value);
    
    /**
     * 设置 [Kpi Res 用户连接值]
     */
    public Integer getKpi_res_users_connected_value();

    /**
     * 获取 [Kpi Res 用户连接值]脏标记
     */
    public boolean getKpi_res_users_connected_valueDirtyFlag();
    /**
     * 获取 [电商销售]
     */
    public void setKpi_website_sale_total(String kpi_website_sale_total);
    
    /**
     * 设置 [电商销售]
     */
    public String getKpi_website_sale_total();

    /**
     * 获取 [电商销售]脏标记
     */
    public boolean getKpi_website_sale_totalDirtyFlag();
    /**
     * 获取 [Kpi网站销售总价值]
     */
    public void setKpi_website_sale_total_value(Double kpi_website_sale_total_value);
    
    /**
     * 设置 [Kpi网站销售总价值]
     */
    public Double getKpi_website_sale_total_value();

    /**
     * 获取 [Kpi网站销售总价值]脏标记
     */
    public boolean getKpi_website_sale_total_valueDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [下一发送日期]
     */
    public void setNext_run_date(Timestamp next_run_date);
    
    /**
     * 设置 [下一发送日期]
     */
    public Timestamp getNext_run_date();

    /**
     * 获取 [下一发送日期]脏标记
     */
    public boolean getNext_run_dateDirtyFlag();
    /**
     * 获取 [周期]
     */
    public void setPeriodicity(String periodicity);
    
    /**
     * 设置 [周期]
     */
    public String getPeriodicity();

    /**
     * 获取 [周期]脏标记
     */
    public boolean getPeriodicityDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [EMail模板]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id_text(String template_id_text);
    
    /**
     * 设置 [EMail模板]
     */
    public String getTemplate_id_text();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_id_textDirtyFlag();
    /**
     * 获取 [收件人]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [收件人]
     */
    public String getUser_ids();

    /**
     * 获取 [收件人]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_country_state;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_country_state.Res_country_stateCreate_uid_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_country_state
 * 属性：Create_uid_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_country_stateCreate_uid_textDefaultValidator.class})
public @interface Res_country_stateCreate_uid_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

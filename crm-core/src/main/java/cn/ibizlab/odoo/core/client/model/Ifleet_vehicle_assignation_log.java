package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [fleet_vehicle_assignation_log] 对象
 */
public interface Ifleet_vehicle_assignation_log {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [终止日期]
     */
    public void setDate_end(Timestamp date_end);
    
    /**
     * 设置 [终止日期]
     */
    public Timestamp getDate_end();

    /**
     * 获取 [终止日期]脏标记
     */
    public boolean getDate_endDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setDriver_id(Integer driver_id);
    
    /**
     * 设置 [驾驶员]
     */
    public Integer getDriver_id();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getDriver_idDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setDriver_id_text(String driver_id_text);
    
    /**
     * 设置 [驾驶员]
     */
    public String getDriver_id_text();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getDriver_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [车辆]
     */
    public void setVehicle_id(Integer vehicle_id);
    
    /**
     * 设置 [车辆]
     */
    public Integer getVehicle_id();

    /**
     * 获取 [车辆]脏标记
     */
    public boolean getVehicle_idDirtyFlag();
    /**
     * 获取 [车辆]
     */
    public void setVehicle_id_text(String vehicle_id_text);
    
    /**
     * 设置 [车辆]
     */
    public String getVehicle_id_text();

    /**
     * 获取 [车辆]脏标记
     */
    public boolean getVehicle_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_services;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
public interface Ifleet_vehicle_log_servicesClientService{

    public Ifleet_vehicle_log_services createModel() ;

    public void remove(Ifleet_vehicle_log_services fleet_vehicle_log_services);

    public void create(Ifleet_vehicle_log_services fleet_vehicle_log_services);

    public void get(Ifleet_vehicle_log_services fleet_vehicle_log_services);

    public void updateBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services);

    public Page<Ifleet_vehicle_log_services> fetchDefault(SearchContext context);

    public void removeBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services);

    public void update(Ifleet_vehicle_log_services fleet_vehicle_log_services);

    public void createBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services);

    public Page<Ifleet_vehicle_log_services> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_log_services fleet_vehicle_log_services);

}

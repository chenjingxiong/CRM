package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;


/**
 * 实体[Mro_request_reject] 服务对象接口
 */
public interface IMro_request_rejectService{

    boolean create(Mro_request_reject et) ;
    void createBatch(List<Mro_request_reject> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mro_request_reject get(Integer key) ;
    boolean update(Mro_request_reject et) ;
    void updateBatch(List<Mro_request_reject> list) ;
    Mro_request_reject getDraft(Mro_request_reject et) ;
    Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_category] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-category", fallback = product_categoryFallback.class)
public interface product_categoryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/product_categories/{id}")
    Product_category update(@PathVariable("id") Integer id,@RequestBody Product_category product_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_categories/batch")
    Boolean updateBatch(@RequestBody List<Product_category> product_categories);



    @RequestMapping(method = RequestMethod.POST, value = "/product_categories")
    Product_category create(@RequestBody Product_category product_category);

    @RequestMapping(method = RequestMethod.POST, value = "/product_categories/batch")
    Boolean createBatch(@RequestBody List<Product_category> product_categories);



    @RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/product_categories/{id}")
    Product_category get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_categories/searchdefault")
    Page<Product_category> searchDefault(@RequestBody Product_categorySearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/product_categories/select")
    Page<Product_category> select();


    @RequestMapping(method = RequestMethod.POST, value = "/product_categories/save")
    Boolean save(@RequestBody Product_category product_category);

    @RequestMapping(method = RequestMethod.POST, value = "/product_categories/save")
    Boolean saveBatch(@RequestBody List<Product_category> product_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/product_categories/getdraft")
    Product_category getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_categories/checkkey")
    Boolean checkKey(@RequestBody Product_category product_category);


}

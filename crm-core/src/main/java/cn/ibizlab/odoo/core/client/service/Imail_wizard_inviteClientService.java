package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_wizard_invite;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_wizard_invite] 服务对象接口
 */
public interface Imail_wizard_inviteClientService{

    public Imail_wizard_invite createModel() ;

    public Page<Imail_wizard_invite> fetchDefault(SearchContext context);

    public void update(Imail_wizard_invite mail_wizard_invite);

    public void updateBatch(List<Imail_wizard_invite> mail_wizard_invites);

    public void create(Imail_wizard_invite mail_wizard_invite);

    public void removeBatch(List<Imail_wizard_invite> mail_wizard_invites);

    public void get(Imail_wizard_invite mail_wizard_invite);

    public void remove(Imail_wizard_invite mail_wizard_invite);

    public void createBatch(List<Imail_wizard_invite> mail_wizard_invites);

    public Page<Imail_wizard_invite> select(SearchContext context);

    public void getDraft(Imail_wizard_invite mail_wizard_invite);

}

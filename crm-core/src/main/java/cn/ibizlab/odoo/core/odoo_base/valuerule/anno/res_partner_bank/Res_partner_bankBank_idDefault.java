package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner_bank;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_partner_bank.Res_partner_bankBank_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_partner_bank
 * 属性：Bank_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_partner_bankBank_idDefaultValidator.class})
public @interface Res_partner_bankBank_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

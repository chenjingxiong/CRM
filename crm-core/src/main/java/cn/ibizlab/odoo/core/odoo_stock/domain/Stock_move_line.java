package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品移动(移库明细)] 对象
 */
@Data
public class Stock_move_line extends EntityClient implements Serializable {

    /**
     * 完成
     */
    @DEField(name = "qty_done")
    @JSONField(name = "qty_done")
    @JsonProperty("qty_done")
    private Double qtyDone;

    /**
     * 使用已有批次/序列号码
     */
    @JSONField(name = "picking_type_use_existing_lots")
    @JsonProperty("picking_type_use_existing_lots")
    private String pickingTypeUseExistingLots;

    /**
     * 消耗行
     */
    @JSONField(name = "consume_line_ids")
    @JsonProperty("consume_line_ids")
    private String consumeLineIds;

    /**
     * 批次可见
     */
    @JSONField(name = "lots_visible")
    @JsonProperty("lots_visible")
    private String lotsVisible;

    /**
     * 已保留
     */
    @DEField(name = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 完成工单
     */
    @DEField(name = "done_wo")
    @JSONField(name = "done_wo")
    @JsonProperty("done_wo")
    private String doneWo;

    /**
     * 创建新批次/序列号码
     */
    @JSONField(name = "picking_type_use_create_lots")
    @JsonProperty("picking_type_use_create_lots")
    private String pickingTypeUseCreateLots;

    /**
     * 生产行
     */
    @JSONField(name = "produce_line_ids")
    @JsonProperty("produce_line_ids")
    private String produceLineIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 移动整个包裹
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private String pickingTypeEntirePacks;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 批次/序列号 名称
     */
    @DEField(name = "lot_name")
    @JSONField(name = "lot_name")
    @JsonProperty("lot_name")
    private String lotName;

    /**
     * 实际预留数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 产成品数量
     */
    @DEField(name = "lot_produced_qty")
    @JSONField(name = "lot_produced_qty")
    @JsonProperty("lot_produced_qty")
    private Double lotProducedQty;

    /**
     * 源包裹
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;

    /**
     * 完工批次/序列号
     */
    @JSONField(name = "lot_produced_id_text")
    @JsonProperty("lot_produced_id_text")
    private String lotProducedIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 工单
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 追踪
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 库存移动
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 至
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 是锁定
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private String isLocked;

    /**
     * 初始需求是否可以编辑
     */
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private String isInitialDemandEditable;

    /**
     * 生产单
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 批次/序列号码
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 编号
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 目的地包裹
     */
    @JSONField(name = "result_package_id_text")
    @JsonProperty("result_package_id_text")
    private String resultPackageIdText;

    /**
     * 移动完成
     */
    @JSONField(name = "done_move")
    @JsonProperty("done_move")
    private String doneMove;

    /**
     * 库存拣货
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 所有者
     */
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;

    /**
     * 从
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 库存移动
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 目的地包裹
     */
    @DEField(name = "result_package_id")
    @JSONField(name = "result_package_id")
    @JsonProperty("result_package_id")
    private Integer resultPackageId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 完工批次/序列号
     */
    @DEField(name = "lot_produced_id")
    @JSONField(name = "lot_produced_id")
    @JsonProperty("lot_produced_id")
    private Integer lotProducedId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 批次/序列号码
     */
    @DEField(name = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;

    /**
     * 库存拣货
     */
    @DEField(name = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 工单
     */
    @DEField(name = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 从
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 源包裹
     */
    @DEField(name = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Integer packageId;

    /**
     * 所有者
     */
    @DEField(name = "owner_id")
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Integer ownerId;

    /**
     * 包裹层级
     */
    @DEField(name = "package_level_id")
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    private Integer packageLevelId;

    /**
     * 至
     */
    @DEField(name = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;

    /**
     * 生产单
     */
    @DEField(name = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;


    /**
     * 
     */
    @JSONField(name = "odooproduction")
    @JsonProperty("odooproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JSONField(name = "odooworkorder")
    @JsonProperty("odooworkorder")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odooowner")
    @JsonProperty("odooowner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooOwner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocationdest")
    @JsonProperty("odoolocationdest")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoopackagelevel")
    @JsonProperty("odoopackagelevel")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level odooPackageLevel;

    /**
     * 
     */
    @JSONField(name = "odoopicking")
    @JsonProperty("odoopicking")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JSONField(name = "odoolot")
    @JsonProperty("odoolot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JSONField(name = "odoolotproduced")
    @JsonProperty("odoolotproduced")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooLotProduced;

    /**
     * 
     */
    @JSONField(name = "odoopackage")
    @JsonProperty("odoopackage")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package odooPackage;

    /**
     * 
     */
    @JSONField(name = "odooresultpackage")
    @JsonProperty("odooresultpackage")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package odooResultPackage;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [完成]
     */
    public void setQtyDone(Double qtyDone){
        this.qtyDone = qtyDone ;
        this.modify("qty_done",qtyDone);
    }
    /**
     * 设置 [已保留]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }
    /**
     * 设置 [完成工单]
     */
    public void setDoneWo(String doneWo){
        this.doneWo = doneWo ;
        this.modify("done_wo",doneWo);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [批次/序列号 名称]
     */
    public void setLotName(String lotName){
        this.lotName = lotName ;
        this.modify("lot_name",lotName);
    }
    /**
     * 设置 [实际预留数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [产成品数量]
     */
    public void setLotProducedQty(Double lotProducedQty){
        this.lotProducedQty = lotProducedQty ;
        this.modify("lot_produced_qty",lotProducedQty);
    }
    /**
     * 设置 [单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }
    /**
     * 设置 [库存移动]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [目的地包裹]
     */
    public void setResultPackageId(Integer resultPackageId){
        this.resultPackageId = resultPackageId ;
        this.modify("result_package_id",resultPackageId);
    }
    /**
     * 设置 [完工批次/序列号]
     */
    public void setLotProducedId(Integer lotProducedId){
        this.lotProducedId = lotProducedId ;
        this.modify("lot_produced_id",lotProducedId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [批次/序列号码]
     */
    public void setLotId(Integer lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }
    /**
     * 设置 [库存拣货]
     */
    public void setPickingId(Integer pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }
    /**
     * 设置 [工单]
     */
    public void setWorkorderId(Integer workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }
    /**
     * 设置 [从]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [源包裹]
     */
    public void setPackageId(Integer packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }
    /**
     * 设置 [所有者]
     */
    public void setOwnerId(Integer ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }
    /**
     * 设置 [包裹层级]
     */
    public void setPackageLevelId(Integer packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }
    /**
     * 设置 [至]
     */
    public void setLocationDestId(Integer locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }
    /**
     * 设置 [生产单]
     */
    public void setProductionId(Integer productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

}



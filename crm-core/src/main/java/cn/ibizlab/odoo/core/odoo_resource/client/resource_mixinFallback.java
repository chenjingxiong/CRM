package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_mixin] 服务对象接口
 */
@Component
public class resource_mixinFallback implements resource_mixinFeignClient{

    public Resource_mixin create(Resource_mixin resource_mixin){
            return null;
     }
    public Boolean createBatch(List<Resource_mixin> resource_mixins){
            return false;
     }


    public Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Resource_mixin get(Integer id){
            return null;
     }


    public Resource_mixin update(Integer id, Resource_mixin resource_mixin){
            return null;
     }
    public Boolean updateBatch(List<Resource_mixin> resource_mixins){
            return false;
     }



    public Page<Resource_mixin> select(){
            return null;
     }

    public Resource_mixin getDraft(){
            return null;
    }



}

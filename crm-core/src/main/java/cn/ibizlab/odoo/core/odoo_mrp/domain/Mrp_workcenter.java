package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作中心] 对象
 */
@Data
public class Mrp_workcenter extends EntityClient implements Serializable {

    /**
     * 每小时成本
     */
    @DEField(name = "costs_hour")
    @JSONField(name = "costs_hour")
    @JsonProperty("costs_hour")
    private Double costsHour;

    /**
     * 工作中心负载
     */
    @JSONField(name = "workcenter_load")
    @JsonProperty("workcenter_load")
    private Double workcenterLoad;

    /**
     * 工作中心状态
     */
    @DEField(name = "working_state")
    @JSONField(name = "working_state")
    @JsonProperty("working_state")
    private String workingState;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 延迟的订单合计
     */
    @JSONField(name = "workorder_late_count")
    @JsonProperty("workorder_late_count")
    private Integer workorderLateCount;

    /**
     * 效能
     */
    @JSONField(name = "performance")
    @JsonProperty("performance")
    private Integer performance;

    /**
     * OEE 目标
     */
    @DEField(name = "oee_target")
    @JSONField(name = "oee_target")
    @JsonProperty("oee_target")
    private Double oeeTarget;

    /**
     * 容量
     */
    @JSONField(name = "capacity")
    @JsonProperty("capacity")
    private Double capacity;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * # 工单
     */
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 时间日志
     */
    @JSONField(name = "time_ids")
    @JsonProperty("time_ids")
    private String timeIds;

    /**
     * 生产性时间
     */
    @JSONField(name = "productive_time")
    @JsonProperty("productive_time")
    private Double productiveTime;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 工艺行
     */
    @JSONField(name = "routing_line_ids")
    @JsonProperty("routing_line_ids")
    private String routingLineIds;

    /**
     * OEE
     */
    @JSONField(name = "oee")
    @JsonProperty("oee")
    private Double oee;

    /**
     * 说明
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 代码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 颜色
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 生产后时间
     */
    @DEField(name = "time_stop")
    @JSONField(name = "time_stop")
    @JsonProperty("time_stop")
    private Double timeStop;

    /**
     * 生产前时间
     */
    @DEField(name = "time_start")
    @JSONField(name = "time_start")
    @JsonProperty("time_start")
    private Double timeStart;

    /**
     * 待定的订单合计
     */
    @JSONField(name = "workorder_pending_count")
    @JsonProperty("workorder_pending_count")
    private Integer workorderPendingCount;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 阻塞时间
     */
    @JSONField(name = "blocked_time")
    @JsonProperty("blocked_time")
    private Double blockedTime;

    /**
     * # 读取工单
     */
    @JSONField(name = "workorder_ready_count")
    @JsonProperty("workorder_ready_count")
    private Integer workorderReadyCount;

    /**
     * 运行的订单合计
     */
    @JSONField(name = "workorder_progress_count")
    @JsonProperty("workorder_progress_count")
    private Integer workorderProgressCount;

    /**
     * 订单
     */
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    private String orderIds;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 时区
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 时间效率
     */
    @JSONField(name = "time_efficiency")
    @JsonProperty("time_efficiency")
    private Double timeEfficiency;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 工作时间
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 工作中心
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 资源
     */
    @DEField(name = "resource_id")
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    private Integer resourceId;


    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odooresource")
    @JsonProperty("odooresource")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource odooResource;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [每小时成本]
     */
    public void setCostsHour(Double costsHour){
        this.costsHour = costsHour ;
        this.modify("costs_hour",costsHour);
    }
    /**
     * 设置 [工作中心状态]
     */
    public void setWorkingState(String workingState){
        this.workingState = workingState ;
        this.modify("working_state",workingState);
    }
    /**
     * 设置 [OEE 目标]
     */
    public void setOeeTarget(Double oeeTarget){
        this.oeeTarget = oeeTarget ;
        this.modify("oee_target",oeeTarget);
    }
    /**
     * 设置 [容量]
     */
    public void setCapacity(Double capacity){
        this.capacity = capacity ;
        this.modify("capacity",capacity);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [说明]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [颜色]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [生产后时间]
     */
    public void setTimeStop(Double timeStop){
        this.timeStop = timeStop ;
        this.modify("time_stop",timeStop);
    }
    /**
     * 设置 [生产前时间]
     */
    public void setTimeStart(Double timeStart){
        this.timeStart = timeStart ;
        this.modify("time_start",timeStart);
    }
    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [资源]
     */
    public void setResourceId(Integer resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }

}



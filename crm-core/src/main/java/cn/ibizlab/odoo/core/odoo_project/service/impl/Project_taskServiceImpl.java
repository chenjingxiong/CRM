package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_taskService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_project.client.project_taskFeignClient;

/**
 * 实体[任务] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_taskServiceImpl implements IProject_taskService {

    @Autowired
    project_taskFeignClient project_taskFeignClient;


    @Override
    public boolean update(Project_task et) {
        Project_task rt = project_taskFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Project_task> list){
        project_taskFeignClient.updateBatch(list) ;
    }

    @Override
    public Project_task getDraft(Project_task et) {
        et=project_taskFeignClient.getDraft();
        return et;
    }

    @Override
    public Project_task get(Integer id) {
		Project_task et=project_taskFeignClient.get(id);
        if(et==null){
            et=new Project_task();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=project_taskFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        project_taskFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Project_task et) {
        Project_task rt = project_taskFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_task> list){
        project_taskFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_task> searchDefault(Project_taskSearchContext context) {
        Page<Project_task> project_tasks=project_taskFeignClient.searchDefault(context);
        return project_tasks;
    }


}



package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [repair_fee] 对象
 */
public interface Irepair_fee {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建者]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建者]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建者]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建者]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建者]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建者]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [已开票]
     */
    public void setInvoiced(String invoiced);
    
    /**
     * 设置 [已开票]
     */
    public String getInvoiced();

    /**
     * 获取 [已开票]脏标记
     */
    public boolean getInvoicedDirtyFlag();
    /**
     * 获取 [发票明细]
     */
    public void setInvoice_line_id(Integer invoice_line_id);
    
    /**
     * 设置 [发票明细]
     */
    public Integer getInvoice_line_id();

    /**
     * 获取 [发票明细]脏标记
     */
    public boolean getInvoice_line_idDirtyFlag();
    /**
     * 获取 [发票明细]
     */
    public void setInvoice_line_id_text(String invoice_line_id_text);
    
    /**
     * 设置 [发票明细]
     */
    public String getInvoice_line_id_text();

    /**
     * 获取 [发票明细]脏标记
     */
    public boolean getInvoice_line_id_textDirtyFlag();
    /**
     * 获取 [描述]
     */
    public void setName(String name);
    
    /**
     * 设置 [描述]
     */
    public String getName();

    /**
     * 获取 [描述]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [小计]
     */
    public void setPrice_subtotal(Double price_subtotal);
    
    /**
     * 设置 [小计]
     */
    public Double getPrice_subtotal();

    /**
     * 获取 [小计]脏标记
     */
    public boolean getPrice_subtotalDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setPrice_unit(Double price_unit);
    
    /**
     * 设置 [单价]
     */
    public Double getPrice_unit();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getPrice_unitDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [产品量度单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [产品量度单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [产品量度单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [产品量度单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [维修单编＃]
     */
    public void setRepair_id(Integer repair_id);
    
    /**
     * 设置 [维修单编＃]
     */
    public Integer getRepair_id();

    /**
     * 获取 [维修单编＃]脏标记
     */
    public boolean getRepair_idDirtyFlag();
    /**
     * 获取 [维修单编＃]
     */
    public void setRepair_id_text(String repair_id_text);
    
    /**
     * 设置 [维修单编＃]
     */
    public String getRepair_id_text();

    /**
     * 获取 [维修单编＃]脏标记
     */
    public boolean getRepair_id_textDirtyFlag();
    /**
     * 获取 [税]
     */
    public void setTax_id(String tax_id);
    
    /**
     * 设置 [税]
     */
    public String getTax_id();

    /**
     * 获取 [税]脏标记
     */
    public boolean getTax_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改时间]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改时间]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改时间]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

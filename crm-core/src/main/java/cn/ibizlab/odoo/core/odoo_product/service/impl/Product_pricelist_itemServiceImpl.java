package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelist_itemService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_pricelist_itemFeignClient;

/**
 * 实体[价格表明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_pricelist_itemServiceImpl implements IProduct_pricelist_itemService {

    @Autowired
    product_pricelist_itemFeignClient product_pricelist_itemFeignClient;


    @Override
    public Product_pricelist_item get(Integer id) {
		Product_pricelist_item et=product_pricelist_itemFeignClient.get(id);
        if(et==null){
            et=new Product_pricelist_item();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Product_pricelist_item et) {
        Product_pricelist_item rt = product_pricelist_itemFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_pricelist_item> list){
        product_pricelist_itemFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_pricelist_itemFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_pricelist_itemFeignClient.removeBatch(idList);
    }

    @Override
    public Product_pricelist_item getDraft(Product_pricelist_item et) {
        et=product_pricelist_itemFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_pricelist_item et) {
        Product_pricelist_item rt = product_pricelist_itemFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_pricelist_item> list){
        product_pricelist_itemFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean checkKey(Product_pricelist_item et) {
        return product_pricelist_itemFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(Product_pricelist_item et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!product_pricelist_itemFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Product_pricelist_item> list) {
        product_pricelist_itemFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context) {
        Page<Product_pricelist_item> product_pricelist_items=product_pricelist_itemFeignClient.searchDefault(context);
        return product_pricelist_items;
    }


}



package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_testFeignClient;

/**
 * 实体[测试资源模型] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_testServiceImpl implements IResource_testService {

    @Autowired
    resource_testFeignClient resource_testFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=resource_testFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_testFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Resource_test et) {
        Resource_test rt = resource_testFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_test> list){
        resource_testFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Resource_test et) {
        Resource_test rt = resource_testFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_test> list){
        resource_testFeignClient.createBatch(list) ;
    }

    @Override
    public Resource_test get(Integer id) {
		Resource_test et=resource_testFeignClient.get(id);
        if(et==null){
            et=new Resource_test();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Resource_test getDraft(Resource_test et) {
        et=resource_testFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_test> searchDefault(Resource_testSearchContext context) {
        Page<Resource_test> resource_tests=resource_testFeignClient.searchDefault(context);
        return resource_tests;
    }


}



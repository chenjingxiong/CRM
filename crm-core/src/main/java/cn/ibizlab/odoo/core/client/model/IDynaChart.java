package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [DynaChart] 对象
 */
public interface IDynaChart {

    /**
     * 获取 [建立时间]
     */
    public void setCreateDate(Timestamp createDate);
    
    /**
     * 设置 [建立时间]
     */
    public Timestamp getCreateDate();

    /**
     * 获取 [建立时间]脏标记
     */
    public boolean getCreateDateDirtyFlag();
    /**
     * 获取 [动态图表名称]
     */
    public void setDynaChartName(String dynaChartName);
    
    /**
     * 设置 [动态图表名称]
     */
    public String getDynaChartName();

    /**
     * 获取 [动态图表名称]脏标记
     */
    public boolean getDynaChartNameDirtyFlag();
    /**
     * 获取 [更新时间]
     */
    public void setUpdateDate(Timestamp updateDate);
    
    /**
     * 设置 [更新时间]
     */
    public Timestamp getUpdateDate();

    /**
     * 获取 [更新时间]脏标记
     */
    public boolean getUpdateDateDirtyFlag();
    /**
     * 获取 [模型标识]
     */
    public void setModelId(String modelId);
    
    /**
     * 设置 [模型标识]
     */
    public String getModelId();

    /**
     * 获取 [模型标识]脏标记
     */
    public boolean getModelIdDirtyFlag();
    /**
     * 获取 [应用标识]
     */
    public void setAppId(String appId);
    
    /**
     * 设置 [应用标识]
     */
    public String getAppId();

    /**
     * 获取 [应用标识]脏标记
     */
    public boolean getAppIdDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [模型]
     */
    public String getModel();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [更新人]
     */
    public void setUpdateMan(String updateMan);
    
    /**
     * 设置 [更新人]
     */
    public String getUpdateMan();

    /**
     * 获取 [更新人]脏标记
     */
    public boolean getUpdateManDirtyFlag();
    /**
     * 获取 [用户标识]
     */
    public void setUserId(String userId);
    
    /**
     * 设置 [用户标识]
     */
    public String getUserId();

    /**
     * 获取 [用户标识]脏标记
     */
    public boolean getUserIdDirtyFlag();
    /**
     * 获取 [建立人]
     */
    public void setCreateMan(String createMan);
    
    /**
     * 设置 [建立人]
     */
    public String getCreateMan();

    /**
     * 获取 [建立人]脏标记
     */
    public boolean getCreateManDirtyFlag();
    /**
     * 获取 [动态图表标识]
     */
    public void setDynaChartId(String dynaChartId);
    
    /**
     * 设置 [动态图表标识]
     */
    public String getDynaChartId();

    /**
     * 获取 [动态图表标识]脏标记
     */
    public boolean getDynaChartIdDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_service_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_service_type] 服务对象接口
 */
public interface Ifleet_service_typeClientService{

    public Ifleet_service_type createModel() ;

    public void updateBatch(List<Ifleet_service_type> fleet_service_types);

    public Page<Ifleet_service_type> fetchDefault(SearchContext context);

    public void update(Ifleet_service_type fleet_service_type);

    public void removeBatch(List<Ifleet_service_type> fleet_service_types);

    public void get(Ifleet_service_type fleet_service_type);

    public void createBatch(List<Ifleet_service_type> fleet_service_types);

    public void create(Ifleet_service_type fleet_service_type);

    public void remove(Ifleet_service_type fleet_service_type);

    public Page<Ifleet_service_type> select(SearchContext context);

    public void getDraft(Ifleet_service_type fleet_service_type);

}

package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_leave] 服务对象接口
 */
@Component
public class hr_leaveFallback implements hr_leaveFeignClient{

    public Page<Hr_leave> searchDefault(Hr_leaveSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Hr_leave create(Hr_leave hr_leave){
            return null;
     }
    public Boolean createBatch(List<Hr_leave> hr_leaves){
            return false;
     }

    public Hr_leave get(Integer id){
            return null;
     }


    public Hr_leave update(Integer id, Hr_leave hr_leave){
            return null;
     }
    public Boolean updateBatch(List<Hr_leave> hr_leaves){
            return false;
     }


    public Page<Hr_leave> select(){
            return null;
     }

    public Hr_leave getDraft(){
            return null;
    }



}

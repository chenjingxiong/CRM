package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_package_destination] 服务对象接口
 */
@Component
public class stock_package_destinationFallback implements stock_package_destinationFeignClient{

    public Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context){
            return null;
     }


    public Stock_package_destination update(Integer id, Stock_package_destination stock_package_destination){
            return null;
     }
    public Boolean updateBatch(List<Stock_package_destination> stock_package_destinations){
            return false;
     }



    public Stock_package_destination get(Integer id){
            return null;
     }


    public Stock_package_destination create(Stock_package_destination stock_package_destination){
            return null;
     }
    public Boolean createBatch(List<Stock_package_destination> stock_package_destinations){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Stock_package_destination> select(){
            return null;
     }

    public Stock_package_destination getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;


/**
 * 实体[Fleet_vehicle_odometer] 服务对象接口
 */
public interface IFleet_vehicle_odometerService{

    Fleet_vehicle_odometer getDraft(Fleet_vehicle_odometer et) ;
    boolean create(Fleet_vehicle_odometer et) ;
    void createBatch(List<Fleet_vehicle_odometer> list) ;
    boolean update(Fleet_vehicle_odometer et) ;
    void updateBatch(List<Fleet_vehicle_odometer> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Fleet_vehicle_odometer get(Integer key) ;
    Page<Fleet_vehicle_odometer> searchDefault(Fleet_vehicle_odometerSearchContext context) ;

}




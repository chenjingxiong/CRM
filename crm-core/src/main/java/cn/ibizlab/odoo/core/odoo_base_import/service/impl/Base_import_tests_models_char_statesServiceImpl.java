package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_states;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_statesSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_statesService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_char_statesFeignClient;

/**
 * 实体[测试:基本导入模型，字符状态] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_char_statesServiceImpl implements IBase_import_tests_models_char_statesService {

    @Autowired
    base_import_tests_models_char_statesFeignClient base_import_tests_models_char_statesFeignClient;


    @Override
    public Base_import_tests_models_char_states get(Integer id) {
		Base_import_tests_models_char_states et=base_import_tests_models_char_statesFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_char_states();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_import_tests_models_char_states et) {
        Base_import_tests_models_char_states rt = base_import_tests_models_char_statesFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_char_states> list){
        base_import_tests_models_char_statesFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_import_tests_models_char_states getDraft(Base_import_tests_models_char_states et) {
        et=base_import_tests_models_char_statesFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Base_import_tests_models_char_states et) {
        Base_import_tests_models_char_states rt = base_import_tests_models_char_statesFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_char_states> list){
        base_import_tests_models_char_statesFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_char_statesFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_char_statesFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_char_states> searchDefault(Base_import_tests_models_char_statesSearchContext context) {
        Page<Base_import_tests_models_char_states> base_import_tests_models_char_statess=base_import_tests_models_char_statesFeignClient.searchDefault(context);
        return base_import_tests_models_char_statess;
    }


}



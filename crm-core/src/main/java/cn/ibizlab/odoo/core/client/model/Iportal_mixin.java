package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [portal_mixin] 对象
 */
public interface Iportal_mixin {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问 URL]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问 URL]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问 URL]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

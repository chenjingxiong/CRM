package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_inventory] 服务对象接口
 */
@Component
public class stock_inventoryFallback implements stock_inventoryFeignClient{

    public Stock_inventory update(Integer id, Stock_inventory stock_inventory){
            return null;
     }
    public Boolean updateBatch(List<Stock_inventory> stock_inventories){
            return false;
     }


    public Page<Stock_inventory> searchDefault(Stock_inventorySearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_inventory create(Stock_inventory stock_inventory){
            return null;
     }
    public Boolean createBatch(List<Stock_inventory> stock_inventories){
            return false;
     }



    public Stock_inventory get(Integer id){
            return null;
     }


    public Page<Stock_inventory> select(){
            return null;
     }

    public Stock_inventory getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
public interface Istock_warn_insufficient_qty_repairClientService{

    public Istock_warn_insufficient_qty_repair createModel() ;

    public void get(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    public void create(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    public void createBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs);

    public void removeBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs);

    public Page<Istock_warn_insufficient_qty_repair> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs);

    public void remove(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    public void update(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    public Page<Istock_warn_insufficient_qty_repair> select(SearchContext context);

    public void getDraft(Istock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

}

package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_workorder] 服务对象接口
 */
@Component
public class mro_workorderFallback implements mro_workorderFeignClient{

    public Mro_workorder create(Mro_workorder mro_workorder){
            return null;
     }
    public Boolean createBatch(List<Mro_workorder> mro_workorders){
            return false;
     }

    public Mro_workorder update(Integer id, Mro_workorder mro_workorder){
            return null;
     }
    public Boolean updateBatch(List<Mro_workorder> mro_workorders){
            return false;
     }


    public Mro_workorder get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Mro_workorder> searchDefault(Mro_workorderSearchContext context){
            return null;
     }


    public Page<Mro_workorder> select(){
            return null;
     }

    public Mro_workorder getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;


/**
 * 实体[Res_bank] 服务对象接口
 */
public interface IRes_bankService{

    boolean create(Res_bank et) ;
    void createBatch(List<Res_bank> list) ;
    Res_bank get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_bank getDraft(Res_bank et) ;
    boolean update(Res_bank et) ;
    void updateBatch(List<Res_bank> list) ;
    Page<Res_bank> searchDefault(Res_bankSearchContext context) ;

}




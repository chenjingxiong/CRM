package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_alias;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_alias] 服务对象接口
 */
public interface Imail_aliasClientService{

    public Imail_alias createModel() ;

    public Page<Imail_alias> fetchDefault(SearchContext context);

    public void removeBatch(List<Imail_alias> mail_aliases);

    public void update(Imail_alias mail_alias);

    public void updateBatch(List<Imail_alias> mail_aliases);

    public void get(Imail_alias mail_alias);

    public void remove(Imail_alias mail_alias);

    public void create(Imail_alias mail_alias);

    public void createBatch(List<Imail_alias> mail_aliases);

    public Page<Imail_alias> select(SearchContext context);

    public void getDraft(Imail_alias mail_alias);

}

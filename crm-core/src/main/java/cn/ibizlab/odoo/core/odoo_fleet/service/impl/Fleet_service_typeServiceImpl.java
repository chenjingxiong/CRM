package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_service_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_service_typeFeignClient;

/**
 * 实体[车辆服务类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_service_typeServiceImpl implements IFleet_service_typeService {

    @Autowired
    fleet_service_typeFeignClient fleet_service_typeFeignClient;


    @Override
    public boolean update(Fleet_service_type et) {
        Fleet_service_type rt = fleet_service_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_service_type> list){
        fleet_service_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_service_type get(Integer id) {
		Fleet_service_type et=fleet_service_typeFeignClient.get(id);
        if(et==null){
            et=new Fleet_service_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Fleet_service_type getDraft(Fleet_service_type et) {
        et=fleet_service_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Fleet_service_type et) {
        Fleet_service_type rt = fleet_service_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_service_type> list){
        fleet_service_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_service_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_service_typeFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context) {
        Page<Fleet_service_type> fleet_service_types=fleet_service_typeFeignClient.searchDefault(context);
        return fleet_service_types;
    }


}



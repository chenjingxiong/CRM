package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_multi_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_multi_mixin] 服务对象接口
 */
public interface Iwebsite_multi_mixinClientService{

    public Iwebsite_multi_mixin createModel() ;

    public void get(Iwebsite_multi_mixin website_multi_mixin);

    public void create(Iwebsite_multi_mixin website_multi_mixin);

    public void remove(Iwebsite_multi_mixin website_multi_mixin);

    public Page<Iwebsite_multi_mixin> fetchDefault(SearchContext context);

    public void createBatch(List<Iwebsite_multi_mixin> website_multi_mixins);

    public void updateBatch(List<Iwebsite_multi_mixin> website_multi_mixins);

    public void update(Iwebsite_multi_mixin website_multi_mixin);

    public void removeBatch(List<Iwebsite_multi_mixin> website_multi_mixins);

    public Page<Iwebsite_multi_mixin> select(SearchContext context);

    public void getDraft(Iwebsite_multi_mixin website_multi_mixin);

}

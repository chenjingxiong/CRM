package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_dept;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
public interface Ihr_holidays_summary_deptClientService{

    public Ihr_holidays_summary_dept createModel() ;

    public void remove(Ihr_holidays_summary_dept hr_holidays_summary_dept);

    public Page<Ihr_holidays_summary_dept> fetchDefault(SearchContext context);

    public void get(Ihr_holidays_summary_dept hr_holidays_summary_dept);

    public void createBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts);

    public void create(Ihr_holidays_summary_dept hr_holidays_summary_dept);

    public void removeBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts);

    public void update(Ihr_holidays_summary_dept hr_holidays_summary_dept);

    public void updateBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts);

    public Page<Ihr_holidays_summary_dept> select(SearchContext context);

    public void getDraft(Ihr_holidays_summary_dept hr_holidays_summary_dept);

}

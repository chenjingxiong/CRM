package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_value;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_attribute_value] 服务对象接口
 */
public interface Iproduct_attribute_valueClientService{

    public Iproduct_attribute_value createModel() ;

    public void update(Iproduct_attribute_value product_attribute_value);

    public void removeBatch(List<Iproduct_attribute_value> product_attribute_values);

    public void updateBatch(List<Iproduct_attribute_value> product_attribute_values);

    public void remove(Iproduct_attribute_value product_attribute_value);

    public void createBatch(List<Iproduct_attribute_value> product_attribute_values);

    public void get(Iproduct_attribute_value product_attribute_value);

    public void create(Iproduct_attribute_value product_attribute_value);

    public Page<Iproduct_attribute_value> fetchDefault(SearchContext context);

    public Page<Iproduct_attribute_value> select(SearchContext context);

    public void getDraft(Iproduct_attribute_value product_attribute_value);

}

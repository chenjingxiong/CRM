package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_channelFeignClient;

/**
 * 实体[讨论频道] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_channelServiceImpl implements IMail_channelService {

    @Autowired
    mail_channelFeignClient mail_channelFeignClient;


    @Override
    public Mail_channel getDraft(Mail_channel et) {
        et=mail_channelFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_channel et) {
        Mail_channel rt = mail_channelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_channel> list){
        mail_channelFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_channelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_channelFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_channel et) {
        Mail_channel rt = mail_channelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_channel> list){
        mail_channelFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_channel get(Integer id) {
		Mail_channel et=mail_channelFeignClient.get(id);
        if(et==null){
            et=new Mail_channel();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_channel> searchDefault(Mail_channelSearchContext context) {
        Page<Mail_channel> mail_channels=mail_channelFeignClient.searchDefault(context);
        return mail_channels;
    }


}



package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenter_productivity_lossFeignClient;

/**
 * 实体[工作中心生产力损失] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenter_productivity_lossServiceImpl implements IMrp_workcenter_productivity_lossService {

    @Autowired
    mrp_workcenter_productivity_lossFeignClient mrp_workcenter_productivity_lossFeignClient;


    @Override
    public Mrp_workcenter_productivity_loss getDraft(Mrp_workcenter_productivity_loss et) {
        et=mrp_workcenter_productivity_lossFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mrp_workcenter_productivity_loss et) {
        Mrp_workcenter_productivity_loss rt = mrp_workcenter_productivity_lossFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter_productivity_loss> list){
        mrp_workcenter_productivity_lossFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_workcenter_productivity_lossFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_workcenter_productivity_lossFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_workcenter_productivity_loss get(Integer id) {
		Mrp_workcenter_productivity_loss et=mrp_workcenter_productivity_lossFeignClient.get(id);
        if(et==null){
            et=new Mrp_workcenter_productivity_loss();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mrp_workcenter_productivity_loss et) {
        Mrp_workcenter_productivity_loss rt = mrp_workcenter_productivity_lossFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_workcenter_productivity_loss> list){
        mrp_workcenter_productivity_lossFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        Page<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losss=mrp_workcenter_productivity_lossFeignClient.searchDefault(context);
        return mrp_workcenter_productivity_losss;
    }


}



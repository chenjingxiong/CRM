package cn.ibizlab.odoo.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Product_product] 查询条件对象
 */
@Slf4j
@Data
public class Product_productSearchContext extends SearchContextBase {
	private String n_activity_state_eq;//[活动状态]

	private String n_name_eq;//[名称]

	private String n_name_like;//[名称]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_product_tmpl_id_eq;//[产品模板]

}




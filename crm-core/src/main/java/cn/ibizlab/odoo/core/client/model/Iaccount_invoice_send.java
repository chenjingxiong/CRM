package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_invoice_send] 对象
 */
public interface Iaccount_invoice_send {

    /**
     * 获取 [有效域]
     */
    public void setActive_domain(String active_domain);
    
    /**
     * 设置 [有效域]
     */
    public String getActive_domain();

    /**
     * 获取 [有效域]脏标记
     */
    public boolean getActive_domainDirtyFlag();
    /**
     * 获取 [添加签名]
     */
    public void setAdd_sign(String add_sign);
    
    /**
     * 设置 [添加签名]
     */
    public String getAdd_sign();

    /**
     * 获取 [添加签名]脏标记
     */
    public boolean getAdd_signDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setAttachment_ids(String attachment_ids);
    
    /**
     * 设置 [附件]
     */
    public String getAttachment_ids();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getAttachment_idsDirtyFlag();
    /**
     * 获取 [作者头像]
     */
    public void setAuthor_avatar(byte[] author_avatar);
    
    /**
     * 设置 [作者头像]
     */
    public byte[] getAuthor_avatar();

    /**
     * 获取 [作者头像]脏标记
     */
    public boolean getAuthor_avatarDirtyFlag();
    /**
     * 获取 [作者]
     */
    public void setAuthor_id(Integer author_id);
    
    /**
     * 设置 [作者]
     */
    public Integer getAuthor_id();

    /**
     * 获取 [作者]脏标记
     */
    public boolean getAuthor_idDirtyFlag();
    /**
     * 获取 [删除邮件]
     */
    public void setAuto_delete(String auto_delete);
    
    /**
     * 设置 [删除邮件]
     */
    public String getAuto_delete();

    /**
     * 获取 [删除邮件]脏标记
     */
    public boolean getAuto_deleteDirtyFlag();
    /**
     * 获取 [删除消息副本]
     */
    public void setAuto_delete_message(String auto_delete_message);
    
    /**
     * 设置 [删除消息副本]
     */
    public String getAuto_delete_message();

    /**
     * 获取 [删除消息副本]脏标记
     */
    public boolean getAuto_delete_messageDirtyFlag();
    /**
     * 获取 [内容]
     */
    public void setBody(String body);
    
    /**
     * 设置 [内容]
     */
    public String getBody();

    /**
     * 获取 [内容]脏标记
     */
    public boolean getBodyDirtyFlag();
    /**
     * 获取 [频道]
     */
    public void setChannel_ids(String channel_ids);
    
    /**
     * 设置 [频道]
     */
    public String getChannel_ids();

    /**
     * 获取 [频道]脏标记
     */
    public boolean getChannel_idsDirtyFlag();
    /**
     * 获取 [下级信息]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [下级信息]
     */
    public String getChild_ids();

    /**
     * 获取 [下级信息]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [邮件撰写者]
     */
    public void setComposer_id(Integer composer_id);
    
    /**
     * 设置 [邮件撰写者]
     */
    public Integer getComposer_id();

    /**
     * 获取 [邮件撰写者]脏标记
     */
    public boolean getComposer_idDirtyFlag();
    /**
     * 获取 [写作模式]
     */
    public void setComposition_mode(String composition_mode);
    
    /**
     * 设置 [写作模式]
     */
    public String getComposition_mode();

    /**
     * 获取 [写作模式]脏标记
     */
    public boolean getComposition_modeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [来自]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [来自]
     */
    public String getEmail_from();

    /**
     * 获取 [来自]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [有误差]
     */
    public void setHas_error(String has_error);
    
    /**
     * 设置 [有误差]
     */
    public String getHas_error();

    /**
     * 获取 [有误差]脏标记
     */
    public boolean getHas_errorDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_ids(String invoice_ids);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_ids();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idsDirtyFlag();
    /**
     * 获取 [不发送的发票]
     */
    public void setInvoice_without_email(String invoice_without_email);
    
    /**
     * 设置 [不发送的发票]
     */
    public String getInvoice_without_email();

    /**
     * 获取 [不发送的发票]脏标记
     */
    public boolean getInvoice_without_emailDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setIs_email(String is_email);
    
    /**
     * 设置 [EMail]
     */
    public String getIs_email();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getIs_emailDirtyFlag();
    /**
     * 获取 [记录内部备注]
     */
    public void setIs_log(String is_log);
    
    /**
     * 设置 [记录内部备注]
     */
    public String getIs_log();

    /**
     * 获取 [记录内部备注]脏标记
     */
    public boolean getIs_logDirtyFlag();
    /**
     * 获取 [打印]
     */
    public void setIs_print(String is_print);
    
    /**
     * 设置 [打印]
     */
    public String getIs_print();

    /**
     * 获取 [打印]脏标记
     */
    public boolean getIs_printDirtyFlag();
    /**
     * 获取 [布局]
     */
    public void setLayout(String layout);
    
    /**
     * 设置 [布局]
     */
    public String getLayout();

    /**
     * 获取 [布局]脏标记
     */
    public boolean getLayoutDirtyFlag();
    /**
     * 获取 [信]
     */
    public void setLetter_ids(String letter_ids);
    
    /**
     * 设置 [信]
     */
    public String getLetter_ids();

    /**
     * 获取 [信]脏标记
     */
    public boolean getLetter_idsDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setMailing_list_ids(String mailing_list_ids);
    
    /**
     * 设置 [邮件列表]
     */
    public String getMailing_list_ids();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getMailing_list_idsDirtyFlag();
    /**
     * 获取 [邮件活动类型]
     */
    public void setMail_activity_type_id(Integer mail_activity_type_id);
    
    /**
     * 设置 [邮件活动类型]
     */
    public Integer getMail_activity_type_id();

    /**
     * 获取 [邮件活动类型]脏标记
     */
    public boolean getMail_activity_type_idDirtyFlag();
    /**
     * 获取 [邮件发送服务器]
     */
    public void setMail_server_id(Integer mail_server_id);
    
    /**
     * 设置 [邮件发送服务器]
     */
    public Integer getMail_server_id();

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    public boolean getMail_server_idDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);
    
    /**
     * 设置 [群发邮件营销]
     */
    public Integer getMass_mailing_campaign_id();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getMass_mailing_campaign_idDirtyFlag();
    /**
     * 获取 [群发邮件]
     */
    public void setMass_mailing_id(Integer mass_mailing_id);
    
    /**
     * 设置 [群发邮件]
     */
    public Integer getMass_mailing_id();

    /**
     * 获取 [群发邮件]脏标记
     */
    public boolean getMass_mailing_idDirtyFlag();
    /**
     * 获取 [群发邮件标题]
     */
    public void setMass_mailing_name(String mass_mailing_name);
    
    /**
     * 设置 [群发邮件标题]
     */
    public String getMass_mailing_name();

    /**
     * 获取 [群发邮件标题]脏标记
     */
    public boolean getMass_mailing_nameDirtyFlag();
    /**
     * 获取 [Message-Id]
     */
    public void setMessage_id(String message_id);
    
    /**
     * 设置 [Message-Id]
     */
    public String getMessage_id();

    /**
     * 获取 [Message-Id]脏标记
     */
    public boolean getMessage_idDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setMessage_type(String message_type);
    
    /**
     * 设置 [类型]
     */
    public String getMessage_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getMessage_typeDirtyFlag();
    /**
     * 获取 [相关的文档模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [相关的文档模型]
     */
    public String getModel();

    /**
     * 获取 [相关的文档模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [审核状态]
     */
    public void setModeration_status(String moderation_status);
    
    /**
     * 设置 [审核状态]
     */
    public String getModeration_status();

    /**
     * 获取 [审核状态]脏标记
     */
    public boolean getModeration_statusDirtyFlag();
    /**
     * 获取 [审核人]
     */
    public void setModerator_id(Integer moderator_id);
    
    /**
     * 设置 [审核人]
     */
    public Integer getModerator_id();

    /**
     * 获取 [审核人]脏标记
     */
    public boolean getModerator_idDirtyFlag();
    /**
     * 获取 [待处理]
     */
    public void setNeedaction(String needaction);
    
    /**
     * 设置 [待处理]
     */
    public String getNeedaction();

    /**
     * 获取 [待处理]脏标记
     */
    public boolean getNeedactionDirtyFlag();
    /**
     * 获取 [待处理的业务伙伴]
     */
    public void setNeedaction_partner_ids(String needaction_partner_ids);
    
    /**
     * 设置 [待处理的业务伙伴]
     */
    public String getNeedaction_partner_ids();

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    public boolean getNeedaction_partner_idsDirtyFlag();
    /**
     * 获取 [需要审核]
     */
    public void setNeed_moderation(String need_moderation);
    
    /**
     * 设置 [需要审核]
     */
    public String getNeed_moderation();

    /**
     * 获取 [需要审核]脏标记
     */
    public boolean getNeed_moderationDirtyFlag();
    /**
     * 获取 [通知]
     */
    public void setNotification_ids(String notification_ids);
    
    /**
     * 设置 [通知]
     */
    public String getNotification_ids();

    /**
     * 获取 [通知]脏标记
     */
    public boolean getNotification_idsDirtyFlag();
    /**
     * 获取 [通知关注者]
     */
    public void setNotify(String notify);
    
    /**
     * 设置 [通知关注者]
     */
    public String getNotify();

    /**
     * 获取 [通知关注者]脏标记
     */
    public boolean getNotifyDirtyFlag();
    /**
     * 获取 [线程无应答]
     */
    public void setNo_auto_thread(String no_auto_thread);
    
    /**
     * 设置 [线程无应答]
     */
    public String getNo_auto_thread();

    /**
     * 获取 [线程无应答]脏标记
     */
    public boolean getNo_auto_threadDirtyFlag();
    /**
     * 获取 [上级消息]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级消息]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级消息]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [额外的联系人]
     */
    public void setPartner_ids(String partner_ids);
    
    /**
     * 设置 [额外的联系人]
     */
    public String getPartner_ids();

    /**
     * 获取 [额外的联系人]脏标记
     */
    public boolean getPartner_idsDirtyFlag();
    /**
     * 获取 [已打印]
     */
    public void setPrinted(String printed);
    
    /**
     * 设置 [已打印]
     */
    public String getPrinted();

    /**
     * 获取 [已打印]脏标记
     */
    public boolean getPrintedDirtyFlag();
    /**
     * 获取 [相关评级]
     */
    public void setRating_ids(String rating_ids);
    
    /**
     * 设置 [相关评级]
     */
    public String getRating_ids();

    /**
     * 获取 [相关评级]脏标记
     */
    public boolean getRating_idsDirtyFlag();
    /**
     * 获取 [评级值]
     */
    public void setRating_value(Double rating_value);
    
    /**
     * 设置 [评级值]
     */
    public Double getRating_value();

    /**
     * 获取 [评级值]脏标记
     */
    public boolean getRating_valueDirtyFlag();
    /**
     * 获取 [消息记录名称]
     */
    public void setRecord_name(String record_name);
    
    /**
     * 设置 [消息记录名称]
     */
    public String getRecord_name();

    /**
     * 获取 [消息记录名称]脏标记
     */
    public boolean getRecord_nameDirtyFlag();
    /**
     * 获取 [回复 至]
     */
    public void setReply_to(String reply_to);
    
    /**
     * 设置 [回复 至]
     */
    public String getReply_to();

    /**
     * 获取 [回复 至]脏标记
     */
    public boolean getReply_toDirtyFlag();
    /**
     * 获取 [相关文档编号]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [相关文档编号]
     */
    public Integer getRes_id();

    /**
     * 获取 [相关文档编号]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [邮戳(s)]
     */
    public void setSnailmail_cost(Double snailmail_cost);
    
    /**
     * 设置 [邮戳(s)]
     */
    public Double getSnailmail_cost();

    /**
     * 获取 [邮戳(s)]脏标记
     */
    public boolean getSnailmail_costDirtyFlag();
    /**
     * 获取 [透过邮递]
     */
    public void setSnailmail_is_letter(String snailmail_is_letter);
    
    /**
     * 设置 [透过邮递]
     */
    public String getSnailmail_is_letter();

    /**
     * 获取 [透过邮递]脏标记
     */
    public boolean getSnailmail_is_letterDirtyFlag();
    /**
     * 获取 [标星号邮件]
     */
    public void setStarred(String starred);
    
    /**
     * 设置 [标星号邮件]
     */
    public String getStarred();

    /**
     * 获取 [标星号邮件]脏标记
     */
    public boolean getStarredDirtyFlag();
    /**
     * 获取 [收藏夹]
     */
    public void setStarred_partner_ids(String starred_partner_ids);
    
    /**
     * 设置 [收藏夹]
     */
    public String getStarred_partner_ids();

    /**
     * 获取 [收藏夹]脏标记
     */
    public boolean getStarred_partner_idsDirtyFlag();
    /**
     * 获取 [主题]
     */
    public void setSubject(String subject);
    
    /**
     * 设置 [主题]
     */
    public String getSubject();

    /**
     * 获取 [主题]脏标记
     */
    public boolean getSubjectDirtyFlag();
    /**
     * 获取 [子类型]
     */
    public void setSubtype_id(Integer subtype_id);
    
    /**
     * 设置 [子类型]
     */
    public Integer getSubtype_id();

    /**
     * 获取 [子类型]脏标记
     */
    public boolean getSubtype_idDirtyFlag();
    /**
     * 获取 [使用模版]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [使用模版]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [使用模版]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [使用模版]
     */
    public void setTemplate_id_text(String template_id_text);
    
    /**
     * 设置 [使用模版]
     */
    public String getTemplate_id_text();

    /**
     * 获取 [使用模版]脏标记
     */
    public boolean getTemplate_id_textDirtyFlag();
    /**
     * 获取 [追踪值]
     */
    public void setTracking_value_ids(String tracking_value_ids);
    
    /**
     * 设置 [追踪值]
     */
    public String getTracking_value_ids();

    /**
     * 获取 [追踪值]脏标记
     */
    public boolean getTracking_value_idsDirtyFlag();
    /**
     * 获取 [使用有效域]
     */
    public void setUse_active_domain(String use_active_domain);
    
    /**
     * 设置 [使用有效域]
     */
    public String getUse_active_domain();

    /**
     * 获取 [使用有效域]脏标记
     */
    public boolean getUse_active_domainDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [已发布]
     */
    public String getWebsite_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_autocomplete_syncService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partner_autocomplete_syncFeignClient;

/**
 * 实体[合作伙伴自动完成同步] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_autocomplete_syncServiceImpl implements IRes_partner_autocomplete_syncService {

    @Autowired
    res_partner_autocomplete_syncFeignClient res_partner_autocomplete_syncFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=res_partner_autocomplete_syncFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partner_autocomplete_syncFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Res_partner_autocomplete_sync et) {
        Res_partner_autocomplete_sync rt = res_partner_autocomplete_syncFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner_autocomplete_sync> list){
        res_partner_autocomplete_syncFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_partner_autocomplete_sync getDraft(Res_partner_autocomplete_sync et) {
        et=res_partner_autocomplete_syncFeignClient.getDraft();
        return et;
    }

    @Override
    public Res_partner_autocomplete_sync get(Integer id) {
		Res_partner_autocomplete_sync et=res_partner_autocomplete_syncFeignClient.get(id);
        if(et==null){
            et=new Res_partner_autocomplete_sync();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Res_partner_autocomplete_sync et) {
        Res_partner_autocomplete_sync rt = res_partner_autocomplete_syncFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_autocomplete_sync> list){
        res_partner_autocomplete_syncFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_autocomplete_sync> searchDefault(Res_partner_autocomplete_syncSearchContext context) {
        Page<Res_partner_autocomplete_sync> res_partner_autocomplete_syncs=res_partner_autocomplete_syncFeignClient.searchDefault(context);
        return res_partner_autocomplete_syncs;
    }


}



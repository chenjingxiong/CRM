package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_registration] 服务对象接口
 */
@FeignClient(value = "odoo-event", contextId = "event-registration", fallback = event_registrationFallback.class)
public interface event_registrationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/{id}")
    Event_registration update(@PathVariable("id") Integer id,@RequestBody Event_registration event_registration);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/batch")
    Boolean updateBatch(@RequestBody List<Event_registration> event_registrations);



    @RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/event_registrations/{id}")
    Event_registration get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/event_registrations")
    Event_registration create(@RequestBody Event_registration event_registration);

    @RequestMapping(method = RequestMethod.POST, value = "/event_registrations/batch")
    Boolean createBatch(@RequestBody List<Event_registration> event_registrations);



    @RequestMapping(method = RequestMethod.POST, value = "/event_registrations/searchdefault")
    Page<Event_registration> searchDefault(@RequestBody Event_registrationSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/event_registrations/select")
    Page<Event_registration> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_registrations/getdraft")
    Event_registration getDraft();


}

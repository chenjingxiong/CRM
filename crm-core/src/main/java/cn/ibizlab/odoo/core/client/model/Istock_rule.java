package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_rule] 对象
 */
public interface Istock_rule {

    /**
     * 获取 [动作]
     */
    public void setAction(String action);
    
    /**
     * 设置 [动作]
     */
    public String getAction();

    /**
     * 获取 [动作]脏标记
     */
    public boolean getActionDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [自动移动]
     */
    public void setAuto(String auto);
    
    /**
     * 设置 [自动移动]
     */
    public String getAuto();

    /**
     * 获取 [自动移动]脏标记
     */
    public boolean getAutoDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [延迟]
     */
    public void setDelay(Integer delay);
    
    /**
     * 设置 [延迟]
     */
    public Integer getDelay();

    /**
     * 获取 [延迟]脏标记
     */
    public boolean getDelayDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [固定的补货组]
     */
    public void setGroup_id(Integer group_id);
    
    /**
     * 设置 [固定的补货组]
     */
    public Integer getGroup_id();

    /**
     * 获取 [固定的补货组]脏标记
     */
    public boolean getGroup_idDirtyFlag();
    /**
     * 获取 [补货组的传播]
     */
    public void setGroup_propagation_option(String group_propagation_option);
    
    /**
     * 设置 [补货组的传播]
     */
    public String getGroup_propagation_option();

    /**
     * 获取 [补货组的传播]脏标记
     */
    public boolean getGroup_propagation_optionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [目的位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [目的位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [目的位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [目的位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_src_id(Integer location_src_id);
    
    /**
     * 设置 [源位置]
     */
    public Integer getLocation_src_id();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_src_idDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setLocation_src_id_text(String location_src_id_text);
    
    /**
     * 设置 [源位置]
     */
    public String getLocation_src_id_text();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getLocation_src_id_textDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [业务伙伴地址]
     */
    public void setPartner_address_id(Integer partner_address_id);
    
    /**
     * 设置 [业务伙伴地址]
     */
    public Integer getPartner_address_id();

    /**
     * 获取 [业务伙伴地址]脏标记
     */
    public boolean getPartner_address_idDirtyFlag();
    /**
     * 获取 [业务伙伴地址]
     */
    public void setPartner_address_id_text(String partner_address_id_text);
    
    /**
     * 设置 [业务伙伴地址]
     */
    public String getPartner_address_id_text();

    /**
     * 获取 [业务伙伴地址]脏标记
     */
    public boolean getPartner_address_id_textDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id(Integer picking_type_id);
    
    /**
     * 设置 [作业类型]
     */
    public Integer getPicking_type_id();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_idDirtyFlag();
    /**
     * 获取 [作业类型]
     */
    public void setPicking_type_id_text(String picking_type_id_text);
    
    /**
     * 设置 [作业类型]
     */
    public String getPicking_type_id_text();

    /**
     * 获取 [作业类型]脏标记
     */
    public boolean getPicking_type_id_textDirtyFlag();
    /**
     * 获取 [移动供应方法]
     */
    public void setProcure_method(String procure_method);
    
    /**
     * 设置 [移动供应方法]
     */
    public String getProcure_method();

    /**
     * 获取 [移动供应方法]脏标记
     */
    public boolean getProcure_methodDirtyFlag();
    /**
     * 获取 [传播取消以及拆分]
     */
    public void setPropagate(String propagate);
    
    /**
     * 设置 [传播取消以及拆分]
     */
    public String getPropagate();

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    public boolean getPropagateDirtyFlag();
    /**
     * 获取 [传播的仓库]
     */
    public void setPropagate_warehouse_id(Integer propagate_warehouse_id);
    
    /**
     * 设置 [传播的仓库]
     */
    public Integer getPropagate_warehouse_id();

    /**
     * 获取 [传播的仓库]脏标记
     */
    public boolean getPropagate_warehouse_idDirtyFlag();
    /**
     * 获取 [传播的仓库]
     */
    public void setPropagate_warehouse_id_text(String propagate_warehouse_id_text);
    
    /**
     * 设置 [传播的仓库]
     */
    public String getPropagate_warehouse_id_text();

    /**
     * 获取 [传播的仓库]脏标记
     */
    public boolean getPropagate_warehouse_id_textDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_id(Integer route_id);
    
    /**
     * 设置 [路线]
     */
    public Integer getRoute_id();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_idDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_id_text(String route_id_text);
    
    /**
     * 设置 [路线]
     */
    public String getRoute_id_text();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_id_textDirtyFlag();
    /**
     * 获取 [路线序列]
     */
    public void setRoute_sequence(Integer route_sequence);
    
    /**
     * 设置 [路线序列]
     */
    public Integer getRoute_sequence();

    /**
     * 获取 [路线序列]脏标记
     */
    public boolean getRoute_sequenceDirtyFlag();
    /**
     * 获取 [规则消息]
     */
    public void setRule_message(String rule_message);
    
    /**
     * 设置 [规则消息]
     */
    public String getRule_message();

    /**
     * 获取 [规则消息]脏标记
     */
    public boolean getRule_messageDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id(Integer warehouse_id);
    
    /**
     * 设置 [仓库]
     */
    public Integer getWarehouse_id();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_id_text(String warehouse_id_text);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_id_text();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

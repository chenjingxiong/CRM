package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_menu] 服务对象接口
 */
@FeignClient(value = "odoo-website", contextId = "website-menu", fallback = website_menuFallback.class)
public interface website_menuFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/website_menus/{id}")
    Website_menu get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/website_menus")
    Website_menu create(@RequestBody Website_menu website_menu);

    @RequestMapping(method = RequestMethod.POST, value = "/website_menus/batch")
    Boolean createBatch(@RequestBody List<Website_menu> website_menus);


    @RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/website_menus/{id}")
    Website_menu update(@PathVariable("id") Integer id,@RequestBody Website_menu website_menu);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_menus/batch")
    Boolean updateBatch(@RequestBody List<Website_menu> website_menus);



    @RequestMapping(method = RequestMethod.POST, value = "/website_menus/searchdefault")
    Page<Website_menu> searchDefault(@RequestBody Website_menuSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/website_menus/select")
    Page<Website_menu> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_menus/getdraft")
    Website_menu getDraft();


}

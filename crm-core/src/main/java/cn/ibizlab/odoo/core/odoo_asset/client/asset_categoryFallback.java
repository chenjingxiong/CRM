package cn.ibizlab.odoo.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[asset_category] 服务对象接口
 */
@Component
public class asset_categoryFallback implements asset_categoryFeignClient{

    public Asset_category create(Asset_category asset_category){
            return null;
     }
    public Boolean createBatch(List<Asset_category> asset_categories){
            return false;
     }

    public Asset_category get(Integer id){
            return null;
     }


    public Page<Asset_category> searchDefault(Asset_categorySearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Asset_category update(Integer id, Asset_category asset_category){
            return null;
     }
    public Boolean updateBatch(List<Asset_category> asset_categories){
            return false;
     }



    public Page<Asset_category> select(){
            return null;
     }

    public Asset_category getDraft(){
            return null;
    }



}

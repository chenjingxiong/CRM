package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_import_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_import_wizardFeignClient;

/**
 * 实体[从文件中导入你的供应商账单。] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_import_wizardServiceImpl implements IAccount_invoice_import_wizardService {

    @Autowired
    account_invoice_import_wizardFeignClient account_invoice_import_wizardFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_import_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_import_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_invoice_import_wizard et) {
        Account_invoice_import_wizard rt = account_invoice_import_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_import_wizard> list){
        account_invoice_import_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_invoice_import_wizard get(Integer id) {
		Account_invoice_import_wizard et=account_invoice_import_wizardFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_import_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_invoice_import_wizard getDraft(Account_invoice_import_wizard et) {
        et=account_invoice_import_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_invoice_import_wizard et) {
        Account_invoice_import_wizard rt = account_invoice_import_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_import_wizard> list){
        account_invoice_import_wizardFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_import_wizard> searchDefault(Account_invoice_import_wizardSearchContext context) {
        Page<Account_invoice_import_wizard> account_invoice_import_wizards=account_invoice_import_wizardFeignClient.searchDefault(context);
        return account_invoice_import_wizards;
    }


}



package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_resource] 服务对象接口
 */
@Component
public class resource_resourceFallback implements resource_resourceFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Resource_resource get(Integer id){
            return null;
     }



    public Page<Resource_resource> searchDefault(Resource_resourceSearchContext context){
            return null;
     }


    public Resource_resource update(Integer id, Resource_resource resource_resource){
            return null;
     }
    public Boolean updateBatch(List<Resource_resource> resource_resources){
            return false;
     }


    public Resource_resource create(Resource_resource resource_resource){
            return null;
     }
    public Boolean createBatch(List<Resource_resource> resource_resources){
            return false;
     }

    public Page<Resource_resource> select(){
            return null;
     }

    public Resource_resource getDraft(){
            return null;
    }



}

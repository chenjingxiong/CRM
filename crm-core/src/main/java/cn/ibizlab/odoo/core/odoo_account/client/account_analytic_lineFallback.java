package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_line] 服务对象接口
 */
@Component
public class account_analytic_lineFallback implements account_analytic_lineFeignClient{

    public Account_analytic_line get(Integer id){
            return null;
     }


    public Account_analytic_line create(Account_analytic_line account_analytic_line){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_line> account_analytic_lines){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Account_analytic_line update(Integer id, Account_analytic_line account_analytic_line){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_line> account_analytic_lines){
            return false;
     }


    public Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context){
            return null;
     }


    public Page<Account_analytic_line> select(){
            return null;
     }

    public Account_analytic_line getDraft(){
            return null;
    }



}

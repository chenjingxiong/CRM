package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;


/**
 * 实体[Mro_task] 服务对象接口
 */
public interface IMro_taskService{

    boolean update(Mro_task et) ;
    void updateBatch(List<Mro_task> list) ;
    Mro_task getDraft(Mro_task et) ;
    Mro_task get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mro_task et) ;
    void createBatch(List<Mro_task> list) ;
    Page<Mro_task> searchDefault(Mro_taskSearchContext context) ;

}




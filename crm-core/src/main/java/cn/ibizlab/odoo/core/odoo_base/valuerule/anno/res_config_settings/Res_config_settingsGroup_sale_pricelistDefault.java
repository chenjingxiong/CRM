package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_config_settings;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_config_settings.Res_config_settingsGroup_sale_pricelistDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_config_settings
 * 属性：Group_sale_pricelist
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_config_settingsGroup_sale_pricelistDefaultValidator.class})
public @interface Res_config_settingsGroup_sale_pricelistDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

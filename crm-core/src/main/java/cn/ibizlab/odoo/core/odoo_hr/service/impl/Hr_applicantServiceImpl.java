package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicantService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_applicantFeignClient;

/**
 * 实体[申请人] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_applicantServiceImpl implements IHr_applicantService {

    @Autowired
    hr_applicantFeignClient hr_applicantFeignClient;


    @Override
    public boolean create(Hr_applicant et) {
        Hr_applicant rt = hr_applicantFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_applicant> list){
        hr_applicantFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_applicant getDraft(Hr_applicant et) {
        et=hr_applicantFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_applicantFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_applicantFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Hr_applicant et) {
        Hr_applicant rt = hr_applicantFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_applicant> list){
        hr_applicantFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_applicant get(Integer id) {
		Hr_applicant et=hr_applicantFeignClient.get(id);
        if(et==null){
            et=new Hr_applicant();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context) {
        Page<Hr_applicant> hr_applicants=hr_applicantFeignClient.searchDefault(context);
        return hr_applicants;
    }


}



package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challenge_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_challenge_lineFeignClient;

/**
 * 实体[游戏化挑战的一般目标] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_challenge_lineServiceImpl implements IGamification_challenge_lineService {

    @Autowired
    gamification_challenge_lineFeignClient gamification_challenge_lineFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=gamification_challenge_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        gamification_challenge_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Gamification_challenge_line et) {
        Gamification_challenge_line rt = gamification_challenge_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_challenge_line> list){
        gamification_challenge_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Gamification_challenge_line get(Integer id) {
		Gamification_challenge_line et=gamification_challenge_lineFeignClient.get(id);
        if(et==null){
            et=new Gamification_challenge_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Gamification_challenge_line et) {
        Gamification_challenge_line rt = gamification_challenge_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Gamification_challenge_line> list){
        gamification_challenge_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Gamification_challenge_line getDraft(Gamification_challenge_line et) {
        et=gamification_challenge_lineFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_challenge_line> searchDefault(Gamification_challenge_lineSearchContext context) {
        Page<Gamification_challenge_line> gamification_challenge_lines=gamification_challenge_lineFeignClient.searchDefault(context);
        return gamification_challenge_lines;
    }


}



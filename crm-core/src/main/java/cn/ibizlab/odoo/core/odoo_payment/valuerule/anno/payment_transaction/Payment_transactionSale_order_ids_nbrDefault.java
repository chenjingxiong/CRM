package cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_transaction;

import cn.ibizlab.odoo.core.odoo_payment.valuerule.validator.payment_transaction.Payment_transactionSale_order_ids_nbrDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Payment_transaction
 * 属性：Sale_order_ids_nbr
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Payment_transactionSale_order_ids_nbrDefaultValidator.class})
public @interface Payment_transactionSale_order_ids_nbrDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

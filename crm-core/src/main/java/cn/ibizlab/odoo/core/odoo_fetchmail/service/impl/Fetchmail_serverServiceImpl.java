package cn.ibizlab.odoo.core.odoo_fetchmail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import cn.ibizlab.odoo.core.odoo_fetchmail.service.IFetchmail_serverService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fetchmail.client.fetchmail_serverFeignClient;

/**
 * 实体[Incoming Mail Server] 服务对象接口实现
 */
@Slf4j
@Service
public class Fetchmail_serverServiceImpl implements IFetchmail_serverService {

    @Autowired
    fetchmail_serverFeignClient fetchmail_serverFeignClient;


    @Override
    public Fetchmail_server get(Integer id) {
		Fetchmail_server et=fetchmail_serverFeignClient.get(id);
        if(et==null){
            et=new Fetchmail_server();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Fetchmail_server et) {
        Fetchmail_server rt = fetchmail_serverFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fetchmail_server> list){
        fetchmail_serverFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Fetchmail_server et) {
        Fetchmail_server rt = fetchmail_serverFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fetchmail_server> list){
        fetchmail_serverFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fetchmail_serverFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fetchmail_serverFeignClient.removeBatch(idList);
    }

    @Override
    public Fetchmail_server getDraft(Fetchmail_server et) {
        et=fetchmail_serverFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context) {
        Page<Fetchmail_server> fetchmail_servers=fetchmail_serverFeignClient.searchDefault(context);
        return fetchmail_servers;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_activity_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity_type] 服务对象接口
 */
public interface Imail_activity_typeClientService{

    public Imail_activity_type createModel() ;

    public Page<Imail_activity_type> fetchDefault(SearchContext context);

    public void update(Imail_activity_type mail_activity_type);

    public void create(Imail_activity_type mail_activity_type);

    public void get(Imail_activity_type mail_activity_type);

    public void updateBatch(List<Imail_activity_type> mail_activity_types);

    public void removeBatch(List<Imail_activity_type> mail_activity_types);

    public void remove(Imail_activity_type mail_activity_type);

    public void createBatch(List<Imail_activity_type> mail_activity_types);

    public Page<Imail_activity_type> select(SearchContext context);

    public void getDraft(Imail_activity_type mail_activity_type);

    public void checkKey(Imail_activity_type mail_activity_type);

    public void save(Imail_activity_type mail_activity_type);

}

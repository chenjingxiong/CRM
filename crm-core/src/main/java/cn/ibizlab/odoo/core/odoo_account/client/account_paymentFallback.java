package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_payment] 服务对象接口
 */
@Component
public class account_paymentFallback implements account_paymentFeignClient{


    public Account_payment create(Account_payment account_payment){
            return null;
     }
    public Boolean createBatch(List<Account_payment> account_payments){
            return false;
     }

    public Account_payment update(Integer id, Account_payment account_payment){
            return null;
     }
    public Boolean updateBatch(List<Account_payment> account_payments){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Account_payment> searchDefault(Account_paymentSearchContext context){
            return null;
     }


    public Account_payment get(Integer id){
            return null;
     }


    public Page<Account_payment> select(){
            return null;
     }

    public Boolean checkKey(Account_payment account_payment){
            return false;
     }


    public Account_payment getDraft(){
            return null;
    }



    public Boolean save(Account_payment account_payment){
            return false;
     }
    public Boolean saveBatch(List<Account_payment> account_payments){
            return false;
     }

}

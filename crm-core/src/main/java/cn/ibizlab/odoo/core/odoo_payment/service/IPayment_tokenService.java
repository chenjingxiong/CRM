package cn.ibizlab.odoo.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;


/**
 * 实体[Payment_token] 服务对象接口
 */
public interface IPayment_tokenService{

    boolean create(Payment_token et) ;
    void createBatch(List<Payment_token> list) ;
    boolean update(Payment_token et) ;
    void updateBatch(List<Payment_token> list) ;
    Payment_token getDraft(Payment_token et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Payment_token get(Integer key) ;
    Page<Payment_token> searchDefault(Payment_tokenSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品] 对象
 */
@Data
public class Product_product extends EntityClient implements Serializable {

    /**
     * 变种卖家
     */
    @JSONField(name = "variant_seller_ids")
    @JsonProperty("variant_seller_ids")
    private String variantSellerIds;

    /**
     * 模板属性值
     */
    @JSONField(name = "product_template_attribute_value_ids")
    @JsonProperty("product_template_attribute_value_ids")
    private String productTemplateAttributeValueIds;

    /**
     * 产品
     */
    @JSONField(name = "product_variant_ids")
    @JsonProperty("product_variant_ids")
    private String productVariantIds;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 体积
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * 标价
     */
    @JSONField(name = "lst_price")
    @JsonProperty("lst_price")
    private Double lstPrice;

    /**
     * 有效的产品属性
     */
    @JSONField(name = "valid_product_attribute_ids")
    @JsonProperty("valid_product_attribute_ids")
    private String validProductAttributeIds;

    /**
     * 库存FIFO手工凭证
     */
    @JSONField(name = "stock_fifo_manual_move_ids")
    @JsonProperty("stock_fifo_manual_move_ids")
    private String stockFifoManualMoveIds;

    /**
     * 即时库存
     */
    @JSONField(name = "stock_quant_ids")
    @JsonProperty("stock_quant_ids")
    private String stockQuantIds;

    /**
     * 进项税
     */
    @JSONField(name = "supplier_taxes_id")
    @JsonProperty("supplier_taxes_id")
    private String supplierTaxesId;

    /**
     * 价格表明细
     */
    @JSONField(name = "pricelist_item_ids")
    @JsonProperty("pricelist_item_ids")
    private String pricelistItemIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 附件产品
     */
    @JSONField(name = "accessory_product_ids")
    @JsonProperty("accessory_product_ids")
    private String accessoryProductIds;

    /**
     * 供应商
     */
    @JSONField(name = "seller_ids")
    @JsonProperty("seller_ids")
    private String sellerIds;

    /**
     * Valid Product Attribute Values Without No Variant Attributes
     */
    @JSONField(name = "valid_product_attribute_value_wnva_ids")
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    private String validProductAttributeValueWnvaIds;

    /**
     * 客户单号
     */
    @JSONField(name = "partner_ref")
    @JsonProperty("partner_ref")
    private String partnerRef;

    /**
     * 图片
     */
    @JSONField(name = "product_image_ids")
    @JsonProperty("product_image_ids")
    private String productImageIds;

    /**
     * 已生产
     */
    @JSONField(name = "mrp_product_qty")
    @JsonProperty("mrp_product_qty")
    private Double mrpProductQty;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * Valid Product Attribute Lines
     */
    @JSONField(name = "valid_product_template_attribute_line_ids")
    @JsonProperty("valid_product_template_attribute_line_ids")
    private String validProductTemplateAttributeLineIds;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 网站产品目录
     */
    @JSONField(name = "public_categ_ids")
    @JsonProperty("public_categ_ids")
    private String publicCategIds;

    /**
     * 活动入场券
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;

    /**
     * 价格
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 产品属性
     */
    @JSONField(name = "attribute_line_ids")
    @JsonProperty("attribute_line_ids")
    private String attributeLineIds;

    /**
     * 预测数量
     */
    @JSONField(name = "virtual_available")
    @JsonProperty("virtual_available")
    private Double virtualAvailable;

    /**
     * 订货规则
     */
    @JSONField(name = "nbr_reordering_rules")
    @JsonProperty("nbr_reordering_rules")
    private Integer nbrReorderingRules;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 网站价格差异
     */
    @JSONField(name = "website_price_difference")
    @JsonProperty("website_price_difference")
    private String websitePriceDifference;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 购物车数量
     */
    @JSONField(name = "cart_qty")
    @JsonProperty("cart_qty")
    private Integer cartQty;

    /**
     * 网站公开价格
     */
    @JSONField(name = "website_public_price")
    @JsonProperty("website_public_price")
    private Double websitePublicPrice;

    /**
     * 评级
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * BOM组件
     */
    @JSONField(name = "bom_line_ids")
    @JsonProperty("bom_line_ids")
    private String bomLineIds;

    /**
     * 网站价格
     */
    @JSONField(name = "website_price")
    @JsonProperty("website_price")
    private Double websitePrice;

    /**
     * 出向
     */
    @JSONField(name = "outgoing_qty")
    @JsonProperty("outgoing_qty")
    private Double outgoingQty;

    /**
     * 已售出
     */
    @JSONField(name = "sales_count")
    @JsonProperty("sales_count")
    private Double salesCount;

    /**
     * Valid Product Attributes Without No Variant Attributes
     */
    @JSONField(name = "valid_product_attribute_wnva_ids")
    @JsonProperty("valid_product_attribute_wnva_ids")
    private String validProductAttributeWnvaIds;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * Valid Existing Variants
     */
    @JSONField(name = "valid_existing_variant_ids")
    @JsonProperty("valid_existing_variant_ids")
    private String validExistingVariantIds;

    /**
     * 库存货币价值
     */
    @JSONField(name = "stock_value_currency_id")
    @JsonProperty("stock_value_currency_id")
    private Integer stockValueCurrencyId;

    /**
     * 值
     */
    @JSONField(name = "stock_value")
    @JsonProperty("stock_value")
    private Double stockValue;

    /**
     * 样式
     */
    @JSONField(name = "website_style_ids")
    @JsonProperty("website_style_ids")
    private String websiteStyleIds;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 重量
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 物料清单
     */
    @JSONField(name = "bom_ids")
    @JsonProperty("bom_ids")
    private String bomIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     */
    @JSONField(name = "valid_product_template_attribute_line_wnva_ids")
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    private String validProductTemplateAttributeLineWnvaIds;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 有效的产品属性值
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    private String validProductAttributeValueIds;

    /**
     * 在手数量
     */
    @JSONField(name = "qty_available")
    @JsonProperty("qty_available")
    private Double qtyAvailable;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 变体图像
     */
    @JSONField(name = "image_variant")
    @JsonProperty("image_variant")
    private byte[] imageVariant;

    /**
     * 库存移动
     */
    @JSONField(name = "stock_move_ids")
    @JsonProperty("stock_move_ids")
    private String stockMoveIds;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 库存FIFO实时计价
     */
    @JSONField(name = "stock_fifo_real_time_aml_ids")
    @JsonProperty("stock_fifo_real_time_aml_ids")
    private String stockFifoRealTimeAmlIds;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 参照
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 重订货最小数量
     */
    @JSONField(name = "reordering_min_qty")
    @JsonProperty("reordering_min_qty")
    private Double reorderingMinQty;

    /**
     * 大尺寸图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 路线
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 销项税
     */
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;

    /**
     * # 物料清单
     */
    @JSONField(name = "bom_count")
    @JsonProperty("bom_count")
    private Integer bomCount;

    /**
     * 动作数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 产品包裹
     */
    @JSONField(name = "packaging_ids")
    @JsonProperty("packaging_ids")
    private String packagingIds;

    /**
     * Valid Archived Variants
     */
    @JSONField(name = "valid_archived_variant_ids")
    @JsonProperty("valid_archived_variant_ids")
    private String validArchivedVariantIds;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 价格表项目
     */
    @JSONField(name = "item_ids")
    @JsonProperty("item_ids")
    private String itemIds;

    /**
     * 已采购
     */
    @JSONField(name = "purchased_product_qty")
    @JsonProperty("purchased_product_qty")
    private Double purchasedProductQty;

    /**
     * 重订货最大数量
     */
    @JSONField(name = "reordering_max_qty")
    @JsonProperty("reordering_max_qty")
    private Double reorderingMaxQty;

    /**
     * 最小库存规则
     */
    @JSONField(name = "orderpoint_ids")
    @JsonProperty("orderpoint_ids")
    private String orderpointIds;

    /**
     * 可选产品
     */
    @JSONField(name = "optional_product_ids")
    @JsonProperty("optional_product_ids")
    private String optionalProductIds;

    /**
     * 是产品变体
     */
    @JSONField(name = "is_product_variant")
    @JsonProperty("is_product_variant")
    private String isProductVariant;

    /**
     * # BOM 使用的地方
     */
    @JSONField(name = "used_in_bom_count")
    @JsonProperty("used_in_bom_count")
    private Integer usedInBomCount;

    /**
     * 数量
     */
    @JSONField(name = "qty_at_date")
    @JsonProperty("qty_at_date")
    private Double qtyAtDate;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 成本
     */
    @JSONField(name = "standard_price")
    @JsonProperty("standard_price")
    private Double standardPrice;

    /**
     * 属性值
     */
    @JSONField(name = "attribute_value_ids")
    @JsonProperty("attribute_value_ids")
    private String attributeValueIds;

    /**
     * 变体价格额外
     */
    @JSONField(name = "price_extra")
    @JsonProperty("price_extra")
    private Double priceExtra;

    /**
     * BOM产品变体.
     */
    @JSONField(name = "variant_bom_ids")
    @JsonProperty("variant_bom_ids")
    private String variantBomIds;

    /**
     * 替代产品
     */
    @JSONField(name = "alternative_product_ids")
    @JsonProperty("alternative_product_ids")
    private String alternativeProductIds;

    /**
     * 内部参考
     */
    @DEField(name = "default_code")
    @JSONField(name = "default_code")
    @JsonProperty("default_code")
    private String defaultCode;

    /**
     * 类别路线
     */
    @JSONField(name = "route_from_categ_ids")
    @JsonProperty("route_from_categ_ids")
    private String routeFromCategIds;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 入库
     */
    @JSONField(name = "incoming_qty")
    @JsonProperty("incoming_qty")
    private Double incomingQty;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 追踪
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 拣货说明
     */
    @JSONField(name = "description_picking")
    @JsonProperty("description_picking")
    private String descriptionPicking;

    /**
     * 库存出货科目
     */
    @JSONField(name = "property_stock_account_output")
    @JsonProperty("property_stock_account_output")
    private Integer propertyStockAccountOutput;

    /**
     * 销售
     */
    @JSONField(name = "sale_ok")
    @JsonProperty("sale_ok")
    private String saleOk;

    /**
     * 网站的说明
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 网站opengraph图像
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 公司
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 称重
     */
    @JSONField(name = "to_weight")
    @JsonProperty("to_weight")
    private String toWeight;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 收货说明
     */
    @JSONField(name = "description_pickingin")
    @JsonProperty("description_pickingin")
    private String descriptionPickingin;

    /**
     * 销售价格
     */
    @JSONField(name = "list_price")
    @JsonProperty("list_price")
    private Double listPrice;

    /**
     * 隐藏费用政策
     */
    @JSONField(name = "hide_expense_policy")
    @JsonProperty("hide_expense_policy")
    private String hideExpensePolicy;

    /**
     * 销售说明
     */
    @JSONField(name = "description_sale")
    @JsonProperty("description_sale")
    private String descriptionSale;

    /**
     * 成本方法
     */
    @JSONField(name = "cost_method")
    @JsonProperty("cost_method")
    private String costMethod;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 销售订单行消息
     */
    @JSONField(name = "sale_line_warn_msg")
    @JsonProperty("sale_line_warn_msg")
    private String saleLineWarnMsg;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 出租
     */
    @JSONField(name = "rental")
    @JsonProperty("rental")
    private String rental;

    /**
     * 价格差异科目
     */
    @JSONField(name = "property_account_creditor_price_difference")
    @JsonProperty("property_account_creditor_price_difference")
    private Integer propertyAccountCreditorPriceDifference;

    /**
     * 重量计量单位标签
     */
    @JSONField(name = "weight_uom_name")
    @JsonProperty("weight_uom_name")
    private String weightUomName;

    /**
     * 成本币种
     */
    @JSONField(name = "cost_currency_id")
    @JsonProperty("cost_currency_id")
    private Integer costCurrencyId;

    /**
     * 库存进货科目
     */
    @JSONField(name = "property_stock_account_input")
    @JsonProperty("property_stock_account_input")
    private Integer propertyStockAccountInput;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 制造提前期(日)
     */
    @JSONField(name = "produce_delay")
    @JsonProperty("produce_delay")
    private Double produceDelay;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 最新反馈评级
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;

    /**
     * 尺寸 Y
     */
    @JSONField(name = "website_size_y")
    @JsonProperty("website_size_y")
    private Integer websiteSizeY;

    /**
     * 是一张活动票吗？
     */
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private String eventOk;

    /**
     * 库存可用性
     */
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;

    /**
     * 采购
     */
    @JSONField(name = "purchase_ok")
    @JsonProperty("purchase_ok")
    private String purchaseOk;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最新值评级
     */
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * 网站meta标题
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 最新图像评级
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 采购说明
     */
    @JSONField(name = "description_purchase")
    @JsonProperty("description_purchase")
    private String descriptionPurchase;

    /**
     * 网站
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 报销
     */
    @JSONField(name = "can_be_expensed")
    @JsonProperty("can_be_expensed")
    private String canBeExpensed;

    /**
     * 销售订单行
     */
    @JSONField(name = "sale_line_warn")
    @JsonProperty("sale_line_warn")
    private String saleLineWarn;

    /**
     * 尺寸 X
     */
    @JSONField(name = "website_size_x")
    @JsonProperty("website_size_x")
    private Integer websiteSizeX;

    /**
     * 自动采购
     */
    @JSONField(name = "service_to_purchase")
    @JsonProperty("service_to_purchase")
    private String serviceToPurchase;

    /**
     * 网站序列
     */
    @JSONField(name = "website_sequence")
    @JsonProperty("website_sequence")
    private Integer websiteSequence;

    /**
     * 库存位置
     */
    @JSONField(name = "property_stock_inventory")
    @JsonProperty("property_stock_inventory")
    private Integer propertyStockInventory;

    /**
     * 地点
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 库存计价
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    private String propertyValuation;

    /**
     * 已发布
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 重开收据规则
     */
    @JSONField(name = "expense_policy")
    @JsonProperty("expense_policy")
    private String expensePolicy;

    /**
     * 测量的重量单位
     */
    @JSONField(name = "weight_uom_id")
    @JsonProperty("weight_uom_id")
    private Integer weightUomId;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 生产位置
     */
    @JSONField(name = "property_stock_production")
    @JsonProperty("property_stock_production")
    private Integer propertyStockProduction;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 网站meta关键词
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 出库单说明
     */
    @JSONField(name = "description_pickingout")
    @JsonProperty("description_pickingout")
    private String descriptionPickingout;

    /**
     * 价格表
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 评级数
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 网站元说明
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 计价
     */
    @JSONField(name = "valuation")
    @JsonProperty("valuation")
    private String valuation;

    /**
     * 开票策略
     */
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    private String invoicePolicy;

    /**
     * 采购订单明细的消息
     */
    @JSONField(name = "purchase_line_warn_msg")
    @JsonProperty("purchase_line_warn_msg")
    private String purchaseLineWarnMsg;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 收入科目
     */
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Integer propertyAccountIncomeId;

    /**
     * 成本方法
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    private String propertyCostMethod;

    /**
     * 产品种类
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * Can be Part
     */
    @JSONField(name = "isparts")
    @JsonProperty("isparts")
    private String isparts;

    /**
     * 计量单位
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    private Integer uomId;

    /**
     * 产品
     */
    @JSONField(name = "product_variant_id")
    @JsonProperty("product_variant_id")
    private Integer productVariantId;

    /**
     * 产品类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 控制策略
     */
    @JSONField(name = "purchase_method")
    @JsonProperty("purchase_method")
    private String purchaseMethod;

    /**
     * 负责人
     */
    @JSONField(name = "responsible_id")
    @JsonProperty("responsible_id")
    private Integer responsibleId;

    /**
     * 跟踪服务
     */
    @JSONField(name = "service_type")
    @JsonProperty("service_type")
    private String serviceType;

    /**
     * 单位名称
     */
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    private String uomName;

    /**
     * 可用阈值
     */
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 采购订单行
     */
    @JSONField(name = "purchase_line_warn")
    @JsonProperty("purchase_line_warn")
    private String purchaseLineWarn;

    /**
     * # 产品变体
     */
    @JSONField(name = "product_variant_count")
    @JsonProperty("product_variant_count")
    private Integer productVariantCount;

    /**
     * POS类别
     */
    @JSONField(name = "pos_categ_id")
    @JsonProperty("pos_categ_id")
    private Integer posCategId;

    /**
     * 自定义消息
     */
    @JSONField(name = "custom_message")
    @JsonProperty("custom_message")
    private String customMessage;

    /**
     * 费用科目
     */
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Integer propertyAccountExpenseId;

    /**
     * 客户前置时间
     */
    @JSONField(name = "sale_delay")
    @JsonProperty("sale_delay")
    private Double saleDelay;

    /**
     * 采购计量单位
     */
    @JSONField(name = "uom_po_id")
    @JsonProperty("uom_po_id")
    private Integer uomPoId;

    /**
     * POS可用
     */
    @JSONField(name = "available_in_pos")
    @JsonProperty("available_in_pos")
    private String availableInPos;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 产品模板
     */
    @DEField(name = "product_tmpl_id")
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;


    /**
     * 
     */
    @JSONField(name = "odooproducttmpl")
    @JsonProperty("odooproducttmpl")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_template odooProductTmpl;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [体积]
     */
    public void setVolume(Double volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [重量]
     */
    public void setWeight(Double weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }
    /**
     * 设置 [内部参考]
     */
    public void setDefaultCode(String defaultCode){
        this.defaultCode = defaultCode ;
        this.modify("default_code",defaultCode);
    }
    /**
     * 设置 [产品模板]
     */
    public void setProductTmplId(Integer productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

}



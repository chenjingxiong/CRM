package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;


/**
 * 实体[Stock_return_picking_line] 服务对象接口
 */
public interface IStock_return_picking_lineService{

    boolean update(Stock_return_picking_line et) ;
    void updateBatch(List<Stock_return_picking_line> list) ;
    boolean create(Stock_return_picking_line et) ;
    void createBatch(List<Stock_return_picking_line> list) ;
    Stock_return_picking_line get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_return_picking_line getDraft(Stock_return_picking_line et) ;
    Page<Stock_return_picking_line> searchDefault(Stock_return_picking_lineSearchContext context) ;

}




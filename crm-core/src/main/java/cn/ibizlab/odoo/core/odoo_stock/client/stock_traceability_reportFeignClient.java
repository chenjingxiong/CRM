package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_traceability_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_traceability_report] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-traceability-report", fallback = stock_traceability_reportFallback.class)
public interface stock_traceability_reportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports")
    Stock_traceability_report create(@RequestBody Stock_traceability_report stock_traceability_report);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/batch")
    Boolean createBatch(@RequestBody List<Stock_traceability_report> stock_traceability_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_traceability_reports/{id}")
    Stock_traceability_report update(@PathVariable("id") Integer id,@RequestBody Stock_traceability_report stock_traceability_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_traceability_reports/batch")
    Boolean updateBatch(@RequestBody List<Stock_traceability_report> stock_traceability_reports);




    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_traceability_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_traceability_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_traceability_reports/{id}")
    Stock_traceability_report get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_traceability_reports/searchdefault")
    Page<Stock_traceability_report> searchDefault(@RequestBody Stock_traceability_reportSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_traceability_reports/select")
    Page<Stock_traceability_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_traceability_reports/getdraft")
    Stock_traceability_report getDraft();


}

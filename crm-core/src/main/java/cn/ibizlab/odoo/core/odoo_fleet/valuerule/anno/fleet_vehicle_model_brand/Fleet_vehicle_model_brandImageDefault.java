package cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_model_brand;

import cn.ibizlab.odoo.core.odoo_fleet.valuerule.validator.fleet_vehicle_model_brand.Fleet_vehicle_model_brandImageDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Fleet_vehicle_model_brand
 * 属性：Image
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Fleet_vehicle_model_brandImageDefaultValidator.class})
public @interface Fleet_vehicle_model_brandImageDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

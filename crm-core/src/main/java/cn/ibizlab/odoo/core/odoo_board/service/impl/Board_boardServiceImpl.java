package cn.ibizlab.odoo.core.odoo_board.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_board.domain.Board_board;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;
import cn.ibizlab.odoo.core.odoo_board.service.IBoard_boardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_board.client.board_boardFeignClient;

/**
 * 实体[仪表板] 服务对象接口实现
 */
@Slf4j
@Service
public class Board_boardServiceImpl implements IBoard_boardService {

    @Autowired
    board_boardFeignClient board_boardFeignClient;


    @Override
    public Board_board getDraft(Board_board et) {
        et=board_boardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Board_board et) {
        Board_board rt = board_boardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Board_board> list){
        board_boardFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=board_boardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        board_boardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Board_board et) {
        Board_board rt = board_boardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Board_board> list){
        board_boardFeignClient.createBatch(list) ;
    }

    @Override
    public Board_board get(Integer id) {
		Board_board et=board_boardFeignClient.get(id);
        if(et==null){
            et=new Board_board();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Board_board> searchDefault(Board_boardSearchContext context) {
        Page<Board_board> board_boards=board_boardFeignClient.searchDefault(context);
        return board_boards;
    }


}



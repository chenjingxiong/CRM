package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_overprocessed_transferService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_overprocessed_transferFeignClient;

/**
 * 实体[已过帐移动] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_overprocessed_transferServiceImpl implements IStock_overprocessed_transferService {

    @Autowired
    stock_overprocessed_transferFeignClient stock_overprocessed_transferFeignClient;


    @Override
    public Stock_overprocessed_transfer getDraft(Stock_overprocessed_transfer et) {
        et=stock_overprocessed_transferFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_overprocessed_transfer et) {
        Stock_overprocessed_transfer rt = stock_overprocessed_transferFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_overprocessed_transfer> list){
        stock_overprocessed_transferFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_overprocessed_transfer get(Integer id) {
		Stock_overprocessed_transfer et=stock_overprocessed_transferFeignClient.get(id);
        if(et==null){
            et=new Stock_overprocessed_transfer();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_overprocessed_transfer et) {
        Stock_overprocessed_transfer rt = stock_overprocessed_transferFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_overprocessed_transfer> list){
        stock_overprocessed_transferFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_overprocessed_transferFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_overprocessed_transferFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_overprocessed_transfer> searchDefault(Stock_overprocessed_transferSearchContext context) {
        Page<Stock_overprocessed_transfer> stock_overprocessed_transfers=stock_overprocessed_transferFeignClient.searchDefault(context);
        return stock_overprocessed_transfers;
    }


}



package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_users_log] 服务对象接口
 */
@Component
public class res_users_logFallback implements res_users_logFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Res_users_log create(Res_users_log res_users_log){
            return null;
     }
    public Boolean createBatch(List<Res_users_log> res_users_logs){
            return false;
     }

    public Page<Res_users_log> searchDefault(Res_users_logSearchContext context){
            return null;
     }


    public Res_users_log update(Integer id, Res_users_log res_users_log){
            return null;
     }
    public Boolean updateBatch(List<Res_users_log> res_users_logs){
            return false;
     }


    public Res_users_log get(Integer id){
            return null;
     }




    public Page<Res_users_log> select(){
            return null;
     }

    public Res_users_log getDraft(){
            return null;
    }



}

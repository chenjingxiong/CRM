package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_move_line] 对象
 */
public interface Iaccount_move_line {

    /**
     * 获取 [科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [货币金额]
     */
    public void setAmount_currency(Double amount_currency);
    
    /**
     * 设置 [货币金额]
     */
    public Double getAmount_currency();

    /**
     * 获取 [货币金额]脏标记
     */
    public boolean getAmount_currencyDirtyFlag();
    /**
     * 获取 [残值金额]
     */
    public void setAmount_residual(Double amount_residual);
    
    /**
     * 设置 [残值金额]
     */
    public Double getAmount_residual();

    /**
     * 获取 [残值金额]脏标记
     */
    public boolean getAmount_residualDirtyFlag();
    /**
     * 获取 [外币残余金额]
     */
    public void setAmount_residual_currency(Double amount_residual_currency);
    
    /**
     * 设置 [外币残余金额]
     */
    public Double getAmount_residual_currency();

    /**
     * 获取 [外币残余金额]脏标记
     */
    public boolean getAmount_residual_currencyDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id(Integer analytic_account_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAnalytic_account_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id_text(String analytic_account_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAnalytic_account_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_id_textDirtyFlag();
    /**
     * 获取 [分析明细行]
     */
    public void setAnalytic_line_ids(String analytic_line_ids);
    
    /**
     * 设置 [分析明细行]
     */
    public String getAnalytic_line_ids();

    /**
     * 获取 [分析明细行]脏标记
     */
    public boolean getAnalytic_line_idsDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [余额]
     */
    public void setBalance(Double balance);
    
    /**
     * 设置 [余额]
     */
    public Double getBalance();

    /**
     * 获取 [余额]脏标记
     */
    public boolean getBalanceDirtyFlag();
    /**
     * 获取 [现金余额]
     */
    public void setBalance_cash_basis(Double balance_cash_basis);
    
    /**
     * 设置 [现金余额]
     */
    public Double getBalance_cash_basis();

    /**
     * 获取 [现金余额]脏标记
     */
    public boolean getBalance_cash_basisDirtyFlag();
    /**
     * 获取 [无催款]
     */
    public void setBlocked(String blocked);
    
    /**
     * 设置 [无催款]
     */
    public String getBlocked();

    /**
     * 获取 [无催款]脏标记
     */
    public boolean getBlockedDirtyFlag();
    /**
     * 获取 [公司货币]
     */
    public void setCompany_currency_id(Integer company_currency_id);
    
    /**
     * 设置 [公司货币]
     */
    public Integer getCompany_currency_id();

    /**
     * 获取 [公司货币]脏标记
     */
    public boolean getCompany_currency_idDirtyFlag();
    /**
     * 获取 [公司货币]
     */
    public void setCompany_currency_id_text(String company_currency_id_text);
    
    /**
     * 设置 [公司货币]
     */
    public String getCompany_currency_id_text();

    /**
     * 获取 [公司货币]脏标记
     */
    public boolean getCompany_currency_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [对方]
     */
    public void setCounterpart(String counterpart);
    
    /**
     * 设置 [对方]
     */
    public String getCounterpart();

    /**
     * 获取 [对方]脏标记
     */
    public boolean getCounterpartDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [贷方]
     */
    public void setCredit(Double credit);
    
    /**
     * 设置 [贷方]
     */
    public Double getCredit();

    /**
     * 获取 [贷方]脏标记
     */
    public boolean getCreditDirtyFlag();
    /**
     * 获取 [贷方现金基础]
     */
    public void setCredit_cash_basis(Double credit_cash_basis);
    
    /**
     * 设置 [贷方现金基础]
     */
    public Double getCredit_cash_basis();

    /**
     * 获取 [贷方现金基础]脏标记
     */
    public boolean getCredit_cash_basisDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [到期日期]
     */
    public void setDate_maturity(Timestamp date_maturity);
    
    /**
     * 设置 [到期日期]
     */
    public Timestamp getDate_maturity();

    /**
     * 获取 [到期日期]脏标记
     */
    public boolean getDate_maturityDirtyFlag();
    /**
     * 获取 [借方]
     */
    public void setDebit(Double debit);
    
    /**
     * 设置 [借方]
     */
    public Double getDebit();

    /**
     * 获取 [借方]脏标记
     */
    public boolean getDebitDirtyFlag();
    /**
     * 获取 [借记现金基础]
     */
    public void setDebit_cash_basis(Double debit_cash_basis);
    
    /**
     * 设置 [借记现金基础]
     */
    public Double getDebit_cash_basis();

    /**
     * 获取 [借记现金基础]脏标记
     */
    public boolean getDebit_cash_basisDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [费用]
     */
    public void setExpense_id(Integer expense_id);
    
    /**
     * 设置 [费用]
     */
    public Integer getExpense_id();

    /**
     * 获取 [费用]脏标记
     */
    public boolean getExpense_idDirtyFlag();
    /**
     * 获取 [费用]
     */
    public void setExpense_id_text(String expense_id_text);
    
    /**
     * 设置 [费用]
     */
    public String getExpense_id_text();

    /**
     * 获取 [费用]脏标记
     */
    public boolean getExpense_id_textDirtyFlag();
    /**
     * 获取 [匹配号码]
     */
    public void setFull_reconcile_id(Integer full_reconcile_id);
    
    /**
     * 设置 [匹配号码]
     */
    public Integer getFull_reconcile_id();

    /**
     * 获取 [匹配号码]脏标记
     */
    public boolean getFull_reconcile_idDirtyFlag();
    /**
     * 获取 [匹配号码]
     */
    public void setFull_reconcile_id_text(String full_reconcile_id_text);
    
    /**
     * 设置 [匹配号码]
     */
    public String getFull_reconcile_id_text();

    /**
     * 获取 [匹配号码]脏标记
     */
    public boolean getFull_reconcile_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id(Integer invoice_id);
    
    /**
     * 设置 [发票]
     */
    public Integer getInvoice_id();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_idDirtyFlag();
    /**
     * 获取 [发票]
     */
    public void setInvoice_id_text(String invoice_id_text);
    
    /**
     * 设置 [发票]
     */
    public String getInvoice_id_text();

    /**
     * 获取 [发票]脏标记
     */
    public boolean getInvoice_id_textDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [匹配的贷方]
     */
    public void setMatched_credit_ids(String matched_credit_ids);
    
    /**
     * 设置 [匹配的贷方]
     */
    public String getMatched_credit_ids();

    /**
     * 获取 [匹配的贷方]脏标记
     */
    public boolean getMatched_credit_idsDirtyFlag();
    /**
     * 获取 [匹配的借记卡]
     */
    public void setMatched_debit_ids(String matched_debit_ids);
    
    /**
     * 设置 [匹配的借记卡]
     */
    public String getMatched_debit_ids();

    /**
     * 获取 [匹配的借记卡]脏标记
     */
    public boolean getMatched_debit_idsDirtyFlag();
    /**
     * 获取 [日记账分录]
     */
    public void setMove_id(Integer move_id);
    
    /**
     * 设置 [日记账分录]
     */
    public Integer getMove_id();

    /**
     * 获取 [日记账分录]脏标记
     */
    public boolean getMove_idDirtyFlag();
    /**
     * 获取 [日记账分录]
     */
    public void setMove_id_text(String move_id_text);
    
    /**
     * 设置 [日记账分录]
     */
    public String getMove_id_text();

    /**
     * 获取 [日记账分录]脏标记
     */
    public boolean getMove_id_textDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setName(String name);
    
    /**
     * 设置 [标签]
     */
    public String getName();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [记叙]
     */
    public void setNarration(String narration);
    
    /**
     * 设置 [记叙]
     */
    public String getNarration();

    /**
     * 获取 [记叙]脏标记
     */
    public boolean getNarrationDirtyFlag();
    /**
     * 获取 [上级状态]
     */
    public void setParent_state(String parent_state);
    
    /**
     * 设置 [上级状态]
     */
    public String getParent_state();

    /**
     * 获取 [上级状态]脏标记
     */
    public boolean getParent_stateDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [发起人付款]
     */
    public void setPayment_id(Integer payment_id);
    
    /**
     * 设置 [发起人付款]
     */
    public Integer getPayment_id();

    /**
     * 获取 [发起人付款]脏标记
     */
    public boolean getPayment_idDirtyFlag();
    /**
     * 获取 [发起人付款]
     */
    public void setPayment_id_text(String payment_id_text);
    
    /**
     * 设置 [发起人付款]
     */
    public String getPayment_id_text();

    /**
     * 获取 [发起人付款]脏标记
     */
    public boolean getPayment_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setQuantity(Double quantity);
    
    /**
     * 设置 [数量]
     */
    public Double getQuantity();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getQuantityDirtyFlag();
    /**
     * 获取 [重新计算税额]
     */
    public void setRecompute_tax_line(String recompute_tax_line);
    
    /**
     * 设置 [重新计算税额]
     */
    public String getRecompute_tax_line();

    /**
     * 获取 [重新计算税额]脏标记
     */
    public boolean getRecompute_tax_lineDirtyFlag();
    /**
     * 获取 [已核销]
     */
    public void setReconciled(String reconciled);
    
    /**
     * 设置 [已核销]
     */
    public String getReconciled();

    /**
     * 获取 [已核销]脏标记
     */
    public boolean getReconciledDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setRef(String ref);
    
    /**
     * 设置 [参考]
     */
    public String getRef();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getRefDirtyFlag();
    /**
     * 获取 [报告]
     */
    public void setStatement_id(Integer statement_id);
    
    /**
     * 设置 [报告]
     */
    public Integer getStatement_id();

    /**
     * 获取 [报告]脏标记
     */
    public boolean getStatement_idDirtyFlag();
    /**
     * 获取 [报告]
     */
    public void setStatement_id_text(String statement_id_text);
    
    /**
     * 设置 [报告]
     */
    public String getStatement_id_text();

    /**
     * 获取 [报告]脏标记
     */
    public boolean getStatement_id_textDirtyFlag();
    /**
     * 获取 [用该分录核销的银行核销单明细]
     */
    public void setStatement_line_id(Integer statement_line_id);
    
    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    public Integer getStatement_line_id();

    /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    public boolean getStatement_line_idDirtyFlag();
    /**
     * 获取 [用该分录核销的银行核销单明细]
     */
    public void setStatement_line_id_text(String statement_line_id_text);
    
    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    public String getStatement_line_id_text();

    /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    public boolean getStatement_line_id_textDirtyFlag();
    /**
     * 获取 [基本金额]
     */
    public void setTax_base_amount(Double tax_base_amount);
    
    /**
     * 设置 [基本金额]
     */
    public Double getTax_base_amount();

    /**
     * 获取 [基本金额]脏标记
     */
    public boolean getTax_base_amountDirtyFlag();
    /**
     * 获取 [出现在VAT报告]
     */
    public void setTax_exigible(String tax_exigible);
    
    /**
     * 设置 [出现在VAT报告]
     */
    public String getTax_exigible();

    /**
     * 获取 [出现在VAT报告]脏标记
     */
    public boolean getTax_exigibleDirtyFlag();
    /**
     * 获取 [采用的税]
     */
    public void setTax_ids(String tax_ids);
    
    /**
     * 设置 [采用的税]
     */
    public String getTax_ids();

    /**
     * 获取 [采用的税]脏标记
     */
    public boolean getTax_idsDirtyFlag();
    /**
     * 获取 [旧税额]
     */
    public void setTax_line_grouping_key(String tax_line_grouping_key);
    
    /**
     * 设置 [旧税额]
     */
    public String getTax_line_grouping_key();

    /**
     * 获取 [旧税额]脏标记
     */
    public boolean getTax_line_grouping_keyDirtyFlag();
    /**
     * 获取 [发起人税]
     */
    public void setTax_line_id(Integer tax_line_id);
    
    /**
     * 设置 [发起人税]
     */
    public Integer getTax_line_id();

    /**
     * 获取 [发起人税]脏标记
     */
    public boolean getTax_line_idDirtyFlag();
    /**
     * 获取 [发起人税]
     */
    public void setTax_line_id_text(String tax_line_id_text);
    
    /**
     * 设置 [发起人税]
     */
    public String getTax_line_id_text();

    /**
     * 获取 [发起人税]脏标记
     */
    public boolean getTax_line_id_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setUser_type_id(Integer user_type_id);
    
    /**
     * 设置 [类型]
     */
    public Integer getUser_type_id();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getUser_type_idDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setUser_type_id_text(String user_type_id_text);
    
    /**
     * 设置 [类型]
     */
    public String getUser_type_id_text();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getUser_type_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

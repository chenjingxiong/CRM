package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irepair_order_make_invoice;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
public interface Irepair_order_make_invoiceClientService{

    public Irepair_order_make_invoice createModel() ;

    public void update(Irepair_order_make_invoice repair_order_make_invoice);

    public void create(Irepair_order_make_invoice repair_order_make_invoice);

    public void get(Irepair_order_make_invoice repair_order_make_invoice);

    public void updateBatch(List<Irepair_order_make_invoice> repair_order_make_invoices);

    public void createBatch(List<Irepair_order_make_invoice> repair_order_make_invoices);

    public void removeBatch(List<Irepair_order_make_invoice> repair_order_make_invoices);

    public Page<Irepair_order_make_invoice> fetchDefault(SearchContext context);

    public void remove(Irepair_order_make_invoice repair_order_make_invoice);

    public Page<Irepair_order_make_invoice> select(SearchContext context);

    public void getDraft(Irepair_order_make_invoice repair_order_make_invoice);

}

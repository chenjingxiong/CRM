package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing_contact;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_mass_mailing_contact.Mail_mass_mailing_contactIs_email_validDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_mass_mailing_contact
 * 属性：Is_email_valid
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_mass_mailing_contactIs_email_validDefaultValidator.class})
public @interface Mail_mass_mailing_contactIs_email_validDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

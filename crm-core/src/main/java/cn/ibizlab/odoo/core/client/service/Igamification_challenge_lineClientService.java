package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
public interface Igamification_challenge_lineClientService{

    public Igamification_challenge_line createModel() ;

    public void create(Igamification_challenge_line gamification_challenge_line);

    public void update(Igamification_challenge_line gamification_challenge_line);

    public void updateBatch(List<Igamification_challenge_line> gamification_challenge_lines);

    public void removeBatch(List<Igamification_challenge_line> gamification_challenge_lines);

    public Page<Igamification_challenge_line> fetchDefault(SearchContext context);

    public void get(Igamification_challenge_line gamification_challenge_line);

    public void createBatch(List<Igamification_challenge_line> gamification_challenge_lines);

    public void remove(Igamification_challenge_line gamification_challenge_line);

    public Page<Igamification_challenge_line> select(SearchContext context);

    public void getDraft(Igamification_challenge_line gamification_challenge_line);

}

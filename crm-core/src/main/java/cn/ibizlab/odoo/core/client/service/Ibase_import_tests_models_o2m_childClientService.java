package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m_child;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
public interface Ibase_import_tests_models_o2m_childClientService{

    public Ibase_import_tests_models_o2m_child createModel() ;

    public void create(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    public void remove(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    public void createBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children);

    public void removeBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children);

    public void updateBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children);

    public Page<Ibase_import_tests_models_o2m_child> fetchDefault(SearchContext context);

    public void get(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    public void update(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child);

    public Page<Ibase_import_tests_models_o2m_child> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child);

}

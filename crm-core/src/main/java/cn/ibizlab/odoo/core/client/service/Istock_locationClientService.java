package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_location;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_location] 服务对象接口
 */
public interface Istock_locationClientService{

    public Istock_location createModel() ;

    public void update(Istock_location stock_location);

    public void create(Istock_location stock_location);

    public void remove(Istock_location stock_location);

    public void removeBatch(List<Istock_location> stock_locations);

    public void get(Istock_location stock_location);

    public void createBatch(List<Istock_location> stock_locations);

    public Page<Istock_location> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_location> stock_locations);

    public Page<Istock_location> select(SearchContext context);

    public void getDraft(Istock_location stock_location);

}

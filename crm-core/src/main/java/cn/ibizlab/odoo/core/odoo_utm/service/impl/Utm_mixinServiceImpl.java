package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_utm.client.utm_mixinFeignClient;

/**
 * 实体[UTM Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_mixinServiceImpl implements IUtm_mixinService {

    @Autowired
    utm_mixinFeignClient utm_mixinFeignClient;


    @Override
    public boolean update(Utm_mixin et) {
        Utm_mixin rt = utm_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Utm_mixin> list){
        utm_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public Utm_mixin getDraft(Utm_mixin et) {
        et=utm_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public Utm_mixin get(Integer id) {
		Utm_mixin et=utm_mixinFeignClient.get(id);
        if(et==null){
            et=new Utm_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Utm_mixin et) {
        Utm_mixin rt = utm_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_mixin> list){
        utm_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=utm_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        utm_mixinFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context) {
        Page<Utm_mixin> utm_mixins=utm_mixinFeignClient.searchDefault(context);
        return utm_mixins;
    }


}



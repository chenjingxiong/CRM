package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_package_level] 服务对象接口
 */
@Component
public class stock_package_levelFallback implements stock_package_levelFeignClient{


    public Stock_package_level create(Stock_package_level stock_package_level){
            return null;
     }
    public Boolean createBatch(List<Stock_package_level> stock_package_levels){
            return false;
     }

    public Stock_package_level update(Integer id, Stock_package_level stock_package_level){
            return null;
     }
    public Boolean updateBatch(List<Stock_package_level> stock_package_levels){
            return false;
     }




    public Page<Stock_package_level> searchDefault(Stock_package_levelSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_package_level get(Integer id){
            return null;
     }


    public Page<Stock_package_level> select(){
            return null;
     }

    public Stock_package_level getDraft(){
            return null;
    }



}

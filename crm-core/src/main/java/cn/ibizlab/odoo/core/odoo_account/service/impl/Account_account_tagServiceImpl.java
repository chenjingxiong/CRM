package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_account_tagFeignClient;

/**
 * 实体[账户标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_account_tagServiceImpl implements IAccount_account_tagService {

    @Autowired
    account_account_tagFeignClient account_account_tagFeignClient;


    @Override
    public boolean update(Account_account_tag et) {
        Account_account_tag rt = account_account_tagFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_account_tag> list){
        account_account_tagFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_account_tag et) {
        Account_account_tag rt = account_account_tagFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account_tag> list){
        account_account_tagFeignClient.createBatch(list) ;
    }

    @Override
    public Account_account_tag get(Integer id) {
		Account_account_tag et=account_account_tagFeignClient.get(id);
        if(et==null){
            et=new Account_account_tag();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_account_tagFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_account_tagFeignClient.removeBatch(idList);
    }

    @Override
    public Account_account_tag getDraft(Account_account_tag et) {
        et=account_account_tagFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account_tag> searchDefault(Account_account_tagSearchContext context) {
        Page<Account_account_tag> account_account_tags=account_account_tagFeignClient.searchDefault(context);
        return account_account_tags;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
public interface Imail_mass_mailing_listClientService{

    public Imail_mass_mailing_list createModel() ;

    public void update(Imail_mass_mailing_list mail_mass_mailing_list);

    public void create(Imail_mass_mailing_list mail_mass_mailing_list);

    public void createBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists);

    public void remove(Imail_mass_mailing_list mail_mass_mailing_list);

    public Page<Imail_mass_mailing_list> fetchDefault(SearchContext context);

    public void updateBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists);

    public void removeBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists);

    public void get(Imail_mass_mailing_list mail_mass_mailing_list);

    public Page<Imail_mass_mailing_list> select(SearchContext context);

    public void getDraft(Imail_mass_mailing_list mail_mass_mailing_list);

}

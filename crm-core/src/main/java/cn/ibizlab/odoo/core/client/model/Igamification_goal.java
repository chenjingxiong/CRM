package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_goal] 对象
 */
public interface Igamification_goal {

    /**
     * 获取 [挑战]
     */
    public void setChallenge_id(Integer challenge_id);
    
    /**
     * 设置 [挑战]
     */
    public Integer getChallenge_id();

    /**
     * 获取 [挑战]脏标记
     */
    public boolean getChallenge_idDirtyFlag();
    /**
     * 获取 [挑战]
     */
    public void setChallenge_id_text(String challenge_id_text);
    
    /**
     * 设置 [挑战]
     */
    public String getChallenge_id_text();

    /**
     * 获取 [挑战]脏标记
     */
    public boolean getChallenge_id_textDirtyFlag();
    /**
     * 获取 [关闭的目标]
     */
    public void setClosed(String closed);
    
    /**
     * 设置 [关闭的目标]
     */
    public String getClosed();

    /**
     * 获取 [关闭的目标]脏标记
     */
    public boolean getClosedDirtyFlag();
    /**
     * 获取 [完整性]
     */
    public void setCompleteness(Double completeness);
    
    /**
     * 设置 [完整性]
     */
    public Double getCompleteness();

    /**
     * 获取 [完整性]脏标记
     */
    public boolean getCompletenessDirtyFlag();
    /**
     * 获取 [计算模式]
     */
    public void setComputation_mode(String computation_mode);
    
    /**
     * 设置 [计算模式]
     */
    public String getComputation_mode();

    /**
     * 获取 [计算模式]脏标记
     */
    public boolean getComputation_modeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [当前值]
     */
    public void setCurrent(Double current);
    
    /**
     * 设置 [当前值]
     */
    public Double getCurrent();

    /**
     * 获取 [当前值]脏标记
     */
    public boolean getCurrentDirtyFlag();
    /**
     * 获取 [目标绩效]
     */
    public void setDefinition_condition(String definition_condition);
    
    /**
     * 设置 [目标绩效]
     */
    public String getDefinition_condition();

    /**
     * 获取 [目标绩效]脏标记
     */
    public boolean getDefinition_conditionDirtyFlag();
    /**
     * 获取 [定义说明]
     */
    public void setDefinition_description(String definition_description);
    
    /**
     * 设置 [定义说明]
     */
    public String getDefinition_description();

    /**
     * 获取 [定义说明]脏标记
     */
    public boolean getDefinition_descriptionDirtyFlag();
    /**
     * 获取 [显示为]
     */
    public void setDefinition_display(String definition_display);
    
    /**
     * 设置 [显示为]
     */
    public String getDefinition_display();

    /**
     * 获取 [显示为]脏标记
     */
    public boolean getDefinition_displayDirtyFlag();
    /**
     * 获取 [目标定义]
     */
    public void setDefinition_id(Integer definition_id);
    
    /**
     * 设置 [目标定义]
     */
    public Integer getDefinition_id();

    /**
     * 获取 [目标定义]脏标记
     */
    public boolean getDefinition_idDirtyFlag();
    /**
     * 获取 [目标定义]
     */
    public void setDefinition_id_text(String definition_id_text);
    
    /**
     * 设置 [目标定义]
     */
    public String getDefinition_id_text();

    /**
     * 获取 [目标定义]脏标记
     */
    public boolean getDefinition_id_textDirtyFlag();
    /**
     * 获取 [后缀]
     */
    public void setDefinition_suffix(String definition_suffix);
    
    /**
     * 设置 [后缀]
     */
    public String getDefinition_suffix();

    /**
     * 获取 [后缀]脏标记
     */
    public boolean getDefinition_suffixDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setEnd_date(Timestamp end_date);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getEnd_date();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getEnd_dateDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最近更新]
     */
    public void setLast_update(Timestamp last_update);
    
    /**
     * 设置 [最近更新]
     */
    public Timestamp getLast_update();

    /**
     * 获取 [最近更新]脏标记
     */
    public boolean getLast_updateDirtyFlag();
    /**
     * 获取 [挑战行]
     */
    public void setLine_id(Integer line_id);
    
    /**
     * 设置 [挑战行]
     */
    public Integer getLine_id();

    /**
     * 获取 [挑战行]脏标记
     */
    public boolean getLine_idDirtyFlag();
    /**
     * 获取 [挑战行]
     */
    public void setLine_id_text(String line_id_text);
    
    /**
     * 设置 [挑战行]
     */
    public String getLine_id_text();

    /**
     * 获取 [挑战行]脏标记
     */
    public boolean getLine_id_textDirtyFlag();
    /**
     * 获取 [提醒延迟]
     */
    public void setRemind_update_delay(Integer remind_update_delay);
    
    /**
     * 设置 [提醒延迟]
     */
    public Integer getRemind_update_delay();

    /**
     * 获取 [提醒延迟]脏标记
     */
    public boolean getRemind_update_delayDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [达到]
     */
    public void setTarget_goal(Double target_goal);
    
    /**
     * 设置 [达到]
     */
    public Double getTarget_goal();

    /**
     * 获取 [达到]脏标记
     */
    public boolean getTarget_goalDirtyFlag();
    /**
     * 获取 [更新]
     */
    public void setTo_update(String to_update);
    
    /**
     * 设置 [更新]
     */
    public String getTo_update();

    /**
     * 获取 [更新]脏标记
     */
    public boolean getTo_updateDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
@FeignClient(value = "odoo-lunch", contextId = "lunch-order-line-lucky", fallback = lunch_order_line_luckyFallback.class)
public interface lunch_order_line_luckyFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/searchdefault")
    Page<Lunch_order_line_lucky> searchDefault(@RequestBody Lunch_order_line_luckySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/{id}")
    Lunch_order_line_lucky get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies")
    Lunch_order_line_lucky create(@RequestBody Lunch_order_line_lucky lunch_order_line_lucky);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/batch")
    Boolean createBatch(@RequestBody List<Lunch_order_line_lucky> lunch_order_line_luckies);


    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/{id}")
    Lunch_order_line_lucky update(@PathVariable("id") Integer id,@RequestBody Lunch_order_line_lucky lunch_order_line_lucky);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/batch")
    Boolean updateBatch(@RequestBody List<Lunch_order_line_lucky> lunch_order_line_luckies);


    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/select")
    Page<Lunch_order_line_lucky> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/getdraft")
    Lunch_order_line_lucky getDraft();


}

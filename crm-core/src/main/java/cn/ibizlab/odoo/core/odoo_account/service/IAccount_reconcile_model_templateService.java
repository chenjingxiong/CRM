package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;


/**
 * 实体[Account_reconcile_model_template] 服务对象接口
 */
public interface IAccount_reconcile_model_templateService{

    Account_reconcile_model_template get(Integer key) ;
    boolean update(Account_reconcile_model_template et) ;
    void updateBatch(List<Account_reconcile_model_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_reconcile_model_template et) ;
    void createBatch(List<Account_reconcile_model_template> list) ;
    Account_reconcile_model_template getDraft(Account_reconcile_model_template et) ;
    Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context) ;

}




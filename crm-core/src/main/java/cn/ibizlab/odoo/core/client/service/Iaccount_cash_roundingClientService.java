package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_cash_rounding;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_cash_rounding] 服务对象接口
 */
public interface Iaccount_cash_roundingClientService{

    public Iaccount_cash_rounding createModel() ;

    public void get(Iaccount_cash_rounding account_cash_rounding);

    public void remove(Iaccount_cash_rounding account_cash_rounding);

    public void createBatch(List<Iaccount_cash_rounding> account_cash_roundings);

    public void removeBatch(List<Iaccount_cash_rounding> account_cash_roundings);

    public void create(Iaccount_cash_rounding account_cash_rounding);

    public Page<Iaccount_cash_rounding> fetchDefault(SearchContext context);

    public void update(Iaccount_cash_rounding account_cash_rounding);

    public void updateBatch(List<Iaccount_cash_rounding> account_cash_roundings);

    public Page<Iaccount_cash_rounding> select(SearchContext context);

    public void getDraft(Iaccount_cash_rounding account_cash_rounding);

}

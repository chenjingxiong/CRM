package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_convert_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_convert_order] 服务对象接口
 */
public interface Imro_convert_orderClientService{

    public Imro_convert_order createModel() ;

    public void create(Imro_convert_order mro_convert_order);

    public void updateBatch(List<Imro_convert_order> mro_convert_orders);

    public void get(Imro_convert_order mro_convert_order);

    public void remove(Imro_convert_order mro_convert_order);

    public Page<Imro_convert_order> fetchDefault(SearchContext context);

    public void update(Imro_convert_order mro_convert_order);

    public void removeBatch(List<Imro_convert_order> mro_convert_orders);

    public void createBatch(List<Imro_convert_order> mro_convert_orders);

    public Page<Imro_convert_order> select(SearchContext context);

    public void getDraft(Imro_convert_order mro_convert_order);

}

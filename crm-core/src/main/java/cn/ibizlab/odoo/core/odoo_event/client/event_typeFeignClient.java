package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_type] 服务对象接口
 */
@FeignClient(value = "odoo-event", contextId = "event-type", fallback = event_typeFallback.class)
public interface event_typeFeignClient {




    @RequestMapping(method = RequestMethod.GET, value = "/event_types/{id}")
    Event_type get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/event_types/searchdefault")
    Page<Event_type> searchDefault(@RequestBody Event_typeSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/event_types")
    Event_type create(@RequestBody Event_type event_type);

    @RequestMapping(method = RequestMethod.POST, value = "/event_types/batch")
    Boolean createBatch(@RequestBody List<Event_type> event_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/event_types/{id}")
    Event_type update(@PathVariable("id") Integer id,@RequestBody Event_type event_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_types/batch")
    Boolean updateBatch(@RequestBody List<Event_type> event_types);


    @RequestMapping(method = RequestMethod.GET, value = "/event_types/select")
    Page<Event_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_types/getdraft")
    Event_type getDraft();


}

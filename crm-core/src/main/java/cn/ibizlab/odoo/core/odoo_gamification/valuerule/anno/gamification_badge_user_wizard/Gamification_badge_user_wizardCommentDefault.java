package cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_badge_user_wizard;

import cn.ibizlab.odoo.core.odoo_gamification.valuerule.validator.gamification_badge_user_wizard.Gamification_badge_user_wizardCommentDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Gamification_badge_user_wizard
 * 属性：Comment
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Gamification_badge_user_wizardCommentDefaultValidator.class})
public @interface Gamification_badge_user_wizardCommentDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

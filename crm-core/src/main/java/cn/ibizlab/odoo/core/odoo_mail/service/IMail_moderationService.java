package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;


/**
 * 实体[Mail_moderation] 服务对象接口
 */
public interface IMail_moderationService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mail_moderation et) ;
    void updateBatch(List<Mail_moderation> list) ;
    boolean create(Mail_moderation et) ;
    void createBatch(List<Mail_moderation> list) ;
    Mail_moderation getDraft(Mail_moderation et) ;
    Mail_moderation get(Integer key) ;
    Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_picking] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-picking", fallback = stock_pickingFallback.class)
public interface stock_pickingFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_pickings")
    Stock_picking create(@RequestBody Stock_picking stock_picking);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/batch")
    Boolean createBatch(@RequestBody List<Stock_picking> stock_pickings);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/{id}")
    Stock_picking get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/{id}")
    Stock_picking update(@PathVariable("id") Integer id,@RequestBody Stock_picking stock_picking);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/batch")
    Boolean updateBatch(@RequestBody List<Stock_picking> stock_pickings);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/searchdefault")
    Page<Stock_picking> searchDefault(@RequestBody Stock_pickingSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/select")
    Page<Stock_picking> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/getdraft")
    Stock_picking getDraft();


}

package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_expense;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_expense.Hr_expensePayment_modeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_expense
 * 属性：Payment_mode
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_expensePayment_modeDefaultValidator.class})
public @interface Hr_expensePayment_modeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_import_journal_creationFeignClient;

/**
 * 实体[在银行对账单导入创建日记账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_import_journal_creationServiceImpl implements IAccount_bank_statement_import_journal_creationService {

    @Autowired
    account_bank_statement_import_journal_creationFeignClient account_bank_statement_import_journal_creationFeignClient;


    @Override
    public boolean create(Account_bank_statement_import_journal_creation et) {
        Account_bank_statement_import_journal_creation rt = account_bank_statement_import_journal_creationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_import_journal_creation> list){
        account_bank_statement_import_journal_creationFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statement_import_journal_creationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statement_import_journal_creationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_bank_statement_import_journal_creation et) {
        Account_bank_statement_import_journal_creation rt = account_bank_statement_import_journal_creationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement_import_journal_creation> list){
        account_bank_statement_import_journal_creationFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_bank_statement_import_journal_creation getDraft(Account_bank_statement_import_journal_creation et) {
        et=account_bank_statement_import_journal_creationFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_bank_statement_import_journal_creation get(Integer id) {
		Account_bank_statement_import_journal_creation et=account_bank_statement_import_journal_creationFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement_import_journal_creation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_import_journal_creation> searchDefault(Account_bank_statement_import_journal_creationSearchContext context) {
        Page<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations=account_bank_statement_import_journal_creationFeignClient.searchDefault(context);
        return account_bank_statement_import_journal_creations;
    }


}



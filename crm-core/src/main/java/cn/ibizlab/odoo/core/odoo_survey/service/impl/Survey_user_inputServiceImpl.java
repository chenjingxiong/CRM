package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_inputService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_user_inputFeignClient;

/**
 * 实体[调查用户输入] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_user_inputServiceImpl implements ISurvey_user_inputService {

    @Autowired
    survey_user_inputFeignClient survey_user_inputFeignClient;


    @Override
    public boolean update(Survey_user_input et) {
        Survey_user_input rt = survey_user_inputFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_user_input> list){
        survey_user_inputFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Survey_user_input et) {
        Survey_user_input rt = survey_user_inputFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_user_input> list){
        survey_user_inputFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_user_inputFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_user_inputFeignClient.removeBatch(idList);
    }

    @Override
    public Survey_user_input getDraft(Survey_user_input et) {
        et=survey_user_inputFeignClient.getDraft();
        return et;
    }

    @Override
    public Survey_user_input get(Integer id) {
		Survey_user_input et=survey_user_inputFeignClient.get(id);
        if(et==null){
            et=new Survey_user_input();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context) {
        Page<Survey_user_input> survey_user_inputs=survey_user_inputFeignClient.searchDefault(context);
        return survey_user_inputs;
    }


}



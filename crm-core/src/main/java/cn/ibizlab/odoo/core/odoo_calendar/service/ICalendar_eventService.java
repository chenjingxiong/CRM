package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;


/**
 * 实体[Calendar_event] 服务对象接口
 */
public interface ICalendar_eventService{

    boolean update(Calendar_event et) ;
    void updateBatch(List<Calendar_event> list) ;
    Calendar_event get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Calendar_event getDraft(Calendar_event et) ;
    boolean create(Calendar_event et) ;
    void createBatch(List<Calendar_event> list) ;
    boolean save(Calendar_event et) ;
    void saveBatch(List<Calendar_event> list) ;
    boolean checkKey(Calendar_event et) ;
    Page<Calendar_event> searchDefault(Calendar_eventSearchContext context) ;

}




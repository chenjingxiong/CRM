package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_modelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_reconcile_modelFeignClient;

/**
 * 实体[请在发票和付款匹配期间创建日记账分录] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconcile_modelServiceImpl implements IAccount_reconcile_modelService {

    @Autowired
    account_reconcile_modelFeignClient account_reconcile_modelFeignClient;


    @Override
    public Account_reconcile_model get(Integer id) {
		Account_reconcile_model et=account_reconcile_modelFeignClient.get(id);
        if(et==null){
            et=new Account_reconcile_model();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_reconcile_model getDraft(Account_reconcile_model et) {
        et=account_reconcile_modelFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_reconcile_modelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_reconcile_modelFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_reconcile_model et) {
        Account_reconcile_model rt = account_reconcile_modelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconcile_model> list){
        account_reconcile_modelFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_reconcile_model et) {
        Account_reconcile_model rt = account_reconcile_modelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_reconcile_model> list){
        account_reconcile_modelFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context) {
        Page<Account_reconcile_model> account_reconcile_models=account_reconcile_modelFeignClient.searchDefault(context);
        return account_reconcile_models;
    }


}



package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_sendService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_sendFeignClient;

/**
 * 实体[发送会计发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_sendServiceImpl implements IAccount_invoice_sendService {

    @Autowired
    account_invoice_sendFeignClient account_invoice_sendFeignClient;


    @Override
    public Account_invoice_send getDraft(Account_invoice_send et) {
        et=account_invoice_sendFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_invoice_send get(Integer id) {
		Account_invoice_send et=account_invoice_sendFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_send();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_sendFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_sendFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_invoice_send et) {
        Account_invoice_send rt = account_invoice_sendFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_send> list){
        account_invoice_sendFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_invoice_send et) {
        Account_invoice_send rt = account_invoice_sendFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_send> list){
        account_invoice_sendFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context) {
        Page<Account_invoice_send> account_invoice_sends=account_invoice_sendFeignClient.searchDefault(context);
        return account_invoice_sends;
    }


}



package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_tax_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_tax_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_position_tax_templateFeignClient;

/**
 * 实体[税率科目调整模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_position_tax_templateServiceImpl implements IAccount_fiscal_position_tax_templateService {

    @Autowired
    account_fiscal_position_tax_templateFeignClient account_fiscal_position_tax_templateFeignClient;


    @Override
    public Account_fiscal_position_tax_template get(Integer id) {
		Account_fiscal_position_tax_template et=account_fiscal_position_tax_templateFeignClient.get(id);
        if(et==null){
            et=new Account_fiscal_position_tax_template();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_fiscal_position_tax_template et) {
        Account_fiscal_position_tax_template rt = account_fiscal_position_tax_templateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_fiscal_position_tax_template> list){
        account_fiscal_position_tax_templateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_fiscal_position_tax_templateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_fiscal_position_tax_templateFeignClient.removeBatch(idList);
    }

    @Override
    public Account_fiscal_position_tax_template getDraft(Account_fiscal_position_tax_template et) {
        et=account_fiscal_position_tax_templateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_fiscal_position_tax_template et) {
        Account_fiscal_position_tax_template rt = account_fiscal_position_tax_templateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position_tax_template> list){
        account_fiscal_position_tax_templateFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position_tax_template> searchDefault(Account_fiscal_position_tax_templateSearchContext context) {
        Page<Account_fiscal_position_tax_template> account_fiscal_position_tax_templates=account_fiscal_position_tax_templateFeignClient.searchDefault(context);
        return account_fiscal_position_tax_templates;
    }


}



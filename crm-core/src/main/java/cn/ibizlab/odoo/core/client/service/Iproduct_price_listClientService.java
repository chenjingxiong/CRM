package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_price_list;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_price_list] 服务对象接口
 */
public interface Iproduct_price_listClientService{

    public Iproduct_price_list createModel() ;

    public void createBatch(List<Iproduct_price_list> product_price_lists);

    public void create(Iproduct_price_list product_price_list);

    public void updateBatch(List<Iproduct_price_list> product_price_lists);

    public void get(Iproduct_price_list product_price_list);

    public void update(Iproduct_price_list product_price_list);

    public Page<Iproduct_price_list> fetchDefault(SearchContext context);

    public void remove(Iproduct_price_list product_price_list);

    public void removeBatch(List<Iproduct_price_list> product_price_lists);

    public Page<Iproduct_price_list> select(SearchContext context);

    public void getDraft(Iproduct_price_list product_price_list);

}

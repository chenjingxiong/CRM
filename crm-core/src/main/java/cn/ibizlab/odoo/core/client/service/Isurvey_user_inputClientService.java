package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_user_input;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_user_input] 服务对象接口
 */
public interface Isurvey_user_inputClientService{

    public Isurvey_user_input createModel() ;

    public void remove(Isurvey_user_input survey_user_input);

    public Page<Isurvey_user_input> fetchDefault(SearchContext context);

    public void create(Isurvey_user_input survey_user_input);

    public void createBatch(List<Isurvey_user_input> survey_user_inputs);

    public void update(Isurvey_user_input survey_user_input);

    public void updateBatch(List<Isurvey_user_input> survey_user_inputs);

    public void get(Isurvey_user_input survey_user_input);

    public void removeBatch(List<Isurvey_user_input> survey_user_inputs);

    public Page<Isurvey_user_input> select(SearchContext context);

    public void getDraft(Isurvey_user_input survey_user_input);

}

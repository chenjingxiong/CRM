package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;


/**
 * 实体[Base_automation] 服务对象接口
 */
public interface IBase_automationService{

    Base_automation getDraft(Base_automation et) ;
    Base_automation get(Integer key) ;
    boolean create(Base_automation et) ;
    void createBatch(List<Base_automation> list) ;
    boolean update(Base_automation et) ;
    void updateBatch(List<Base_automation> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_automation> searchDefault(Base_automationSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;


/**
 * 实体[Project_task_type] 服务对象接口
 */
public interface IProject_task_typeService{

    Project_task_type get(Integer key) ;
    boolean create(Project_task_type et) ;
    void createBatch(List<Project_task_type> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Project_task_type et) ;
    void updateBatch(List<Project_task_type> list) ;
    Project_task_type getDraft(Project_task_type et) ;
    Page<Project_task_type> searchDefault(Project_task_typeSearchContext context) ;

}




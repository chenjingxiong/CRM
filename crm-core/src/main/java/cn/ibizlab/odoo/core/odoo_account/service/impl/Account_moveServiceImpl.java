package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_moveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_moveFeignClient;

/**
 * 实体[凭证录入] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_moveServiceImpl implements IAccount_moveService {

    @Autowired
    account_moveFeignClient account_moveFeignClient;


    @Override
    public boolean update(Account_move et) {
        Account_move rt = account_moveFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_move> list){
        account_moveFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_moveFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_moveFeignClient.removeBatch(idList);
    }

    @Override
    public Account_move get(Integer id) {
		Account_move et=account_moveFeignClient.get(id);
        if(et==null){
            et=new Account_move();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_move et) {
        Account_move rt = account_moveFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move> list){
        account_moveFeignClient.createBatch(list) ;
    }

    @Override
    public Account_move getDraft(Account_move et) {
        et=account_moveFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move> searchDefault(Account_moveSearchContext context) {
        Page<Account_move> account_moves=account_moveFeignClient.searchDefault(context);
        return account_moves;
    }


}



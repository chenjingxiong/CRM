package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_moderationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_moderationFeignClient;

/**
 * 实体[渠道黑/白名单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_moderationServiceImpl implements IMail_moderationService {

    @Autowired
    mail_moderationFeignClient mail_moderationFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_moderationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_moderationFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Mail_moderation et) {
        Mail_moderation rt = mail_moderationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_moderation> list){
        mail_moderationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_moderation et) {
        Mail_moderation rt = mail_moderationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_moderation> list){
        mail_moderationFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_moderation getDraft(Mail_moderation et) {
        et=mail_moderationFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_moderation get(Integer id) {
		Mail_moderation et=mail_moderationFeignClient.get(id);
        if(et==null){
            et=new Mail_moderation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context) {
        Page<Mail_moderation> mail_moderations=mail_moderationFeignClient.searchDefault(context);
        return mail_moderations;
    }


}



package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_iconService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_payment.client.payment_iconFeignClient;

/**
 * 实体[支付图标] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_iconServiceImpl implements IPayment_iconService {

    @Autowired
    payment_iconFeignClient payment_iconFeignClient;


    @Override
    public boolean update(Payment_icon et) {
        Payment_icon rt = payment_iconFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Payment_icon> list){
        payment_iconFeignClient.updateBatch(list) ;
    }

    @Override
    public Payment_icon getDraft(Payment_icon et) {
        et=payment_iconFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=payment_iconFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        payment_iconFeignClient.removeBatch(idList);
    }

    @Override
    public Payment_icon get(Integer id) {
		Payment_icon et=payment_iconFeignClient.get(id);
        if(et==null){
            et=new Payment_icon();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Payment_icon et) {
        Payment_icon rt = payment_iconFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_icon> list){
        payment_iconFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_icon> searchDefault(Payment_iconSearchContext context) {
        Page<Payment_icon> payment_icons=payment_iconFeignClient.searchDefault(context);
        return payment_icons;
    }


}



package cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_request;

import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.validator.maintenance_request.Maintenance_requestMessage_unreadDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Maintenance_request
 * 属性：Message_unread
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Maintenance_requestMessage_unreadDefaultValidator.class})
public @interface Maintenance_requestMessage_unreadDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_currency] 对象
 */
public interface Ires_currency {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [货币子单位]
     */
    public void setCurrency_subunit_label(String currency_subunit_label);
    
    /**
     * 设置 [货币子单位]
     */
    public String getCurrency_subunit_label();

    /**
     * 获取 [货币子单位]脏标记
     */
    public boolean getCurrency_subunit_labelDirtyFlag();
    /**
     * 获取 [货币单位]
     */
    public void setCurrency_unit_label(String currency_unit_label);
    
    /**
     * 设置 [货币单位]
     */
    public String getCurrency_unit_label();

    /**
     * 获取 [货币单位]脏标记
     */
    public boolean getCurrency_unit_labelDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [小数点位置]
     */
    public void setDecimal_places(Integer decimal_places);
    
    /**
     * 设置 [小数点位置]
     */
    public Integer getDecimal_places();

    /**
     * 获取 [小数点位置]脏标记
     */
    public boolean getDecimal_placesDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setName(String name);
    
    /**
     * 设置 [币种]
     */
    public String getName();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [符号位置]
     */
    public void setPosition(String position);
    
    /**
     * 设置 [符号位置]
     */
    public String getPosition();

    /**
     * 获取 [符号位置]脏标记
     */
    public boolean getPositionDirtyFlag();
    /**
     * 获取 [当前汇率]
     */
    public void setRate(Double rate);
    
    /**
     * 设置 [当前汇率]
     */
    public Double getRate();

    /**
     * 获取 [当前汇率]脏标记
     */
    public boolean getRateDirtyFlag();
    /**
     * 获取 [比率]
     */
    public void setRate_ids(String rate_ids);
    
    /**
     * 设置 [比率]
     */
    public String getRate_ids();

    /**
     * 获取 [比率]脏标记
     */
    public boolean getRate_idsDirtyFlag();
    /**
     * 获取 [舍入系数]
     */
    public void setRounding(Double rounding);
    
    /**
     * 设置 [舍入系数]
     */
    public Double getRounding();

    /**
     * 获取 [舍入系数]脏标记
     */
    public boolean getRoundingDirtyFlag();
    /**
     * 获取 [货币符号]
     */
    public void setSymbol(String symbol);
    
    /**
     * 设置 [货币符号]
     */
    public String getSymbol();

    /**
     * 获取 [货币符号]脏标记
     */
    public boolean getSymbolDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_followers;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_followers] 服务对象接口
 */
public interface Imail_followersClientService{

    public Imail_followers createModel() ;

    public void createBatch(List<Imail_followers> mail_followers);

    public void update(Imail_followers mail_followers);

    public void get(Imail_followers mail_followers);

    public void updateBatch(List<Imail_followers> mail_followers);

    public Page<Imail_followers> fetchDefault(SearchContext context);

    public void removeBatch(List<Imail_followers> mail_followers);

    public void remove(Imail_followers mail_followers);

    public void create(Imail_followers mail_followers);

    public Page<Imail_followers> select(SearchContext context);

    public void getDraft(Imail_followers mail_followers);

}

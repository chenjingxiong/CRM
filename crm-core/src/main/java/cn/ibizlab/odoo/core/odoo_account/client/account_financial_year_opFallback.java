package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_financial_year_op] 服务对象接口
 */
@Component
public class account_financial_year_opFallback implements account_financial_year_opFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }




    public Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context){
            return null;
     }


    public Account_financial_year_op get(Integer id){
            return null;
     }


    public Account_financial_year_op update(Integer id, Account_financial_year_op account_financial_year_op){
            return null;
     }
    public Boolean updateBatch(List<Account_financial_year_op> account_financial_year_ops){
            return false;
     }


    public Account_financial_year_op create(Account_financial_year_op account_financial_year_op){
            return null;
     }
    public Boolean createBatch(List<Account_financial_year_op> account_financial_year_ops){
            return false;
     }

    public Page<Account_financial_year_op> select(){
            return null;
     }

    public Account_financial_year_op getDraft(){
            return null;
    }



}

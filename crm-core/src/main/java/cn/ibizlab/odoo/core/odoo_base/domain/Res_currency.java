package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [币种] 对象
 */
@Data
public class Res_currency extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 小数点位置
     */
    @DEField(name = "decimal_places")
    @JSONField(name = "decimal_places")
    @JsonProperty("decimal_places")
    private Integer decimalPlaces;

    /**
     * 币种
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 货币单位
     */
    @DEField(name = "currency_unit_label")
    @JSONField(name = "currency_unit_label")
    @JsonProperty("currency_unit_label")
    private String currencyUnitLabel;

    /**
     * 舍入系数
     */
    @JSONField(name = "rounding")
    @JsonProperty("rounding")
    private Double rounding;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 符号位置
     */
    @JSONField(name = "position")
    @JsonProperty("position")
    private String position;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 当前汇率
     */
    @JSONField(name = "rate")
    @JsonProperty("rate")
    private Double rate;

    /**
     * 比率
     */
    @JSONField(name = "rate_ids")
    @JsonProperty("rate_ids")
    private String rateIds;

    /**
     * 货币符号
     */
    @JSONField(name = "symbol")
    @JsonProperty("symbol")
    private String symbol;

    /**
     * 货币子单位
     */
    @DEField(name = "currency_subunit_label")
    @JSONField(name = "currency_subunit_label")
    @JsonProperty("currency_subunit_label")
    private String currencySubunitLabel;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [小数点位置]
     */
    public void setDecimalPlaces(Integer decimalPlaces){
        this.decimalPlaces = decimalPlaces ;
        this.modify("decimal_places",decimalPlaces);
    }
    /**
     * 设置 [币种]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [货币单位]
     */
    public void setCurrencyUnitLabel(String currencyUnitLabel){
        this.currencyUnitLabel = currencyUnitLabel ;
        this.modify("currency_unit_label",currencyUnitLabel);
    }
    /**
     * 设置 [舍入系数]
     */
    public void setRounding(Double rounding){
        this.rounding = rounding ;
        this.modify("rounding",rounding);
    }
    /**
     * 设置 [符号位置]
     */
    public void setPosition(String position){
        this.position = position ;
        this.modify("position",position);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [货币符号]
     */
    public void setSymbol(String symbol){
        this.symbol = symbol ;
        this.modify("symbol",symbol);
    }
    /**
     * 设置 [货币子单位]
     */
    public void setCurrencySubunitLabel(String currencySubunitLabel){
        this.currencySubunitLabel = currencySubunitLabel ;
        this.modify("currency_subunit_label",currencySubunitLabel);
    }

}



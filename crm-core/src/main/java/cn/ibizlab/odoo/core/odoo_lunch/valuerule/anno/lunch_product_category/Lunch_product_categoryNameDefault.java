package cn.ibizlab.odoo.core.odoo_lunch.valuerule.anno.lunch_product_category;

import cn.ibizlab.odoo.core.odoo_lunch.valuerule.validator.lunch_product_category.Lunch_product_categoryNameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Lunch_product_category
 * 属性：Name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Lunch_product_categoryNameDefaultValidator.class})
public @interface Lunch_product_categoryNameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

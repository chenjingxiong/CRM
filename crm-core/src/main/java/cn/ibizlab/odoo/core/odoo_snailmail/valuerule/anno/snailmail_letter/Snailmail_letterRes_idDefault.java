package cn.ibizlab.odoo.core.odoo_snailmail.valuerule.anno.snailmail_letter;

import cn.ibizlab.odoo.core.odoo_snailmail.valuerule.validator.snailmail_letter.Snailmail_letterRes_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Snailmail_letter
 * 属性：Res_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Snailmail_letterRes_idDefaultValidator.class})
public @interface Snailmail_letterRes_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

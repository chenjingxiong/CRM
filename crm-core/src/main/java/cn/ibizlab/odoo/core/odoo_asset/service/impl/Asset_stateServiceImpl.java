package cn.ibizlab.odoo.core.odoo_asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_stateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_asset.client.asset_stateFeignClient;

/**
 * 实体[State of Asset] 服务对象接口实现
 */
@Slf4j
@Service
public class Asset_stateServiceImpl implements IAsset_stateService {

    @Autowired
    asset_stateFeignClient asset_stateFeignClient;


    @Override
    public boolean create(Asset_state et) {
        Asset_state rt = asset_stateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Asset_state> list){
        asset_stateFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=asset_stateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        asset_stateFeignClient.removeBatch(idList);
    }

    @Override
    public Asset_state get(Integer id) {
		Asset_state et=asset_stateFeignClient.get(id);
        if(et==null){
            et=new Asset_state();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Asset_state et) {
        Asset_state rt = asset_stateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Asset_state> list){
        asset_stateFeignClient.updateBatch(list) ;
    }

    @Override
    public Asset_state getDraft(Asset_state et) {
        et=asset_stateFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Asset_state> searchDefault(Asset_stateSearchContext context) {
        Page<Asset_state> asset_states=asset_stateFeignClient.searchDefault(context);
        return asset_states;
    }


}



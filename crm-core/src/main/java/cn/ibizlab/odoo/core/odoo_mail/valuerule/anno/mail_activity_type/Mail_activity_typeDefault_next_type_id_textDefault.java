package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_activity_type;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_activity_type.Mail_activity_typeDefault_next_type_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_activity_type
 * 属性：Default_next_type_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_activity_typeDefault_next_type_id_textDefaultValidator.class})
public @interface Mail_activity_typeDefault_next_type_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

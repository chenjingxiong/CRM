package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_lineFeignClient;

/**
 * 实体[分析行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_lineServiceImpl implements IAccount_analytic_lineService {

    @Autowired
    account_analytic_lineFeignClient account_analytic_lineFeignClient;


    @Override
    public boolean create(Account_analytic_line et) {
        Account_analytic_line rt = account_analytic_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_line> list){
        account_analytic_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Account_analytic_line get(Integer id) {
		Account_analytic_line et=account_analytic_lineFeignClient.get(id);
        if(et==null){
            et=new Account_analytic_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_analytic_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_analytic_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Account_analytic_line getDraft(Account_analytic_line et) {
        et=account_analytic_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_analytic_line et) {
        Account_analytic_line rt = account_analytic_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_analytic_line> list){
        account_analytic_lineFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context) {
        Page<Account_analytic_line> account_analytic_lines=account_analytic_lineFeignClient.searchDefault(context);
        return account_analytic_lines;
    }


}



package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipmentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_equipmentFeignClient;

/**
 * 实体[保养设备] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_equipmentServiceImpl implements IMaintenance_equipmentService {

    @Autowired
    maintenance_equipmentFeignClient maintenance_equipmentFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=maintenance_equipmentFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        maintenance_equipmentFeignClient.removeBatch(idList);
    }

    @Override
    public Maintenance_equipment get(Integer id) {
		Maintenance_equipment et=maintenance_equipmentFeignClient.get(id);
        if(et==null){
            et=new Maintenance_equipment();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Maintenance_equipment getDraft(Maintenance_equipment et) {
        et=maintenance_equipmentFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Maintenance_equipment et) {
        Maintenance_equipment rt = maintenance_equipmentFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_equipment> list){
        maintenance_equipmentFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Maintenance_equipment et) {
        Maintenance_equipment rt = maintenance_equipmentFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Maintenance_equipment> list){
        maintenance_equipmentFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context) {
        Page<Maintenance_equipment> maintenance_equipments=maintenance_equipmentFeignClient.searchDefault(context);
        return maintenance_equipments;
    }


}



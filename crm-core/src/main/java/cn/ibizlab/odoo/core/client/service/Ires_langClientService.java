package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_lang;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_lang] 服务对象接口
 */
public interface Ires_langClientService{

    public Ires_lang createModel() ;

    public void get(Ires_lang res_lang);

    public void remove(Ires_lang res_lang);

    public void update(Ires_lang res_lang);

    public void updateBatch(List<Ires_lang> res_langs);

    public Page<Ires_lang> fetchDefault(SearchContext context);

    public void removeBatch(List<Ires_lang> res_langs);

    public void create(Ires_lang res_lang);

    public void createBatch(List<Ires_lang> res_langs);

    public Page<Ires_lang> select(SearchContext context);

    public void getDraft(Ires_lang res_lang);

}

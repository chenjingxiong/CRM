package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qty_repairFeignClient;

/**
 * 实体[警告维修数量不足] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qty_repairServiceImpl implements IStock_warn_insufficient_qty_repairService {

    @Autowired
    stock_warn_insufficient_qty_repairFeignClient stock_warn_insufficient_qty_repairFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warn_insufficient_qty_repairFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warn_insufficient_qty_repairFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_warn_insufficient_qty_repair get(Integer id) {
		Stock_warn_insufficient_qty_repair et=stock_warn_insufficient_qty_repairFeignClient.get(id);
        if(et==null){
            et=new Stock_warn_insufficient_qty_repair();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_warn_insufficient_qty_repair getDraft(Stock_warn_insufficient_qty_repair et) {
        et=stock_warn_insufficient_qty_repairFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty_repair et) {
        Stock_warn_insufficient_qty_repair rt = stock_warn_insufficient_qty_repairFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty_repair> list){
        stock_warn_insufficient_qty_repairFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_warn_insufficient_qty_repair et) {
        Stock_warn_insufficient_qty_repair rt = stock_warn_insufficient_qty_repairFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warn_insufficient_qty_repair> list){
        stock_warn_insufficient_qty_repairFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty_repair> searchDefault(Stock_warn_insufficient_qty_repairSearchContext context) {
        Page<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs=stock_warn_insufficient_qty_repairFeignClient.searchDefault(context);
        return stock_warn_insufficient_qty_repairs;
    }


}



package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_servicesService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_log_servicesFeignClient;

/**
 * 实体[车辆服务] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_log_servicesServiceImpl implements IFleet_vehicle_log_servicesService {

    @Autowired
    fleet_vehicle_log_servicesFeignClient fleet_vehicle_log_servicesFeignClient;


    @Override
    public boolean create(Fleet_vehicle_log_services et) {
        Fleet_vehicle_log_services rt = fleet_vehicle_log_servicesFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_log_services> list){
        fleet_vehicle_log_servicesFeignClient.createBatch(list) ;
    }

    @Override
    public Fleet_vehicle_log_services getDraft(Fleet_vehicle_log_services et) {
        et=fleet_vehicle_log_servicesFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_log_servicesFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_log_servicesFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Fleet_vehicle_log_services et) {
        Fleet_vehicle_log_services rt = fleet_vehicle_log_servicesFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_log_services> list){
        fleet_vehicle_log_servicesFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_vehicle_log_services get(Integer id) {
		Fleet_vehicle_log_services et=fleet_vehicle_log_servicesFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_log_services();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_log_services> searchDefault(Fleet_vehicle_log_servicesSearchContext context) {
        Page<Fleet_vehicle_log_services> fleet_vehicle_log_servicess=fleet_vehicle_log_servicesFeignClient.searchDefault(context);
        return fleet_vehicle_log_servicess;
    }


}



package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailingFeignClient;

/**
 * 实体[群发邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailingServiceImpl implements IMail_mass_mailingService {

    @Autowired
    mail_mass_mailingFeignClient mail_mass_mailingFeignClient;


    @Override
    public boolean update(Mail_mass_mailing et) {
        Mail_mass_mailing rt = mail_mass_mailingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing> list){
        mail_mass_mailingFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_mass_mailing et) {
        Mail_mass_mailing rt = mail_mass_mailingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing> list){
        mail_mass_mailingFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailingFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_mass_mailing get(Integer id) {
		Mail_mass_mailing et=mail_mass_mailingFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_mass_mailing getDraft(Mail_mass_mailing et) {
        et=mail_mass_mailingFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context) {
        Page<Mail_mass_mailing> mail_mass_mailings=mail_mass_mailingFeignClient.searchDefault(context);
        return mail_mass_mailings;
    }


}



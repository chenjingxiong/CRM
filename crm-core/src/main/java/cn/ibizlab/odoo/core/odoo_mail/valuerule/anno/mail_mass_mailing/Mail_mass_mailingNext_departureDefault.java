package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_mass_mailing.Mail_mass_mailingNext_departureDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_mass_mailing
 * 属性：Next_departure
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_mass_mailingNext_departureDefaultValidator.class})
public @interface Mail_mass_mailingNext_departureDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

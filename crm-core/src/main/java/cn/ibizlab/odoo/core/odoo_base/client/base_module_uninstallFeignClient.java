package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_module_uninstall] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-module-uninstall", fallback = base_module_uninstallFallback.class)
public interface base_module_uninstallFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/{id}")
    Base_module_uninstall update(@PathVariable("id") Integer id,@RequestBody Base_module_uninstall base_module_uninstall);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/batch")
    Boolean updateBatch(@RequestBody List<Base_module_uninstall> base_module_uninstalls);




    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/{id}")
    Base_module_uninstall get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls")
    Base_module_uninstall create(@RequestBody Base_module_uninstall base_module_uninstall);

    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/batch")
    Boolean createBatch(@RequestBody List<Base_module_uninstall> base_module_uninstalls);



    @RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/searchdefault")
    Page<Base_module_uninstall> searchDefault(@RequestBody Base_module_uninstallSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/select")
    Page<Base_module_uninstall> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/getdraft")
    Base_module_uninstall getDraft();


}

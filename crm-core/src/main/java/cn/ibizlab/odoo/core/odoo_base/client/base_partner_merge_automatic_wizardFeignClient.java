package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-partner-merge-automatic-wizard", fallback = base_partner_merge_automatic_wizardFallback.class)
public interface base_partner_merge_automatic_wizardFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/searchdefault")
    Page<Base_partner_merge_automatic_wizard> searchDefault(@RequestBody Base_partner_merge_automatic_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/{id}")
    Base_partner_merge_automatic_wizard update(@PathVariable("id") Integer id,@RequestBody Base_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/batch")
    Boolean updateBatch(@RequestBody List<Base_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards);




    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/{id}")
    Base_partner_merge_automatic_wizard get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards")
    Base_partner_merge_automatic_wizard create(@RequestBody Base_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/batch")
    Boolean createBatch(@RequestBody List<Base_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards);



    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/select")
    Page<Base_partner_merge_automatic_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/getdraft")
    Base_partner_merge_automatic_wizard getDraft();


}

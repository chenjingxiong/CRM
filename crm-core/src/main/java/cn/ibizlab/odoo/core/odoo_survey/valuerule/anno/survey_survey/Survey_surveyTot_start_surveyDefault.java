package cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_survey;

import cn.ibizlab.odoo.core.odoo_survey.valuerule.validator.survey_survey.Survey_surveyTot_start_surveyDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Survey_survey
 * 属性：Tot_start_survey
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Survey_surveyTot_start_surveyDefaultValidator.class})
public @interface Survey_surveyTot_start_surveyDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

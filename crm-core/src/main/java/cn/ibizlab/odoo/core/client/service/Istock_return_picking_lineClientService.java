package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_return_picking_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
public interface Istock_return_picking_lineClientService{

    public Istock_return_picking_line createModel() ;

    public void removeBatch(List<Istock_return_picking_line> stock_return_picking_lines);

    public void remove(Istock_return_picking_line stock_return_picking_line);

    public void createBatch(List<Istock_return_picking_line> stock_return_picking_lines);

    public void get(Istock_return_picking_line stock_return_picking_line);

    public void update(Istock_return_picking_line stock_return_picking_line);

    public void updateBatch(List<Istock_return_picking_line> stock_return_picking_lines);

    public Page<Istock_return_picking_line> fetchDefault(SearchContext context);

    public void create(Istock_return_picking_line stock_return_picking_line);

    public Page<Istock_return_picking_line> select(SearchContext context);

    public void getDraft(Istock_return_picking_line stock_return_picking_line);

}

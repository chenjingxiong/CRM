package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_unbuild;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
public interface Imrp_unbuildClientService{

    public Imrp_unbuild createModel() ;

    public void createBatch(List<Imrp_unbuild> mrp_unbuilds);

    public void updateBatch(List<Imrp_unbuild> mrp_unbuilds);

    public void create(Imrp_unbuild mrp_unbuild);

    public void removeBatch(List<Imrp_unbuild> mrp_unbuilds);

    public void update(Imrp_unbuild mrp_unbuild);

    public Page<Imrp_unbuild> fetchDefault(SearchContext context);

    public void remove(Imrp_unbuild mrp_unbuild);

    public void get(Imrp_unbuild mrp_unbuild);

    public Page<Imrp_unbuild> select(SearchContext context);

    public void getDraft(Imrp_unbuild mrp_unbuild);

}

package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [日记账] 对象
 */
@Data
public class Account_journal extends EntityClient implements Serializable {

    /**
     * 信用票：下一号码
     */
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;

    /**
     * 允许的科目类型
     */
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    private String typeControlIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 至少一个转出
     */
    @DEField(name = "at_least_one_outbound")
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private String atLeastOneOutbound;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 分录序列
     */
    @DEField(name = "sequence_id")
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;

    /**
     * 在仪表板显示日记账
     */
    @DEField(name = "show_on_dashboard")
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private String showOnDashboard;

    /**
     * 在销售点中使用
     */
    @DEField(name = "journal_user")
    @JSONField(name = "journal_user")
    @JsonProperty("journal_user")
    private String journalUser;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 银行费用
     */
    @DEField(name = "bank_statements_source")
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    private String bankStatementsSource;

    /**
     * 至少一个转入
     */
    @DEField(name = "at_least_one_inbound")
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private String atLeastOneInbound;

    /**
     * 银行核销时过账
     */
    @DEField(name = "post_at_bank_rec")
    @JSONField(name = "post_at_bank_rec")
    @JsonProperty("post_at_bank_rec")
    private String postAtBankRec;

    /**
     * 日记账名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 下一号码
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;

    /**
     * 属于用户的当前公司
     */
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private String belongsToCompany;

    /**
     * 简码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 为收款
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 分组发票明细行
     */
    @DEField(name = "group_invoice_lines")
    @JSONField(name = "group_invoice_lines")
    @JsonProperty("group_invoice_lines")
    private String groupInvoiceLines;

    /**
     * 信用票分录序列
     */
    @DEField(name = "refund_sequence_id")
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;

    /**
     * 别名域
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 允许的科目
     */
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    private String accountControlIds;

    /**
     * 允许取消分录
     */
    @DEField(name = "update_posted")
    @JSONField(name = "update_posted")
    @JsonProperty("update_posted")
    private String updatePosted;

    /**
     * 授权差异的总金额
     */
    @DEField(name = "amount_authorized_diff")
    @JSONField(name = "amount_authorized_diff")
    @JsonProperty("amount_authorized_diff")
    private Double amountAuthorizedDiff;

    /**
     * 看板仪表板图表
     */
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    private String kanbanDashboardGraph;

    /**
     * 为付款
     */
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    private String outboundPaymentMethodIds;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 看板仪表板
     */
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    private String kanbanDashboard;

    /**
     * 专用的信用票序列
     */
    @DEField(name = "refund_sequence")
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private String refundSequence;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 利润科目
     */
    @JSONField(name = "profit_account_id_text")
    @JsonProperty("profit_account_id_text")
    private String profitAccountIdText;

    /**
     * 供应商账单的别名
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 账户号码
     */
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    private String bankAccNumber;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 银行
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Integer bankId;

    /**
     * 损失科目
     */
    @JSONField(name = "loss_account_id_text")
    @JsonProperty("loss_account_id_text")
    private String lossAccountIdText;

    /**
     * 默认贷方科目
     */
    @JSONField(name = "default_credit_account_id_text")
    @JsonProperty("default_credit_account_id_text")
    private String defaultCreditAccountIdText;

    /**
     * 账户持有人
     */
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    private Integer companyPartnerId;

    /**
     * 默认借方科目
     */
    @JSONField(name = "default_debit_account_id_text")
    @JsonProperty("default_debit_account_id_text")
    private String defaultDebitAccountIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 银行账户
     */
    @DEField(name = "bank_account_id")
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 损失科目
     */
    @DEField(name = "loss_account_id")
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    private Integer lossAccountId;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 默认贷方科目
     */
    @DEField(name = "default_credit_account_id")
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    private Integer defaultCreditAccountId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 默认借方科目
     */
    @DEField(name = "default_debit_account_id")
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    private Integer defaultDebitAccountId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 利润科目
     */
    @DEField(name = "profit_account_id")
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    private Integer profitAccountId;


    /**
     * 
     */
    @JSONField(name = "odoodefaultcreditaccount")
    @JsonProperty("odoodefaultcreditaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooDefaultCreditAccount;

    /**
     * 
     */
    @JSONField(name = "odoodefaultdebitaccount")
    @JsonProperty("odoodefaultdebitaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooDefaultDebitAccount;

    /**
     * 
     */
    @JSONField(name = "odoolossaccount")
    @JsonProperty("odoolossaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooLossAccount;

    /**
     * 
     */
    @JSONField(name = "odooprofitaccount")
    @JsonProperty("odooprofitaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooProfitAccount;

    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoobankaccount")
    @JsonProperty("odoobankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooBankAccount;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [至少一个转出]
     */
    public void setAtLeastOneOutbound(String atLeastOneOutbound){
        this.atLeastOneOutbound = atLeastOneOutbound ;
        this.modify("at_least_one_outbound",atLeastOneOutbound);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [分录序列]
     */
    public void setSequenceId(Integer sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }
    /**
     * 设置 [在仪表板显示日记账]
     */
    public void setShowOnDashboard(String showOnDashboard){
        this.showOnDashboard = showOnDashboard ;
        this.modify("show_on_dashboard",showOnDashboard);
    }
    /**
     * 设置 [在销售点中使用]
     */
    public void setJournalUser(String journalUser){
        this.journalUser = journalUser ;
        this.modify("journal_user",journalUser);
    }
    /**
     * 设置 [银行费用]
     */
    public void setBankStatementsSource(String bankStatementsSource){
        this.bankStatementsSource = bankStatementsSource ;
        this.modify("bank_statements_source",bankStatementsSource);
    }
    /**
     * 设置 [至少一个转入]
     */
    public void setAtLeastOneInbound(String atLeastOneInbound){
        this.atLeastOneInbound = atLeastOneInbound ;
        this.modify("at_least_one_inbound",atLeastOneInbound);
    }
    /**
     * 设置 [银行核销时过账]
     */
    public void setPostAtBankRec(String postAtBankRec){
        this.postAtBankRec = postAtBankRec ;
        this.modify("post_at_bank_rec",postAtBankRec);
    }
    /**
     * 设置 [日记账名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [简码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [分组发票明细行]
     */
    public void setGroupInvoiceLines(String groupInvoiceLines){
        this.groupInvoiceLines = groupInvoiceLines ;
        this.modify("group_invoice_lines",groupInvoiceLines);
    }
    /**
     * 设置 [信用票分录序列]
     */
    public void setRefundSequenceId(Integer refundSequenceId){
        this.refundSequenceId = refundSequenceId ;
        this.modify("refund_sequence_id",refundSequenceId);
    }
    /**
     * 设置 [允许取消分录]
     */
    public void setUpdatePosted(String updatePosted){
        this.updatePosted = updatePosted ;
        this.modify("update_posted",updatePosted);
    }
    /**
     * 设置 [授权差异的总金额]
     */
    public void setAmountAuthorizedDiff(Double amountAuthorizedDiff){
        this.amountAuthorizedDiff = amountAuthorizedDiff ;
        this.modify("amount_authorized_diff",amountAuthorizedDiff);
    }
    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [专用的信用票序列]
     */
    public void setRefundSequence(String refundSequence){
        this.refundSequence = refundSequence ;
        this.modify("refund_sequence",refundSequence);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [银行账户]
     */
    public void setBankAccountId(Integer bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }
    /**
     * 设置 [损失科目]
     */
    public void setLossAccountId(Integer lossAccountId){
        this.lossAccountId = lossAccountId ;
        this.modify("loss_account_id",lossAccountId);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [默认贷方科目]
     */
    public void setDefaultCreditAccountId(Integer defaultCreditAccountId){
        this.defaultCreditAccountId = defaultCreditAccountId ;
        this.modify("default_credit_account_id",defaultCreditAccountId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [默认借方科目]
     */
    public void setDefaultDebitAccountId(Integer defaultDebitAccountId){
        this.defaultDebitAccountId = defaultDebitAccountId ;
        this.modify("default_debit_account_id",defaultDebitAccountId);
    }
    /**
     * 设置 [利润科目]
     */
    public void setProfitAccountId(Integer profitAccountId){
        this.profitAccountId = profitAccountId ;
        this.modify("profit_account_id",profitAccountId);
    }

}



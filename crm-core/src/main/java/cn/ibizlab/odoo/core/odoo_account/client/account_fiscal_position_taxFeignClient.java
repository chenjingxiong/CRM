package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-fiscal-position-tax", fallback = account_fiscal_position_taxFallback.class)
public interface account_fiscal_position_taxFeignClient {



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/{id}")
    Account_fiscal_position_tax get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/{id}")
    Account_fiscal_position_tax update(@PathVariable("id") Integer id,@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_position_tax> account_fiscal_position_taxes);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes")
    Account_fiscal_position_tax create(@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_position_tax> account_fiscal_position_taxes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/searchdefault")
    Page<Account_fiscal_position_tax> searchDefault(@RequestBody Account_fiscal_position_taxSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/select")
    Page<Account_fiscal_position_tax> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/getdraft")
    Account_fiscal_position_tax getDraft();


}

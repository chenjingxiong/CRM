package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_product_configurator] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-product-configurator", fallback = sale_product_configuratorFallback.class)
public interface sale_product_configuratorFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/sale_product_configurators")
    Sale_product_configurator create(@RequestBody Sale_product_configurator sale_product_configurator);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_product_configurators/batch")
    Boolean createBatch(@RequestBody List<Sale_product_configurator> sale_product_configurators);




    @RequestMapping(method = RequestMethod.GET, value = "/sale_product_configurators/{id}")
    Sale_product_configurator get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/sale_product_configurators/{id}")
    Sale_product_configurator update(@PathVariable("id") Integer id,@RequestBody Sale_product_configurator sale_product_configurator);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_product_configurators/batch")
    Boolean updateBatch(@RequestBody List<Sale_product_configurator> sale_product_configurators);




    @RequestMapping(method = RequestMethod.POST, value = "/sale_product_configurators/searchdefault")
    Page<Sale_product_configurator> searchDefault(@RequestBody Sale_product_configuratorSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_product_configurators/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_product_configurators/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_product_configurators/select")
    Page<Sale_product_configurator> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_product_configurators/getdraft")
    Sale_product_configurator getDraft();


}

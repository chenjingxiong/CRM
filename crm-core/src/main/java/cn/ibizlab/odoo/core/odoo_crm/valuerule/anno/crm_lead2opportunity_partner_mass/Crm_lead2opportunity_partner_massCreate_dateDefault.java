package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_lead2opportunity_partner_mass;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_lead2opportunity_partner_mass.Crm_lead2opportunity_partner_massCreate_dateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_lead2opportunity_partner_mass
 * 属性：Create_date
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_lead2opportunity_partner_massCreate_dateDefaultValidator.class})
public @interface Crm_lead2opportunity_partner_massCreate_dateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

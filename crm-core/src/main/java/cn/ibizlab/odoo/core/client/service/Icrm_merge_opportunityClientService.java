package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_merge_opportunity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
public interface Icrm_merge_opportunityClientService{

    public Icrm_merge_opportunity createModel() ;

    public void createBatch(List<Icrm_merge_opportunity> crm_merge_opportunities);

    public void remove(Icrm_merge_opportunity crm_merge_opportunity);

    public void updateBatch(List<Icrm_merge_opportunity> crm_merge_opportunities);

    public Page<Icrm_merge_opportunity> fetchDefault(SearchContext context);

    public void update(Icrm_merge_opportunity crm_merge_opportunity);

    public void removeBatch(List<Icrm_merge_opportunity> crm_merge_opportunities);

    public void create(Icrm_merge_opportunity crm_merge_opportunity);

    public void get(Icrm_merge_opportunity crm_merge_opportunity);

    public Page<Icrm_merge_opportunity> select(SearchContext context);

    public void getDraft(Icrm_merge_opportunity crm_merge_opportunity);

}

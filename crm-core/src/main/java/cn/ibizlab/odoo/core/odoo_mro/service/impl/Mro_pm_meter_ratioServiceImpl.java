package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_ratioService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meter_ratioFeignClient;

/**
 * 实体[Rules for Meter to Meter Ratio] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meter_ratioServiceImpl implements IMro_pm_meter_ratioService {

    @Autowired
    mro_pm_meter_ratioFeignClient mro_pm_meter_ratioFeignClient;


    @Override
    public Mro_pm_meter_ratio get(Integer id) {
		Mro_pm_meter_ratio et=mro_pm_meter_ratioFeignClient.get(id);
        if(et==null){
            et=new Mro_pm_meter_ratio();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mro_pm_meter_ratio et) {
        Mro_pm_meter_ratio rt = mro_pm_meter_ratioFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_pm_meter_ratio> list){
        mro_pm_meter_ratioFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_pm_meter_ratioFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_pm_meter_ratioFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mro_pm_meter_ratio et) {
        Mro_pm_meter_ratio rt = mro_pm_meter_ratioFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter_ratio> list){
        mro_pm_meter_ratioFeignClient.createBatch(list) ;
    }

    @Override
    public Mro_pm_meter_ratio getDraft(Mro_pm_meter_ratio et) {
        et=mro_pm_meter_ratioFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter_ratio> searchDefault(Mro_pm_meter_ratioSearchContext context) {
        Page<Mro_pm_meter_ratio> mro_pm_meter_ratios=mro_pm_meter_ratioFeignClient.searchDefault(context);
        return mro_pm_meter_ratios;
    }


}



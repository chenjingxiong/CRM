package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_tax_template] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-tax-template", fallback = account_tax_templateFallback.class)
public interface account_tax_templateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_tax_templates/{id}")
    Account_tax_template update(@PathVariable("id") Integer id,@RequestBody Account_tax_template account_tax_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_tax_templates/batch")
    Boolean updateBatch(@RequestBody List<Account_tax_template> account_tax_templates);


    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_templates")
    Account_tax_template create(@RequestBody Account_tax_template account_tax_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_templates/batch")
    Boolean createBatch(@RequestBody List<Account_tax_template> account_tax_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_templates/{id}")
    Account_tax_template get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_templates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/account_tax_templates/searchdefault")
    Page<Account_tax_template> searchDefault(@RequestBody Account_tax_templateSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_templates/select")
    Page<Account_tax_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_tax_templates/getdraft")
    Account_tax_template getDraft();


}

package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;


/**
 * 实体[Res_currency] 服务对象接口
 */
public interface IRes_currencyService{

    Res_currency get(Integer key) ;
    boolean update(Res_currency et) ;
    void updateBatch(List<Res_currency> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_currency et) ;
    void createBatch(List<Res_currency> list) ;
    Res_currency getDraft(Res_currency et) ;
    Page<Res_currency> searchDefault(Res_currencySearchContext context) ;

}




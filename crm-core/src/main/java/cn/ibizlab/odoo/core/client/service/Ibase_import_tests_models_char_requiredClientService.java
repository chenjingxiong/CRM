package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_required;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_char_required] 服务对象接口
 */
public interface Ibase_import_tests_models_char_requiredClientService{

    public Ibase_import_tests_models_char_required createModel() ;

    public void remove(Ibase_import_tests_models_char_required base_import_tests_models_char_required);

    public Page<Ibase_import_tests_models_char_required> fetchDefault(SearchContext context);

    public void updateBatch(List<Ibase_import_tests_models_char_required> base_import_tests_models_char_requireds);

    public void create(Ibase_import_tests_models_char_required base_import_tests_models_char_required);

    public void createBatch(List<Ibase_import_tests_models_char_required> base_import_tests_models_char_requireds);

    public void update(Ibase_import_tests_models_char_required base_import_tests_models_char_required);

    public void get(Ibase_import_tests_models_char_required base_import_tests_models_char_required);

    public void removeBatch(List<Ibase_import_tests_models_char_required> base_import_tests_models_char_requireds);

    public Page<Ibase_import_tests_models_char_required> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_char_required base_import_tests_models_char_required);

}

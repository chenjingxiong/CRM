package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-unbuild", fallback = mrp_unbuildFallback.class)
public interface mrp_unbuildFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds")
    Mrp_unbuild create(@RequestBody Mrp_unbuild mrp_unbuild);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/batch")
    Boolean createBatch(@RequestBody List<Mrp_unbuild> mrp_unbuilds);



    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/{id}")
    Mrp_unbuild update(@PathVariable("id") Integer id,@RequestBody Mrp_unbuild mrp_unbuild);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/batch")
    Boolean updateBatch(@RequestBody List<Mrp_unbuild> mrp_unbuilds);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/searchdefault")
    Page<Mrp_unbuild> searchDefault(@RequestBody Mrp_unbuildSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/{id}")
    Mrp_unbuild get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/select")
    Page<Mrp_unbuild> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/getdraft")
    Mrp_unbuild getDraft();


}

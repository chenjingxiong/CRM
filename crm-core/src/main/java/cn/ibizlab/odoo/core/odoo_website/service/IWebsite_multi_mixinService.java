package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_multi_mixinSearchContext;


/**
 * 实体[Website_multi_mixin] 服务对象接口
 */
public interface IWebsite_multi_mixinService{

    boolean update(Website_multi_mixin et) ;
    void updateBatch(List<Website_multi_mixin> list) ;
    Website_multi_mixin get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Website_multi_mixin getDraft(Website_multi_mixin et) ;
    boolean create(Website_multi_mixin et) ;
    void createBatch(List<Website_multi_mixin> list) ;
    Page<Website_multi_mixin> searchDefault(Website_multi_mixinSearchContext context) ;

}




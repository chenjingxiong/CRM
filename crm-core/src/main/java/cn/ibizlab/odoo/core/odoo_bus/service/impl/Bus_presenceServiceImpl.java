package cn.ibizlab.odoo.core.odoo_bus.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_presenceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_bus.client.bus_presenceFeignClient;

/**
 * 实体[用户上线] 服务对象接口实现
 */
@Slf4j
@Service
public class Bus_presenceServiceImpl implements IBus_presenceService {

    @Autowired
    bus_presenceFeignClient bus_presenceFeignClient;


    @Override
    public Bus_presence get(Integer id) {
		Bus_presence et=bus_presenceFeignClient.get(id);
        if(et==null){
            et=new Bus_presence();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Bus_presence getDraft(Bus_presence et) {
        et=bus_presenceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=bus_presenceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        bus_presenceFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Bus_presence et) {
        Bus_presence rt = bus_presenceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Bus_presence> list){
        bus_presenceFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Bus_presence et) {
        Bus_presence rt = bus_presenceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Bus_presence> list){
        bus_presenceFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Bus_presence> searchDefault(Bus_presenceSearchContext context) {
        Page<Bus_presence> bus_presences=bus_presenceFeignClient.searchDefault(context);
        return bus_presences;
    }


}



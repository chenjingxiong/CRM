package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作岗位] 对象
 */
@Data
public class Hr_job extends EntityClient implements Serializable {

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 当前员工数量
     */
    @DEField(name = "no_of_employee")
    @JSONField(name = "no_of_employee")
    @JsonProperty("no_of_employee")
    private Integer noOfEmployee;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 已雇用员工
     */
    @DEField(name = "no_of_hired_employee")
    @JSONField(name = "no_of_hired_employee")
    @JsonProperty("no_of_hired_employee")
    private Integer noOfHiredEmployee;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 网站元说明
     */
    @DEField(name = "website_meta_description")
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 期望的新员工
     */
    @DEField(name = "no_of_recruitment")
    @JSONField(name = "no_of_recruitment")
    @JsonProperty("no_of_recruitment")
    private Integer noOfRecruitment;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 工作岗位
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 工作说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 要求
     */
    @JSONField(name = "requirements")
    @JsonProperty("requirements")
    private String requirements;

    /**
     * 网站meta标题
     */
    @DEField(name = "website_meta_title")
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 应用数量
     */
    @JSONField(name = "application_count")
    @JsonProperty("application_count")
    private Integer applicationCount;

    /**
     * 求职申请
     */
    @JSONField(name = "application_ids")
    @JsonProperty("application_ids")
    private String applicationIds;

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 网站opengraph图像
     */
    @DEField(name = "website_meta_og_img")
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 预计员工数合计
     */
    @DEField(name = "expected_employees")
    @JSONField(name = "expected_employees")
    @JsonProperty("expected_employees")
    private Integer expectedEmployees;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 信息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 网站说明
     */
    @DEField(name = "website_description")
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 文档
     */
    @JSONField(name = "document_ids")
    @JsonProperty("document_ids")
    private String documentIds;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 员工
     */
    @JSONField(name = "employee_ids")
    @JsonProperty("employee_ids")
    private String employeeIds;

    /**
     * 文档数
     */
    @JSONField(name = "documents_count")
    @JsonProperty("documents_count")
    private Integer documentsCount;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 网站meta关键词
     */
    @DEField(name = "website_meta_keywords")
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 默认值
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 别名
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 记录线索ID
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 网域别名
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 上级模型
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 上级记录ID
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 模型别名
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 工作地点
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;

    /**
     * 所有者
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 部门
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 招聘负责人
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 部门经理
     */
    @JSONField(name = "manager_id_text")
    @JsonProperty("manager_id_text")
    private String managerIdText;

    /**
     * 别名联系人安全
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 人力资源主管
     */
    @JSONField(name = "hr_responsible_id_text")
    @JsonProperty("hr_responsible_id_text")
    private String hrResponsibleIdText;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 部门经理
     */
    @DEField(name = "manager_id")
    @JSONField(name = "manager_id")
    @JsonProperty("manager_id")
    private Integer managerId;

    /**
     * 工作地点
     */
    @DEField(name = "address_id")
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Integer addressId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 人力资源主管
     */
    @DEField(name = "hr_responsible_id")
    @JSONField(name = "hr_responsible_id")
    @JsonProperty("hr_responsible_id")
    private Integer hrResponsibleId;

    /**
     * 部门
     */
    @DEField(name = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 招聘负责人
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;


    /**
     * 
     */
    @JSONField(name = "odoodepartment")
    @JsonProperty("odoodepartment")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JSONField(name = "odoomanager")
    @JsonProperty("odoomanager")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooManager;

    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odooaddress")
    @JsonProperty("odooaddress")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAddress;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoohrresponsible")
    @JsonProperty("odoohrresponsible")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooHrResponsible;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [当前员工数量]
     */
    public void setNoOfEmployee(Integer noOfEmployee){
        this.noOfEmployee = noOfEmployee ;
        this.modify("no_of_employee",noOfEmployee);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [已雇用员工]
     */
    public void setNoOfHiredEmployee(Integer noOfHiredEmployee){
        this.noOfHiredEmployee = noOfHiredEmployee ;
        this.modify("no_of_hired_employee",noOfHiredEmployee);
    }
    /**
     * 设置 [网站元说明]
     */
    public void setWebsiteMetaDescription(String websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }
    /**
     * 设置 [期望的新员工]
     */
    public void setNoOfRecruitment(Integer noOfRecruitment){
        this.noOfRecruitment = noOfRecruitment ;
        this.modify("no_of_recruitment",noOfRecruitment);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [工作岗位]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [工作说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [要求]
     */
    public void setRequirements(String requirements){
        this.requirements = requirements ;
        this.modify("requirements",requirements);
    }
    /**
     * 设置 [网站meta标题]
     */
    public void setWebsiteMetaTitle(String websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }
    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [网站opengraph图像]
     */
    public void setWebsiteMetaOgImg(String websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }
    /**
     * 设置 [预计员工数合计]
     */
    public void setExpectedEmployees(Integer expectedEmployees){
        this.expectedEmployees = expectedEmployees ;
        this.modify("expected_employees",expectedEmployees);
    }
    /**
     * 设置 [网站说明]
     */
    public void setWebsiteDescription(String websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [网站meta关键词]
     */
    public void setWebsiteMetaKeywords(String websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [部门经理]
     */
    public void setManagerId(Integer managerId){
        this.managerId = managerId ;
        this.modify("manager_id",managerId);
    }
    /**
     * 设置 [工作地点]
     */
    public void setAddressId(Integer addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }
    /**
     * 设置 [人力资源主管]
     */
    public void setHrResponsibleId(Integer hrResponsibleId){
        this.hrResponsibleId = hrResponsibleId ;
        this.modify("hr_responsible_id",hrResponsibleId);
    }
    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Integer departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }
    /**
     * 设置 [招聘负责人]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

}



package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_rules_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_rules_reportFeignClient;

/**
 * 实体[库存规则报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_rules_reportServiceImpl implements IStock_rules_reportService {

    @Autowired
    stock_rules_reportFeignClient stock_rules_reportFeignClient;


    @Override
    public boolean create(Stock_rules_report et) {
        Stock_rules_report rt = stock_rules_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_rules_report> list){
        stock_rules_reportFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_rules_report get(Integer id) {
		Stock_rules_report et=stock_rules_reportFeignClient.get(id);
        if(et==null){
            et=new Stock_rules_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_rules_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_rules_reportFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_rules_report et) {
        Stock_rules_report rt = stock_rules_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_rules_report> list){
        stock_rules_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_rules_report getDraft(Stock_rules_report et) {
        et=stock_rules_reportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context) {
        Page<Stock_rules_report> stock_rules_reports=stock_rules_reportFeignClient.searchDefault(context);
        return stock_rules_reports;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconcile_model] 服务对象接口
 */
public interface Iaccount_reconcile_modelClientService{

    public Iaccount_reconcile_model createModel() ;

    public void remove(Iaccount_reconcile_model account_reconcile_model);

    public void create(Iaccount_reconcile_model account_reconcile_model);

    public void update(Iaccount_reconcile_model account_reconcile_model);

    public void updateBatch(List<Iaccount_reconcile_model> account_reconcile_models);

    public void createBatch(List<Iaccount_reconcile_model> account_reconcile_models);

    public void get(Iaccount_reconcile_model account_reconcile_model);

    public void removeBatch(List<Iaccount_reconcile_model> account_reconcile_models);

    public Page<Iaccount_reconcile_model> fetchDefault(SearchContext context);

    public Page<Iaccount_reconcile_model> select(SearchContext context);

    public void getDraft(Iaccount_reconcile_model account_reconcile_model);

}

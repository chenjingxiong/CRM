package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iportal_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_wizard] 服务对象接口
 */
public interface Iportal_wizardClientService{

    public Iportal_wizard createModel() ;

    public void updateBatch(List<Iportal_wizard> portal_wizards);

    public void createBatch(List<Iportal_wizard> portal_wizards);

    public void removeBatch(List<Iportal_wizard> portal_wizards);

    public Page<Iportal_wizard> fetchDefault(SearchContext context);

    public void create(Iportal_wizard portal_wizard);

    public void remove(Iportal_wizard portal_wizard);

    public void get(Iportal_wizard portal_wizard);

    public void update(Iportal_wizard portal_wizard);

    public Page<Iportal_wizard> select(SearchContext context);

    public void getDraft(Iportal_wizard portal_wizard);

}

package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_product_category] 服务对象接口
 */
@Component
public class lunch_product_categoryFallback implements lunch_product_categoryFeignClient{

    public Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context){
            return null;
     }


    public Lunch_product_category create(Lunch_product_category lunch_product_category){
            return null;
     }
    public Boolean createBatch(List<Lunch_product_category> lunch_product_categories){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Lunch_product_category update(Integer id, Lunch_product_category lunch_product_category){
            return null;
     }
    public Boolean updateBatch(List<Lunch_product_category> lunch_product_categories){
            return false;
     }


    public Lunch_product_category get(Integer id){
            return null;
     }




    public Page<Lunch_product_category> select(){
            return null;
     }

    public Lunch_product_category getDraft(){
            return null;
    }



}

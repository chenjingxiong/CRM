package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存包装层级] 对象
 */
@Data
public class Stock_package_level extends EntityClient implements Serializable {

    /**
     * 凭证明细
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 移动
     */
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    private String moveIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 从
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 显示批次文本
     */
    @JSONField(name = "show_lots_text")
    @JsonProperty("show_lots_text")
    private String showLotsText;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 完成
     */
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private String isDone;

    /**
     * 显示批次M2O
     */
    @JSONField(name = "show_lots_m2o")
    @JsonProperty("show_lots_m2o")
    private String showLotsM2o;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 是新鲜包裹
     */
    @JSONField(name = "is_fresh_package")
    @JsonProperty("is_fresh_package")
    private String isFreshPackage;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 分拣
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 包裹
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;

    /**
     * 至
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 源位置
     */
    @JSONField(name = "picking_source_location")
    @JsonProperty("picking_source_location")
    private Integer pickingSourceLocation;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 分拣
     */
    @DEField(name = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 包裹
     */
    @DEField(name = "package_id")
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Integer packageId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 至
     */
    @DEField(name = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocationdest")
    @JsonProperty("odoolocationdest")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JSONField(name = "odoopicking")
    @JsonProperty("odoopicking")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JSONField(name = "odoopackage")
    @JsonProperty("odoopackage")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package odooPackage;




    /**
     * 设置 [分拣]
     */
    public void setPickingId(Integer pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }
    /**
     * 设置 [包裹]
     */
    public void setPackageId(Integer packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }
    /**
     * 设置 [至]
     */
    public void setLocationDestId(Integer locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

}



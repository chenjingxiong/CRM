package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;


/**
 * 实体[Mail_activity_type] 服务对象接口
 */
public interface IMail_activity_typeService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_activity_type get(Integer key) ;
    Mail_activity_type getDraft(Mail_activity_type et) ;
    boolean checkKey(Mail_activity_type et) ;
    boolean create(Mail_activity_type et) ;
    void createBatch(List<Mail_activity_type> list) ;
    boolean update(Mail_activity_type et) ;
    void updateBatch(List<Mail_activity_type> list) ;
    boolean save(Mail_activity_type et) ;
    void saveBatch(List<Mail_activity_type> list) ;
    Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context) ;

}




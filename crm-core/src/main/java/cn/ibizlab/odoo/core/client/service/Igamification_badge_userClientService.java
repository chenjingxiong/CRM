package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_badge_user;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge_user] 服务对象接口
 */
public interface Igamification_badge_userClientService{

    public Igamification_badge_user createModel() ;

    public void createBatch(List<Igamification_badge_user> gamification_badge_users);

    public void get(Igamification_badge_user gamification_badge_user);

    public void updateBatch(List<Igamification_badge_user> gamification_badge_users);

    public void update(Igamification_badge_user gamification_badge_user);

    public void removeBatch(List<Igamification_badge_user> gamification_badge_users);

    public void create(Igamification_badge_user gamification_badge_user);

    public Page<Igamification_badge_user> fetchDefault(SearchContext context);

    public void remove(Igamification_badge_user gamification_badge_user);

    public Page<Igamification_badge_user> select(SearchContext context);

    public void getDraft(Igamification_badge_user gamification_badge_user);

}

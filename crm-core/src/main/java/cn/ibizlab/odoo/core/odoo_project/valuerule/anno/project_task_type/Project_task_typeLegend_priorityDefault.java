package cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_task_type;

import cn.ibizlab.odoo.core.odoo_project.valuerule.validator.project_task_type.Project_task_typeLegend_priorityDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Project_task_type
 * 属性：Legend_priority
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Project_task_typeLegend_priorityDefaultValidator.class})
public @interface Project_task_typeLegend_priorityDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

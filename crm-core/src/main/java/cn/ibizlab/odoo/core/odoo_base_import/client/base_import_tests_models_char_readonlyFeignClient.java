package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-char-readonly", fallback = base_import_tests_models_char_readonlyFallback.class)
public interface base_import_tests_models_char_readonlyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_readonlies/{id}")
    Base_import_tests_models_char_readonly update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_readonlies/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlies);




    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies")
    Base_import_tests_models_char_readonly create(@RequestBody Base_import_tests_models_char_readonly base_import_tests_models_char_readonly);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlies);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_readonlies/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_readonlies/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_readonlies/searchdefault")
    Page<Base_import_tests_models_char_readonly> searchDefault(@RequestBody Base_import_tests_models_char_readonlySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_readonlies/{id}")
    Base_import_tests_models_char_readonly get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_readonlies/select")
    Page<Base_import_tests_models_char_readonly> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_readonlies/getdraft")
    Base_import_tests_models_char_readonly getDraft();


}

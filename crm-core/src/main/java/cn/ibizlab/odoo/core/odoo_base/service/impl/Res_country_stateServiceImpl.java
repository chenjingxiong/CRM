package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_stateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_stateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_country_stateFeignClient;

/**
 * 实体[国家/地区州/省] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_country_stateServiceImpl implements IRes_country_stateService {

    @Autowired
    res_country_stateFeignClient res_country_stateFeignClient;


    @Override
    public Res_country_state get(Integer id) {
		Res_country_state et=res_country_stateFeignClient.get(id);
        if(et==null){
            et=new Res_country_state();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Res_country_state et) {
        Res_country_state rt = res_country_stateFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_country_state> list){
        res_country_stateFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_country_state et) {
        Res_country_state rt = res_country_stateFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_country_state> list){
        res_country_stateFeignClient.createBatch(list) ;
    }

    @Override
    public Res_country_state getDraft(Res_country_state et) {
        et=res_country_stateFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_country_stateFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_country_stateFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_country_state> searchDefault(Res_country_stateSearchContext context) {
        Page<Res_country_state> res_country_states=res_country_stateFeignClient.searchDefault(context);
        return res_country_states;
    }


}



package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_calendar] 服务对象接口
 */
@FeignClient(value = "odoo-resource", contextId = "resource-calendar", fallback = resource_calendarFallback.class)
public interface resource_calendarFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/{id}")
    Resource_calendar update(@PathVariable("id") Integer id,@RequestBody Resource_calendar resource_calendar);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/batch")
    Boolean updateBatch(@RequestBody List<Resource_calendar> resource_calendars);



    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/searchdefault")
    Page<Resource_calendar> searchDefault(@RequestBody Resource_calendarSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/{id}")
    Resource_calendar get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendars")
    Resource_calendar create(@RequestBody Resource_calendar resource_calendar);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/batch")
    Boolean createBatch(@RequestBody List<Resource_calendar> resource_calendars);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/select")
    Page<Resource_calendar> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/getdraft")
    Resource_calendar getDraft();


}

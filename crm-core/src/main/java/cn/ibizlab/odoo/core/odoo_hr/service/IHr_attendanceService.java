package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;


/**
 * 实体[Hr_attendance] 服务对象接口
 */
public interface IHr_attendanceService{

    Hr_attendance getDraft(Hr_attendance et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Hr_attendance et) ;
    void createBatch(List<Hr_attendance> list) ;
    boolean update(Hr_attendance et) ;
    void updateBatch(List<Hr_attendance> list) ;
    Hr_attendance get(Integer key) ;
    Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_config;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;


/**
 * 实体[Res_config] 服务对象接口
 */
public interface IRes_configService{

    Res_config get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Res_config et) ;
    void updateBatch(List<Res_config> list) ;
    boolean create(Res_config et) ;
    void createBatch(List<Res_config> list) ;
    Res_config getDraft(Res_config et) ;
    Page<Res_config> searchDefault(Res_configSearchContext context) ;

}




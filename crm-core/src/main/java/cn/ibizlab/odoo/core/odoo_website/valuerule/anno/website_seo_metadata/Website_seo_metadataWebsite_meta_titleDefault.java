package cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_seo_metadata;

import cn.ibizlab.odoo.core.odoo_website.valuerule.validator.website_seo_metadata.Website_seo_metadataWebsite_meta_titleDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Website_seo_metadata
 * 属性：Website_meta_title
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Website_seo_metadataWebsite_meta_titleDefaultValidator.class})
public @interface Website_seo_metadataWebsite_meta_titleDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

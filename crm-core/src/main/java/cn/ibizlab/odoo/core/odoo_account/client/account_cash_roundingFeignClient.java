package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_cash_rounding] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-cash-rounding", fallback = account_cash_roundingFallback.class)
public interface account_cash_roundingFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/{id}")
    Account_cash_rounding get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings")
    Account_cash_rounding create(@RequestBody Account_cash_rounding account_cash_rounding);

    @RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/batch")
    Boolean createBatch(@RequestBody List<Account_cash_rounding> account_cash_roundings);



    @RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/searchdefault")
    Page<Account_cash_rounding> searchDefault(@RequestBody Account_cash_roundingSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/{id}")
    Account_cash_rounding update(@PathVariable("id") Integer id,@RequestBody Account_cash_rounding account_cash_rounding);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/batch")
    Boolean updateBatch(@RequestBody List<Account_cash_rounding> account_cash_roundings);



    @RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/select")
    Page<Account_cash_rounding> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/getdraft")
    Account_cash_rounding getDraft();


}

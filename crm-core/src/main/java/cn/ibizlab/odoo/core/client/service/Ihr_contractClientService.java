package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_contract;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_contract] 服务对象接口
 */
public interface Ihr_contractClientService{

    public Ihr_contract createModel() ;

    public Page<Ihr_contract> fetchDefault(SearchContext context);

    public void remove(Ihr_contract hr_contract);

    public void get(Ihr_contract hr_contract);

    public void createBatch(List<Ihr_contract> hr_contracts);

    public void updateBatch(List<Ihr_contract> hr_contracts);

    public void create(Ihr_contract hr_contract);

    public void update(Ihr_contract hr_contract);

    public void removeBatch(List<Ihr_contract> hr_contracts);

    public Page<Ihr_contract> select(SearchContext context);

    public void getDraft(Ihr_contract hr_contract);

}

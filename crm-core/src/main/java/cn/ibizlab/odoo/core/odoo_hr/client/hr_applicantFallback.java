package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_applicant] 服务对象接口
 */
@Component
public class hr_applicantFallback implements hr_applicantFeignClient{

    public Hr_applicant get(Integer id){
            return null;
     }


    public Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context){
            return null;
     }



    public Hr_applicant create(Hr_applicant hr_applicant){
            return null;
     }
    public Boolean createBatch(List<Hr_applicant> hr_applicants){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Hr_applicant update(Integer id, Hr_applicant hr_applicant){
            return null;
     }
    public Boolean updateBatch(List<Hr_applicant> hr_applicants){
            return false;
     }



    public Page<Hr_applicant> select(){
            return null;
     }

    public Hr_applicant getDraft(){
            return null;
    }



}

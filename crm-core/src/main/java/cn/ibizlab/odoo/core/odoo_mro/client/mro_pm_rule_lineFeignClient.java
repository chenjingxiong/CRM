package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-pm-rule-line", fallback = mro_pm_rule_lineFallback.class)
public interface mro_pm_rule_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines")
    Mro_pm_rule_line create(@RequestBody Mro_pm_rule_line mro_pm_rule_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_rule_line> mro_pm_rule_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/searchdefault")
    Page<Mro_pm_rule_line> searchDefault(@RequestBody Mro_pm_rule_lineSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/{id}")
    Mro_pm_rule_line update(@PathVariable("id") Integer id,@RequestBody Mro_pm_rule_line mro_pm_rule_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_rule_line> mro_pm_rule_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/{id}")
    Mro_pm_rule_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/select")
    Page<Mro_pm_rule_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/getdraft")
    Mro_pm_rule_line getDraft();


}

package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;


/**
 * 实体[Account_invoice_line] 服务对象接口
 */
public interface IAccount_invoice_lineService{

    boolean update(Account_invoice_line et) ;
    void updateBatch(List<Account_invoice_line> list) ;
    boolean checkKey(Account_invoice_line et) ;
    Account_invoice_line get(Integer key) ;
    Account_invoice_line getDraft(Account_invoice_line et) ;
    boolean save(Account_invoice_line et) ;
    void saveBatch(List<Account_invoice_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_invoice_line et) ;
    void createBatch(List<Account_invoice_line> list) ;
    Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context) ;

}




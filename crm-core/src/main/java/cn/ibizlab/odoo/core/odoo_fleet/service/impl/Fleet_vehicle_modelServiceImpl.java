package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_modelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_modelFeignClient;

/**
 * 实体[车辆型号] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_modelServiceImpl implements IFleet_vehicle_modelService {

    @Autowired
    fleet_vehicle_modelFeignClient fleet_vehicle_modelFeignClient;


    @Override
    public boolean update(Fleet_vehicle_model et) {
        Fleet_vehicle_model rt = fleet_vehicle_modelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_model> list){
        fleet_vehicle_modelFeignClient.updateBatch(list) ;
    }

    @Override
    public Fleet_vehicle_model get(Integer id) {
		Fleet_vehicle_model et=fleet_vehicle_modelFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_model();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_modelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_modelFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Fleet_vehicle_model et) {
        Fleet_vehicle_model rt = fleet_vehicle_modelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_model> list){
        fleet_vehicle_modelFeignClient.createBatch(list) ;
    }

    @Override
    public Fleet_vehicle_model getDraft(Fleet_vehicle_model et) {
        et=fleet_vehicle_modelFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context) {
        Page<Fleet_vehicle_model> fleet_vehicle_models=fleet_vehicle_modelFeignClient.searchDefault(context);
        return fleet_vehicle_models;
    }


}



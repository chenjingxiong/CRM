package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_attendance] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-attendance", fallback = hr_attendanceFallback.class)
public interface hr_attendanceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/{id}")
    Hr_attendance update(@PathVariable("id") Integer id,@RequestBody Hr_attendance hr_attendance);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/batch")
    Boolean updateBatch(@RequestBody List<Hr_attendance> hr_attendances);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_attendances")
    Hr_attendance create(@RequestBody Hr_attendance hr_attendance);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/batch")
    Boolean createBatch(@RequestBody List<Hr_attendance> hr_attendances);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/{id}")
    Hr_attendance get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/searchdefault")
    Page<Hr_attendance> searchDefault(@RequestBody Hr_attendanceSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/select")
    Page<Hr_attendance> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/getdraft")
    Hr_attendance getDraft();


}

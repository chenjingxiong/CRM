package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_confirmSearchContext;


/**
 * 实体[Account_invoice_confirm] 服务对象接口
 */
public interface IAccount_invoice_confirmService{

    boolean create(Account_invoice_confirm et) ;
    void createBatch(List<Account_invoice_confirm> list) ;
    Account_invoice_confirm getDraft(Account_invoice_confirm et) ;
    Account_invoice_confirm get(Integer key) ;
    boolean update(Account_invoice_confirm et) ;
    void updateBatch(List<Account_invoice_confirm> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_invoice_confirm> searchDefault(Account_invoice_confirmSearchContext context) ;

}




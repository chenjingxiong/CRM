package cn.ibizlab.odoo.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[project_project] 服务对象接口
 */
@FeignClient(value = "odoo-project", contextId = "project-project", fallback = project_projectFallback.class)
public interface project_projectFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/project_projects/searchdefault")
    Page<Project_project> searchDefault(@RequestBody Project_projectSearchContext context);





    @RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/project_projects")
    Project_project create(@RequestBody Project_project project_project);

    @RequestMapping(method = RequestMethod.POST, value = "/project_projects/batch")
    Boolean createBatch(@RequestBody List<Project_project> project_projects);


    @RequestMapping(method = RequestMethod.GET, value = "/project_projects/{id}")
    Project_project get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/project_projects/{id}")
    Project_project update(@PathVariable("id") Integer id,@RequestBody Project_project project_project);

    @RequestMapping(method = RequestMethod.PUT, value = "/project_projects/batch")
    Boolean updateBatch(@RequestBody List<Project_project> project_projects);


    @RequestMapping(method = RequestMethod.GET, value = "/project_projects/select")
    Page<Project_project> select();


    @RequestMapping(method = RequestMethod.GET, value = "/project_projects/getdraft")
    Project_project getDraft();


}

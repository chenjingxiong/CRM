package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required_related;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_required_relatedSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_m2o_required_related] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-m2o-required-related", fallback = base_import_tests_models_m2o_required_relatedFallback.class)
public interface base_import_tests_models_m2o_required_relatedFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_required_relateds")
    Base_import_tests_models_m2o_required_related create(@RequestBody Base_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_required_relateds/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_required_relateds/{id}")
    Base_import_tests_models_m2o_required_related update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_required_relateds/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds);



    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_required_relateds/{id}")
    Base_import_tests_models_m2o_required_related get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_required_relateds/searchdefault")
    Page<Base_import_tests_models_m2o_required_related> searchDefault(@RequestBody Base_import_tests_models_m2o_required_relatedSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_required_relateds/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_required_relateds/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_required_relateds/select")
    Page<Base_import_tests_models_m2o_required_related> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_required_relateds/getdraft")
    Base_import_tests_models_m2o_required_related getDraft();


}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_bank_statement] 对象
 */
public interface Iaccount_bank_statement {

    /**
     * 获取 [会计日期]
     */
    public void setAccounting_date(Timestamp accounting_date);
    
    /**
     * 设置 [会计日期]
     */
    public Timestamp getAccounting_date();

    /**
     * 获取 [会计日期]脏标记
     */
    public boolean getAccounting_dateDirtyFlag();
    /**
     * 获取 [默认借方科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [默认借方科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [默认借方科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [所有的明细行都已核销]
     */
    public void setAll_lines_reconciled(String all_lines_reconciled);
    
    /**
     * 设置 [所有的明细行都已核销]
     */
    public String getAll_lines_reconciled();

    /**
     * 获取 [所有的明细行都已核销]脏标记
     */
    public boolean getAll_lines_reconciledDirtyFlag();
    /**
     * 获取 [计算余额]
     */
    public void setBalance_end(Double balance_end);
    
    /**
     * 设置 [计算余额]
     */
    public Double getBalance_end();

    /**
     * 获取 [计算余额]脏标记
     */
    public boolean getBalance_endDirtyFlag();
    /**
     * 获取 [结束余额]
     */
    public void setBalance_end_real(Double balance_end_real);
    
    /**
     * 设置 [结束余额]
     */
    public Double getBalance_end_real();

    /**
     * 获取 [结束余额]脏标记
     */
    public boolean getBalance_end_realDirtyFlag();
    /**
     * 获取 [起始余额]
     */
    public void setBalance_start(Double balance_start);
    
    /**
     * 设置 [起始余额]
     */
    public Double getBalance_start();

    /**
     * 获取 [起始余额]脏标记
     */
    public boolean getBalance_startDirtyFlag();
    /**
     * 获取 [关闭钱箱]
     */
    public void setCashbox_end_id(Integer cashbox_end_id);
    
    /**
     * 设置 [关闭钱箱]
     */
    public Integer getCashbox_end_id();

    /**
     * 获取 [关闭钱箱]脏标记
     */
    public boolean getCashbox_end_idDirtyFlag();
    /**
     * 获取 [开始钱箱]
     */
    public void setCashbox_start_id(Integer cashbox_start_id);
    
    /**
     * 设置 [开始钱箱]
     */
    public Integer getCashbox_start_id();

    /**
     * 获取 [开始钱箱]脏标记
     */
    public boolean getCashbox_start_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [关闭在]
     */
    public void setDate_done(Timestamp date_done);
    
    /**
     * 设置 [关闭在]
     */
    public Timestamp getDate_done();

    /**
     * 获取 [关闭在]脏标记
     */
    public boolean getDate_doneDirtyFlag();
    /**
     * 获取 [差异]
     */
    public void setDifference(Double difference);
    
    /**
     * 设置 [差异]
     */
    public Double getDifference();

    /**
     * 获取 [差异]脏标记
     */
    public boolean getDifferenceDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [是0]
     */
    public void setIs_difference_zero(String is_difference_zero);
    
    /**
     * 设置 [是0]
     */
    public String getIs_difference_zero();

    /**
     * 获取 [是0]脏标记
     */
    public boolean getIs_difference_zeroDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setJournal_type(String journal_type);
    
    /**
     * 设置 [类型]
     */
    public String getJournal_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getJournal_typeDirtyFlag();
    /**
     * 获取 [对账单明细行]
     */
    public void setLine_ids(String line_ids);
    
    /**
     * 设置 [对账单明细行]
     */
    public String getLine_ids();

    /**
     * 获取 [对账单明细行]脏标记
     */
    public boolean getLine_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [需要一个动作消息的编码]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [需要一个动作消息的编码]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作编号]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作编号]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作编号]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [凭证明细数]
     */
    public void setMove_line_count(Integer move_line_count);
    
    /**
     * 设置 [凭证明细数]
     */
    public Integer getMove_line_count();

    /**
     * 获取 [凭证明细数]脏标记
     */
    public boolean getMove_line_countDirtyFlag();
    /**
     * 获取 [分录明细行]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [分录明细行]
     */
    public String getMove_line_ids();

    /**
     * 获取 [分录明细行]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setName(String name);
    
    /**
     * 设置 [参考]
     */
    public String getName();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [会话]
     */
    public void setPos_session_id(Integer pos_session_id);
    
    /**
     * 设置 [会话]
     */
    public Integer getPos_session_id();

    /**
     * 获取 [会话]脏标记
     */
    public boolean getPos_session_idDirtyFlag();
    /**
     * 获取 [外部引用]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [外部引用]
     */
    public String getReference();

    /**
     * 获取 [外部引用]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [交易小计]
     */
    public void setTotal_entry_encoding(Double total_entry_encoding);
    
    /**
     * 设置 [交易小计]
     */
    public Double getTotal_entry_encoding();

    /**
     * 获取 [交易小计]脏标记
     */
    public boolean getTotal_entry_encodingDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

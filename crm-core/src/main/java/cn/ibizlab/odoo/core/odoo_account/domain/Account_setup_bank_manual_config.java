package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [银行设置通用配置] 对象
 */
@Data
public class Account_setup_bank_manual_config extends EntityClient implements Serializable {

    /**
     * 会计日记账
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private String journalId;

    /**
     * 日记账
     */
    @JSONField(name = "linked_journal_id")
    @JsonProperty("linked_journal_id")
    private Integer linkedJournalId;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 新科目名
     */
    @JSONField(name = "new_journal_name")
    @JsonProperty("new_journal_name")
    private String newJournalName;

    /**
     * 代码
     */
    @DEField(name = "new_journal_code")
    @JSONField(name = "new_journal_code")
    @JsonProperty("new_journal_code")
    private String newJournalCode;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建或链接选项
     */
    @DEField(name = "create_or_link_option")
    @JSONField(name = "create_or_link_option")
    @JsonProperty("create_or_link_option")
    private String createOrLinkOption;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 科目类型
     */
    @JSONField(name = "related_acc_type")
    @JsonProperty("related_acc_type")
    private String relatedAccType;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 账户持有人
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 名称
     */
    @JSONField(name = "bank_name")
    @JsonProperty("bank_name")
    private String bankName;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 银行
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Integer bankId;

    /**
     * ‎有所有必需的参数‎
     */
    @JSONField(name = "qr_code_valid")
    @JsonProperty("qr_code_valid")
    private String qrCodeValid;

    /**
     * 公司
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 核对银行账号
     */
    @JSONField(name = "sanitized_acc_number")
    @JsonProperty("sanitized_acc_number")
    private String sanitizedAccNumber;

    /**
     * 类型
     */
    @JSONField(name = "acc_type")
    @JsonProperty("acc_type")
    private String accType;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 账户号码
     */
    @JSONField(name = "acc_number")
    @JsonProperty("acc_number")
    private String accNumber;

    /**
     * 银行识别代码
     */
    @JSONField(name = "bank_bic")
    @JsonProperty("bank_bic")
    private String bankBic;

    /**
     * 账户持有人名称
     */
    @JSONField(name = "acc_holder_name")
    @JsonProperty("acc_holder_name")
    private String accHolderName;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 业务伙伴银行账户
     */
    @DEField(name = "res_partner_bank_id")
    @JSONField(name = "res_partner_bank_id")
    @JsonProperty("res_partner_bank_id")
    private Integer resPartnerBankId;


    /**
     * 
     */
    @JSONField(name = "odoorespartnerbank")
    @JsonProperty("odoorespartnerbank")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooResPartnerBank;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [代码]
     */
    public void setNewJournalCode(String newJournalCode){
        this.newJournalCode = newJournalCode ;
        this.modify("new_journal_code",newJournalCode);
    }
    /**
     * 设置 [创建或链接选项]
     */
    public void setCreateOrLinkOption(String createOrLinkOption){
        this.createOrLinkOption = createOrLinkOption ;
        this.modify("create_or_link_option",createOrLinkOption);
    }
    /**
     * 设置 [业务伙伴银行账户]
     */
    public void setResPartnerBankId(Integer resPartnerBankId){
        this.resPartnerBankId = resPartnerBankId ;
        this.modify("res_partner_bank_id",resPartnerBankId);
    }

}



package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;


/**
 * 实体[Lunch_alert] 服务对象接口
 */
public interface ILunch_alertService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Lunch_alert et) ;
    void updateBatch(List<Lunch_alert> list) ;
    Lunch_alert get(Integer key) ;
    Lunch_alert getDraft(Lunch_alert et) ;
    boolean create(Lunch_alert et) ;
    void createBatch(List<Lunch_alert> list) ;
    Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context) ;

}




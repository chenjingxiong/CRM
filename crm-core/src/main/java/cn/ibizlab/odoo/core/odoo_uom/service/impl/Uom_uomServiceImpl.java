package cn.ibizlab.odoo.core.odoo_uom.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_uomService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_uom.client.uom_uomFeignClient;

/**
 * 实体[产品计量单位] 服务对象接口实现
 */
@Slf4j
@Service
public class Uom_uomServiceImpl implements IUom_uomService {

    @Autowired
    uom_uomFeignClient uom_uomFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=uom_uomFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        uom_uomFeignClient.removeBatch(idList);
    }

    @Override
    public Uom_uom getDraft(Uom_uom et) {
        et=uom_uomFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Uom_uom et) {
        Uom_uom rt = uom_uomFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Uom_uom> list){
        uom_uomFeignClient.updateBatch(list) ;
    }

    @Override
    public Uom_uom get(Integer id) {
		Uom_uom et=uom_uomFeignClient.get(id);
        if(et==null){
            et=new Uom_uom();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Uom_uom et) {
        Uom_uom rt = uom_uomFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Uom_uom> list){
        uom_uomFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Uom_uom> searchDefault(Uom_uomSearchContext context) {
        Page<Uom_uom> uom_uoms=uom_uomFeignClient.searchDefault(context);
        return uom_uoms;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_picking_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_picking_type] 服务对象接口
 */
public interface Istock_picking_typeClientService{

    public Istock_picking_type createModel() ;

    public void createBatch(List<Istock_picking_type> stock_picking_types);

    public Page<Istock_picking_type> fetchDefault(SearchContext context);

    public void removeBatch(List<Istock_picking_type> stock_picking_types);

    public void create(Istock_picking_type stock_picking_type);

    public void update(Istock_picking_type stock_picking_type);

    public void get(Istock_picking_type stock_picking_type);

    public void updateBatch(List<Istock_picking_type> stock_picking_types);

    public void remove(Istock_picking_type stock_picking_type);

    public Page<Istock_picking_type> select(SearchContext context);

    public void getDraft(Istock_picking_type stock_picking_type);

}

package cn.ibizlab.odoo.core.odoo_base_import.valuerule.anno.base_import_tests_models_m2o;

import cn.ibizlab.odoo.core.odoo_base_import.valuerule.validator.base_import_tests_models_m2o.Base_import_tests_models_m2oValueDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_import_tests_models_m2o
 * 属性：Value
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_import_tests_models_m2oValueDefaultValidator.class})
public @interface Base_import_tests_models_m2oValueDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

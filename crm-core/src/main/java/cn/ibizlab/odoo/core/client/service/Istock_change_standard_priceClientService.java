package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_change_standard_price;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
public interface Istock_change_standard_priceClientService{

    public Istock_change_standard_price createModel() ;

    public void get(Istock_change_standard_price stock_change_standard_price);

    public void create(Istock_change_standard_price stock_change_standard_price);

    public void updateBatch(List<Istock_change_standard_price> stock_change_standard_prices);

    public void remove(Istock_change_standard_price stock_change_standard_price);

    public Page<Istock_change_standard_price> fetchDefault(SearchContext context);

    public void update(Istock_change_standard_price stock_change_standard_price);

    public void removeBatch(List<Istock_change_standard_price> stock_change_standard_prices);

    public void createBatch(List<Istock_change_standard_price> stock_change_standard_prices);

    public Page<Istock_change_standard_price> select(SearchContext context);

    public void getDraft(Istock_change_standard_price stock_change_standard_price);

}

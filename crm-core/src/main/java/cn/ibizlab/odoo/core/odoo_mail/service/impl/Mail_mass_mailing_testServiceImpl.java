package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_testFeignClient;

/**
 * 实体[示例邮件向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_testServiceImpl implements IMail_mass_mailing_testService {

    @Autowired
    mail_mass_mailing_testFeignClient mail_mass_mailing_testFeignClient;


    @Override
    public boolean update(Mail_mass_mailing_test et) {
        Mail_mass_mailing_test rt = mail_mass_mailing_testFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_test> list){
        mail_mass_mailing_testFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_mass_mailing_test et) {
        Mail_mass_mailing_test rt = mail_mass_mailing_testFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_test> list){
        mail_mass_mailing_testFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_mass_mailing_test getDraft(Mail_mass_mailing_test et) {
        et=mail_mass_mailing_testFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_mass_mailing_test get(Integer id) {
		Mail_mass_mailing_test et=mail_mass_mailing_testFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_test();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_testFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_testFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context) {
        Page<Mail_mass_mailing_test> mail_mass_mailing_tests=mail_mass_mailing_testFeignClient.searchDefault(context);
        return mail_mass_mailing_tests;
    }


}



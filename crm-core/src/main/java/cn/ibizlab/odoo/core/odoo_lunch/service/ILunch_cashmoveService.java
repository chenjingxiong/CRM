package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;


/**
 * 实体[Lunch_cashmove] 服务对象接口
 */
public interface ILunch_cashmoveService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Lunch_cashmove et) ;
    void updateBatch(List<Lunch_cashmove> list) ;
    boolean create(Lunch_cashmove et) ;
    void createBatch(List<Lunch_cashmove> list) ;
    Lunch_cashmove get(Integer key) ;
    Lunch_cashmove getDraft(Lunch_cashmove et) ;
    Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context) ;

}




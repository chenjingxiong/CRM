package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;


/**
 * 实体[Hr_recruitment_degree] 服务对象接口
 */
public interface IHr_recruitment_degreeService{

    Hr_recruitment_degree getDraft(Hr_recruitment_degree et) ;
    boolean update(Hr_recruitment_degree et) ;
    void updateBatch(List<Hr_recruitment_degree> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_recruitment_degree get(Integer key) ;
    boolean create(Hr_recruitment_degree et) ;
    void createBatch(List<Hr_recruitment_degree> list) ;
    Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context) ;

}




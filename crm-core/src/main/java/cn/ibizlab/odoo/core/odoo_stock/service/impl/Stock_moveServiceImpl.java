package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_moveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_moveFeignClient;

/**
 * 实体[库存移动] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_moveServiceImpl implements IStock_moveService {

    @Autowired
    stock_moveFeignClient stock_moveFeignClient;


    @Override
    public Stock_move get(Integer id) {
		Stock_move et=stock_moveFeignClient.get(id);
        if(et==null){
            et=new Stock_move();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_move getDraft(Stock_move et) {
        et=stock_moveFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Stock_move et) {
        Stock_move rt = stock_moveFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_move> list){
        stock_moveFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_move et) {
        Stock_move rt = stock_moveFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_move> list){
        stock_moveFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_moveFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_moveFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_move> searchDefault(Stock_moveSearchContext context) {
        Page<Stock_move> stock_moves=stock_moveFeignClient.searchDefault(context);
        return stock_moves;
    }


}



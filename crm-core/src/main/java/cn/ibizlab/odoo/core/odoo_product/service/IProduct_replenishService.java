package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;


/**
 * 实体[Product_replenish] 服务对象接口
 */
public interface IProduct_replenishService{

    Product_replenish get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Product_replenish et) ;
    void createBatch(List<Product_replenish> list) ;
    Product_replenish getDraft(Product_replenish et) ;
    boolean update(Product_replenish et) ;
    void updateBatch(List<Product_replenish> list) ;
    Page<Product_replenish> searchDefault(Product_replenishSearchContext context) ;

}




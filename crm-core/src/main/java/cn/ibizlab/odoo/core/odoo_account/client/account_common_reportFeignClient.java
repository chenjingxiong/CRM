package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_common_report] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-common-report", fallback = account_common_reportFallback.class)
public interface account_common_reportFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_common_reports/{id}")
    Account_common_report get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/account_common_reports")
    Account_common_report create(@RequestBody Account_common_report account_common_report);

    @RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/batch")
    Boolean createBatch(@RequestBody List<Account_common_report> account_common_reports);




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_common_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_common_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/searchdefault")
    Page<Account_common_report> searchDefault(@RequestBody Account_common_reportSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_common_reports/{id}")
    Account_common_report update(@PathVariable("id") Integer id,@RequestBody Account_common_report account_common_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_common_reports/batch")
    Boolean updateBatch(@RequestBody List<Account_common_report> account_common_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/account_common_reports/select")
    Page<Account_common_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_common_reports/getdraft")
    Account_common_report getDraft();


}

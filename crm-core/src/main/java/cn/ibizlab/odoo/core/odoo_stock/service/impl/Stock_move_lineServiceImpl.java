package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_move_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_move_lineFeignClient;

/**
 * 实体[产品移动(移库明细)] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_move_lineServiceImpl implements IStock_move_lineService {

    @Autowired
    stock_move_lineFeignClient stock_move_lineFeignClient;


    @Override
    public Stock_move_line get(Integer id) {
		Stock_move_line et=stock_move_lineFeignClient.get(id);
        if(et==null){
            et=new Stock_move_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_move_line et) {
        Stock_move_line rt = stock_move_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_move_line> list){
        stock_move_lineFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_move_line et) {
        Stock_move_line rt = stock_move_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_move_line> list){
        stock_move_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_move_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_move_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_move_line getDraft(Stock_move_line et) {
        et=stock_move_lineFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context) {
        Page<Stock_move_line> stock_move_lines=stock_move_lineFeignClient.searchDefault(context);
        return stock_move_lines;
    }


}



package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;


/**
 * 实体[Base_update_translations] 服务对象接口
 */
public interface IBase_update_translationsService{

    Base_update_translations getDraft(Base_update_translations et) ;
    boolean update(Base_update_translations et) ;
    void updateBatch(List<Base_update_translations> list) ;
    Base_update_translations get(Integer key) ;
    boolean create(Base_update_translations et) ;
    void createBatch(List<Base_update_translations> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context) ;

}




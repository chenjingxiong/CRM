package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_register_payments] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-register-payments", fallback = account_register_paymentsFallback.class)
public interface account_register_paymentsFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments")
    Account_register_payments create(@RequestBody Account_register_payments account_register_payments);

    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/batch")
    Boolean createBatch(@RequestBody List<Account_register_payments> account_register_payments);





    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/searchdefault")
    Page<Account_register_payments> searchDefault(@RequestBody Account_register_paymentsSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/{id}")
    Account_register_payments update(@PathVariable("id") Integer id,@RequestBody Account_register_payments account_register_payments);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/batch")
    Boolean updateBatch(@RequestBody List<Account_register_payments> account_register_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/{id}")
    Account_register_payments get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/select")
    Page<Account_register_payments> select();


    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/checkkey")
    Boolean checkKey(@RequestBody Account_register_payments account_register_payments);


    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/save")
    Boolean save(@RequestBody Account_register_payments account_register_payments);

    @RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/save")
    Boolean saveBatch(@RequestBody List<Account_register_payments> account_register_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/getdraft")
    Account_register_payments getDraft();


}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_move_reversal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move_reversal] 服务对象接口
 */
public interface Iaccount_move_reversalClientService{

    public Iaccount_move_reversal createModel() ;

    public void createBatch(List<Iaccount_move_reversal> account_move_reversals);

    public void update(Iaccount_move_reversal account_move_reversal);

    public void removeBatch(List<Iaccount_move_reversal> account_move_reversals);

    public void create(Iaccount_move_reversal account_move_reversal);

    public void updateBatch(List<Iaccount_move_reversal> account_move_reversals);

    public void get(Iaccount_move_reversal account_move_reversal);

    public void remove(Iaccount_move_reversal account_move_reversal);

    public Page<Iaccount_move_reversal> fetchDefault(SearchContext context);

    public Page<Iaccount_move_reversal> select(SearchContext context);

    public void getDraft(Iaccount_move_reversal account_move_reversal);

}

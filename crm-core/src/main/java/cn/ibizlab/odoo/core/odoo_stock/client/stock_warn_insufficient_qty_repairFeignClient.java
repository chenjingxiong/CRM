package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-warn-insufficient-qty-repair", fallback = stock_warn_insufficient_qty_repairFallback.class)
public interface stock_warn_insufficient_qty_repairFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/{id}")
    Stock_warn_insufficient_qty_repair get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs")
    Stock_warn_insufficient_qty_repair create(@RequestBody Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/batch")
    Boolean createBatch(@RequestBody List<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs);





    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/searchdefault")
    Page<Stock_warn_insufficient_qty_repair> searchDefault(@RequestBody Stock_warn_insufficient_qty_repairSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/{id}")
    Stock_warn_insufficient_qty_repair update(@PathVariable("id") Integer id,@RequestBody Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/batch")
    Boolean updateBatch(@RequestBody List<Stock_warn_insufficient_qty_repair> stock_warn_insufficient_qty_repairs);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/select")
    Page<Stock_warn_insufficient_qty_repair> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/getdraft")
    Stock_warn_insufficient_qty_repair getDraft();


}

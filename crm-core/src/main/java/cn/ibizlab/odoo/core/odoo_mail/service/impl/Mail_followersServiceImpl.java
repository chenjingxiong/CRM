package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_followersService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_followersFeignClient;

/**
 * 实体[文档关注者] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_followersServiceImpl implements IMail_followersService {

    @Autowired
    mail_followersFeignClient mail_followersFeignClient;


    @Override
    public Mail_followers getDraft(Mail_followers et) {
        et=mail_followersFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_followers get(Integer id) {
		Mail_followers et=mail_followersFeignClient.get(id);
        if(et==null){
            et=new Mail_followers();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_followers et) {
        Mail_followers rt = mail_followersFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_followers> list){
        mail_followersFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_followers et) {
        Mail_followers rt = mail_followersFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_followers> list){
        mail_followersFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_followersFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_followersFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_followers> searchDefault(Mail_followersSearchContext context) {
        Page<Mail_followers> mail_followerss=mail_followersFeignClient.searchDefault(context);
        return mail_followerss;
    }


}



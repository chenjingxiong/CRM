package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_resend_cancel] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-resend-cancel", fallback = mail_resend_cancelFallback.class)
public interface mail_resend_cancelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/{id}")
    Mail_resend_cancel update(@PathVariable("id") Integer id,@RequestBody Mail_resend_cancel mail_resend_cancel);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/batch")
    Boolean updateBatch(@RequestBody List<Mail_resend_cancel> mail_resend_cancels);






    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/searchdefault")
    Page<Mail_resend_cancel> searchDefault(@RequestBody Mail_resend_cancelSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels")
    Mail_resend_cancel create(@RequestBody Mail_resend_cancel mail_resend_cancel);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/batch")
    Boolean createBatch(@RequestBody List<Mail_resend_cancel> mail_resend_cancels);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/{id}")
    Mail_resend_cancel get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/select")
    Page<Mail_resend_cancel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/getdraft")
    Mail_resend_cancel getDraft();


}

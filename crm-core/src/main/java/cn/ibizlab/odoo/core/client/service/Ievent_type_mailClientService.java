package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_type_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_type_mail] 服务对象接口
 */
public interface Ievent_type_mailClientService{

    public Ievent_type_mail createModel() ;

    public void createBatch(List<Ievent_type_mail> event_type_mails);

    public void update(Ievent_type_mail event_type_mail);

    public void remove(Ievent_type_mail event_type_mail);

    public void updateBatch(List<Ievent_type_mail> event_type_mails);

    public void removeBatch(List<Ievent_type_mail> event_type_mails);

    public Page<Ievent_type_mail> fetchDefault(SearchContext context);

    public void get(Ievent_type_mail event_type_mail);

    public void create(Ievent_type_mail event_type_mail);

    public Page<Ievent_type_mail> select(SearchContext context);

    public void getDraft(Ievent_type_mail event_type_mail);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iutm_source;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_source] 服务对象接口
 */
public interface Iutm_sourceClientService{

    public Iutm_source createModel() ;

    public void createBatch(List<Iutm_source> utm_sources);

    public void updateBatch(List<Iutm_source> utm_sources);

    public void create(Iutm_source utm_source);

    public void get(Iutm_source utm_source);

    public void removeBatch(List<Iutm_source> utm_sources);

    public void remove(Iutm_source utm_source);

    public Page<Iutm_source> fetchDefault(SearchContext context);

    public void update(Iutm_source utm_source);

    public Page<Iutm_source> select(SearchContext context);

    public void getDraft(Iutm_source utm_source);

}

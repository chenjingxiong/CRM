package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-template-attribute-line", fallback = product_template_attribute_lineFallback.class)
public interface product_template_attribute_lineFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/searchdefault")
    Page<Product_template_attribute_line> searchDefault(@RequestBody Product_template_attribute_lineSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/{id}")
    Product_template_attribute_line update(@PathVariable("id") Integer id,@RequestBody Product_template_attribute_line product_template_attribute_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/batch")
    Boolean updateBatch(@RequestBody List<Product_template_attribute_line> product_template_attribute_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines")
    Product_template_attribute_line create(@RequestBody Product_template_attribute_line product_template_attribute_line);

    @RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/batch")
    Boolean createBatch(@RequestBody List<Product_template_attribute_line> product_template_attribute_lines);



    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/{id}")
    Product_template_attribute_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/select")
    Page<Product_template_attribute_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/getdraft")
    Product_template_attribute_line getDraft();


}

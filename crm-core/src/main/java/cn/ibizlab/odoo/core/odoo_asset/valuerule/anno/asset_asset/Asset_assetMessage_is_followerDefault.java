package cn.ibizlab.odoo.core.odoo_asset.valuerule.anno.asset_asset;

import cn.ibizlab.odoo.core.odoo_asset.valuerule.validator.asset_asset.Asset_assetMessage_is_followerDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Asset_asset
 * 属性：Message_is_follower
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Asset_assetMessage_is_followerDefaultValidator.class})
public @interface Asset_assetMessage_is_followerDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

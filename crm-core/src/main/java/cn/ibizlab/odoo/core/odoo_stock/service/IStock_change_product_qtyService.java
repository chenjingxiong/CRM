package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;


/**
 * 实体[Stock_change_product_qty] 服务对象接口
 */
public interface IStock_change_product_qtyService{

    boolean update(Stock_change_product_qty et) ;
    void updateBatch(List<Stock_change_product_qty> list) ;
    Stock_change_product_qty getDraft(Stock_change_product_qty et) ;
    boolean create(Stock_change_product_qty et) ;
    void createBatch(List<Stock_change_product_qty> list) ;
    Stock_change_product_qty get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_change_product_qty> searchDefault(Stock_change_product_qtySearchContext context) ;

}




package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_invoice] 对象
 */
public interface Iaccount_invoice {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问网址]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问网址]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问网址]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [按组分配税额]
     */
    public void setAmount_by_group(byte[] amount_by_group);
    
    /**
     * 设置 [按组分配税额]
     */
    public byte[] getAmount_by_group();

    /**
     * 获取 [按组分配税额]脏标记
     */
    public boolean getAmount_by_groupDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setAmount_tax(Double amount_tax);
    
    /**
     * 设置 [税率]
     */
    public Double getAmount_tax();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getAmount_taxDirtyFlag();
    /**
     * 获取 [Tax in Invoice Currency]
     */
    public void setAmount_tax_signed(Double amount_tax_signed);
    
    /**
     * 设置 [Tax in Invoice Currency]
     */
    public Double getAmount_tax_signed();

    /**
     * 获取 [Tax in Invoice Currency]脏标记
     */
    public boolean getAmount_tax_signedDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setAmount_total(Double amount_total);
    
    /**
     * 设置 [总计]
     */
    public Double getAmount_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getAmount_totalDirtyFlag();
    /**
     * 获取 [公司货币的合计]
     */
    public void setAmount_total_company_signed(Double amount_total_company_signed);
    
    /**
     * 设置 [公司货币的合计]
     */
    public Double getAmount_total_company_signed();

    /**
     * 获取 [公司货币的合计]脏标记
     */
    public boolean getAmount_total_company_signedDirtyFlag();
    /**
     * 获取 [以发票币种总计]
     */
    public void setAmount_total_signed(Double amount_total_signed);
    
    /**
     * 设置 [以发票币种总计]
     */
    public Double getAmount_total_signed();

    /**
     * 获取 [以发票币种总计]脏标记
     */
    public boolean getAmount_total_signedDirtyFlag();
    /**
     * 获取 [未税金额]
     */
    public void setAmount_untaxed(Double amount_untaxed);
    
    /**
     * 设置 [未税金额]
     */
    public Double getAmount_untaxed();

    /**
     * 获取 [未税金额]脏标记
     */
    public boolean getAmount_untaxedDirtyFlag();
    /**
     * 获取 [Untaxed Amount in Invoice Currency]
     */
    public void setAmount_untaxed_invoice_signed(Double amount_untaxed_invoice_signed);
    
    /**
     * 设置 [Untaxed Amount in Invoice Currency]
     */
    public Double getAmount_untaxed_invoice_signed();

    /**
     * 获取 [Untaxed Amount in Invoice Currency]脏标记
     */
    public boolean getAmount_untaxed_invoice_signedDirtyFlag();
    /**
     * 获取 [按公司本位币计的不含税金额]
     */
    public void setAmount_untaxed_signed(Double amount_untaxed_signed);
    
    /**
     * 设置 [按公司本位币计的不含税金额]
     */
    public Double getAmount_untaxed_signed();

    /**
     * 获取 [按公司本位币计的不含税金额]脏标记
     */
    public boolean getAmount_untaxed_signedDirtyFlag();
    /**
     * 获取 [已授权的交易]
     */
    public void setAuthorized_transaction_ids(String authorized_transaction_ids);
    
    /**
     * 设置 [已授权的交易]
     */
    public String getAuthorized_transaction_ids();

    /**
     * 获取 [已授权的交易]脏标记
     */
    public boolean getAuthorized_transaction_idsDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [现金舍入方式]
     */
    public void setCash_rounding_id(Integer cash_rounding_id);
    
    /**
     * 设置 [现金舍入方式]
     */
    public Integer getCash_rounding_id();

    /**
     * 获取 [现金舍入方式]脏标记
     */
    public boolean getCash_rounding_idDirtyFlag();
    /**
     * 获取 [现金舍入方式]
     */
    public void setCash_rounding_id_text(String cash_rounding_id_text);
    
    /**
     * 设置 [现金舍入方式]
     */
    public String getCash_rounding_id_text();

    /**
     * 获取 [现金舍入方式]脏标记
     */
    public boolean getCash_rounding_id_textDirtyFlag();
    /**
     * 获取 [额外的信息]
     */
    public void setComment(String comment);
    
    /**
     * 设置 [额外的信息]
     */
    public String getComment();

    /**
     * 获取 [额外的信息]脏标记
     */
    public boolean getCommentDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id(Integer commercial_partner_id);
    
    /**
     * 设置 [商业实体]
     */
    public Integer getCommercial_partner_id();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_idDirtyFlag();
    /**
     * 获取 [商业实体]
     */
    public void setCommercial_partner_id_text(String commercial_partner_id_text);
    
    /**
     * 设置 [商业实体]
     */
    public String getCommercial_partner_id_text();

    /**
     * 获取 [商业实体]脏标记
     */
    public boolean getCommercial_partner_id_textDirtyFlag();
    /**
     * 获取 [公司货币]
     */
    public void setCompany_currency_id(Integer company_currency_id);
    
    /**
     * 设置 [公司货币]
     */
    public Integer getCompany_currency_id();

    /**
     * 获取 [公司货币]脏标记
     */
    public boolean getCompany_currency_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [会计日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [会计日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [会计日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [到期日期]
     */
    public void setDate_due(Timestamp date_due);
    
    /**
     * 设置 [到期日期]
     */
    public Timestamp getDate_due();

    /**
     * 获取 [到期日期]脏标记
     */
    public boolean getDate_dueDirtyFlag();
    /**
     * 获取 [开票日期]
     */
    public void setDate_invoice(Timestamp date_invoice);
    
    /**
     * 设置 [开票日期]
     */
    public Timestamp getDate_invoice();

    /**
     * 获取 [开票日期]脏标记
     */
    public boolean getDate_invoiceDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id(Integer fiscal_position_id);
    
    /**
     * 设置 [税科目调整]
     */
    public Integer getFiscal_position_id();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_idDirtyFlag();
    /**
     * 获取 [税科目调整]
     */
    public void setFiscal_position_id_text(String fiscal_position_id_text);
    
    /**
     * 设置 [税科目调整]
     */
    public String getFiscal_position_id_text();

    /**
     * 获取 [税科目调整]脏标记
     */
    public boolean getFiscal_position_id_textDirtyFlag();
    /**
     * 获取 [有未清项]
     */
    public void setHas_outstanding(String has_outstanding);
    
    /**
     * 设置 [有未清项]
     */
    public String getHas_outstanding();

    /**
     * 获取 [有未清项]脏标记
     */
    public boolean getHas_outstandingDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [贸易条款]
     */
    public void setIncoterms_id(Integer incoterms_id);
    
    /**
     * 设置 [贸易条款]
     */
    public Integer getIncoterms_id();

    /**
     * 获取 [贸易条款]脏标记
     */
    public boolean getIncoterms_idDirtyFlag();
    /**
     * 获取 [贸易条款]
     */
    public void setIncoterms_id_text(String incoterms_id_text);
    
    /**
     * 设置 [贸易条款]
     */
    public String getIncoterms_id_text();

    /**
     * 获取 [贸易条款]脏标记
     */
    public boolean getIncoterms_id_textDirtyFlag();
    /**
     * 获取 [国际贸易术语]
     */
    public void setIncoterm_id(Integer incoterm_id);
    
    /**
     * 设置 [国际贸易术语]
     */
    public Integer getIncoterm_id();

    /**
     * 获取 [国际贸易术语]脏标记
     */
    public boolean getIncoterm_idDirtyFlag();
    /**
     * 获取 [国际贸易术语]
     */
    public void setIncoterm_id_text(String incoterm_id_text);
    
    /**
     * 设置 [国际贸易术语]
     */
    public String getIncoterm_id_text();

    /**
     * 获取 [国际贸易术语]脏标记
     */
    public boolean getIncoterm_id_textDirtyFlag();
    /**
     * 获取 [发票标示]
     */
    public void setInvoice_icon(String invoice_icon);
    
    /**
     * 设置 [发票标示]
     */
    public String getInvoice_icon();

    /**
     * 获取 [发票标示]脏标记
     */
    public boolean getInvoice_iconDirtyFlag();
    /**
     * 获取 [发票行]
     */
    public void setInvoice_line_ids(String invoice_line_ids);
    
    /**
     * 设置 [发票行]
     */
    public String getInvoice_line_ids();

    /**
     * 获取 [发票行]脏标记
     */
    public boolean getInvoice_line_idsDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [需要一个动作消息的编码]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [需要一个动作消息的编码]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作编号]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作编号]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作编号]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [日记账分录]
     */
    public void setMove_id(Integer move_id);
    
    /**
     * 设置 [日记账分录]
     */
    public Integer getMove_id();

    /**
     * 获取 [日记账分录]脏标记
     */
    public boolean getMove_idDirtyFlag();
    /**
     * 获取 [日记账分录名称]
     */
    public void setMove_name(String move_name);
    
    /**
     * 设置 [日记账分录名称]
     */
    public String getMove_name();

    /**
     * 获取 [日记账分录名称]脏标记
     */
    public boolean getMove_nameDirtyFlag();
    /**
     * 获取 [参考/说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [参考/说明]
     */
    public String getName();

    /**
     * 获取 [参考/说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [号码]
     */
    public void setNumber(String number);
    
    /**
     * 设置 [号码]
     */
    public String getNumber();

    /**
     * 获取 [号码]脏标记
     */
    public boolean getNumberDirtyFlag();
    /**
     * 获取 [源文档]
     */
    public void setOrigin(String origin);
    
    /**
     * 设置 [源文档]
     */
    public String getOrigin();

    /**
     * 获取 [源文档]脏标记
     */
    public boolean getOriginDirtyFlag();
    /**
     * 获取 [未到期贷项]
     */
    public void setOutstanding_credits_debits_widget(String outstanding_credits_debits_widget);
    
    /**
     * 设置 [未到期贷项]
     */
    public String getOutstanding_credits_debits_widget();

    /**
     * 获取 [未到期贷项]脏标记
     */
    public boolean getOutstanding_credits_debits_widgetDirtyFlag();
    /**
     * 获取 [银行账户]
     */
    public void setPartner_bank_id(Integer partner_bank_id);
    
    /**
     * 设置 [银行账户]
     */
    public Integer getPartner_bank_id();

    /**
     * 获取 [银行账户]脏标记
     */
    public boolean getPartner_bank_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [送货地址]
     */
    public void setPartner_shipping_id(Integer partner_shipping_id);
    
    /**
     * 设置 [送货地址]
     */
    public Integer getPartner_shipping_id();

    /**
     * 获取 [送货地址]脏标记
     */
    public boolean getPartner_shipping_idDirtyFlag();
    /**
     * 获取 [送货地址]
     */
    public void setPartner_shipping_id_text(String partner_shipping_id_text);
    
    /**
     * 设置 [送货地址]
     */
    public String getPartner_shipping_id_text();

    /**
     * 获取 [送货地址]脏标记
     */
    public boolean getPartner_shipping_id_textDirtyFlag();
    /**
     * 获取 [支付挂件]
     */
    public void setPayments_widget(String payments_widget);
    
    /**
     * 设置 [支付挂件]
     */
    public String getPayments_widget();

    /**
     * 获取 [支付挂件]脏标记
     */
    public boolean getPayments_widgetDirtyFlag();
    /**
     * 获取 [付款]
     */
    public void setPayment_ids(String payment_ids);
    
    /**
     * 设置 [付款]
     */
    public String getPayment_ids();

    /**
     * 获取 [付款]脏标记
     */
    public boolean getPayment_idsDirtyFlag();
    /**
     * 获取 [付款凭证明细]
     */
    public void setPayment_move_line_ids(String payment_move_line_ids);
    
    /**
     * 设置 [付款凭证明细]
     */
    public String getPayment_move_line_ids();

    /**
     * 获取 [付款凭证明细]脏标记
     */
    public boolean getPayment_move_line_idsDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id(Integer payment_term_id);
    
    /**
     * 设置 [付款条款]
     */
    public Integer getPayment_term_id();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_idDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_term_id_text(String payment_term_id_text);
    
    /**
     * 设置 [付款条款]
     */
    public String getPayment_term_id_text();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_term_id_textDirtyFlag();
    /**
     * 获取 [添加采购订单]
     */
    public void setPurchase_id(Integer purchase_id);
    
    /**
     * 设置 [添加采购订单]
     */
    public Integer getPurchase_id();

    /**
     * 获取 [添加采购订单]脏标记
     */
    public boolean getPurchase_idDirtyFlag();
    /**
     * 获取 [添加采购订单]
     */
    public void setPurchase_id_text(String purchase_id_text);
    
    /**
     * 设置 [添加采购订单]
     */
    public String getPurchase_id_text();

    /**
     * 获取 [添加采购订单]脏标记
     */
    public boolean getPurchase_id_textDirtyFlag();
    /**
     * 获取 [已付／已核销]
     */
    public void setReconciled(String reconciled);
    
    /**
     * 设置 [已付／已核销]
     */
    public String getReconciled();

    /**
     * 获取 [已付／已核销]脏标记
     */
    public boolean getReconciledDirtyFlag();
    /**
     * 获取 [付款参考:]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [付款参考:]
     */
    public String getReference();

    /**
     * 获取 [付款参考:]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [此发票为信用票的发票]
     */
    public void setRefund_invoice_id(Integer refund_invoice_id);
    
    /**
     * 设置 [此发票为信用票的发票]
     */
    public Integer getRefund_invoice_id();

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    public boolean getRefund_invoice_idDirtyFlag();
    /**
     * 获取 [退款发票]
     */
    public void setRefund_invoice_ids(String refund_invoice_ids);
    
    /**
     * 设置 [退款发票]
     */
    public String getRefund_invoice_ids();

    /**
     * 获取 [退款发票]脏标记
     */
    public boolean getRefund_invoice_idsDirtyFlag();
    /**
     * 获取 [此发票为信用票的发票]
     */
    public void setRefund_invoice_id_text(String refund_invoice_id_text);
    
    /**
     * 设置 [此发票为信用票的发票]
     */
    public String getRefund_invoice_id_text();

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    public boolean getRefund_invoice_id_textDirtyFlag();
    /**
     * 获取 [到期金额]
     */
    public void setResidual(Double residual);
    
    /**
     * 设置 [到期金额]
     */
    public Double getResidual();

    /**
     * 获取 [到期金额]脏标记
     */
    public boolean getResidualDirtyFlag();
    /**
     * 获取 [公司使用币种的逾期金额]
     */
    public void setResidual_company_signed(Double residual_company_signed);
    
    /**
     * 设置 [公司使用币种的逾期金额]
     */
    public Double getResidual_company_signed();

    /**
     * 获取 [公司使用币种的逾期金额]脏标记
     */
    public boolean getResidual_company_signedDirtyFlag();
    /**
     * 获取 [发票使用币种的逾期金额]
     */
    public void setResidual_signed(Double residual_signed);
    
    /**
     * 设置 [发票使用币种的逾期金额]
     */
    public Double getResidual_signed();

    /**
     * 获取 [发票使用币种的逾期金额]脏标记
     */
    public boolean getResidual_signedDirtyFlag();
    /**
     * 获取 [已汇]
     */
    public void setSent(String sent);
    
    /**
     * 设置 [已汇]
     */
    public String getSent();

    /**
     * 获取 [已汇]脏标记
     */
    public boolean getSentDirtyFlag();
    /**
     * 获取 [下一号码]
     */
    public void setSequence_number_next(String sequence_number_next);
    
    /**
     * 设置 [下一号码]
     */
    public String getSequence_number_next();

    /**
     * 获取 [下一号码]脏标记
     */
    public boolean getSequence_number_nextDirtyFlag();
    /**
     * 获取 [下一个号码前缀]
     */
    public void setSequence_number_next_prefix(String sequence_number_next_prefix);
    
    /**
     * 设置 [下一个号码前缀]
     */
    public String getSequence_number_next_prefix();

    /**
     * 获取 [下一个号码前缀]脏标记
     */
    public boolean getSequence_number_next_prefixDirtyFlag();
    /**
     * 获取 [源邮箱]
     */
    public void setSource_email(String source_email);
    
    /**
     * 设置 [源邮箱]
     */
    public String getSource_email();

    /**
     * 获取 [源邮箱]脏标记
     */
    public boolean getSource_emailDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [税率明细行]
     */
    public void setTax_line_ids(String tax_line_ids);
    
    /**
     * 设置 [税率明细行]
     */
    public String getTax_line_ids();

    /**
     * 获取 [税率明细行]脏标记
     */
    public boolean getTax_line_idsDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [交易]
     */
    public void setTransaction_ids(String transaction_ids);
    
    /**
     * 设置 [交易]
     */
    public String getTransaction_ids();

    /**
     * 获取 [交易]脏标记
     */
    public boolean getTransaction_idsDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [供应商账单]
     */
    public void setVendor_bill_id(Integer vendor_bill_id);
    
    /**
     * 设置 [供应商账单]
     */
    public Integer getVendor_bill_id();

    /**
     * 获取 [供应商账单]脏标记
     */
    public boolean getVendor_bill_idDirtyFlag();
    /**
     * 获取 [供应商账单]
     */
    public void setVendor_bill_id_text(String vendor_bill_id_text);
    
    /**
     * 设置 [供应商账单]
     */
    public String getVendor_bill_id_text();

    /**
     * 获取 [供应商账单]脏标记
     */
    public boolean getVendor_bill_id_textDirtyFlag();
    /**
     * 获取 [自动完成]
     */
    public void setVendor_bill_purchase_id(Integer vendor_bill_purchase_id);
    
    /**
     * 设置 [自动完成]
     */
    public Integer getVendor_bill_purchase_id();

    /**
     * 获取 [自动完成]脏标记
     */
    public boolean getVendor_bill_purchase_idDirtyFlag();
    /**
     * 获取 [自动完成]
     */
    public void setVendor_bill_purchase_id_text(String vendor_bill_purchase_id_text);
    
    /**
     * 设置 [自动完成]
     */
    public String getVendor_bill_purchase_id_text();

    /**
     * 获取 [自动完成]脏标记
     */
    public boolean getVendor_bill_purchase_id_textDirtyFlag();
    /**
     * 获取 [供应商名称]
     */
    public void setVendor_display_name(String vendor_display_name);
    
    /**
     * 设置 [供应商名称]
     */
    public String getVendor_display_name();

    /**
     * 获取 [供应商名称]脏标记
     */
    public boolean getVendor_display_nameDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

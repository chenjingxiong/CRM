package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [税率] 对象
 */
@Data
public class Account_tax extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 税范围
     */
    @DEField(name = "type_tax_use")
    @JSONField(name = "type_tax_use")
    @JsonProperty("type_tax_use")
    private String typeTaxUse;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 子级税
     */
    @JSONField(name = "children_tax_ids")
    @JsonProperty("children_tax_ids")
    private String childrenTaxIds;

    /**
     * 包含在分析成本
     */
    @JSONField(name = "analytic")
    @JsonProperty("analytic")
    private String analytic;

    /**
     * 包含在价格中
     */
    @DEField(name = "price_include")
    @JSONField(name = "price_include")
    @JsonProperty("price_include")
    private String priceInclude;

    /**
     * 发票上的标签
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 应有税金
     */
    @DEField(name = "tax_exigibility")
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 税率名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 税率计算
     */
    @DEField(name = "amount_type")
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    private String amountType;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 影响后续税收的基础
     */
    @DEField(name = "include_base_amount")
    @JSONField(name = "include_base_amount")
    @JsonProperty("include_base_amount")
    private String includeBaseAmount;

    /**
     * 基本税应收科目
     */
    @JSONField(name = "cash_basis_base_account_id_text")
    @JsonProperty("cash_basis_base_account_id_text")
    private String cashBasisBaseAccountIdText;

    /**
     * 税应收科目
     */
    @JSONField(name = "cash_basis_account_id_text")
    @JsonProperty("cash_basis_account_id_text")
    private String cashBasisAccountIdText;

    /**
     * 隐藏现金收付制选项
     */
    @JSONField(name = "hide_tax_exigibility")
    @JsonProperty("hide_tax_exigibility")
    private String hideTaxExigibility;

    /**
     * 税组
     */
    @JSONField(name = "tax_group_id_text")
    @JsonProperty("tax_group_id_text")
    private String taxGroupIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 退款单的税率科目
     */
    @JSONField(name = "refund_account_id_text")
    @JsonProperty("refund_account_id_text")
    private String refundAccountIdText;

    /**
     * 税率科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 退款单的税率科目
     */
    @DEField(name = "refund_account_id")
    @JSONField(name = "refund_account_id")
    @JsonProperty("refund_account_id")
    private Integer refundAccountId;

    /**
     * 税应收科目
     */
    @DEField(name = "cash_basis_account_id")
    @JSONField(name = "cash_basis_account_id")
    @JsonProperty("cash_basis_account_id")
    private Integer cashBasisAccountId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 税率科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 基本税应收科目
     */
    @DEField(name = "cash_basis_base_account_id")
    @JSONField(name = "cash_basis_base_account_id")
    @JsonProperty("cash_basis_base_account_id")
    private Integer cashBasisBaseAccountId;

    /**
     * 税组
     */
    @DEField(name = "tax_group_id")
    @JSONField(name = "tax_group_id")
    @JsonProperty("tax_group_id")
    private Integer taxGroupId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odoocashbasisaccount")
    @JsonProperty("odoocashbasisaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooCashBasisAccount;

    /**
     * 
     */
    @JSONField(name = "odoocashbasisbaseaccount")
    @JsonProperty("odoocashbasisbaseaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooCashBasisBaseAccount;

    /**
     * 
     */
    @JSONField(name = "odoorefundaccount")
    @JsonProperty("odoorefundaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooRefundAccount;

    /**
     * 
     */
    @JSONField(name = "odootaxgroup")
    @JsonProperty("odootaxgroup")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group odooTaxGroup;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [税范围]
     */
    public void setTypeTaxUse(String typeTaxUse){
        this.typeTaxUse = typeTaxUse ;
        this.modify("type_tax_use",typeTaxUse);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [包含在分析成本]
     */
    public void setAnalytic(String analytic){
        this.analytic = analytic ;
        this.modify("analytic",analytic);
    }
    /**
     * 设置 [包含在价格中]
     */
    public void setPriceInclude(String priceInclude){
        this.priceInclude = priceInclude ;
        this.modify("price_include",priceInclude);
    }
    /**
     * 设置 [发票上的标签]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [应有税金]
     */
    public void setTaxExigibility(String taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }
    /**
     * 设置 [税率名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [税率计算]
     */
    public void setAmountType(String amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }
    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [影响后续税收的基础]
     */
    public void setIncludeBaseAmount(String includeBaseAmount){
        this.includeBaseAmount = includeBaseAmount ;
        this.modify("include_base_amount",includeBaseAmount);
    }
    /**
     * 设置 [退款单的税率科目]
     */
    public void setRefundAccountId(Integer refundAccountId){
        this.refundAccountId = refundAccountId ;
        this.modify("refund_account_id",refundAccountId);
    }
    /**
     * 设置 [税应收科目]
     */
    public void setCashBasisAccountId(Integer cashBasisAccountId){
        this.cashBasisAccountId = cashBasisAccountId ;
        this.modify("cash_basis_account_id",cashBasisAccountId);
    }
    /**
     * 设置 [税率科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [基本税应收科目]
     */
    public void setCashBasisBaseAccountId(Integer cashBasisBaseAccountId){
        this.cashBasisBaseAccountId = cashBasisBaseAccountId ;
        this.modify("cash_basis_base_account_id",cashBasisBaseAccountId);
    }
    /**
     * 设置 [税组]
     */
    public void setTaxGroupId(Integer taxGroupId){
        this.taxGroupId = taxGroupId ;
        this.modify("tax_group_id",taxGroupId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

}



package cn.ibizlab.odoo.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;


/**
 * 实体[Maintenance_request] 服务对象接口
 */
public interface IMaintenance_requestService{

    Maintenance_request getDraft(Maintenance_request et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Maintenance_request et) ;
    void createBatch(List<Maintenance_request> list) ;
    boolean update(Maintenance_request et) ;
    void updateBatch(List<Maintenance_request> list) ;
    Maintenance_request get(Integer key) ;
    Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context) ;

}




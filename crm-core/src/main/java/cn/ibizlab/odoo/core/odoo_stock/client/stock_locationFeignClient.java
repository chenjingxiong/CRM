package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_location] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-location", fallback = stock_locationFallback.class)
public interface stock_locationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/{id}")
    Stock_location update(@PathVariable("id") Integer id,@RequestBody Stock_location stock_location);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/batch")
    Boolean updateBatch(@RequestBody List<Stock_location> stock_locations);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_locations")
    Stock_location create(@RequestBody Stock_location stock_location);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_locations/batch")
    Boolean createBatch(@RequestBody List<Stock_location> stock_locations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_locations/{id}")
    Stock_location get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_locations/searchdefault")
    Page<Stock_location> searchDefault(@RequestBody Stock_locationSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_locations/select")
    Page<Stock_location> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_locations/getdraft")
    Stock_location getDraft();


}

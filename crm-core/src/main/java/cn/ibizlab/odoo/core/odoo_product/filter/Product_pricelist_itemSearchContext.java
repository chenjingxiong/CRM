package cn.ibizlab.odoo.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Product_pricelist_item] 查询条件对象
 */
@Slf4j
@Data
public class Product_pricelist_itemSearchContext extends SearchContextBase {
	private String n_name_like;//[名称]

	private String n_base_eq;//[基于]

	private String n_compute_price_eq;//[计算价格]

	private String n_applied_on_eq;//[应用于]

	private String n_pricelist_id_text_eq;//[价格表]

	private String n_pricelist_id_text_like;//[价格表]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_categ_id_text_eq;//[产品种类]

	private String n_categ_id_text_like;//[产品种类]

	private String n_base_pricelist_id_text_eq;//[其他价格表]

	private String n_base_pricelist_id_text_like;//[其他价格表]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_product_tmpl_id_text_eq;//[产品模板]

	private String n_product_tmpl_id_text_like;//[产品模板]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_product_id_text_eq;//[产品]

	private String n_product_id_text_like;//[产品]

	private Integer n_product_id_eq;//[产品]

	private Integer n_pricelist_id_eq;//[价格表]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_company_id_eq;//[公司]

	private Integer n_base_pricelist_id_eq;//[其他价格表]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_categ_id_eq;//[产品种类]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_product_tmpl_id_eq;//[产品模板]

}




package cn.ibizlab.odoo.core.odoo_calendar.valuerule.anno.calendar_contacts;

import cn.ibizlab.odoo.core.odoo_calendar.valuerule.validator.calendar_contacts.Calendar_contactsIdDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Calendar_contacts
 * 属性：Id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Calendar_contactsIdDefaultValidator.class})
public @interface Calendar_contactsIdDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

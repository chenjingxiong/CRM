package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partner_categoryFeignClient;

/**
 * 实体[业务伙伴标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_categoryServiceImpl implements IRes_partner_categoryService {

    @Autowired
    res_partner_categoryFeignClient res_partner_categoryFeignClient;


    @Override
    public Res_partner_category get(Integer id) {
		Res_partner_category et=res_partner_categoryFeignClient.get(id);
        if(et==null){
            et=new Res_partner_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_partner_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partner_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Res_partner_category et) {
        Res_partner_category rt = res_partner_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_category> list){
        res_partner_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Res_partner_category et) {
        Res_partner_category rt = res_partner_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner_category> list){
        res_partner_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_partner_category getDraft(Res_partner_category et) {
        et=res_partner_categoryFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context) {
        Page<Res_partner_category> res_partner_categorys=res_partner_categoryFeignClient.searchDefault(context);
        return res_partner_categorys;
    }


}



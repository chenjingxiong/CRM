package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;


/**
 * 实体[Account_chart_template] 服务对象接口
 */
public interface IAccount_chart_templateService{

    Account_chart_template getDraft(Account_chart_template et) ;
    Account_chart_template get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_chart_template et) ;
    void updateBatch(List<Account_chart_template> list) ;
    boolean create(Account_chart_template et) ;
    void createBatch(List<Account_chart_template> list) ;
    Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context) ;

}




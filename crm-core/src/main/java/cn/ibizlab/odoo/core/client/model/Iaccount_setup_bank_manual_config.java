package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_setup_bank_manual_config] 对象
 */
public interface Iaccount_setup_bank_manual_config {

    /**
     * 获取 [账户持有人名称]
     */
    public void setAcc_holder_name(String acc_holder_name);
    
    /**
     * 设置 [账户持有人名称]
     */
    public String getAcc_holder_name();

    /**
     * 获取 [账户持有人名称]脏标记
     */
    public boolean getAcc_holder_nameDirtyFlag();
    /**
     * 获取 [账户号码]
     */
    public void setAcc_number(String acc_number);
    
    /**
     * 设置 [账户号码]
     */
    public String getAcc_number();

    /**
     * 获取 [账户号码]脏标记
     */
    public boolean getAcc_numberDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setAcc_type(String acc_type);
    
    /**
     * 设置 [类型]
     */
    public String getAcc_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getAcc_typeDirtyFlag();
    /**
     * 获取 [银行识别代码]
     */
    public void setBank_bic(String bank_bic);
    
    /**
     * 设置 [银行识别代码]
     */
    public String getBank_bic();

    /**
     * 获取 [银行识别代码]脏标记
     */
    public boolean getBank_bicDirtyFlag();
    /**
     * 获取 [银行]
     */
    public void setBank_id(Integer bank_id);
    
    /**
     * 设置 [银行]
     */
    public Integer getBank_id();

    /**
     * 获取 [银行]脏标记
     */
    public boolean getBank_idDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setBank_name(String bank_name);
    
    /**
     * 设置 [名称]
     */
    public String getBank_name();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getBank_nameDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建或链接选项]
     */
    public void setCreate_or_link_option(String create_or_link_option);
    
    /**
     * 设置 [创建或链接选项]
     */
    public String getCreate_or_link_option();

    /**
     * 获取 [创建或链接选项]脏标记
     */
    public boolean getCreate_or_link_optionDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [会计日记账]
     */
    public void setJournal_id(String journal_id);
    
    /**
     * 设置 [会计日记账]
     */
    public String getJournal_id();

    /**
     * 获取 [会计日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setLinked_journal_id(Integer linked_journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getLinked_journal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getLinked_journal_idDirtyFlag();
    /**
     * 获取 [代码]
     */
    public void setNew_journal_code(String new_journal_code);
    
    /**
     * 设置 [代码]
     */
    public String getNew_journal_code();

    /**
     * 获取 [代码]脏标记
     */
    public boolean getNew_journal_codeDirtyFlag();
    /**
     * 获取 [新科目名]
     */
    public void setNew_journal_name(String new_journal_name);
    
    /**
     * 设置 [新科目名]
     */
    public String getNew_journal_name();

    /**
     * 获取 [新科目名]脏标记
     */
    public boolean getNew_journal_nameDirtyFlag();
    /**
     * 获取 [账户持有人]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [账户持有人]
     */
    public Integer getPartner_id();

    /**
     * 获取 [账户持有人]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [‎有所有必需的参数‎]
     */
    public void setQr_code_valid(String qr_code_valid);
    
    /**
     * 设置 [‎有所有必需的参数‎]
     */
    public String getQr_code_valid();

    /**
     * 获取 [‎有所有必需的参数‎]脏标记
     */
    public boolean getQr_code_validDirtyFlag();
    /**
     * 获取 [科目类型]
     */
    public void setRelated_acc_type(String related_acc_type);
    
    /**
     * 设置 [科目类型]
     */
    public String getRelated_acc_type();

    /**
     * 获取 [科目类型]脏标记
     */
    public boolean getRelated_acc_typeDirtyFlag();
    /**
     * 获取 [业务伙伴银行账户]
     */
    public void setRes_partner_bank_id(Integer res_partner_bank_id);
    
    /**
     * 设置 [业务伙伴银行账户]
     */
    public Integer getRes_partner_bank_id();

    /**
     * 获取 [业务伙伴银行账户]脏标记
     */
    public boolean getRes_partner_bank_idDirtyFlag();
    /**
     * 获取 [核对银行账号]
     */
    public void setSanitized_acc_number(String sanitized_acc_number);
    
    /**
     * 设置 [核对银行账号]
     */
    public String getSanitized_acc_number();

    /**
     * 获取 [核对银行账号]脏标记
     */
    public boolean getSanitized_acc_numberDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

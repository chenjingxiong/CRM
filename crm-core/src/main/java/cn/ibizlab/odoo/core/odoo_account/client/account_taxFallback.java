package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_tax] 服务对象接口
 */
@Component
public class account_taxFallback implements account_taxFeignClient{

    public Account_tax get(Integer id){
            return null;
     }


    public Account_tax create(Account_tax account_tax){
            return null;
     }
    public Boolean createBatch(List<Account_tax> account_taxes){
            return false;
     }


    public Account_tax update(Integer id, Account_tax account_tax){
            return null;
     }
    public Boolean updateBatch(List<Account_tax> account_taxes){
            return false;
     }



    public Page<Account_tax> searchDefault(Account_taxSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_tax> select(){
            return null;
     }

    public Account_tax getDraft(){
            return null;
    }



}

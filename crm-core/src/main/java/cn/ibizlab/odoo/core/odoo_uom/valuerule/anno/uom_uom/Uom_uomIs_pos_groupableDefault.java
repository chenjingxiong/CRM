package cn.ibizlab.odoo.core.odoo_uom.valuerule.anno.uom_uom;

import cn.ibizlab.odoo.core.odoo_uom.valuerule.validator.uom_uom.Uom_uomIs_pos_groupableDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Uom_uom
 * 属性：Is_pos_groupable
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Uom_uomIs_pos_groupableDefaultValidator.class})
public @interface Uom_uomIs_pos_groupableDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

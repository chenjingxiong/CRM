package cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_bill_union;

import cn.ibizlab.odoo.core.odoo_purchase.valuerule.validator.purchase_bill_union.Purchase_bill_unionNameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Purchase_bill_union
 * 属性：Name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Purchase_bill_unionNameDefaultValidator.class})
public @interface Purchase_bill_unionNameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

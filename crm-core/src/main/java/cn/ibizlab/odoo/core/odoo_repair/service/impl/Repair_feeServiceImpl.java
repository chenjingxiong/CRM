package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_feeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_repair.client.repair_feeFeignClient;

/**
 * 实体[修理费] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_feeServiceImpl implements IRepair_feeService {

    @Autowired
    repair_feeFeignClient repair_feeFeignClient;


    @Override
    public boolean create(Repair_fee et) {
        Repair_fee rt = repair_feeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_fee> list){
        repair_feeFeignClient.createBatch(list) ;
    }

    @Override
    public Repair_fee getDraft(Repair_fee et) {
        et=repair_feeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=repair_feeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        repair_feeFeignClient.removeBatch(idList);
    }

    @Override
    public Repair_fee get(Integer id) {
		Repair_fee et=repair_feeFeignClient.get(id);
        if(et==null){
            et=new Repair_fee();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Repair_fee et) {
        Repair_fee rt = repair_feeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Repair_fee> list){
        repair_feeFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_fee> searchDefault(Repair_feeSearchContext context) {
        Page<Repair_fee> repair_fees=repair_feeFeignClient.searchDefault(context);
        return repair_fees;
    }


}



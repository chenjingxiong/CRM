package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_replenish] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-replenish", fallback = product_replenishFallback.class)
public interface product_replenishFeignClient {





    @RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/searchdefault")
    Page<Product_replenish> searchDefault(@RequestBody Product_replenishSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/{id}")
    Product_replenish update(@PathVariable("id") Integer id,@RequestBody Product_replenish product_replenish);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/batch")
    Boolean updateBatch(@RequestBody List<Product_replenish> product_replenishes);


    @RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/{id}")
    Product_replenish get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/product_replenishes")
    Product_replenish create(@RequestBody Product_replenish product_replenish);

    @RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/batch")
    Boolean createBatch(@RequestBody List<Product_replenish> product_replenishes);


    @RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/select")
    Page<Product_replenish> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/getdraft")
    Product_replenish getDraft();


}

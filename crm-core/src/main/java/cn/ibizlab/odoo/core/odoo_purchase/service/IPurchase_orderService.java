package cn.ibizlab.odoo.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;


/**
 * 实体[Purchase_order] 服务对象接口
 */
public interface IPurchase_orderService{

    Purchase_order get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Purchase_order et) ;
    void updateBatch(List<Purchase_order> list) ;
    Purchase_order getDraft(Purchase_order et) ;
    boolean create(Purchase_order et) ;
    void createBatch(List<Purchase_order> list) ;
    Page<Purchase_order> searchDefault(Purchase_orderSearchContext context) ;

}




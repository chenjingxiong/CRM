package cn.ibizlab.odoo.core.odoo_web_editor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_web_editor.client.web_editor_converter_testFeignClient;

/**
 * 实体[Web编辑器转换器测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_editor_converter_testServiceImpl implements IWeb_editor_converter_testService {

    @Autowired
    web_editor_converter_testFeignClient web_editor_converter_testFeignClient;


    @Override
    public boolean create(Web_editor_converter_test et) {
        Web_editor_converter_test rt = web_editor_converter_testFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_editor_converter_test> list){
        web_editor_converter_testFeignClient.createBatch(list) ;
    }

    @Override
    public Web_editor_converter_test get(Integer id) {
		Web_editor_converter_test et=web_editor_converter_testFeignClient.get(id);
        if(et==null){
            et=new Web_editor_converter_test();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=web_editor_converter_testFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        web_editor_converter_testFeignClient.removeBatch(idList);
    }

    @Override
    public Web_editor_converter_test getDraft(Web_editor_converter_test et) {
        et=web_editor_converter_testFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Web_editor_converter_test et) {
        Web_editor_converter_test rt = web_editor_converter_testFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Web_editor_converter_test> list){
        web_editor_converter_testFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context) {
        Page<Web_editor_converter_test> web_editor_converter_tests=web_editor_converter_testFeignClient.searchDefault(context);
        return web_editor_converter_tests;
    }


}



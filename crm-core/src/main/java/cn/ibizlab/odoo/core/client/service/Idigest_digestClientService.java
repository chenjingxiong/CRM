package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Idigest_digest;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[digest_digest] 服务对象接口
 */
public interface Idigest_digestClientService{

    public Idigest_digest createModel() ;

    public void updateBatch(List<Idigest_digest> digest_digests);

    public void createBatch(List<Idigest_digest> digest_digests);

    public void create(Idigest_digest digest_digest);

    public void get(Idigest_digest digest_digest);

    public void removeBatch(List<Idigest_digest> digest_digests);

    public void update(Idigest_digest digest_digest);

    public Page<Idigest_digest> fetchDefault(SearchContext context);

    public void remove(Idigest_digest digest_digest);

    public Page<Idigest_digest> select(SearchContext context);

    public void getDraft(Idigest_digest digest_digest);

}

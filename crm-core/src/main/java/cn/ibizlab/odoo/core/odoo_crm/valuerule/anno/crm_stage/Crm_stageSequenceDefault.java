package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_stage;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_stage.Crm_stageSequenceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_stage
 * 属性：Sequence
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_stageSequenceDefaultValidator.class})
public @interface Crm_stageSequenceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

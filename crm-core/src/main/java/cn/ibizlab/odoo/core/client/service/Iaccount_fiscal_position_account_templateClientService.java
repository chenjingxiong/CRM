package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position_account_template] 服务对象接口
 */
public interface Iaccount_fiscal_position_account_templateClientService{

    public Iaccount_fiscal_position_account_template createModel() ;

    public void createBatch(List<Iaccount_fiscal_position_account_template> account_fiscal_position_account_templates);

    public void create(Iaccount_fiscal_position_account_template account_fiscal_position_account_template);

    public void removeBatch(List<Iaccount_fiscal_position_account_template> account_fiscal_position_account_templates);

    public Page<Iaccount_fiscal_position_account_template> fetchDefault(SearchContext context);

    public void update(Iaccount_fiscal_position_account_template account_fiscal_position_account_template);

    public void updateBatch(List<Iaccount_fiscal_position_account_template> account_fiscal_position_account_templates);

    public void remove(Iaccount_fiscal_position_account_template account_fiscal_position_account_template);

    public void get(Iaccount_fiscal_position_account_template account_fiscal_position_account_template);

    public Page<Iaccount_fiscal_position_account_template> select(SearchContext context);

    public void getDraft(Iaccount_fiscal_position_account_template account_fiscal_position_account_template);

}

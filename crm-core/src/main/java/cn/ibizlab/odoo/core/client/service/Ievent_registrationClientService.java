package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_registration;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_registration] 服务对象接口
 */
public interface Ievent_registrationClientService{

    public Ievent_registration createModel() ;

    public void update(Ievent_registration event_registration);

    public void updateBatch(List<Ievent_registration> event_registrations);

    public void remove(Ievent_registration event_registration);

    public void get(Ievent_registration event_registration);

    public void create(Ievent_registration event_registration);

    public Page<Ievent_registration> fetchDefault(SearchContext context);

    public void createBatch(List<Ievent_registration> event_registrations);

    public void removeBatch(List<Ievent_registration> event_registrations);

    public Page<Ievent_registration> select(SearchContext context);

    public void getDraft(Ievent_registration event_registration);

}

package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[purchase_report] 服务对象接口
 */
@FeignClient(value = "odoo-purchase", contextId = "purchase-report", fallback = purchase_reportFallback.class)
public interface purchase_reportFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/{id}")
    Purchase_report update(@PathVariable("id") Integer id,@RequestBody Purchase_report purchase_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/batch")
    Boolean updateBatch(@RequestBody List<Purchase_report> purchase_reports);



    @RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/{id}")
    Purchase_report get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_reports")
    Purchase_report create(@RequestBody Purchase_report purchase_report);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/batch")
    Boolean createBatch(@RequestBody List<Purchase_report> purchase_reports);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/searchdefault")
    Page<Purchase_report> searchDefault(@RequestBody Purchase_reportSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/select")
    Page<Purchase_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/getdraft")
    Purchase_report getDraft();


}

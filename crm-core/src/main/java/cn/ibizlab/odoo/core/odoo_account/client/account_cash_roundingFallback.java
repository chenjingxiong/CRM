package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_cash_rounding] 服务对象接口
 */
@Component
public class account_cash_roundingFallback implements account_cash_roundingFeignClient{

    public Account_cash_rounding get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Account_cash_rounding create(Account_cash_rounding account_cash_rounding){
            return null;
     }
    public Boolean createBatch(List<Account_cash_rounding> account_cash_roundings){
            return false;
     }

    public Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context){
            return null;
     }


    public Account_cash_rounding update(Integer id, Account_cash_rounding account_cash_rounding){
            return null;
     }
    public Boolean updateBatch(List<Account_cash_rounding> account_cash_roundings){
            return false;
     }



    public Page<Account_cash_rounding> select(){
            return null;
     }

    public Account_cash_rounding getDraft(){
            return null;
    }



}

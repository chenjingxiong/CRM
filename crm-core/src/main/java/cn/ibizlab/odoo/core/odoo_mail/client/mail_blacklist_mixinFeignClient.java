package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-blacklist-mixin", fallback = mail_blacklist_mixinFallback.class)
public interface mail_blacklist_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins")
    Mail_blacklist_mixin create(@RequestBody Mail_blacklist_mixin mail_blacklist_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/batch")
    Boolean createBatch(@RequestBody List<Mail_blacklist_mixin> mail_blacklist_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklist_mixins/searchdefault")
    Page<Mail_blacklist_mixin> searchDefault(@RequestBody Mail_blacklist_mixinSearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklist_mixins/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklist_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklist_mixins/{id}")
    Mail_blacklist_mixin update(@PathVariable("id") Integer id,@RequestBody Mail_blacklist_mixin mail_blacklist_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklist_mixins/batch")
    Boolean updateBatch(@RequestBody List<Mail_blacklist_mixin> mail_blacklist_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklist_mixins/{id}")
    Mail_blacklist_mixin get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklist_mixins/select")
    Page<Mail_blacklist_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklist_mixins/getdraft")
    Mail_blacklist_mixin getDraft();


}

package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_country;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_country.Res_countryVat_labelDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_country
 * 属性：Vat_label
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_countryVat_labelDefaultValidator.class})
public @interface Res_countryVat_labelDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

package cn.ibizlab.odoo.core.odoo_asset.valuerule.anno.asset_state;

import cn.ibizlab.odoo.core.odoo_asset.valuerule.validator.asset_state.Asset_stateState_colorDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Asset_state
 * 属性：State_color
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Asset_stateState_colorDefaultValidator.class})
public @interface Asset_stateState_colorDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

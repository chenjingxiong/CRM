package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_accountFeignClient;

/**
 * 实体[分析账户] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_accountServiceImpl implements IAccount_analytic_accountService {

    @Autowired
    account_analytic_accountFeignClient account_analytic_accountFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_analytic_accountFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_analytic_accountFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_analytic_account et) {
        Account_analytic_account rt = account_analytic_accountFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_account> list){
        account_analytic_accountFeignClient.createBatch(list) ;
    }

    @Override
    public Account_analytic_account getDraft(Account_analytic_account et) {
        et=account_analytic_accountFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_analytic_account et) {
        Account_analytic_account rt = account_analytic_accountFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_analytic_account> list){
        account_analytic_accountFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_analytic_account get(Integer id) {
		Account_analytic_account et=account_analytic_accountFeignClient.get(id);
        if(et==null){
            et=new Account_analytic_account();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_account> searchDefault(Account_analytic_accountSearchContext context) {
        Page<Account_analytic_account> account_analytic_accounts=account_analytic_accountFeignClient.searchDefault(context);
        return account_analytic_accounts;
    }


}



package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
@Component
public class account_fiscal_position_accountFallback implements account_fiscal_position_accountFeignClient{


    public Page<Account_fiscal_position_account> searchDefault(Account_fiscal_position_accountSearchContext context){
            return null;
     }


    public Account_fiscal_position_account create(Account_fiscal_position_account account_fiscal_position_account){
            return null;
     }
    public Boolean createBatch(List<Account_fiscal_position_account> account_fiscal_position_accounts){
            return false;
     }


    public Account_fiscal_position_account update(Integer id, Account_fiscal_position_account account_fiscal_position_account){
            return null;
     }
    public Boolean updateBatch(List<Account_fiscal_position_account> account_fiscal_position_accounts){
            return false;
     }


    public Account_fiscal_position_account get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Account_fiscal_position_account> select(){
            return null;
     }

    public Account_fiscal_position_account getDraft(){
            return null;
    }



}

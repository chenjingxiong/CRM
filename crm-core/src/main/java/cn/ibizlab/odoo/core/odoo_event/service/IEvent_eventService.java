package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;


/**
 * 实体[Event_event] 服务对象接口
 */
public interface IEvent_eventService{

    Event_event get(Integer key) ;
    boolean create(Event_event et) ;
    void createBatch(List<Event_event> list) ;
    boolean update(Event_event et) ;
    void updateBatch(List<Event_event> list) ;
    Event_event getDraft(Event_event et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Event_event> searchDefault(Event_eventSearchContext context) ;

}




package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_employee_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_employee_category] 服务对象接口
 */
public interface Ihr_employee_categoryClientService{

    public Ihr_employee_category createModel() ;

    public Page<Ihr_employee_category> fetchDefault(SearchContext context);

    public void updateBatch(List<Ihr_employee_category> hr_employee_categories);

    public void get(Ihr_employee_category hr_employee_category);

    public void createBatch(List<Ihr_employee_category> hr_employee_categories);

    public void update(Ihr_employee_category hr_employee_category);

    public void removeBatch(List<Ihr_employee_category> hr_employee_categories);

    public void create(Ihr_employee_category hr_employee_category);

    public void remove(Ihr_employee_category hr_employee_category);

    public Page<Ihr_employee_category> select(SearchContext context);

    public void getDraft(Ihr_employee_category hr_employee_category);

}

package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoiceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoiceFeignClient;

/**
 * 实体[发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoiceServiceImpl implements IAccount_invoiceService {

    @Autowired
    account_invoiceFeignClient account_invoiceFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoiceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoiceFeignClient.removeBatch(idList);
    }

    @Override
    public boolean checkKey(Account_invoice et) {
        return account_invoiceFeignClient.checkKey(et);
    }
    @Override
    public boolean create(Account_invoice et) {
        Account_invoice rt = account_invoiceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice> list){
        account_invoiceFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_invoice et) {
        Account_invoice rt = account_invoiceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice> list){
        account_invoiceFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_invoice get(Integer id) {
		Account_invoice et=account_invoiceFeignClient.get(id);
        if(et==null){
            et=new Account_invoice();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_invoice getDraft(Account_invoice et) {
        et=account_invoiceFeignClient.getDraft();
        return et;
    }

    @Override
    @Transactional
    public boolean save(Account_invoice et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!account_invoiceFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Account_invoice> list) {
        account_invoiceFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice> searchDefault(Account_invoiceSearchContext context) {
        Page<Account_invoice> account_invoices=account_invoiceFeignClient.searchDefault(context);
        return account_invoices;
    }


}



package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;


/**
 * 实体[Stock_scrap] 服务对象接口
 */
public interface IStock_scrapService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_scrap et) ;
    void createBatch(List<Stock_scrap> list) ;
    Stock_scrap getDraft(Stock_scrap et) ;
    boolean update(Stock_scrap et) ;
    void updateBatch(List<Stock_scrap> list) ;
    Stock_scrap get(Integer key) ;
    Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;


/**
 * 实体[Stock_warn_insufficient_qty_scrap] 服务对象接口
 */
public interface IStock_warn_insufficient_qty_scrapService{

    Stock_warn_insufficient_qty_scrap getDraft(Stock_warn_insufficient_qty_scrap et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_warn_insufficient_qty_scrap et) ;
    void updateBatch(List<Stock_warn_insufficient_qty_scrap> list) ;
    boolean create(Stock_warn_insufficient_qty_scrap et) ;
    void createBatch(List<Stock_warn_insufficient_qty_scrap> list) ;
    Stock_warn_insufficient_qty_scrap get(Integer key) ;
    Page<Stock_warn_insufficient_qty_scrap> searchDefault(Stock_warn_insufficient_qty_scrapSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_labelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_labelFeignClient;

/**
 * 实体[调查标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_labelServiceImpl implements ISurvey_labelService {

    @Autowired
    survey_labelFeignClient survey_labelFeignClient;


    @Override
    public boolean create(Survey_label et) {
        Survey_label rt = survey_labelFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_label> list){
        survey_labelFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Survey_label et) {
        Survey_label rt = survey_labelFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_label> list){
        survey_labelFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_label get(Integer id) {
		Survey_label et=survey_labelFeignClient.get(id);
        if(et==null){
            et=new Survey_label();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_labelFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_labelFeignClient.removeBatch(idList);
    }

    @Override
    public Survey_label getDraft(Survey_label et) {
        et=survey_labelFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_label> searchDefault(Survey_labelSearchContext context) {
        Page<Survey_label> survey_labels=survey_labelFeignClient.searchDefault(context);
        return survey_labels;
    }


}



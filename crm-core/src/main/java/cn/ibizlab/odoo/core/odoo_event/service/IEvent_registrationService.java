package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;


/**
 * 实体[Event_registration] 服务对象接口
 */
public interface IEvent_registrationService{

    boolean create(Event_registration et) ;
    void createBatch(List<Event_registration> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Event_registration et) ;
    void updateBatch(List<Event_registration> list) ;
    Event_registration get(Integer key) ;
    Event_registration getDraft(Event_registration et) ;
    Page<Event_registration> searchDefault(Event_registrationSearchContext context) ;

}




package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;


/**
 * 实体[Event_confirm] 服务对象接口
 */
public interface IEvent_confirmService{

    Event_confirm getDraft(Event_confirm et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Event_confirm et) ;
    void updateBatch(List<Event_confirm> list) ;
    Event_confirm get(Integer key) ;
    boolean create(Event_confirm et) ;
    void createBatch(List<Event_confirm> list) ;
    Page<Event_confirm> searchDefault(Event_confirmSearchContext context) ;

}




package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [im_livechat_channel_rule] 对象
 */
public interface Iim_livechat_channel_rule {

    /**
     * 获取 [动作]
     */
    public void setAction(String action);
    
    /**
     * 设置 [动作]
     */
    public String getAction();

    /**
     * 获取 [动作]脏标记
     */
    public boolean getActionDirtyFlag();
    /**
     * 获取 [自动弹出时间]
     */
    public void setAuto_popup_timer(Integer auto_popup_timer);
    
    /**
     * 设置 [自动弹出时间]
     */
    public Integer getAuto_popup_timer();

    /**
     * 获取 [自动弹出时间]脏标记
     */
    public boolean getAuto_popup_timerDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [渠道]
     */
    public Integer getChannel_id();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setChannel_id_text(String channel_id_text);
    
    /**
     * 设置 [渠道]
     */
    public String getChannel_id_text();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getChannel_id_textDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_ids(String country_ids);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_ids();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [URL的正则表达式]
     */
    public void setRegex_url(String regex_url);
    
    /**
     * 设置 [URL的正则表达式]
     */
    public String getRegex_url();

    /**
     * 获取 [URL的正则表达式]脏标记
     */
    public boolean getRegex_urlDirtyFlag();
    /**
     * 获取 [匹配的订单]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [匹配的订单]
     */
    public Integer getSequence();

    /**
     * 获取 [匹配的订单]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

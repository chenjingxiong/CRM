package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [活动] 对象
 */
@Data
public class Mail_activity extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 邮件模板
     */
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    private String mailTemplateIds;

    /**
     * 自动活动
     */
    @JSONField(name = "automated")
    @JsonProperty("automated")
    private String automated;

    /**
     * 文档名称
     */
    @DEField(name = "res_name")
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 到期时间
     */
    @DEField(name = "date_deadline")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 摘要
     */
    @JSONField(name = "summary")
    @JsonProperty("summary")
    private String summary;

    /**
     * 下一活动可用
     */
    @JSONField(name = "has_recommended_activities")
    @JsonProperty("has_recommended_activities")
    private String hasRecommendedActivities;

    /**
     * 相关文档编号
     */
    @DEField(name = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 备注
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 反馈
     */
    @JSONField(name = "feedback")
    @JsonProperty("feedback")
    private String feedback;

    /**
     * 文档模型
     */
    @DEField(name = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 相关的文档模型
     */
    @DEField(name = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 分派给
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 类别
     */
    @JSONField(name = "activity_category")
    @JsonProperty("activity_category")
    private String activityCategory;

    /**
     * 活动
     */
    @JSONField(name = "activity_type_id_text")
    @JsonProperty("activity_type_id_text")
    private String activityTypeIdText;

    /**
     * 前一活动类型
     */
    @JSONField(name = "previous_activity_type_id_text")
    @JsonProperty("previous_activity_type_id_text")
    private String previousActivityTypeIdText;

    /**
     * 相关便签
     */
    @JSONField(name = "note_id_text")
    @JsonProperty("note_id_text")
    private String noteIdText;

    /**
     * 建立者
     */
    @JSONField(name = "create_user_id_text")
    @JsonProperty("create_user_id_text")
    private String createUserIdText;

    /**
     * 图标
     */
    @JSONField(name = "icon")
    @JsonProperty("icon")
    private String icon;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 排版类型
     */
    @JSONField(name = "activity_decoration")
    @JsonProperty("activity_decoration")
    private String activityDecoration;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 自动安排下一个活动
     */
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private String forceNext;

    /**
     * 推荐的活动类型
     */
    @JSONField(name = "recommended_activity_type_id_text")
    @JsonProperty("recommended_activity_type_id_text")
    private String recommendedActivityTypeIdText;

    /**
     * 日历会议
     */
    @JSONField(name = "calendar_event_id_text")
    @JsonProperty("calendar_event_id_text")
    private String calendarEventIdText;

    /**
     * 推荐的活动类型
     */
    @DEField(name = "recommended_activity_type_id")
    @JSONField(name = "recommended_activity_type_id")
    @JsonProperty("recommended_activity_type_id")
    private Integer recommendedActivityTypeId;

    /**
     * 活动
     */
    @DEField(name = "activity_type_id")
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 建立者
     */
    @DEField(name = "create_user_id")
    @JSONField(name = "create_user_id")
    @JsonProperty("create_user_id")
    private Integer createUserId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 分派给
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 相关便签
     */
    @DEField(name = "note_id")
    @JSONField(name = "note_id")
    @JsonProperty("note_id")
    private Integer noteId;

    /**
     * 前一活动类型
     */
    @DEField(name = "previous_activity_type_id")
    @JSONField(name = "previous_activity_type_id")
    @JsonProperty("previous_activity_type_id")
    private Integer previousActivityTypeId;

    /**
     * 日历会议
     */
    @DEField(name = "calendar_event_id")
    @JSONField(name = "calendar_event_id")
    @JsonProperty("calendar_event_id")
    private Integer calendarEventId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocalendarevent")
    @JsonProperty("odoocalendarevent")
    private cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event odooCalendarEvent;

    /**
     * 
     */
    @JSONField(name = "odooactivitytype")
    @JsonProperty("odooactivitytype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooActivityType;

    /**
     * 
     */
    @JSONField(name = "odoopreviousactivitytype")
    @JsonProperty("odoopreviousactivitytype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooPreviousActivityType;

    /**
     * 
     */
    @JSONField(name = "odoorecommendedactivitytype")
    @JsonProperty("odoorecommendedactivitytype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooRecommendedActivityType;

    /**
     * 
     */
    @JSONField(name = "odoonote")
    @JsonProperty("odoonote")
    private cn.ibizlab.odoo.core.odoo_note.domain.Note_note odooNote;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoocreateuser")
    @JsonProperty("odoocreateuser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreateUser;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [自动活动]
     */
    public void setAutomated(String automated){
        this.automated = automated ;
        this.modify("automated",automated);
    }
    /**
     * 设置 [文档名称]
     */
    public void setResName(String resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }
    /**
     * 设置 [到期时间]
     */
    public void setDateDeadline(Timestamp dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }
    /**
     * 设置 [摘要]
     */
    public void setSummary(String summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }
    /**
     * 设置 [相关文档编号]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }
    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [反馈]
     */
    public void setFeedback(String feedback){
        this.feedback = feedback ;
        this.modify("feedback",feedback);
    }
    /**
     * 设置 [文档模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }
    /**
     * 设置 [相关的文档模型]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }
    /**
     * 设置 [推荐的活动类型]
     */
    public void setRecommendedActivityTypeId(Integer recommendedActivityTypeId){
        this.recommendedActivityTypeId = recommendedActivityTypeId ;
        this.modify("recommended_activity_type_id",recommendedActivityTypeId);
    }
    /**
     * 设置 [活动]
     */
    public void setActivityTypeId(Integer activityTypeId){
        this.activityTypeId = activityTypeId ;
        this.modify("activity_type_id",activityTypeId);
    }
    /**
     * 设置 [建立者]
     */
    public void setCreateUserId(Integer createUserId){
        this.createUserId = createUserId ;
        this.modify("create_user_id",createUserId);
    }
    /**
     * 设置 [分派给]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [相关便签]
     */
    public void setNoteId(Integer noteId){
        this.noteId = noteId ;
        this.modify("note_id",noteId);
    }
    /**
     * 设置 [前一活动类型]
     */
    public void setPreviousActivityTypeId(Integer previousActivityTypeId){
        this.previousActivityTypeId = previousActivityTypeId ;
        this.modify("previous_activity_type_id",previousActivityTypeId);
    }
    /**
     * 设置 [日历会议]
     */
    public void setCalendarEventId(Integer calendarEventId){
        this.calendarEventId = calendarEventId ;
        this.modify("calendar_event_id",calendarEventId);
    }

}



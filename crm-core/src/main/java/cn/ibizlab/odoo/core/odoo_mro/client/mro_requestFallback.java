package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_request] 服务对象接口
 */
@Component
public class mro_requestFallback implements mro_requestFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mro_request create(Mro_request mro_request){
            return null;
     }
    public Boolean createBatch(List<Mro_request> mro_requests){
            return false;
     }


    public Mro_request update(Integer id, Mro_request mro_request){
            return null;
     }
    public Boolean updateBatch(List<Mro_request> mro_requests){
            return false;
     }




    public Page<Mro_request> searchDefault(Mro_requestSearchContext context){
            return null;
     }


    public Mro_request get(Integer id){
            return null;
     }


    public Page<Mro_request> select(){
            return null;
     }

    public Mro_request getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_chart_template] 对象
 */
public interface Iaccount_chart_template {

    /**
     * 获取 [关联的科目模板]
     */
    public void setAccount_ids(String account_ids);
    
    /**
     * 设置 [关联的科目模板]
     */
    public String getAccount_ids();

    /**
     * 获取 [关联的科目模板]脏标记
     */
    public boolean getAccount_idsDirtyFlag();
    /**
     * 获取 [银行科目的前缀]
     */
    public void setBank_account_code_prefix(String bank_account_code_prefix);
    
    /**
     * 设置 [银行科目的前缀]
     */
    public String getBank_account_code_prefix();

    /**
     * 获取 [银行科目的前缀]脏标记
     */
    public boolean getBank_account_code_prefixDirtyFlag();
    /**
     * 获取 [主现金科目的前缀]
     */
    public void setCash_account_code_prefix(String cash_account_code_prefix);
    
    /**
     * 设置 [主现金科目的前缀]
     */
    public String getCash_account_code_prefix();

    /**
     * 获取 [主现金科目的前缀]脏标记
     */
    public boolean getCash_account_code_prefixDirtyFlag();
    /**
     * 获取 [# 数字]
     */
    public void setCode_digits(Integer code_digits);
    
    /**
     * 设置 [# 数字]
     */
    public Integer getCode_digits();

    /**
     * 获取 [# 数字]脏标记
     */
    public boolean getCode_digitsDirtyFlag();
    /**
     * 获取 [税率完整集合]
     */
    public void setComplete_tax_set(String complete_tax_set);
    
    /**
     * 设置 [税率完整集合]
     */
    public String getComplete_tax_set();

    /**
     * 获取 [税率完整集合]脏标记
     */
    public boolean getComplete_tax_setDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [汇率损失科目]
     */
    public void setExpense_currency_exchange_account_id(Integer expense_currency_exchange_account_id);
    
    /**
     * 设置 [汇率损失科目]
     */
    public Integer getExpense_currency_exchange_account_id();

    /**
     * 获取 [汇率损失科目]脏标记
     */
    public boolean getExpense_currency_exchange_account_idDirtyFlag();
    /**
     * 获取 [汇率损失科目]
     */
    public void setExpense_currency_exchange_account_id_text(String expense_currency_exchange_account_id_text);
    
    /**
     * 设置 [汇率损失科目]
     */
    public String getExpense_currency_exchange_account_id_text();

    /**
     * 获取 [汇率损失科目]脏标记
     */
    public boolean getExpense_currency_exchange_account_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [汇率增益科目]
     */
    public void setIncome_currency_exchange_account_id(Integer income_currency_exchange_account_id);
    
    /**
     * 设置 [汇率增益科目]
     */
    public Integer getIncome_currency_exchange_account_id();

    /**
     * 获取 [汇率增益科目]脏标记
     */
    public boolean getIncome_currency_exchange_account_idDirtyFlag();
    /**
     * 获取 [汇率增益科目]
     */
    public void setIncome_currency_exchange_account_id_text(String income_currency_exchange_account_id_text);
    
    /**
     * 设置 [汇率增益科目]
     */
    public String getIncome_currency_exchange_account_id_text();

    /**
     * 获取 [汇率增益科目]脏标记
     */
    public boolean getIncome_currency_exchange_account_id_textDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [上级表模板]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级表模板]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级表模板]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级表模板]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级表模板]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级表模板]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [费用科目的类别]
     */
    public void setProperty_account_expense_categ_id(Integer property_account_expense_categ_id);
    
    /**
     * 设置 [费用科目的类别]
     */
    public Integer getProperty_account_expense_categ_id();

    /**
     * 获取 [费用科目的类别]脏标记
     */
    public boolean getProperty_account_expense_categ_idDirtyFlag();
    /**
     * 获取 [费用科目的类别]
     */
    public void setProperty_account_expense_categ_id_text(String property_account_expense_categ_id_text);
    
    /**
     * 设置 [费用科目的类别]
     */
    public String getProperty_account_expense_categ_id_text();

    /**
     * 获取 [费用科目的类别]脏标记
     */
    public boolean getProperty_account_expense_categ_id_textDirtyFlag();
    /**
     * 获取 [产品模板的费用科目]
     */
    public void setProperty_account_expense_id(Integer property_account_expense_id);
    
    /**
     * 设置 [产品模板的费用科目]
     */
    public Integer getProperty_account_expense_id();

    /**
     * 获取 [产品模板的费用科目]脏标记
     */
    public boolean getProperty_account_expense_idDirtyFlag();
    /**
     * 获取 [产品模板的费用科目]
     */
    public void setProperty_account_expense_id_text(String property_account_expense_id_text);
    
    /**
     * 设置 [产品模板的费用科目]
     */
    public String getProperty_account_expense_id_text();

    /**
     * 获取 [产品模板的费用科目]脏标记
     */
    public boolean getProperty_account_expense_id_textDirtyFlag();
    /**
     * 获取 [收入科目的类别]
     */
    public void setProperty_account_income_categ_id(Integer property_account_income_categ_id);
    
    /**
     * 设置 [收入科目的类别]
     */
    public Integer getProperty_account_income_categ_id();

    /**
     * 获取 [收入科目的类别]脏标记
     */
    public boolean getProperty_account_income_categ_idDirtyFlag();
    /**
     * 获取 [收入科目的类别]
     */
    public void setProperty_account_income_categ_id_text(String property_account_income_categ_id_text);
    
    /**
     * 设置 [收入科目的类别]
     */
    public String getProperty_account_income_categ_id_text();

    /**
     * 获取 [收入科目的类别]脏标记
     */
    public boolean getProperty_account_income_categ_id_textDirtyFlag();
    /**
     * 获取 [产品模板的收入科目]
     */
    public void setProperty_account_income_id(Integer property_account_income_id);
    
    /**
     * 设置 [产品模板的收入科目]
     */
    public Integer getProperty_account_income_id();

    /**
     * 获取 [产品模板的收入科目]脏标记
     */
    public boolean getProperty_account_income_idDirtyFlag();
    /**
     * 获取 [产品模板的收入科目]
     */
    public void setProperty_account_income_id_text(String property_account_income_id_text);
    
    /**
     * 设置 [产品模板的收入科目]
     */
    public String getProperty_account_income_id_text();

    /**
     * 获取 [产品模板的收入科目]脏标记
     */
    public boolean getProperty_account_income_id_textDirtyFlag();
    /**
     * 获取 [应付科目]
     */
    public void setProperty_account_payable_id(Integer property_account_payable_id);
    
    /**
     * 设置 [应付科目]
     */
    public Integer getProperty_account_payable_id();

    /**
     * 获取 [应付科目]脏标记
     */
    public boolean getProperty_account_payable_idDirtyFlag();
    /**
     * 获取 [应付科目]
     */
    public void setProperty_account_payable_id_text(String property_account_payable_id_text);
    
    /**
     * 设置 [应付科目]
     */
    public String getProperty_account_payable_id_text();

    /**
     * 获取 [应付科目]脏标记
     */
    public boolean getProperty_account_payable_id_textDirtyFlag();
    /**
     * 获取 [应收科目]
     */
    public void setProperty_account_receivable_id(Integer property_account_receivable_id);
    
    /**
     * 设置 [应收科目]
     */
    public Integer getProperty_account_receivable_id();

    /**
     * 获取 [应收科目]脏标记
     */
    public boolean getProperty_account_receivable_idDirtyFlag();
    /**
     * 获取 [应收科目]
     */
    public void setProperty_account_receivable_id_text(String property_account_receivable_id_text);
    
    /**
     * 设置 [应收科目]
     */
    public String getProperty_account_receivable_id_text();

    /**
     * 获取 [应收科目]脏标记
     */
    public boolean getProperty_account_receivable_id_textDirtyFlag();
    /**
     * 获取 [库存计价的入库科目]
     */
    public void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);
    
    /**
     * 设置 [库存计价的入库科目]
     */
    public Integer getProperty_stock_account_input_categ_id();

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    public boolean getProperty_stock_account_input_categ_idDirtyFlag();
    /**
     * 获取 [库存计价的入库科目]
     */
    public void setProperty_stock_account_input_categ_id_text(String property_stock_account_input_categ_id_text);
    
    /**
     * 设置 [库存计价的入库科目]
     */
    public String getProperty_stock_account_input_categ_id_text();

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag();
    /**
     * 获取 [库存计价的出货科目]
     */
    public void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);
    
    /**
     * 设置 [库存计价的出货科目]
     */
    public Integer getProperty_stock_account_output_categ_id();

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    public boolean getProperty_stock_account_output_categ_idDirtyFlag();
    /**
     * 获取 [库存计价的出货科目]
     */
    public void setProperty_stock_account_output_categ_id_text(String property_stock_account_output_categ_id_text);
    
    /**
     * 设置 [库存计价的出货科目]
     */
    public String getProperty_stock_account_output_categ_id_text();

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag();
    /**
     * 获取 [库存计价的科目模板]
     */
    public void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);
    
    /**
     * 设置 [库存计价的科目模板]
     */
    public Integer getProperty_stock_valuation_account_id();

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    public boolean getProperty_stock_valuation_account_idDirtyFlag();
    /**
     * 获取 [库存计价的科目模板]
     */
    public void setProperty_stock_valuation_account_id_text(String property_stock_valuation_account_id_text);
    
    /**
     * 设置 [库存计价的科目模板]
     */
    public String getProperty_stock_valuation_account_id_text();

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag();
    /**
     * 获取 [口语]
     */
    public void setSpoken_languages(String spoken_languages);
    
    /**
     * 设置 [口语]
     */
    public String getSpoken_languages();

    /**
     * 获取 [口语]脏标记
     */
    public boolean getSpoken_languagesDirtyFlag();
    /**
     * 获取 [税模板列表]
     */
    public void setTax_template_ids(String tax_template_ids);
    
    /**
     * 设置 [税模板列表]
     */
    public String getTax_template_ids();

    /**
     * 获取 [税模板列表]脏标记
     */
    public boolean getTax_template_idsDirtyFlag();
    /**
     * 获取 [主转账帐户的前缀]
     */
    public void setTransfer_account_code_prefix(String transfer_account_code_prefix);
    
    /**
     * 设置 [主转账帐户的前缀]
     */
    public String getTransfer_account_code_prefix();

    /**
     * 获取 [主转账帐户的前缀]脏标记
     */
    public boolean getTransfer_account_code_prefixDirtyFlag();
    /**
     * 获取 [使用anglo-saxon会计]
     */
    public void setUse_anglo_saxon(String use_anglo_saxon);
    
    /**
     * 设置 [使用anglo-saxon会计]
     */
    public String getUse_anglo_saxon();

    /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    public boolean getUse_anglo_saxonDirtyFlag();
    /**
     * 获取 [可显示？]
     */
    public void setVisible(String visible);
    
    /**
     * 设置 [可显示？]
     */
    public String getVisible();

    /**
     * 获取 [可显示？]脏标记
     */
    public boolean getVisibleDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

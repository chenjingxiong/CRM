package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_team;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_team.Crm_teamUse_quotationsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_team
 * 属性：Use_quotations
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_teamUse_quotationsDefaultValidator.class})
public @interface Crm_teamUse_quotationsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

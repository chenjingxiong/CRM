package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
@Component
public class mrp_workcenterFallback implements mrp_workcenterFeignClient{


    public Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context){
            return null;
     }



    public Mrp_workcenter create(Mrp_workcenter mrp_workcenter){
            return null;
     }
    public Boolean createBatch(List<Mrp_workcenter> mrp_workcenters){
            return false;
     }


    public Mrp_workcenter get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_workcenter update(Integer id, Mrp_workcenter mrp_workcenter){
            return null;
     }
    public Boolean updateBatch(List<Mrp_workcenter> mrp_workcenters){
            return false;
     }


    public Page<Mrp_workcenter> select(){
            return null;
     }

    public Mrp_workcenter getDraft(){
            return null;
    }



}

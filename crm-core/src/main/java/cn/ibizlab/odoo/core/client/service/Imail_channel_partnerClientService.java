package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_channel_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_channel_partner] 服务对象接口
 */
public interface Imail_channel_partnerClientService{

    public Imail_channel_partner createModel() ;

    public void updateBatch(List<Imail_channel_partner> mail_channel_partners);

    public void removeBatch(List<Imail_channel_partner> mail_channel_partners);

    public void createBatch(List<Imail_channel_partner> mail_channel_partners);

    public void create(Imail_channel_partner mail_channel_partner);

    public void update(Imail_channel_partner mail_channel_partner);

    public Page<Imail_channel_partner> fetchDefault(SearchContext context);

    public void remove(Imail_channel_partner mail_channel_partner);

    public void get(Imail_channel_partner mail_channel_partner);

    public Page<Imail_channel_partner> select(SearchContext context);

    public void getDraft(Imail_channel_partner mail_channel_partner);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_refund;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_refund] 服务对象接口
 */
public interface Iaccount_invoice_refundClientService{

    public Iaccount_invoice_refund createModel() ;

    public void updateBatch(List<Iaccount_invoice_refund> account_invoice_refunds);

    public void get(Iaccount_invoice_refund account_invoice_refund);

    public void removeBatch(List<Iaccount_invoice_refund> account_invoice_refunds);

    public void remove(Iaccount_invoice_refund account_invoice_refund);

    public Page<Iaccount_invoice_refund> fetchDefault(SearchContext context);

    public void update(Iaccount_invoice_refund account_invoice_refund);

    public void create(Iaccount_invoice_refund account_invoice_refund);

    public void createBatch(List<Iaccount_invoice_refund> account_invoice_refunds);

    public Page<Iaccount_invoice_refund> select(SearchContext context);

    public void getDraft(Iaccount_invoice_refund account_invoice_refund);

}

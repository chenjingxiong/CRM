package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contract_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_contract_typeFeignClient;

/**
 * 实体[合同类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_contract_typeServiceImpl implements IHr_contract_typeService {

    @Autowired
    hr_contract_typeFeignClient hr_contract_typeFeignClient;


    @Override
    public Hr_contract_type getDraft(Hr_contract_type et) {
        et=hr_contract_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Hr_contract_type et) {
        Hr_contract_type rt = hr_contract_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_contract_type> list){
        hr_contract_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_contract_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_contract_typeFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_contract_type get(Integer id) {
		Hr_contract_type et=hr_contract_typeFeignClient.get(id);
        if(et==null){
            et=new Hr_contract_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Hr_contract_type et) {
        Hr_contract_type rt = hr_contract_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_contract_type> list){
        hr_contract_typeFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context) {
        Page<Hr_contract_type> hr_contract_types=hr_contract_typeFeignClient.searchDefault(context);
        return hr_contract_types;
    }


}



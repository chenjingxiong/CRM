package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;


/**
 * 实体[Crm_team] 服务对象接口
 */
public interface ICrm_teamService{

    Crm_team get(Integer key) ;
    boolean checkKey(Crm_team et) ;
    Crm_team getDraft(Crm_team et) ;
    boolean update(Crm_team et) ;
    void updateBatch(List<Crm_team> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Crm_team et) ;
    void createBatch(List<Crm_team> list) ;
    boolean save(Crm_team et) ;
    void saveBatch(List<Crm_team> list) ;
    Page<Crm_team> searchDefault(Crm_teamSearchContext context) ;

}




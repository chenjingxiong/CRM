package cn.ibizlab.odoo.core.odoo_digest.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[digest_digest] 服务对象接口
 */
@Component
public class digest_digestFallback implements digest_digestFeignClient{



    public Digest_digest create(Digest_digest digest_digest){
            return null;
     }
    public Boolean createBatch(List<Digest_digest> digest_digests){
            return false;
     }

    public Digest_digest get(Integer id){
            return null;
     }



    public Digest_digest update(Integer id, Digest_digest digest_digest){
            return null;
     }
    public Boolean updateBatch(List<Digest_digest> digest_digests){
            return false;
     }


    public Page<Digest_digest> searchDefault(Digest_digestSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Digest_digest> select(){
            return null;
     }

    public Digest_digest getDraft(){
            return null;
    }



}

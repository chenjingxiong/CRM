package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_odometerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_odometerFeignClient;

/**
 * 实体[车辆的里程表记录] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_odometerServiceImpl implements IFleet_vehicle_odometerService {

    @Autowired
    fleet_vehicle_odometerFeignClient fleet_vehicle_odometerFeignClient;


    @Override
    public Fleet_vehicle_odometer getDraft(Fleet_vehicle_odometer et) {
        et=fleet_vehicle_odometerFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Fleet_vehicle_odometer et) {
        Fleet_vehicle_odometer rt = fleet_vehicle_odometerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_odometer> list){
        fleet_vehicle_odometerFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Fleet_vehicle_odometer et) {
        Fleet_vehicle_odometer rt = fleet_vehicle_odometerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_odometer> list){
        fleet_vehicle_odometerFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_odometerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_odometerFeignClient.removeBatch(idList);
    }

    @Override
    public Fleet_vehicle_odometer get(Integer id) {
		Fleet_vehicle_odometer et=fleet_vehicle_odometerFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_odometer();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_odometer> searchDefault(Fleet_vehicle_odometerSearchContext context) {
        Page<Fleet_vehicle_odometer> fleet_vehicle_odometers=fleet_vehicle_odometerFeignClient.searchDefault(context);
        return fleet_vehicle_odometers;
    }


}



package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface Iwebsite_sale_payment_acquirer_onboarding_wizardClientService{

    public Iwebsite_sale_payment_acquirer_onboarding_wizard createModel() ;

    public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> fetchDefault(SearchContext context);

    public void create(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    public void updateBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);

    public void removeBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);

    public void get(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    public void update(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    public void remove(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    public void createBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);

    public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> select(SearchContext context);

    public void getDraft(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

}

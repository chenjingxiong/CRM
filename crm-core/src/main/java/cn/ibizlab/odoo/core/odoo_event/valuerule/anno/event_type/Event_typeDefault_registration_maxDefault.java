package cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_type;

import cn.ibizlab.odoo.core.odoo_event.valuerule.validator.event_type.Event_typeDefault_registration_maxDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Event_type
 * 属性：Default_registration_max
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Event_typeDefault_registration_maxDefaultValidator.class})
public @interface Event_typeDefault_registration_maxDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

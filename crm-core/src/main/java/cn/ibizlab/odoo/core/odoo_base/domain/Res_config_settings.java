package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [配置设定] 对象
 */
@Data
public class Res_config_settings extends EntityClient implements Serializable {

    /**
     * 导入.qif 文件
     */
    @DEField(name = "module_account_bank_statement_import_qif")
    @JSONField(name = "module_account_bank_statement_import_qif")
    @JsonProperty("module_account_bank_statement_import_qif")
    private String moduleAccountBankStatementImportQif;

    /**
     * 导入.ofx格式
     */
    @DEField(name = "module_account_bank_statement_import_ofx")
    @JSONField(name = "module_account_bank_statement_import_ofx")
    @JsonProperty("module_account_bank_statement_import_ofx")
    private String moduleAccountBankStatementImportOfx;

    /**
     * Google 地图
     */
    @JSONField(name = "has_google_maps")
    @JsonProperty("has_google_maps")
    private String hasGoogleMaps;

    /**
     * 欧盟数字商品增值税
     */
    @DEField(name = "module_l10n_eu_service")
    @JSONField(name = "module_l10n_eu_service")
    @JsonProperty("module_l10n_eu_service")
    private String moduleL10nEuService;

    /**
     * 内容发布网络 (CDN)
     */
    @JSONField(name = "cdn_activated")
    @JsonProperty("cdn_activated")
    private String cdnActivated;

    /**
     * 默认语言代码
     */
    @JSONField(name = "website_default_lang_code")
    @JsonProperty("website_default_lang_code")
    private String websiteDefaultLangCode;

    /**
     * 允许在登录页开启密码重置功能
     */
    @DEField(name = "auth_signup_reset_password")
    @JSONField(name = "auth_signup_reset_password")
    @JsonProperty("auth_signup_reset_password")
    private String authSignupResetPassword;

    /**
     * 国家/地区分组
     */
    @JSONField(name = "website_country_group_ids")
    @JsonProperty("website_country_group_ids")
    private String websiteCountryGroupIds;

    /**
     * Instagram 账号
     */
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;

    /**
     * 采购招标
     */
    @DEField(name = "module_purchase_requisition")
    @JSONField(name = "module_purchase_requisition")
    @JsonProperty("module_purchase_requisition")
    private String modulePurchaseRequisition;

    /**
     * Easypost 接口
     */
    @DEField(name = "module_delivery_easypost")
    @JSONField(name = "module_delivery_easypost")
    @JsonProperty("module_delivery_easypost")
    private String moduleDeliveryEasypost;

    /**
     * 折扣
     */
    @DEField(name = "group_discount_per_so_line")
    @JSONField(name = "group_discount_per_so_line")
    @JsonProperty("group_discount_per_so_line")
    private String groupDiscountPerSoLine;

    /**
     * 自动开票
     */
    @DEField(name = "automatic_invoice")
    @JSONField(name = "automatic_invoice")
    @JsonProperty("automatic_invoice")
    private String automaticInvoice;

    /**
     * Plaid 接口
     */
    @DEField(name = "module_account_plaid")
    @JSONField(name = "module_account_plaid")
    @JsonProperty("module_account_plaid")
    private String moduleAccountPlaid;

    /**
     * Google 客户 ID
     */
    @JSONField(name = "google_management_client_id")
    @JsonProperty("google_management_client_id")
    private String googleManagementClientId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 允许支票打印和存款
     */
    @DEField(name = "module_account_check_printing")
    @JSONField(name = "module_account_check_printing")
    @JsonProperty("module_account_check_printing")
    private String moduleAccountCheckPrinting;

    /**
     * 自动填充公司数据
     */
    @DEField(name = "module_partner_autocomplete")
    @JSONField(name = "module_partner_autocomplete")
    @JsonProperty("module_partner_autocomplete")
    private String modulePartnerAutocomplete;

    /**
     * 语言数量
     */
    @JSONField(name = "language_count")
    @JsonProperty("language_count")
    private Integer languageCount;

    /**
     * 链接跟踪器
     */
    @DEField(name = "module_website_links")
    @JSONField(name = "module_website_links")
    @JsonProperty("module_website_links")
    private String moduleWebsiteLinks;

    /**
     * 联系表单上的技术数据
     */
    @JSONField(name = "website_form_enable_metadata")
    @JsonProperty("website_form_enable_metadata")
    private String websiteFormEnableMetadata;

    /**
     * 设置社交平台
     */
    @JSONField(name = "has_social_network")
    @JsonProperty("has_social_network")
    private String hasSocialNetwork;

    /**
     * 客户地址
     */
    @DEField(name = "group_sale_delivery_address")
    @JSONField(name = "group_sale_delivery_address")
    @JsonProperty("group_sale_delivery_address")
    private String groupSaleDeliveryAddress;

    /**
     * 谷歌分析密钥
     */
    @JSONField(name = "google_analytics_key")
    @JsonProperty("google_analytics_key")
    private String googleAnalyticsKey;

    /**
     * LDAP认证
     */
    @DEField(name = "module_auth_ldap")
    @JSONField(name = "module_auth_ldap")
    @JsonProperty("module_auth_ldap")
    private String moduleAuthLdap;

    /**
     * 具体用户账号
     */
    @JSONField(name = "specific_user_account")
    @JsonProperty("specific_user_account")
    private String specificUserAccount;

    /**
     * 在线发布
     */
    @DEField(name = "module_website_hr_recruitment")
    @JSONField(name = "module_website_hr_recruitment")
    @JsonProperty("module_website_hr_recruitment")
    private String moduleWebsiteHrRecruitment;

    /**
     * 预测
     */
    @DEField(name = "module_project_forecast")
    @JSONField(name = "module_project_forecast")
    @JsonProperty("module_project_forecast")
    private String moduleProjectForecast;

    /**
     * 寄售
     */
    @DEField(name = "group_stock_tracking_owner")
    @JSONField(name = "group_stock_tracking_owner")
    @JsonProperty("group_stock_tracking_owner")
    private String groupStockTrackingOwner;

    /**
     * 允许用户同步Google日历
     */
    @DEField(name = "module_google_calendar")
    @JSONField(name = "module_google_calendar")
    @JsonProperty("module_google_calendar")
    private String moduleGoogleCalendar;

    /**
     * 开票
     */
    @DEField(name = "module_account")
    @JSONField(name = "module_account")
    @JsonProperty("module_account")
    private String moduleAccount;

    /**
     * 附加Google文档到记录
     */
    @DEField(name = "module_google_drive")
    @JSONField(name = "module_google_drive")
    @JsonProperty("module_google_drive")
    private String moduleGoogleDrive;

    /**
     * POS价格表
     */
    @DEField(name = "pos_pricelist_setting")
    @JSONField(name = "pos_pricelist_setting")
    @JsonProperty("pos_pricelist_setting")
    private String posPricelistSetting;

    /**
     * 多公司间所有公司的公共用户
     */
    @DEField(name = "company_share_partner")
    @JSONField(name = "company_share_partner")
    @JsonProperty("company_share_partner")
    private String companySharePartner;

    /**
     * 自动汇率
     */
    @DEField(name = "module_currency_rate_live")
    @JSONField(name = "module_currency_rate_live")
    @JsonProperty("module_currency_rate_live")
    private String moduleCurrencyRateLive;

    /**
     * 形式发票
     */
    @DEField(name = "group_proforma_sales")
    @JSONField(name = "group_proforma_sales")
    @JsonProperty("group_proforma_sales")
    private String groupProformaSales;

    /**
     * FedEx 接口
     */
    @DEField(name = "module_delivery_fedex")
    @JSONField(name = "module_delivery_fedex")
    @JsonProperty("module_delivery_fedex")
    private String moduleDeliveryFedex;

    /**
     * 特定的EMail
     */
    @DEField(name = "module_product_email_template")
    @JSONField(name = "module_product_email_template")
    @JsonProperty("module_product_email_template")
    private String moduleProductEmailTemplate;

    /**
     * 显示效果
     */
    @DEField(name = "show_effect")
    @JSONField(name = "show_effect")
    @JsonProperty("show_effect")
    private String showEffect;

    /**
     * 拣货策略
     */
    @DEField(name = "default_picking_policy")
    @JSONField(name = "default_picking_policy")
    @JsonProperty("default_picking_policy")
    private String defaultPickingPolicy;

    /**
     * Youtube账号
     */
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;

    /**
     * 网站公司
     */
    @JSONField(name = "website_company_id")
    @JsonProperty("website_company_id")
    private Integer websiteCompanyId;

    /**
     * 副产品
     */
    @DEField(name = "module_mrp_byproduct")
    @JSONField(name = "module_mrp_byproduct")
    @JsonProperty("module_mrp_byproduct")
    private String moduleMrpByproduct;

    /**
     * USPS 接口
     */
    @DEField(name = "module_delivery_usps")
    @JSONField(name = "module_delivery_usps")
    @JsonProperty("module_delivery_usps")
    private String moduleDeliveryUsps;

    /**
     * DHL 接口
     */
    @DEField(name = "module_delivery_dhl")
    @JSONField(name = "module_delivery_dhl")
    @JsonProperty("module_delivery_dhl")
    private String moduleDeliveryDhl;

    /**
     * 使用项目评级
     */
    @DEField(name = "group_project_rating")
    @JSONField(name = "group_project_rating")
    @JsonProperty("group_project_rating")
    private String groupProjectRating;

    /**
     * Google 地图 API 密钥
     */
    @JSONField(name = "google_maps_api_key")
    @JsonProperty("google_maps_api_key")
    private String googleMapsApiKey;

    /**
     * 线索
     */
    @DEField(name = "group_use_lead")
    @JSONField(name = "group_use_lead")
    @JsonProperty("group_use_lead")
    private String groupUseLead;

    /**
     * 交货包裹
     */
    @DEField(name = "group_stock_tracking_lot")
    @JSONField(name = "group_stock_tracking_lot")
    @JsonProperty("group_stock_tracking_lot")
    private String groupStockTrackingLot;

    /**
     * 多步路由
     */
    @DEField(name = "group_stock_adv_location")
    @JSONField(name = "group_stock_adv_location")
    @JsonProperty("group_stock_adv_location")
    private String groupStockAdvLocation;

    /**
     * 产品复价
     */
    @DEField(name = "pos_sales_price")
    @JSONField(name = "pos_sales_price")
    @JsonProperty("pos_sales_price")
    private String posSalesPrice;

    /**
     * 公司有科目表
     */
    @JSONField(name = "has_chart_of_accounts")
    @JsonProperty("has_chart_of_accounts")
    private String hasChartOfAccounts;

    /**
     * 计价方法
     */
    @DEField(name = "multi_sales_price_method")
    @JSONField(name = "multi_sales_price_method")
    @JsonProperty("multi_sales_price_method")
    private String multiSalesPriceMethod;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 保留
     */
    @DEField(name = "module_procurement_jit")
    @JSONField(name = "module_procurement_jit")
    @JsonProperty("module_procurement_jit")
    private String moduleProcurementJit;

    /**
     * 资产管理
     */
    @DEField(name = "module_account_asset")
    @JSONField(name = "module_account_asset")
    @JsonProperty("module_account_asset")
    private String moduleAccountAsset;

    /**
     * 允许员工通过EMail记录费用
     */
    @DEField(name = "use_mailgateway")
    @JSONField(name = "use_mailgateway")
    @JsonProperty("use_mailgateway")
    private String useMailgateway;

    /**
     * 为每个客户使用价格表来适配您的价格
     */
    @DEField(name = "group_sale_pricelist")
    @JSONField(name = "group_sale_pricelist")
    @JsonProperty("group_sale_pricelist")
    private String groupSalePricelist;

    /**
     * 默认销售团队
     */
    @JSONField(name = "crm_default_team_id")
    @JsonProperty("crm_default_team_id")
    private Integer crmDefaultTeamId;

    /**
     * 储存位置
     */
    @DEField(name = "group_stock_multi_locations")
    @JSONField(name = "group_stock_multi_locations")
    @JsonProperty("group_stock_multi_locations")
    private String groupStockMultiLocations;

    /**
     * 默认制造提前期
     */
    @DEField(name = "use_manufacturing_lead")
    @JSONField(name = "use_manufacturing_lead")
    @JsonProperty("use_manufacturing_lead")
    private String useManufacturingLead;

    /**
     * Google 电子表格
     */
    @DEField(name = "module_google_spreadsheet")
    @JSONField(name = "module_google_spreadsheet")
    @JsonProperty("module_google_spreadsheet")
    private String moduleGoogleSpreadsheet;

    /**
     * 税目汇总表
     */
    @DEField(name = "show_line_subtotals_tax_selection")
    @JSONField(name = "show_line_subtotals_tax_selection")
    @JsonProperty("show_line_subtotals_tax_selection")
    private String showLineSubtotalsTaxSelection;

    /**
     * 语言
     */
    @JSONField(name = "language_ids")
    @JsonProperty("language_ids")
    private String languageIds;

    /**
     * 电商物流成本
     */
    @DEField(name = "module_website_sale_delivery")
    @JSONField(name = "module_website_sale_delivery")
    @JsonProperty("module_website_sale_delivery")
    private String moduleWebsiteSaleDelivery;

    /**
     * 主生产排程
     */
    @DEField(name = "module_mrp_mps")
    @JSONField(name = "module_mrp_mps")
    @JsonProperty("module_mrp_mps")
    private String moduleMrpMps;

    /**
     * 信用不足
     */
    @JSONField(name = "partner_autocomplete_insufficient_credit")
    @JsonProperty("partner_autocomplete_insufficient_credit")
    private String partnerAutocompleteInsufficientCredit;

    /**
     * 显示组织架构图
     */
    @DEField(name = "module_hr_org_chart")
    @JSONField(name = "module_hr_org_chart")
    @JsonProperty("module_hr_org_chart")
    private String moduleHrOrgChart;

    /**
     * 到期日
     */
    @DEField(name = "module_product_expiry")
    @JSONField(name = "module_product_expiry")
    @JsonProperty("module_product_expiry")
    private String moduleProductExpiry;

    /**
     * bpost 接口
     */
    @DEField(name = "module_delivery_bpost")
    @JSONField(name = "module_delivery_bpost")
    @JsonProperty("module_delivery_bpost")
    private String moduleDeliveryBpost;

    /**
     * 条码扫描器
     */
    @DEField(name = "module_stock_barcode")
    @JSONField(name = "module_stock_barcode")
    @JsonProperty("module_stock_barcode")
    private String moduleStockBarcode;

    /**
     * GitHub账户
     */
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;

    /**
     * 国际贸易统计组织
     */
    @DEField(name = "module_account_intrastat")
    @JSONField(name = "module_account_intrastat")
    @JsonProperty("module_account_intrastat")
    private String moduleAccountIntrastat;

    /**
     * 锁定
     */
    @DEField(name = "auto_done_setting")
    @JSONField(name = "auto_done_setting")
    @JsonProperty("auto_done_setting")
    private String autoDoneSetting;

    /**
     * 顾客账号
     */
    @JSONField(name = "auth_signup_uninvited")
    @JsonProperty("auth_signup_uninvited")
    private String authSignupUninvited;

    /**
     * 分享产品 给所有公司
     */
    @DEField(name = "company_share_product")
    @JSONField(name = "company_share_product")
    @JsonProperty("company_share_product")
    private String companyShareProduct;

    /**
     * 报价单模板
     */
    @DEField(name = "group_sale_order_template")
    @JSONField(name = "group_sale_order_template")
    @JsonProperty("group_sale_order_template")
    private String groupSaleOrderTemplate;

    /**
     * 使用SEPA直接计入借方
     */
    @DEField(name = "module_account_sepa_direct_debit")
    @JSONField(name = "module_account_sepa_direct_debit")
    @JsonProperty("module_account_sepa_direct_debit")
    private String moduleAccountSepaDirectDebit;

    /**
     * 默认报价有效期
     */
    @DEField(name = "use_quotation_validity_days")
    @JSONField(name = "use_quotation_validity_days")
    @JsonProperty("use_quotation_validity_days")
    private String useQuotationValidityDays;

    /**
     * 催款等级
     */
    @DEField(name = "module_account_reports_followup")
    @JSONField(name = "module_account_reports_followup")
    @JsonProperty("module_account_reports_followup")
    private String moduleAccountReportsFollowup;

    /**
     * 使用批量付款
     */
    @DEField(name = "module_account_batch_payment")
    @JSONField(name = "module_account_batch_payment")
    @JsonProperty("module_account_batch_payment")
    private String moduleAccountBatchPayment;

    /**
     * Twitter账号
     */
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;

    /**
     * 预算管理
     */
    @DEField(name = "module_account_budget")
    @JSONField(name = "module_account_budget")
    @JsonProperty("module_account_budget")
    private String moduleAccountBudget;

    /**
     * MRP 工单
     */
    @DEField(name = "group_mrp_routings")
    @JSONField(name = "group_mrp_routings")
    @JsonProperty("group_mrp_routings")
    private String groupMrpRoutings;

    /**
     * 现金舍入
     */
    @DEField(name = "group_cash_rounding")
    @JSONField(name = "group_cash_rounding")
    @JsonProperty("group_cash_rounding")
    private String groupCashRounding;

    /**
     * 到岸成本
     */
    @DEField(name = "module_stock_landed_costs")
    @JSONField(name = "module_stock_landed_costs")
    @JsonProperty("module_stock_landed_costs")
    private String moduleStockLandedCosts;

    /**
     * 网站名称
     */
    @JSONField(name = "website_name")
    @JsonProperty("website_name")
    private String websiteName;

    /**
     * 库存
     */
    @DEField(name = "module_website_sale_stock")
    @JSONField(name = "module_website_sale_stock")
    @JsonProperty("module_website_sale_stock")
    private String moduleWebsiteSaleStock;

    /**
     * 网站弹出窗口
     */
    @DEField(name = "group_website_popup_on_exit")
    @JSONField(name = "group_website_popup_on_exit")
    @JsonProperty("group_website_popup_on_exit")
    private String groupWebsitePopupOnExit;

    /**
     * 耿宗并计划
     */
    @DEField(name = "module_website_event_track")
    @JSONField(name = "module_website_event_track")
    @JsonProperty("module_website_event_track")
    private String moduleWebsiteEventTrack;

    /**
     * 供应商价格表
     */
    @DEField(name = "group_manage_vendor_price")
    @JSONField(name = "group_manage_vendor_price")
    @JsonProperty("group_manage_vendor_price")
    private String groupManageVendorPrice;

    /**
     * 运输成本
     */
    @DEField(name = "module_delivery")
    @JSONField(name = "module_delivery")
    @JsonProperty("module_delivery")
    private String moduleDelivery;

    /**
     * 自动票据处理
     */
    @DEField(name = "module_account_invoice_extract")
    @JSONField(name = "module_account_invoice_extract")
    @JsonProperty("module_account_invoice_extract")
    private String moduleAccountInvoiceExtract;

    /**
     * 网站直播频道
     */
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 销售订单警告
     */
    @DEField(name = "group_warning_sale")
    @JSONField(name = "group_warning_sale")
    @JsonProperty("group_warning_sale")
    private String groupWarningSale;

    /**
     * CDN基本网址
     */
    @JSONField(name = "cdn_url")
    @JsonProperty("cdn_url")
    private String cdnUrl;

    /**
     * 条码
     */
    @DEField(name = "module_event_barcode")
    @JSONField(name = "module_event_barcode")
    @JsonProperty("module_event_barcode")
    private String moduleEventBarcode;

    /**
     * 别名域
     */
    @DEField(name = "alias_domain")
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 领英账号
     */
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;

    /**
     * 多仓库
     */
    @DEField(name = "group_stock_multi_warehouses")
    @JSONField(name = "group_stock_multi_warehouses")
    @JsonProperty("group_stock_multi_warehouses")
    private String groupStockMultiWarehouses;

    /**
     * 销售员
     */
    @JSONField(name = "salesperson_id")
    @JsonProperty("salesperson_id")
    private Integer salespersonId;

    /**
     * 动态报告
     */
    @DEField(name = "module_account_reports")
    @JSONField(name = "module_account_reports")
    @JsonProperty("module_account_reports")
    private String moduleAccountReports;

    /**
     * 显示产品的价目表
     */
    @DEField(name = "group_product_pricelist")
    @JSONField(name = "group_product_pricelist")
    @JsonProperty("group_product_pricelist")
    private String groupProductPricelist;

    /**
     * 号码格式
     */
    @DEField(name = "module_crm_phone_validation")
    @JSONField(name = "module_crm_phone_validation")
    @JsonProperty("module_crm_phone_validation")
    private String moduleCrmPhoneValidation;

    /**
     * A / B测试
     */
    @DEField(name = "module_website_version")
    @JSONField(name = "module_website_version")
    @JsonProperty("module_website_version")
    private String moduleWebsiteVersion;

    /**
     * 允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据
     */
    @DEField(name = "module_base_import")
    @JSONField(name = "module_base_import")
    @JsonProperty("module_base_import")
    private String moduleBaseImport;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 以 CSV 格式导入
     */
    @DEField(name = "module_account_bank_statement_import_csv")
    @JSONField(name = "module_account_bank_statement_import_csv")
    @JsonProperty("module_account_bank_statement_import_csv")
    private String moduleAccountBankStatementImportCsv;

    /**
     * 科目税
     */
    @DEField(name = "module_account_taxcloud")
    @JSONField(name = "module_account_taxcloud")
    @JsonProperty("module_account_taxcloud")
    private String moduleAccountTaxcloud;

    /**
     * 默认条款和条件
     */
    @DEField(name = "use_sale_note")
    @JSONField(name = "use_sale_note")
    @JsonProperty("use_sale_note")
    private String useSaleNote;

    /**
     * 放弃时长
     */
    @JSONField(name = "cart_abandoned_delay")
    @JsonProperty("cart_abandoned_delay")
    private Double cartAbandonedDelay;

    /**
     * 网站域名
     */
    @JSONField(name = "website_domain")
    @JsonProperty("website_domain")
    private String websiteDomain;

    /**
     * Accounting
     */
    @DEField(name = "module_account_accountant")
    @JSONField(name = "module_account_accountant")
    @JsonProperty("module_account_accountant")
    private String moduleAccountAccountant;

    /**
     * 毛利
     */
    @DEField(name = "module_sale_margin")
    @JSONField(name = "module_sale_margin")
    @JsonProperty("module_sale_margin")
    private String moduleSaleMargin;

    /**
     * 摘要邮件
     */
    @DEField(name = "digest_emails")
    @JSONField(name = "digest_emails")
    @JsonProperty("digest_emails")
    private String digestEmails;

    /**
     * 协作pad
     */
    @DEField(name = "module_pad")
    @JSONField(name = "module_pad")
    @JsonProperty("module_pad")
    private String modulePad;

    /**
     * 发票警告
     */
    @DEField(name = "group_warning_account")
    @JSONField(name = "group_warning_account")
    @JsonProperty("group_warning_account")
    private String groupWarningAccount;

    /**
     * 贸易条款
     */
    @DEField(name = "group_display_incoterm")
    @JSONField(name = "group_display_incoterm")
    @JsonProperty("group_display_incoterm")
    private String groupDisplayIncoterm;

    /**
     * 心愿单
     */
    @DEField(name = "module_website_sale_wishlist")
    @JSONField(name = "module_website_sale_wishlist")
    @JsonProperty("module_website_sale_wishlist")
    private String moduleWebsiteSaleWishlist;

    /**
     * 默认访问权限
     */
    @DEField(name = "user_default_rights")
    @JSONField(name = "user_default_rights")
    @JsonProperty("user_default_rights")
    private String userDefaultRights;

    /**
     * 账单控制
     */
    @DEField(name = "default_purchase_method")
    @JSONField(name = "default_purchase_method")
    @JsonProperty("default_purchase_method")
    private String defaultPurchaseMethod;

    /**
     * 送货地址
     */
    @DEField(name = "group_delivery_invoice_address")
    @JSONField(name = "group_delivery_invoice_address")
    @JsonProperty("group_delivery_invoice_address")
    private String groupDeliveryInvoiceAddress;

    /**
     * 显示批次 / 序列号
     */
    @DEField(name = "group_lot_on_delivery_slip")
    @JSONField(name = "group_lot_on_delivery_slip")
    @JsonProperty("group_lot_on_delivery_slip")
    private String groupLotOnDeliverySlip;

    /**
     * 入场券
     */
    @DEField(name = "module_event_sale")
    @JSONField(name = "module_event_sale")
    @JsonProperty("module_event_sale")
    private String moduleEventSale;

    /**
     * 显示含税明细行在汇总表(B2B).
     */
    @DEField(name = "group_show_line_subtotals_tax_included")
    @JSONField(name = "group_show_line_subtotals_tax_included")
    @JsonProperty("group_show_line_subtotals_tax_included")
    private String groupShowLineSubtotalsTaxIncluded;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 变体和选项
     */
    @DEField(name = "group_product_variant")
    @JSONField(name = "group_product_variant")
    @JsonProperty("group_product_variant")
    private String groupProductVariant;

    /**
     * SEPA贷记交易
     */
    @DEField(name = "module_account_sepa")
    @JSONField(name = "module_account_sepa")
    @JsonProperty("module_account_sepa")
    private String moduleAccountSepa;

    /**
     * 多币种
     */
    @DEField(name = "group_multi_currency")
    @JSONField(name = "group_multi_currency")
    @JsonProperty("group_multi_currency")
    private String groupMultiCurrency;

    /**
     * 使用供应商单据里的产品
     */
    @DEField(name = "group_products_in_bills")
    @JSONField(name = "group_products_in_bills")
    @JsonProperty("group_products_in_bills")
    private String groupProductsInBills;

    /**
     * 分析会计
     */
    @DEField(name = "group_analytic_accounting")
    @JSONField(name = "group_analytic_accounting")
    @JsonProperty("group_analytic_accounting")
    private String groupAnalyticAccounting;

    /**
     * 产品包装
     */
    @DEField(name = "group_stock_packaging")
    @JSONField(name = "group_stock_packaging")
    @JsonProperty("group_stock_packaging")
    private String groupStockPackaging;

    /**
     * 谷歌文档密钥
     */
    @JSONField(name = "website_slide_google_app_key")
    @JsonProperty("website_slide_google_app_key")
    private String websiteSlideGoogleAppKey;

    /**
     * 采购订单批准
     */
    @DEField(name = "po_order_approval")
    @JSONField(name = "po_order_approval")
    @JsonProperty("po_order_approval")
    private String poOrderApproval;

    /**
     * 销售模块是否已安装
     */
    @DEField(name = "is_installed_sale")
    @JSONField(name = "is_installed_sale")
    @JsonProperty("is_installed_sale")
    private String isInstalledSale;

    /**
     * 发票在线付款
     */
    @DEField(name = "module_account_payment")
    @JSONField(name = "module_account_payment")
    @JsonProperty("module_account_payment")
    private String moduleAccountPayment;

    /**
     * 分析标签
     */
    @DEField(name = "group_analytic_tags")
    @JSONField(name = "group_analytic_tags")
    @JsonProperty("group_analytic_tags")
    private String groupAnalyticTags;

    /**
     * 交货日期
     */
    @DEField(name = "group_sale_order_dates")
    @JSONField(name = "group_sale_order_dates")
    @JsonProperty("group_sale_order_dates")
    private String groupSaleOrderDates;

    /**
     * Asterisk (开源VoIP平台)
     */
    @DEField(name = "module_voip")
    @JSONField(name = "module_voip")
    @JsonProperty("module_voip")
    private String moduleVoip;

    /**
     * 购物车恢复EMail
     */
    @JSONField(name = "cart_recovery_mail_template")
    @JsonProperty("cart_recovery_mail_template")
    private Integer cartRecoveryMailTemplate;

    /**
     * 多网站
     */
    @DEField(name = "group_multi_website")
    @JSONField(name = "group_multi_website")
    @JsonProperty("group_multi_website")
    private String groupMultiWebsite;

    /**
     * 使用外部验证提供者 (OAuth)
     */
    @DEField(name = "module_auth_oauth")
    @JSONField(name = "module_auth_oauth")
    @JsonProperty("module_auth_oauth")
    private String moduleAuthOauth;

    /**
     * 送货管理
     */
    @DEField(name = "sale_delivery_settings")
    @JSONField(name = "sale_delivery_settings")
    @JsonProperty("sale_delivery_settings")
    private String saleDeliverySettings;

    /**
     * 报价单生成器
     */
    @DEField(name = "module_sale_quotation_builder")
    @JSONField(name = "module_sale_quotation_builder")
    @JsonProperty("module_sale_quotation_builder")
    private String moduleSaleQuotationBuilder;

    /**
     * 管理公司间交易
     */
    @DEField(name = "module_inter_company_rules")
    @JSONField(name = "module_inter_company_rules")
    @JsonProperty("module_inter_company_rules")
    private String moduleInterCompanyRules;

    /**
     * 销售的安全提前期
     */
    @DEField(name = "use_security_lead")
    @JSONField(name = "use_security_lead")
    @JsonProperty("use_security_lead")
    private String useSecurityLead;

    /**
     * 开票策略
     */
    @DEField(name = "default_invoice_policy")
    @JSONField(name = "default_invoice_policy")
    @JsonProperty("default_invoice_policy")
    private String defaultInvoicePolicy;

    /**
     * 每个产品的多种销售价格
     */
    @DEField(name = "multi_sales_price")
    @JSONField(name = "multi_sales_price")
    @JsonProperty("multi_sales_price")
    private String multiSalesPrice;

    /**
     * 锁定确认订单
     */
    @DEField(name = "lock_confirmed_po")
    @JSONField(name = "lock_confirmed_po")
    @JsonProperty("lock_confirmed_po")
    private String lockConfirmedPo;

    /**
     * 重量单位
     */
    @DEField(name = "product_weight_in_lbs")
    @JSONField(name = "product_weight_in_lbs")
    @JsonProperty("product_weight_in_lbs")
    private String productWeightInLbs;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 谷歌分析仪表板
     */
    @JSONField(name = "has_google_analytics_dashboard")
    @JsonProperty("has_google_analytics_dashboard")
    private String hasGoogleAnalyticsDashboard;

    /**
     * 批量拣货
     */
    @DEField(name = "module_stock_picking_batch")
    @JSONField(name = "module_stock_picking_batch")
    @JsonProperty("module_stock_picking_batch")
    private String moduleStockPickingBatch;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 默认的费用别名
     */
    @DEField(name = "expense_alias_prefix")
    @JSONField(name = "expense_alias_prefix")
    @JsonProperty("expense_alias_prefix")
    private String expenseAliasPrefix;

    /**
     * 收入识别
     */
    @DEField(name = "module_account_deferred_revenue")
    @JSONField(name = "module_account_deferred_revenue")
    @JsonProperty("module_account_deferred_revenue")
    private String moduleAccountDeferredRevenue;

    /**
     * 脸书账号
     */
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;

    /**
     * Unsplash图像库
     */
    @DEField(name = "module_web_unsplash")
    @JSONField(name = "module_web_unsplash")
    @JsonProperty("module_web_unsplash")
    private String moduleWebUnsplash;

    /**
     * 群发邮件营销
     */
    @DEField(name = "group_mass_mailing_campaign")
    @JSONField(name = "group_mass_mailing_campaign")
    @JsonProperty("group_mass_mailing_campaign")
    private String groupMassMailingCampaign;

    /**
     * 用 CAMT.053 格式导入
     */
    @DEField(name = "module_account_bank_statement_import_camt")
    @JSONField(name = "module_account_bank_statement_import_camt")
    @JsonProperty("module_account_bank_statement_import_camt")
    private String moduleAccountBankStatementImportCamt;

    /**
     * 允许产品毛利
     */
    @DEField(name = "module_product_margin")
    @JSONField(name = "module_product_margin")
    @JsonProperty("module_product_margin")
    private String moduleProductMargin;

    /**
     * 子任务
     */
    @DEField(name = "group_subtask_project")
    @JSONField(name = "group_subtask_project")
    @JsonProperty("group_subtask_project")
    private String groupSubtaskProject;

    /**
     * 三方匹配:采购，收货和发票
     */
    @DEField(name = "module_account_3way_match")
    @JSONField(name = "module_account_3way_match")
    @JsonProperty("module_account_3way_match")
    private String moduleAccount3wayMatch;

    /**
     * 数字内容
     */
    @DEField(name = "module_website_sale_digital")
    @JSONField(name = "module_website_sale_digital")
    @JsonProperty("module_website_sale_digital")
    private String moduleWebsiteSaleDigital;

    /**
     * 优惠券和促销
     */
    @DEField(name = "module_sale_coupon")
    @JSONField(name = "module_sale_coupon")
    @JsonProperty("module_sale_coupon")
    private String moduleSaleCoupon;

    /**
     * 在取消订阅页面上显示黑名单按钮
     */
    @DEField(name = "show_blacklist_buttons")
    @JSONField(name = "show_blacklist_buttons")
    @JsonProperty("show_blacklist_buttons")
    private String showBlacklistButtons;

    /**
     * 库存警报
     */
    @DEField(name = "group_warning_stock")
    @JSONField(name = "group_warning_stock")
    @JsonProperty("group_warning_stock")
    private String groupWarningStock;

    /**
     * 管理多公司
     */
    @DEField(name = "group_multi_company")
    @JSONField(name = "group_multi_company")
    @JsonProperty("group_multi_company")
    private String groupMultiCompany;

    /**
     * 员工 PIN
     */
    @DEField(name = "group_attendance_use_pin")
    @JSONField(name = "group_attendance_use_pin")
    @JsonProperty("group_attendance_use_pin")
    private String groupAttendanceUsePin;

    /**
     * 销售团队
     */
    @JSONField(name = "salesteam_id")
    @JsonProperty("salesteam_id")
    private Integer salesteamId;

    /**
     * 图标
     */
    @JSONField(name = "favicon")
    @JsonProperty("favicon")
    private byte[] favicon;

    /**
     * 产品比较工具
     */
    @DEField(name = "module_website_sale_comparison")
    @JSONField(name = "module_website_sale_comparison")
    @JsonProperty("module_website_sale_comparison")
    private String moduleWebsiteSaleComparison;

    /**
     * 可用阈值
     */
    @DEField(name = "available_threshold")
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 安全交货时间
     */
    @DEField(name = "use_po_lead")
    @JSONField(name = "use_po_lead")
    @JsonProperty("use_po_lead")
    private String usePoLead;

    /**
     * 财年
     */
    @DEField(name = "group_fiscal_year")
    @JSONField(name = "group_fiscal_year")
    @JsonProperty("group_fiscal_year")
    private String groupFiscalYear;

    /**
     * 任务日志
     */
    @DEField(name = "module_hr_timesheet")
    @JSONField(name = "module_hr_timesheet")
    @JsonProperty("module_hr_timesheet")
    private String moduleHrTimesheet;

    /**
     * 产品生命周期管理 (PLM)
     */
    @DEField(name = "module_mrp_plm")
    @JSONField(name = "module_mrp_plm")
    @JsonProperty("module_mrp_plm")
    private String moduleMrpPlm;

    /**
     * 银行接口－自动同步银行费用
     */
    @DEField(name = "module_account_yodlee")
    @JSONField(name = "module_account_yodlee")
    @JsonProperty("module_account_yodlee")
    private String moduleAccountYodlee;

    /**
     * CDN筛选
     */
    @JSONField(name = "cdn_filters")
    @JsonProperty("cdn_filters")
    private String cdnFilters;

    /**
     * 无重新排程传播
     */
    @DEField(name = "use_propagation_minimum_delta")
    @JSONField(name = "use_propagation_minimum_delta")
    @JsonProperty("use_propagation_minimum_delta")
    private String usePropagationMinimumDelta;

    /**
     * UPS 接口
     */
    @DEField(name = "module_delivery_ups")
    @JSONField(name = "module_delivery_ups")
    @JsonProperty("module_delivery_ups")
    private String moduleDeliveryUps;

    /**
     * 失败的邮件
     */
    @DEField(name = "fail_counter")
    @JSONField(name = "fail_counter")
    @JsonProperty("fail_counter")
    private Integer failCounter;

    /**
     * 批次和序列号
     */
    @DEField(name = "group_stock_production_lot")
    @JSONField(name = "group_stock_production_lot")
    @JsonProperty("group_stock_production_lot")
    private String groupStockProductionLot;

    /**
     * 有会计分录
     */
    @JSONField(name = "has_accounting_entries")
    @JsonProperty("has_accounting_entries")
    private String hasAccountingEntries;

    /**
     * 计量单位
     */
    @DEField(name = "group_uom")
    @JSONField(name = "group_uom")
    @JsonProperty("group_uom")
    private String groupUom;

    /**
     * 指定邮件服务器
     */
    @DEField(name = "mass_mailing_outgoing_mail_server")
    @JSONField(name = "mass_mailing_outgoing_mail_server")
    @JsonProperty("mass_mailing_outgoing_mail_server")
    private String massMailingOutgoingMailServer;

    /**
     * 默认线索别名
     */
    @DEField(name = "crm_alias_prefix")
    @JSONField(name = "crm_alias_prefix")
    @JsonProperty("crm_alias_prefix")
    private String crmAliasPrefix;

    /**
     * Google+账户
     */
    @JSONField(name = "social_googleplus")
    @JsonProperty("social_googleplus")
    private String socialGoogleplus;

    /**
     * 订单特定路线
     */
    @DEField(name = "group_route_so_lines")
    @JSONField(name = "group_route_so_lines")
    @JsonProperty("group_route_so_lines")
    private String groupRouteSoLines;

    /**
     * Google 客户端密钥
     */
    @JSONField(name = "google_management_client_secret")
    @JsonProperty("google_management_client_secret")
    private String googleManagementClientSecret;

    /**
     * 默认社交分享图片
     */
    @JSONField(name = "social_default_image")
    @JsonProperty("social_default_image")
    private byte[] socialDefaultImage;

    /**
     * Google 分析
     */
    @JSONField(name = "has_google_analytics")
    @JsonProperty("has_google_analytics")
    private String hasGoogleAnalytics;

    /**
     * 调查登记
     */
    @DEField(name = "module_website_event_questions")
    @JSONField(name = "module_website_event_questions")
    @JsonProperty("module_website_event_questions")
    private String moduleWebsiteEventQuestions;

    /**
     * 默认语言
     */
    @JSONField(name = "website_default_lang_id")
    @JsonProperty("website_default_lang_id")
    private Integer websiteDefaultLangId;

    /**
     * 库存可用性
     */
    @DEField(name = "inventory_availability")
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;

    /**
     * 采购警告
     */
    @DEField(name = "group_warning_purchase")
    @JSONField(name = "group_warning_purchase")
    @JsonProperty("group_warning_purchase")
    private String groupWarningPurchase;

    /**
     * 质量
     */
    @DEField(name = "module_quality_control")
    @JSONField(name = "module_quality_control")
    @JsonProperty("module_quality_control")
    private String moduleQualityControl;

    /**
     * 手动分配EMail
     */
    @DEField(name = "generate_lead_from_alias")
    @JSONField(name = "generate_lead_from_alias")
    @JsonProperty("generate_lead_from_alias")
    private String generateLeadFromAlias;

    /**
     * 价格表
     */
    @DEField(name = "sale_pricelist_setting")
    @JSONField(name = "sale_pricelist_setting")
    @JsonProperty("sale_pricelist_setting")
    private String salePricelistSetting;

    /**
     * 给客户显示价目表
     */
    @DEField(name = "group_pricelist_item")
    @JSONField(name = "group_pricelist_item")
    @JsonProperty("group_pricelist_item")
    private String groupPricelistItem;

    /**
     * 外部邮件服务器
     */
    @DEField(name = "external_email_server_default")
    @JSONField(name = "external_email_server_default")
    @JsonProperty("external_email_server_default")
    private String externalEmailServerDefault;

    /**
     * 访问秘钥
     */
    @DEField(name = "unsplash_access_key")
    @JSONField(name = "unsplash_access_key")
    @JsonProperty("unsplash_access_key")
    private String unsplashAccessKey;

    /**
     * 用Gengo翻译您的网站
     */
    @DEField(name = "module_base_gengo")
    @JSONField(name = "module_base_gengo")
    @JsonProperty("module_base_gengo")
    private String moduleBaseGengo;

    /**
     * 在线票务
     */
    @DEField(name = "module_website_event_sale")
    @JSONField(name = "module_website_event_sale")
    @JsonProperty("module_website_event_sale")
    private String moduleWebsiteEventSale;

    /**
     * 默认销售人员
     */
    @JSONField(name = "crm_default_user_id")
    @JsonProperty("crm_default_user_id")
    private Integer crmDefaultUserId;

    /**
     * 代发货
     */
    @DEField(name = "module_stock_dropshipping")
    @JSONField(name = "module_stock_dropshipping")
    @JsonProperty("module_stock_dropshipping")
    private String moduleStockDropshipping;

    /**
     * 从你的网站流量创建线索/商机
     */
    @DEField(name = "module_crm_reveal")
    @JSONField(name = "module_crm_reveal")
    @JsonProperty("module_crm_reveal")
    private String moduleCrmReveal;

    /**
     * 邮件服务器
     */
    @DEField(name = "mass_mailing_mail_server_id")
    @JSONField(name = "mass_mailing_mail_server_id")
    @JsonProperty("mass_mailing_mail_server_id")
    private Integer massMailingMailServerId;

    /**
     * 明细行汇总含税(B2B).
     */
    @DEField(name = "group_show_line_subtotals_tax_excluded")
    @JSONField(name = "group_show_line_subtotals_tax_excluded")
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    private String groupShowLineSubtotalsTaxExcluded;

    /**
     * 面试表单
     */
    @DEField(name = "module_hr_recruitment_survey")
    @JSONField(name = "module_hr_recruitment_survey")
    @JsonProperty("module_hr_recruitment_survey")
    private String moduleHrRecruitmentSurvey;

    /**
     * 工单
     */
    @DEField(name = "module_mrp_workorder")
    @JSONField(name = "module_mrp_workorder")
    @JsonProperty("module_mrp_workorder")
    private String moduleMrpWorkorder;

    /**
     * 集成卡支付
     */
    @DEField(name = "module_pos_mercury")
    @JSONField(name = "module_pos_mercury")
    @JsonProperty("module_pos_mercury")
    private String modulePosMercury;

    /**
     * 交流
     */
    @JSONField(name = "invoice_reference_type")
    @JsonProperty("invoice_reference_type")
    private String invoiceReferenceType;

    /**
     * 公司货币
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 现金收付制
     */
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 银行核销阈值
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;

    /**
     * 税率现金收付制日记账
     */
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    private Integer taxCashBasisJournalId;

    /**
     * 采购提前时间
     */
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    private Double poLead;

    /**
     * 彩色打印
     */
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    private String snailmailColor;

    /**
     * 纸张格式
     */
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    private Integer paperformatId;

    /**
     * 在线签名
     */
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    private String portalConfirmationSign;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 摘要邮件
     */
    @JSONField(name = "digest_id_text")
    @JsonProperty("digest_id_text")
    private String digestIdText;

    /**
     * 条款及条件
     */
    @JSONField(name = "sale_note")
    @JsonProperty("sale_note")
    private String saleNote;

    /**
     * 用作通过注册创建的新用户的模版
     */
    @JSONField(name = "auth_signup_template_user_id_text")
    @JsonProperty("auth_signup_template_user_id_text")
    private String authSignupTemplateUserIdText;

    /**
     * 打印
     */
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    private String invoiceIsPrint;

    /**
     * 押金产品
     */
    @JSONField(name = "deposit_default_product_id_text")
    @JsonProperty("deposit_default_product_id_text")
    private String depositDefaultProductIdText;

    /**
     * 公司的上班时间
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 采购订单修改 *
     */
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    private String poLock;

    /**
     * 透过邮递
     */
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    private String invoiceIsSnailmail;

    /**
     * 制造提前期(日)
     */
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    private Double manufacturingLead;

    /**
     * 审批层级 *
     */
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    private String poDoubleValidation;

    /**
     * 自定义报表页脚
     */
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    private String reportFooter;

    /**
     * EMail模板
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 发送EMail
     */
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    private String invoiceIsEmail;

    /**
     * 在线支付
     */
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    private String portalConfirmationPay;

    /**
     * 显示SEPA QR码
     */
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 双面打印
     */
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    private String snailmailDuplex;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 安全时间
     */
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    private Double securityLead;

    /**
     * 文档模板
     */
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    private Integer externalReportLayoutId;

    /**
     * 默认进项税
     */
    @JSONField(name = "purchase_tax_id")
    @JsonProperty("purchase_tax_id")
    private Integer purchaseTaxId;

    /**
     * 传播的最小差值
     */
    @JSONField(name = "propagation_minimum_delta")
    @JsonProperty("propagation_minimum_delta")
    private Integer propagationMinimumDelta;

    /**
     * 默认报价有效期（日）
     */
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    private Integer quotationValidityDays;

    /**
     * 税率计算的舍入方法
     */
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;

    /**
     * 汇兑损益
     */
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    private Integer currencyExchangeJournalId;

    /**
     * 最小金额
     */
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    private Double poDoubleValidationAmount;

    /**
     * 模板
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 默认销售税
     */
    @JSONField(name = "sale_tax_id")
    @JsonProperty("sale_tax_id")
    private Integer saleTaxId;

    /**
     * 默认模板
     */
    @JSONField(name = "default_sale_order_template_id_text")
    @JsonProperty("default_sale_order_template_id_text")
    private String defaultSaleOrderTemplateIdText;

    /**
     * 默认模板
     */
    @DEField(name = "default_sale_order_template_id")
    @JSONField(name = "default_sale_order_template_id")
    @JsonProperty("default_sale_order_template_id")
    private Integer defaultSaleOrderTemplateId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 摘要邮件
     */
    @DEField(name = "digest_id")
    @JSONField(name = "digest_id")
    @JsonProperty("digest_id")
    private Integer digestId;

    /**
     * EMail模板
     */
    @DEField(name = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;

    /**
     * 模板
     */
    @DEField(name = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 押金产品
     */
    @DEField(name = "deposit_default_product_id")
    @JSONField(name = "deposit_default_product_id")
    @JsonProperty("deposit_default_product_id")
    private Integer depositDefaultProductId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 用作通过注册创建的新用户的模版
     */
    @DEField(name = "auth_signup_template_user_id")
    @JSONField(name = "auth_signup_template_user_id")
    @JsonProperty("auth_signup_template_user_id")
    private Integer authSignupTemplateUserId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocharttemplate")
    @JsonProperty("odoocharttemplate")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JSONField(name = "odoodigest")
    @JsonProperty("odoodigest")
    private cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest odooDigest;

    /**
     * 
     */
    @JSONField(name = "odootemplate")
    @JsonProperty("odootemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JSONField(name = "odoodepositdefaultproduct")
    @JsonProperty("odoodepositdefaultproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooDepositDefaultProduct;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odooauthsignuptemplateuser")
    @JsonProperty("odooauthsignuptemplateuser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooAuthSignupTemplateUser;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoodefaultsaleordertemplate")
    @JsonProperty("odoodefaultsaleordertemplate")
    private cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template odooDefaultSaleOrderTemplate;




    /**
     * 设置 [导入.qif 文件]
     */
    public void setModuleAccountBankStatementImportQif(String moduleAccountBankStatementImportQif){
        this.moduleAccountBankStatementImportQif = moduleAccountBankStatementImportQif ;
        this.modify("module_account_bank_statement_import_qif",moduleAccountBankStatementImportQif);
    }
    /**
     * 设置 [导入.ofx格式]
     */
    public void setModuleAccountBankStatementImportOfx(String moduleAccountBankStatementImportOfx){
        this.moduleAccountBankStatementImportOfx = moduleAccountBankStatementImportOfx ;
        this.modify("module_account_bank_statement_import_ofx",moduleAccountBankStatementImportOfx);
    }
    /**
     * 设置 [欧盟数字商品增值税]
     */
    public void setModuleL10nEuService(String moduleL10nEuService){
        this.moduleL10nEuService = moduleL10nEuService ;
        this.modify("module_l10n_eu_service",moduleL10nEuService);
    }
    /**
     * 设置 [允许在登录页开启密码重置功能]
     */
    public void setAuthSignupResetPassword(String authSignupResetPassword){
        this.authSignupResetPassword = authSignupResetPassword ;
        this.modify("auth_signup_reset_password",authSignupResetPassword);
    }
    /**
     * 设置 [采购招标]
     */
    public void setModulePurchaseRequisition(String modulePurchaseRequisition){
        this.modulePurchaseRequisition = modulePurchaseRequisition ;
        this.modify("module_purchase_requisition",modulePurchaseRequisition);
    }
    /**
     * 设置 [Easypost 接口]
     */
    public void setModuleDeliveryEasypost(String moduleDeliveryEasypost){
        this.moduleDeliveryEasypost = moduleDeliveryEasypost ;
        this.modify("module_delivery_easypost",moduleDeliveryEasypost);
    }
    /**
     * 设置 [折扣]
     */
    public void setGroupDiscountPerSoLine(String groupDiscountPerSoLine){
        this.groupDiscountPerSoLine = groupDiscountPerSoLine ;
        this.modify("group_discount_per_so_line",groupDiscountPerSoLine);
    }
    /**
     * 设置 [自动开票]
     */
    public void setAutomaticInvoice(String automaticInvoice){
        this.automaticInvoice = automaticInvoice ;
        this.modify("automatic_invoice",automaticInvoice);
    }
    /**
     * 设置 [Plaid 接口]
     */
    public void setModuleAccountPlaid(String moduleAccountPlaid){
        this.moduleAccountPlaid = moduleAccountPlaid ;
        this.modify("module_account_plaid",moduleAccountPlaid);
    }
    /**
     * 设置 [允许支票打印和存款]
     */
    public void setModuleAccountCheckPrinting(String moduleAccountCheckPrinting){
        this.moduleAccountCheckPrinting = moduleAccountCheckPrinting ;
        this.modify("module_account_check_printing",moduleAccountCheckPrinting);
    }
    /**
     * 设置 [自动填充公司数据]
     */
    public void setModulePartnerAutocomplete(String modulePartnerAutocomplete){
        this.modulePartnerAutocomplete = modulePartnerAutocomplete ;
        this.modify("module_partner_autocomplete",modulePartnerAutocomplete);
    }
    /**
     * 设置 [链接跟踪器]
     */
    public void setModuleWebsiteLinks(String moduleWebsiteLinks){
        this.moduleWebsiteLinks = moduleWebsiteLinks ;
        this.modify("module_website_links",moduleWebsiteLinks);
    }
    /**
     * 设置 [客户地址]
     */
    public void setGroupSaleDeliveryAddress(String groupSaleDeliveryAddress){
        this.groupSaleDeliveryAddress = groupSaleDeliveryAddress ;
        this.modify("group_sale_delivery_address",groupSaleDeliveryAddress);
    }
    /**
     * 设置 [LDAP认证]
     */
    public void setModuleAuthLdap(String moduleAuthLdap){
        this.moduleAuthLdap = moduleAuthLdap ;
        this.modify("module_auth_ldap",moduleAuthLdap);
    }
    /**
     * 设置 [在线发布]
     */
    public void setModuleWebsiteHrRecruitment(String moduleWebsiteHrRecruitment){
        this.moduleWebsiteHrRecruitment = moduleWebsiteHrRecruitment ;
        this.modify("module_website_hr_recruitment",moduleWebsiteHrRecruitment);
    }
    /**
     * 设置 [预测]
     */
    public void setModuleProjectForecast(String moduleProjectForecast){
        this.moduleProjectForecast = moduleProjectForecast ;
        this.modify("module_project_forecast",moduleProjectForecast);
    }
    /**
     * 设置 [寄售]
     */
    public void setGroupStockTrackingOwner(String groupStockTrackingOwner){
        this.groupStockTrackingOwner = groupStockTrackingOwner ;
        this.modify("group_stock_tracking_owner",groupStockTrackingOwner);
    }
    /**
     * 设置 [允许用户同步Google日历]
     */
    public void setModuleGoogleCalendar(String moduleGoogleCalendar){
        this.moduleGoogleCalendar = moduleGoogleCalendar ;
        this.modify("module_google_calendar",moduleGoogleCalendar);
    }
    /**
     * 设置 [开票]
     */
    public void setModuleAccount(String moduleAccount){
        this.moduleAccount = moduleAccount ;
        this.modify("module_account",moduleAccount);
    }
    /**
     * 设置 [附加Google文档到记录]
     */
    public void setModuleGoogleDrive(String moduleGoogleDrive){
        this.moduleGoogleDrive = moduleGoogleDrive ;
        this.modify("module_google_drive",moduleGoogleDrive);
    }
    /**
     * 设置 [POS价格表]
     */
    public void setPosPricelistSetting(String posPricelistSetting){
        this.posPricelistSetting = posPricelistSetting ;
        this.modify("pos_pricelist_setting",posPricelistSetting);
    }
    /**
     * 设置 [多公司间所有公司的公共用户]
     */
    public void setCompanySharePartner(String companySharePartner){
        this.companySharePartner = companySharePartner ;
        this.modify("company_share_partner",companySharePartner);
    }
    /**
     * 设置 [自动汇率]
     */
    public void setModuleCurrencyRateLive(String moduleCurrencyRateLive){
        this.moduleCurrencyRateLive = moduleCurrencyRateLive ;
        this.modify("module_currency_rate_live",moduleCurrencyRateLive);
    }
    /**
     * 设置 [形式发票]
     */
    public void setGroupProformaSales(String groupProformaSales){
        this.groupProformaSales = groupProformaSales ;
        this.modify("group_proforma_sales",groupProformaSales);
    }
    /**
     * 设置 [FedEx 接口]
     */
    public void setModuleDeliveryFedex(String moduleDeliveryFedex){
        this.moduleDeliveryFedex = moduleDeliveryFedex ;
        this.modify("module_delivery_fedex",moduleDeliveryFedex);
    }
    /**
     * 设置 [特定的EMail]
     */
    public void setModuleProductEmailTemplate(String moduleProductEmailTemplate){
        this.moduleProductEmailTemplate = moduleProductEmailTemplate ;
        this.modify("module_product_email_template",moduleProductEmailTemplate);
    }
    /**
     * 设置 [显示效果]
     */
    public void setShowEffect(String showEffect){
        this.showEffect = showEffect ;
        this.modify("show_effect",showEffect);
    }
    /**
     * 设置 [拣货策略]
     */
    public void setDefaultPickingPolicy(String defaultPickingPolicy){
        this.defaultPickingPolicy = defaultPickingPolicy ;
        this.modify("default_picking_policy",defaultPickingPolicy);
    }
    /**
     * 设置 [副产品]
     */
    public void setModuleMrpByproduct(String moduleMrpByproduct){
        this.moduleMrpByproduct = moduleMrpByproduct ;
        this.modify("module_mrp_byproduct",moduleMrpByproduct);
    }
    /**
     * 设置 [USPS 接口]
     */
    public void setModuleDeliveryUsps(String moduleDeliveryUsps){
        this.moduleDeliveryUsps = moduleDeliveryUsps ;
        this.modify("module_delivery_usps",moduleDeliveryUsps);
    }
    /**
     * 设置 [DHL 接口]
     */
    public void setModuleDeliveryDhl(String moduleDeliveryDhl){
        this.moduleDeliveryDhl = moduleDeliveryDhl ;
        this.modify("module_delivery_dhl",moduleDeliveryDhl);
    }
    /**
     * 设置 [使用项目评级]
     */
    public void setGroupProjectRating(String groupProjectRating){
        this.groupProjectRating = groupProjectRating ;
        this.modify("group_project_rating",groupProjectRating);
    }
    /**
     * 设置 [线索]
     */
    public void setGroupUseLead(String groupUseLead){
        this.groupUseLead = groupUseLead ;
        this.modify("group_use_lead",groupUseLead);
    }
    /**
     * 设置 [交货包裹]
     */
    public void setGroupStockTrackingLot(String groupStockTrackingLot){
        this.groupStockTrackingLot = groupStockTrackingLot ;
        this.modify("group_stock_tracking_lot",groupStockTrackingLot);
    }
    /**
     * 设置 [多步路由]
     */
    public void setGroupStockAdvLocation(String groupStockAdvLocation){
        this.groupStockAdvLocation = groupStockAdvLocation ;
        this.modify("group_stock_adv_location",groupStockAdvLocation);
    }
    /**
     * 设置 [产品复价]
     */
    public void setPosSalesPrice(String posSalesPrice){
        this.posSalesPrice = posSalesPrice ;
        this.modify("pos_sales_price",posSalesPrice);
    }
    /**
     * 设置 [计价方法]
     */
    public void setMultiSalesPriceMethod(String multiSalesPriceMethod){
        this.multiSalesPriceMethod = multiSalesPriceMethod ;
        this.modify("multi_sales_price_method",multiSalesPriceMethod);
    }
    /**
     * 设置 [保留]
     */
    public void setModuleProcurementJit(String moduleProcurementJit){
        this.moduleProcurementJit = moduleProcurementJit ;
        this.modify("module_procurement_jit",moduleProcurementJit);
    }
    /**
     * 设置 [资产管理]
     */
    public void setModuleAccountAsset(String moduleAccountAsset){
        this.moduleAccountAsset = moduleAccountAsset ;
        this.modify("module_account_asset",moduleAccountAsset);
    }
    /**
     * 设置 [允许员工通过EMail记录费用]
     */
    public void setUseMailgateway(String useMailgateway){
        this.useMailgateway = useMailgateway ;
        this.modify("use_mailgateway",useMailgateway);
    }
    /**
     * 设置 [为每个客户使用价格表来适配您的价格]
     */
    public void setGroupSalePricelist(String groupSalePricelist){
        this.groupSalePricelist = groupSalePricelist ;
        this.modify("group_sale_pricelist",groupSalePricelist);
    }
    /**
     * 设置 [储存位置]
     */
    public void setGroupStockMultiLocations(String groupStockMultiLocations){
        this.groupStockMultiLocations = groupStockMultiLocations ;
        this.modify("group_stock_multi_locations",groupStockMultiLocations);
    }
    /**
     * 设置 [默认制造提前期]
     */
    public void setUseManufacturingLead(String useManufacturingLead){
        this.useManufacturingLead = useManufacturingLead ;
        this.modify("use_manufacturing_lead",useManufacturingLead);
    }
    /**
     * 设置 [Google 电子表格]
     */
    public void setModuleGoogleSpreadsheet(String moduleGoogleSpreadsheet){
        this.moduleGoogleSpreadsheet = moduleGoogleSpreadsheet ;
        this.modify("module_google_spreadsheet",moduleGoogleSpreadsheet);
    }
    /**
     * 设置 [税目汇总表]
     */
    public void setShowLineSubtotalsTaxSelection(String showLineSubtotalsTaxSelection){
        this.showLineSubtotalsTaxSelection = showLineSubtotalsTaxSelection ;
        this.modify("show_line_subtotals_tax_selection",showLineSubtotalsTaxSelection);
    }
    /**
     * 设置 [电商物流成本]
     */
    public void setModuleWebsiteSaleDelivery(String moduleWebsiteSaleDelivery){
        this.moduleWebsiteSaleDelivery = moduleWebsiteSaleDelivery ;
        this.modify("module_website_sale_delivery",moduleWebsiteSaleDelivery);
    }
    /**
     * 设置 [主生产排程]
     */
    public void setModuleMrpMps(String moduleMrpMps){
        this.moduleMrpMps = moduleMrpMps ;
        this.modify("module_mrp_mps",moduleMrpMps);
    }
    /**
     * 设置 [显示组织架构图]
     */
    public void setModuleHrOrgChart(String moduleHrOrgChart){
        this.moduleHrOrgChart = moduleHrOrgChart ;
        this.modify("module_hr_org_chart",moduleHrOrgChart);
    }
    /**
     * 设置 [到期日]
     */
    public void setModuleProductExpiry(String moduleProductExpiry){
        this.moduleProductExpiry = moduleProductExpiry ;
        this.modify("module_product_expiry",moduleProductExpiry);
    }
    /**
     * 设置 [bpost 接口]
     */
    public void setModuleDeliveryBpost(String moduleDeliveryBpost){
        this.moduleDeliveryBpost = moduleDeliveryBpost ;
        this.modify("module_delivery_bpost",moduleDeliveryBpost);
    }
    /**
     * 设置 [条码扫描器]
     */
    public void setModuleStockBarcode(String moduleStockBarcode){
        this.moduleStockBarcode = moduleStockBarcode ;
        this.modify("module_stock_barcode",moduleStockBarcode);
    }
    /**
     * 设置 [国际贸易统计组织]
     */
    public void setModuleAccountIntrastat(String moduleAccountIntrastat){
        this.moduleAccountIntrastat = moduleAccountIntrastat ;
        this.modify("module_account_intrastat",moduleAccountIntrastat);
    }
    /**
     * 设置 [锁定]
     */
    public void setAutoDoneSetting(String autoDoneSetting){
        this.autoDoneSetting = autoDoneSetting ;
        this.modify("auto_done_setting",autoDoneSetting);
    }
    /**
     * 设置 [分享产品 给所有公司]
     */
    public void setCompanyShareProduct(String companyShareProduct){
        this.companyShareProduct = companyShareProduct ;
        this.modify("company_share_product",companyShareProduct);
    }
    /**
     * 设置 [报价单模板]
     */
    public void setGroupSaleOrderTemplate(String groupSaleOrderTemplate){
        this.groupSaleOrderTemplate = groupSaleOrderTemplate ;
        this.modify("group_sale_order_template",groupSaleOrderTemplate);
    }
    /**
     * 设置 [使用SEPA直接计入借方]
     */
    public void setModuleAccountSepaDirectDebit(String moduleAccountSepaDirectDebit){
        this.moduleAccountSepaDirectDebit = moduleAccountSepaDirectDebit ;
        this.modify("module_account_sepa_direct_debit",moduleAccountSepaDirectDebit);
    }
    /**
     * 设置 [默认报价有效期]
     */
    public void setUseQuotationValidityDays(String useQuotationValidityDays){
        this.useQuotationValidityDays = useQuotationValidityDays ;
        this.modify("use_quotation_validity_days",useQuotationValidityDays);
    }
    /**
     * 设置 [催款等级]
     */
    public void setModuleAccountReportsFollowup(String moduleAccountReportsFollowup){
        this.moduleAccountReportsFollowup = moduleAccountReportsFollowup ;
        this.modify("module_account_reports_followup",moduleAccountReportsFollowup);
    }
    /**
     * 设置 [使用批量付款]
     */
    public void setModuleAccountBatchPayment(String moduleAccountBatchPayment){
        this.moduleAccountBatchPayment = moduleAccountBatchPayment ;
        this.modify("module_account_batch_payment",moduleAccountBatchPayment);
    }
    /**
     * 设置 [预算管理]
     */
    public void setModuleAccountBudget(String moduleAccountBudget){
        this.moduleAccountBudget = moduleAccountBudget ;
        this.modify("module_account_budget",moduleAccountBudget);
    }
    /**
     * 设置 [MRP 工单]
     */
    public void setGroupMrpRoutings(String groupMrpRoutings){
        this.groupMrpRoutings = groupMrpRoutings ;
        this.modify("group_mrp_routings",groupMrpRoutings);
    }
    /**
     * 设置 [现金舍入]
     */
    public void setGroupCashRounding(String groupCashRounding){
        this.groupCashRounding = groupCashRounding ;
        this.modify("group_cash_rounding",groupCashRounding);
    }
    /**
     * 设置 [到岸成本]
     */
    public void setModuleStockLandedCosts(String moduleStockLandedCosts){
        this.moduleStockLandedCosts = moduleStockLandedCosts ;
        this.modify("module_stock_landed_costs",moduleStockLandedCosts);
    }
    /**
     * 设置 [库存]
     */
    public void setModuleWebsiteSaleStock(String moduleWebsiteSaleStock){
        this.moduleWebsiteSaleStock = moduleWebsiteSaleStock ;
        this.modify("module_website_sale_stock",moduleWebsiteSaleStock);
    }
    /**
     * 设置 [网站弹出窗口]
     */
    public void setGroupWebsitePopupOnExit(String groupWebsitePopupOnExit){
        this.groupWebsitePopupOnExit = groupWebsitePopupOnExit ;
        this.modify("group_website_popup_on_exit",groupWebsitePopupOnExit);
    }
    /**
     * 设置 [耿宗并计划]
     */
    public void setModuleWebsiteEventTrack(String moduleWebsiteEventTrack){
        this.moduleWebsiteEventTrack = moduleWebsiteEventTrack ;
        this.modify("module_website_event_track",moduleWebsiteEventTrack);
    }
    /**
     * 设置 [供应商价格表]
     */
    public void setGroupManageVendorPrice(String groupManageVendorPrice){
        this.groupManageVendorPrice = groupManageVendorPrice ;
        this.modify("group_manage_vendor_price",groupManageVendorPrice);
    }
    /**
     * 设置 [运输成本]
     */
    public void setModuleDelivery(String moduleDelivery){
        this.moduleDelivery = moduleDelivery ;
        this.modify("module_delivery",moduleDelivery);
    }
    /**
     * 设置 [自动票据处理]
     */
    public void setModuleAccountInvoiceExtract(String moduleAccountInvoiceExtract){
        this.moduleAccountInvoiceExtract = moduleAccountInvoiceExtract ;
        this.modify("module_account_invoice_extract",moduleAccountInvoiceExtract);
    }
    /**
     * 设置 [销售订单警告]
     */
    public void setGroupWarningSale(String groupWarningSale){
        this.groupWarningSale = groupWarningSale ;
        this.modify("group_warning_sale",groupWarningSale);
    }
    /**
     * 设置 [条码]
     */
    public void setModuleEventBarcode(String moduleEventBarcode){
        this.moduleEventBarcode = moduleEventBarcode ;
        this.modify("module_event_barcode",moduleEventBarcode);
    }
    /**
     * 设置 [别名域]
     */
    public void setAliasDomain(String aliasDomain){
        this.aliasDomain = aliasDomain ;
        this.modify("alias_domain",aliasDomain);
    }
    /**
     * 设置 [多仓库]
     */
    public void setGroupStockMultiWarehouses(String groupStockMultiWarehouses){
        this.groupStockMultiWarehouses = groupStockMultiWarehouses ;
        this.modify("group_stock_multi_warehouses",groupStockMultiWarehouses);
    }
    /**
     * 设置 [动态报告]
     */
    public void setModuleAccountReports(String moduleAccountReports){
        this.moduleAccountReports = moduleAccountReports ;
        this.modify("module_account_reports",moduleAccountReports);
    }
    /**
     * 设置 [显示产品的价目表]
     */
    public void setGroupProductPricelist(String groupProductPricelist){
        this.groupProductPricelist = groupProductPricelist ;
        this.modify("group_product_pricelist",groupProductPricelist);
    }
    /**
     * 设置 [号码格式]
     */
    public void setModuleCrmPhoneValidation(String moduleCrmPhoneValidation){
        this.moduleCrmPhoneValidation = moduleCrmPhoneValidation ;
        this.modify("module_crm_phone_validation",moduleCrmPhoneValidation);
    }
    /**
     * 设置 [A / B测试]
     */
    public void setModuleWebsiteVersion(String moduleWebsiteVersion){
        this.moduleWebsiteVersion = moduleWebsiteVersion ;
        this.modify("module_website_version",moduleWebsiteVersion);
    }
    /**
     * 设置 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    public void setModuleBaseImport(String moduleBaseImport){
        this.moduleBaseImport = moduleBaseImport ;
        this.modify("module_base_import",moduleBaseImport);
    }
    /**
     * 设置 [以 CSV 格式导入]
     */
    public void setModuleAccountBankStatementImportCsv(String moduleAccountBankStatementImportCsv){
        this.moduleAccountBankStatementImportCsv = moduleAccountBankStatementImportCsv ;
        this.modify("module_account_bank_statement_import_csv",moduleAccountBankStatementImportCsv);
    }
    /**
     * 设置 [科目税]
     */
    public void setModuleAccountTaxcloud(String moduleAccountTaxcloud){
        this.moduleAccountTaxcloud = moduleAccountTaxcloud ;
        this.modify("module_account_taxcloud",moduleAccountTaxcloud);
    }
    /**
     * 设置 [默认条款和条件]
     */
    public void setUseSaleNote(String useSaleNote){
        this.useSaleNote = useSaleNote ;
        this.modify("use_sale_note",useSaleNote);
    }
    /**
     * 设置 [Accounting]
     */
    public void setModuleAccountAccountant(String moduleAccountAccountant){
        this.moduleAccountAccountant = moduleAccountAccountant ;
        this.modify("module_account_accountant",moduleAccountAccountant);
    }
    /**
     * 设置 [毛利]
     */
    public void setModuleSaleMargin(String moduleSaleMargin){
        this.moduleSaleMargin = moduleSaleMargin ;
        this.modify("module_sale_margin",moduleSaleMargin);
    }
    /**
     * 设置 [摘要邮件]
     */
    public void setDigestEmails(String digestEmails){
        this.digestEmails = digestEmails ;
        this.modify("digest_emails",digestEmails);
    }
    /**
     * 设置 [协作pad]
     */
    public void setModulePad(String modulePad){
        this.modulePad = modulePad ;
        this.modify("module_pad",modulePad);
    }
    /**
     * 设置 [发票警告]
     */
    public void setGroupWarningAccount(String groupWarningAccount){
        this.groupWarningAccount = groupWarningAccount ;
        this.modify("group_warning_account",groupWarningAccount);
    }
    /**
     * 设置 [贸易条款]
     */
    public void setGroupDisplayIncoterm(String groupDisplayIncoterm){
        this.groupDisplayIncoterm = groupDisplayIncoterm ;
        this.modify("group_display_incoterm",groupDisplayIncoterm);
    }
    /**
     * 设置 [心愿单]
     */
    public void setModuleWebsiteSaleWishlist(String moduleWebsiteSaleWishlist){
        this.moduleWebsiteSaleWishlist = moduleWebsiteSaleWishlist ;
        this.modify("module_website_sale_wishlist",moduleWebsiteSaleWishlist);
    }
    /**
     * 设置 [默认访问权限]
     */
    public void setUserDefaultRights(String userDefaultRights){
        this.userDefaultRights = userDefaultRights ;
        this.modify("user_default_rights",userDefaultRights);
    }
    /**
     * 设置 [账单控制]
     */
    public void setDefaultPurchaseMethod(String defaultPurchaseMethod){
        this.defaultPurchaseMethod = defaultPurchaseMethod ;
        this.modify("default_purchase_method",defaultPurchaseMethod);
    }
    /**
     * 设置 [送货地址]
     */
    public void setGroupDeliveryInvoiceAddress(String groupDeliveryInvoiceAddress){
        this.groupDeliveryInvoiceAddress = groupDeliveryInvoiceAddress ;
        this.modify("group_delivery_invoice_address",groupDeliveryInvoiceAddress);
    }
    /**
     * 设置 [显示批次 / 序列号]
     */
    public void setGroupLotOnDeliverySlip(String groupLotOnDeliverySlip){
        this.groupLotOnDeliverySlip = groupLotOnDeliverySlip ;
        this.modify("group_lot_on_delivery_slip",groupLotOnDeliverySlip);
    }
    /**
     * 设置 [入场券]
     */
    public void setModuleEventSale(String moduleEventSale){
        this.moduleEventSale = moduleEventSale ;
        this.modify("module_event_sale",moduleEventSale);
    }
    /**
     * 设置 [显示含税明细行在汇总表(B2B).]
     */
    public void setGroupShowLineSubtotalsTaxIncluded(String groupShowLineSubtotalsTaxIncluded){
        this.groupShowLineSubtotalsTaxIncluded = groupShowLineSubtotalsTaxIncluded ;
        this.modify("group_show_line_subtotals_tax_included",groupShowLineSubtotalsTaxIncluded);
    }
    /**
     * 设置 [变体和选项]
     */
    public void setGroupProductVariant(String groupProductVariant){
        this.groupProductVariant = groupProductVariant ;
        this.modify("group_product_variant",groupProductVariant);
    }
    /**
     * 设置 [SEPA贷记交易]
     */
    public void setModuleAccountSepa(String moduleAccountSepa){
        this.moduleAccountSepa = moduleAccountSepa ;
        this.modify("module_account_sepa",moduleAccountSepa);
    }
    /**
     * 设置 [多币种]
     */
    public void setGroupMultiCurrency(String groupMultiCurrency){
        this.groupMultiCurrency = groupMultiCurrency ;
        this.modify("group_multi_currency",groupMultiCurrency);
    }
    /**
     * 设置 [使用供应商单据里的产品]
     */
    public void setGroupProductsInBills(String groupProductsInBills){
        this.groupProductsInBills = groupProductsInBills ;
        this.modify("group_products_in_bills",groupProductsInBills);
    }
    /**
     * 设置 [分析会计]
     */
    public void setGroupAnalyticAccounting(String groupAnalyticAccounting){
        this.groupAnalyticAccounting = groupAnalyticAccounting ;
        this.modify("group_analytic_accounting",groupAnalyticAccounting);
    }
    /**
     * 设置 [产品包装]
     */
    public void setGroupStockPackaging(String groupStockPackaging){
        this.groupStockPackaging = groupStockPackaging ;
        this.modify("group_stock_packaging",groupStockPackaging);
    }
    /**
     * 设置 [采购订单批准]
     */
    public void setPoOrderApproval(String poOrderApproval){
        this.poOrderApproval = poOrderApproval ;
        this.modify("po_order_approval",poOrderApproval);
    }
    /**
     * 设置 [销售模块是否已安装]
     */
    public void setIsInstalledSale(String isInstalledSale){
        this.isInstalledSale = isInstalledSale ;
        this.modify("is_installed_sale",isInstalledSale);
    }
    /**
     * 设置 [发票在线付款]
     */
    public void setModuleAccountPayment(String moduleAccountPayment){
        this.moduleAccountPayment = moduleAccountPayment ;
        this.modify("module_account_payment",moduleAccountPayment);
    }
    /**
     * 设置 [分析标签]
     */
    public void setGroupAnalyticTags(String groupAnalyticTags){
        this.groupAnalyticTags = groupAnalyticTags ;
        this.modify("group_analytic_tags",groupAnalyticTags);
    }
    /**
     * 设置 [交货日期]
     */
    public void setGroupSaleOrderDates(String groupSaleOrderDates){
        this.groupSaleOrderDates = groupSaleOrderDates ;
        this.modify("group_sale_order_dates",groupSaleOrderDates);
    }
    /**
     * 设置 [Asterisk (开源VoIP平台)]
     */
    public void setModuleVoip(String moduleVoip){
        this.moduleVoip = moduleVoip ;
        this.modify("module_voip",moduleVoip);
    }
    /**
     * 设置 [多网站]
     */
    public void setGroupMultiWebsite(String groupMultiWebsite){
        this.groupMultiWebsite = groupMultiWebsite ;
        this.modify("group_multi_website",groupMultiWebsite);
    }
    /**
     * 设置 [使用外部验证提供者 (OAuth)]
     */
    public void setModuleAuthOauth(String moduleAuthOauth){
        this.moduleAuthOauth = moduleAuthOauth ;
        this.modify("module_auth_oauth",moduleAuthOauth);
    }
    /**
     * 设置 [送货管理]
     */
    public void setSaleDeliverySettings(String saleDeliverySettings){
        this.saleDeliverySettings = saleDeliverySettings ;
        this.modify("sale_delivery_settings",saleDeliverySettings);
    }
    /**
     * 设置 [报价单生成器]
     */
    public void setModuleSaleQuotationBuilder(String moduleSaleQuotationBuilder){
        this.moduleSaleQuotationBuilder = moduleSaleQuotationBuilder ;
        this.modify("module_sale_quotation_builder",moduleSaleQuotationBuilder);
    }
    /**
     * 设置 [管理公司间交易]
     */
    public void setModuleInterCompanyRules(String moduleInterCompanyRules){
        this.moduleInterCompanyRules = moduleInterCompanyRules ;
        this.modify("module_inter_company_rules",moduleInterCompanyRules);
    }
    /**
     * 设置 [销售的安全提前期]
     */
    public void setUseSecurityLead(String useSecurityLead){
        this.useSecurityLead = useSecurityLead ;
        this.modify("use_security_lead",useSecurityLead);
    }
    /**
     * 设置 [开票策略]
     */
    public void setDefaultInvoicePolicy(String defaultInvoicePolicy){
        this.defaultInvoicePolicy = defaultInvoicePolicy ;
        this.modify("default_invoice_policy",defaultInvoicePolicy);
    }
    /**
     * 设置 [每个产品的多种销售价格]
     */
    public void setMultiSalesPrice(String multiSalesPrice){
        this.multiSalesPrice = multiSalesPrice ;
        this.modify("multi_sales_price",multiSalesPrice);
    }
    /**
     * 设置 [锁定确认订单]
     */
    public void setLockConfirmedPo(String lockConfirmedPo){
        this.lockConfirmedPo = lockConfirmedPo ;
        this.modify("lock_confirmed_po",lockConfirmedPo);
    }
    /**
     * 设置 [重量单位]
     */
    public void setProductWeightInLbs(String productWeightInLbs){
        this.productWeightInLbs = productWeightInLbs ;
        this.modify("product_weight_in_lbs",productWeightInLbs);
    }
    /**
     * 设置 [批量拣货]
     */
    public void setModuleStockPickingBatch(String moduleStockPickingBatch){
        this.moduleStockPickingBatch = moduleStockPickingBatch ;
        this.modify("module_stock_picking_batch",moduleStockPickingBatch);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [默认的费用别名]
     */
    public void setExpenseAliasPrefix(String expenseAliasPrefix){
        this.expenseAliasPrefix = expenseAliasPrefix ;
        this.modify("expense_alias_prefix",expenseAliasPrefix);
    }
    /**
     * 设置 [收入识别]
     */
    public void setModuleAccountDeferredRevenue(String moduleAccountDeferredRevenue){
        this.moduleAccountDeferredRevenue = moduleAccountDeferredRevenue ;
        this.modify("module_account_deferred_revenue",moduleAccountDeferredRevenue);
    }
    /**
     * 设置 [Unsplash图像库]
     */
    public void setModuleWebUnsplash(String moduleWebUnsplash){
        this.moduleWebUnsplash = moduleWebUnsplash ;
        this.modify("module_web_unsplash",moduleWebUnsplash);
    }
    /**
     * 设置 [群发邮件营销]
     */
    public void setGroupMassMailingCampaign(String groupMassMailingCampaign){
        this.groupMassMailingCampaign = groupMassMailingCampaign ;
        this.modify("group_mass_mailing_campaign",groupMassMailingCampaign);
    }
    /**
     * 设置 [用 CAMT.053 格式导入]
     */
    public void setModuleAccountBankStatementImportCamt(String moduleAccountBankStatementImportCamt){
        this.moduleAccountBankStatementImportCamt = moduleAccountBankStatementImportCamt ;
        this.modify("module_account_bank_statement_import_camt",moduleAccountBankStatementImportCamt);
    }
    /**
     * 设置 [允许产品毛利]
     */
    public void setModuleProductMargin(String moduleProductMargin){
        this.moduleProductMargin = moduleProductMargin ;
        this.modify("module_product_margin",moduleProductMargin);
    }
    /**
     * 设置 [子任务]
     */
    public void setGroupSubtaskProject(String groupSubtaskProject){
        this.groupSubtaskProject = groupSubtaskProject ;
        this.modify("group_subtask_project",groupSubtaskProject);
    }
    /**
     * 设置 [三方匹配:采购，收货和发票]
     */
    public void setModuleAccount3wayMatch(String moduleAccount3wayMatch){
        this.moduleAccount3wayMatch = moduleAccount3wayMatch ;
        this.modify("module_account_3way_match",moduleAccount3wayMatch);
    }
    /**
     * 设置 [数字内容]
     */
    public void setModuleWebsiteSaleDigital(String moduleWebsiteSaleDigital){
        this.moduleWebsiteSaleDigital = moduleWebsiteSaleDigital ;
        this.modify("module_website_sale_digital",moduleWebsiteSaleDigital);
    }
    /**
     * 设置 [优惠券和促销]
     */
    public void setModuleSaleCoupon(String moduleSaleCoupon){
        this.moduleSaleCoupon = moduleSaleCoupon ;
        this.modify("module_sale_coupon",moduleSaleCoupon);
    }
    /**
     * 设置 [在取消订阅页面上显示黑名单按钮]
     */
    public void setShowBlacklistButtons(String showBlacklistButtons){
        this.showBlacklistButtons = showBlacklistButtons ;
        this.modify("show_blacklist_buttons",showBlacklistButtons);
    }
    /**
     * 设置 [库存警报]
     */
    public void setGroupWarningStock(String groupWarningStock){
        this.groupWarningStock = groupWarningStock ;
        this.modify("group_warning_stock",groupWarningStock);
    }
    /**
     * 设置 [管理多公司]
     */
    public void setGroupMultiCompany(String groupMultiCompany){
        this.groupMultiCompany = groupMultiCompany ;
        this.modify("group_multi_company",groupMultiCompany);
    }
    /**
     * 设置 [员工 PIN]
     */
    public void setGroupAttendanceUsePin(String groupAttendanceUsePin){
        this.groupAttendanceUsePin = groupAttendanceUsePin ;
        this.modify("group_attendance_use_pin",groupAttendanceUsePin);
    }
    /**
     * 设置 [产品比较工具]
     */
    public void setModuleWebsiteSaleComparison(String moduleWebsiteSaleComparison){
        this.moduleWebsiteSaleComparison = moduleWebsiteSaleComparison ;
        this.modify("module_website_sale_comparison",moduleWebsiteSaleComparison);
    }
    /**
     * 设置 [可用阈值]
     */
    public void setAvailableThreshold(Double availableThreshold){
        this.availableThreshold = availableThreshold ;
        this.modify("available_threshold",availableThreshold);
    }
    /**
     * 设置 [安全交货时间]
     */
    public void setUsePoLead(String usePoLead){
        this.usePoLead = usePoLead ;
        this.modify("use_po_lead",usePoLead);
    }
    /**
     * 设置 [财年]
     */
    public void setGroupFiscalYear(String groupFiscalYear){
        this.groupFiscalYear = groupFiscalYear ;
        this.modify("group_fiscal_year",groupFiscalYear);
    }
    /**
     * 设置 [任务日志]
     */
    public void setModuleHrTimesheet(String moduleHrTimesheet){
        this.moduleHrTimesheet = moduleHrTimesheet ;
        this.modify("module_hr_timesheet",moduleHrTimesheet);
    }
    /**
     * 设置 [产品生命周期管理 (PLM)]
     */
    public void setModuleMrpPlm(String moduleMrpPlm){
        this.moduleMrpPlm = moduleMrpPlm ;
        this.modify("module_mrp_plm",moduleMrpPlm);
    }
    /**
     * 设置 [银行接口－自动同步银行费用]
     */
    public void setModuleAccountYodlee(String moduleAccountYodlee){
        this.moduleAccountYodlee = moduleAccountYodlee ;
        this.modify("module_account_yodlee",moduleAccountYodlee);
    }
    /**
     * 设置 [无重新排程传播]
     */
    public void setUsePropagationMinimumDelta(String usePropagationMinimumDelta){
        this.usePropagationMinimumDelta = usePropagationMinimumDelta ;
        this.modify("use_propagation_minimum_delta",usePropagationMinimumDelta);
    }
    /**
     * 设置 [UPS 接口]
     */
    public void setModuleDeliveryUps(String moduleDeliveryUps){
        this.moduleDeliveryUps = moduleDeliveryUps ;
        this.modify("module_delivery_ups",moduleDeliveryUps);
    }
    /**
     * 设置 [失败的邮件]
     */
    public void setFailCounter(Integer failCounter){
        this.failCounter = failCounter ;
        this.modify("fail_counter",failCounter);
    }
    /**
     * 设置 [批次和序列号]
     */
    public void setGroupStockProductionLot(String groupStockProductionLot){
        this.groupStockProductionLot = groupStockProductionLot ;
        this.modify("group_stock_production_lot",groupStockProductionLot);
    }
    /**
     * 设置 [计量单位]
     */
    public void setGroupUom(String groupUom){
        this.groupUom = groupUom ;
        this.modify("group_uom",groupUom);
    }
    /**
     * 设置 [指定邮件服务器]
     */
    public void setMassMailingOutgoingMailServer(String massMailingOutgoingMailServer){
        this.massMailingOutgoingMailServer = massMailingOutgoingMailServer ;
        this.modify("mass_mailing_outgoing_mail_server",massMailingOutgoingMailServer);
    }
    /**
     * 设置 [默认线索别名]
     */
    public void setCrmAliasPrefix(String crmAliasPrefix){
        this.crmAliasPrefix = crmAliasPrefix ;
        this.modify("crm_alias_prefix",crmAliasPrefix);
    }
    /**
     * 设置 [订单特定路线]
     */
    public void setGroupRouteSoLines(String groupRouteSoLines){
        this.groupRouteSoLines = groupRouteSoLines ;
        this.modify("group_route_so_lines",groupRouteSoLines);
    }
    /**
     * 设置 [调查登记]
     */
    public void setModuleWebsiteEventQuestions(String moduleWebsiteEventQuestions){
        this.moduleWebsiteEventQuestions = moduleWebsiteEventQuestions ;
        this.modify("module_website_event_questions",moduleWebsiteEventQuestions);
    }
    /**
     * 设置 [库存可用性]
     */
    public void setInventoryAvailability(String inventoryAvailability){
        this.inventoryAvailability = inventoryAvailability ;
        this.modify("inventory_availability",inventoryAvailability);
    }
    /**
     * 设置 [采购警告]
     */
    public void setGroupWarningPurchase(String groupWarningPurchase){
        this.groupWarningPurchase = groupWarningPurchase ;
        this.modify("group_warning_purchase",groupWarningPurchase);
    }
    /**
     * 设置 [质量]
     */
    public void setModuleQualityControl(String moduleQualityControl){
        this.moduleQualityControl = moduleQualityControl ;
        this.modify("module_quality_control",moduleQualityControl);
    }
    /**
     * 设置 [手动分配EMail]
     */
    public void setGenerateLeadFromAlias(String generateLeadFromAlias){
        this.generateLeadFromAlias = generateLeadFromAlias ;
        this.modify("generate_lead_from_alias",generateLeadFromAlias);
    }
    /**
     * 设置 [价格表]
     */
    public void setSalePricelistSetting(String salePricelistSetting){
        this.salePricelistSetting = salePricelistSetting ;
        this.modify("sale_pricelist_setting",salePricelistSetting);
    }
    /**
     * 设置 [给客户显示价目表]
     */
    public void setGroupPricelistItem(String groupPricelistItem){
        this.groupPricelistItem = groupPricelistItem ;
        this.modify("group_pricelist_item",groupPricelistItem);
    }
    /**
     * 设置 [外部邮件服务器]
     */
    public void setExternalEmailServerDefault(String externalEmailServerDefault){
        this.externalEmailServerDefault = externalEmailServerDefault ;
        this.modify("external_email_server_default",externalEmailServerDefault);
    }
    /**
     * 设置 [访问秘钥]
     */
    public void setUnsplashAccessKey(String unsplashAccessKey){
        this.unsplashAccessKey = unsplashAccessKey ;
        this.modify("unsplash_access_key",unsplashAccessKey);
    }
    /**
     * 设置 [用Gengo翻译您的网站]
     */
    public void setModuleBaseGengo(String moduleBaseGengo){
        this.moduleBaseGengo = moduleBaseGengo ;
        this.modify("module_base_gengo",moduleBaseGengo);
    }
    /**
     * 设置 [在线票务]
     */
    public void setModuleWebsiteEventSale(String moduleWebsiteEventSale){
        this.moduleWebsiteEventSale = moduleWebsiteEventSale ;
        this.modify("module_website_event_sale",moduleWebsiteEventSale);
    }
    /**
     * 设置 [代发货]
     */
    public void setModuleStockDropshipping(String moduleStockDropshipping){
        this.moduleStockDropshipping = moduleStockDropshipping ;
        this.modify("module_stock_dropshipping",moduleStockDropshipping);
    }
    /**
     * 设置 [从你的网站流量创建线索/商机]
     */
    public void setModuleCrmReveal(String moduleCrmReveal){
        this.moduleCrmReveal = moduleCrmReveal ;
        this.modify("module_crm_reveal",moduleCrmReveal);
    }
    /**
     * 设置 [邮件服务器]
     */
    public void setMassMailingMailServerId(Integer massMailingMailServerId){
        this.massMailingMailServerId = massMailingMailServerId ;
        this.modify("mass_mailing_mail_server_id",massMailingMailServerId);
    }
    /**
     * 设置 [明细行汇总含税(B2B).]
     */
    public void setGroupShowLineSubtotalsTaxExcluded(String groupShowLineSubtotalsTaxExcluded){
        this.groupShowLineSubtotalsTaxExcluded = groupShowLineSubtotalsTaxExcluded ;
        this.modify("group_show_line_subtotals_tax_excluded",groupShowLineSubtotalsTaxExcluded);
    }
    /**
     * 设置 [面试表单]
     */
    public void setModuleHrRecruitmentSurvey(String moduleHrRecruitmentSurvey){
        this.moduleHrRecruitmentSurvey = moduleHrRecruitmentSurvey ;
        this.modify("module_hr_recruitment_survey",moduleHrRecruitmentSurvey);
    }
    /**
     * 设置 [工单]
     */
    public void setModuleMrpWorkorder(String moduleMrpWorkorder){
        this.moduleMrpWorkorder = moduleMrpWorkorder ;
        this.modify("module_mrp_workorder",moduleMrpWorkorder);
    }
    /**
     * 设置 [集成卡支付]
     */
    public void setModulePosMercury(String modulePosMercury){
        this.modulePosMercury = modulePosMercury ;
        this.modify("module_pos_mercury",modulePosMercury);
    }
    /**
     * 设置 [默认模板]
     */
    public void setDefaultSaleOrderTemplateId(Integer defaultSaleOrderTemplateId){
        this.defaultSaleOrderTemplateId = defaultSaleOrderTemplateId ;
        this.modify("default_sale_order_template_id",defaultSaleOrderTemplateId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [摘要邮件]
     */
    public void setDigestId(Integer digestId){
        this.digestId = digestId ;
        this.modify("digest_id",digestId);
    }
    /**
     * 设置 [EMail模板]
     */
    public void setTemplateId(Integer templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }
    /**
     * 设置 [模板]
     */
    public void setChartTemplateId(Integer chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }
    /**
     * 设置 [押金产品]
     */
    public void setDepositDefaultProductId(Integer depositDefaultProductId){
        this.depositDefaultProductId = depositDefaultProductId ;
        this.modify("deposit_default_product_id",depositDefaultProductId);
    }
    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    public void setAuthSignupTemplateUserId(Integer authSignupTemplateUserId){
        this.authSignupTemplateUserId = authSignupTemplateUserId ;
        this.modify("auth_signup_template_user_id",authSignupTemplateUserId);
    }

}



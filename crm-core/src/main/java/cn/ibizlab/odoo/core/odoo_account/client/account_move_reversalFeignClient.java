package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_move_reversal] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-move-reversal", fallback = account_move_reversalFallback.class)
public interface account_move_reversalFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/{id}")
    Account_move_reversal update(@PathVariable("id") Integer id,@RequestBody Account_move_reversal account_move_reversal);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/batch")
    Boolean updateBatch(@RequestBody List<Account_move_reversal> account_move_reversals);



    @RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals")
    Account_move_reversal create(@RequestBody Account_move_reversal account_move_reversal);

    @RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/batch")
    Boolean createBatch(@RequestBody List<Account_move_reversal> account_move_reversals);



    @RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/{id}")
    Account_move_reversal get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/searchdefault")
    Page<Account_move_reversal> searchDefault(@RequestBody Account_move_reversalSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/select")
    Page<Account_move_reversal> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/getdraft")
    Account_move_reversal getDraft();


}

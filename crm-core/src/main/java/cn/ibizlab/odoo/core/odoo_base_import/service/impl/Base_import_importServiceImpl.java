package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_importSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_importService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_importFeignClient;

/**
 * 实体[基础导入] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_importServiceImpl implements IBase_import_importService {

    @Autowired
    base_import_importFeignClient base_import_importFeignClient;


    @Override
    public Base_import_import getDraft(Base_import_import et) {
        et=base_import_importFeignClient.getDraft();
        return et;
    }

    @Override
    public Base_import_import get(Integer id) {
		Base_import_import et=base_import_importFeignClient.get(id);
        if(et==null){
            et=new Base_import_import();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_import_import et) {
        Base_import_import rt = base_import_importFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_import> list){
        base_import_importFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_import_import et) {
        Base_import_import rt = base_import_importFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_import> list){
        base_import_importFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_importFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_importFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_import> searchDefault(Base_import_importSearchContext context) {
        Page<Base_import_import> base_import_imports=base_import_importFeignClient.searchDefault(context);
        return base_import_imports;
    }


}



package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_company] 对象
 */
public interface Ires_company {

    /**
     * 获取 [银行核销阈值]
     */
    public void setAccount_bank_reconciliation_start(Timestamp account_bank_reconciliation_start);
    
    /**
     * 设置 [银行核销阈值]
     */
    public Timestamp getAccount_bank_reconciliation_start();

    /**
     * 获取 [银行核销阈值]脏标记
     */
    public boolean getAccount_bank_reconciliation_startDirtyFlag();
    /**
     * 获取 [处于会计面板的状态]
     */
    public void setAccount_dashboard_onboarding_state(String account_dashboard_onboarding_state);
    
    /**
     * 设置 [处于会计面板的状态]
     */
    public String getAccount_dashboard_onboarding_state();

    /**
     * 获取 [处于会计面板的状态]脏标记
     */
    public boolean getAccount_dashboard_onboarding_stateDirtyFlag();
    /**
     * 获取 [处于会计发票面板的状态]
     */
    public void setAccount_invoice_onboarding_state(String account_invoice_onboarding_state);
    
    /**
     * 设置 [处于会计发票面板的状态]
     */
    public String getAccount_invoice_onboarding_state();

    /**
     * 获取 [处于会计发票面板的状态]脏标记
     */
    public boolean getAccount_invoice_onboarding_stateDirtyFlag();
    /**
     * 获取 [科目号码]
     */
    public void setAccount_no(String account_no);
    
    /**
     * 设置 [科目号码]
     */
    public String getAccount_no();

    /**
     * 获取 [科目号码]脏标记
     */
    public boolean getAccount_noDirtyFlag();
    /**
     * 获取 [有待被确认的发票步骤的状态]
     */
    public void setAccount_onboarding_invoice_layout_state(String account_onboarding_invoice_layout_state);
    
    /**
     * 设置 [有待被确认的发票步骤的状态]
     */
    public String getAccount_onboarding_invoice_layout_state();

    /**
     * 获取 [有待被确认的发票步骤的状态]脏标记
     */
    public boolean getAccount_onboarding_invoice_layout_stateDirtyFlag();
    /**
     * 获取 [有待被确认的报价单步骤的状态]
     */
    public void setAccount_onboarding_sale_tax_state(String account_onboarding_sale_tax_state);
    
    /**
     * 设置 [有待被确认的报价单步骤的状态]
     */
    public String getAccount_onboarding_sale_tax_state();

    /**
     * 获取 [有待被确认的报价单步骤的状态]脏标记
     */
    public boolean getAccount_onboarding_sale_tax_stateDirtyFlag();
    /**
     * 获取 [有待被确认的样品报价单步骤的状态]
     */
    public void setAccount_onboarding_sample_invoice_state(String account_onboarding_sample_invoice_state);
    
    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public String getAccount_onboarding_sample_invoice_state();

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    public boolean getAccount_onboarding_sample_invoice_stateDirtyFlag();
    /**
     * 获取 [期初日期]
     */
    public void setAccount_opening_date(Timestamp account_opening_date);
    
    /**
     * 设置 [期初日期]
     */
    public Timestamp getAccount_opening_date();

    /**
     * 获取 [期初日期]脏标记
     */
    public boolean getAccount_opening_dateDirtyFlag();
    /**
     * 获取 [期初日记账]
     */
    public void setAccount_opening_journal_id(Integer account_opening_journal_id);
    
    /**
     * 设置 [期初日记账]
     */
    public Integer getAccount_opening_journal_id();

    /**
     * 获取 [期初日记账]脏标记
     */
    public boolean getAccount_opening_journal_idDirtyFlag();
    /**
     * 获取 [期初日记账分录]
     */
    public void setAccount_opening_move_id(Integer account_opening_move_id);
    
    /**
     * 设置 [期初日记账分录]
     */
    public Integer getAccount_opening_move_id();

    /**
     * 获取 [期初日记账分录]脏标记
     */
    public boolean getAccount_opening_move_idDirtyFlag();
    /**
     * 获取 [期初日记账分录]
     */
    public void setAccount_opening_move_id_text(String account_opening_move_id_text);
    
    /**
     * 设置 [期初日记账分录]
     */
    public String getAccount_opening_move_id_text();

    /**
     * 获取 [期初日记账分录]脏标记
     */
    public boolean getAccount_opening_move_id_textDirtyFlag();
    /**
     * 获取 [默认进项税]
     */
    public void setAccount_purchase_tax_id(Integer account_purchase_tax_id);
    
    /**
     * 设置 [默认进项税]
     */
    public Integer getAccount_purchase_tax_id();

    /**
     * 获取 [默认进项税]脏标记
     */
    public boolean getAccount_purchase_tax_idDirtyFlag();
    /**
     * 获取 [默认进项税]
     */
    public void setAccount_purchase_tax_id_text(String account_purchase_tax_id_text);
    
    /**
     * 设置 [默认进项税]
     */
    public String getAccount_purchase_tax_id_text();

    /**
     * 获取 [默认进项税]脏标记
     */
    public boolean getAccount_purchase_tax_id_textDirtyFlag();
    /**
     * 获取 [默认销售税]
     */
    public void setAccount_sale_tax_id(Integer account_sale_tax_id);
    
    /**
     * 设置 [默认销售税]
     */
    public Integer getAccount_sale_tax_id();

    /**
     * 获取 [默认销售税]脏标记
     */
    public boolean getAccount_sale_tax_idDirtyFlag();
    /**
     * 获取 [默认销售税]
     */
    public void setAccount_sale_tax_id_text(String account_sale_tax_id_text);
    
    /**
     * 设置 [默认销售税]
     */
    public String getAccount_sale_tax_id_text();

    /**
     * 获取 [默认销售税]脏标记
     */
    public boolean getAccount_sale_tax_id_textDirtyFlag();
    /**
     * 获取 [有待被确认的银行数据步骤的状态]
     */
    public void setAccount_setup_bank_data_state(String account_setup_bank_data_state);
    
    /**
     * 设置 [有待被确认的银行数据步骤的状态]
     */
    public String getAccount_setup_bank_data_state();

    /**
     * 获取 [有待被确认的银行数据步骤的状态]脏标记
     */
    public boolean getAccount_setup_bank_data_stateDirtyFlag();
    /**
     * 获取 [科目状态]
     */
    public void setAccount_setup_coa_state(String account_setup_coa_state);
    
    /**
     * 设置 [科目状态]
     */
    public String getAccount_setup_coa_state();

    /**
     * 获取 [科目状态]脏标记
     */
    public boolean getAccount_setup_coa_stateDirtyFlag();
    /**
     * 获取 [有待被确认的会计年度步骤的状态]
     */
    public void setAccount_setup_fy_data_state(String account_setup_fy_data_state);
    
    /**
     * 设置 [有待被确认的会计年度步骤的状态]
     */
    public String getAccount_setup_fy_data_state();

    /**
     * 获取 [有待被确认的会计年度步骤的状态]脏标记
     */
    public boolean getAccount_setup_fy_data_stateDirtyFlag();
    /**
     * 获取 [使用anglo-saxon会计]
     */
    public void setAnglo_saxon_accounting(String anglo_saxon_accounting);
    
    /**
     * 设置 [使用anglo-saxon会计]
     */
    public String getAnglo_saxon_accounting();

    /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    public boolean getAnglo_saxon_accountingDirtyFlag();
    /**
     * 获取 [银行科目的前缀]
     */
    public void setBank_account_code_prefix(String bank_account_code_prefix);
    
    /**
     * 设置 [银行科目的前缀]
     */
    public String getBank_account_code_prefix();

    /**
     * 获取 [银行科目的前缀]脏标记
     */
    public boolean getBank_account_code_prefixDirtyFlag();
    /**
     * 获取 [银行账户]
     */
    public void setBank_ids(String bank_ids);
    
    /**
     * 设置 [银行账户]
     */
    public String getBank_ids();

    /**
     * 获取 [银行账户]脏标记
     */
    public boolean getBank_idsDirtyFlag();
    /**
     * 获取 [银行日记账]
     */
    public void setBank_journal_ids(String bank_journal_ids);
    
    /**
     * 设置 [银行日记账]
     */
    public String getBank_journal_ids();

    /**
     * 获取 [银行日记账]脏标记
     */
    public boolean getBank_journal_idsDirtyFlag();
    /**
     * 获取 [公司状态]
     */
    public void setBase_onboarding_company_state(String base_onboarding_company_state);
    
    /**
     * 设置 [公司状态]
     */
    public String getBase_onboarding_company_state();

    /**
     * 获取 [公司状态]脏标记
     */
    public boolean getBase_onboarding_company_stateDirtyFlag();
    /**
     * 获取 [现金科目的前缀]
     */
    public void setCash_account_code_prefix(String cash_account_code_prefix);
    
    /**
     * 设置 [现金科目的前缀]
     */
    public String getCash_account_code_prefix();

    /**
     * 获取 [现金科目的前缀]脏标记
     */
    public boolean getCash_account_code_prefixDirtyFlag();
    /**
     * 获取 [预设邮件]
     */
    public void setCatchall(String catchall);
    
    /**
     * 设置 [预设邮件]
     */
    public String getCatchall();

    /**
     * 获取 [预设邮件]脏标记
     */
    public boolean getCatchallDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id(Integer chart_template_id);
    
    /**
     * 设置 [表模板]
     */
    public Integer getChart_template_id();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_idDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id_text(String chart_template_id_text);
    
    /**
     * 设置 [表模板]
     */
    public String getChart_template_id_text();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_id_textDirtyFlag();
    /**
     * 获取 [下级公司]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [下级公司]
     */
    public String getChild_ids();

    /**
     * 获取 [下级公司]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [城市]
     */
    public void setCity(String city);
    
    /**
     * 设置 [城市]
     */
    public String getCity();

    /**
     * 获取 [城市]脏标记
     */
    public boolean getCityDirtyFlag();
    /**
     * 获取 [公司注册]
     */
    public void setCompany_registry(String company_registry);
    
    /**
     * 设置 [公司注册]
     */
    public String getCompany_registry();

    /**
     * 获取 [公司注册]脏标记
     */
    public boolean getCompany_registryDirtyFlag();
    /**
     * 获取 [国家/地区]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家/地区]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家/地区]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [汇兑损益]
     */
    public void setCurrency_exchange_journal_id(Integer currency_exchange_journal_id);
    
    /**
     * 设置 [汇兑损益]
     */
    public Integer getCurrency_exchange_journal_id();

    /**
     * 获取 [汇兑损益]脏标记
     */
    public boolean getCurrency_exchange_journal_idDirtyFlag();
    /**
     * 获取 [汇兑损益]
     */
    public void setCurrency_exchange_journal_id_text(String currency_exchange_journal_id_text);
    
    /**
     * 设置 [汇兑损益]
     */
    public String getCurrency_exchange_journal_id_text();

    /**
     * 获取 [汇兑损益]脏标记
     */
    public boolean getCurrency_exchange_journal_id_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [预计会计科目表]
     */
    public void setExpects_chart_of_accounts(String expects_chart_of_accounts);
    
    /**
     * 设置 [预计会计科目表]
     */
    public String getExpects_chart_of_accounts();

    /**
     * 获取 [预计会计科目表]脏标记
     */
    public boolean getExpects_chart_of_accountsDirtyFlag();
    /**
     * 获取 [汇率损失科目]
     */
    public void setExpense_currency_exchange_account_id(Integer expense_currency_exchange_account_id);
    
    /**
     * 设置 [汇率损失科目]
     */
    public Integer getExpense_currency_exchange_account_id();

    /**
     * 获取 [汇率损失科目]脏标记
     */
    public boolean getExpense_currency_exchange_account_idDirtyFlag();
    /**
     * 获取 [文档模板]
     */
    public void setExternal_report_layout_id(Integer external_report_layout_id);
    
    /**
     * 设置 [文档模板]
     */
    public Integer getExternal_report_layout_id();

    /**
     * 获取 [文档模板]脏标记
     */
    public boolean getExternal_report_layout_idDirtyFlag();
    /**
     * 获取 [会计年度的最后一天]
     */
    public void setFiscalyear_last_day(Integer fiscalyear_last_day);
    
    /**
     * 设置 [会计年度的最后一天]
     */
    public Integer getFiscalyear_last_day();

    /**
     * 获取 [会计年度的最后一天]脏标记
     */
    public boolean getFiscalyear_last_dayDirtyFlag();
    /**
     * 获取 [会计年度的最后一个月]
     */
    public void setFiscalyear_last_month(String fiscalyear_last_month);
    
    /**
     * 设置 [会计年度的最后一个月]
     */
    public String getFiscalyear_last_month();

    /**
     * 获取 [会计年度的最后一个月]脏标记
     */
    public boolean getFiscalyear_last_monthDirtyFlag();
    /**
     * 获取 [锁定日期]
     */
    public void setFiscalyear_lock_date(Timestamp fiscalyear_lock_date);
    
    /**
     * 设置 [锁定日期]
     */
    public Timestamp getFiscalyear_lock_date();

    /**
     * 获取 [锁定日期]脏标记
     */
    public boolean getFiscalyear_lock_dateDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [汇率增益科目]
     */
    public void setIncome_currency_exchange_account_id(Integer income_currency_exchange_account_id);
    
    /**
     * 设置 [汇率增益科目]
     */
    public Integer getIncome_currency_exchange_account_id();

    /**
     * 获取 [汇率增益科目]脏标记
     */
    public boolean getIncome_currency_exchange_account_idDirtyFlag();
    /**
     * 获取 [默认国际贸易术语]
     */
    public void setIncoterm_id(Integer incoterm_id);
    
    /**
     * 设置 [默认国际贸易术语]
     */
    public Integer getIncoterm_id();

    /**
     * 获取 [默认国际贸易术语]脏标记
     */
    public boolean getIncoterm_idDirtyFlag();
    /**
     * 获取 [默认国际贸易术语]
     */
    public void setIncoterm_id_text(String incoterm_id_text);
    
    /**
     * 设置 [默认国际贸易术语]
     */
    public String getIncoterm_id_text();

    /**
     * 获取 [默认国际贸易术语]脏标记
     */
    public boolean getIncoterm_id_textDirtyFlag();
    /**
     * 获取 [内部中转位置]
     */
    public void setInternal_transit_location_id(Integer internal_transit_location_id);
    
    /**
     * 设置 [内部中转位置]
     */
    public Integer getInternal_transit_location_id();

    /**
     * 获取 [内部中转位置]脏标记
     */
    public boolean getInternal_transit_location_idDirtyFlag();
    /**
     * 获取 [内部中转位置]
     */
    public void setInternal_transit_location_id_text(String internal_transit_location_id_text);
    
    /**
     * 设置 [内部中转位置]
     */
    public String getInternal_transit_location_id_text();

    /**
     * 获取 [内部中转位置]脏标记
     */
    public boolean getInternal_transit_location_id_textDirtyFlag();
    /**
     * 获取 [默认邮件]
     */
    public void setInvoice_is_email(String invoice_is_email);
    
    /**
     * 设置 [默认邮件]
     */
    public String getInvoice_is_email();

    /**
     * 获取 [默认邮件]脏标记
     */
    public boolean getInvoice_is_emailDirtyFlag();
    /**
     * 获取 [通过默认值打印]
     */
    public void setInvoice_is_print(String invoice_is_print);
    
    /**
     * 设置 [通过默认值打印]
     */
    public String getInvoice_is_print();

    /**
     * 获取 [通过默认值打印]脏标记
     */
    public boolean getInvoice_is_printDirtyFlag();
    /**
     * 获取 [默认以信件发送]
     */
    public void setInvoice_is_snailmail(String invoice_is_snailmail);
    
    /**
     * 设置 [默认以信件发送]
     */
    public String getInvoice_is_snailmail();

    /**
     * 获取 [默认以信件发送]脏标记
     */
    public boolean getInvoice_is_snailmailDirtyFlag();
    /**
     * 获取 [默认信息类型]
     */
    public void setInvoice_reference_type(String invoice_reference_type);
    
    /**
     * 设置 [默认信息类型]
     */
    public String getInvoice_reference_type();

    /**
     * 获取 [默认信息类型]脏标记
     */
    public boolean getInvoice_reference_typeDirtyFlag();
    /**
     * 获取 [公司 Logo]
     */
    public void setLogo(byte[] logo);
    
    /**
     * 设置 [公司 Logo]
     */
    public byte[] getLogo();

    /**
     * 获取 [公司 Logo]脏标记
     */
    public boolean getLogoDirtyFlag();
    /**
     * 获取 [网页徽标]
     */
    public void setLogo_web(byte[] logo_web);
    
    /**
     * 设置 [网页徽标]
     */
    public byte[] getLogo_web();

    /**
     * 获取 [网页徽标]脏标记
     */
    public boolean getLogo_webDirtyFlag();
    /**
     * 获取 [制造提前期(日)]
     */
    public void setManufacturing_lead(Double manufacturing_lead);
    
    /**
     * 设置 [制造提前期(日)]
     */
    public Double getManufacturing_lead();

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    public boolean getManufacturing_leadDirtyFlag();
    /**
     * 获取 [公司名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [公司名称]
     */
    public String getName();

    /**
     * 获取 [公司名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [命名规则]
     */
    public void setNomenclature_id(Integer nomenclature_id);
    
    /**
     * 设置 [命名规则]
     */
    public Integer getNomenclature_id();

    /**
     * 获取 [命名规则]脏标记
     */
    public boolean getNomenclature_idDirtyFlag();
    /**
     * 获取 [逾期追款消息]
     */
    public void setOverdue_msg(String overdue_msg);
    
    /**
     * 设置 [逾期追款消息]
     */
    public String getOverdue_msg();

    /**
     * 获取 [逾期追款消息]脏标记
     */
    public boolean getOverdue_msgDirtyFlag();
    /**
     * 获取 [纸张格式]
     */
    public void setPaperformat_id(Integer paperformat_id);
    
    /**
     * 设置 [纸张格式]
     */
    public Integer getPaperformat_id();

    /**
     * 获取 [纸张格式]脏标记
     */
    public boolean getPaperformat_idDirtyFlag();
    /**
     * 获取 [上级公司]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级公司]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级公司]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级公司]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级公司]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级公司]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [公司数据库ID]
     */
    public void setPartner_gid(Integer partner_gid);
    
    /**
     * 设置 [公司数据库ID]
     */
    public Integer getPartner_gid();

    /**
     * 获取 [公司数据库ID]脏标记
     */
    public boolean getPartner_gidDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [入职支付收单机构的状态]
     */
    public void setPayment_acquirer_onboarding_state(String payment_acquirer_onboarding_state);
    
    /**
     * 设置 [入职支付收单机构的状态]
     */
    public String getPayment_acquirer_onboarding_state();

    /**
     * 获取 [入职支付收单机构的状态]脏标记
     */
    public boolean getPayment_acquirer_onboarding_stateDirtyFlag();
    /**
     * 获取 [选择付款方式]
     */
    public void setPayment_onboarding_payment_method(String payment_onboarding_payment_method);
    
    /**
     * 设置 [选择付款方式]
     */
    public String getPayment_onboarding_payment_method();

    /**
     * 获取 [选择付款方式]脏标记
     */
    public boolean getPayment_onboarding_payment_methodDirtyFlag();
    /**
     * 获取 [非顾问的锁定日期]
     */
    public void setPeriod_lock_date(Timestamp period_lock_date);
    
    /**
     * 设置 [非顾问的锁定日期]
     */
    public Timestamp getPeriod_lock_date();

    /**
     * 获取 [非顾问的锁定日期]脏标记
     */
    public boolean getPeriod_lock_dateDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPhone(String phone);
    
    /**
     * 设置 [电话]
     */
    public String getPhone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPhoneDirtyFlag();
    /**
     * 获取 [在线支付]
     */
    public void setPortal_confirmation_pay(String portal_confirmation_pay);
    
    /**
     * 设置 [在线支付]
     */
    public String getPortal_confirmation_pay();

    /**
     * 获取 [在线支付]脏标记
     */
    public boolean getPortal_confirmation_payDirtyFlag();
    /**
     * 获取 [在线签名]
     */
    public void setPortal_confirmation_sign(String portal_confirmation_sign);
    
    /**
     * 设置 [在线签名]
     */
    public String getPortal_confirmation_sign();

    /**
     * 获取 [在线签名]脏标记
     */
    public boolean getPortal_confirmation_signDirtyFlag();
    /**
     * 获取 [批准等级]
     */
    public void setPo_double_validation(String po_double_validation);
    
    /**
     * 设置 [批准等级]
     */
    public String getPo_double_validation();

    /**
     * 获取 [批准等级]脏标记
     */
    public boolean getPo_double_validationDirtyFlag();
    /**
     * 获取 [再次验证金额]
     */
    public void setPo_double_validation_amount(Double po_double_validation_amount);
    
    /**
     * 设置 [再次验证金额]
     */
    public Double getPo_double_validation_amount();

    /**
     * 获取 [再次验证金额]脏标记
     */
    public boolean getPo_double_validation_amountDirtyFlag();
    /**
     * 获取 [采购提前时间]
     */
    public void setPo_lead(Double po_lead);
    
    /**
     * 设置 [采购提前时间]
     */
    public Double getPo_lead();

    /**
     * 获取 [采购提前时间]脏标记
     */
    public boolean getPo_leadDirtyFlag();
    /**
     * 获取 [销售订单修改]
     */
    public void setPo_lock(String po_lock);
    
    /**
     * 设置 [销售订单修改]
     */
    public String getPo_lock();

    /**
     * 获取 [销售订单修改]脏标记
     */
    public boolean getPo_lockDirtyFlag();
    /**
     * 获取 [连接在一起的库存移动的日期变化传播的最小差值。]
     */
    public void setPropagation_minimum_delta(Integer propagation_minimum_delta);
    
    /**
     * 设置 [连接在一起的库存移动的日期变化传播的最小差值。]
     */
    public Integer getPropagation_minimum_delta();

    /**
     * 获取 [连接在一起的库存移动的日期变化传播的最小差值。]脏标记
     */
    public boolean getPropagation_minimum_deltaDirtyFlag();
    /**
     * 获取 [库存计价的入库科目]
     */
    public void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);
    
    /**
     * 设置 [库存计价的入库科目]
     */
    public Integer getProperty_stock_account_input_categ_id();

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    public boolean getProperty_stock_account_input_categ_idDirtyFlag();
    /**
     * 获取 [库存计价的入库科目]
     */
    public void setProperty_stock_account_input_categ_id_text(String property_stock_account_input_categ_id_text);
    
    /**
     * 设置 [库存计价的入库科目]
     */
    public String getProperty_stock_account_input_categ_id_text();

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag();
    /**
     * 获取 [库存计价的出货科目]
     */
    public void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);
    
    /**
     * 设置 [库存计价的出货科目]
     */
    public Integer getProperty_stock_account_output_categ_id();

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    public boolean getProperty_stock_account_output_categ_idDirtyFlag();
    /**
     * 获取 [库存计价的出货科目]
     */
    public void setProperty_stock_account_output_categ_id_text(String property_stock_account_output_categ_id_text);
    
    /**
     * 设置 [库存计价的出货科目]
     */
    public String getProperty_stock_account_output_categ_id_text();

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag();
    /**
     * 获取 [库存计价的科目模板]
     */
    public void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);
    
    /**
     * 设置 [库存计价的科目模板]
     */
    public Integer getProperty_stock_valuation_account_id();

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    public boolean getProperty_stock_valuation_account_idDirtyFlag();
    /**
     * 获取 [库存计价的科目模板]
     */
    public void setProperty_stock_valuation_account_id_text(String property_stock_valuation_account_id_text);
    
    /**
     * 设置 [库存计价的科目模板]
     */
    public String getProperty_stock_valuation_account_id_text();

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag();
    /**
     * 获取 [显示SEPA QR码]
     */
    public void setQr_code(String qr_code);
    
    /**
     * 设置 [显示SEPA QR码]
     */
    public String getQr_code();

    /**
     * 获取 [显示SEPA QR码]脏标记
     */
    public boolean getQr_codeDirtyFlag();
    /**
     * 获取 [默认报价有效期（日）]
     */
    public void setQuotation_validity_days(Integer quotation_validity_days);
    
    /**
     * 设置 [默认报价有效期（日）]
     */
    public Integer getQuotation_validity_days();

    /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    public boolean getQuotation_validity_daysDirtyFlag();
    /**
     * 获取 [报表页脚]
     */
    public void setReport_footer(String report_footer);
    
    /**
     * 设置 [报表页脚]
     */
    public String getReport_footer();

    /**
     * 获取 [报表页脚]脏标记
     */
    public boolean getReport_footerDirtyFlag();
    /**
     * 获取 [公司口号]
     */
    public void setReport_header(String report_header);
    
    /**
     * 设置 [公司口号]
     */
    public String getReport_header();

    /**
     * 获取 [公司口号]脏标记
     */
    public boolean getReport_headerDirtyFlag();
    /**
     * 获取 [默认工作时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [默认工作时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [默认工作时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_ids(String resource_calendar_ids);
    
    /**
     * 设置 [工作时间]
     */
    public String getResource_calendar_ids();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_idsDirtyFlag();
    /**
     * 获取 [默认工作时间]
     */
    public void setResource_calendar_id_text(String resource_calendar_id_text);
    
    /**
     * 设置 [默认工作时间]
     */
    public String getResource_calendar_id_text();

    /**
     * 获取 [默认工作时间]脏标记
     */
    public boolean getResource_calendar_id_textDirtyFlag();
    /**
     * 获取 [默认条款和条件]
     */
    public void setSale_note(String sale_note);
    
    /**
     * 设置 [默认条款和条件]
     */
    public String getSale_note();

    /**
     * 获取 [默认条款和条件]脏标记
     */
    public boolean getSale_noteDirtyFlag();
    /**
     * 获取 [有待被确认的订单步骤的状态]
     */
    public void setSale_onboarding_order_confirmation_state(String sale_onboarding_order_confirmation_state);
    
    /**
     * 设置 [有待被确认的订单步骤的状态]
     */
    public String getSale_onboarding_order_confirmation_state();

    /**
     * 获取 [有待被确认的订单步骤的状态]脏标记
     */
    public boolean getSale_onboarding_order_confirmation_stateDirtyFlag();
    /**
     * 获取 [请选择付款方式]
     */
    public void setSale_onboarding_payment_method(String sale_onboarding_payment_method);
    
    /**
     * 设置 [请选择付款方式]
     */
    public String getSale_onboarding_payment_method();

    /**
     * 获取 [请选择付款方式]脏标记
     */
    public boolean getSale_onboarding_payment_methodDirtyFlag();
    /**
     * 获取 [有待被确认的样品报价单步骤的状态]
     */
    public void setSale_onboarding_sample_quotation_state(String sale_onboarding_sample_quotation_state);
    
    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public String getSale_onboarding_sample_quotation_state();

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    public boolean getSale_onboarding_sample_quotation_stateDirtyFlag();
    /**
     * 获取 [正进行销售面板的状态]
     */
    public void setSale_quotation_onboarding_state(String sale_quotation_onboarding_state);
    
    /**
     * 设置 [正进行销售面板的状态]
     */
    public String getSale_quotation_onboarding_state();

    /**
     * 获取 [正进行销售面板的状态]脏标记
     */
    public boolean getSale_quotation_onboarding_stateDirtyFlag();
    /**
     * 获取 [销售安全天数]
     */
    public void setSecurity_lead(Double security_lead);
    
    /**
     * 设置 [销售安全天数]
     */
    public Double getSecurity_lead();

    /**
     * 获取 [销售安全天数]脏标记
     */
    public boolean getSecurity_leadDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setSnailmail_color(String snailmail_color);
    
    /**
     * 设置 [颜色]
     */
    public String getSnailmail_color();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getSnailmail_colorDirtyFlag();
    /**
     * 获取 [双面]
     */
    public void setSnailmail_duplex(String snailmail_duplex);
    
    /**
     * 设置 [双面]
     */
    public String getSnailmail_duplex();

    /**
     * 获取 [双面]脏标记
     */
    public boolean getSnailmail_duplexDirtyFlag();
    /**
     * 获取 [脸书账号]
     */
    public void setSocial_facebook(String social_facebook);
    
    /**
     * 设置 [脸书账号]
     */
    public String getSocial_facebook();

    /**
     * 获取 [脸书账号]脏标记
     */
    public boolean getSocial_facebookDirtyFlag();
    /**
     * 获取 [GitHub账户]
     */
    public void setSocial_github(String social_github);
    
    /**
     * 设置 [GitHub账户]
     */
    public String getSocial_github();

    /**
     * 获取 [GitHub账户]脏标记
     */
    public boolean getSocial_githubDirtyFlag();
    /**
     * 获取 [Google+账户]
     */
    public void setSocial_googleplus(String social_googleplus);
    
    /**
     * 设置 [Google+账户]
     */
    public String getSocial_googleplus();

    /**
     * 获取 [Google+账户]脏标记
     */
    public boolean getSocial_googleplusDirtyFlag();
    /**
     * 获取 [Instagram 账号]
     */
    public void setSocial_instagram(String social_instagram);
    
    /**
     * 设置 [Instagram 账号]
     */
    public String getSocial_instagram();

    /**
     * 获取 [Instagram 账号]脏标记
     */
    public boolean getSocial_instagramDirtyFlag();
    /**
     * 获取 [领英账号]
     */
    public void setSocial_linkedin(String social_linkedin);
    
    /**
     * 设置 [领英账号]
     */
    public String getSocial_linkedin();

    /**
     * 获取 [领英账号]脏标记
     */
    public boolean getSocial_linkedinDirtyFlag();
    /**
     * 获取 [Twitter账号]
     */
    public void setSocial_twitter(String social_twitter);
    
    /**
     * 设置 [Twitter账号]
     */
    public String getSocial_twitter();

    /**
     * 获取 [Twitter账号]脏标记
     */
    public boolean getSocial_twitterDirtyFlag();
    /**
     * 获取 [Youtube账号]
     */
    public void setSocial_youtube(String social_youtube);
    
    /**
     * 设置 [Youtube账号]
     */
    public String getSocial_youtube();

    /**
     * 获取 [Youtube账号]脏标记
     */
    public boolean getSocial_youtubeDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState_id(Integer state_id);
    
    /**
     * 设置 [状态]
     */
    public Integer getState_id();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getState_idDirtyFlag();
    /**
     * 获取 [街道]
     */
    public void setStreet(String street);
    
    /**
     * 设置 [街道]
     */
    public String getStreet();

    /**
     * 获取 [街道]脏标记
     */
    public boolean getStreetDirtyFlag();
    /**
     * 获取 [街道 2]
     */
    public void setStreet2(String street2);
    
    /**
     * 设置 [街道 2]
     */
    public String getStreet2();

    /**
     * 获取 [街道 2]脏标记
     */
    public boolean getStreet2DirtyFlag();
    /**
     * 获取 [税率计算的舍入方法]
     */
    public void setTax_calculation_rounding_method(String tax_calculation_rounding_method);
    
    /**
     * 设置 [税率计算的舍入方法]
     */
    public String getTax_calculation_rounding_method();

    /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    public boolean getTax_calculation_rounding_methodDirtyFlag();
    /**
     * 获取 [现金收付制日记账]
     */
    public void setTax_cash_basis_journal_id(Integer tax_cash_basis_journal_id);
    
    /**
     * 设置 [现金收付制日记账]
     */
    public Integer getTax_cash_basis_journal_id();

    /**
     * 获取 [现金收付制日记账]脏标记
     */
    public boolean getTax_cash_basis_journal_idDirtyFlag();
    /**
     * 获取 [现金收付制日记账]
     */
    public void setTax_cash_basis_journal_id_text(String tax_cash_basis_journal_id_text);
    
    /**
     * 设置 [现金收付制日记账]
     */
    public String getTax_cash_basis_journal_id_text();

    /**
     * 获取 [现金收付制日记账]脏标记
     */
    public boolean getTax_cash_basis_journal_id_textDirtyFlag();
    /**
     * 获取 [使用现金收付制]
     */
    public void setTax_exigibility(String tax_exigibility);
    
    /**
     * 设置 [使用现金收付制]
     */
    public String getTax_exigibility();

    /**
     * 获取 [使用现金收付制]脏标记
     */
    public boolean getTax_exigibilityDirtyFlag();
    /**
     * 获取 [转账帐户的前缀]
     */
    public void setTransfer_account_code_prefix(String transfer_account_code_prefix);
    
    /**
     * 设置 [转账帐户的前缀]
     */
    public String getTransfer_account_code_prefix();

    /**
     * 获取 [转账帐户的前缀]脏标记
     */
    public boolean getTransfer_account_code_prefixDirtyFlag();
    /**
     * 获取 [银行间转账科目]
     */
    public void setTransfer_account_id(Integer transfer_account_id);
    
    /**
     * 设置 [银行间转账科目]
     */
    public Integer getTransfer_account_id();

    /**
     * 获取 [银行间转账科目]脏标记
     */
    public boolean getTransfer_account_idDirtyFlag();
    /**
     * 获取 [银行间转账科目]
     */
    public void setTransfer_account_id_text(String transfer_account_id_text);
    
    /**
     * 设置 [银行间转账科目]
     */
    public String getTransfer_account_id_text();

    /**
     * 获取 [银行间转账科目]脏标记
     */
    public boolean getTransfer_account_id_textDirtyFlag();
    /**
     * 获取 [接受的用户]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [接受的用户]
     */
    public String getUser_ids();

    /**
     * 获取 [接受的用户]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [税号]
     */
    public void setVat(String vat);
    
    /**
     * 设置 [税号]
     */
    public String getVat();

    /**
     * 获取 [税号]脏标记
     */
    public boolean getVatDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite(String website);
    
    /**
     * 设置 [网站]
     */
    public String getWebsite();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsiteDirtyFlag();
    /**
     * 获取 [网站销售状态入职付款收单机构步骤]
     */
    public void setWebsite_sale_onboarding_payment_acquirer_state(String website_sale_onboarding_payment_acquirer_state);
    
    /**
     * 设置 [网站销售状态入职付款收单机构步骤]
     */
    public String getWebsite_sale_onboarding_payment_acquirer_state();

    /**
     * 获取 [网站销售状态入职付款收单机构步骤]脏标记
     */
    public boolean getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag();
    /**
     * 获取 [招聘网站主题一步完成]
     */
    public void setWebsite_theme_onboarding_done(String website_theme_onboarding_done);
    
    /**
     * 设置 [招聘网站主题一步完成]
     */
    public String getWebsite_theme_onboarding_done();

    /**
     * 获取 [招聘网站主题一步完成]脏标记
     */
    public boolean getWebsite_theme_onboarding_doneDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [邮政编码]
     */
    public void setZip(String zip);
    
    /**
     * 设置 [邮政编码]
     */
    public String getZip();

    /**
     * 获取 [邮政编码]脏标记
     */
    public boolean getZipDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

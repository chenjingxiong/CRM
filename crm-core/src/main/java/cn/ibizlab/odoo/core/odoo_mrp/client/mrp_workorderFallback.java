package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
@Component
public class mrp_workorderFallback implements mrp_workorderFeignClient{



    public Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context){
            return null;
     }


    public Mrp_workorder create(Mrp_workorder mrp_workorder){
            return null;
     }
    public Boolean createBatch(List<Mrp_workorder> mrp_workorders){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_workorder update(Integer id, Mrp_workorder mrp_workorder){
            return null;
     }
    public Boolean updateBatch(List<Mrp_workorder> mrp_workorders){
            return false;
     }


    public Mrp_workorder get(Integer id){
            return null;
     }


    public Page<Mrp_workorder> select(){
            return null;
     }

    public Mrp_workorder getDraft(){
            return null;
    }



}

package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_abstract_payment] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-abstract-payment", fallback = account_abstract_paymentFallback.class)
public interface account_abstract_paymentFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/searchdefault")
    Page<Account_abstract_payment> searchDefault(@RequestBody Account_abstract_paymentSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments")
    Account_abstract_payment create(@RequestBody Account_abstract_payment account_abstract_payment);

    @RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/batch")
    Boolean createBatch(@RequestBody List<Account_abstract_payment> account_abstract_payments);



    @RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/{id}")
    Account_abstract_payment get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/{id}")
    Account_abstract_payment update(@PathVariable("id") Integer id,@RequestBody Account_abstract_payment account_abstract_payment);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/batch")
    Boolean updateBatch(@RequestBody List<Account_abstract_payment> account_abstract_payments);



    @RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/select")
    Page<Account_abstract_payment> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/getdraft")
    Account_abstract_payment getDraft();


}

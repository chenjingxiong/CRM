package cn.ibizlab.odoo.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[asset_state] 服务对象接口
 */
@FeignClient(value = "odoo-asset", contextId = "asset-state", fallback = asset_stateFallback.class)
public interface asset_stateFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/asset_states/{id}")
    Asset_state get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/asset_states")
    Asset_state create(@RequestBody Asset_state asset_state);

    @RequestMapping(method = RequestMethod.POST, value = "/asset_states/batch")
    Boolean createBatch(@RequestBody List<Asset_state> asset_states);



    @RequestMapping(method = RequestMethod.POST, value = "/asset_states/searchdefault")
    Page<Asset_state> searchDefault(@RequestBody Asset_stateSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/asset_states/{id}")
    Asset_state update(@PathVariable("id") Integer id,@RequestBody Asset_state asset_state);

    @RequestMapping(method = RequestMethod.PUT, value = "/asset_states/batch")
    Boolean updateBatch(@RequestBody List<Asset_state> asset_states);


    @RequestMapping(method = RequestMethod.GET, value = "/asset_states/select")
    Page<Asset_state> select();


    @RequestMapping(method = RequestMethod.GET, value = "/asset_states/getdraft")
    Asset_state getDraft();


}

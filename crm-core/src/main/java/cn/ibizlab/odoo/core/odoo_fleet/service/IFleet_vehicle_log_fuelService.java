package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;


/**
 * 实体[Fleet_vehicle_log_fuel] 服务对象接口
 */
public interface IFleet_vehicle_log_fuelService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Fleet_vehicle_log_fuel et) ;
    void createBatch(List<Fleet_vehicle_log_fuel> list) ;
    Fleet_vehicle_log_fuel getDraft(Fleet_vehicle_log_fuel et) ;
    boolean update(Fleet_vehicle_log_fuel et) ;
    void updateBatch(List<Fleet_vehicle_log_fuel> list) ;
    Fleet_vehicle_log_fuel get(Integer key) ;
    Page<Fleet_vehicle_log_fuel> searchDefault(Fleet_vehicle_log_fuelSearchContext context) ;

}




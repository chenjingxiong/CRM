package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_mail] 服务对象接口
 */
@Component
public class event_mailFallback implements event_mailFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Event_mail create(Event_mail event_mail){
            return null;
     }
    public Boolean createBatch(List<Event_mail> event_mails){
            return false;
     }



    public Page<Event_mail> searchDefault(Event_mailSearchContext context){
            return null;
     }


    public Event_mail get(Integer id){
            return null;
     }


    public Event_mail update(Integer id, Event_mail event_mail){
            return null;
     }
    public Boolean updateBatch(List<Event_mail> event_mails){
            return false;
     }


    public Page<Event_mail> select(){
            return null;
     }

    public Event_mail getDraft(){
            return null;
    }



}

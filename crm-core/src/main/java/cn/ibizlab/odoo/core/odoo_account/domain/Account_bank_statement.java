package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [银行对账单] 对象
 */
@Data
public class Account_bank_statement extends EntityClient implements Serializable {

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 分录明细行
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 参考
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 结束余额
     */
    @DEField(name = "balance_end_real")
    @JSONField(name = "balance_end_real")
    @JsonProperty("balance_end_real")
    private Double balanceEndReal;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 会话
     */
    @DEField(name = "pos_session_id")
    @JSONField(name = "pos_session_id")
    @JsonProperty("pos_session_id")
    private Integer posSessionId;

    /**
     * 计算余额
     */
    @DEField(name = "balance_end")
    @JSONField(name = "balance_end")
    @JsonProperty("balance_end")
    private Double balanceEnd;

    /**
     * 交易小计
     */
    @DEField(name = "total_entry_encoding")
    @JSONField(name = "total_entry_encoding")
    @JsonProperty("total_entry_encoding")
    private Double totalEntryEncoding;

    /**
     * 关闭在
     */
    @DEField(name = "date_done")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_done" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_done")
    private Timestamp dateDone;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 所有的明细行都已核销
     */
    @JSONField(name = "all_lines_reconciled")
    @JsonProperty("all_lines_reconciled")
    private String allLinesReconciled;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 操作编号
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 对账单明细行
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 凭证明细数
     */
    @JSONField(name = "move_line_count")
    @JsonProperty("move_line_count")
    private Integer moveLineCount;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 是0
     */
    @JSONField(name = "is_difference_zero")
    @JsonProperty("is_difference_zero")
    private String isDifferenceZero;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 需要一个动作消息的编码
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 差异
     */
    @JSONField(name = "difference")
    @JsonProperty("difference")
    private Double difference;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 外部引用
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 起始余额
     */
    @DEField(name = "balance_start")
    @JSONField(name = "balance_start")
    @JsonProperty("balance_start")
    private Double balanceStart;

    /**
     * 会计日期
     */
    @DEField(name = "accounting_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "accounting_date" , format="yyyy-MM-dd")
    @JsonProperty("accounting_date")
    private Timestamp accountingDate;

    /**
     * 负责人
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 类型
     */
    @JSONField(name = "journal_type")
    @JsonProperty("journal_type")
    private String journalType;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 默认借方科目
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 开始钱箱
     */
    @DEField(name = "cashbox_start_id")
    @JSONField(name = "cashbox_start_id")
    @JsonProperty("cashbox_start_id")
    private Integer cashboxStartId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 关闭钱箱
     */
    @DEField(name = "cashbox_end_id")
    @JSONField(name = "cashbox_end_id")
    @JsonProperty("cashbox_end_id")
    private Integer cashboxEndId;

    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocashboxend")
    @JsonProperty("odoocashboxend")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox odooCashboxEnd;

    /**
     * 
     */
    @JSONField(name = "odoocashboxstart")
    @JsonProperty("odoocashboxstart")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox odooCashboxStart;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [参考]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [结束余额]
     */
    public void setBalanceEndReal(Double balanceEndReal){
        this.balanceEndReal = balanceEndReal ;
        this.modify("balance_end_real",balanceEndReal);
    }
    /**
     * 设置 [会话]
     */
    public void setPosSessionId(Integer posSessionId){
        this.posSessionId = posSessionId ;
        this.modify("pos_session_id",posSessionId);
    }
    /**
     * 设置 [计算余额]
     */
    public void setBalanceEnd(Double balanceEnd){
        this.balanceEnd = balanceEnd ;
        this.modify("balance_end",balanceEnd);
    }
    /**
     * 设置 [交易小计]
     */
    public void setTotalEntryEncoding(Double totalEntryEncoding){
        this.totalEntryEncoding = totalEntryEncoding ;
        this.modify("total_entry_encoding",totalEntryEncoding);
    }
    /**
     * 设置 [关闭在]
     */
    public void setDateDone(Timestamp dateDone){
        this.dateDone = dateDone ;
        this.modify("date_done",dateDone);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [差异]
     */
    public void setDifference(Double difference){
        this.difference = difference ;
        this.modify("difference",difference);
    }
    /**
     * 设置 [外部引用]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }
    /**
     * 设置 [起始余额]
     */
    public void setBalanceStart(Double balanceStart){
        this.balanceStart = balanceStart ;
        this.modify("balance_start",balanceStart);
    }
    /**
     * 设置 [会计日期]
     */
    public void setAccountingDate(Timestamp accountingDate){
        this.accountingDate = accountingDate ;
        this.modify("accounting_date",accountingDate);
    }
    /**
     * 设置 [开始钱箱]
     */
    public void setCashboxStartId(Integer cashboxStartId){
        this.cashboxStartId = cashboxStartId ;
        this.modify("cashbox_start_id",cashboxStartId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [关闭钱箱]
     */
    public void setCashboxEndId(Integer cashboxEndId){
        this.cashboxEndId = cashboxEndId ;
        this.modify("cashbox_end_id",cashboxEndId);
    }
    /**
     * 设置 [负责人]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

}



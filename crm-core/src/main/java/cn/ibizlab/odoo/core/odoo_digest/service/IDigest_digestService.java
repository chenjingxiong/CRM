package cn.ibizlab.odoo.core.odoo_digest.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;


/**
 * 实体[Digest_digest] 服务对象接口
 */
public interface IDigest_digestService{

    boolean create(Digest_digest et) ;
    void createBatch(List<Digest_digest> list) ;
    Digest_digest get(Integer key) ;
    Digest_digest getDraft(Digest_digest et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Digest_digest et) ;
    void updateBatch(List<Digest_digest> list) ;
    Page<Digest_digest> searchDefault(Digest_digestSearchContext context) ;

}




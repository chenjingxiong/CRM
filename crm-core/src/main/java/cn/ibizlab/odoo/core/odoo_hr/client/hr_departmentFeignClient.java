package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_department] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-department", fallback = hr_departmentFallback.class)
public interface hr_departmentFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/{id}")
    Hr_department get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/{id}")
    Hr_department update(@PathVariable("id") Integer id,@RequestBody Hr_department hr_department);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/batch")
    Boolean updateBatch(@RequestBody List<Hr_department> hr_departments);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments")
    Hr_department create(@RequestBody Hr_department hr_department);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/batch")
    Boolean createBatch(@RequestBody List<Hr_department> hr_departments);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/searchdefault")
    Page<Hr_department> searchDefault(@RequestBody Hr_departmentSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/select")
    Page<Hr_department> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/getdraft")
    Hr_department getDraft();


}

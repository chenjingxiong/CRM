package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_mass_mailing_list_contact_rel] 对象
 */
public interface Imail_mass_mailing_list_contact_rel {

    /**
     * 获取 [联系人人数]
     */
    public void setContact_count(Integer contact_count);
    
    /**
     * 设置 [联系人人数]
     */
    public Integer getContact_count();

    /**
     * 获取 [联系人人数]脏标记
     */
    public boolean getContact_countDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setContact_id(Integer contact_id);
    
    /**
     * 设置 [联系]
     */
    public Integer getContact_id();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getContact_idDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setContact_id_text(String contact_id_text);
    
    /**
     * 设置 [联系]
     */
    public String getContact_id_text();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getContact_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [黑名单]
     */
    public void setIs_blacklisted(String is_blacklisted);
    
    /**
     * 设置 [黑名单]
     */
    public String getIs_blacklisted();

    /**
     * 获取 [黑名单]脏标记
     */
    public boolean getIs_blacklistedDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setList_id(Integer list_id);
    
    /**
     * 设置 [邮件列表]
     */
    public Integer getList_id();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getList_idDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setList_id_text(String list_id_text);
    
    /**
     * 设置 [邮件列表]
     */
    public String getList_id_text();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getList_id_textDirtyFlag();
    /**
     * 获取 [被退回]
     */
    public void setMessage_bounce(Integer message_bounce);
    
    /**
     * 设置 [被退回]
     */
    public Integer getMessage_bounce();

    /**
     * 获取 [被退回]脏标记
     */
    public boolean getMessage_bounceDirtyFlag();
    /**
     * 获取 [退出]
     */
    public void setOpt_out(String opt_out);
    
    /**
     * 设置 [退出]
     */
    public String getOpt_out();

    /**
     * 获取 [退出]脏标记
     */
    public boolean getOpt_outDirtyFlag();
    /**
     * 获取 [取消订阅日期]
     */
    public void setUnsubscription_date(Timestamp unsubscription_date);
    
    /**
     * 设置 [取消订阅日期]
     */
    public Timestamp getUnsubscription_date();

    /**
     * 获取 [取消订阅日期]脏标记
     */
    public boolean getUnsubscription_dateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

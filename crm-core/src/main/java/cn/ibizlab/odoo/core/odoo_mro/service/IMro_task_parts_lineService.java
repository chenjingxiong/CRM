package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;


/**
 * 实体[Mro_task_parts_line] 服务对象接口
 */
public interface IMro_task_parts_lineService{

    Mro_task_parts_line getDraft(Mro_task_parts_line et) ;
    boolean update(Mro_task_parts_line et) ;
    void updateBatch(List<Mro_task_parts_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mro_task_parts_line et) ;
    void createBatch(List<Mro_task_parts_line> list) ;
    Mro_task_parts_line get(Integer key) ;
    Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context) ;

}




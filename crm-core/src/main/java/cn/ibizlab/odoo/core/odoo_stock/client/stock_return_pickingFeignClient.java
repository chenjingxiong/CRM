package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_return_picking] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-return-picking", fallback = stock_return_pickingFallback.class)
public interface stock_return_pickingFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_pickings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_pickings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/searchdefault")
    Page<Stock_return_picking> searchDefault(@RequestBody Stock_return_pickingSearchContext context);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings")
    Stock_return_picking create(@RequestBody Stock_return_picking stock_return_picking);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/batch")
    Boolean createBatch(@RequestBody List<Stock_return_picking> stock_return_pickings);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_return_pickings/{id}")
    Stock_return_picking update(@PathVariable("id") Integer id,@RequestBody Stock_return_picking stock_return_picking);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_return_pickings/batch")
    Boolean updateBatch(@RequestBody List<Stock_return_picking> stock_return_pickings);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_pickings/{id}")
    Stock_return_picking get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_pickings/select")
    Page<Stock_return_picking> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_return_pickings/getdraft")
    Stock_return_picking getDraft();


}

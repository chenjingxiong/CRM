package cn.ibizlab.odoo.core.odoo_crm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Crm_lead] 查询条件对象
 */
@Slf4j
@Data
public class Crm_leadSearchContext extends SearchContextBase {
	private String n_name_like;//[商机]

	private String n_activity_state_eq;//[活动状态]

	private String n_type_eq;//[类型]

	private String n_kanban_state_eq;//[看板状态]

	private String n_priority_eq;//[优先级]

	private String n_user_id_text_eq;//[销售员]

	private String n_user_id_text_like;//[销售员]

	private String n_source_id_text_eq;//[来源]

	private String n_source_id_text_like;//[来源]

	private String n_partner_address_name_eq;//[业务伙伴联系姓名]

	private String n_partner_address_name_like;//[业务伙伴联系姓名]

	private String n_medium_id_text_eq;//[媒介]

	private String n_medium_id_text_like;//[媒介]

	private String n_team_id_text_eq;//[销售团队]

	private String n_team_id_text_like;//[销售团队]

	private String n_write_uid_text_eq;//[最后更新]

	private String n_write_uid_text_like;//[最后更新]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_state_id_text_eq;//[省份]

	private String n_state_id_text_like;//[省份]

	private String n_campaign_id_text_eq;//[营销]

	private String n_campaign_id_text_like;//[营销]

	private String n_stage_id_text_eq;//[阶段]

	private String n_stage_id_text_like;//[阶段]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_country_id_text_eq;//[国家]

	private String n_country_id_text_like;//[国家]

	private String n_title_text_eq;//[称谓]

	private String n_title_text_like;//[称谓]

	private String n_lost_reason_text_eq;//[失去原因]

	private String n_lost_reason_text_like;//[失去原因]

	private Integer n_lost_reason_eq;//[失去原因]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_write_uid_eq;//[最后更新]

	private Integer n_company_id_eq;//[公司]

	private Integer n_user_id_eq;//[销售员]

	private Integer n_state_id_eq;//[省份]

	private Integer n_medium_id_eq;//[媒介]

	private Integer n_stage_id_eq;//[阶段]

	private Integer n_source_id_eq;//[来源]

	private Integer n_country_id_eq;//[国家]

	private Integer n_campaign_id_eq;//[营销]

	private Integer n_partner_id_eq;//[客户]

	private Integer n_team_id_eq;//[销售团队]

	private Integer n_title_eq;//[称谓]

}




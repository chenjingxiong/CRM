package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_config] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-config", fallback = res_configFallback.class)
public interface res_configFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_configs")
    Res_config create(@RequestBody Res_config res_config);

    @RequestMapping(method = RequestMethod.POST, value = "/res_configs/batch")
    Boolean createBatch(@RequestBody List<Res_config> res_configs);



    @RequestMapping(method = RequestMethod.GET, value = "/res_configs/{id}")
    Res_config get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_configs/{id}")
    Res_config update(@PathVariable("id") Integer id,@RequestBody Res_config res_config);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_configs/batch")
    Boolean updateBatch(@RequestBody List<Res_config> res_configs);



    @RequestMapping(method = RequestMethod.POST, value = "/res_configs/searchdefault")
    Page<Res_config> searchDefault(@RequestBody Res_configSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/res_configs/select")
    Page<Res_config> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_configs/getdraft")
    Res_config getDraft();


}

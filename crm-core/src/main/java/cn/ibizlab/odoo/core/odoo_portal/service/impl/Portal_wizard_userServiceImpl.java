package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizard_userService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_portal.client.portal_wizard_userFeignClient;

/**
 * 实体[门户用户配置] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_wizard_userServiceImpl implements IPortal_wizard_userService {

    @Autowired
    portal_wizard_userFeignClient portal_wizard_userFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=portal_wizard_userFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        portal_wizard_userFeignClient.removeBatch(idList);
    }

    @Override
    public Portal_wizard_user getDraft(Portal_wizard_user et) {
        et=portal_wizard_userFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Portal_wizard_user et) {
        Portal_wizard_user rt = portal_wizard_userFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_wizard_user> list){
        portal_wizard_userFeignClient.createBatch(list) ;
    }

    @Override
    public Portal_wizard_user get(Integer id) {
		Portal_wizard_user et=portal_wizard_userFeignClient.get(id);
        if(et==null){
            et=new Portal_wizard_user();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Portal_wizard_user et) {
        Portal_wizard_user rt = portal_wizard_userFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Portal_wizard_user> list){
        portal_wizard_userFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context) {
        Page<Portal_wizard_user> portal_wizard_users=portal_wizard_userFeignClient.searchDefault(context);
        return portal_wizard_users;
    }


}



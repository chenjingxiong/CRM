package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_automation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_automation] 服务对象接口
 */
public interface Ibase_automationClientService{

    public Ibase_automation createModel() ;

    public void removeBatch(List<Ibase_automation> base_automations);

    public void updateBatch(List<Ibase_automation> base_automations);

    public void createBatch(List<Ibase_automation> base_automations);

    public void create(Ibase_automation base_automation);

    public void get(Ibase_automation base_automation);

    public void remove(Ibase_automation base_automation);

    public void update(Ibase_automation base_automation);

    public Page<Ibase_automation> fetchDefault(SearchContext context);

    public Page<Ibase_automation> select(SearchContext context);

    public void getDraft(Ibase_automation base_automation);

}

package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imaintenance_request;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_request] 服务对象接口
 */
public interface Imaintenance_requestClientService{

    public Imaintenance_request createModel() ;

    public void updateBatch(List<Imaintenance_request> maintenance_requests);

    public void create(Imaintenance_request maintenance_request);

    public void remove(Imaintenance_request maintenance_request);

    public void createBatch(List<Imaintenance_request> maintenance_requests);

    public void get(Imaintenance_request maintenance_request);

    public Page<Imaintenance_request> fetchDefault(SearchContext context);

    public void update(Imaintenance_request maintenance_request);

    public void removeBatch(List<Imaintenance_request> maintenance_requests);

    public Page<Imaintenance_request> select(SearchContext context);

    public void getDraft(Imaintenance_request maintenance_request);

}

package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_activity_report;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_activity_report.Crm_activity_reportSubjectDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_activity_report
 * 属性：Subject
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_activity_reportSubjectDefaultValidator.class})
public @interface Crm_activity_reportSubjectDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

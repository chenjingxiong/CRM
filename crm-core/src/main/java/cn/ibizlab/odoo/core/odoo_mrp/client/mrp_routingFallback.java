package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_routing] 服务对象接口
 */
@Component
public class mrp_routingFallback implements mrp_routingFeignClient{

    public Mrp_routing update(Integer id, Mrp_routing mrp_routing){
            return null;
     }
    public Boolean updateBatch(List<Mrp_routing> mrp_routings){
            return false;
     }


    public Mrp_routing get(Integer id){
            return null;
     }


    public Mrp_routing create(Mrp_routing mrp_routing){
            return null;
     }
    public Boolean createBatch(List<Mrp_routing> mrp_routings){
            return false;
     }

    public Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mrp_routing> select(){
            return null;
     }

    public Mrp_routing getDraft(){
            return null;
    }



}

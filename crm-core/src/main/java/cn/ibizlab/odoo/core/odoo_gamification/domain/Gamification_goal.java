package cn.ibizlab.odoo.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [游戏化目标] 对象
 */
@Data
public class Gamification_goal extends EntityClient implements Serializable {

    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 更新
     */
    @DEField(name = "to_update")
    @JSONField(name = "to_update")
    @JsonProperty("to_update")
    private String toUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 提醒延迟
     */
    @DEField(name = "remind_update_delay")
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 完整性
     */
    @JSONField(name = "completeness")
    @JsonProperty("completeness")
    private Double completeness;

    /**
     * 达到
     */
    @DEField(name = "target_goal")
    @JSONField(name = "target_goal")
    @JsonProperty("target_goal")
    private Double targetGoal;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 关闭的目标
     */
    @JSONField(name = "closed")
    @JsonProperty("closed")
    private String closed;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最近更新
     */
    @DEField(name = "last_update")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_update" , format="yyyy-MM-dd")
    @JsonProperty("last_update")
    private Timestamp lastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 结束日期
     */
    @DEField(name = "end_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 当前值
     */
    @JSONField(name = "current")
    @JsonProperty("current")
    private Double current;

    /**
     * 定义说明
     */
    @JSONField(name = "definition_description")
    @JsonProperty("definition_description")
    private String definitionDescription;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 后缀
     */
    @JSONField(name = "definition_suffix")
    @JsonProperty("definition_suffix")
    private String definitionSuffix;

    /**
     * 挑战
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    private String challengeIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 目标定义
     */
    @JSONField(name = "definition_id_text")
    @JsonProperty("definition_id_text")
    private String definitionIdText;

    /**
     * 显示为
     */
    @JSONField(name = "definition_display")
    @JsonProperty("definition_display")
    private String definitionDisplay;

    /**
     * 挑战行
     */
    @JSONField(name = "line_id_text")
    @JsonProperty("line_id_text")
    private String lineIdText;

    /**
     * 用户
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 目标绩效
     */
    @JSONField(name = "definition_condition")
    @JsonProperty("definition_condition")
    private String definitionCondition;

    /**
     * 计算模式
     */
    @JSONField(name = "computation_mode")
    @JsonProperty("computation_mode")
    private String computationMode;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 挑战行
     */
    @DEField(name = "line_id")
    @JSONField(name = "line_id")
    @JsonProperty("line_id")
    private Integer lineId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 目标定义
     */
    @DEField(name = "definition_id")
    @JSONField(name = "definition_id")
    @JsonProperty("definition_id")
    private Integer definitionId;

    /**
     * 挑战
     */
    @DEField(name = "challenge_id")
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    private Integer challengeId;

    /**
     * 用户
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;


    /**
     * 
     */
    @JSONField(name = "odooline")
    @JsonProperty("odooline")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line odooLine;

    /**
     * 
     */
    @JSONField(name = "odoochallenge")
    @JsonProperty("odoochallenge")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge odooChallenge;

    /**
     * 
     */
    @JSONField(name = "odoodefinition")
    @JsonProperty("odoodefinition")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition odooDefinition;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }
    /**
     * 设置 [更新]
     */
    public void setToUpdate(String toUpdate){
        this.toUpdate = toUpdate ;
        this.modify("to_update",toUpdate);
    }
    /**
     * 设置 [提醒延迟]
     */
    public void setRemindUpdateDelay(Integer remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [达到]
     */
    public void setTargetGoal(Double targetGoal){
        this.targetGoal = targetGoal ;
        this.modify("target_goal",targetGoal);
    }
    /**
     * 设置 [关闭的目标]
     */
    public void setClosed(String closed){
        this.closed = closed ;
        this.modify("closed",closed);
    }
    /**
     * 设置 [最近更新]
     */
    public void setLastUpdate(Timestamp lastUpdate){
        this.lastUpdate = lastUpdate ;
        this.modify("last_update",lastUpdate);
    }
    /**
     * 设置 [结束日期]
     */
    public void setEndDate(Timestamp endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }
    /**
     * 设置 [当前值]
     */
    public void setCurrent(Double current){
        this.current = current ;
        this.modify("current",current);
    }
    /**
     * 设置 [挑战行]
     */
    public void setLineId(Integer lineId){
        this.lineId = lineId ;
        this.modify("line_id",lineId);
    }
    /**
     * 设置 [目标定义]
     */
    public void setDefinitionId(Integer definitionId){
        this.definitionId = definitionId ;
        this.modify("definition_id",definitionId);
    }
    /**
     * 设置 [挑战]
     */
    public void setChallengeId(Integer challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }
    /**
     * 设置 [用户]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

}



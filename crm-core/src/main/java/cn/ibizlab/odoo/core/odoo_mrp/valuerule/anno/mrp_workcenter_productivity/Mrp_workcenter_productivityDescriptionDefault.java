package cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workcenter_productivity;

import cn.ibizlab.odoo.core.odoo_mrp.valuerule.validator.mrp_workcenter_productivity.Mrp_workcenter_productivityDescriptionDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mrp_workcenter_productivity
 * 属性：Description
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mrp_workcenter_productivityDescriptionDefaultValidator.class})
public @interface Mrp_workcenter_productivityDescriptionDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

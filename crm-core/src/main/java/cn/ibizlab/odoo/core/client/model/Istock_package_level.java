package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_package_level] 对象
 */
public interface Istock_package_level {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [完成]
     */
    public void setIs_done(String is_done);
    
    /**
     * 设置 [完成]
     */
    public String getIs_done();

    /**
     * 获取 [完成]脏标记
     */
    public boolean getIs_doneDirtyFlag();
    /**
     * 获取 [是新鲜包裹]
     */
    public void setIs_fresh_package(String is_fresh_package);
    
    /**
     * 设置 [是新鲜包裹]
     */
    public String getIs_fresh_package();

    /**
     * 获取 [是新鲜包裹]脏标记
     */
    public boolean getIs_fresh_packageDirtyFlag();
    /**
     * 获取 [至]
     */
    public void setLocation_dest_id(Integer location_dest_id);
    
    /**
     * 设置 [至]
     */
    public Integer getLocation_dest_id();

    /**
     * 获取 [至]脏标记
     */
    public boolean getLocation_dest_idDirtyFlag();
    /**
     * 获取 [至]
     */
    public void setLocation_dest_id_text(String location_dest_id_text);
    
    /**
     * 设置 [至]
     */
    public String getLocation_dest_id_text();

    /**
     * 获取 [至]脏标记
     */
    public boolean getLocation_dest_id_textDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [从]
     */
    public Integer getLocation_id();

    /**
     * 获取 [从]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [移动]
     */
    public void setMove_ids(String move_ids);
    
    /**
     * 设置 [移动]
     */
    public String getMove_ids();

    /**
     * 获取 [移动]脏标记
     */
    public boolean getMove_idsDirtyFlag();
    /**
     * 获取 [凭证明细]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [凭证明细]
     */
    public String getMove_line_ids();

    /**
     * 获取 [凭证明细]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id(Integer package_id);
    
    /**
     * 设置 [包裹]
     */
    public Integer getPackage_id();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_idDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setPackage_id_text(String package_id_text);
    
    /**
     * 设置 [包裹]
     */
    public String getPackage_id_text();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getPackage_id_textDirtyFlag();
    /**
     * 获取 [分拣]
     */
    public void setPicking_id(Integer picking_id);
    
    /**
     * 设置 [分拣]
     */
    public Integer getPicking_id();

    /**
     * 获取 [分拣]脏标记
     */
    public boolean getPicking_idDirtyFlag();
    /**
     * 获取 [分拣]
     */
    public void setPicking_id_text(String picking_id_text);
    
    /**
     * 设置 [分拣]
     */
    public String getPicking_id_text();

    /**
     * 获取 [分拣]脏标记
     */
    public boolean getPicking_id_textDirtyFlag();
    /**
     * 获取 [源位置]
     */
    public void setPicking_source_location(Integer picking_source_location);
    
    /**
     * 设置 [源位置]
     */
    public Integer getPicking_source_location();

    /**
     * 获取 [源位置]脏标记
     */
    public boolean getPicking_source_locationDirtyFlag();
    /**
     * 获取 [显示批次M2O]
     */
    public void setShow_lots_m2o(String show_lots_m2o);
    
    /**
     * 设置 [显示批次M2O]
     */
    public String getShow_lots_m2o();

    /**
     * 获取 [显示批次M2O]脏标记
     */
    public boolean getShow_lots_m2oDirtyFlag();
    /**
     * 获取 [显示批次文本]
     */
    public void setShow_lots_text(String show_lots_text);
    
    /**
     * 设置 [显示批次文本]
     */
    public String getShow_lots_text();

    /**
     * 获取 [显示批次文本]脏标记
     */
    public boolean getShow_lots_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}

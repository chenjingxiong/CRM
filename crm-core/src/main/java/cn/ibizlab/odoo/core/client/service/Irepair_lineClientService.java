package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irepair_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_line] 服务对象接口
 */
public interface Irepair_lineClientService{

    public Irepair_line createModel() ;

    public void updateBatch(List<Irepair_line> repair_lines);

    public void createBatch(List<Irepair_line> repair_lines);

    public Page<Irepair_line> fetchDefault(SearchContext context);

    public void get(Irepair_line repair_line);

    public void update(Irepair_line repair_line);

    public void removeBatch(List<Irepair_line> repair_lines);

    public void create(Irepair_line repair_line);

    public void remove(Irepair_line repair_line);

    public Page<Irepair_line> select(SearchContext context);

    public void getDraft(Irepair_line repair_line);

}

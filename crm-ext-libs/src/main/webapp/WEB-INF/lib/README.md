## 私有组件引入说明
1. 将私有组件上传至<项目>-ext-libs/src/main/webapp/WEB-INF/lib/下
    例如
    <项目>-ext-libs/src/main/webapp/WEB-INF/lib/mail.jar
2. 修改<项目>-ext-libs/pom.xml，在<dependencies>中添加组件相关内容，且<systemPath>指定为步骤一的路径，<version>为必须的（没有可以自填一个）。
    例如
    <项目>-ext-libs/pom.xml
    ```xml
        <dependencyManagement>
            <dependencies>
                <dependency>
                    <groupId>com.sun.mail</groupId>
                    <artifactId>javax.mail</artifactId>
                    <systemPath>${project.basedir}/src/main/webapp/WEB-INF/lib/mail.jar</systemPath>
                    <version>1.4.7</version>
                </dependency>
            </dependencies>
        </dependencyManagement>
    ```
3. 在需要引入该组件的地方（一般为usr部分），引入依赖
    例如
    <项目>-usr/pom.xml
    ```xml
        <dependencies>
            <dependency>
                <groupId>com.sun.mail</groupId>
                <artifactId>javax.mail</artifactId>
                <version>1.4.7</version>
            </dependency>
        </dependencies>
    ```

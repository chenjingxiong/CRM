package cn.ibizlab.odoo.web.security.permission.odoo_base;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_base.service.Res_partner_pService;
import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner_p;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("res_partner_p_pms")
public class Res_partner_pPermission {

    @Autowired
    Res_partner_pService res_partner_pservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer res_partner_p_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  Integer res_partner_p_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  String action){
        return true ;
    }


}

package cn.ibizlab.odoo.web.odoo_product.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.web.odoo_product.filter.*;
import cn.ibizlab.odoo.web.odoo_product.feign.Product_price_listFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Product_price_listService {

    Product_price_listFeignClient client;

    @Autowired
    public Product_price_listService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_price_listFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_price_listFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer product_price_list_id) {
        return client.remove( product_price_list_id);
    }

    public Product_price_list update(Integer product_price_list_id, Product_price_list product_price_list) {
        return client.update(product_price_list_id, product_price_list);
    }

    public Product_price_list updateBatch(Integer product_price_list_id, Product_price_list product_price_list) {
        return client.updateBatch(product_price_list_id, product_price_list);
    }

	public Product_price_list create(Product_price_list product_price_list) {
        return client.create(product_price_list);
    }

    public Product_price_list get( Integer product_price_list_id) {
        return client.get( product_price_list_id);
    }

    public Product_price_list getDraft(Integer product_price_list_id, Product_price_list product_price_list) {
        return client.getDraft(product_price_list_id, product_price_list);
    }

    public Product_price_list removeBatch(Integer product_price_list_id, Product_price_list product_price_list) {
        return client.removeBatch(product_price_list_id, product_price_list);
    }

    public Product_price_list createBatch(Integer product_price_list_id, Product_price_list product_price_list) {
        return client.createBatch(product_price_list_id, product_price_list);
    }

	public Page<Product_price_list> fetchDefault(Product_price_listSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

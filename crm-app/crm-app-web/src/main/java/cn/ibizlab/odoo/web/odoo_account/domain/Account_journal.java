package cn.ibizlab.odoo.web.odoo_account.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[日记账]
 */
public class Account_journal implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 信用票：下一号码
     */
    private Integer refund_sequence_number_next;

    @JsonIgnore
    private boolean refund_sequence_number_nextDirtyFlag;
    
    /**
     * 允许的科目类型
     */
    private String type_control_ids;

    @JsonIgnore
    private boolean type_control_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 至少一个转出
     */
    private String at_least_one_outbound;

    @JsonIgnore
    private boolean at_least_one_outboundDirtyFlag;
    
    /**
     * 序号
     */
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;
    
    /**
     * 分录序列
     */
    private Integer sequence_id;

    @JsonIgnore
    private boolean sequence_idDirtyFlag;
    
    /**
     * 在仪表板显示日记账
     */
    private String show_on_dashboard;

    @JsonIgnore
    private boolean show_on_dashboardDirtyFlag;
    
    /**
     * 在销售点中使用
     */
    private String journal_user;

    @JsonIgnore
    private boolean journal_userDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 银行费用
     */
    private String bank_statements_source;

    @JsonIgnore
    private boolean bank_statements_sourceDirtyFlag;
    
    /**
     * 至少一个转入
     */
    private String at_least_one_inbound;

    @JsonIgnore
    private boolean at_least_one_inboundDirtyFlag;
    
    /**
     * 银行核销时过账
     */
    private String post_at_bank_rec;

    @JsonIgnore
    private boolean post_at_bank_recDirtyFlag;
    
    /**
     * 日记账名称
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 下一号码
     */
    private Integer sequence_number_next;

    @JsonIgnore
    private boolean sequence_number_nextDirtyFlag;
    
    /**
     * 属于用户的当前公司
     */
    private String belongs_to_company;

    @JsonIgnore
    private boolean belongs_to_companyDirtyFlag;
    
    /**
     * 简码
     */
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 为收款
     */
    private String inbound_payment_method_ids;

    @JsonIgnore
    private boolean inbound_payment_method_idsDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 分组发票明细行
     */
    private String group_invoice_lines;

    @JsonIgnore
    private boolean group_invoice_linesDirtyFlag;
    
    /**
     * 信用票分录序列
     */
    private Integer refund_sequence_id;

    @JsonIgnore
    private boolean refund_sequence_idDirtyFlag;
    
    /**
     * 别名域
     */
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;
    
    /**
     * 允许的科目
     */
    private String account_control_ids;

    @JsonIgnore
    private boolean account_control_idsDirtyFlag;
    
    /**
     * 允许取消分录
     */
    private String update_posted;

    @JsonIgnore
    private boolean update_postedDirtyFlag;
    
    /**
     * 授权差异的总金额
     */
    private Double amount_authorized_diff;

    @JsonIgnore
    private boolean amount_authorized_diffDirtyFlag;
    
    /**
     * 看板仪表板图表
     */
    private String kanban_dashboard_graph;

    @JsonIgnore
    private boolean kanban_dashboard_graphDirtyFlag;
    
    /**
     * 为付款
     */
    private String outbound_payment_method_ids;

    @JsonIgnore
    private boolean outbound_payment_method_idsDirtyFlag;
    
    /**
     * 类型
     */
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 看板仪表板
     */
    private String kanban_dashboard;

    @JsonIgnore
    private boolean kanban_dashboardDirtyFlag;
    
    /**
     * 专用的信用票序列
     */
    private String refund_sequence;

    @JsonIgnore
    private boolean refund_sequenceDirtyFlag;
    
    /**
     * 颜色索引
     */
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 利润科目
     */
    private String profit_account_id_text;

    @JsonIgnore
    private boolean profit_account_id_textDirtyFlag;
    
    /**
     * 供应商账单的别名
     */
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;
    
    /**
     * 账户号码
     */
    private String bank_acc_number;

    @JsonIgnore
    private boolean bank_acc_numberDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 银行
     */
    private Integer bank_id;

    @JsonIgnore
    private boolean bank_idDirtyFlag;
    
    /**
     * 损失科目
     */
    private String loss_account_id_text;

    @JsonIgnore
    private boolean loss_account_id_textDirtyFlag;
    
    /**
     * 默认贷方科目
     */
    private String default_credit_account_id_text;

    @JsonIgnore
    private boolean default_credit_account_id_textDirtyFlag;
    
    /**
     * 账户持有人
     */
    private Integer company_partner_id;

    @JsonIgnore
    private boolean company_partner_idDirtyFlag;
    
    /**
     * 默认借方科目
     */
    private String default_debit_account_id_text;

    @JsonIgnore
    private boolean default_debit_account_id_textDirtyFlag;
    
    /**
     * 币种
     */
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 银行账户
     */
    private Integer bank_account_id;

    @JsonIgnore
    private boolean bank_account_idDirtyFlag;
    
    /**
     * 损失科目
     */
    private Integer loss_account_id;

    @JsonIgnore
    private boolean loss_account_idDirtyFlag;
    
    /**
     * 别名
     */
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 默认贷方科目
     */
    private Integer default_credit_account_id;

    @JsonIgnore
    private boolean default_credit_account_idDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 默认借方科目
     */
    private Integer default_debit_account_id;

    @JsonIgnore
    private boolean default_debit_account_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 利润科目
     */
    private Integer profit_account_id;

    @JsonIgnore
    private boolean profit_account_idDirtyFlag;
    

    /**
     * 获取 [信用票：下一号码]
     */
    @JsonProperty("refund_sequence_number_next")
    public Integer getRefund_sequence_number_next(){
        return this.refund_sequence_number_next ;
    }

    /**
     * 设置 [信用票：下一号码]
     */
    @JsonProperty("refund_sequence_number_next")
    public void setRefund_sequence_number_next(Integer  refund_sequence_number_next){
        this.refund_sequence_number_next = refund_sequence_number_next ;
        this.refund_sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [信用票：下一号码]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_number_nextDirtyFlag(){
        return this.refund_sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [允许的科目类型]
     */
    @JsonProperty("type_control_ids")
    public String getType_control_ids(){
        return this.type_control_ids ;
    }

    /**
     * 设置 [允许的科目类型]
     */
    @JsonProperty("type_control_ids")
    public void setType_control_ids(String  type_control_ids){
        this.type_control_ids = type_control_ids ;
        this.type_control_idsDirtyFlag = true ;
    }

    /**
     * 获取 [允许的科目类型]脏标记
     */
    @JsonIgnore
    public boolean getType_control_idsDirtyFlag(){
        return this.type_control_idsDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [至少一个转出]
     */
    @JsonProperty("at_least_one_outbound")
    public String getAt_least_one_outbound(){
        return this.at_least_one_outbound ;
    }

    /**
     * 设置 [至少一个转出]
     */
    @JsonProperty("at_least_one_outbound")
    public void setAt_least_one_outbound(String  at_least_one_outbound){
        this.at_least_one_outbound = at_least_one_outbound ;
        this.at_least_one_outboundDirtyFlag = true ;
    }

    /**
     * 获取 [至少一个转出]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_outboundDirtyFlag(){
        return this.at_least_one_outboundDirtyFlag ;
    }

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }

    /**
     * 获取 [分录序列]
     */
    @JsonProperty("sequence_id")
    public Integer getSequence_id(){
        return this.sequence_id ;
    }

    /**
     * 设置 [分录序列]
     */
    @JsonProperty("sequence_id")
    public void setSequence_id(Integer  sequence_id){
        this.sequence_id = sequence_id ;
        this.sequence_idDirtyFlag = true ;
    }

    /**
     * 获取 [分录序列]脏标记
     */
    @JsonIgnore
    public boolean getSequence_idDirtyFlag(){
        return this.sequence_idDirtyFlag ;
    }

    /**
     * 获取 [在仪表板显示日记账]
     */
    @JsonProperty("show_on_dashboard")
    public String getShow_on_dashboard(){
        return this.show_on_dashboard ;
    }

    /**
     * 设置 [在仪表板显示日记账]
     */
    @JsonProperty("show_on_dashboard")
    public void setShow_on_dashboard(String  show_on_dashboard){
        this.show_on_dashboard = show_on_dashboard ;
        this.show_on_dashboardDirtyFlag = true ;
    }

    /**
     * 获取 [在仪表板显示日记账]脏标记
     */
    @JsonIgnore
    public boolean getShow_on_dashboardDirtyFlag(){
        return this.show_on_dashboardDirtyFlag ;
    }

    /**
     * 获取 [在销售点中使用]
     */
    @JsonProperty("journal_user")
    public String getJournal_user(){
        return this.journal_user ;
    }

    /**
     * 设置 [在销售点中使用]
     */
    @JsonProperty("journal_user")
    public void setJournal_user(String  journal_user){
        this.journal_user = journal_user ;
        this.journal_userDirtyFlag = true ;
    }

    /**
     * 获取 [在销售点中使用]脏标记
     */
    @JsonIgnore
    public boolean getJournal_userDirtyFlag(){
        return this.journal_userDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [银行费用]
     */
    @JsonProperty("bank_statements_source")
    public String getBank_statements_source(){
        return this.bank_statements_source ;
    }

    /**
     * 设置 [银行费用]
     */
    @JsonProperty("bank_statements_source")
    public void setBank_statements_source(String  bank_statements_source){
        this.bank_statements_source = bank_statements_source ;
        this.bank_statements_sourceDirtyFlag = true ;
    }

    /**
     * 获取 [银行费用]脏标记
     */
    @JsonIgnore
    public boolean getBank_statements_sourceDirtyFlag(){
        return this.bank_statements_sourceDirtyFlag ;
    }

    /**
     * 获取 [至少一个转入]
     */
    @JsonProperty("at_least_one_inbound")
    public String getAt_least_one_inbound(){
        return this.at_least_one_inbound ;
    }

    /**
     * 设置 [至少一个转入]
     */
    @JsonProperty("at_least_one_inbound")
    public void setAt_least_one_inbound(String  at_least_one_inbound){
        this.at_least_one_inbound = at_least_one_inbound ;
        this.at_least_one_inboundDirtyFlag = true ;
    }

    /**
     * 获取 [至少一个转入]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_inboundDirtyFlag(){
        return this.at_least_one_inboundDirtyFlag ;
    }

    /**
     * 获取 [银行核销时过账]
     */
    @JsonProperty("post_at_bank_rec")
    public String getPost_at_bank_rec(){
        return this.post_at_bank_rec ;
    }

    /**
     * 设置 [银行核销时过账]
     */
    @JsonProperty("post_at_bank_rec")
    public void setPost_at_bank_rec(String  post_at_bank_rec){
        this.post_at_bank_rec = post_at_bank_rec ;
        this.post_at_bank_recDirtyFlag = true ;
    }

    /**
     * 获取 [银行核销时过账]脏标记
     */
    @JsonIgnore
    public boolean getPost_at_bank_recDirtyFlag(){
        return this.post_at_bank_recDirtyFlag ;
    }

    /**
     * 获取 [日记账名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [日记账名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [日记账名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public Integer getSequence_number_next(){
        return this.sequence_number_next ;
    }

    /**
     * 设置 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(Integer  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [下一号码]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return this.sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [属于用户的当前公司]
     */
    @JsonProperty("belongs_to_company")
    public String getBelongs_to_company(){
        return this.belongs_to_company ;
    }

    /**
     * 设置 [属于用户的当前公司]
     */
    @JsonProperty("belongs_to_company")
    public void setBelongs_to_company(String  belongs_to_company){
        this.belongs_to_company = belongs_to_company ;
        this.belongs_to_companyDirtyFlag = true ;
    }

    /**
     * 获取 [属于用户的当前公司]脏标记
     */
    @JsonIgnore
    public boolean getBelongs_to_companyDirtyFlag(){
        return this.belongs_to_companyDirtyFlag ;
    }

    /**
     * 获取 [简码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [简码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [简码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [为收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public String getInbound_payment_method_ids(){
        return this.inbound_payment_method_ids ;
    }

    /**
     * 设置 [为收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public void setInbound_payment_method_ids(String  inbound_payment_method_ids){
        this.inbound_payment_method_ids = inbound_payment_method_ids ;
        this.inbound_payment_method_idsDirtyFlag = true ;
    }

    /**
     * 获取 [为收款]脏标记
     */
    @JsonIgnore
    public boolean getInbound_payment_method_idsDirtyFlag(){
        return this.inbound_payment_method_idsDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [分组发票明细行]
     */
    @JsonProperty("group_invoice_lines")
    public String getGroup_invoice_lines(){
        return this.group_invoice_lines ;
    }

    /**
     * 设置 [分组发票明细行]
     */
    @JsonProperty("group_invoice_lines")
    public void setGroup_invoice_lines(String  group_invoice_lines){
        this.group_invoice_lines = group_invoice_lines ;
        this.group_invoice_linesDirtyFlag = true ;
    }

    /**
     * 获取 [分组发票明细行]脏标记
     */
    @JsonIgnore
    public boolean getGroup_invoice_linesDirtyFlag(){
        return this.group_invoice_linesDirtyFlag ;
    }

    /**
     * 获取 [信用票分录序列]
     */
    @JsonProperty("refund_sequence_id")
    public Integer getRefund_sequence_id(){
        return this.refund_sequence_id ;
    }

    /**
     * 设置 [信用票分录序列]
     */
    @JsonProperty("refund_sequence_id")
    public void setRefund_sequence_id(Integer  refund_sequence_id){
        this.refund_sequence_id = refund_sequence_id ;
        this.refund_sequence_idDirtyFlag = true ;
    }

    /**
     * 获取 [信用票分录序列]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_idDirtyFlag(){
        return this.refund_sequence_idDirtyFlag ;
    }

    /**
     * 获取 [别名域]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [别名域]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [别名域]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }

    /**
     * 获取 [允许的科目]
     */
    @JsonProperty("account_control_ids")
    public String getAccount_control_ids(){
        return this.account_control_ids ;
    }

    /**
     * 设置 [允许的科目]
     */
    @JsonProperty("account_control_ids")
    public void setAccount_control_ids(String  account_control_ids){
        this.account_control_ids = account_control_ids ;
        this.account_control_idsDirtyFlag = true ;
    }

    /**
     * 获取 [允许的科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_control_idsDirtyFlag(){
        return this.account_control_idsDirtyFlag ;
    }

    /**
     * 获取 [允许取消分录]
     */
    @JsonProperty("update_posted")
    public String getUpdate_posted(){
        return this.update_posted ;
    }

    /**
     * 设置 [允许取消分录]
     */
    @JsonProperty("update_posted")
    public void setUpdate_posted(String  update_posted){
        this.update_posted = update_posted ;
        this.update_postedDirtyFlag = true ;
    }

    /**
     * 获取 [允许取消分录]脏标记
     */
    @JsonIgnore
    public boolean getUpdate_postedDirtyFlag(){
        return this.update_postedDirtyFlag ;
    }

    /**
     * 获取 [授权差异的总金额]
     */
    @JsonProperty("amount_authorized_diff")
    public Double getAmount_authorized_diff(){
        return this.amount_authorized_diff ;
    }

    /**
     * 设置 [授权差异的总金额]
     */
    @JsonProperty("amount_authorized_diff")
    public void setAmount_authorized_diff(Double  amount_authorized_diff){
        this.amount_authorized_diff = amount_authorized_diff ;
        this.amount_authorized_diffDirtyFlag = true ;
    }

    /**
     * 获取 [授权差异的总金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_authorized_diffDirtyFlag(){
        return this.amount_authorized_diffDirtyFlag ;
    }

    /**
     * 获取 [看板仪表板图表]
     */
    @JsonProperty("kanban_dashboard_graph")
    public String getKanban_dashboard_graph(){
        return this.kanban_dashboard_graph ;
    }

    /**
     * 设置 [看板仪表板图表]
     */
    @JsonProperty("kanban_dashboard_graph")
    public void setKanban_dashboard_graph(String  kanban_dashboard_graph){
        this.kanban_dashboard_graph = kanban_dashboard_graph ;
        this.kanban_dashboard_graphDirtyFlag = true ;
    }

    /**
     * 获取 [看板仪表板图表]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboard_graphDirtyFlag(){
        return this.kanban_dashboard_graphDirtyFlag ;
    }

    /**
     * 获取 [为付款]
     */
    @JsonProperty("outbound_payment_method_ids")
    public String getOutbound_payment_method_ids(){
        return this.outbound_payment_method_ids ;
    }

    /**
     * 设置 [为付款]
     */
    @JsonProperty("outbound_payment_method_ids")
    public void setOutbound_payment_method_ids(String  outbound_payment_method_ids){
        this.outbound_payment_method_ids = outbound_payment_method_ids ;
        this.outbound_payment_method_idsDirtyFlag = true ;
    }

    /**
     * 获取 [为付款]脏标记
     */
    @JsonIgnore
    public boolean getOutbound_payment_method_idsDirtyFlag(){
        return this.outbound_payment_method_idsDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [看板仪表板]
     */
    @JsonProperty("kanban_dashboard")
    public String getKanban_dashboard(){
        return this.kanban_dashboard ;
    }

    /**
     * 设置 [看板仪表板]
     */
    @JsonProperty("kanban_dashboard")
    public void setKanban_dashboard(String  kanban_dashboard){
        this.kanban_dashboard = kanban_dashboard ;
        this.kanban_dashboardDirtyFlag = true ;
    }

    /**
     * 获取 [看板仪表板]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboardDirtyFlag(){
        return this.kanban_dashboardDirtyFlag ;
    }

    /**
     * 获取 [专用的信用票序列]
     */
    @JsonProperty("refund_sequence")
    public String getRefund_sequence(){
        return this.refund_sequence ;
    }

    /**
     * 设置 [专用的信用票序列]
     */
    @JsonProperty("refund_sequence")
    public void setRefund_sequence(String  refund_sequence){
        this.refund_sequence = refund_sequence ;
        this.refund_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [专用的信用票序列]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequenceDirtyFlag(){
        return this.refund_sequenceDirtyFlag ;
    }

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [利润科目]
     */
    @JsonProperty("profit_account_id_text")
    public String getProfit_account_id_text(){
        return this.profit_account_id_text ;
    }

    /**
     * 设置 [利润科目]
     */
    @JsonProperty("profit_account_id_text")
    public void setProfit_account_id_text(String  profit_account_id_text){
        this.profit_account_id_text = profit_account_id_text ;
        this.profit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [利润科目]脏标记
     */
    @JsonIgnore
    public boolean getProfit_account_id_textDirtyFlag(){
        return this.profit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [供应商账单的别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [供应商账单的别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [供应商账单的别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }

    /**
     * 获取 [账户号码]
     */
    @JsonProperty("bank_acc_number")
    public String getBank_acc_number(){
        return this.bank_acc_number ;
    }

    /**
     * 设置 [账户号码]
     */
    @JsonProperty("bank_acc_number")
    public void setBank_acc_number(String  bank_acc_number){
        this.bank_acc_number = bank_acc_number ;
        this.bank_acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [账户号码]脏标记
     */
    @JsonIgnore
    public boolean getBank_acc_numberDirtyFlag(){
        return this.bank_acc_numberDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return this.bank_id ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return this.bank_idDirtyFlag ;
    }

    /**
     * 获取 [损失科目]
     */
    @JsonProperty("loss_account_id_text")
    public String getLoss_account_id_text(){
        return this.loss_account_id_text ;
    }

    /**
     * 设置 [损失科目]
     */
    @JsonProperty("loss_account_id_text")
    public void setLoss_account_id_text(String  loss_account_id_text){
        this.loss_account_id_text = loss_account_id_text ;
        this.loss_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [损失科目]脏标记
     */
    @JsonIgnore
    public boolean getLoss_account_id_textDirtyFlag(){
        return this.loss_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id_text")
    public String getDefault_credit_account_id_text(){
        return this.default_credit_account_id_text ;
    }

    /**
     * 设置 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id_text")
    public void setDefault_credit_account_id_text(String  default_credit_account_id_text){
        this.default_credit_account_id_text = default_credit_account_id_text ;
        this.default_credit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [默认贷方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_credit_account_id_textDirtyFlag(){
        return this.default_credit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [账户持有人]
     */
    @JsonProperty("company_partner_id")
    public Integer getCompany_partner_id(){
        return this.company_partner_id ;
    }

    /**
     * 设置 [账户持有人]
     */
    @JsonProperty("company_partner_id")
    public void setCompany_partner_id(Integer  company_partner_id){
        this.company_partner_id = company_partner_id ;
        this.company_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [账户持有人]脏标记
     */
    @JsonIgnore
    public boolean getCompany_partner_idDirtyFlag(){
        return this.company_partner_idDirtyFlag ;
    }

    /**
     * 获取 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id_text")
    public String getDefault_debit_account_id_text(){
        return this.default_debit_account_id_text ;
    }

    /**
     * 设置 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id_text")
    public void setDefault_debit_account_id_text(String  default_debit_account_id_text){
        this.default_debit_account_id_text = default_debit_account_id_text ;
        this.default_debit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [默认借方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_debit_account_id_textDirtyFlag(){
        return this.default_debit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return this.bank_account_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return this.bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [损失科目]
     */
    @JsonProperty("loss_account_id")
    public Integer getLoss_account_id(){
        return this.loss_account_id ;
    }

    /**
     * 设置 [损失科目]
     */
    @JsonProperty("loss_account_id")
    public void setLoss_account_id(Integer  loss_account_id){
        this.loss_account_id = loss_account_id ;
        this.loss_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [损失科目]脏标记
     */
    @JsonIgnore
    public boolean getLoss_account_idDirtyFlag(){
        return this.loss_account_idDirtyFlag ;
    }

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id")
    public Integer getDefault_credit_account_id(){
        return this.default_credit_account_id ;
    }

    /**
     * 设置 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id")
    public void setDefault_credit_account_id(Integer  default_credit_account_id){
        this.default_credit_account_id = default_credit_account_id ;
        this.default_credit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [默认贷方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_credit_account_idDirtyFlag(){
        return this.default_credit_account_idDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id")
    public Integer getDefault_debit_account_id(){
        return this.default_debit_account_id ;
    }

    /**
     * 设置 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id")
    public void setDefault_debit_account_id(Integer  default_debit_account_id){
        this.default_debit_account_id = default_debit_account_id ;
        this.default_debit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [默认借方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_debit_account_idDirtyFlag(){
        return this.default_debit_account_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [利润科目]
     */
    @JsonProperty("profit_account_id")
    public Integer getProfit_account_id(){
        return this.profit_account_id ;
    }

    /**
     * 设置 [利润科目]
     */
    @JsonProperty("profit_account_id")
    public void setProfit_account_id(Integer  profit_account_id){
        this.profit_account_id = profit_account_id ;
        this.profit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [利润科目]脏标记
     */
    @JsonIgnore
    public boolean getProfit_account_idDirtyFlag(){
        return this.profit_account_idDirtyFlag ;
    }



}

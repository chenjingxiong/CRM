package cn.ibizlab.odoo.web.odoo_product.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.web.odoo_product.filter.*;


public interface Product_price_listFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_price_lists/{product_price_list_id}")
    public Boolean remove(@PathVariable("product_price_list_id") Integer product_price_list_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_price_lists/{product_price_list_id}")
    public Product_price_list update(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_price_lists/{product_price_list_id}/updatebatch")
    public Product_price_list updateBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_price_lists")
    public Product_price_list create(@RequestBody Product_price_list product_price_list) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_price_lists/{product_price_list_id}")
    public Product_price_list get(@PathVariable("product_price_list_id") Integer product_price_list_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_price_lists/{product_price_list_id}/getdraft")
    public Product_price_list getDraft(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_price_lists/{product_price_list_id}/removebatch")
    public Product_price_list removeBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_price_lists/{product_price_list_id}/createbatch")
    public Product_price_list createBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/product_price_lists/fetchdefault")
	public Page<Product_price_list> fetchDefault(Product_price_listSearchContext searchContext) ;
}

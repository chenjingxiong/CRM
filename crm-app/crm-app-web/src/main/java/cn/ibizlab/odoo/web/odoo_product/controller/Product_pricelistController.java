package cn.ibizlab.odoo.web.odoo_product.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_product.service.Product_pricelistService;
import cn.ibizlab.odoo.web.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.web.odoo_product.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Product_pricelistController {
	@Autowired
    Product_pricelistService product_pricelistservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/{product_pricelist_id}")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'READ')")
    public ResponseEntity<Product_pricelist> get(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
        Product_pricelist product_pricelist = product_pricelistservice.get( product_pricelist_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/{product_pricelist_id}/save")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'')")
    public ResponseEntity<Product_pricelist> save(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.save(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/{product_pricelist_id}/removebatch")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'DELETE')")
    public ResponseEntity<Product_pricelist> removeBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.removeBatch(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/getdraft")
    @PreAuthorize("@product_pricelist_pms.check('CREATE')")
    public ResponseEntity<Product_pricelist> getDraft() {
        //Product_pricelist product_pricelist = product_pricelistservice.getDraft( product_pricelist_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Product_pricelist());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/{product_pricelist_id}")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'UPDATE')")
    public ResponseEntity<Product_pricelist> update(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.update(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/product_pricelists/{product_pricelist_id}")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'UPDATE')")
    public ResponseEntity<Product_pricelist> api_update(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.update(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/{product_pricelist_id}")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
        boolean b = product_pricelistservice.remove( product_pricelist_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/{product_pricelist_id}/updatebatch")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'UPDATE')")
    public ResponseEntity<Product_pricelist> updateBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.updateBatch(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/checkkey")
    @PreAuthorize("@product_pricelist_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelist product_pricelist) {
        boolean b = product_pricelistservice.checkKey(product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/{product_pricelist_id}/createbatch")
    @PreAuthorize("@product_pricelist_pms.check(#product_pricelist_id,'CREATE')")
    public ResponseEntity<Product_pricelist> createBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.createBatch(product_pricelist_id, product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists")
    @PreAuthorize("@product_pricelist_pms.check('CREATE')")
    public ResponseEntity<Product_pricelist> create(@RequestBody Product_pricelist product_pricelist) {
        Product_pricelist product_pricelist2 = product_pricelistservice.create(product_pricelist);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelists/fetchdefault")
    @PreAuthorize("@product_pricelist_pms.check('READ')")
	public ResponseEntity<List<Product_pricelist>> fetchDefault(Product_pricelistSearchContext searchContext,Pageable pageable) {
        
        Page<Product_pricelist> page = product_pricelistservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

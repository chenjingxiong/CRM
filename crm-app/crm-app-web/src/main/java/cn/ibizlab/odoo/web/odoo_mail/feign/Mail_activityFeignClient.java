package cn.ibizlab.odoo.web.odoo_mail.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.web.odoo_mail.filter.*;


public interface Mail_activityFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activities/{mail_activity_id}/getdraft")
    public Mail_activity getDraft(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activities/{mail_activity_id}/removebatch")
    public Mail_activity removeBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activities/{mail_activity_id}")
    public Mail_activity update(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activities/{mail_activity_id}/updatebatch")
    public Mail_activity updateBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/checkkey")
    public Boolean checkKey(@RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/{mail_activity_id}/save")
    public Mail_activity save(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activities/{mail_activity_id}")
    public Boolean remove(@PathVariable("mail_activity_id") Integer mail_activity_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities/{mail_activity_id}/createbatch")
    public Mail_activity createBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activities/{mail_activity_id}")
    public Mail_activity get(@PathVariable("mail_activity_id") Integer mail_activity_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activities")
    public Mail_activity create(@RequestBody Mail_activity mail_activity) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/mail_activities/fetchdefault")
	public Page<Mail_activity> fetchDefault(Mail_activitySearchContext searchContext) ;
}

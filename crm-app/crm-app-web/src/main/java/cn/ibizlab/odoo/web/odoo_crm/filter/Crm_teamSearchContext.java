package cn.ibizlab.odoo.web.odoo_crm.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Crm_teamSearchContext extends SearchContext implements Serializable {

	public String n_dashboard_graph_type_eq;//[类型]
	public String n_team_type_eq;//[团队类型]
	public String n_dashboard_graph_model_eq;//[内容]
	public String n_dashboard_graph_period_pipeline_eq;//[预计关闭]
	public String n_dashboard_graph_group_pos_eq;//[POS分组]
	public String n_dashboard_graph_group_eq;//[分组]
	public String n_dashboard_graph_period_eq;//[比例]
	public String n_dashboard_graph_group_pipeline_eq;//[分组方式]
	public String n_name_like;//[销售团队]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_user_id_text_eq;//[团队负责人]
	public String n_user_id_text_like;//[团队负责人]
	public String n_write_uid_text_eq;//[最后更新]
	public String n_write_uid_text_like;//[最后更新]
	public Integer n_write_uid_eq;//[最后更新]
	public Integer n_alias_id_eq;//[别名]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_company_id_eq;//[公司]
	public Integer n_user_id_eq;//[团队负责人]

}
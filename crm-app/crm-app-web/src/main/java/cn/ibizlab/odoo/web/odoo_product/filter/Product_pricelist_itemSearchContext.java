package cn.ibizlab.odoo.web.odoo_product.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Product_pricelist_itemSearchContext extends SearchContext implements Serializable {

	public String n_name_like;//[名称]
	public String n_base_eq;//[基于]
	public String n_compute_price_eq;//[计算价格]
	public String n_applied_on_eq;//[应用于]
	public String n_pricelist_id_text_eq;//[价格表]
	public String n_pricelist_id_text_like;//[价格表]
	public String n_currency_id_text_eq;//[币种]
	public String n_currency_id_text_like;//[币种]
	public String n_categ_id_text_eq;//[产品种类]
	public String n_categ_id_text_like;//[产品种类]
	public String n_base_pricelist_id_text_eq;//[其他价格表]
	public String n_base_pricelist_id_text_like;//[其他价格表]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_product_tmpl_id_text_eq;//[产品模板]
	public String n_product_tmpl_id_text_like;//[产品模板]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_product_id_text_eq;//[产品]
	public String n_product_id_text_like;//[产品]
	public Integer n_product_id_eq;//[产品]
	public Integer n_pricelist_id_eq;//[价格表]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_company_id_eq;//[公司]
	public Integer n_base_pricelist_id_eq;//[其他价格表]
	public Integer n_currency_id_eq;//[币种]
	public Integer n_categ_id_eq;//[产品种类]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_product_tmpl_id_eq;//[产品模板]

}
package cn.ibizlab.odoo.web.odoo_crm.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Crm_leadSearchContext extends SearchContext implements Serializable {

	public String n_name_like;//[商机]
	public String n_activity_state_eq;//[活动状态]
	public String n_type_eq;//[类型]
	public String n_kanban_state_eq;//[看板状态]
	public String n_priority_eq;//[优先级]
	public String n_user_id_text_eq;//[销售员]
	public String n_user_id_text_like;//[销售员]
	public String n_source_id_text_eq;//[来源]
	public String n_source_id_text_like;//[来源]
	public String n_partner_address_name_eq;//[业务伙伴联系姓名]
	public String n_partner_address_name_like;//[业务伙伴联系姓名]
	public String n_medium_id_text_eq;//[媒介]
	public String n_medium_id_text_like;//[媒介]
	public String n_team_id_text_eq;//[销售团队]
	public String n_team_id_text_like;//[销售团队]
	public String n_write_uid_text_eq;//[最后更新]
	public String n_write_uid_text_like;//[最后更新]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_state_id_text_eq;//[省份]
	public String n_state_id_text_like;//[省份]
	public String n_campaign_id_text_eq;//[营销]
	public String n_campaign_id_text_like;//[营销]
	public String n_stage_id_text_eq;//[阶段]
	public String n_stage_id_text_like;//[阶段]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_country_id_text_eq;//[国家]
	public String n_country_id_text_like;//[国家]
	public String n_title_text_eq;//[称谓]
	public String n_title_text_like;//[称谓]
	public String n_lost_reason_text_eq;//[失去原因]
	public String n_lost_reason_text_like;//[失去原因]
	public Integer n_lost_reason_eq;//[失去原因]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_write_uid_eq;//[最后更新]
	public Integer n_company_id_eq;//[公司]
	public Integer n_user_id_eq;//[销售员]
	public Integer n_state_id_eq;//[省份]
	public Integer n_medium_id_eq;//[媒介]
	public Integer n_stage_id_eq;//[阶段]
	public Integer n_source_id_eq;//[来源]
	public Integer n_country_id_eq;//[国家]
	public Integer n_campaign_id_eq;//[营销]
	public Integer n_partner_id_eq;//[客户]
	public Integer n_team_id_eq;//[销售团队]
	public Integer n_title_eq;//[称谓]

}
package cn.ibizlab.odoo.web.odoo_sale.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Sale_order_lineSearchContext extends SearchContext implements Serializable {

	public String n_qty_delivered_method_eq;//[更新数量的方法]
	public String n_invoice_status_eq;//[发票状态]
	public String n_display_type_eq;//[显示类型]
	public String n_name_like;//[说明]
	public String n_event_ticket_id_text_eq;//[活动入场券]
	public String n_event_ticket_id_text_like;//[活动入场券]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_linked_line_id_text_eq;//[链接的订单明细]
	public String n_linked_line_id_text_like;//[链接的订单明细]
	public String n_order_partner_id_text_eq;//[客户]
	public String n_order_partner_id_text_like;//[客户]
	public String n_currency_id_text_eq;//[币种]
	public String n_currency_id_text_like;//[币种]
	public String n_product_id_text_eq;//[产品]
	public String n_product_id_text_like;//[产品]
	public String n_product_packaging_text_eq;//[包裹]
	public String n_product_packaging_text_like;//[包裹]
	public String n_order_id_text_eq;//[订单关联]
	public String n_order_id_text_like;//[订单关联]
	public String n_event_id_text_eq;//[活动]
	public String n_event_id_text_like;//[活动]
	public String n_salesman_id_text_eq;//[销售员]
	public String n_salesman_id_text_like;//[销售员]
	public String n_product_uom_text_eq;//[计量单位]
	public String n_product_uom_text_like;//[计量单位]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_route_id_text_eq;//[路线]
	public String n_route_id_text_like;//[路线]
	public Integer n_salesman_id_eq;//[销售员]
	public Integer n_currency_id_eq;//[币种]
	public Integer n_order_id_eq;//[订单关联]
	public Integer n_event_id_eq;//[活动]
	public Integer n_linked_line_id_eq;//[链接的订单明细]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_route_id_eq;//[路线]
	public Integer n_order_partner_id_eq;//[客户]
	public Integer n_product_uom_eq;//[计量单位]
	public Integer n_product_id_eq;//[产品]
	public Integer n_company_id_eq;//[公司]
	public Integer n_product_packaging_eq;//[包裹]
	public Integer n_event_ticket_id_eq;//[活动入场券]

}
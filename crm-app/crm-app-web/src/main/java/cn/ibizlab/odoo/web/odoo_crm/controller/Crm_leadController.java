package cn.ibizlab.odoo.web.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_crm.service.Crm_leadService;
import cn.ibizlab.odoo.web.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.web.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_leadController {
	@Autowired
    Crm_leadService crm_leadservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/getdraft")
    @PreAuthorize("@crm_lead_pms.check('CREATE')")
    public ResponseEntity<Crm_lead> getDraft() {
        //Crm_lead crm_lead = crm_leadservice.getDraft( crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lead());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        boolean b = crm_leadservice.remove( crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/{crm_lead_id}/save")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'')")
    public ResponseEntity<Crm_lead> save(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.save(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{crm_lead_id}/updatebatch")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> updateBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.updateBatch(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'READ')")
    public ResponseEntity<Crm_lead> get(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_lead crm_lead = crm_leadservice.get( crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/{crm_lead_id}/createbatch")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'CREATE')")
    public ResponseEntity<Crm_lead> createBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.createBatch(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{crm_lead_id}/removebatch")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'DELETE')")
    public ResponseEntity<Crm_lead> removeBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.removeBatch(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads")
    @PreAuthorize("@crm_lead_pms.check('CREATE')")
    public ResponseEntity<Crm_lead> create(@RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.create(crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/checkkey")
    @PreAuthorize("@crm_lead_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lead crm_lead) {
        boolean b = crm_leadservice.checkKey(crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.update(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.check(#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> api_update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.update(crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault")
    @PreAuthorize("@crm_lead_pms.check('READ')")
	public ResponseEntity<List<Crm_lead>> fetchDefault(Crm_leadSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lead> page = crm_leadservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault2")
    @PreAuthorize("@crm_lead_pms.check('READ')")
	public ResponseEntity<List<Crm_lead>> fetchDefault2(Crm_leadSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lead> page = crm_leadservice.fetchDefault2(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/crm_leads/getdraft")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Crm_lead> getDraftByRes_partner() {
        //Crm_lead crm_lead = crm_leadservice.getDraftByRes_partner(res_partner_id,  crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lead());
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'DELETE')")
    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) {
        boolean b = crm_leadservice.removeByRes_partner(res_partner_id,  crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/save")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'')")
    public ResponseEntity<Crm_lead> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.saveByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/updatebatch")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.updateBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'READ')")
    public ResponseEntity<Crm_lead> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_lead crm_lead = crm_leadservice.getByRes_partner(res_partner_id,  crm_lead_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/createbatch")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'CREATE')")
    public ResponseEntity<Crm_lead> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.createBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/removebatch")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'DELETE')")
    public ResponseEntity<Crm_lead> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.removeBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Crm_lead> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.createByRes_partner(res_partner_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/crm_leads/checkkey")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,'')")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_lead crm_lead) {
        boolean b = crm_leadservice.checkKeyByRes_partner(res_partner_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.updateByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,#crm_lead_id,'UPDATE')")
    public ResponseEntity<Crm_lead> api_updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) {
        Crm_lead crm_lead2 = crm_leadservice.updateByRes_partner(res_partner_id, crm_lead_id, crm_lead);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2);
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/crm_leads/fetchdefault")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Crm_lead>> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Crm_leadSearchContext searchContext,Pageable pageable) {
        searchContext.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> page = crm_leadservice.fetchDefaultByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/crm_leads/fetchdefault2")
    @PreAuthorize("@crm_lead_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Crm_lead>> fetchDefault2ByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Crm_leadSearchContext searchContext,Pageable pageable) {
        searchContext.setN_partner_id_eq(res_partner_id);
        Page<Crm_lead> page = crm_leadservice.fetchDefault2ByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

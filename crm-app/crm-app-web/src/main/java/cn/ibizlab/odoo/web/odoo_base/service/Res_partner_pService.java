package cn.ibizlab.odoo.web.odoo_base.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner_p;
import cn.ibizlab.odoo.web.odoo_base.filter.*;
import cn.ibizlab.odoo.web.odoo_base.feign.Res_partner_pFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Res_partner_pService {

    Res_partner_pFeignClient client;

    @Autowired
    public Res_partner_pService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_partner_pFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_partner_pFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer res_partner_p_id) {
        return client.remove( res_partner_p_id);
    }

    public Res_partner_p get( Integer res_partner_p_id) {
        return client.get( res_partner_p_id);
    }

    public boolean checkKey(Res_partner_p res_partner_p) {
        return client.checkKey(res_partner_p);
    }

    public Res_partner_p save(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.save(res_partner_p_id, res_partner_p);
    }

    public Res_partner_p update(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.update(res_partner_p_id, res_partner_p);
    }

    public Res_partner_p createBatch(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.createBatch(res_partner_p_id, res_partner_p);
    }

    public Res_partner_p updateBatch(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.updateBatch(res_partner_p_id, res_partner_p);
    }

    public Res_partner_p getDraft(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.getDraft(res_partner_p_id, res_partner_p);
    }

	public Res_partner_p create(Res_partner_p res_partner_p) {
        return client.create(res_partner_p);
    }

    public Res_partner_p removeBatch(Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.removeBatch(res_partner_p_id, res_partner_p);
    }

	public Page<Res_partner_p> fetchContacts(Res_partner_pSearchContext searchContext) {
        return client.fetchContacts(searchContext);
    }

	public Page<Res_partner_p> fetchCompany(Res_partner_pSearchContext searchContext) {
        return client.fetchCompany(searchContext);
    }

	public Page<Res_partner_p> fetchDefault(Res_partner_pSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }


    public boolean removeByRes_partner(Integer res_partner_id,  Integer res_partner_p_id) {
        return client.removeByRes_partner(res_partner_id,  res_partner_p_id);
    }

    public Res_partner_p getByRes_partner(Integer res_partner_id,  Integer res_partner_p_id) {
        return client.getByRes_partner(res_partner_id,  res_partner_p_id);
    }

    public boolean checkKeyByRes_partner(Integer res_partner_id, Res_partner_p res_partner_p) {
        return client.checkKeyByRes_partner(res_partner_id, res_partner_p);
    }

    public Res_partner_p saveByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.saveByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

    public Res_partner_p updateByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.updateByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

    public Res_partner_p createBatchByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.createBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

    public Res_partner_p updateBatchByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.updateBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

    public Res_partner_p getDraftByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.getDraftByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

	public Res_partner_p createByRes_partner(Integer res_partner_id, Res_partner_p res_partner_p) {
        return client.createByRes_partner(res_partner_id, res_partner_p);
    }

    public Res_partner_p removeBatchByRes_partner(Integer res_partner_id, Integer res_partner_p_id, Res_partner_p res_partner_p) {
        return client.removeBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
    }

	public Page<Res_partner_p> fetchContactsByRes_partner(Integer res_partner_id , Res_partner_pSearchContext searchContext) {
        return client.fetchContactsByRes_partner(res_partner_id , searchContext);
    }

	public Page<Res_partner_p> fetchCompanyByRes_partner(Integer res_partner_id , Res_partner_pSearchContext searchContext) {
        return client.fetchCompanyByRes_partner(res_partner_id , searchContext);
    }

	public Page<Res_partner_p> fetchDefaultByRes_partner(Integer res_partner_id , Res_partner_pSearchContext searchContext) {
        return client.fetchDefaultByRes_partner(res_partner_id , searchContext);
    }

}

package cn.ibizlab.odoo.web.odoo_mail.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.web.odoo_mail.filter.*;
import cn.ibizlab.odoo.web.odoo_mail.feign.Mail_activityFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Mail_activityService {

    Mail_activityFeignClient client;

    @Autowired
    public Mail_activityService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_activityFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_activityFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Mail_activity getDraft(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.getDraft(mail_activity_id, mail_activity);
    }

    public Mail_activity removeBatch(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.removeBatch(mail_activity_id, mail_activity);
    }

    public Mail_activity update(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.update(mail_activity_id, mail_activity);
    }

    public Mail_activity updateBatch(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.updateBatch(mail_activity_id, mail_activity);
    }

    public boolean checkKey(Mail_activity mail_activity) {
        return client.checkKey(mail_activity);
    }

    public Mail_activity save(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.save(mail_activity_id, mail_activity);
    }

    public boolean remove( Integer mail_activity_id) {
        return client.remove( mail_activity_id);
    }

    public Mail_activity createBatch(Integer mail_activity_id, Mail_activity mail_activity) {
        return client.createBatch(mail_activity_id, mail_activity);
    }

    public Mail_activity get( Integer mail_activity_id) {
        return client.get( mail_activity_id);
    }

	public Mail_activity create(Mail_activity mail_activity) {
        return client.create(mail_activity);
    }

	public Page<Mail_activity> fetchDefault(Mail_activitySearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

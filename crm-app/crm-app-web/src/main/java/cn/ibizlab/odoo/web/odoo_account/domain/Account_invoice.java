package cn.ibizlab.odoo.web.odoo_account.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[发票]
 */
public class Account_invoice implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 未到期贷项
     */
    private String outstanding_credits_debits_widget;

    @JsonIgnore
    private boolean outstanding_credits_debits_widgetDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 日记账分录名称
     */
    private String move_name;

    @JsonIgnore
    private boolean move_nameDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;
    
    /**
     * 门户访问网址
     */
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;
    
    /**
     * 付款凭证明细
     */
    private String payment_move_line_ids;

    @JsonIgnore
    private boolean payment_move_line_idsDirtyFlag;
    
    /**
     * 源文档
     */
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;
    
    /**
     * 会计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;
    
    /**
     * 公司货币的合计
     */
    private Double amount_total_company_signed;

    @JsonIgnore
    private boolean amount_total_company_signedDirtyFlag;
    
    /**
     * 操作编号
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 按公司本位币计的不含税金额
     */
    private Double amount_untaxed_signed;

    @JsonIgnore
    private boolean amount_untaxed_signedDirtyFlag;
    
    /**
     * 到期金额
     */
    private Double residual;

    @JsonIgnore
    private boolean residualDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 有未清项
     */
    private String has_outstanding;

    @JsonIgnore
    private boolean has_outstandingDirtyFlag;
    
    /**
     * 额外的信息
     */
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * 下一活动类型
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 按组分配税额
     */
    private byte[] amount_by_group;

    @JsonIgnore
    private boolean amount_by_groupDirtyFlag;
    
    /**
     * 支付挂件
     */
    private String payments_widget;

    @JsonIgnore
    private boolean payments_widgetDirtyFlag;
    
    /**
     * 已付／已核销
     */
    private String reconciled;

    @JsonIgnore
    private boolean reconciledDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 已汇
     */
    private String sent;

    @JsonIgnore
    private boolean sentDirtyFlag;
    
    /**
     * 以发票币种总计
     */
    private Double amount_total_signed;

    @JsonIgnore
    private boolean amount_total_signedDirtyFlag;
    
    /**
     * 发票行
     */
    private String invoice_line_ids;

    @JsonIgnore
    private boolean invoice_line_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 访问警告
     */
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 活动状态
     */
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;
    
    /**
     * 发票使用币种的逾期金额
     */
    private Double residual_signed;

    @JsonIgnore
    private boolean residual_signedDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * 安全令牌
     */
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;
    
    /**
     * 供应商名称
     */
    private String vendor_display_name;

    @JsonIgnore
    private boolean vendor_display_nameDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 未税金额
     */
    private Double amount_untaxed;

    @JsonIgnore
    private boolean amount_untaxedDirtyFlag;
    
    /**
     * 交易
     */
    private String transaction_ids;

    @JsonIgnore
    private boolean transaction_idsDirtyFlag;
    
    /**
     * 到期日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_due;

    @JsonIgnore
    private boolean date_dueDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 已授权的交易
     */
    private String authorized_transaction_ids;

    @JsonIgnore
    private boolean authorized_transaction_idsDirtyFlag;
    
    /**
     * 税率明细行
     */
    private String tax_line_ids;

    @JsonIgnore
    private boolean tax_line_idsDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 源邮箱
     */
    private String source_email;

    @JsonIgnore
    private boolean source_emailDirtyFlag;
    
    /**
     * 税率
     */
    private Double amount_tax;

    @JsonIgnore
    private boolean amount_taxDirtyFlag;
    
    /**
     * 是关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 状态
     */
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * Untaxed Amount in Invoice Currency
     */
    private Double amount_untaxed_invoice_signed;

    @JsonIgnore
    private boolean amount_untaxed_invoice_signedDirtyFlag;
    
    /**
     * 需要采取行动
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 下一个号码前缀
     */
    private String sequence_number_next_prefix;

    @JsonIgnore
    private boolean sequence_number_next_prefixDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 退款发票
     */
    private String refund_invoice_ids;

    @JsonIgnore
    private boolean refund_invoice_idsDirtyFlag;
    
    /**
     * 公司使用币种的逾期金额
     */
    private Double residual_company_signed;

    @JsonIgnore
    private boolean residual_company_signedDirtyFlag;
    
    /**
     * 开票日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_invoice;

    @JsonIgnore
    private boolean date_invoiceDirtyFlag;
    
    /**
     * 参考/说明
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 责任用户
     */
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;
    
    /**
     * 付款参考:
     */
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;
    
    /**
     * 网站
     */
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;
    
    /**
     * 下一号码
     */
    private String sequence_number_next;

    @JsonIgnore
    private boolean sequence_number_nextDirtyFlag;
    
    /**
     * 网站信息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * Tax in Invoice Currency
     */
    private Double amount_tax_signed;

    @JsonIgnore
    private boolean amount_tax_signedDirtyFlag;
    
    /**
     * 发票标示
     */
    private String invoice_icon;

    @JsonIgnore
    private boolean invoice_iconDirtyFlag;
    
    /**
     * 付款
     */
    private String payment_ids;

    @JsonIgnore
    private boolean payment_idsDirtyFlag;
    
    /**
     * 需要一个动作消息的编码
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 总计
     */
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 类型
     */
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;
    
    /**
     * 日记账
     */
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;
    
    /**
     * 添加采购订单
     */
    private String purchase_id_text;

    @JsonIgnore
    private boolean purchase_id_textDirtyFlag;
    
    /**
     * 自动完成
     */
    private String vendor_bill_purchase_id_text;

    @JsonIgnore
    private boolean vendor_bill_purchase_id_textDirtyFlag;
    
    /**
     * 来源
     */
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;
    
    /**
     * 营销
     */
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;
    
    /**
     * 送货地址
     */
    private String partner_shipping_id_text;

    @JsonIgnore
    private boolean partner_shipping_id_textDirtyFlag;
    
    /**
     * 币种
     */
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 贸易条款
     */
    private String incoterms_id_text;

    @JsonIgnore
    private boolean incoterms_id_textDirtyFlag;
    
    /**
     * 公司货币
     */
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;
    
    /**
     * 科目
     */
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;
    
    /**
     * 国际贸易术语
     */
    private String incoterm_id_text;

    @JsonIgnore
    private boolean incoterm_id_textDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;
    
    /**
     * 媒体
     */
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 现金舍入方式
     */
    private String cash_rounding_id_text;

    @JsonIgnore
    private boolean cash_rounding_id_textDirtyFlag;
    
    /**
     * 供应商账单
     */
    private String vendor_bill_id_text;

    @JsonIgnore
    private boolean vendor_bill_id_textDirtyFlag;
    
    /**
     * 此发票为信用票的发票
     */
    private String refund_invoice_id_text;

    @JsonIgnore
    private boolean refund_invoice_id_textDirtyFlag;
    
    /**
     * 税科目调整
     */
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * 号码
     */
    private String number;

    @JsonIgnore
    private boolean numberDirtyFlag;
    
    /**
     * 商业实体
     */
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 销售团队
     */
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;
    
    /**
     * 付款条款
     */
    private String payment_term_id_text;

    @JsonIgnore
    private boolean payment_term_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 销售团队
     */
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 科目
     */
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;
    
    /**
     * 媒体
     */
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 日记账分录
     */
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;
    
    /**
     * 付款条款
     */
    private Integer payment_term_id;

    @JsonIgnore
    private boolean payment_term_idDirtyFlag;
    
    /**
     * 日记账
     */
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;
    
    /**
     * 国际贸易术语
     */
    private Integer incoterm_id;

    @JsonIgnore
    private boolean incoterm_idDirtyFlag;
    
    /**
     * 添加采购订单
     */
    private Integer purchase_id;

    @JsonIgnore
    private boolean purchase_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;
    
    /**
     * 此发票为信用票的发票
     */
    private Integer refund_invoice_id;

    @JsonIgnore
    private boolean refund_invoice_idDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 银行账户
     */
    private Integer partner_bank_id;

    @JsonIgnore
    private boolean partner_bank_idDirtyFlag;
    
    /**
     * 现金舍入方式
     */
    private Integer cash_rounding_id;

    @JsonIgnore
    private boolean cash_rounding_idDirtyFlag;
    
    /**
     * 商业实体
     */
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;
    
    /**
     * 送货地址
     */
    private Integer partner_shipping_id;

    @JsonIgnore
    private boolean partner_shipping_idDirtyFlag;
    
    /**
     * 贸易条款
     */
    private Integer incoterms_id;

    @JsonIgnore
    private boolean incoterms_idDirtyFlag;
    
    /**
     * 来源
     */
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;
    
    /**
     * 供应商账单
     */
    private Integer vendor_bill_id;

    @JsonIgnore
    private boolean vendor_bill_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 自动完成
     */
    private Integer vendor_bill_purchase_id;

    @JsonIgnore
    private boolean vendor_bill_purchase_idDirtyFlag;
    
    /**
     * 营销
     */
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;
    

    /**
     * 获取 [未到期贷项]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public String getOutstanding_credits_debits_widget(){
        return this.outstanding_credits_debits_widget ;
    }

    /**
     * 设置 [未到期贷项]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public void setOutstanding_credits_debits_widget(String  outstanding_credits_debits_widget){
        this.outstanding_credits_debits_widget = outstanding_credits_debits_widget ;
        this.outstanding_credits_debits_widgetDirtyFlag = true ;
    }

    /**
     * 获取 [未到期贷项]脏标记
     */
    @JsonIgnore
    public boolean getOutstanding_credits_debits_widgetDirtyFlag(){
        return this.outstanding_credits_debits_widgetDirtyFlag ;
    }

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return this.move_name ;
    }

    /**
     * 设置 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

    /**
     * 获取 [日记账分录名称]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return this.move_nameDirtyFlag ;
    }

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [门户访问网址]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问网址]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [门户访问网址]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }

    /**
     * 获取 [付款凭证明细]
     */
    @JsonProperty("payment_move_line_ids")
    public String getPayment_move_line_ids(){
        return this.payment_move_line_ids ;
    }

    /**
     * 设置 [付款凭证明细]
     */
    @JsonProperty("payment_move_line_ids")
    public void setPayment_move_line_ids(String  payment_move_line_ids){
        this.payment_move_line_ids = payment_move_line_ids ;
        this.payment_move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [付款凭证明细]脏标记
     */
    @JsonIgnore
    public boolean getPayment_move_line_idsDirtyFlag(){
        return this.payment_move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }

    /**
     * 获取 [会计日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [会计日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [会计日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }

    /**
     * 获取 [公司货币的合计]
     */
    @JsonProperty("amount_total_company_signed")
    public Double getAmount_total_company_signed(){
        return this.amount_total_company_signed ;
    }

    /**
     * 设置 [公司货币的合计]
     */
    @JsonProperty("amount_total_company_signed")
    public void setAmount_total_company_signed(Double  amount_total_company_signed){
        this.amount_total_company_signed = amount_total_company_signed ;
        this.amount_total_company_signedDirtyFlag = true ;
    }

    /**
     * 获取 [公司货币的合计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_company_signedDirtyFlag(){
        return this.amount_total_company_signedDirtyFlag ;
    }

    /**
     * 获取 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [操作编号]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [按公司本位币计的不含税金额]
     */
    @JsonProperty("amount_untaxed_signed")
    public Double getAmount_untaxed_signed(){
        return this.amount_untaxed_signed ;
    }

    /**
     * 设置 [按公司本位币计的不含税金额]
     */
    @JsonProperty("amount_untaxed_signed")
    public void setAmount_untaxed_signed(Double  amount_untaxed_signed){
        this.amount_untaxed_signed = amount_untaxed_signed ;
        this.amount_untaxed_signedDirtyFlag = true ;
    }

    /**
     * 获取 [按公司本位币计的不含税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_signedDirtyFlag(){
        return this.amount_untaxed_signedDirtyFlag ;
    }

    /**
     * 获取 [到期金额]
     */
    @JsonProperty("residual")
    public Double getResidual(){
        return this.residual ;
    }

    /**
     * 设置 [到期金额]
     */
    @JsonProperty("residual")
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.residualDirtyFlag = true ;
    }

    /**
     * 获取 [到期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidualDirtyFlag(){
        return this.residualDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [有未清项]
     */
    @JsonProperty("has_outstanding")
    public String getHas_outstanding(){
        return this.has_outstanding ;
    }

    /**
     * 设置 [有未清项]
     */
    @JsonProperty("has_outstanding")
    public void setHas_outstanding(String  has_outstanding){
        this.has_outstanding = has_outstanding ;
        this.has_outstandingDirtyFlag = true ;
    }

    /**
     * 获取 [有未清项]脏标记
     */
    @JsonIgnore
    public boolean getHas_outstandingDirtyFlag(){
        return this.has_outstandingDirtyFlag ;
    }

    /**
     * 获取 [额外的信息]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [额外的信息]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [额外的信息]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return this.amount_by_group ;
    }

    /**
     * 设置 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

    /**
     * 获取 [按组分配税额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return this.amount_by_groupDirtyFlag ;
    }

    /**
     * 获取 [支付挂件]
     */
    @JsonProperty("payments_widget")
    public String getPayments_widget(){
        return this.payments_widget ;
    }

    /**
     * 设置 [支付挂件]
     */
    @JsonProperty("payments_widget")
    public void setPayments_widget(String  payments_widget){
        this.payments_widget = payments_widget ;
        this.payments_widgetDirtyFlag = true ;
    }

    /**
     * 获取 [支付挂件]脏标记
     */
    @JsonIgnore
    public boolean getPayments_widgetDirtyFlag(){
        return this.payments_widgetDirtyFlag ;
    }

    /**
     * 获取 [已付／已核销]
     */
    @JsonProperty("reconciled")
    public String getReconciled(){
        return this.reconciled ;
    }

    /**
     * 设置 [已付／已核销]
     */
    @JsonProperty("reconciled")
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.reconciledDirtyFlag = true ;
    }

    /**
     * 获取 [已付／已核销]脏标记
     */
    @JsonIgnore
    public boolean getReconciledDirtyFlag(){
        return this.reconciledDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [已汇]
     */
    @JsonProperty("sent")
    public String getSent(){
        return this.sent ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("sent")
    public void setSent(String  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }

    /**
     * 获取 [以发票币种总计]
     */
    @JsonProperty("amount_total_signed")
    public Double getAmount_total_signed(){
        return this.amount_total_signed ;
    }

    /**
     * 设置 [以发票币种总计]
     */
    @JsonProperty("amount_total_signed")
    public void setAmount_total_signed(Double  amount_total_signed){
        this.amount_total_signed = amount_total_signed ;
        this.amount_total_signedDirtyFlag = true ;
    }

    /**
     * 获取 [以发票币种总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_signedDirtyFlag(){
        return this.amount_total_signedDirtyFlag ;
    }

    /**
     * 获取 [发票行]
     */
    @JsonProperty("invoice_line_ids")
    public String getInvoice_line_ids(){
        return this.invoice_line_ids ;
    }

    /**
     * 设置 [发票行]
     */
    @JsonProperty("invoice_line_ids")
    public void setInvoice_line_ids(String  invoice_line_ids){
        this.invoice_line_ids = invoice_line_ids ;
        this.invoice_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [发票行]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_idsDirtyFlag(){
        return this.invoice_line_idsDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }

    /**
     * 获取 [发票使用币种的逾期金额]
     */
    @JsonProperty("residual_signed")
    public Double getResidual_signed(){
        return this.residual_signed ;
    }

    /**
     * 设置 [发票使用币种的逾期金额]
     */
    @JsonProperty("residual_signed")
    public void setResidual_signed(Double  residual_signed){
        this.residual_signed = residual_signed ;
        this.residual_signedDirtyFlag = true ;
    }

    /**
     * 获取 [发票使用币种的逾期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidual_signedDirtyFlag(){
        return this.residual_signedDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }

    /**
     * 获取 [供应商名称]
     */
    @JsonProperty("vendor_display_name")
    public String getVendor_display_name(){
        return this.vendor_display_name ;
    }

    /**
     * 设置 [供应商名称]
     */
    @JsonProperty("vendor_display_name")
    public void setVendor_display_name(String  vendor_display_name){
        this.vendor_display_name = vendor_display_name ;
        this.vendor_display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [供应商名称]脏标记
     */
    @JsonIgnore
    public boolean getVendor_display_nameDirtyFlag(){
        return this.vendor_display_nameDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return this.amount_untaxed ;
    }

    /**
     * 设置 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

    /**
     * 获取 [未税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return this.amount_untaxedDirtyFlag ;
    }

    /**
     * 获取 [交易]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return this.transaction_ids ;
    }

    /**
     * 设置 [交易]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [交易]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return this.transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [到期日期]
     */
    @JsonProperty("date_due")
    public Timestamp getDate_due(){
        return this.date_due ;
    }

    /**
     * 设置 [到期日期]
     */
    @JsonProperty("date_due")
    public void setDate_due(Timestamp  date_due){
        this.date_due = date_due ;
        this.date_dueDirtyFlag = true ;
    }

    /**
     * 获取 [到期日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_dueDirtyFlag(){
        return this.date_dueDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return this.authorized_transaction_ids ;
    }

    /**
     * 设置 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [已授权的交易]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return this.authorized_transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [税率明细行]
     */
    @JsonProperty("tax_line_ids")
    public String getTax_line_ids(){
        return this.tax_line_ids ;
    }

    /**
     * 设置 [税率明细行]
     */
    @JsonProperty("tax_line_ids")
    public void setTax_line_ids(String  tax_line_ids){
        this.tax_line_ids = tax_line_ids ;
        this.tax_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [税率明细行]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_idsDirtyFlag(){
        return this.tax_line_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [源邮箱]
     */
    @JsonProperty("source_email")
    public String getSource_email(){
        return this.source_email ;
    }

    /**
     * 设置 [源邮箱]
     */
    @JsonProperty("source_email")
    public void setSource_email(String  source_email){
        this.source_email = source_email ;
        this.source_emailDirtyFlag = true ;
    }

    /**
     * 获取 [源邮箱]脏标记
     */
    @JsonIgnore
    public boolean getSource_emailDirtyFlag(){
        return this.source_emailDirtyFlag ;
    }

    /**
     * 获取 [税率]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return this.amount_tax ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

    /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return this.amount_taxDirtyFlag ;
    }

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [Untaxed Amount in Invoice Currency]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public Double getAmount_untaxed_invoice_signed(){
        return this.amount_untaxed_invoice_signed ;
    }

    /**
     * 设置 [Untaxed Amount in Invoice Currency]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public void setAmount_untaxed_invoice_signed(Double  amount_untaxed_invoice_signed){
        this.amount_untaxed_invoice_signed = amount_untaxed_invoice_signed ;
        this.amount_untaxed_invoice_signedDirtyFlag = true ;
    }

    /**
     * 获取 [Untaxed Amount in Invoice Currency]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_invoice_signedDirtyFlag(){
        return this.amount_untaxed_invoice_signedDirtyFlag ;
    }

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [下一个号码前缀]
     */
    @JsonProperty("sequence_number_next_prefix")
    public String getSequence_number_next_prefix(){
        return this.sequence_number_next_prefix ;
    }

    /**
     * 设置 [下一个号码前缀]
     */
    @JsonProperty("sequence_number_next_prefix")
    public void setSequence_number_next_prefix(String  sequence_number_next_prefix){
        this.sequence_number_next_prefix = sequence_number_next_prefix ;
        this.sequence_number_next_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [下一个号码前缀]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_next_prefixDirtyFlag(){
        return this.sequence_number_next_prefixDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [退款发票]
     */
    @JsonProperty("refund_invoice_ids")
    public String getRefund_invoice_ids(){
        return this.refund_invoice_ids ;
    }

    /**
     * 设置 [退款发票]
     */
    @JsonProperty("refund_invoice_ids")
    public void setRefund_invoice_ids(String  refund_invoice_ids){
        this.refund_invoice_ids = refund_invoice_ids ;
        this.refund_invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [退款发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idsDirtyFlag(){
        return this.refund_invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [公司使用币种的逾期金额]
     */
    @JsonProperty("residual_company_signed")
    public Double getResidual_company_signed(){
        return this.residual_company_signed ;
    }

    /**
     * 设置 [公司使用币种的逾期金额]
     */
    @JsonProperty("residual_company_signed")
    public void setResidual_company_signed(Double  residual_company_signed){
        this.residual_company_signed = residual_company_signed ;
        this.residual_company_signedDirtyFlag = true ;
    }

    /**
     * 获取 [公司使用币种的逾期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidual_company_signedDirtyFlag(){
        return this.residual_company_signedDirtyFlag ;
    }

    /**
     * 获取 [开票日期]
     */
    @JsonProperty("date_invoice")
    public Timestamp getDate_invoice(){
        return this.date_invoice ;
    }

    /**
     * 设置 [开票日期]
     */
    @JsonProperty("date_invoice")
    public void setDate_invoice(Timestamp  date_invoice){
        this.date_invoice = date_invoice ;
        this.date_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [开票日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_invoiceDirtyFlag(){
        return this.date_invoiceDirtyFlag ;
    }

    /**
     * 获取 [参考/说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [参考/说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [参考/说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [付款参考:]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [付款参考:]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [付款参考:]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }

    /**
     * 获取 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public String getSequence_number_next(){
        return this.sequence_number_next ;
    }

    /**
     * 设置 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(String  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [下一号码]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return this.sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [Tax in Invoice Currency]
     */
    @JsonProperty("amount_tax_signed")
    public Double getAmount_tax_signed(){
        return this.amount_tax_signed ;
    }

    /**
     * 设置 [Tax in Invoice Currency]
     */
    @JsonProperty("amount_tax_signed")
    public void setAmount_tax_signed(Double  amount_tax_signed){
        this.amount_tax_signed = amount_tax_signed ;
        this.amount_tax_signedDirtyFlag = true ;
    }

    /**
     * 获取 [Tax in Invoice Currency]脏标记
     */
    @JsonIgnore
    public boolean getAmount_tax_signedDirtyFlag(){
        return this.amount_tax_signedDirtyFlag ;
    }

    /**
     * 获取 [发票标示]
     */
    @JsonProperty("invoice_icon")
    public String getInvoice_icon(){
        return this.invoice_icon ;
    }

    /**
     * 设置 [发票标示]
     */
    @JsonProperty("invoice_icon")
    public void setInvoice_icon(String  invoice_icon){
        this.invoice_icon = invoice_icon ;
        this.invoice_iconDirtyFlag = true ;
    }

    /**
     * 获取 [发票标示]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_iconDirtyFlag(){
        return this.invoice_iconDirtyFlag ;
    }

    /**
     * 获取 [付款]
     */
    @JsonProperty("payment_ids")
    public String getPayment_ids(){
        return this.payment_ids ;
    }

    /**
     * 设置 [付款]
     */
    @JsonProperty("payment_ids")
    public void setPayment_ids(String  payment_ids){
        this.payment_ids = payment_ids ;
        this.payment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idsDirtyFlag(){
        return this.payment_idsDirtyFlag ;
    }

    /**
     * 获取 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [总计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [添加采购订单]
     */
    @JsonProperty("purchase_id_text")
    public String getPurchase_id_text(){
        return this.purchase_id_text ;
    }

    /**
     * 设置 [添加采购订单]
     */
    @JsonProperty("purchase_id_text")
    public void setPurchase_id_text(String  purchase_id_text){
        this.purchase_id_text = purchase_id_text ;
        this.purchase_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [添加采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_id_textDirtyFlag(){
        return this.purchase_id_textDirtyFlag ;
    }

    /**
     * 获取 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public String getVendor_bill_purchase_id_text(){
        return this.vendor_bill_purchase_id_text ;
    }

    /**
     * 设置 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public void setVendor_bill_purchase_id_text(String  vendor_bill_purchase_id_text){
        this.vendor_bill_purchase_id_text = vendor_bill_purchase_id_text ;
        this.vendor_bill_purchase_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [自动完成]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_id_textDirtyFlag(){
        return this.vendor_bill_purchase_id_textDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return this.partner_shipping_id_text ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return this.partner_shipping_id_textDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterms_id_text")
    public String getIncoterms_id_text(){
        return this.incoterms_id_text ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterms_id_text")
    public void setIncoterms_id_text(String  incoterms_id_text){
        this.incoterms_id_text = incoterms_id_text ;
        this.incoterms_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_id_textDirtyFlag(){
        return this.incoterms_id_textDirtyFlag ;
    }

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }

    /**
     * 获取 [国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public String getIncoterm_id_text(){
        return this.incoterm_id_text ;
    }

    /**
     * 设置 [国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public void setIncoterm_id_text(String  incoterm_id_text){
        this.incoterm_id_text = incoterm_id_text ;
        this.incoterm_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_id_textDirtyFlag(){
        return this.incoterm_id_textDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id_text")
    public String getCash_rounding_id_text(){
        return this.cash_rounding_id_text ;
    }

    /**
     * 设置 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id_text")
    public void setCash_rounding_id_text(String  cash_rounding_id_text){
        this.cash_rounding_id_text = cash_rounding_id_text ;
        this.cash_rounding_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [现金舍入方式]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_id_textDirtyFlag(){
        return this.cash_rounding_id_textDirtyFlag ;
    }

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public String getVendor_bill_id_text(){
        return this.vendor_bill_id_text ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public void setVendor_bill_id_text(String  vendor_bill_id_text){
        this.vendor_bill_id_text = vendor_bill_id_text ;
        this.vendor_bill_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_id_textDirtyFlag(){
        return this.vendor_bill_id_textDirtyFlag ;
    }

    /**
     * 获取 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id_text")
    public String getRefund_invoice_id_text(){
        return this.refund_invoice_id_text ;
    }

    /**
     * 设置 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id_text")
    public void setRefund_invoice_id_text(String  refund_invoice_id_text){
        this.refund_invoice_id_text = refund_invoice_id_text ;
        this.refund_invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_id_textDirtyFlag(){
        return this.refund_invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [号码]
     */
    @JsonProperty("number")
    public String getNumber(){
        return this.number ;
    }

    /**
     * 设置 [号码]
     */
    @JsonProperty("number")
    public void setNumber(String  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

    /**
     * 获取 [号码]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return this.numberDirtyFlag ;
    }

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return this.payment_term_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return this.payment_term_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [日记账分录]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [日记账分录]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return this.payment_term_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return this.payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }

    /**
     * 获取 [国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public Integer getIncoterm_id(){
        return this.incoterm_id ;
    }

    /**
     * 设置 [国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public void setIncoterm_id(Integer  incoterm_id){
        this.incoterm_id = incoterm_id ;
        this.incoterm_idDirtyFlag = true ;
    }

    /**
     * 获取 [国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_idDirtyFlag(){
        return this.incoterm_idDirtyFlag ;
    }

    /**
     * 获取 [添加采购订单]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return this.purchase_id ;
    }

    /**
     * 设置 [添加采购订单]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [添加采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return this.purchase_idDirtyFlag ;
    }

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id")
    public Integer getRefund_invoice_id(){
        return this.refund_invoice_id ;
    }

    /**
     * 设置 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id")
    public void setRefund_invoice_id(Integer  refund_invoice_id){
        this.refund_invoice_id = refund_invoice_id ;
        this.refund_invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idDirtyFlag(){
        return this.refund_invoice_idDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public Integer getPartner_bank_id(){
        return this.partner_bank_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public void setPartner_bank_id(Integer  partner_bank_id){
        this.partner_bank_id = partner_bank_id ;
        this.partner_bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_idDirtyFlag(){
        return this.partner_bank_idDirtyFlag ;
    }

    /**
     * 获取 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id")
    public Integer getCash_rounding_id(){
        return this.cash_rounding_id ;
    }

    /**
     * 设置 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id")
    public void setCash_rounding_id(Integer  cash_rounding_id){
        this.cash_rounding_id = cash_rounding_id ;
        this.cash_rounding_idDirtyFlag = true ;
    }

    /**
     * 获取 [现金舍入方式]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_idDirtyFlag(){
        return this.cash_rounding_idDirtyFlag ;
    }

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return this.partner_shipping_id ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

    /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return this.partner_shipping_idDirtyFlag ;
    }

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterms_id")
    public Integer getIncoterms_id(){
        return this.incoterms_id ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterms_id")
    public void setIncoterms_id(Integer  incoterms_id){
        this.incoterms_id = incoterms_id ;
        this.incoterms_idDirtyFlag = true ;
    }

    /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_idDirtyFlag(){
        return this.incoterms_idDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public Integer getVendor_bill_id(){
        return this.vendor_bill_id ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public void setVendor_bill_id(Integer  vendor_bill_id){
        this.vendor_bill_id = vendor_bill_id ;
        this.vendor_bill_idDirtyFlag = true ;
    }

    /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_idDirtyFlag(){
        return this.vendor_bill_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public Integer getVendor_bill_purchase_id(){
        return this.vendor_bill_purchase_id ;
    }

    /**
     * 设置 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public void setVendor_bill_purchase_id(Integer  vendor_bill_purchase_id){
        this.vendor_bill_purchase_id = vendor_bill_purchase_id ;
        this.vendor_bill_purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [自动完成]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_idDirtyFlag(){
        return this.vendor_bill_purchase_idDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }



}

package cn.ibizlab.odoo.web.odoo_account.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.web.odoo_account.feign.Account_paymentFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Account_paymentService {

    Account_paymentFeignClient client;

    @Autowired
    public Account_paymentService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_paymentFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_paymentFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean checkKey(Account_payment account_payment) {
        return client.checkKey(account_payment);
    }

    public Account_payment update(Integer account_payment_id, Account_payment account_payment) {
        return client.update(account_payment_id, account_payment);
    }

    public Account_payment getDraft(Integer account_payment_id, Account_payment account_payment) {
        return client.getDraft(account_payment_id, account_payment);
    }

    public boolean remove( Integer account_payment_id) {
        return client.remove( account_payment_id);
    }

    public Account_payment removeBatch(Integer account_payment_id, Account_payment account_payment) {
        return client.removeBatch(account_payment_id, account_payment);
    }

    public Account_payment save(Integer account_payment_id, Account_payment account_payment) {
        return client.save(account_payment_id, account_payment);
    }

    public Account_payment updateBatch(Integer account_payment_id, Account_payment account_payment) {
        return client.updateBatch(account_payment_id, account_payment);
    }

	public Account_payment create(Account_payment account_payment) {
        return client.create(account_payment);
    }

    public Account_payment get( Integer account_payment_id) {
        return client.get( account_payment_id);
    }

    public Account_payment createBatch(Integer account_payment_id, Account_payment account_payment) {
        return client.createBatch(account_payment_id, account_payment);
    }

	public Page<Account_payment> fetchDefault(Account_paymentSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }


    public boolean checkKeyByRes_partner(Integer res_partner_id, Account_payment account_payment) {
        return client.checkKeyByRes_partner(res_partner_id, account_payment);
    }

    public Account_payment updateByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.updateByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

    public Account_payment getDraftByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.getDraftByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

    public boolean removeByRes_partner(Integer res_partner_id,  Integer account_payment_id) {
        return client.removeByRes_partner(res_partner_id,  account_payment_id);
    }

    public Account_payment removeBatchByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.removeBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

    public Account_payment saveByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.saveByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

    public Account_payment updateBatchByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.updateBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

	public Account_payment createByRes_partner(Integer res_partner_id, Account_payment account_payment) {
        return client.createByRes_partner(res_partner_id, account_payment);
    }

    public Account_payment getByRes_partner(Integer res_partner_id,  Integer account_payment_id) {
        return client.getByRes_partner(res_partner_id,  account_payment_id);
    }

    public Account_payment createBatchByRes_partner(Integer res_partner_id, Integer account_payment_id, Account_payment account_payment) {
        return client.createBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
    }

	public Page<Account_payment> fetchDefaultByRes_partner(Integer res_partner_id , Account_paymentSearchContext searchContext) {
        return client.fetchDefaultByRes_partner(res_partner_id , searchContext);
    }

}

package cn.ibizlab.odoo.web.odoo_product.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[产品]
 */
public class Product_product implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 变种卖家
     */
    private String variant_seller_ids;

    @JsonIgnore
    private boolean variant_seller_idsDirtyFlag;
    
    /**
     * 模板属性值
     */
    private String product_template_attribute_value_ids;

    @JsonIgnore
    private boolean product_template_attribute_value_idsDirtyFlag;
    
    /**
     * 产品
     */
    private String product_variant_ids;

    @JsonIgnore
    private boolean product_variant_idsDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * 体积
     */
    private Double volume;

    @JsonIgnore
    private boolean volumeDirtyFlag;
    
    /**
     * 标价
     */
    private Double lst_price;

    @JsonIgnore
    private boolean lst_priceDirtyFlag;
    
    /**
     * 有效的产品属性
     */
    private String valid_product_attribute_ids;

    @JsonIgnore
    private boolean valid_product_attribute_idsDirtyFlag;
    
    /**
     * 库存FIFO手工凭证
     */
    private String stock_fifo_manual_move_ids;

    @JsonIgnore
    private boolean stock_fifo_manual_move_idsDirtyFlag;
    
    /**
     * 即时库存
     */
    private String stock_quant_ids;

    @JsonIgnore
    private boolean stock_quant_idsDirtyFlag;
    
    /**
     * 进项税
     */
    private String supplier_taxes_id;

    @JsonIgnore
    private boolean supplier_taxes_idDirtyFlag;
    
    /**
     * 价格表明细
     */
    private String pricelist_item_ids;

    @JsonIgnore
    private boolean pricelist_item_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 附件产品
     */
    private String accessory_product_ids;

    @JsonIgnore
    private boolean accessory_product_idsDirtyFlag;
    
    /**
     * 供应商
     */
    private String seller_ids;

    @JsonIgnore
    private boolean seller_idsDirtyFlag;
    
    /**
     * Valid Product Attribute Values Without No Variant Attributes
     */
    private String valid_product_attribute_value_wnva_ids;

    @JsonIgnore
    private boolean valid_product_attribute_value_wnva_idsDirtyFlag;
    
    /**
     * 客户单号
     */
    private String partner_ref;

    @JsonIgnore
    private boolean partner_refDirtyFlag;
    
    /**
     * 图片
     */
    private String product_image_ids;

    @JsonIgnore
    private boolean product_image_idsDirtyFlag;
    
    /**
     * 已生产
     */
    private Double mrp_product_qty;

    @JsonIgnore
    private boolean mrp_product_qtyDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * Valid Product Attribute Lines
     */
    private String valid_product_template_attribute_line_ids;

    @JsonIgnore
    private boolean valid_product_template_attribute_line_idsDirtyFlag;
    
    /**
     * 下一活动类型
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 网站产品目录
     */
    private String public_categ_ids;

    @JsonIgnore
    private boolean public_categ_idsDirtyFlag;
    
    /**
     * 活动入场券
     */
    private String event_ticket_ids;

    @JsonIgnore
    private boolean event_ticket_idsDirtyFlag;
    
    /**
     * 价格
     */
    private Double price;

    @JsonIgnore
    private boolean priceDirtyFlag;
    
    /**
     * 产品属性
     */
    private String attribute_line_ids;

    @JsonIgnore
    private boolean attribute_line_idsDirtyFlag;
    
    /**
     * 预测数量
     */
    private Double virtual_available;

    @JsonIgnore
    private boolean virtual_availableDirtyFlag;
    
    /**
     * 订货规则
     */
    private Integer nbr_reordering_rules;

    @JsonIgnore
    private boolean nbr_reordering_rulesDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 是关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 网站价格差异
     */
    private String website_price_difference;

    @JsonIgnore
    private boolean website_price_differenceDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 购物车数量
     */
    private Integer cart_qty;

    @JsonIgnore
    private boolean cart_qtyDirtyFlag;
    
    /**
     * 网站公开价格
     */
    private Double website_public_price;

    @JsonIgnore
    private boolean website_public_priceDirtyFlag;
    
    /**
     * 评级
     */
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;
    
    /**
     * BOM组件
     */
    private String bom_line_ids;

    @JsonIgnore
    private boolean bom_line_idsDirtyFlag;
    
    /**
     * 网站价格
     */
    private Double website_price;

    @JsonIgnore
    private boolean website_priceDirtyFlag;
    
    /**
     * 出向
     */
    private Double outgoing_qty;

    @JsonIgnore
    private boolean outgoing_qtyDirtyFlag;
    
    /**
     * 已售出
     */
    private Double sales_count;

    @JsonIgnore
    private boolean sales_countDirtyFlag;
    
    /**
     * Valid Product Attributes Without No Variant Attributes
     */
    private String valid_product_attribute_wnva_ids;

    @JsonIgnore
    private boolean valid_product_attribute_wnva_idsDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;
    
    /**
     * Valid Existing Variants
     */
    private String valid_existing_variant_ids;

    @JsonIgnore
    private boolean valid_existing_variant_idsDirtyFlag;
    
    /**
     * 库存货币价值
     */
    private Integer stock_value_currency_id;

    @JsonIgnore
    private boolean stock_value_currency_idDirtyFlag;
    
    /**
     * 值
     */
    private Double stock_value;

    @JsonIgnore
    private boolean stock_valueDirtyFlag;
    
    /**
     * 样式
     */
    private String website_style_ids;

    @JsonIgnore
    private boolean website_style_idsDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 重量
     */
    private Double weight;

    @JsonIgnore
    private boolean weightDirtyFlag;
    
    /**
     * 物料清单
     */
    private String bom_ids;

    @JsonIgnore
    private boolean bom_idsDirtyFlag;
    
    /**
     * 错误数
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 活动状态
     */
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;
    
    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     */
    private String valid_product_template_attribute_line_wnva_ids;

    @JsonIgnore
    private boolean valid_product_template_attribute_line_wnva_idsDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 有效的产品属性值
     */
    private String valid_product_attribute_value_ids;

    @JsonIgnore
    private boolean valid_product_attribute_value_idsDirtyFlag;
    
    /**
     * 在手数量
     */
    private Double qty_available;

    @JsonIgnore
    private boolean qty_availableDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * 变体图像
     */
    private byte[] image_variant;

    @JsonIgnore
    private boolean image_variantDirtyFlag;
    
    /**
     * 库存移动
     */
    private String stock_move_ids;

    @JsonIgnore
    private boolean stock_move_idsDirtyFlag;
    
    /**
     * 需要采取行动
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 网站信息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * 库存FIFO实时计价
     */
    private String stock_fifo_real_time_aml_ids;

    @JsonIgnore
    private boolean stock_fifo_real_time_aml_idsDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 参照
     */
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;
    
    /**
     * 重订货最小数量
     */
    private Double reordering_min_qty;

    @JsonIgnore
    private boolean reordering_min_qtyDirtyFlag;
    
    /**
     * 大尺寸图像
     */
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;
    
    /**
     * 路线
     */
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;
    
    /**
     * 销项税
     */
    private String taxes_id;

    @JsonIgnore
    private boolean taxes_idDirtyFlag;
    
    /**
     * # 物料清单
     */
    private Integer bom_count;

    @JsonIgnore
    private boolean bom_countDirtyFlag;
    
    /**
     * 动作数量
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 产品包裹
     */
    private String packaging_ids;

    @JsonIgnore
    private boolean packaging_idsDirtyFlag;
    
    /**
     * Valid Archived Variants
     */
    private String valid_archived_variant_ids;

    @JsonIgnore
    private boolean valid_archived_variant_idsDirtyFlag;
    
    /**
     * 责任用户
     */
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;
    
    /**
     * 价格表项目
     */
    private String item_ids;

    @JsonIgnore
    private boolean item_idsDirtyFlag;
    
    /**
     * 已采购
     */
    private Double purchased_product_qty;

    @JsonIgnore
    private boolean purchased_product_qtyDirtyFlag;
    
    /**
     * 重订货最大数量
     */
    private Double reordering_max_qty;

    @JsonIgnore
    private boolean reordering_max_qtyDirtyFlag;
    
    /**
     * 最小库存规则
     */
    private String orderpoint_ids;

    @JsonIgnore
    private boolean orderpoint_idsDirtyFlag;
    
    /**
     * 可选产品
     */
    private String optional_product_ids;

    @JsonIgnore
    private boolean optional_product_idsDirtyFlag;
    
    /**
     * 是产品变体
     */
    private String is_product_variant;

    @JsonIgnore
    private boolean is_product_variantDirtyFlag;
    
    /**
     * # BOM 使用的地方
     */
    private Integer used_in_bom_count;

    @JsonIgnore
    private boolean used_in_bom_countDirtyFlag;
    
    /**
     * 数量
     */
    private Double qty_at_date;

    @JsonIgnore
    private boolean qty_at_dateDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 条码
     */
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 成本
     */
    private Double standard_price;

    @JsonIgnore
    private boolean standard_priceDirtyFlag;
    
    /**
     * 属性值
     */
    private String attribute_value_ids;

    @JsonIgnore
    private boolean attribute_value_idsDirtyFlag;
    
    /**
     * 变体价格额外
     */
    private Double price_extra;

    @JsonIgnore
    private boolean price_extraDirtyFlag;
    
    /**
     * BOM产品变体.
     */
    private String variant_bom_ids;

    @JsonIgnore
    private boolean variant_bom_idsDirtyFlag;
    
    /**
     * 替代产品
     */
    private String alternative_product_ids;

    @JsonIgnore
    private boolean alternative_product_idsDirtyFlag;
    
    /**
     * 内部参考
     */
    private String default_code;

    @JsonIgnore
    private boolean default_codeDirtyFlag;
    
    /**
     * 类别路线
     */
    private String route_from_categ_ids;

    @JsonIgnore
    private boolean route_from_categ_idsDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;
    
    /**
     * 入库
     */
    private Double incoming_qty;

    @JsonIgnore
    private boolean incoming_qtyDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 追踪
     */
    private String tracking;

    @JsonIgnore
    private boolean trackingDirtyFlag;
    
    /**
     * 拣货说明
     */
    private String description_picking;

    @JsonIgnore
    private boolean description_pickingDirtyFlag;
    
    /**
     * 库存出货科目
     */
    private Integer property_stock_account_output;

    @JsonIgnore
    private boolean property_stock_account_outputDirtyFlag;
    
    /**
     * 销售
     */
    private String sale_ok;

    @JsonIgnore
    private boolean sale_okDirtyFlag;
    
    /**
     * 网站的说明
     */
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 称重
     */
    private String to_weight;

    @JsonIgnore
    private boolean to_weightDirtyFlag;
    
    /**
     * 说明
     */
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;
    
    /**
     * 收货说明
     */
    private String description_pickingin;

    @JsonIgnore
    private boolean description_pickinginDirtyFlag;
    
    /**
     * 销售价格
     */
    private Double list_price;

    @JsonIgnore
    private boolean list_priceDirtyFlag;
    
    /**
     * 隐藏费用政策
     */
    private String hide_expense_policy;

    @JsonIgnore
    private boolean hide_expense_policyDirtyFlag;
    
    /**
     * 销售说明
     */
    private String description_sale;

    @JsonIgnore
    private boolean description_saleDirtyFlag;
    
    /**
     * 成本方法
     */
    private String cost_method;

    @JsonIgnore
    private boolean cost_methodDirtyFlag;
    
    /**
     * 序号
     */
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;
    
    /**
     * 销售订单行消息
     */
    private String sale_line_warn_msg;

    @JsonIgnore
    private boolean sale_line_warn_msgDirtyFlag;
    
    /**
     * 仓库
     */
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;
    
    /**
     * 出租
     */
    private String rental;

    @JsonIgnore
    private boolean rentalDirtyFlag;
    
    /**
     * 价格差异科目
     */
    private Integer property_account_creditor_price_difference;

    @JsonIgnore
    private boolean property_account_creditor_price_differenceDirtyFlag;
    
    /**
     * 重量计量单位标签
     */
    private String weight_uom_name;

    @JsonIgnore
    private boolean weight_uom_nameDirtyFlag;
    
    /**
     * 成本币种
     */
    private Integer cost_currency_id;

    @JsonIgnore
    private boolean cost_currency_idDirtyFlag;
    
    /**
     * 库存进货科目
     */
    private Integer property_stock_account_input;

    @JsonIgnore
    private boolean property_stock_account_inputDirtyFlag;
    
    /**
     * 名称
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 制造提前期(日)
     */
    private Double produce_delay;

    @JsonIgnore
    private boolean produce_delayDirtyFlag;
    
    /**
     * SEO优化
     */
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 网站网址
     */
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;
    
    /**
     * 最新反馈评级
     */
    private String rating_last_feedback;

    @JsonIgnore
    private boolean rating_last_feedbackDirtyFlag;
    
    /**
     * 尺寸 Y
     */
    private Integer website_size_y;

    @JsonIgnore
    private boolean website_size_yDirtyFlag;
    
    /**
     * 是一张活动票吗？
     */
    private String event_ok;

    @JsonIgnore
    private boolean event_okDirtyFlag;
    
    /**
     * 库存可用性
     */
    private String inventory_availability;

    @JsonIgnore
    private boolean inventory_availabilityDirtyFlag;
    
    /**
     * 采购
     */
    private String purchase_ok;

    @JsonIgnore
    private boolean purchase_okDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 最新值评级
     */
    private Double rating_last_value;

    @JsonIgnore
    private boolean rating_last_valueDirtyFlag;
    
    /**
     * 网站meta标题
     */
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;
    
    /**
     * 最新图像评级
     */
    private byte[] rating_last_image;

    @JsonIgnore
    private boolean rating_last_imageDirtyFlag;
    
    /**
     * 采购说明
     */
    private String description_purchase;

    @JsonIgnore
    private boolean description_purchaseDirtyFlag;
    
    /**
     * 网站
     */
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;
    
    /**
     * 报销
     */
    private String can_be_expensed;

    @JsonIgnore
    private boolean can_be_expensedDirtyFlag;
    
    /**
     * 销售订单行
     */
    private String sale_line_warn;

    @JsonIgnore
    private boolean sale_line_warnDirtyFlag;
    
    /**
     * 尺寸 X
     */
    private Integer website_size_x;

    @JsonIgnore
    private boolean website_size_xDirtyFlag;
    
    /**
     * 自动采购
     */
    private String service_to_purchase;

    @JsonIgnore
    private boolean service_to_purchaseDirtyFlag;
    
    /**
     * 网站序列
     */
    private Integer website_sequence;

    @JsonIgnore
    private boolean website_sequenceDirtyFlag;
    
    /**
     * 库存位置
     */
    private Integer property_stock_inventory;

    @JsonIgnore
    private boolean property_stock_inventoryDirtyFlag;
    
    /**
     * 地点
     */
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;
    
    /**
     * 库存计价
     */
    private String property_valuation;

    @JsonIgnore
    private boolean property_valuationDirtyFlag;
    
    /**
     * 已发布
     */
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;
    
    /**
     * 重开收据规则
     */
    private String expense_policy;

    @JsonIgnore
    private boolean expense_policyDirtyFlag;
    
    /**
     * 测量的重量单位
     */
    private Integer weight_uom_id;

    @JsonIgnore
    private boolean weight_uom_idDirtyFlag;
    
    /**
     * 颜色索引
     */
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;
    
    /**
     * 生产位置
     */
    private Integer property_stock_production;

    @JsonIgnore
    private boolean property_stock_productionDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 出库单说明
     */
    private String description_pickingout;

    @JsonIgnore
    private boolean description_pickingoutDirtyFlag;
    
    /**
     * 价格表
     */
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;
    
    /**
     * 评级数
     */
    private Integer rating_count;

    @JsonIgnore
    private boolean rating_countDirtyFlag;
    
    /**
     * 网站元说明
     */
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 计价
     */
    private String valuation;

    @JsonIgnore
    private boolean valuationDirtyFlag;
    
    /**
     * 开票策略
     */
    private String invoice_policy;

    @JsonIgnore
    private boolean invoice_policyDirtyFlag;
    
    /**
     * 采购订单明细的消息
     */
    private String purchase_line_warn_msg;

    @JsonIgnore
    private boolean purchase_line_warn_msgDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 收入科目
     */
    private Integer property_account_income_id;

    @JsonIgnore
    private boolean property_account_income_idDirtyFlag;
    
    /**
     * 成本方法
     */
    private String property_cost_method;

    @JsonIgnore
    private boolean property_cost_methodDirtyFlag;
    
    /**
     * 产品种类
     */
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;
    
    /**
     * Can be Part
     */
    private String isParts;

    @JsonIgnore
    private boolean isPartsDirtyFlag;
    
    /**
     * 计量单位
     */
    private Integer uom_id;

    @JsonIgnore
    private boolean uom_idDirtyFlag;
    
    /**
     * 产品
     */
    private Integer product_variant_id;

    @JsonIgnore
    private boolean product_variant_idDirtyFlag;
    
    /**
     * 产品类型
     */
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;
    
    /**
     * 控制策略
     */
    private String purchase_method;

    @JsonIgnore
    private boolean purchase_methodDirtyFlag;
    
    /**
     * 负责人
     */
    private Integer responsible_id;

    @JsonIgnore
    private boolean responsible_idDirtyFlag;
    
    /**
     * 跟踪服务
     */
    private String service_type;

    @JsonIgnore
    private boolean service_typeDirtyFlag;
    
    /**
     * 单位名称
     */
    private String uom_name;

    @JsonIgnore
    private boolean uom_nameDirtyFlag;
    
    /**
     * 可用阈值
     */
    private Double available_threshold;

    @JsonIgnore
    private boolean available_thresholdDirtyFlag;
    
    /**
     * 采购订单行
     */
    private String purchase_line_warn;

    @JsonIgnore
    private boolean purchase_line_warnDirtyFlag;
    
    /**
     * # 产品变体
     */
    private Integer product_variant_count;

    @JsonIgnore
    private boolean product_variant_countDirtyFlag;
    
    /**
     * POS类别
     */
    private Integer pos_categ_id;

    @JsonIgnore
    private boolean pos_categ_idDirtyFlag;
    
    /**
     * 自定义消息
     */
    private String custom_message;

    @JsonIgnore
    private boolean custom_messageDirtyFlag;
    
    /**
     * 费用科目
     */
    private Integer property_account_expense_id;

    @JsonIgnore
    private boolean property_account_expense_idDirtyFlag;
    
    /**
     * 客户前置时间
     */
    private Double sale_delay;

    @JsonIgnore
    private boolean sale_delayDirtyFlag;
    
    /**
     * 采购计量单位
     */
    private Integer uom_po_id;

    @JsonIgnore
    private boolean uom_po_idDirtyFlag;
    
    /**
     * POS可用
     */
    private String available_in_pos;

    @JsonIgnore
    private boolean available_in_posDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 产品模板
     */
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;
    

    /**
     * 获取 [变种卖家]
     */
    @JsonProperty("variant_seller_ids")
    public String getVariant_seller_ids(){
        return this.variant_seller_ids ;
    }

    /**
     * 设置 [变种卖家]
     */
    @JsonProperty("variant_seller_ids")
    public void setVariant_seller_ids(String  variant_seller_ids){
        this.variant_seller_ids = variant_seller_ids ;
        this.variant_seller_idsDirtyFlag = true ;
    }

    /**
     * 获取 [变种卖家]脏标记
     */
    @JsonIgnore
    public boolean getVariant_seller_idsDirtyFlag(){
        return this.variant_seller_idsDirtyFlag ;
    }

    /**
     * 获取 [模板属性值]
     */
    @JsonProperty("product_template_attribute_value_ids")
    public String getProduct_template_attribute_value_ids(){
        return this.product_template_attribute_value_ids ;
    }

    /**
     * 设置 [模板属性值]
     */
    @JsonProperty("product_template_attribute_value_ids")
    public void setProduct_template_attribute_value_ids(String  product_template_attribute_value_ids){
        this.product_template_attribute_value_ids = product_template_attribute_value_ids ;
        this.product_template_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [模板属性值]脏标记
     */
    @JsonIgnore
    public boolean getProduct_template_attribute_value_idsDirtyFlag(){
        return this.product_template_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_variant_ids")
    public String getProduct_variant_ids(){
        return this.product_variant_ids ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_variant_ids")
    public void setProduct_variant_ids(String  product_variant_ids){
        this.product_variant_ids = product_variant_ids ;
        this.product_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_idsDirtyFlag(){
        return this.product_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [体积]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return this.volume ;
    }

    /**
     * 设置 [体积]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

    /**
     * 获取 [体积]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return this.volumeDirtyFlag ;
    }

    /**
     * 获取 [标价]
     */
    @JsonProperty("lst_price")
    public Double getLst_price(){
        return this.lst_price ;
    }

    /**
     * 设置 [标价]
     */
    @JsonProperty("lst_price")
    public void setLst_price(Double  lst_price){
        this.lst_price = lst_price ;
        this.lst_priceDirtyFlag = true ;
    }

    /**
     * 获取 [标价]脏标记
     */
    @JsonIgnore
    public boolean getLst_priceDirtyFlag(){
        return this.lst_priceDirtyFlag ;
    }

    /**
     * 获取 [有效的产品属性]
     */
    @JsonProperty("valid_product_attribute_ids")
    public String getValid_product_attribute_ids(){
        return this.valid_product_attribute_ids ;
    }

    /**
     * 设置 [有效的产品属性]
     */
    @JsonProperty("valid_product_attribute_ids")
    public void setValid_product_attribute_ids(String  valid_product_attribute_ids){
        this.valid_product_attribute_ids = valid_product_attribute_ids ;
        this.valid_product_attribute_idsDirtyFlag = true ;
    }

    /**
     * 获取 [有效的产品属性]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_idsDirtyFlag(){
        return this.valid_product_attribute_idsDirtyFlag ;
    }

    /**
     * 获取 [库存FIFO手工凭证]
     */
    @JsonProperty("stock_fifo_manual_move_ids")
    public String getStock_fifo_manual_move_ids(){
        return this.stock_fifo_manual_move_ids ;
    }

    /**
     * 设置 [库存FIFO手工凭证]
     */
    @JsonProperty("stock_fifo_manual_move_ids")
    public void setStock_fifo_manual_move_ids(String  stock_fifo_manual_move_ids){
        this.stock_fifo_manual_move_ids = stock_fifo_manual_move_ids ;
        this.stock_fifo_manual_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [库存FIFO手工凭证]脏标记
     */
    @JsonIgnore
    public boolean getStock_fifo_manual_move_idsDirtyFlag(){
        return this.stock_fifo_manual_move_idsDirtyFlag ;
    }

    /**
     * 获取 [即时库存]
     */
    @JsonProperty("stock_quant_ids")
    public String getStock_quant_ids(){
        return this.stock_quant_ids ;
    }

    /**
     * 设置 [即时库存]
     */
    @JsonProperty("stock_quant_ids")
    public void setStock_quant_ids(String  stock_quant_ids){
        this.stock_quant_ids = stock_quant_ids ;
        this.stock_quant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [即时库存]脏标记
     */
    @JsonIgnore
    public boolean getStock_quant_idsDirtyFlag(){
        return this.stock_quant_idsDirtyFlag ;
    }

    /**
     * 获取 [进项税]
     */
    @JsonProperty("supplier_taxes_id")
    public String getSupplier_taxes_id(){
        return this.supplier_taxes_id ;
    }

    /**
     * 设置 [进项税]
     */
    @JsonProperty("supplier_taxes_id")
    public void setSupplier_taxes_id(String  supplier_taxes_id){
        this.supplier_taxes_id = supplier_taxes_id ;
        this.supplier_taxes_idDirtyFlag = true ;
    }

    /**
     * 获取 [进项税]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_taxes_idDirtyFlag(){
        return this.supplier_taxes_idDirtyFlag ;
    }

    /**
     * 获取 [价格表明细]
     */
    @JsonProperty("pricelist_item_ids")
    public String getPricelist_item_ids(){
        return this.pricelist_item_ids ;
    }

    /**
     * 设置 [价格表明细]
     */
    @JsonProperty("pricelist_item_ids")
    public void setPricelist_item_ids(String  pricelist_item_ids){
        this.pricelist_item_ids = pricelist_item_ids ;
        this.pricelist_item_idsDirtyFlag = true ;
    }

    /**
     * 获取 [价格表明细]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_item_idsDirtyFlag(){
        return this.pricelist_item_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [附件产品]
     */
    @JsonProperty("accessory_product_ids")
    public String getAccessory_product_ids(){
        return this.accessory_product_ids ;
    }

    /**
     * 设置 [附件产品]
     */
    @JsonProperty("accessory_product_ids")
    public void setAccessory_product_ids(String  accessory_product_ids){
        this.accessory_product_ids = accessory_product_ids ;
        this.accessory_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [附件产品]脏标记
     */
    @JsonIgnore
    public boolean getAccessory_product_idsDirtyFlag(){
        return this.accessory_product_idsDirtyFlag ;
    }

    /**
     * 获取 [供应商]
     */
    @JsonProperty("seller_ids")
    public String getSeller_ids(){
        return this.seller_ids ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("seller_ids")
    public void setSeller_ids(String  seller_ids){
        this.seller_ids = seller_ids ;
        this.seller_idsDirtyFlag = true ;
    }

    /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getSeller_idsDirtyFlag(){
        return this.seller_idsDirtyFlag ;
    }

    /**
     * 获取 [Valid Product Attribute Values Without No Variant Attributes]
     */
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    public String getValid_product_attribute_value_wnva_ids(){
        return this.valid_product_attribute_value_wnva_ids ;
    }

    /**
     * 设置 [Valid Product Attribute Values Without No Variant Attributes]
     */
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    public void setValid_product_attribute_value_wnva_ids(String  valid_product_attribute_value_wnva_ids){
        this.valid_product_attribute_value_wnva_ids = valid_product_attribute_value_wnva_ids ;
        this.valid_product_attribute_value_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Product Attribute Values Without No Variant Attributes]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_wnva_idsDirtyFlag(){
        return this.valid_product_attribute_value_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [客户单号]
     */
    @JsonProperty("partner_ref")
    public String getPartner_ref(){
        return this.partner_ref ;
    }

    /**
     * 设置 [客户单号]
     */
    @JsonProperty("partner_ref")
    public void setPartner_ref(String  partner_ref){
        this.partner_ref = partner_ref ;
        this.partner_refDirtyFlag = true ;
    }

    /**
     * 获取 [客户单号]脏标记
     */
    @JsonIgnore
    public boolean getPartner_refDirtyFlag(){
        return this.partner_refDirtyFlag ;
    }

    /**
     * 获取 [图片]
     */
    @JsonProperty("product_image_ids")
    public String getProduct_image_ids(){
        return this.product_image_ids ;
    }

    /**
     * 设置 [图片]
     */
    @JsonProperty("product_image_ids")
    public void setProduct_image_ids(String  product_image_ids){
        this.product_image_ids = product_image_ids ;
        this.product_image_idsDirtyFlag = true ;
    }

    /**
     * 获取 [图片]脏标记
     */
    @JsonIgnore
    public boolean getProduct_image_idsDirtyFlag(){
        return this.product_image_idsDirtyFlag ;
    }

    /**
     * 获取 [已生产]
     */
    @JsonProperty("mrp_product_qty")
    public Double getMrp_product_qty(){
        return this.mrp_product_qty ;
    }

    /**
     * 设置 [已生产]
     */
    @JsonProperty("mrp_product_qty")
    public void setMrp_product_qty(Double  mrp_product_qty){
        this.mrp_product_qty = mrp_product_qty ;
        this.mrp_product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [已生产]脏标记
     */
    @JsonIgnore
    public boolean getMrp_product_qtyDirtyFlag(){
        return this.mrp_product_qtyDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [Valid Product Attribute Lines]
     */
    @JsonProperty("valid_product_template_attribute_line_ids")
    public String getValid_product_template_attribute_line_ids(){
        return this.valid_product_template_attribute_line_ids ;
    }

    /**
     * 设置 [Valid Product Attribute Lines]
     */
    @JsonProperty("valid_product_template_attribute_line_ids")
    public void setValid_product_template_attribute_line_ids(String  valid_product_template_attribute_line_ids){
        this.valid_product_template_attribute_line_ids = valid_product_template_attribute_line_ids ;
        this.valid_product_template_attribute_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Product Attribute Lines]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_template_attribute_line_idsDirtyFlag(){
        return this.valid_product_template_attribute_line_idsDirtyFlag ;
    }

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [网站产品目录]
     */
    @JsonProperty("public_categ_ids")
    public String getPublic_categ_ids(){
        return this.public_categ_ids ;
    }

    /**
     * 设置 [网站产品目录]
     */
    @JsonProperty("public_categ_ids")
    public void setPublic_categ_ids(String  public_categ_ids){
        this.public_categ_ids = public_categ_ids ;
        this.public_categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站产品目录]脏标记
     */
    @JsonIgnore
    public boolean getPublic_categ_idsDirtyFlag(){
        return this.public_categ_idsDirtyFlag ;
    }

    /**
     * 获取 [活动入场券]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return this.event_ticket_ids ;
    }

    /**
     * 设置 [活动入场券]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动入场券]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return this.event_ticket_idsDirtyFlag ;
    }

    /**
     * 获取 [价格]
     */
    @JsonProperty("price")
    public Double getPrice(){
        return this.price ;
    }

    /**
     * 设置 [价格]
     */
    @JsonProperty("price")
    public void setPrice(Double  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

    /**
     * 获取 [价格]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return this.priceDirtyFlag ;
    }

    /**
     * 获取 [产品属性]
     */
    @JsonProperty("attribute_line_ids")
    public String getAttribute_line_ids(){
        return this.attribute_line_ids ;
    }

    /**
     * 设置 [产品属性]
     */
    @JsonProperty("attribute_line_ids")
    public void setAttribute_line_ids(String  attribute_line_ids){
        this.attribute_line_ids = attribute_line_ids ;
        this.attribute_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [产品属性]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_line_idsDirtyFlag(){
        return this.attribute_line_idsDirtyFlag ;
    }

    /**
     * 获取 [预测数量]
     */
    @JsonProperty("virtual_available")
    public Double getVirtual_available(){
        return this.virtual_available ;
    }

    /**
     * 设置 [预测数量]
     */
    @JsonProperty("virtual_available")
    public void setVirtual_available(Double  virtual_available){
        this.virtual_available = virtual_available ;
        this.virtual_availableDirtyFlag = true ;
    }

    /**
     * 获取 [预测数量]脏标记
     */
    @JsonIgnore
    public boolean getVirtual_availableDirtyFlag(){
        return this.virtual_availableDirtyFlag ;
    }

    /**
     * 获取 [订货规则]
     */
    @JsonProperty("nbr_reordering_rules")
    public Integer getNbr_reordering_rules(){
        return this.nbr_reordering_rules ;
    }

    /**
     * 设置 [订货规则]
     */
    @JsonProperty("nbr_reordering_rules")
    public void setNbr_reordering_rules(Integer  nbr_reordering_rules){
        this.nbr_reordering_rules = nbr_reordering_rules ;
        this.nbr_reordering_rulesDirtyFlag = true ;
    }

    /**
     * 获取 [订货规则]脏标记
     */
    @JsonIgnore
    public boolean getNbr_reordering_rulesDirtyFlag(){
        return this.nbr_reordering_rulesDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [网站价格差异]
     */
    @JsonProperty("website_price_difference")
    public String getWebsite_price_difference(){
        return this.website_price_difference ;
    }

    /**
     * 设置 [网站价格差异]
     */
    @JsonProperty("website_price_difference")
    public void setWebsite_price_difference(String  website_price_difference){
        this.website_price_difference = website_price_difference ;
        this.website_price_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [网站价格差异]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_price_differenceDirtyFlag(){
        return this.website_price_differenceDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [购物车数量]
     */
    @JsonProperty("cart_qty")
    public Integer getCart_qty(){
        return this.cart_qty ;
    }

    /**
     * 设置 [购物车数量]
     */
    @JsonProperty("cart_qty")
    public void setCart_qty(Integer  cart_qty){
        this.cart_qty = cart_qty ;
        this.cart_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [购物车数量]脏标记
     */
    @JsonIgnore
    public boolean getCart_qtyDirtyFlag(){
        return this.cart_qtyDirtyFlag ;
    }

    /**
     * 获取 [网站公开价格]
     */
    @JsonProperty("website_public_price")
    public Double getWebsite_public_price(){
        return this.website_public_price ;
    }

    /**
     * 设置 [网站公开价格]
     */
    @JsonProperty("website_public_price")
    public void setWebsite_public_price(Double  website_public_price){
        this.website_public_price = website_public_price ;
        this.website_public_priceDirtyFlag = true ;
    }

    /**
     * 获取 [网站公开价格]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_public_priceDirtyFlag(){
        return this.website_public_priceDirtyFlag ;
    }

    /**
     * 获取 [评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }

    /**
     * 获取 [BOM组件]
     */
    @JsonProperty("bom_line_ids")
    public String getBom_line_ids(){
        return this.bom_line_ids ;
    }

    /**
     * 设置 [BOM组件]
     */
    @JsonProperty("bom_line_ids")
    public void setBom_line_ids(String  bom_line_ids){
        this.bom_line_ids = bom_line_ids ;
        this.bom_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BOM组件]脏标记
     */
    @JsonIgnore
    public boolean getBom_line_idsDirtyFlag(){
        return this.bom_line_idsDirtyFlag ;
    }

    /**
     * 获取 [网站价格]
     */
    @JsonProperty("website_price")
    public Double getWebsite_price(){
        return this.website_price ;
    }

    /**
     * 设置 [网站价格]
     */
    @JsonProperty("website_price")
    public void setWebsite_price(Double  website_price){
        this.website_price = website_price ;
        this.website_priceDirtyFlag = true ;
    }

    /**
     * 获取 [网站价格]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_priceDirtyFlag(){
        return this.website_priceDirtyFlag ;
    }

    /**
     * 获取 [出向]
     */
    @JsonProperty("outgoing_qty")
    public Double getOutgoing_qty(){
        return this.outgoing_qty ;
    }

    /**
     * 设置 [出向]
     */
    @JsonProperty("outgoing_qty")
    public void setOutgoing_qty(Double  outgoing_qty){
        this.outgoing_qty = outgoing_qty ;
        this.outgoing_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [出向]脏标记
     */
    @JsonIgnore
    public boolean getOutgoing_qtyDirtyFlag(){
        return this.outgoing_qtyDirtyFlag ;
    }

    /**
     * 获取 [已售出]
     */
    @JsonProperty("sales_count")
    public Double getSales_count(){
        return this.sales_count ;
    }

    /**
     * 设置 [已售出]
     */
    @JsonProperty("sales_count")
    public void setSales_count(Double  sales_count){
        this.sales_count = sales_count ;
        this.sales_countDirtyFlag = true ;
    }

    /**
     * 获取 [已售出]脏标记
     */
    @JsonIgnore
    public boolean getSales_countDirtyFlag(){
        return this.sales_countDirtyFlag ;
    }

    /**
     * 获取 [Valid Product Attributes Without No Variant Attributes]
     */
    @JsonProperty("valid_product_attribute_wnva_ids")
    public String getValid_product_attribute_wnva_ids(){
        return this.valid_product_attribute_wnva_ids ;
    }

    /**
     * 设置 [Valid Product Attributes Without No Variant Attributes]
     */
    @JsonProperty("valid_product_attribute_wnva_ids")
    public void setValid_product_attribute_wnva_ids(String  valid_product_attribute_wnva_ids){
        this.valid_product_attribute_wnva_ids = valid_product_attribute_wnva_ids ;
        this.valid_product_attribute_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Product Attributes Without No Variant Attributes]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_wnva_idsDirtyFlag(){
        return this.valid_product_attribute_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }

    /**
     * 获取 [Valid Existing Variants]
     */
    @JsonProperty("valid_existing_variant_ids")
    public String getValid_existing_variant_ids(){
        return this.valid_existing_variant_ids ;
    }

    /**
     * 设置 [Valid Existing Variants]
     */
    @JsonProperty("valid_existing_variant_ids")
    public void setValid_existing_variant_ids(String  valid_existing_variant_ids){
        this.valid_existing_variant_ids = valid_existing_variant_ids ;
        this.valid_existing_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Existing Variants]脏标记
     */
    @JsonIgnore
    public boolean getValid_existing_variant_idsDirtyFlag(){
        return this.valid_existing_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [库存货币价值]
     */
    @JsonProperty("stock_value_currency_id")
    public Integer getStock_value_currency_id(){
        return this.stock_value_currency_id ;
    }

    /**
     * 设置 [库存货币价值]
     */
    @JsonProperty("stock_value_currency_id")
    public void setStock_value_currency_id(Integer  stock_value_currency_id){
        this.stock_value_currency_id = stock_value_currency_id ;
        this.stock_value_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [库存货币价值]脏标记
     */
    @JsonIgnore
    public boolean getStock_value_currency_idDirtyFlag(){
        return this.stock_value_currency_idDirtyFlag ;
    }

    /**
     * 获取 [值]
     */
    @JsonProperty("stock_value")
    public Double getStock_value(){
        return this.stock_value ;
    }

    /**
     * 设置 [值]
     */
    @JsonProperty("stock_value")
    public void setStock_value(Double  stock_value){
        this.stock_value = stock_value ;
        this.stock_valueDirtyFlag = true ;
    }

    /**
     * 获取 [值]脏标记
     */
    @JsonIgnore
    public boolean getStock_valueDirtyFlag(){
        return this.stock_valueDirtyFlag ;
    }

    /**
     * 获取 [样式]
     */
    @JsonProperty("website_style_ids")
    public String getWebsite_style_ids(){
        return this.website_style_ids ;
    }

    /**
     * 设置 [样式]
     */
    @JsonProperty("website_style_ids")
    public void setWebsite_style_ids(String  website_style_ids){
        this.website_style_ids = website_style_ids ;
        this.website_style_idsDirtyFlag = true ;
    }

    /**
     * 获取 [样式]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_style_idsDirtyFlag(){
        return this.website_style_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [重量]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return this.weight ;
    }

    /**
     * 设置 [重量]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

    /**
     * 获取 [重量]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return this.weightDirtyFlag ;
    }

    /**
     * 获取 [物料清单]
     */
    @JsonProperty("bom_ids")
    public String getBom_ids(){
        return this.bom_ids ;
    }

    /**
     * 设置 [物料清单]
     */
    @JsonProperty("bom_ids")
    public void setBom_ids(String  bom_ids){
        this.bom_ids = bom_ids ;
        this.bom_idsDirtyFlag = true ;
    }

    /**
     * 获取 [物料清单]脏标记
     */
    @JsonIgnore
    public boolean getBom_idsDirtyFlag(){
        return this.bom_idsDirtyFlag ;
    }

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }

    /**
     * 获取 [Valid Product Attribute Lines Without No Variant Attributes]
     */
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    public String getValid_product_template_attribute_line_wnva_ids(){
        return this.valid_product_template_attribute_line_wnva_ids ;
    }

    /**
     * 设置 [Valid Product Attribute Lines Without No Variant Attributes]
     */
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    public void setValid_product_template_attribute_line_wnva_ids(String  valid_product_template_attribute_line_wnva_ids){
        this.valid_product_template_attribute_line_wnva_ids = valid_product_template_attribute_line_wnva_ids ;
        this.valid_product_template_attribute_line_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Product Attribute Lines Without No Variant Attributes]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_template_attribute_line_wnva_idsDirtyFlag(){
        return this.valid_product_template_attribute_line_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [有效的产品属性值]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public String getValid_product_attribute_value_ids(){
        return this.valid_product_attribute_value_ids ;
    }

    /**
     * 设置 [有效的产品属性值]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public void setValid_product_attribute_value_ids(String  valid_product_attribute_value_ids){
        this.valid_product_attribute_value_ids = valid_product_attribute_value_ids ;
        this.valid_product_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [有效的产品属性值]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_idsDirtyFlag(){
        return this.valid_product_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [在手数量]
     */
    @JsonProperty("qty_available")
    public Double getQty_available(){
        return this.qty_available ;
    }

    /**
     * 设置 [在手数量]
     */
    @JsonProperty("qty_available")
    public void setQty_available(Double  qty_available){
        this.qty_available = qty_available ;
        this.qty_availableDirtyFlag = true ;
    }

    /**
     * 获取 [在手数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_availableDirtyFlag(){
        return this.qty_availableDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [变体图像]
     */
    @JsonProperty("image_variant")
    public byte[] getImage_variant(){
        return this.image_variant ;
    }

    /**
     * 设置 [变体图像]
     */
    @JsonProperty("image_variant")
    public void setImage_variant(byte[]  image_variant){
        this.image_variant = image_variant ;
        this.image_variantDirtyFlag = true ;
    }

    /**
     * 获取 [变体图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_variantDirtyFlag(){
        return this.image_variantDirtyFlag ;
    }

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("stock_move_ids")
    public String getStock_move_ids(){
        return this.stock_move_ids ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("stock_move_ids")
    public void setStock_move_ids(String  stock_move_ids){
        this.stock_move_ids = stock_move_ids ;
        this.stock_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_idsDirtyFlag(){
        return this.stock_move_idsDirtyFlag ;
    }

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [库存FIFO实时计价]
     */
    @JsonProperty("stock_fifo_real_time_aml_ids")
    public String getStock_fifo_real_time_aml_ids(){
        return this.stock_fifo_real_time_aml_ids ;
    }

    /**
     * 设置 [库存FIFO实时计价]
     */
    @JsonProperty("stock_fifo_real_time_aml_ids")
    public void setStock_fifo_real_time_aml_ids(String  stock_fifo_real_time_aml_ids){
        this.stock_fifo_real_time_aml_ids = stock_fifo_real_time_aml_ids ;
        this.stock_fifo_real_time_aml_idsDirtyFlag = true ;
    }

    /**
     * 获取 [库存FIFO实时计价]脏标记
     */
    @JsonIgnore
    public boolean getStock_fifo_real_time_aml_idsDirtyFlag(){
        return this.stock_fifo_real_time_aml_idsDirtyFlag ;
    }

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [参照]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [参照]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [参照]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }

    /**
     * 获取 [重订货最小数量]
     */
    @JsonProperty("reordering_min_qty")
    public Double getReordering_min_qty(){
        return this.reordering_min_qty ;
    }

    /**
     * 设置 [重订货最小数量]
     */
    @JsonProperty("reordering_min_qty")
    public void setReordering_min_qty(Double  reordering_min_qty){
        this.reordering_min_qty = reordering_min_qty ;
        this.reordering_min_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [重订货最小数量]脏标记
     */
    @JsonIgnore
    public boolean getReordering_min_qtyDirtyFlag(){
        return this.reordering_min_qtyDirtyFlag ;
    }

    /**
     * 获取 [大尺寸图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [大尺寸图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [大尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return this.route_ids ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return this.route_idsDirtyFlag ;
    }

    /**
     * 获取 [销项税]
     */
    @JsonProperty("taxes_id")
    public String getTaxes_id(){
        return this.taxes_id ;
    }

    /**
     * 设置 [销项税]
     */
    @JsonProperty("taxes_id")
    public void setTaxes_id(String  taxes_id){
        this.taxes_id = taxes_id ;
        this.taxes_idDirtyFlag = true ;
    }

    /**
     * 获取 [销项税]脏标记
     */
    @JsonIgnore
    public boolean getTaxes_idDirtyFlag(){
        return this.taxes_idDirtyFlag ;
    }

    /**
     * 获取 [# 物料清单]
     */
    @JsonProperty("bom_count")
    public Integer getBom_count(){
        return this.bom_count ;
    }

    /**
     * 设置 [# 物料清单]
     */
    @JsonProperty("bom_count")
    public void setBom_count(Integer  bom_count){
        this.bom_count = bom_count ;
        this.bom_countDirtyFlag = true ;
    }

    /**
     * 获取 [# 物料清单]脏标记
     */
    @JsonIgnore
    public boolean getBom_countDirtyFlag(){
        return this.bom_countDirtyFlag ;
    }

    /**
     * 获取 [动作数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [动作数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [动作数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [产品包裹]
     */
    @JsonProperty("packaging_ids")
    public String getPackaging_ids(){
        return this.packaging_ids ;
    }

    /**
     * 设置 [产品包裹]
     */
    @JsonProperty("packaging_ids")
    public void setPackaging_ids(String  packaging_ids){
        this.packaging_ids = packaging_ids ;
        this.packaging_idsDirtyFlag = true ;
    }

    /**
     * 获取 [产品包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackaging_idsDirtyFlag(){
        return this.packaging_idsDirtyFlag ;
    }

    /**
     * 获取 [Valid Archived Variants]
     */
    @JsonProperty("valid_archived_variant_ids")
    public String getValid_archived_variant_ids(){
        return this.valid_archived_variant_ids ;
    }

    /**
     * 设置 [Valid Archived Variants]
     */
    @JsonProperty("valid_archived_variant_ids")
    public void setValid_archived_variant_ids(String  valid_archived_variant_ids){
        this.valid_archived_variant_ids = valid_archived_variant_ids ;
        this.valid_archived_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [Valid Archived Variants]脏标记
     */
    @JsonIgnore
    public boolean getValid_archived_variant_idsDirtyFlag(){
        return this.valid_archived_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [价格表项目]
     */
    @JsonProperty("item_ids")
    public String getItem_ids(){
        return this.item_ids ;
    }

    /**
     * 设置 [价格表项目]
     */
    @JsonProperty("item_ids")
    public void setItem_ids(String  item_ids){
        this.item_ids = item_ids ;
        this.item_idsDirtyFlag = true ;
    }

    /**
     * 获取 [价格表项目]脏标记
     */
    @JsonIgnore
    public boolean getItem_idsDirtyFlag(){
        return this.item_idsDirtyFlag ;
    }

    /**
     * 获取 [已采购]
     */
    @JsonProperty("purchased_product_qty")
    public Double getPurchased_product_qty(){
        return this.purchased_product_qty ;
    }

    /**
     * 设置 [已采购]
     */
    @JsonProperty("purchased_product_qty")
    public void setPurchased_product_qty(Double  purchased_product_qty){
        this.purchased_product_qty = purchased_product_qty ;
        this.purchased_product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [已采购]脏标记
     */
    @JsonIgnore
    public boolean getPurchased_product_qtyDirtyFlag(){
        return this.purchased_product_qtyDirtyFlag ;
    }

    /**
     * 获取 [重订货最大数量]
     */
    @JsonProperty("reordering_max_qty")
    public Double getReordering_max_qty(){
        return this.reordering_max_qty ;
    }

    /**
     * 设置 [重订货最大数量]
     */
    @JsonProperty("reordering_max_qty")
    public void setReordering_max_qty(Double  reordering_max_qty){
        this.reordering_max_qty = reordering_max_qty ;
        this.reordering_max_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [重订货最大数量]脏标记
     */
    @JsonIgnore
    public boolean getReordering_max_qtyDirtyFlag(){
        return this.reordering_max_qtyDirtyFlag ;
    }

    /**
     * 获取 [最小库存规则]
     */
    @JsonProperty("orderpoint_ids")
    public String getOrderpoint_ids(){
        return this.orderpoint_ids ;
    }

    /**
     * 设置 [最小库存规则]
     */
    @JsonProperty("orderpoint_ids")
    public void setOrderpoint_ids(String  orderpoint_ids){
        this.orderpoint_ids = orderpoint_ids ;
        this.orderpoint_idsDirtyFlag = true ;
    }

    /**
     * 获取 [最小库存规则]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_idsDirtyFlag(){
        return this.orderpoint_idsDirtyFlag ;
    }

    /**
     * 获取 [可选产品]
     */
    @JsonProperty("optional_product_ids")
    public String getOptional_product_ids(){
        return this.optional_product_ids ;
    }

    /**
     * 设置 [可选产品]
     */
    @JsonProperty("optional_product_ids")
    public void setOptional_product_ids(String  optional_product_ids){
        this.optional_product_ids = optional_product_ids ;
        this.optional_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [可选产品]脏标记
     */
    @JsonIgnore
    public boolean getOptional_product_idsDirtyFlag(){
        return this.optional_product_idsDirtyFlag ;
    }

    /**
     * 获取 [是产品变体]
     */
    @JsonProperty("is_product_variant")
    public String getIs_product_variant(){
        return this.is_product_variant ;
    }

    /**
     * 设置 [是产品变体]
     */
    @JsonProperty("is_product_variant")
    public void setIs_product_variant(String  is_product_variant){
        this.is_product_variant = is_product_variant ;
        this.is_product_variantDirtyFlag = true ;
    }

    /**
     * 获取 [是产品变体]脏标记
     */
    @JsonIgnore
    public boolean getIs_product_variantDirtyFlag(){
        return this.is_product_variantDirtyFlag ;
    }

    /**
     * 获取 [# BOM 使用的地方]
     */
    @JsonProperty("used_in_bom_count")
    public Integer getUsed_in_bom_count(){
        return this.used_in_bom_count ;
    }

    /**
     * 设置 [# BOM 使用的地方]
     */
    @JsonProperty("used_in_bom_count")
    public void setUsed_in_bom_count(Integer  used_in_bom_count){
        this.used_in_bom_count = used_in_bom_count ;
        this.used_in_bom_countDirtyFlag = true ;
    }

    /**
     * 获取 [# BOM 使用的地方]脏标记
     */
    @JsonIgnore
    public boolean getUsed_in_bom_countDirtyFlag(){
        return this.used_in_bom_countDirtyFlag ;
    }

    /**
     * 获取 [数量]
     */
    @JsonProperty("qty_at_date")
    public Double getQty_at_date(){
        return this.qty_at_date ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("qty_at_date")
    public void setQty_at_date(Double  qty_at_date){
        this.qty_at_date = qty_at_date ;
        this.qty_at_dateDirtyFlag = true ;
    }

    /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_at_dateDirtyFlag(){
        return this.qty_at_dateDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [条码]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [成本]
     */
    @JsonProperty("standard_price")
    public Double getStandard_price(){
        return this.standard_price ;
    }

    /**
     * 设置 [成本]
     */
    @JsonProperty("standard_price")
    public void setStandard_price(Double  standard_price){
        this.standard_price = standard_price ;
        this.standard_priceDirtyFlag = true ;
    }

    /**
     * 获取 [成本]脏标记
     */
    @JsonIgnore
    public boolean getStandard_priceDirtyFlag(){
        return this.standard_priceDirtyFlag ;
    }

    /**
     * 获取 [属性值]
     */
    @JsonProperty("attribute_value_ids")
    public String getAttribute_value_ids(){
        return this.attribute_value_ids ;
    }

    /**
     * 设置 [属性值]
     */
    @JsonProperty("attribute_value_ids")
    public void setAttribute_value_ids(String  attribute_value_ids){
        this.attribute_value_ids = attribute_value_ids ;
        this.attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [属性值]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_idsDirtyFlag(){
        return this.attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [变体价格额外]
     */
    @JsonProperty("price_extra")
    public Double getPrice_extra(){
        return this.price_extra ;
    }

    /**
     * 设置 [变体价格额外]
     */
    @JsonProperty("price_extra")
    public void setPrice_extra(Double  price_extra){
        this.price_extra = price_extra ;
        this.price_extraDirtyFlag = true ;
    }

    /**
     * 获取 [变体价格额外]脏标记
     */
    @JsonIgnore
    public boolean getPrice_extraDirtyFlag(){
        return this.price_extraDirtyFlag ;
    }

    /**
     * 获取 [BOM产品变体.]
     */
    @JsonProperty("variant_bom_ids")
    public String getVariant_bom_ids(){
        return this.variant_bom_ids ;
    }

    /**
     * 设置 [BOM产品变体.]
     */
    @JsonProperty("variant_bom_ids")
    public void setVariant_bom_ids(String  variant_bom_ids){
        this.variant_bom_ids = variant_bom_ids ;
        this.variant_bom_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BOM产品变体.]脏标记
     */
    @JsonIgnore
    public boolean getVariant_bom_idsDirtyFlag(){
        return this.variant_bom_idsDirtyFlag ;
    }

    /**
     * 获取 [替代产品]
     */
    @JsonProperty("alternative_product_ids")
    public String getAlternative_product_ids(){
        return this.alternative_product_ids ;
    }

    /**
     * 设置 [替代产品]
     */
    @JsonProperty("alternative_product_ids")
    public void setAlternative_product_ids(String  alternative_product_ids){
        this.alternative_product_ids = alternative_product_ids ;
        this.alternative_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [替代产品]脏标记
     */
    @JsonIgnore
    public boolean getAlternative_product_idsDirtyFlag(){
        return this.alternative_product_idsDirtyFlag ;
    }

    /**
     * 获取 [内部参考]
     */
    @JsonProperty("default_code")
    public String getDefault_code(){
        return this.default_code ;
    }

    /**
     * 设置 [内部参考]
     */
    @JsonProperty("default_code")
    public void setDefault_code(String  default_code){
        this.default_code = default_code ;
        this.default_codeDirtyFlag = true ;
    }

    /**
     * 获取 [内部参考]脏标记
     */
    @JsonIgnore
    public boolean getDefault_codeDirtyFlag(){
        return this.default_codeDirtyFlag ;
    }

    /**
     * 获取 [类别路线]
     */
    @JsonProperty("route_from_categ_ids")
    public String getRoute_from_categ_ids(){
        return this.route_from_categ_ids ;
    }

    /**
     * 设置 [类别路线]
     */
    @JsonProperty("route_from_categ_ids")
    public void setRoute_from_categ_ids(String  route_from_categ_ids){
        this.route_from_categ_ids = route_from_categ_ids ;
        this.route_from_categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [类别路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_from_categ_idsDirtyFlag(){
        return this.route_from_categ_idsDirtyFlag ;
    }

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [入库]
     */
    @JsonProperty("incoming_qty")
    public Double getIncoming_qty(){
        return this.incoming_qty ;
    }

    /**
     * 设置 [入库]
     */
    @JsonProperty("incoming_qty")
    public void setIncoming_qty(Double  incoming_qty){
        this.incoming_qty = incoming_qty ;
        this.incoming_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [入库]脏标记
     */
    @JsonIgnore
    public boolean getIncoming_qtyDirtyFlag(){
        return this.incoming_qtyDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [追踪]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return this.tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

    /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return this.trackingDirtyFlag ;
    }

    /**
     * 获取 [拣货说明]
     */
    @JsonProperty("description_picking")
    public String getDescription_picking(){
        return this.description_picking ;
    }

    /**
     * 设置 [拣货说明]
     */
    @JsonProperty("description_picking")
    public void setDescription_picking(String  description_picking){
        this.description_picking = description_picking ;
        this.description_pickingDirtyFlag = true ;
    }

    /**
     * 获取 [拣货说明]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickingDirtyFlag(){
        return this.description_pickingDirtyFlag ;
    }

    /**
     * 获取 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output")
    public Integer getProperty_stock_account_output(){
        return this.property_stock_account_output ;
    }

    /**
     * 设置 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output")
    public void setProperty_stock_account_output(Integer  property_stock_account_output){
        this.property_stock_account_output = property_stock_account_output ;
        this.property_stock_account_outputDirtyFlag = true ;
    }

    /**
     * 获取 [库存出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_outputDirtyFlag(){
        return this.property_stock_account_outputDirtyFlag ;
    }

    /**
     * 获取 [销售]
     */
    @JsonProperty("sale_ok")
    public String getSale_ok(){
        return this.sale_ok ;
    }

    /**
     * 设置 [销售]
     */
    @JsonProperty("sale_ok")
    public void setSale_ok(String  sale_ok){
        this.sale_ok = sale_ok ;
        this.sale_okDirtyFlag = true ;
    }

    /**
     * 获取 [销售]脏标记
     */
    @JsonIgnore
    public boolean getSale_okDirtyFlag(){
        return this.sale_okDirtyFlag ;
    }

    /**
     * 获取 [网站的说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站的说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [网站的说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [称重]
     */
    @JsonProperty("to_weight")
    public String getTo_weight(){
        return this.to_weight ;
    }

    /**
     * 设置 [称重]
     */
    @JsonProperty("to_weight")
    public void setTo_weight(String  to_weight){
        this.to_weight = to_weight ;
        this.to_weightDirtyFlag = true ;
    }

    /**
     * 获取 [称重]脏标记
     */
    @JsonIgnore
    public boolean getTo_weightDirtyFlag(){
        return this.to_weightDirtyFlag ;
    }

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }

    /**
     * 获取 [收货说明]
     */
    @JsonProperty("description_pickingin")
    public String getDescription_pickingin(){
        return this.description_pickingin ;
    }

    /**
     * 设置 [收货说明]
     */
    @JsonProperty("description_pickingin")
    public void setDescription_pickingin(String  description_pickingin){
        this.description_pickingin = description_pickingin ;
        this.description_pickinginDirtyFlag = true ;
    }

    /**
     * 获取 [收货说明]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickinginDirtyFlag(){
        return this.description_pickinginDirtyFlag ;
    }

    /**
     * 获取 [销售价格]
     */
    @JsonProperty("list_price")
    public Double getList_price(){
        return this.list_price ;
    }

    /**
     * 设置 [销售价格]
     */
    @JsonProperty("list_price")
    public void setList_price(Double  list_price){
        this.list_price = list_price ;
        this.list_priceDirtyFlag = true ;
    }

    /**
     * 获取 [销售价格]脏标记
     */
    @JsonIgnore
    public boolean getList_priceDirtyFlag(){
        return this.list_priceDirtyFlag ;
    }

    /**
     * 获取 [隐藏费用政策]
     */
    @JsonProperty("hide_expense_policy")
    public String getHide_expense_policy(){
        return this.hide_expense_policy ;
    }

    /**
     * 设置 [隐藏费用政策]
     */
    @JsonProperty("hide_expense_policy")
    public void setHide_expense_policy(String  hide_expense_policy){
        this.hide_expense_policy = hide_expense_policy ;
        this.hide_expense_policyDirtyFlag = true ;
    }

    /**
     * 获取 [隐藏费用政策]脏标记
     */
    @JsonIgnore
    public boolean getHide_expense_policyDirtyFlag(){
        return this.hide_expense_policyDirtyFlag ;
    }

    /**
     * 获取 [销售说明]
     */
    @JsonProperty("description_sale")
    public String getDescription_sale(){
        return this.description_sale ;
    }

    /**
     * 设置 [销售说明]
     */
    @JsonProperty("description_sale")
    public void setDescription_sale(String  description_sale){
        this.description_sale = description_sale ;
        this.description_saleDirtyFlag = true ;
    }

    /**
     * 获取 [销售说明]脏标记
     */
    @JsonIgnore
    public boolean getDescription_saleDirtyFlag(){
        return this.description_saleDirtyFlag ;
    }

    /**
     * 获取 [成本方法]
     */
    @JsonProperty("cost_method")
    public String getCost_method(){
        return this.cost_method ;
    }

    /**
     * 设置 [成本方法]
     */
    @JsonProperty("cost_method")
    public void setCost_method(String  cost_method){
        this.cost_method = cost_method ;
        this.cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [成本方法]脏标记
     */
    @JsonIgnore
    public boolean getCost_methodDirtyFlag(){
        return this.cost_methodDirtyFlag ;
    }

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }

    /**
     * 获取 [销售订单行消息]
     */
    @JsonProperty("sale_line_warn_msg")
    public String getSale_line_warn_msg(){
        return this.sale_line_warn_msg ;
    }

    /**
     * 设置 [销售订单行消息]
     */
    @JsonProperty("sale_line_warn_msg")
    public void setSale_line_warn_msg(String  sale_line_warn_msg){
        this.sale_line_warn_msg = sale_line_warn_msg ;
        this.sale_line_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单行消息]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_warn_msgDirtyFlag(){
        return this.sale_line_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [出租]
     */
    @JsonProperty("rental")
    public String getRental(){
        return this.rental ;
    }

    /**
     * 设置 [出租]
     */
    @JsonProperty("rental")
    public void setRental(String  rental){
        this.rental = rental ;
        this.rentalDirtyFlag = true ;
    }

    /**
     * 获取 [出租]脏标记
     */
    @JsonIgnore
    public boolean getRentalDirtyFlag(){
        return this.rentalDirtyFlag ;
    }

    /**
     * 获取 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference")
    public Integer getProperty_account_creditor_price_difference(){
        return this.property_account_creditor_price_difference ;
    }

    /**
     * 设置 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference")
    public void setProperty_account_creditor_price_difference(Integer  property_account_creditor_price_difference){
        this.property_account_creditor_price_difference = property_account_creditor_price_difference ;
        this.property_account_creditor_price_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [价格差异科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_creditor_price_differenceDirtyFlag(){
        return this.property_account_creditor_price_differenceDirtyFlag ;
    }

    /**
     * 获取 [重量计量单位标签]
     */
    @JsonProperty("weight_uom_name")
    public String getWeight_uom_name(){
        return this.weight_uom_name ;
    }

    /**
     * 设置 [重量计量单位标签]
     */
    @JsonProperty("weight_uom_name")
    public void setWeight_uom_name(String  weight_uom_name){
        this.weight_uom_name = weight_uom_name ;
        this.weight_uom_nameDirtyFlag = true ;
    }

    /**
     * 获取 [重量计量单位标签]脏标记
     */
    @JsonIgnore
    public boolean getWeight_uom_nameDirtyFlag(){
        return this.weight_uom_nameDirtyFlag ;
    }

    /**
     * 获取 [成本币种]
     */
    @JsonProperty("cost_currency_id")
    public Integer getCost_currency_id(){
        return this.cost_currency_id ;
    }

    /**
     * 设置 [成本币种]
     */
    @JsonProperty("cost_currency_id")
    public void setCost_currency_id(Integer  cost_currency_id){
        this.cost_currency_id = cost_currency_id ;
        this.cost_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [成本币种]脏标记
     */
    @JsonIgnore
    public boolean getCost_currency_idDirtyFlag(){
        return this.cost_currency_idDirtyFlag ;
    }

    /**
     * 获取 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input")
    public Integer getProperty_stock_account_input(){
        return this.property_stock_account_input ;
    }

    /**
     * 设置 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input")
    public void setProperty_stock_account_input(Integer  property_stock_account_input){
        this.property_stock_account_input = property_stock_account_input ;
        this.property_stock_account_inputDirtyFlag = true ;
    }

    /**
     * 获取 [库存进货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_inputDirtyFlag(){
        return this.property_stock_account_inputDirtyFlag ;
    }

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [制造提前期(日)]
     */
    @JsonProperty("produce_delay")
    public Double getProduce_delay(){
        return this.produce_delay ;
    }

    /**
     * 设置 [制造提前期(日)]
     */
    @JsonProperty("produce_delay")
    public void setProduce_delay(Double  produce_delay){
        this.produce_delay = produce_delay ;
        this.produce_delayDirtyFlag = true ;
    }

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    @JsonIgnore
    public boolean getProduce_delayDirtyFlag(){
        return this.produce_delayDirtyFlag ;
    }

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }

    /**
     * 获取 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return this.rating_last_feedback ;
    }

    /**
     * 设置 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [最新反馈评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return this.rating_last_feedbackDirtyFlag ;
    }

    /**
     * 获取 [尺寸 Y]
     */
    @JsonProperty("website_size_y")
    public Integer getWebsite_size_y(){
        return this.website_size_y ;
    }

    /**
     * 设置 [尺寸 Y]
     */
    @JsonProperty("website_size_y")
    public void setWebsite_size_y(Integer  website_size_y){
        this.website_size_y = website_size_y ;
        this.website_size_yDirtyFlag = true ;
    }

    /**
     * 获取 [尺寸 Y]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_size_yDirtyFlag(){
        return this.website_size_yDirtyFlag ;
    }

    /**
     * 获取 [是一张活动票吗？]
     */
    @JsonProperty("event_ok")
    public String getEvent_ok(){
        return this.event_ok ;
    }

    /**
     * 设置 [是一张活动票吗？]
     */
    @JsonProperty("event_ok")
    public void setEvent_ok(String  event_ok){
        this.event_ok = event_ok ;
        this.event_okDirtyFlag = true ;
    }

    /**
     * 获取 [是一张活动票吗？]脏标记
     */
    @JsonIgnore
    public boolean getEvent_okDirtyFlag(){
        return this.event_okDirtyFlag ;
    }

    /**
     * 获取 [库存可用性]
     */
    @JsonProperty("inventory_availability")
    public String getInventory_availability(){
        return this.inventory_availability ;
    }

    /**
     * 设置 [库存可用性]
     */
    @JsonProperty("inventory_availability")
    public void setInventory_availability(String  inventory_availability){
        this.inventory_availability = inventory_availability ;
        this.inventory_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [库存可用性]脏标记
     */
    @JsonIgnore
    public boolean getInventory_availabilityDirtyFlag(){
        return this.inventory_availabilityDirtyFlag ;
    }

    /**
     * 获取 [采购]
     */
    @JsonProperty("purchase_ok")
    public String getPurchase_ok(){
        return this.purchase_ok ;
    }

    /**
     * 设置 [采购]
     */
    @JsonProperty("purchase_ok")
    public void setPurchase_ok(String  purchase_ok){
        this.purchase_ok = purchase_ok ;
        this.purchase_okDirtyFlag = true ;
    }

    /**
     * 获取 [采购]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_okDirtyFlag(){
        return this.purchase_okDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return this.rating_last_value ;
    }

    /**
     * 设置 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

    /**
     * 获取 [最新值评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return this.rating_last_valueDirtyFlag ;
    }

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return this.rating_last_image ;
    }

    /**
     * 设置 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

    /**
     * 获取 [最新图像评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return this.rating_last_imageDirtyFlag ;
    }

    /**
     * 获取 [采购说明]
     */
    @JsonProperty("description_purchase")
    public String getDescription_purchase(){
        return this.description_purchase ;
    }

    /**
     * 设置 [采购说明]
     */
    @JsonProperty("description_purchase")
    public void setDescription_purchase(String  description_purchase){
        this.description_purchase = description_purchase ;
        this.description_purchaseDirtyFlag = true ;
    }

    /**
     * 获取 [采购说明]脏标记
     */
    @JsonIgnore
    public boolean getDescription_purchaseDirtyFlag(){
        return this.description_purchaseDirtyFlag ;
    }

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }

    /**
     * 获取 [报销]
     */
    @JsonProperty("can_be_expensed")
    public String getCan_be_expensed(){
        return this.can_be_expensed ;
    }

    /**
     * 设置 [报销]
     */
    @JsonProperty("can_be_expensed")
    public void setCan_be_expensed(String  can_be_expensed){
        this.can_be_expensed = can_be_expensed ;
        this.can_be_expensedDirtyFlag = true ;
    }

    /**
     * 获取 [报销]脏标记
     */
    @JsonIgnore
    public boolean getCan_be_expensedDirtyFlag(){
        return this.can_be_expensedDirtyFlag ;
    }

    /**
     * 获取 [销售订单行]
     */
    @JsonProperty("sale_line_warn")
    public String getSale_line_warn(){
        return this.sale_line_warn ;
    }

    /**
     * 设置 [销售订单行]
     */
    @JsonProperty("sale_line_warn")
    public void setSale_line_warn(String  sale_line_warn){
        this.sale_line_warn = sale_line_warn ;
        this.sale_line_warnDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单行]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_warnDirtyFlag(){
        return this.sale_line_warnDirtyFlag ;
    }

    /**
     * 获取 [尺寸 X]
     */
    @JsonProperty("website_size_x")
    public Integer getWebsite_size_x(){
        return this.website_size_x ;
    }

    /**
     * 设置 [尺寸 X]
     */
    @JsonProperty("website_size_x")
    public void setWebsite_size_x(Integer  website_size_x){
        this.website_size_x = website_size_x ;
        this.website_size_xDirtyFlag = true ;
    }

    /**
     * 获取 [尺寸 X]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_size_xDirtyFlag(){
        return this.website_size_xDirtyFlag ;
    }

    /**
     * 获取 [自动采购]
     */
    @JsonProperty("service_to_purchase")
    public String getService_to_purchase(){
        return this.service_to_purchase ;
    }

    /**
     * 设置 [自动采购]
     */
    @JsonProperty("service_to_purchase")
    public void setService_to_purchase(String  service_to_purchase){
        this.service_to_purchase = service_to_purchase ;
        this.service_to_purchaseDirtyFlag = true ;
    }

    /**
     * 获取 [自动采购]脏标记
     */
    @JsonIgnore
    public boolean getService_to_purchaseDirtyFlag(){
        return this.service_to_purchaseDirtyFlag ;
    }

    /**
     * 获取 [网站序列]
     */
    @JsonProperty("website_sequence")
    public Integer getWebsite_sequence(){
        return this.website_sequence ;
    }

    /**
     * 设置 [网站序列]
     */
    @JsonProperty("website_sequence")
    public void setWebsite_sequence(Integer  website_sequence){
        this.website_sequence = website_sequence ;
        this.website_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [网站序列]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_sequenceDirtyFlag(){
        return this.website_sequenceDirtyFlag ;
    }

    /**
     * 获取 [库存位置]
     */
    @JsonProperty("property_stock_inventory")
    public Integer getProperty_stock_inventory(){
        return this.property_stock_inventory ;
    }

    /**
     * 设置 [库存位置]
     */
    @JsonProperty("property_stock_inventory")
    public void setProperty_stock_inventory(Integer  property_stock_inventory){
        this.property_stock_inventory = property_stock_inventory ;
        this.property_stock_inventoryDirtyFlag = true ;
    }

    /**
     * 获取 [库存位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_inventoryDirtyFlag(){
        return this.property_stock_inventoryDirtyFlag ;
    }

    /**
     * 获取 [地点]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }

    /**
     * 获取 [库存计价]
     */
    @JsonProperty("property_valuation")
    public String getProperty_valuation(){
        return this.property_valuation ;
    }

    /**
     * 设置 [库存计价]
     */
    @JsonProperty("property_valuation")
    public void setProperty_valuation(String  property_valuation){
        this.property_valuation = property_valuation ;
        this.property_valuationDirtyFlag = true ;
    }

    /**
     * 获取 [库存计价]脏标记
     */
    @JsonIgnore
    public boolean getProperty_valuationDirtyFlag(){
        return this.property_valuationDirtyFlag ;
    }

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }

    /**
     * 获取 [重开收据规则]
     */
    @JsonProperty("expense_policy")
    public String getExpense_policy(){
        return this.expense_policy ;
    }

    /**
     * 设置 [重开收据规则]
     */
    @JsonProperty("expense_policy")
    public void setExpense_policy(String  expense_policy){
        this.expense_policy = expense_policy ;
        this.expense_policyDirtyFlag = true ;
    }

    /**
     * 获取 [重开收据规则]脏标记
     */
    @JsonIgnore
    public boolean getExpense_policyDirtyFlag(){
        return this.expense_policyDirtyFlag ;
    }

    /**
     * 获取 [测量的重量单位]
     */
    @JsonProperty("weight_uom_id")
    public Integer getWeight_uom_id(){
        return this.weight_uom_id ;
    }

    /**
     * 设置 [测量的重量单位]
     */
    @JsonProperty("weight_uom_id")
    public void setWeight_uom_id(Integer  weight_uom_id){
        this.weight_uom_id = weight_uom_id ;
        this.weight_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [测量的重量单位]脏标记
     */
    @JsonIgnore
    public boolean getWeight_uom_idDirtyFlag(){
        return this.weight_uom_idDirtyFlag ;
    }

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }

    /**
     * 获取 [生产位置]
     */
    @JsonProperty("property_stock_production")
    public Integer getProperty_stock_production(){
        return this.property_stock_production ;
    }

    /**
     * 设置 [生产位置]
     */
    @JsonProperty("property_stock_production")
    public void setProperty_stock_production(Integer  property_stock_production){
        this.property_stock_production = property_stock_production ;
        this.property_stock_productionDirtyFlag = true ;
    }

    /**
     * 获取 [生产位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_productionDirtyFlag(){
        return this.property_stock_productionDirtyFlag ;
    }

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [出库单说明]
     */
    @JsonProperty("description_pickingout")
    public String getDescription_pickingout(){
        return this.description_pickingout ;
    }

    /**
     * 设置 [出库单说明]
     */
    @JsonProperty("description_pickingout")
    public void setDescription_pickingout(String  description_pickingout){
        this.description_pickingout = description_pickingout ;
        this.description_pickingoutDirtyFlag = true ;
    }

    /**
     * 获取 [出库单说明]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickingoutDirtyFlag(){
        return this.description_pickingoutDirtyFlag ;
    }

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [评级数]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return this.rating_count ;
    }

    /**
     * 设置 [评级数]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

    /**
     * 获取 [评级数]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return this.rating_countDirtyFlag ;
    }

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [计价]
     */
    @JsonProperty("valuation")
    public String getValuation(){
        return this.valuation ;
    }

    /**
     * 设置 [计价]
     */
    @JsonProperty("valuation")
    public void setValuation(String  valuation){
        this.valuation = valuation ;
        this.valuationDirtyFlag = true ;
    }

    /**
     * 获取 [计价]脏标记
     */
    @JsonIgnore
    public boolean getValuationDirtyFlag(){
        return this.valuationDirtyFlag ;
    }

    /**
     * 获取 [开票策略]
     */
    @JsonProperty("invoice_policy")
    public String getInvoice_policy(){
        return this.invoice_policy ;
    }

    /**
     * 设置 [开票策略]
     */
    @JsonProperty("invoice_policy")
    public void setInvoice_policy(String  invoice_policy){
        this.invoice_policy = invoice_policy ;
        this.invoice_policyDirtyFlag = true ;
    }

    /**
     * 获取 [开票策略]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_policyDirtyFlag(){
        return this.invoice_policyDirtyFlag ;
    }

    /**
     * 获取 [采购订单明细的消息]
     */
    @JsonProperty("purchase_line_warn_msg")
    public String getPurchase_line_warn_msg(){
        return this.purchase_line_warn_msg ;
    }

    /**
     * 设置 [采购订单明细的消息]
     */
    @JsonProperty("purchase_line_warn_msg")
    public void setPurchase_line_warn_msg(String  purchase_line_warn_msg){
        this.purchase_line_warn_msg = purchase_line_warn_msg ;
        this.purchase_line_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单明细的消息]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_warn_msgDirtyFlag(){
        return this.purchase_line_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [收入科目]
     */
    @JsonProperty("property_account_income_id")
    public Integer getProperty_account_income_id(){
        return this.property_account_income_id ;
    }

    /**
     * 设置 [收入科目]
     */
    @JsonProperty("property_account_income_id")
    public void setProperty_account_income_id(Integer  property_account_income_id){
        this.property_account_income_id = property_account_income_id ;
        this.property_account_income_idDirtyFlag = true ;
    }

    /**
     * 获取 [收入科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_idDirtyFlag(){
        return this.property_account_income_idDirtyFlag ;
    }

    /**
     * 获取 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public String getProperty_cost_method(){
        return this.property_cost_method ;
    }

    /**
     * 设置 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public void setProperty_cost_method(String  property_cost_method){
        this.property_cost_method = property_cost_method ;
        this.property_cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [成本方法]脏标记
     */
    @JsonIgnore
    public boolean getProperty_cost_methodDirtyFlag(){
        return this.property_cost_methodDirtyFlag ;
    }

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }

    /**
     * 获取 [Can be Part]
     */
    @JsonProperty("isparts")
    public String getIsParts(){
        return this.isParts ;
    }

    /**
     * 设置 [Can be Part]
     */
    @JsonProperty("isparts")
    public void setIsParts(String  isParts){
        this.isParts = isParts ;
        this.isPartsDirtyFlag = true ;
    }

    /**
     * 获取 [Can be Part]脏标记
     */
    @JsonIgnore
    public boolean getIsPartsDirtyFlag(){
        return this.isPartsDirtyFlag ;
    }

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("uom_id")
    public Integer getUom_id(){
        return this.uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("uom_id")
    public void setUom_id(Integer  uom_id){
        this.uom_id = uom_id ;
        this.uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getUom_idDirtyFlag(){
        return this.uom_idDirtyFlag ;
    }

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_variant_id")
    public Integer getProduct_variant_id(){
        return this.product_variant_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_variant_id")
    public void setProduct_variant_id(Integer  product_variant_id){
        this.product_variant_id = product_variant_id ;
        this.product_variant_idDirtyFlag = true ;
    }

    /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_idDirtyFlag(){
        return this.product_variant_idDirtyFlag ;
    }

    /**
     * 获取 [产品类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [产品类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [产品类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }

    /**
     * 获取 [控制策略]
     */
    @JsonProperty("purchase_method")
    public String getPurchase_method(){
        return this.purchase_method ;
    }

    /**
     * 设置 [控制策略]
     */
    @JsonProperty("purchase_method")
    public void setPurchase_method(String  purchase_method){
        this.purchase_method = purchase_method ;
        this.purchase_methodDirtyFlag = true ;
    }

    /**
     * 获取 [控制策略]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_methodDirtyFlag(){
        return this.purchase_methodDirtyFlag ;
    }

    /**
     * 获取 [负责人]
     */
    @JsonProperty("responsible_id")
    public Integer getResponsible_id(){
        return this.responsible_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("responsible_id")
    public void setResponsible_id(Integer  responsible_id){
        this.responsible_id = responsible_id ;
        this.responsible_idDirtyFlag = true ;
    }

    /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getResponsible_idDirtyFlag(){
        return this.responsible_idDirtyFlag ;
    }

    /**
     * 获取 [跟踪服务]
     */
    @JsonProperty("service_type")
    public String getService_type(){
        return this.service_type ;
    }

    /**
     * 设置 [跟踪服务]
     */
    @JsonProperty("service_type")
    public void setService_type(String  service_type){
        this.service_type = service_type ;
        this.service_typeDirtyFlag = true ;
    }

    /**
     * 获取 [跟踪服务]脏标记
     */
    @JsonIgnore
    public boolean getService_typeDirtyFlag(){
        return this.service_typeDirtyFlag ;
    }

    /**
     * 获取 [单位名称]
     */
    @JsonProperty("uom_name")
    public String getUom_name(){
        return this.uom_name ;
    }

    /**
     * 设置 [单位名称]
     */
    @JsonProperty("uom_name")
    public void setUom_name(String  uom_name){
        this.uom_name = uom_name ;
        this.uom_nameDirtyFlag = true ;
    }

    /**
     * 获取 [单位名称]脏标记
     */
    @JsonIgnore
    public boolean getUom_nameDirtyFlag(){
        return this.uom_nameDirtyFlag ;
    }

    /**
     * 获取 [可用阈值]
     */
    @JsonProperty("available_threshold")
    public Double getAvailable_threshold(){
        return this.available_threshold ;
    }

    /**
     * 设置 [可用阈值]
     */
    @JsonProperty("available_threshold")
    public void setAvailable_threshold(Double  available_threshold){
        this.available_threshold = available_threshold ;
        this.available_thresholdDirtyFlag = true ;
    }

    /**
     * 获取 [可用阈值]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_thresholdDirtyFlag(){
        return this.available_thresholdDirtyFlag ;
    }

    /**
     * 获取 [采购订单行]
     */
    @JsonProperty("purchase_line_warn")
    public String getPurchase_line_warn(){
        return this.purchase_line_warn ;
    }

    /**
     * 设置 [采购订单行]
     */
    @JsonProperty("purchase_line_warn")
    public void setPurchase_line_warn(String  purchase_line_warn){
        this.purchase_line_warn = purchase_line_warn ;
        this.purchase_line_warnDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_warnDirtyFlag(){
        return this.purchase_line_warnDirtyFlag ;
    }

    /**
     * 获取 [# 产品变体]
     */
    @JsonProperty("product_variant_count")
    public Integer getProduct_variant_count(){
        return this.product_variant_count ;
    }

    /**
     * 设置 [# 产品变体]
     */
    @JsonProperty("product_variant_count")
    public void setProduct_variant_count(Integer  product_variant_count){
        this.product_variant_count = product_variant_count ;
        this.product_variant_countDirtyFlag = true ;
    }

    /**
     * 获取 [# 产品变体]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_countDirtyFlag(){
        return this.product_variant_countDirtyFlag ;
    }

    /**
     * 获取 [POS类别]
     */
    @JsonProperty("pos_categ_id")
    public Integer getPos_categ_id(){
        return this.pos_categ_id ;
    }

    /**
     * 设置 [POS类别]
     */
    @JsonProperty("pos_categ_id")
    public void setPos_categ_id(Integer  pos_categ_id){
        this.pos_categ_id = pos_categ_id ;
        this.pos_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [POS类别]脏标记
     */
    @JsonIgnore
    public boolean getPos_categ_idDirtyFlag(){
        return this.pos_categ_idDirtyFlag ;
    }

    /**
     * 获取 [自定义消息]
     */
    @JsonProperty("custom_message")
    public String getCustom_message(){
        return this.custom_message ;
    }

    /**
     * 设置 [自定义消息]
     */
    @JsonProperty("custom_message")
    public void setCustom_message(String  custom_message){
        this.custom_message = custom_message ;
        this.custom_messageDirtyFlag = true ;
    }

    /**
     * 获取 [自定义消息]脏标记
     */
    @JsonIgnore
    public boolean getCustom_messageDirtyFlag(){
        return this.custom_messageDirtyFlag ;
    }

    /**
     * 获取 [费用科目]
     */
    @JsonProperty("property_account_expense_id")
    public Integer getProperty_account_expense_id(){
        return this.property_account_expense_id ;
    }

    /**
     * 设置 [费用科目]
     */
    @JsonProperty("property_account_expense_id")
    public void setProperty_account_expense_id(Integer  property_account_expense_id){
        this.property_account_expense_id = property_account_expense_id ;
        this.property_account_expense_idDirtyFlag = true ;
    }

    /**
     * 获取 [费用科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_idDirtyFlag(){
        return this.property_account_expense_idDirtyFlag ;
    }

    /**
     * 获取 [客户前置时间]
     */
    @JsonProperty("sale_delay")
    public Double getSale_delay(){
        return this.sale_delay ;
    }

    /**
     * 设置 [客户前置时间]
     */
    @JsonProperty("sale_delay")
    public void setSale_delay(Double  sale_delay){
        this.sale_delay = sale_delay ;
        this.sale_delayDirtyFlag = true ;
    }

    /**
     * 获取 [客户前置时间]脏标记
     */
    @JsonIgnore
    public boolean getSale_delayDirtyFlag(){
        return this.sale_delayDirtyFlag ;
    }

    /**
     * 获取 [采购计量单位]
     */
    @JsonProperty("uom_po_id")
    public Integer getUom_po_id(){
        return this.uom_po_id ;
    }

    /**
     * 设置 [采购计量单位]
     */
    @JsonProperty("uom_po_id")
    public void setUom_po_id(Integer  uom_po_id){
        this.uom_po_id = uom_po_id ;
        this.uom_po_idDirtyFlag = true ;
    }

    /**
     * 获取 [采购计量单位]脏标记
     */
    @JsonIgnore
    public boolean getUom_po_idDirtyFlag(){
        return this.uom_po_idDirtyFlag ;
    }

    /**
     * 获取 [POS可用]
     */
    @JsonProperty("available_in_pos")
    public String getAvailable_in_pos(){
        return this.available_in_pos ;
    }

    /**
     * 设置 [POS可用]
     */
    @JsonProperty("available_in_pos")
    public void setAvailable_in_pos(String  available_in_pos){
        this.available_in_pos = available_in_pos ;
        this.available_in_posDirtyFlag = true ;
    }

    /**
     * 获取 [POS可用]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_in_posDirtyFlag(){
        return this.available_in_posDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }



}

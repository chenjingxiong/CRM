package cn.ibizlab.odoo.web.r7rt_dyna.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.r7rt_dyna.domain.DynaChart;
import cn.ibizlab.odoo.web.r7rt_dyna.filter.*;


public interface DynaChartFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/dynacharts/{dynachart_id}")
    public DynaChart get(@PathVariable("dynachart_id") String dynachart_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/dynacharts/{dynachart_id}/getdraft")
    public DynaChart getDraft(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts/checkkey")
    public Boolean checkKey(@RequestBody DynaChart dynachart) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/dynacharts/{dynachart_id}")
    public DynaChart update(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/dynacharts/{dynachart_id}")
    public Boolean remove(@PathVariable("dynachart_id") String dynachart_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts")
    public DynaChart create(@RequestBody DynaChart dynachart) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts/{dynachart_id}/save")
    public DynaChart save(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/dynacharts/fetchdefault")
	public Page<DynaChart> fetchDefault(DynaChartSearchContext searchContext) ;
}

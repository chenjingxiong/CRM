package cn.ibizlab.odoo.web.odoo_base.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner_p;
import cn.ibizlab.odoo.web.odoo_base.filter.*;


public interface Res_partner_pFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partner_ps/{res_partner_p_id}")
    public Boolean remove(@PathVariable("res_partner_p_id") Integer res_partner_p_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partner_ps/{res_partner_p_id}")
    public Res_partner_p get(@PathVariable("res_partner_p_id") Integer res_partner_p_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/checkkey")
    public Boolean checkKey(@RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/{res_partner_p_id}/save")
    public Res_partner_p save(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partner_ps/{res_partner_p_id}")
    public Res_partner_p update(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/{res_partner_p_id}/createbatch")
    public Res_partner_p createBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partner_ps/{res_partner_p_id}/updatebatch")
    public Res_partner_p updateBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partner_ps/{res_partner_p_id}/getdraft")
    public Res_partner_p getDraft(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps")
    public Res_partner_p create(@RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partner_ps/{res_partner_p_id}/removebatch")
    public Res_partner_p removeBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchcontacts")
	public Page<Res_partner_p> fetchContacts(Res_partner_pSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchcompany")
	public Page<Res_partner_p> fetchCompany(Res_partner_pSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchdefault")
	public Page<Res_partner_p> fetchDefault(Res_partner_pSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    public Boolean removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    public Res_partner_p getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/checkkey")
    public Boolean checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/save")
    public Res_partner_p saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    public Res_partner_p updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/createbatch")
    public Res_partner_p createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/updatebatch")
    public Res_partner_p updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/getdraft")
    public Res_partner_p getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps")
    public Res_partner_p createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_p res_partner_p) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/removebatch")
    public Res_partner_p removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchcontacts")
	public Page<Res_partner_p> fetchContactsByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Res_partner_pSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchcompany")
	public Page<Res_partner_p> fetchCompanyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Res_partner_pSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchdefault")
	public Page<Res_partner_p> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Res_partner_pSearchContext searchContext) ;
}

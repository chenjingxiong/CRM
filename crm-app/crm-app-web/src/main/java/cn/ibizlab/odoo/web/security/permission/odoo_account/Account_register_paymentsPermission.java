package cn.ibizlab.odoo.web.security.permission.odoo_account;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_account.service.Account_register_paymentsService;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("account_register_payments_pms")
public class Account_register_paymentsPermission {

    @Autowired
    Account_register_paymentsService account_register_paymentsservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer account_register_payments_id, String action){
        return true ;
    }


}

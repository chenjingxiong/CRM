package cn.ibizlab.odoo.web.odoo_sale.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.web.odoo_sale.filter.*;


public interface Sale_orderFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_orders/{sale_order_id}/updatebatch")
    public Sale_order updateBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_orders/{sale_order_id}/removebatch")
    public Sale_order removeBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/sale_orders/{sale_order_id}")
    public Sale_order update(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_orders/{sale_order_id}")
    public Sale_order get(@PathVariable("sale_order_id") Integer sale_order_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/{sale_order_id}/save")
    public Sale_order save(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/checkkey")
    public Boolean checkKey(@RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/sale_orders/{sale_order_id}")
    public Boolean remove(@PathVariable("sale_order_id") Integer sale_order_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/sale_orders/{sale_order_id}/getdraft")
    public Sale_order getDraft(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders/{sale_order_id}/createbatch")
    public Sale_order createBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/sale_orders")
    public Sale_order create(@RequestBody Sale_order sale_order) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/sale_orders/fetchdefault")
	public Page<Sale_order> fetchDefault(Sale_orderSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/updatebatch")
    public Sale_order updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/removebatch")
    public Sale_order removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    public Sale_order updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    public Sale_order getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/save")
    public Sale_order saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/checkkey")
    public Boolean checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    public Boolean removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/getdraft")
    public Sale_order getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/createbatch")
    public Sale_order createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/sale_orders")
    public Sale_order createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_order sale_order) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/sale_orders/fetchdefault")
	public Page<Sale_order> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Sale_orderSearchContext searchContext) ;
}

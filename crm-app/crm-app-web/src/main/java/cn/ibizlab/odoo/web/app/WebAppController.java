package cn.ibizlab.odoo.web.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.security.userdetail.LoginUser;
import cn.ibizlab.odoo.util.web.SessionConstants;
import cn.ibizlab.odoo.util.web.AppContextConstants;

@RestController
@RequestMapping(value = "")
public class WebAppController {

	@RequestMapping(method = RequestMethod.GET, value = "/appdata")
	public ResponseEntity<JSONObject> getAppData() {
		JSONObject appData = new JSONObject() ;
		JSONObject context = new JSONObject() ;
		List<Map> orgDepts = new ArrayList<Map>();
		LoginUser loginUser = SpringContextHolder.getCurLoginUser();
		if (loginUser != null) {
			context.put(AppContextConstants.CONTEXT_USERID, loginUser.getPersonId());
			context.put(AppContextConstants.CONTEXT_USERNAME, loginUser.getOrgUserName());
			context.put(AppContextConstants.CONTEXT_USERICONPATH, loginUser.getUserIconPath());
			context.put(AppContextConstants.CONTEXT_USERMODE, loginUser.getUserMode());
			context.put(AppContextConstants.CONTEXT_LOGINNAME, loginUser.getUsername());
			context.put(AppContextConstants.CONTEXT_LOCALE, loginUser.getLocale());
			context.put(AppContextConstants.CONTEXT_TIMEZONE, loginUser.getTimeZone());
			context.put(AppContextConstants.CONTEXT_ORGID, loginUser.getOrgId());
			context.put(AppContextConstants.CONTEXT_ORGNAME, loginUser.getOrgName());
			context.put(AppContextConstants.CONTEXT_ORGSECTORID, loginUser.getOrgDeptId());
			context.put(AppContextConstants.CONTEXT_ORGSECTORNAME, loginUser.getOrgDeptName());
			orgDepts = loginUser.getOrgDepts() ;
		}
		appData.put(AppContextConstants.CONTEXT_ORGSECTORS, orgDepts) ;
		appData.put("context", context);
		return ResponseEntity.status(HttpStatus.OK).body(appData);
	}

}

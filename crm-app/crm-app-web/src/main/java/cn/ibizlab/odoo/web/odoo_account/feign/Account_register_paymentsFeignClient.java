package cn.ibizlab.odoo.web.odoo_account.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.web.odoo_account.filter.*;


public interface Account_register_paymentsFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/account_register_payments/checkkey")
    public Boolean checkKey(@RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_register_payments/{account_register_payments_id}/save")
    public Account_register_payments save(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_register_payments/{account_register_payments_id}/updatebatch")
    public Account_register_payments updateBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_register_payments/{account_register_payments_id}")
    public Account_register_payments get(@PathVariable("account_register_payments_id") Integer account_register_payments_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_register_payments/{account_register_payments_id}/removebatch")
    public Account_register_payments removeBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_register_payments")
    public Account_register_payments create(@RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_register_payments/{account_register_payments_id}")
    public Account_register_payments update(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_register_payments/{account_register_payments_id}/createbatch")
    public Account_register_payments createBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_register_payments/{account_register_payments_id}")
    public Boolean remove(@PathVariable("account_register_payments_id") Integer account_register_payments_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_register_payments/{account_register_payments_id}/getdraft")
    public Account_register_payments getDraft(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/account_register_payments/fetchdefault")
	public Page<Account_register_payments> fetchDefault(Account_register_paymentsSearchContext searchContext) ;
}

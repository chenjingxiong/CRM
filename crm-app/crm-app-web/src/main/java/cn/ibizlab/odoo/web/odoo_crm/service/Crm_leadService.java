package cn.ibizlab.odoo.web.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.web.odoo_crm.filter.*;
import cn.ibizlab.odoo.web.odoo_crm.feign.Crm_leadFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_leadService {

    Crm_leadFeignClient client;

    @Autowired
    public Crm_leadService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_leadFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_leadFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_lead getDraft(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.getDraft(crm_lead_id, crm_lead);
    }

    public boolean remove( Integer crm_lead_id) {
        return client.remove( crm_lead_id);
    }

    public Crm_lead save(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.save(crm_lead_id, crm_lead);
    }

    public Crm_lead updateBatch(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.updateBatch(crm_lead_id, crm_lead);
    }

    public Crm_lead get( Integer crm_lead_id) {
        return client.get( crm_lead_id);
    }

    public Crm_lead createBatch(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.createBatch(crm_lead_id, crm_lead);
    }

    public Crm_lead removeBatch(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.removeBatch(crm_lead_id, crm_lead);
    }

	public Crm_lead create(Crm_lead crm_lead) {
        return client.create(crm_lead);
    }

    public boolean checkKey(Crm_lead crm_lead) {
        return client.checkKey(crm_lead);
    }

    public Crm_lead update(Integer crm_lead_id, Crm_lead crm_lead) {
        return client.update(crm_lead_id, crm_lead);
    }

	public Page<Crm_lead> fetchDefault(Crm_leadSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

	public Page<Crm_lead> fetchDefault2(Crm_leadSearchContext searchContext) {
        return client.fetchDefault2(searchContext);
    }


    public Crm_lead getDraftByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.getDraftByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

    public boolean removeByRes_partner(Integer res_partner_id,  Integer crm_lead_id) {
        return client.removeByRes_partner(res_partner_id,  crm_lead_id);
    }

    public Crm_lead saveByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.saveByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

    public Crm_lead updateBatchByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.updateBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

    public Crm_lead getByRes_partner(Integer res_partner_id,  Integer crm_lead_id) {
        return client.getByRes_partner(res_partner_id,  crm_lead_id);
    }

    public Crm_lead createBatchByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.createBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

    public Crm_lead removeBatchByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.removeBatchByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

	public Crm_lead createByRes_partner(Integer res_partner_id, Crm_lead crm_lead) {
        return client.createByRes_partner(res_partner_id, crm_lead);
    }

    public boolean checkKeyByRes_partner(Integer res_partner_id, Crm_lead crm_lead) {
        return client.checkKeyByRes_partner(res_partner_id, crm_lead);
    }

    public Crm_lead updateByRes_partner(Integer res_partner_id, Integer crm_lead_id, Crm_lead crm_lead) {
        return client.updateByRes_partner(res_partner_id, crm_lead_id, crm_lead);
    }

	public Page<Crm_lead> fetchDefaultByRes_partner(Integer res_partner_id , Crm_leadSearchContext searchContext) {
        return client.fetchDefaultByRes_partner(res_partner_id , searchContext);
    }

	public Page<Crm_lead> fetchDefault2ByRes_partner(Integer res_partner_id , Crm_leadSearchContext searchContext) {
        return client.fetchDefault2ByRes_partner(res_partner_id , searchContext);
    }

}

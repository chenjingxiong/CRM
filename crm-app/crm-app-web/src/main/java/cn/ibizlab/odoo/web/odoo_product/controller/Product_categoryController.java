package cn.ibizlab.odoo.web.odoo_product.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_product.service.Product_categoryService;
import cn.ibizlab.odoo.web.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.web.odoo_product.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Product_categoryController {
	@Autowired
    Product_categoryService product_categoryservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/{product_category_id}")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("product_category_id") Integer product_category_id) {
        boolean b = product_categoryservice.remove( product_category_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories")
    @PreAuthorize("@product_category_pms.check('CREATE')")
    public ResponseEntity<Product_category> create(@RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.create(product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/{product_category_id}/updatebatch")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'UPDATE')")
    public ResponseEntity<Product_category> updateBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.updateBatch(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/{product_category_id}/removebatch")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'DELETE')")
    public ResponseEntity<Product_category> removeBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.removeBatch(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/{product_category_id}")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'UPDATE')")
    public ResponseEntity<Product_category> update(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.update(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/product_categories/{product_category_id}")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'UPDATE')")
    public ResponseEntity<Product_category> api_update(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.update(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/{product_category_id}/save")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'')")
    public ResponseEntity<Product_category> save(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.save(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_categories/getdraft")
    @PreAuthorize("@product_category_pms.check('CREATE')")
    public ResponseEntity<Product_category> getDraft() {
        //Product_category product_category = product_categoryservice.getDraft( product_category_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Product_category());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/{product_category_id}/createbatch")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'CREATE')")
    public ResponseEntity<Product_category> createBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) {
        Product_category product_category2 = product_categoryservice.createBatch(product_category_id, product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_category2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/checkkey")
    @PreAuthorize("@product_category_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_category product_category) {
        boolean b = product_categoryservice.checkKey(product_category);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_categories/{product_category_id}")
    @PreAuthorize("@product_category_pms.check(#product_category_id,'READ')")
    public ResponseEntity<Product_category> get(@PathVariable("product_category_id") Integer product_category_id) {
        Product_category product_category = product_categoryservice.get( product_category_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_category);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/product_categories/fetchdefault")
    @PreAuthorize("@product_category_pms.check('READ')")
	public ResponseEntity<List<Product_category>> fetchDefault(Product_categorySearchContext searchContext,Pageable pageable) {
        
        Page<Product_category> page = product_categoryservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

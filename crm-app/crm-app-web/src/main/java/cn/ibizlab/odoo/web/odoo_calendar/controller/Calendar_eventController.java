package cn.ibizlab.odoo.web.odoo_calendar.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_calendar.service.Calendar_eventService;
import cn.ibizlab.odoo.web.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.web.odoo_calendar.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Calendar_eventController {
	@Autowired
    Calendar_eventService calendar_eventservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/{calendar_event_id}")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'UPDATE')")
    public ResponseEntity<Calendar_event> update(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.update(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/calendar_events/{calendar_event_id}")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'UPDATE')")
    public ResponseEntity<Calendar_event> api_update(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.update(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/{calendar_event_id}/updatebatch")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'UPDATE')")
    public ResponseEntity<Calendar_event> updateBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.updateBatch(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/{calendar_event_id}/createbatch")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'CREATE')")
    public ResponseEntity<Calendar_event> createBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.createBatch(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_events/{calendar_event_id}")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'READ')")
    public ResponseEntity<Calendar_event> get(@PathVariable("calendar_event_id") Integer calendar_event_id) {
        Calendar_event calendar_event = calendar_eventservice.get( calendar_event_id);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/{calendar_event_id}/removebatch")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'DELETE')")
    public ResponseEntity<Calendar_event> removeBatch(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.removeBatch(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/{calendar_event_id}")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_id") Integer calendar_event_id) {
        boolean b = calendar_eventservice.remove( calendar_event_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_events/getdraft")
    @PreAuthorize("@calendar_event_pms.check('CREATE')")
    public ResponseEntity<Calendar_event> getDraft() {
        //Calendar_event calendar_event = calendar_eventservice.getDraft( calendar_event_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Calendar_event());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events")
    @PreAuthorize("@calendar_event_pms.check('CREATE')")
    public ResponseEntity<Calendar_event> create(@RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.create(calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/{calendar_event_id}/save")
    @PreAuthorize("@calendar_event_pms.check(#calendar_event_id,'')")
    public ResponseEntity<Calendar_event> save(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_event calendar_event) {
        Calendar_event calendar_event2 = calendar_eventservice.save(calendar_event_id, calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/checkkey")
    @PreAuthorize("@calendar_event_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_event calendar_event) {
        boolean b = calendar_eventservice.checkKey(calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/calendar_events/fetchdefault")
    @PreAuthorize("@calendar_event_pms.check('READ')")
	public ResponseEntity<List<Calendar_event>> fetchDefault(Calendar_eventSearchContext searchContext,Pageable pageable) {
        
        Page<Calendar_event> page = calendar_eventservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

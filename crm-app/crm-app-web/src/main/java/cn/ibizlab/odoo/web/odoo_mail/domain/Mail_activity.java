package cn.ibizlab.odoo.web.odoo_mail.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[活动]
 */
public class Mail_activity implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 邮件模板
     */
    private String mail_template_ids;

    @JsonIgnore
    private boolean mail_template_idsDirtyFlag;
    
    /**
     * 自动活动
     */
    private String automated;

    @JsonIgnore
    private boolean automatedDirtyFlag;
    
    /**
     * 文档名称
     */
    private String res_name;

    @JsonIgnore
    private boolean res_nameDirtyFlag;
    
    /**
     * 状态
     */
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 到期时间
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 摘要
     */
    private String summary;

    @JsonIgnore
    private boolean summaryDirtyFlag;
    
    /**
     * 下一活动可用
     */
    private String has_recommended_activities;

    @JsonIgnore
    private boolean has_recommended_activitiesDirtyFlag;
    
    /**
     * 相关文档编号
     */
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;
    
    /**
     * 备注
     */
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;
    
    /**
     * 反馈
     */
    private String feedback;

    @JsonIgnore
    private boolean feedbackDirtyFlag;
    
    /**
     * 文档模型
     */
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;
    
    /**
     * 分派给
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 类别
     */
    private String activity_category;

    @JsonIgnore
    private boolean activity_categoryDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_type_id_text;

    @JsonIgnore
    private boolean activity_type_id_textDirtyFlag;
    
    /**
     * 前一活动类型
     */
    private String previous_activity_type_id_text;

    @JsonIgnore
    private boolean previous_activity_type_id_textDirtyFlag;
    
    /**
     * 相关便签
     */
    private String note_id_text;

    @JsonIgnore
    private boolean note_id_textDirtyFlag;
    
    /**
     * 建立者
     */
    private String create_user_id_text;

    @JsonIgnore
    private boolean create_user_id_textDirtyFlag;
    
    /**
     * 图标
     */
    private String icon;

    @JsonIgnore
    private boolean iconDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 排版类型
     */
    private String activity_decoration;

    @JsonIgnore
    private boolean activity_decorationDirtyFlag;
    
    /**
     * 最后更新者
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 自动安排下一个活动
     */
    private String force_next;

    @JsonIgnore
    private boolean force_nextDirtyFlag;
    
    /**
     * 推荐的活动类型
     */
    private String recommended_activity_type_id_text;

    @JsonIgnore
    private boolean recommended_activity_type_id_textDirtyFlag;
    
    /**
     * 日历会议
     */
    private String calendar_event_id_text;

    @JsonIgnore
    private boolean calendar_event_id_textDirtyFlag;
    
    /**
     * 推荐的活动类型
     */
    private Integer recommended_activity_type_id;

    @JsonIgnore
    private boolean recommended_activity_type_idDirtyFlag;
    
    /**
     * 活动
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 建立者
     */
    private Integer create_user_id;

    @JsonIgnore
    private boolean create_user_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 分派给
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 相关便签
     */
    private Integer note_id;

    @JsonIgnore
    private boolean note_idDirtyFlag;
    
    /**
     * 前一活动类型
     */
    private Integer previous_activity_type_id;

    @JsonIgnore
    private boolean previous_activity_type_idDirtyFlag;
    
    /**
     * 日历会议
     */
    private Integer calendar_event_id;

    @JsonIgnore
    private boolean calendar_event_idDirtyFlag;
    
    /**
     * 最后更新者
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return this.mail_template_ids ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

    /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return this.mail_template_idsDirtyFlag ;
    }

    /**
     * 获取 [自动活动]
     */
    @JsonProperty("automated")
    public String getAutomated(){
        return this.automated ;
    }

    /**
     * 设置 [自动活动]
     */
    @JsonProperty("automated")
    public void setAutomated(String  automated){
        this.automated = automated ;
        this.automatedDirtyFlag = true ;
    }

    /**
     * 获取 [自动活动]脏标记
     */
    @JsonIgnore
    public boolean getAutomatedDirtyFlag(){
        return this.automatedDirtyFlag ;
    }

    /**
     * 获取 [文档名称]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return this.res_name ;
    }

    /**
     * 设置 [文档名称]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

    /**
     * 获取 [文档名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return this.res_nameDirtyFlag ;
    }

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [到期时间]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return this.date_deadline ;
    }

    /**
     * 设置 [到期时间]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [到期时间]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return this.date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [摘要]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return this.summary ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

    /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return this.summaryDirtyFlag ;
    }

    /**
     * 获取 [下一活动可用]
     */
    @JsonProperty("has_recommended_activities")
    public String getHas_recommended_activities(){
        return this.has_recommended_activities ;
    }

    /**
     * 设置 [下一活动可用]
     */
    @JsonProperty("has_recommended_activities")
    public void setHas_recommended_activities(String  has_recommended_activities){
        this.has_recommended_activities = has_recommended_activities ;
        this.has_recommended_activitiesDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动可用]脏标记
     */
    @JsonIgnore
    public boolean getHas_recommended_activitiesDirtyFlag(){
        return this.has_recommended_activitiesDirtyFlag ;
    }

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }

    /**
     * 获取 [反馈]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return this.feedback ;
    }

    /**
     * 设置 [反馈]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [反馈]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return this.feedbackDirtyFlag ;
    }

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [类别]
     */
    @JsonProperty("activity_category")
    public String getActivity_category(){
        return this.activity_category ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("activity_category")
    public void setActivity_category(String  activity_category){
        this.activity_category = activity_category ;
        this.activity_categoryDirtyFlag = true ;
    }

    /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getActivity_categoryDirtyFlag(){
        return this.activity_categoryDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_type_id_text")
    public String getActivity_type_id_text(){
        return this.activity_type_id_text ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_type_id_text")
    public void setActivity_type_id_text(String  activity_type_id_text){
        this.activity_type_id_text = activity_type_id_text ;
        this.activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_id_textDirtyFlag(){
        return this.activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id_text")
    public String getPrevious_activity_type_id_text(){
        return this.previous_activity_type_id_text ;
    }

    /**
     * 设置 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id_text")
    public void setPrevious_activity_type_id_text(String  previous_activity_type_id_text){
        this.previous_activity_type_id_text = previous_activity_type_id_text ;
        this.previous_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [前一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_id_textDirtyFlag(){
        return this.previous_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [相关便签]
     */
    @JsonProperty("note_id_text")
    public String getNote_id_text(){
        return this.note_id_text ;
    }

    /**
     * 设置 [相关便签]
     */
    @JsonProperty("note_id_text")
    public void setNote_id_text(String  note_id_text){
        this.note_id_text = note_id_text ;
        this.note_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [相关便签]脏标记
     */
    @JsonIgnore
    public boolean getNote_id_textDirtyFlag(){
        return this.note_id_textDirtyFlag ;
    }

    /**
     * 获取 [建立者]
     */
    @JsonProperty("create_user_id_text")
    public String getCreate_user_id_text(){
        return this.create_user_id_text ;
    }

    /**
     * 设置 [建立者]
     */
    @JsonProperty("create_user_id_text")
    public void setCreate_user_id_text(String  create_user_id_text){
        this.create_user_id_text = create_user_id_text ;
        this.create_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [建立者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_id_textDirtyFlag(){
        return this.create_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [图标]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return this.icon ;
    }

    /**
     * 设置 [图标]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

    /**
     * 获取 [图标]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return this.iconDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [排版类型]
     */
    @JsonProperty("activity_decoration")
    public String getActivity_decoration(){
        return this.activity_decoration ;
    }

    /**
     * 设置 [排版类型]
     */
    @JsonProperty("activity_decoration")
    public void setActivity_decoration(String  activity_decoration){
        this.activity_decoration = activity_decoration ;
        this.activity_decorationDirtyFlag = true ;
    }

    /**
     * 获取 [排版类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_decorationDirtyFlag(){
        return this.activity_decorationDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return this.force_next ;
    }

    /**
     * 设置 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return this.force_nextDirtyFlag ;
    }

    /**
     * 获取 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public String getRecommended_activity_type_id_text(){
        return this.recommended_activity_type_id_text ;
    }

    /**
     * 设置 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public void setRecommended_activity_type_id_text(String  recommended_activity_type_id_text){
        this.recommended_activity_type_id_text = recommended_activity_type_id_text ;
        this.recommended_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_id_textDirtyFlag(){
        return this.recommended_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [日历会议]
     */
    @JsonProperty("calendar_event_id_text")
    public String getCalendar_event_id_text(){
        return this.calendar_event_id_text ;
    }

    /**
     * 设置 [日历会议]
     */
    @JsonProperty("calendar_event_id_text")
    public void setCalendar_event_id_text(String  calendar_event_id_text){
        this.calendar_event_id_text = calendar_event_id_text ;
        this.calendar_event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [日历会议]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_id_textDirtyFlag(){
        return this.calendar_event_id_textDirtyFlag ;
    }

    /**
     * 获取 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id")
    public Integer getRecommended_activity_type_id(){
        return this.recommended_activity_type_id ;
    }

    /**
     * 设置 [推荐的活动类型]
     */
    @JsonProperty("recommended_activity_type_id")
    public void setRecommended_activity_type_id(Integer  recommended_activity_type_id){
        this.recommended_activity_type_id = recommended_activity_type_id ;
        this.recommended_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_idDirtyFlag(){
        return this.recommended_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [建立者]
     */
    @JsonProperty("create_user_id")
    public Integer getCreate_user_id(){
        return this.create_user_id ;
    }

    /**
     * 设置 [建立者]
     */
    @JsonProperty("create_user_id")
    public void setCreate_user_id(Integer  create_user_id){
        this.create_user_id = create_user_id ;
        this.create_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [建立者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_idDirtyFlag(){
        return this.create_user_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [分派给]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [分派给]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [分派给]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [相关便签]
     */
    @JsonProperty("note_id")
    public Integer getNote_id(){
        return this.note_id ;
    }

    /**
     * 设置 [相关便签]
     */
    @JsonProperty("note_id")
    public void setNote_id(Integer  note_id){
        this.note_id = note_id ;
        this.note_idDirtyFlag = true ;
    }

    /**
     * 获取 [相关便签]脏标记
     */
    @JsonIgnore
    public boolean getNote_idDirtyFlag(){
        return this.note_idDirtyFlag ;
    }

    /**
     * 获取 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id")
    public Integer getPrevious_activity_type_id(){
        return this.previous_activity_type_id ;
    }

    /**
     * 设置 [前一活动类型]
     */
    @JsonProperty("previous_activity_type_id")
    public void setPrevious_activity_type_id(Integer  previous_activity_type_id){
        this.previous_activity_type_id = previous_activity_type_id ;
        this.previous_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [前一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_idDirtyFlag(){
        return this.previous_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [日历会议]
     */
    @JsonProperty("calendar_event_id")
    public Integer getCalendar_event_id(){
        return this.calendar_event_id ;
    }

    /**
     * 设置 [日历会议]
     */
    @JsonProperty("calendar_event_id")
    public void setCalendar_event_id(Integer  calendar_event_id){
        this.calendar_event_id = calendar_event_id ;
        this.calendar_event_idDirtyFlag = true ;
    }

    /**
     * 获取 [日历会议]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_idDirtyFlag(){
        return this.calendar_event_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }



}

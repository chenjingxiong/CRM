package cn.ibizlab.odoo.web.odoo_account.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_account.service.Account_register_paymentsService;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Account_register_paymentsController {
	@Autowired
    Account_register_paymentsService account_register_paymentsservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/checkkey")
    @PreAuthorize("@account_register_payments_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_register_payments account_register_payments) {
        boolean b = account_register_paymentsservice.checkKey(account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/{account_register_payments_id}/save")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'')")
    public ResponseEntity<Account_register_payments> save(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.save(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/{account_register_payments_id}/updatebatch")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'UPDATE')")
    public ResponseEntity<Account_register_payments> updateBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.updateBatch(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/{account_register_payments_id}")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'READ')")
    public ResponseEntity<Account_register_payments> get(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
        Account_register_payments account_register_payments = account_register_paymentsservice.get( account_register_payments_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/{account_register_payments_id}/removebatch")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'DELETE')")
    public ResponseEntity<Account_register_payments> removeBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.removeBatch(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments")
    @PreAuthorize("@account_register_payments_pms.check('CREATE')")
    public ResponseEntity<Account_register_payments> create(@RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.create(account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/{account_register_payments_id}")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'UPDATE')")
    public ResponseEntity<Account_register_payments> update(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.update(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/account_register_payments/{account_register_payments_id}")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'UPDATE')")
    public ResponseEntity<Account_register_payments> api_update(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.update(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/{account_register_payments_id}/createbatch")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'CREATE')")
    public ResponseEntity<Account_register_payments> createBatch(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_payments account_register_payments) {
        Account_register_payments account_register_payments2 = account_register_paymentsservice.createBatch(account_register_payments_id, account_register_payments);
        return ResponseEntity.status(HttpStatus.OK).body(account_register_payments2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/{account_register_payments_id}")
    @PreAuthorize("@account_register_payments_pms.check(#account_register_payments_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
        boolean b = account_register_paymentsservice.remove( account_register_payments_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/getdraft")
    @PreAuthorize("@account_register_payments_pms.check('CREATE')")
    public ResponseEntity<Account_register_payments> getDraft() {
        //Account_register_payments account_register_payments = account_register_paymentsservice.getDraft( account_register_payments_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_register_payments());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/account_register_payments/fetchdefault")
    @PreAuthorize("@account_register_payments_pms.check('READ')")
	public ResponseEntity<List<Account_register_payments>> fetchDefault(Account_register_paymentsSearchContext searchContext,Pageable pageable) {
        
        Page<Account_register_payments> page = account_register_paymentsservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

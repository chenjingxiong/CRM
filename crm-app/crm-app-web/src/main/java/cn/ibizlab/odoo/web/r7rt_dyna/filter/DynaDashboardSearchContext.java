package cn.ibizlab.odoo.web.r7rt_dyna.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class DynaDashboardSearchContext extends SearchContext implements Serializable {

	public String n_dynadashboardname_like;//[实体名称]

}
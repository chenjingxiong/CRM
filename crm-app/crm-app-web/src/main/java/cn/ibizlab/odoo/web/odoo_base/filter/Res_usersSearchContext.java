package cn.ibizlab.odoo.web.odoo_base.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Res_usersSearchContext extends SearchContext implements Serializable {

	public String n_notification_type_eq;//[通知管理]
	public String n_state_eq;//[状态]
	public String n_odoobot_state_eq;//[OdooBot 状态]
	public String n_sale_team_id_text_eq;//[用户的销售团队]
	public String n_sale_team_id_text_like;//[用户的销售团队]
	public String n_write_uid_text_eq;//[最后更新者]
	public String n_write_uid_text_like;//[最后更新者]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_name_eq;//[名称]
	public String n_name_like;//[名称]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public Integer n_company_id_eq;//[公司]
	public Integer n_partner_id_eq;//[相关的业务伙伴]
	public Integer n_alias_id_eq;//[别名]
	public Integer n_sale_team_id_eq;//[用户的销售团队]
	public Integer n_write_uid_eq;//[最后更新者]
	public Integer n_create_uid_eq;//[创建人]

}
package cn.ibizlab.odoo.web.odoo_sale.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.web.odoo_sale.filter.*;


public interface Sale_order_lineFeignClient {

}

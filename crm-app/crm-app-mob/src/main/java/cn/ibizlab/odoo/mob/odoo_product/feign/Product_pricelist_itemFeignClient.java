package cn.ibizlab.odoo.mob.odoo_product.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;


public interface Product_pricelist_itemFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelist_items/{product_pricelist_item_id}")
    public Product_pricelist_item get(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items")
    public Product_pricelist_item create(@RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelist_items/{product_pricelist_item_id}")
    public Boolean remove(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelist_items/{product_pricelist_item_id}/getdraft")
    public Product_pricelist_item getDraft(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/{product_pricelist_item_id}/createbatch")
    public Product_pricelist_item createBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelist_items/{product_pricelist_item_id}/updatebatch")
    public Product_pricelist_item updateBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelist_items/{product_pricelist_item_id}/removebatch")
    public Product_pricelist_item removeBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelist_items/{product_pricelist_item_id}")
    public Product_pricelist_item update(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/checkkey")
    public Boolean checkKey(@RequestBody Product_pricelist_item product_pricelist_item) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/{product_pricelist_item_id}/save")
    public Product_pricelist_item save(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/product_pricelist_items/fetchdefault")
	public Page<Product_pricelist_item> fetchDefault(Product_pricelist_itemSearchContext searchContext) ;
}

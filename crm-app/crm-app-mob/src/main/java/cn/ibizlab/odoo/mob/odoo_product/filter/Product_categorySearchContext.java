package cn.ibizlab.odoo.mob.odoo_product.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Product_categorySearchContext extends SearchContext implements Serializable {

	public String n_name_like;//[名称]
	public String n_property_valuation_eq;//[库存计价]
	public String n_property_cost_method_eq;//[成本方法]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_parent_id_text_eq;//[上级类别]
	public String n_parent_id_text_like;//[上级类别]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_removal_strategy_id_text_eq;//[强制下架策略]
	public String n_removal_strategy_id_text_like;//[强制下架策略]
	public Integer n_parent_id_eq;//[上级类别]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_removal_strategy_id_eq;//[强制下架策略]
	public Integer n_write_uid_eq;//[最后更新人]

}
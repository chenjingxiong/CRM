package cn.ibizlab.odoo.mob.security.permission.r7rt_dyna;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.r7rt_dyna.service.DynaChartService;
import cn.ibizlab.odoo.mob.r7rt_dyna.domain.DynaChart;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("dynachart_pms")
public class DynaChartPermission {

    @Autowired
    DynaChartService dynachartservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(String dynachart_id, String action){
        return true ;
    }


}

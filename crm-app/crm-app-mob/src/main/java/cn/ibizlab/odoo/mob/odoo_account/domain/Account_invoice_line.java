package cn.ibizlab.odoo.mob.odoo_account.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[发票行]
 */
public class Account_invoice_line implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 显示类型
     */
    private String display_type;

    @JsonIgnore
    private boolean display_typeDirtyFlag;
    
    /**
     * 说明
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 金额 (不含税)
     */
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 源文档
     */
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 销售订单明细
     */
    private String sale_line_ids;

    @JsonIgnore
    private boolean sale_line_idsDirtyFlag;
    
    /**
     * 折扣 (%)
     */
    private Double discount;

    @JsonIgnore
    private boolean discountDirtyFlag;
    
    /**
     * 金额 (含税)
     */
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 数量
     */
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;
    
    /**
     * 税率金额
     */
    private Double price_tax;

    @JsonIgnore
    private boolean price_taxDirtyFlag;
    
    /**
     * 签核的金额
     */
    private Double price_subtotal_signed;

    @JsonIgnore
    private boolean price_subtotal_signedDirtyFlag;
    
    /**
     * 序号
     */
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;
    
    /**
     * 单价
     */
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 税率设置
     */
    private String invoice_line_tax_ids;

    @JsonIgnore
    private boolean invoice_line_tax_idsDirtyFlag;
    
    /**
     * 分析标签
     */
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 舍入明细
     */
    private String is_rounding_line;

    @JsonIgnore
    private boolean is_rounding_lineDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 币种
     */
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;
    
    /**
     * 分析账户
     */
    private String account_analytic_id_text;

    @JsonIgnore
    private boolean account_analytic_id_textDirtyFlag;
    
    /**
     * 产品
     */
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;
    
    /**
     * 发票参考
     */
    private String invoice_id_text;

    @JsonIgnore
    private boolean invoice_id_textDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 类型
     */
    private String invoice_type;

    @JsonIgnore
    private boolean invoice_typeDirtyFlag;
    
    /**
     * 产品图片
     */
    private byte[] product_image;

    @JsonIgnore
    private boolean product_imageDirtyFlag;
    
    /**
     * 计量单位
     */
    private String uom_id_text;

    @JsonIgnore
    private boolean uom_id_textDirtyFlag;
    
    /**
     * 公司货币
     */
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;
    
    /**
     * 采购订单
     */
    private Integer purchase_id;

    @JsonIgnore
    private boolean purchase_idDirtyFlag;
    
    /**
     * 科目
     */
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;
    
    /**
     * 采购订单行
     */
    private String purchase_line_id_text;

    @JsonIgnore
    private boolean purchase_line_id_textDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 发票参考
     */
    private Integer invoice_id;

    @JsonIgnore
    private boolean invoice_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 产品
     */
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;
    
    /**
     * 科目
     */
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;
    
    /**
     * 采购订单行
     */
    private Integer purchase_line_id;

    @JsonIgnore
    private boolean purchase_line_idDirtyFlag;
    
    /**
     * 分析账户
     */
    private Integer account_analytic_id;

    @JsonIgnore
    private boolean account_analytic_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 计量单位
     */
    private Integer uom_id;

    @JsonIgnore
    private boolean uom_idDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    

    /**
     * 获取 [显示类型]
     */
    @JsonProperty("display_type")
    public String getDisplay_type(){
        return this.display_type ;
    }

    /**
     * 设置 [显示类型]
     */
    @JsonProperty("display_type")
    public void setDisplay_type(String  display_type){
        this.display_type = display_type ;
        this.display_typeDirtyFlag = true ;
    }

    /**
     * 获取 [显示类型]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_typeDirtyFlag(){
        return this.display_typeDirtyFlag ;
    }

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [金额 (不含税)]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return this.price_subtotal ;
    }

    /**
     * 设置 [金额 (不含税)]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [金额 (不含税)]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return this.price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [销售订单明细]
     */
    @JsonProperty("sale_line_ids")
    public String getSale_line_ids(){
        return this.sale_line_ids ;
    }

    /**
     * 设置 [销售订单明细]
     */
    @JsonProperty("sale_line_ids")
    public void setSale_line_ids(String  sale_line_ids){
        this.sale_line_ids = sale_line_ids ;
        this.sale_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单明细]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idsDirtyFlag(){
        return this.sale_line_idsDirtyFlag ;
    }

    /**
     * 获取 [折扣 (%)]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return this.discount ;
    }

    /**
     * 设置 [折扣 (%)]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

    /**
     * 获取 [折扣 (%)]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return this.discountDirtyFlag ;
    }

    /**
     * 获取 [金额 (含税)]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [金额 (含税)]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [金额 (含税)]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [数量]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return this.quantity ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return this.quantityDirtyFlag ;
    }

    /**
     * 获取 [税率金额]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return this.price_tax ;
    }

    /**
     * 设置 [税率金额]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

    /**
     * 获取 [税率金额]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return this.price_taxDirtyFlag ;
    }

    /**
     * 获取 [签核的金额]
     */
    @JsonProperty("price_subtotal_signed")
    public Double getPrice_subtotal_signed(){
        return this.price_subtotal_signed ;
    }

    /**
     * 设置 [签核的金额]
     */
    @JsonProperty("price_subtotal_signed")
    public void setPrice_subtotal_signed(Double  price_subtotal_signed){
        this.price_subtotal_signed = price_subtotal_signed ;
        this.price_subtotal_signedDirtyFlag = true ;
    }

    /**
     * 获取 [签核的金额]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotal_signedDirtyFlag(){
        return this.price_subtotal_signedDirtyFlag ;
    }

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [税率设置]
     */
    @JsonProperty("invoice_line_tax_ids")
    public String getInvoice_line_tax_ids(){
        return this.invoice_line_tax_ids ;
    }

    /**
     * 设置 [税率设置]
     */
    @JsonProperty("invoice_line_tax_ids")
    public void setInvoice_line_tax_ids(String  invoice_line_tax_ids){
        this.invoice_line_tax_ids = invoice_line_tax_ids ;
        this.invoice_line_tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [税率设置]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_tax_idsDirtyFlag(){
        return this.invoice_line_tax_idsDirtyFlag ;
    }

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [舍入明细]
     */
    @JsonProperty("is_rounding_line")
    public String getIs_rounding_line(){
        return this.is_rounding_line ;
    }

    /**
     * 设置 [舍入明细]
     */
    @JsonProperty("is_rounding_line")
    public void setIs_rounding_line(String  is_rounding_line){
        this.is_rounding_line = is_rounding_line ;
        this.is_rounding_lineDirtyFlag = true ;
    }

    /**
     * 获取 [舍入明细]脏标记
     */
    @JsonIgnore
    public boolean getIs_rounding_lineDirtyFlag(){
        return this.is_rounding_lineDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return this.account_analytic_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return this.account_analytic_id_textDirtyFlag ;
    }

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }

    /**
     * 获取 [发票参考]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return this.invoice_id_text ;
    }

    /**
     * 设置 [发票参考]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [发票参考]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return this.invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("invoice_type")
    public String getInvoice_type(){
        return this.invoice_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("invoice_type")
    public void setInvoice_type(String  invoice_type){
        this.invoice_type = invoice_type ;
        this.invoice_typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_typeDirtyFlag(){
        return this.invoice_typeDirtyFlag ;
    }

    /**
     * 获取 [产品图片]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return this.product_image ;
    }

    /**
     * 设置 [产品图片]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

    /**
     * 获取 [产品图片]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return this.product_imageDirtyFlag ;
    }

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("uom_id_text")
    public String getUom_id_text(){
        return this.uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("uom_id_text")
    public void setUom_id_text(String  uom_id_text){
        this.uom_id_text = uom_id_text ;
        this.uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getUom_id_textDirtyFlag(){
        return this.uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return this.purchase_id ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return this.purchase_idDirtyFlag ;
    }

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }

    /**
     * 获取 [采购订单行]
     */
    @JsonProperty("purchase_line_id_text")
    public String getPurchase_line_id_text(){
        return this.purchase_line_id_text ;
    }

    /**
     * 设置 [采购订单行]
     */
    @JsonProperty("purchase_line_id_text")
    public void setPurchase_line_id_text(String  purchase_line_id_text){
        this.purchase_line_id_text = purchase_line_id_text ;
        this.purchase_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_id_textDirtyFlag(){
        return this.purchase_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [发票参考]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return this.invoice_id ;
    }

    /**
     * 设置 [发票参考]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [发票参考]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return this.invoice_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }

    /**
     * 获取 [采购订单行]
     */
    @JsonProperty("purchase_line_id")
    public Integer getPurchase_line_id(){
        return this.purchase_line_id ;
    }

    /**
     * 设置 [采购订单行]
     */
    @JsonProperty("purchase_line_id")
    public void setPurchase_line_id(Integer  purchase_line_id){
        this.purchase_line_id = purchase_line_id ;
        this.purchase_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idDirtyFlag(){
        return this.purchase_line_idDirtyFlag ;
    }

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return this.account_analytic_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

    /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return this.account_analytic_idDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("uom_id")
    public Integer getUom_id(){
        return this.uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("uom_id")
    public void setUom_id(Integer  uom_id){
        this.uom_id = uom_id ;
        this.uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getUom_idDirtyFlag(){
        return this.uom_idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }



}

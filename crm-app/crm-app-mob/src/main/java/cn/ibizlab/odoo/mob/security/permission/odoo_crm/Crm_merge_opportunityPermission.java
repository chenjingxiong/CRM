package cn.ibizlab.odoo.mob.security.permission.odoo_crm;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_merge_opportunityService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("crm_merge_opportunity_pms")
public class Crm_merge_opportunityPermission {

    @Autowired
    Crm_merge_opportunityService crm_merge_opportunityservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer crm_merge_opportunity_id, String action){
        return true ;
    }


}

package cn.ibizlab.odoo.mob.r7rt_dyna.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[动态数据看板]
 */
public class DynaDashboard implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 用户标识
     */
    private String userId;

    @JsonIgnore
    private boolean userIdDirtyFlag;
    
    /**
     * 实体标识
     */
    private String dynaDashboardId;

    @JsonIgnore
    private boolean dynaDashboardIdDirtyFlag;
    
    /**
     * 实体名称
     */
    private String dynaDashboardName;

    @JsonIgnore
    private boolean dynaDashboardNameDirtyFlag;
    
    /**
     * 建立人
     */
    private String createMan;

    @JsonIgnore
    private boolean createManDirtyFlag;
    
    /**
     * 更新人
     */
    private String updateMan;

    @JsonIgnore
    private boolean updateManDirtyFlag;
    
    /**
     * 模型标识
     */
    private String modelId;

    @JsonIgnore
    private boolean modelIdDirtyFlag;
    
    /**
     * 更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updateDate;

    @JsonIgnore
    private boolean updateDateDirtyFlag;
    
    /**
     * 应用标识
     */
    private String appId;

    @JsonIgnore
    private boolean appIdDirtyFlag;
    
    /**
     * 建立时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createDate;

    @JsonIgnore
    private boolean createDateDirtyFlag;
    
    /**
     * 模型
     */
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;
    

    /**
     * 获取 [用户标识]
     */
    @JsonProperty("userid")
    public String getUserId(){
        return this.userId ;
    }

    /**
     * 设置 [用户标识]
     */
    @JsonProperty("userid")
    public void setUserId(String  userId){
        this.userId = userId ;
        this.userIdDirtyFlag = true ;
    }

    /**
     * 获取 [用户标识]脏标记
     */
    @JsonIgnore
    public boolean getUserIdDirtyFlag(){
        return this.userIdDirtyFlag ;
    }

    /**
     * 获取 [实体标识]
     */
    @JsonProperty("dynadashboardid")
    public String getDynaDashboardId(){
        return this.dynaDashboardId ;
    }

    /**
     * 设置 [实体标识]
     */
    @JsonProperty("dynadashboardid")
    public void setDynaDashboardId(String  dynaDashboardId){
        this.dynaDashboardId = dynaDashboardId ;
        this.dynaDashboardIdDirtyFlag = true ;
    }

    /**
     * 获取 [实体标识]脏标记
     */
    @JsonIgnore
    public boolean getDynaDashboardIdDirtyFlag(){
        return this.dynaDashboardIdDirtyFlag ;
    }

    /**
     * 获取 [实体名称]
     */
    @JsonProperty("dynadashboardname")
    public String getDynaDashboardName(){
        return this.dynaDashboardName ;
    }

    /**
     * 设置 [实体名称]
     */
    @JsonProperty("dynadashboardname")
    public void setDynaDashboardName(String  dynaDashboardName){
        this.dynaDashboardName = dynaDashboardName ;
        this.dynaDashboardNameDirtyFlag = true ;
    }

    /**
     * 获取 [实体名称]脏标记
     */
    @JsonIgnore
    public boolean getDynaDashboardNameDirtyFlag(){
        return this.dynaDashboardNameDirtyFlag ;
    }

    /**
     * 获取 [建立人]
     */
    @JsonProperty("createman")
    public String getCreateMan(){
        return this.createMan ;
    }

    /**
     * 设置 [建立人]
     */
    @JsonProperty("createman")
    public void setCreateMan(String  createMan){
        this.createMan = createMan ;
        this.createManDirtyFlag = true ;
    }

    /**
     * 获取 [建立人]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return this.createManDirtyFlag ;
    }

    /**
     * 获取 [更新人]
     */
    @JsonProperty("updateman")
    public String getUpdateMan(){
        return this.updateMan ;
    }

    /**
     * 设置 [更新人]
     */
    @JsonProperty("updateman")
    public void setUpdateMan(String  updateMan){
        this.updateMan = updateMan ;
        this.updateManDirtyFlag = true ;
    }

    /**
     * 获取 [更新人]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return this.updateManDirtyFlag ;
    }

    /**
     * 获取 [模型标识]
     */
    @JsonProperty("modelid")
    public String getModelId(){
        return this.modelId ;
    }

    /**
     * 设置 [模型标识]
     */
    @JsonProperty("modelid")
    public void setModelId(String  modelId){
        this.modelId = modelId ;
        this.modelIdDirtyFlag = true ;
    }

    /**
     * 获取 [模型标识]脏标记
     */
    @JsonIgnore
    public boolean getModelIdDirtyFlag(){
        return this.modelIdDirtyFlag ;
    }

    /**
     * 获取 [更新时间]
     */
    @JsonProperty("updatedate")
    public Timestamp getUpdateDate(){
        return this.updateDate ;
    }

    /**
     * 设置 [更新时间]
     */
    @JsonProperty("updatedate")
    public void setUpdateDate(Timestamp  updateDate){
        this.updateDate = updateDate ;
        this.updateDateDirtyFlag = true ;
    }

    /**
     * 获取 [更新时间]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return this.updateDateDirtyFlag ;
    }

    /**
     * 获取 [应用标识]
     */
    @JsonProperty("appid")
    public String getAppId(){
        return this.appId ;
    }

    /**
     * 设置 [应用标识]
     */
    @JsonProperty("appid")
    public void setAppId(String  appId){
        this.appId = appId ;
        this.appIdDirtyFlag = true ;
    }

    /**
     * 获取 [应用标识]脏标记
     */
    @JsonIgnore
    public boolean getAppIdDirtyFlag(){
        return this.appIdDirtyFlag ;
    }

    /**
     * 获取 [建立时间]
     */
    @JsonProperty("createdate")
    public Timestamp getCreateDate(){
        return this.createDate ;
    }

    /**
     * 设置 [建立时间]
     */
    @JsonProperty("createdate")
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.createDateDirtyFlag = true ;
    }

    /**
     * 获取 [建立时间]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return this.createDateDirtyFlag ;
    }

    /**
     * 获取 [模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }



}

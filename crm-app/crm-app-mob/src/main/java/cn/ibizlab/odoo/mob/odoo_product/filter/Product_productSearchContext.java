package cn.ibizlab.odoo.mob.odoo_product.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Product_productSearchContext extends SearchContext implements Serializable {

	public String n_activity_state_eq;//[活动状态]
	public String n_name_eq;//[名称]
	public String n_name_like;//[名称]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_product_tmpl_id_eq;//[产品模板]

}
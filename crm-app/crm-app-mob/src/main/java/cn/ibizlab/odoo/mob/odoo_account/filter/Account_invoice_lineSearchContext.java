package cn.ibizlab.odoo.mob.odoo_account.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Account_invoice_lineSearchContext extends SearchContext implements Serializable {

	public String n_display_type_eq;//[显示类型]
	public String n_name_like;//[说明]
	public String n_currency_id_text_eq;//[币种]
	public String n_currency_id_text_like;//[币种]
	public String n_account_analytic_id_text_eq;//[分析账户]
	public String n_account_analytic_id_text_like;//[分析账户]
	public String n_product_id_text_eq;//[产品]
	public String n_product_id_text_like;//[产品]
	public String n_invoice_id_text_eq;//[发票参考]
	public String n_invoice_id_text_like;//[发票参考]
	public String n_partner_id_text_eq;//[业务伙伴]
	public String n_partner_id_text_like;//[业务伙伴]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_uom_id_text_eq;//[计量单位]
	public String n_uom_id_text_like;//[计量单位]
	public String n_account_id_text_eq;//[科目]
	public String n_account_id_text_like;//[科目]
	public String n_purchase_line_id_text_eq;//[采购订单行]
	public String n_purchase_line_id_text_like;//[采购订单行]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public Integer n_invoice_id_eq;//[发票参考]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_product_id_eq;//[产品]
	public Integer n_account_id_eq;//[科目]
	public Integer n_purchase_line_id_eq;//[采购订单行]
	public Integer n_account_analytic_id_eq;//[分析账户]
	public Integer n_partner_id_eq;//[业务伙伴]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_currency_id_eq;//[币种]
	public Integer n_uom_id_eq;//[计量单位]
	public Integer n_company_id_eq;//[公司]

}
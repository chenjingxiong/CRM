package cn.ibizlab.odoo.mob.odoo_mail.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[活动类型]
 */
public class Mail_activity_type implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 模型已更改
     */
    private String res_model_change;

    @JsonIgnore
    private boolean res_model_changeDirtyFlag;
    
    /**
     * 摘要
     */
    private String summary;

    @JsonIgnore
    private boolean summaryDirtyFlag;
    
    /**
     * 自动安排下一个活动
     */
    private String force_next;

    @JsonIgnore
    private boolean force_nextDirtyFlag;
    
    /**
     * 序号
     */
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 类别
     */
    private String category;

    @JsonIgnore
    private boolean categoryDirtyFlag;
    
    /**
     * 图标
     */
    private String icon;

    @JsonIgnore
    private boolean iconDirtyFlag;
    
    /**
     * 之后
     */
    private Integer delay_count;

    @JsonIgnore
    private boolean delay_countDirtyFlag;
    
    /**
     * 预先活动
     */
    private String previous_type_ids;

    @JsonIgnore
    private boolean previous_type_idsDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 延迟类型
     */
    private String delay_from;

    @JsonIgnore
    private boolean delay_fromDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 邮件模板
     */
    private String mail_template_ids;

    @JsonIgnore
    private boolean mail_template_idsDirtyFlag;
    
    /**
     * 名称
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 模型
     */
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 排版类型
     */
    private String decoration_type;

    @JsonIgnore
    private boolean decoration_typeDirtyFlag;
    
    /**
     * 推荐的下一活动
     */
    private String next_type_ids;

    @JsonIgnore
    private boolean next_type_idsDirtyFlag;
    
    /**
     * 延迟单位
     */
    private String delay_unit;

    @JsonIgnore
    private boolean delay_unitDirtyFlag;
    
    /**
     * 初始模型
     */
    private Integer initial_res_model_id;

    @JsonIgnore
    private boolean initial_res_model_idDirtyFlag;
    
    /**
     * 设置默认下一个活动
     */
    private String default_next_type_id_text;

    @JsonIgnore
    private boolean default_next_type_id_textDirtyFlag;
    
    /**
     * 最后更新者
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 设置默认下一个活动
     */
    private Integer default_next_type_id;

    @JsonIgnore
    private boolean default_next_type_idDirtyFlag;
    

    /**
     * 获取 [模型已更改]
     */
    @JsonProperty("res_model_change")
    public String getRes_model_change(){
        return this.res_model_change ;
    }

    /**
     * 设置 [模型已更改]
     */
    @JsonProperty("res_model_change")
    public void setRes_model_change(String  res_model_change){
        this.res_model_change = res_model_change ;
        this.res_model_changeDirtyFlag = true ;
    }

    /**
     * 获取 [模型已更改]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_changeDirtyFlag(){
        return this.res_model_changeDirtyFlag ;
    }

    /**
     * 获取 [摘要]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return this.summary ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

    /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return this.summaryDirtyFlag ;
    }

    /**
     * 获取 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return this.force_next ;
    }

    /**
     * 设置 [自动安排下一个活动]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return this.force_nextDirtyFlag ;
    }

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [类别]
     */
    @JsonProperty("category")
    public String getCategory(){
        return this.category ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category")
    public void setCategory(String  category){
        this.category = category ;
        this.categoryDirtyFlag = true ;
    }

    /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategoryDirtyFlag(){
        return this.categoryDirtyFlag ;
    }

    /**
     * 获取 [图标]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return this.icon ;
    }

    /**
     * 设置 [图标]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

    /**
     * 获取 [图标]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return this.iconDirtyFlag ;
    }

    /**
     * 获取 [之后]
     */
    @JsonProperty("delay_count")
    public Integer getDelay_count(){
        return this.delay_count ;
    }

    /**
     * 设置 [之后]
     */
    @JsonProperty("delay_count")
    public void setDelay_count(Integer  delay_count){
        this.delay_count = delay_count ;
        this.delay_countDirtyFlag = true ;
    }

    /**
     * 获取 [之后]脏标记
     */
    @JsonIgnore
    public boolean getDelay_countDirtyFlag(){
        return this.delay_countDirtyFlag ;
    }

    /**
     * 获取 [预先活动]
     */
    @JsonProperty("previous_type_ids")
    public String getPrevious_type_ids(){
        return this.previous_type_ids ;
    }

    /**
     * 设置 [预先活动]
     */
    @JsonProperty("previous_type_ids")
    public void setPrevious_type_ids(String  previous_type_ids){
        this.previous_type_ids = previous_type_ids ;
        this.previous_type_idsDirtyFlag = true ;
    }

    /**
     * 获取 [预先活动]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_type_idsDirtyFlag(){
        return this.previous_type_idsDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [延迟类型]
     */
    @JsonProperty("delay_from")
    public String getDelay_from(){
        return this.delay_from ;
    }

    /**
     * 设置 [延迟类型]
     */
    @JsonProperty("delay_from")
    public void setDelay_from(String  delay_from){
        this.delay_from = delay_from ;
        this.delay_fromDirtyFlag = true ;
    }

    /**
     * 获取 [延迟类型]脏标记
     */
    @JsonIgnore
    public boolean getDelay_fromDirtyFlag(){
        return this.delay_fromDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return this.mail_template_ids ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

    /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return this.mail_template_idsDirtyFlag ;
    }

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [排版类型]
     */
    @JsonProperty("decoration_type")
    public String getDecoration_type(){
        return this.decoration_type ;
    }

    /**
     * 设置 [排版类型]
     */
    @JsonProperty("decoration_type")
    public void setDecoration_type(String  decoration_type){
        this.decoration_type = decoration_type ;
        this.decoration_typeDirtyFlag = true ;
    }

    /**
     * 获取 [排版类型]脏标记
     */
    @JsonIgnore
    public boolean getDecoration_typeDirtyFlag(){
        return this.decoration_typeDirtyFlag ;
    }

    /**
     * 获取 [推荐的下一活动]
     */
    @JsonProperty("next_type_ids")
    public String getNext_type_ids(){
        return this.next_type_ids ;
    }

    /**
     * 设置 [推荐的下一活动]
     */
    @JsonProperty("next_type_ids")
    public void setNext_type_ids(String  next_type_ids){
        this.next_type_ids = next_type_ids ;
        this.next_type_idsDirtyFlag = true ;
    }

    /**
     * 获取 [推荐的下一活动]脏标记
     */
    @JsonIgnore
    public boolean getNext_type_idsDirtyFlag(){
        return this.next_type_idsDirtyFlag ;
    }

    /**
     * 获取 [延迟单位]
     */
    @JsonProperty("delay_unit")
    public String getDelay_unit(){
        return this.delay_unit ;
    }

    /**
     * 设置 [延迟单位]
     */
    @JsonProperty("delay_unit")
    public void setDelay_unit(String  delay_unit){
        this.delay_unit = delay_unit ;
        this.delay_unitDirtyFlag = true ;
    }

    /**
     * 获取 [延迟单位]脏标记
     */
    @JsonIgnore
    public boolean getDelay_unitDirtyFlag(){
        return this.delay_unitDirtyFlag ;
    }

    /**
     * 获取 [初始模型]
     */
    @JsonProperty("initial_res_model_id")
    public Integer getInitial_res_model_id(){
        return this.initial_res_model_id ;
    }

    /**
     * 设置 [初始模型]
     */
    @JsonProperty("initial_res_model_id")
    public void setInitial_res_model_id(Integer  initial_res_model_id){
        this.initial_res_model_id = initial_res_model_id ;
        this.initial_res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [初始模型]脏标记
     */
    @JsonIgnore
    public boolean getInitial_res_model_idDirtyFlag(){
        return this.initial_res_model_idDirtyFlag ;
    }

    /**
     * 获取 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id_text")
    public String getDefault_next_type_id_text(){
        return this.default_next_type_id_text ;
    }

    /**
     * 设置 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id_text")
    public void setDefault_next_type_id_text(String  default_next_type_id_text){
        this.default_next_type_id_text = default_next_type_id_text ;
        this.default_next_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_id_textDirtyFlag(){
        return this.default_next_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id")
    public Integer getDefault_next_type_id(){
        return this.default_next_type_id ;
    }

    /**
     * 设置 [设置默认下一个活动]
     */
    @JsonProperty("default_next_type_id")
    public void setDefault_next_type_id(Integer  default_next_type_id){
        this.default_next_type_id = default_next_type_id ;
        this.default_next_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [设置默认下一个活动]脏标记
     */
    @JsonIgnore
    public boolean getDefault_next_type_idDirtyFlag(){
        return this.default_next_type_idDirtyFlag ;
    }



}

package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_stageFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_stages")
    public Crm_stage create(@RequestBody Crm_stage crm_stage) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_stages/{crm_stage_id}/updatebatch")
    public Crm_stage updateBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_stages/{crm_stage_id}/getdraft")
    public Crm_stage getDraft(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_stages/{crm_stage_id}")
    public Boolean remove(@PathVariable("crm_stage_id") Integer crm_stage_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_stages/{crm_stage_id}")
    public Crm_stage update(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_stages/{crm_stage_id}")
    public Crm_stage get(@PathVariable("crm_stage_id") Integer crm_stage_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_stages/{crm_stage_id}/createbatch")
    public Crm_stage createBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_stages/{crm_stage_id}/removebatch")
    public Crm_stage removeBatch(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stage crm_stage) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_stages/fetchdefault")
	public Page<Crm_stage> fetchDefault(Crm_stageSearchContext searchContext) ;
}

package cn.ibizlab.odoo.mob.odoo_mail.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.mob.odoo_mail.filter.*;
import cn.ibizlab.odoo.mob.odoo_mail.feign.Mail_activity_typeFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Mail_activity_typeService {

    Mail_activity_typeFeignClient client;

    @Autowired
    public Mail_activity_typeService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_activity_typeFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Mail_activity_typeFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Mail_activity_type removeBatch(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.removeBatch(mail_activity_type_id, mail_activity_type);
    }

    public Mail_activity_type createBatch(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.createBatch(mail_activity_type_id, mail_activity_type);
    }

    public boolean remove( Integer mail_activity_type_id) {
        return client.remove( mail_activity_type_id);
    }

    public Mail_activity_type get( Integer mail_activity_type_id) {
        return client.get( mail_activity_type_id);
    }

    public Mail_activity_type getDraft(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.getDraft(mail_activity_type_id, mail_activity_type);
    }

    public boolean checkKey(Mail_activity_type mail_activity_type) {
        return client.checkKey(mail_activity_type);
    }

	public Mail_activity_type create(Mail_activity_type mail_activity_type) {
        return client.create(mail_activity_type);
    }

    public Mail_activity_type updateBatch(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.updateBatch(mail_activity_type_id, mail_activity_type);
    }

    public Mail_activity_type update(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.update(mail_activity_type_id, mail_activity_type);
    }

    public Mail_activity_type save(Integer mail_activity_type_id, Mail_activity_type mail_activity_type) {
        return client.save(mail_activity_type_id, mail_activity_type);
    }

	public Page<Mail_activity_type> fetchDefault(Mail_activity_typeSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

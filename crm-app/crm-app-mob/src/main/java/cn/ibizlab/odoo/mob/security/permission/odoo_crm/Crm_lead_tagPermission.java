package cn.ibizlab.odoo.mob.security.permission.odoo_crm;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_lead_tagService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("crm_lead_tag_pms")
public class Crm_lead_tagPermission {

    @Autowired
    Crm_lead_tagService crm_lead_tagservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer crm_lead_tag_id, String action){
        return true ;
    }


}

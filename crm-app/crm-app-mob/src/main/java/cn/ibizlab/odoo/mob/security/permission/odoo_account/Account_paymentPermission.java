package cn.ibizlab.odoo.mob.security.permission.odoo_account;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_account.service.Account_paymentService;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("account_payment_pms")
public class Account_paymentPermission {

    @Autowired
    Account_paymentService account_paymentservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer account_payment_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  Integer account_payment_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  String action){
        return true ;
    }


}

package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_merge_opportunityService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_merge_opportunityController {
	@Autowired
    Crm_merge_opportunityService crm_merge_opportunityservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}/updatebatch")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'UPDATE')")
    public ResponseEntity<Crm_merge_opportunity> updateBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.updateBatch(crm_merge_opportunity_id, crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/getdraft")
    @PreAuthorize("@crm_merge_opportunity_pms.check('CREATE')")
    public ResponseEntity<Crm_merge_opportunity> getDraft() {
        //Crm_merge_opportunity crm_merge_opportunity = crm_merge_opportunityservice.getDraft( crm_merge_opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_merge_opportunity());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'UPDATE')")
    public ResponseEntity<Crm_merge_opportunity> update(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.update(crm_merge_opportunity_id, crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_merge_opportunities/{crm_merge_opportunity_id}")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'UPDATE')")
    public ResponseEntity<Crm_merge_opportunity> api_update(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.update(crm_merge_opportunity_id, crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities")
    @PreAuthorize("@crm_merge_opportunity_pms.check('CREATE')")
    public ResponseEntity<Crm_merge_opportunity> create(@RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.create(crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}/removebatch")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'DELETE')")
    public ResponseEntity<Crm_merge_opportunity> removeBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.removeBatch(crm_merge_opportunity_id, crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
        boolean b = crm_merge_opportunityservice.remove( crm_merge_opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}/createbatch")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'CREATE')")
    public ResponseEntity<Crm_merge_opportunity> createBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) {
        Crm_merge_opportunity crm_merge_opportunity2 = crm_merge_opportunityservice.createBatch(crm_merge_opportunity_id, crm_merge_opportunity);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    @PreAuthorize("@crm_merge_opportunity_pms.check(#crm_merge_opportunity_id,'READ')")
    public ResponseEntity<Crm_merge_opportunity> get(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
        Crm_merge_opportunity crm_merge_opportunity = crm_merge_opportunityservice.get( crm_merge_opportunity_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunity);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_merge_opportunities/fetchdefault")
    @PreAuthorize("@crm_merge_opportunity_pms.check('READ')")
	public ResponseEntity<List<Crm_merge_opportunity>> fetchDefault(Crm_merge_opportunitySearchContext searchContext,Pageable pageable) {
        
        Page<Crm_merge_opportunity> page = crm_merge_opportunityservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

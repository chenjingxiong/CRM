package cn.ibizlab.odoo.mob.odoo_calendar.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Calendar_eventSearchContext extends SearchContext implements Serializable {

	public String n_month_by_eq;//[选项]
	public String n_week_list_eq;//[工作日]
	public String n_end_type_eq;//[重复终止]
	public String n_attendee_status_eq;//[出席者状态]
	public String n_rrule_type_eq;//[重新提起]
	public String n_privacy_eq;//[隐私]
	public String n_state_eq;//[状态]
	public String n_show_as_eq;//[显示时间为]
	public String n_byday_eq;//[按 天]
	public String n_name_like;//[会议主题]
	public String n_opportunity_id_text_eq;//[商机]
	public String n_opportunity_id_text_like;//[商机]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_user_id_text_eq;//[所有者]
	public String n_user_id_text_like;//[所有者]
	public String n_applicant_id_text_eq;//[申请人]
	public String n_applicant_id_text_like;//[申请人]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_applicant_id_eq;//[申请人]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_user_id_eq;//[所有者]
	public Integer n_opportunity_id_eq;//[商机]

}
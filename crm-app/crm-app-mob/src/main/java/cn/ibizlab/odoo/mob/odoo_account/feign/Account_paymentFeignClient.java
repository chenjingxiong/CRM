package cn.ibizlab.odoo.mob.odoo_account.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;


public interface Account_paymentFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/checkkey")
    public Boolean checkKey(@RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payments/{account_payment_id}")
    public Account_payment update(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payments/{account_payment_id}/getdraft")
    public Account_payment getDraft(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payments/{account_payment_id}")
    public Boolean remove(@PathVariable("account_payment_id") Integer account_payment_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payments/{account_payment_id}/removebatch")
    public Account_payment removeBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/{account_payment_id}/save")
    public Account_payment save(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payments/{account_payment_id}/updatebatch")
    public Account_payment updateBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments")
    public Account_payment create(@RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payments/{account_payment_id}")
    public Account_payment get(@PathVariable("account_payment_id") Integer account_payment_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payments/{account_payment_id}/createbatch")
    public Account_payment createBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/account_payments/fetchdefault")
	public Page<Account_payment> fetchDefault(Account_paymentSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/checkkey")
    public Boolean checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    public Account_payment updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}/getdraft")
    public Account_payment getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    public Boolean removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}/removebatch")
    public Account_payment removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}/save")
    public Account_payment saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}/updatebatch")
    public Account_payment updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments")
    public Account_payment createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_payment account_payment) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    public Account_payment getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_payments/{account_payment_id}/createbatch")
    public Account_payment createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/account_payments/fetchdefault")
	public Page<Account_payment> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Account_paymentSearchContext searchContext) ;
}

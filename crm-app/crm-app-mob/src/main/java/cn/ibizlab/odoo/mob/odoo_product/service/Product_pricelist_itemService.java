package cn.ibizlab.odoo.mob.odoo_product.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.mob.odoo_product.feign.Product_pricelist_itemFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Product_pricelist_itemService {

    Product_pricelist_itemFeignClient client;

    @Autowired
    public Product_pricelist_itemService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_pricelist_itemFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_pricelist_itemFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Product_pricelist_item get( Integer product_pricelist_item_id) {
        return client.get( product_pricelist_item_id);
    }

	public Product_pricelist_item create(Product_pricelist_item product_pricelist_item) {
        return client.create(product_pricelist_item);
    }

    public boolean remove( Integer product_pricelist_item_id) {
        return client.remove( product_pricelist_item_id);
    }

    public Product_pricelist_item getDraft(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.getDraft(product_pricelist_item_id, product_pricelist_item);
    }

    public Product_pricelist_item createBatch(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.createBatch(product_pricelist_item_id, product_pricelist_item);
    }

    public Product_pricelist_item updateBatch(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.updateBatch(product_pricelist_item_id, product_pricelist_item);
    }

    public Product_pricelist_item removeBatch(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.removeBatch(product_pricelist_item_id, product_pricelist_item);
    }

    public Product_pricelist_item update(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.update(product_pricelist_item_id, product_pricelist_item);
    }

    public boolean checkKey(Product_pricelist_item product_pricelist_item) {
        return client.checkKey(product_pricelist_item);
    }

    public Product_pricelist_item save(Integer product_pricelist_item_id, Product_pricelist_item product_pricelist_item) {
        return client.save(product_pricelist_item_id, product_pricelist_item);
    }

	public Page<Product_pricelist_item> fetchDefault(Product_pricelist_itemSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

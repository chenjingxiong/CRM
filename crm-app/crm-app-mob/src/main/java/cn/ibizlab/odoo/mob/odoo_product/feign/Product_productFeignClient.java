package cn.ibizlab.odoo.mob.odoo_product.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;


public interface Product_productFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_products/{product_product_id}/updatebatch")
    public Product_product updateBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_products/{product_product_id}")
    public Product_product get(@PathVariable("product_product_id") Integer product_product_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/{product_product_id}/save")
    public Product_product save(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products")
    public Product_product create(@RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/checkkey")
    public Boolean checkKey(@RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_products/{product_product_id}")
    public Product_product update(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_products/{product_product_id}/removebatch")
    public Product_product removeBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_products/{product_product_id}/getdraft")
    public Product_product getDraft(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_products/{product_product_id}")
    public Boolean remove(@PathVariable("product_product_id") Integer product_product_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_products/{product_product_id}/createbatch")
    public Product_product createBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/product_products/fetchdefault")
	public Page<Product_product> fetchDefault(Product_productSearchContext searchContext) ;
}

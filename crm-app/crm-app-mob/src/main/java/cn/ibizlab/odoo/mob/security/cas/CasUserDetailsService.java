package cn.ibizlab.odoo.mob.security.cas;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.cas.authentication.CasAssertionAuthenticationToken;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.security.userdetail.LoginUser;
import cn.ibizlab.odoo.mob.security.userdetail.LoginUserDetailService;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 用于加载用户信息 实现UserDetailsService接口，或者实现AuthenticationUserDetailsService接口
 */
@Slf4j
@IBIZLog
public class CasUserDetailsService implements AuthenticationUserDetailsService<CasAssertionAuthenticationToken> {

	@Autowired
	UserDetailsService userDetailsService ;

    @Override
    public UserDetails loadUserDetails(CasAssertionAuthenticationToken token) throws UsernameNotFoundException {
		//获取用户信息
		UserDetails userDetails = userDetailsService.loadUserByUsername(token.getName()) ;
		return userDetails;
    }

}

package cn.ibizlab.odoo.mob.odoo_account.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_account.service.Account_journalService;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Account_journalController {
	@Autowired
    Account_journalService account_journalservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/{account_journal_id}/createbatch")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'CREATE')")
    public ResponseEntity<Account_journal> createBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.createBatch(account_journal_id, account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/{account_journal_id}")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'READ')")
    public ResponseEntity<Account_journal> get(@PathVariable("account_journal_id") Integer account_journal_id) {
        Account_journal account_journal = account_journalservice.get( account_journal_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/getdraft")
    @PreAuthorize("@account_journal_pms.check('CREATE')")
    public ResponseEntity<Account_journal> getDraft() {
        //Account_journal account_journal = account_journalservice.getDraft( account_journal_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_journal());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals")
    @PreAuthorize("@account_journal_pms.check('CREATE')")
    public ResponseEntity<Account_journal> create(@RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.create(account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/{account_journal_id}/removebatch")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'DELETE')")
    public ResponseEntity<Account_journal> removeBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.removeBatch(account_journal_id, account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/{account_journal_id}")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'UPDATE')")
    public ResponseEntity<Account_journal> update(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.update(account_journal_id, account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/account_journals/{account_journal_id}")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'UPDATE')")
    public ResponseEntity<Account_journal> api_update(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.update(account_journal_id, account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/{account_journal_id}")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("account_journal_id") Integer account_journal_id) {
        boolean b = account_journalservice.remove( account_journal_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/{account_journal_id}/updatebatch")
    @PreAuthorize("@account_journal_pms.check(#account_journal_id,'UPDATE')")
    public ResponseEntity<Account_journal> updateBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) {
        Account_journal account_journal2 = account_journalservice.updateBatch(account_journal_id, account_journal);
        return ResponseEntity.status(HttpStatus.OK).body(account_journal2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/account_journals/fetchdefault")
    @PreAuthorize("@account_journal_pms.check('READ')")
	public ResponseEntity<List<Account_journal>> fetchDefault(Account_journalSearchContext searchContext,Pageable pageable) {
        
        Page<Account_journal> page = account_journalservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

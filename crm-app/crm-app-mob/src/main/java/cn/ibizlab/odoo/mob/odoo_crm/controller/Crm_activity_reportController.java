package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_activity_reportService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_activity_reportController {
	@Autowired
    Crm_activity_reportService crm_activity_reportservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/{crm_activity_report_id}/updatebatch")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'UPDATE')")
    public ResponseEntity<Crm_activity_report> updateBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.updateBatch(crm_activity_report_id, crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports")
    @PreAuthorize("@crm_activity_report_pms.check('CREATE')")
    public ResponseEntity<Crm_activity_report> create(@RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.create(crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/{crm_activity_report_id}")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'UPDATE')")
    public ResponseEntity<Crm_activity_report> update(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.update(crm_activity_report_id, crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_activity_reports/{crm_activity_report_id}")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'UPDATE')")
    public ResponseEntity<Crm_activity_report> api_update(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.update(crm_activity_report_id, crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/{crm_activity_report_id}/removebatch")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'DELETE')")
    public ResponseEntity<Crm_activity_report> removeBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.removeBatch(crm_activity_report_id, crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/{crm_activity_report_id}")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
        boolean b = crm_activity_reportservice.remove( crm_activity_report_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/{crm_activity_report_id}")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'READ')")
    public ResponseEntity<Crm_activity_report> get(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
        Crm_activity_report crm_activity_report = crm_activity_reportservice.get( crm_activity_report_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports/{crm_activity_report_id}/createbatch")
    @PreAuthorize("@crm_activity_report_pms.check(#crm_activity_report_id,'CREATE')")
    public ResponseEntity<Crm_activity_report> createBatch(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_report crm_activity_report) {
        Crm_activity_report crm_activity_report2 = crm_activity_reportservice.createBatch(crm_activity_report_id, crm_activity_report);
        return ResponseEntity.status(HttpStatus.OK).body(crm_activity_report2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/getdraft")
    @PreAuthorize("@crm_activity_report_pms.check('CREATE')")
    public ResponseEntity<Crm_activity_report> getDraft() {
        //Crm_activity_report crm_activity_report = crm_activity_reportservice.getDraft( crm_activity_report_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_activity_report());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_activity_reports/fetchdefault")
    @PreAuthorize("@crm_activity_report_pms.check('READ')")
	public ResponseEntity<List<Crm_activity_report>> fetchDefault(Crm_activity_reportSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_activity_report> page = crm_activity_reportservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

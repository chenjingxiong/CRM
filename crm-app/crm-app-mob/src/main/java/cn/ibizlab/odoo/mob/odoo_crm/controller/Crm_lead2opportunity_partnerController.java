package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_lead2opportunity_partnerService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_lead2opportunity_partnerController {
	@Autowired
    Crm_lead2opportunity_partnerService crm_lead2opportunity_partnerservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check('CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> create(@RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.create(crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
        boolean b = crm_lead2opportunity_partnerservice.remove( crm_lead2opportunity_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'READ')")
    public ResponseEntity<Crm_lead2opportunity_partner> get(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner = crm_lead2opportunity_partnerservice.get( crm_lead2opportunity_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> update(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.update(crm_lead2opportunity_partner_id, crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> api_update(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.update(crm_lead2opportunity_partner_id, crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/removebatch")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'DELETE')")
    public ResponseEntity<Crm_lead2opportunity_partner> removeBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.removeBatch(crm_lead2opportunity_partner_id, crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/createbatch")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> createBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.createBatch(crm_lead2opportunity_partner_id, crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}/updatebatch")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check(#crm_lead2opportunity_partner_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> updateBatch(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partner crm_lead2opportunity_partner) {
        Crm_lead2opportunity_partner crm_lead2opportunity_partner2 = crm_lead2opportunity_partnerservice.updateBatch(crm_lead2opportunity_partner_id, crm_lead2opportunity_partner);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/getdraft")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check('CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner> getDraft() {
        //Crm_lead2opportunity_partner crm_lead2opportunity_partner = crm_lead2opportunity_partnerservice.getDraft( crm_lead2opportunity_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lead2opportunity_partner());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partners/fetchdefault")
    @PreAuthorize("@crm_lead2opportunity_partner_pms.check('READ')")
	public ResponseEntity<List<Crm_lead2opportunity_partner>> fetchDefault(Crm_lead2opportunity_partnerSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lead2opportunity_partner> page = crm_lead2opportunity_partnerservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

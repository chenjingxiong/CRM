package cn.ibizlab.odoo.mob.security.permission.odoo_base;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_base.service.Res_companyService;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("res_company_pms")
public class Res_companyPermission {

    @Autowired
    Res_companyService res_companyservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer res_company_id, String action){
        return true ;
    }


}

package cn.ibizlab.odoo.mob.odoo_account.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;
import cn.ibizlab.odoo.mob.odoo_account.feign.Account_invoice_lineFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Account_invoice_lineService {

    Account_invoice_lineFeignClient client;

    @Autowired
    public Account_invoice_lineService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_invoice_lineFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_invoice_lineFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Account_invoice_line update(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.update(account_invoice_line_id, account_invoice_line);
    }

    public boolean checkKey(Account_invoice_line account_invoice_line) {
        return client.checkKey(account_invoice_line);
    }

    public Account_invoice_line updateBatch(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.updateBatch(account_invoice_line_id, account_invoice_line);
    }

    public Account_invoice_line createBatch(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.createBatch(account_invoice_line_id, account_invoice_line);
    }

    public Account_invoice_line removeBatch(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.removeBatch(account_invoice_line_id, account_invoice_line);
    }

    public Account_invoice_line get( Integer account_invoice_line_id) {
        return client.get( account_invoice_line_id);
    }

    public Account_invoice_line getDraft(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.getDraft(account_invoice_line_id, account_invoice_line);
    }

    public Account_invoice_line save(Integer account_invoice_line_id, Account_invoice_line account_invoice_line) {
        return client.save(account_invoice_line_id, account_invoice_line);
    }

    public boolean remove( Integer account_invoice_line_id) {
        return client.remove( account_invoice_line_id);
    }

	public Account_invoice_line create(Account_invoice_line account_invoice_line) {
        return client.create(account_invoice_line);
    }

	public Page<Account_invoice_line> fetchDefault(Account_invoice_lineSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

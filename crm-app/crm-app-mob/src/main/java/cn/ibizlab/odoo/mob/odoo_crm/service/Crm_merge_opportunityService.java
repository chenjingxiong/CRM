package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_merge_opportunityFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_merge_opportunityService {

    Crm_merge_opportunityFeignClient client;

    @Autowired
    public Crm_merge_opportunityService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_merge_opportunityFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_merge_opportunityFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_merge_opportunity updateBatch(Integer crm_merge_opportunity_id, Crm_merge_opportunity crm_merge_opportunity) {
        return client.updateBatch(crm_merge_opportunity_id, crm_merge_opportunity);
    }

    public Crm_merge_opportunity getDraft(Integer crm_merge_opportunity_id, Crm_merge_opportunity crm_merge_opportunity) {
        return client.getDraft(crm_merge_opportunity_id, crm_merge_opportunity);
    }

    public Crm_merge_opportunity update(Integer crm_merge_opportunity_id, Crm_merge_opportunity crm_merge_opportunity) {
        return client.update(crm_merge_opportunity_id, crm_merge_opportunity);
    }

	public Crm_merge_opportunity create(Crm_merge_opportunity crm_merge_opportunity) {
        return client.create(crm_merge_opportunity);
    }

    public Crm_merge_opportunity removeBatch(Integer crm_merge_opportunity_id, Crm_merge_opportunity crm_merge_opportunity) {
        return client.removeBatch(crm_merge_opportunity_id, crm_merge_opportunity);
    }

    public boolean remove( Integer crm_merge_opportunity_id) {
        return client.remove( crm_merge_opportunity_id);
    }

    public Crm_merge_opportunity createBatch(Integer crm_merge_opportunity_id, Crm_merge_opportunity crm_merge_opportunity) {
        return client.createBatch(crm_merge_opportunity_id, crm_merge_opportunity);
    }

    public Crm_merge_opportunity get( Integer crm_merge_opportunity_id) {
        return client.get( crm_merge_opportunity_id);
    }

	public Page<Crm_merge_opportunity> fetchDefault(Crm_merge_opportunitySearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

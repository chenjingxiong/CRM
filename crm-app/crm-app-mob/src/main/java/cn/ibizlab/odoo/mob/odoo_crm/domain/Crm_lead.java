package cn.ibizlab.odoo.mob.odoo_crm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[线索/商机]
 */
public class Crm_lead implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 责任用户
     */
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;
    
    /**
     * #会议
     */
    private Integer meeting_count;

    @JsonIgnore
    private boolean meeting_countDirtyFlag;
    
    /**
     * 工作岗位
     */
    private String ibizfunction;

    @JsonIgnore
    private boolean ibizfunctionDirtyFlag;
    
    /**
     * 退回
     */
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;
    
    /**
     * 城市
     */
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;
    
    /**
     * 关闭日期
     */
    private Double day_close;

    @JsonIgnore
    private boolean day_closeDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * 按比例分摊收入
     */
    private Double expected_revenue;

    @JsonIgnore
    private boolean expected_revenueDirtyFlag;
    
    /**
     * 关闭日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_closed;

    @JsonIgnore
    private boolean date_closedDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 全局抄送
     */
    private String email_cc;

    @JsonIgnore
    private boolean email_ccDirtyFlag;
    
    /**
     * 联系人姓名
     */
    private String contact_name;

    @JsonIgnore
    private boolean contact_nameDirtyFlag;
    
    /**
     * 最后阶段更新
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_last_stage_update;

    @JsonIgnore
    private boolean date_last_stage_updateDirtyFlag;
    
    /**
     * 预期收益
     */
    private Double planned_revenue;

    @JsonIgnore
    private boolean planned_revenueDirtyFlag;
    
    /**
     * 网站信息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * 下一活动类型
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 预期结束
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;
    
    /**
     * 邮政编码
     */
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;
    
    /**
     * 商机
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 便签
     */
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;
    
    /**
     * 手机
     */
    private String mobile;

    @JsonIgnore
    private boolean mobileDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 类型
     */
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;
    
    /**
     * 网站
     */
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * EMail
     */
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;
    
    /**
     * 转换日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_conversion;

    @JsonIgnore
    private boolean date_conversionDirtyFlag;
    
    /**
     * 客户名称
     */
    private String partner_name;

    @JsonIgnore
    private boolean partner_nameDirtyFlag;
    
    /**
     * 销售订单的总数
     */
    private Double sale_amount_total;

    @JsonIgnore
    private boolean sale_amount_totalDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * 需要激活
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 看板状态
     */
    private String kanban_state;

    @JsonIgnore
    private boolean kanban_stateDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 引荐于
     */
    private String referred;

    @JsonIgnore
    private boolean referredDirtyFlag;
    
    /**
     * 概率
     */
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;
    
    /**
     * 最近行动
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_action_last;

    @JsonIgnore
    private boolean date_action_lastDirtyFlag;
    
    /**
     * 错误数
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 分配日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_open;

    @JsonIgnore
    private boolean date_openDirtyFlag;
    
    /**
     * 电话
     */
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 分配天数
     */
    private Double day_open;

    @JsonIgnore
    private boolean day_openDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;
    
    /**
     * 报价单的数量
     */
    private Integer sale_number;

    @JsonIgnore
    private boolean sale_numberDirtyFlag;
    
    /**
     * 街道 2
     */
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;
    
    /**
     * 订单
     */
    private String order_ids;

    @JsonIgnore
    private boolean order_idsDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 街道
     */
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;
    
    /**
     * 颜色索引
     */
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 标签
     */
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;
    
    /**
     * 黑名单
     */
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;
    
    /**
     * 行动数量
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 优先级
     */
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;
    
    /**
     * 销售员
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 来源
     */
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;
    
    /**
     * 业务伙伴联系姓名
     */
    private String partner_address_name;

    @JsonIgnore
    private boolean partner_address_nameDirtyFlag;
    
    /**
     * 媒介
     */
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;
    
    /**
     * 币种
     */
    private Integer company_currency;

    @JsonIgnore
    private boolean company_currencyDirtyFlag;
    
    /**
     * 销售团队
     */
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;
    
    /**
     * Partner Contact Mobile
     */
    private String partner_address_mobile;

    @JsonIgnore
    private boolean partner_address_mobileDirtyFlag;
    
    /**
     * 最后更新
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 用户EMail
     */
    private String user_email;

    @JsonIgnore
    private boolean user_emailDirtyFlag;
    
    /**
     * 用户 登录
     */
    private String user_login;

    @JsonIgnore
    private boolean user_loginDirtyFlag;
    
    /**
     * 合作伙伴联系电话
     */
    private String partner_address_phone;

    @JsonIgnore
    private boolean partner_address_phoneDirtyFlag;
    
    /**
     * 省份
     */
    private String state_id_text;

    @JsonIgnore
    private boolean state_id_textDirtyFlag;
    
    /**
     * 营销
     */
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;
    
    /**
     * 合作伙伴黑名单
     */
    private String partner_is_blacklisted;

    @JsonIgnore
    private boolean partner_is_blacklistedDirtyFlag;
    
    /**
     * 业务伙伴联系EMail
     */
    private String partner_address_email;

    @JsonIgnore
    private boolean partner_address_emailDirtyFlag;
    
    /**
     * 阶段
     */
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 国家
     */
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;
    
    /**
     * 称谓
     */
    private String title_text;

    @JsonIgnore
    private boolean title_textDirtyFlag;
    
    /**
     * 失去原因
     */
    private String lost_reason_text;

    @JsonIgnore
    private boolean lost_reason_textDirtyFlag;
    
    /**
     * 失去原因
     */
    private Integer lost_reason;

    @JsonIgnore
    private boolean lost_reasonDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 最后更新
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 销售员
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 省份
     */
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;
    
    /**
     * 媒介
     */
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;
    
    /**
     * 阶段
     */
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;
    
    /**
     * 来源
     */
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;
    
    /**
     * 国家
     */
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;
    
    /**
     * 营销
     */
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;
    
    /**
     * 客户
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 销售团队
     */
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;
    
    /**
     * 称谓
     */
    private Integer title;

    @JsonIgnore
    private boolean titleDirtyFlag;
    

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [#会议]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return this.meeting_count ;
    }

    /**
     * 设置 [#会议]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

    /**
     * 获取 [#会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return this.meeting_countDirtyFlag ;
    }

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return this.ibizfunction ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

    /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return this.ibizfunctionDirtyFlag ;
    }

    /**
     * 获取 [退回]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return this.message_bounce ;
    }

    /**
     * 设置 [退回]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [退回]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return this.message_bounceDirtyFlag ;
    }

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }

    /**
     * 获取 [关闭日期]
     */
    @JsonProperty("day_close")
    public Double getDay_close(){
        return this.day_close ;
    }

    /**
     * 设置 [关闭日期]
     */
    @JsonProperty("day_close")
    public void setDay_close(Double  day_close){
        this.day_close = day_close ;
        this.day_closeDirtyFlag = true ;
    }

    /**
     * 获取 [关闭日期]脏标记
     */
    @JsonIgnore
    public boolean getDay_closeDirtyFlag(){
        return this.day_closeDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [按比例分摊收入]
     */
    @JsonProperty("expected_revenue")
    public Double getExpected_revenue(){
        return this.expected_revenue ;
    }

    /**
     * 设置 [按比例分摊收入]
     */
    @JsonProperty("expected_revenue")
    public void setExpected_revenue(Double  expected_revenue){
        this.expected_revenue = expected_revenue ;
        this.expected_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [按比例分摊收入]脏标记
     */
    @JsonIgnore
    public boolean getExpected_revenueDirtyFlag(){
        return this.expected_revenueDirtyFlag ;
    }

    /**
     * 获取 [关闭日期]
     */
    @JsonProperty("date_closed")
    public Timestamp getDate_closed(){
        return this.date_closed ;
    }

    /**
     * 设置 [关闭日期]
     */
    @JsonProperty("date_closed")
    public void setDate_closed(Timestamp  date_closed){
        this.date_closed = date_closed ;
        this.date_closedDirtyFlag = true ;
    }

    /**
     * 获取 [关闭日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_closedDirtyFlag(){
        return this.date_closedDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [全局抄送]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return this.email_cc ;
    }

    /**
     * 设置 [全局抄送]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

    /**
     * 获取 [全局抄送]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return this.email_ccDirtyFlag ;
    }

    /**
     * 获取 [联系人姓名]
     */
    @JsonProperty("contact_name")
    public String getContact_name(){
        return this.contact_name ;
    }

    /**
     * 设置 [联系人姓名]
     */
    @JsonProperty("contact_name")
    public void setContact_name(String  contact_name){
        this.contact_name = contact_name ;
        this.contact_nameDirtyFlag = true ;
    }

    /**
     * 获取 [联系人姓名]脏标记
     */
    @JsonIgnore
    public boolean getContact_nameDirtyFlag(){
        return this.contact_nameDirtyFlag ;
    }

    /**
     * 获取 [最后阶段更新]
     */
    @JsonProperty("date_last_stage_update")
    public Timestamp getDate_last_stage_update(){
        return this.date_last_stage_update ;
    }

    /**
     * 设置 [最后阶段更新]
     */
    @JsonProperty("date_last_stage_update")
    public void setDate_last_stage_update(Timestamp  date_last_stage_update){
        this.date_last_stage_update = date_last_stage_update ;
        this.date_last_stage_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后阶段更新]脏标记
     */
    @JsonIgnore
    public boolean getDate_last_stage_updateDirtyFlag(){
        return this.date_last_stage_updateDirtyFlag ;
    }

    /**
     * 获取 [预期收益]
     */
    @JsonProperty("planned_revenue")
    public Double getPlanned_revenue(){
        return this.planned_revenue ;
    }

    /**
     * 设置 [预期收益]
     */
    @JsonProperty("planned_revenue")
    public void setPlanned_revenue(Double  planned_revenue){
        this.planned_revenue = planned_revenue ;
        this.planned_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [预期收益]脏标记
     */
    @JsonIgnore
    public boolean getPlanned_revenueDirtyFlag(){
        return this.planned_revenueDirtyFlag ;
    }

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [预期结束]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return this.date_deadline ;
    }

    /**
     * 设置 [预期结束]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [预期结束]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return this.date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [便签]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [便签]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [便签]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }

    /**
     * 获取 [手机]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return this.mobile ;
    }

    /**
     * 设置 [手机]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

    /**
     * 获取 [手机]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return this.mobileDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }

    /**
     * 获取 [网站]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return this.website ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return this.websiteDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }

    /**
     * 获取 [转换日期]
     */
    @JsonProperty("date_conversion")
    public Timestamp getDate_conversion(){
        return this.date_conversion ;
    }

    /**
     * 设置 [转换日期]
     */
    @JsonProperty("date_conversion")
    public void setDate_conversion(Timestamp  date_conversion){
        this.date_conversion = date_conversion ;
        this.date_conversionDirtyFlag = true ;
    }

    /**
     * 获取 [转换日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_conversionDirtyFlag(){
        return this.date_conversionDirtyFlag ;
    }

    /**
     * 获取 [客户名称]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return this.partner_name ;
    }

    /**
     * 设置 [客户名称]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

    /**
     * 获取 [客户名称]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return this.partner_nameDirtyFlag ;
    }

    /**
     * 获取 [销售订单的总数]
     */
    @JsonProperty("sale_amount_total")
    public Double getSale_amount_total(){
        return this.sale_amount_total ;
    }

    /**
     * 设置 [销售订单的总数]
     */
    @JsonProperty("sale_amount_total")
    public void setSale_amount_total(Double  sale_amount_total){
        this.sale_amount_total = sale_amount_total ;
        this.sale_amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单的总数]脏标记
     */
    @JsonIgnore
    public boolean getSale_amount_totalDirtyFlag(){
        return this.sale_amount_totalDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [看板状态]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return this.kanban_state ;
    }

    /**
     * 设置 [看板状态]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [看板状态]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return this.kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [引荐于]
     */
    @JsonProperty("referred")
    public String getReferred(){
        return this.referred ;
    }

    /**
     * 设置 [引荐于]
     */
    @JsonProperty("referred")
    public void setReferred(String  referred){
        this.referred = referred ;
        this.referredDirtyFlag = true ;
    }

    /**
     * 获取 [引荐于]脏标记
     */
    @JsonIgnore
    public boolean getReferredDirtyFlag(){
        return this.referredDirtyFlag ;
    }

    /**
     * 获取 [概率]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return this.probability ;
    }

    /**
     * 设置 [概率]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [概率]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return this.probabilityDirtyFlag ;
    }

    /**
     * 获取 [最近行动]
     */
    @JsonProperty("date_action_last")
    public Timestamp getDate_action_last(){
        return this.date_action_last ;
    }

    /**
     * 设置 [最近行动]
     */
    @JsonProperty("date_action_last")
    public void setDate_action_last(Timestamp  date_action_last){
        this.date_action_last = date_action_last ;
        this.date_action_lastDirtyFlag = true ;
    }

    /**
     * 获取 [最近行动]脏标记
     */
    @JsonIgnore
    public boolean getDate_action_lastDirtyFlag(){
        return this.date_action_lastDirtyFlag ;
    }

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [分配日期]
     */
    @JsonProperty("date_open")
    public Timestamp getDate_open(){
        return this.date_open ;
    }

    /**
     * 设置 [分配日期]
     */
    @JsonProperty("date_open")
    public void setDate_open(Timestamp  date_open){
        this.date_open = date_open ;
        this.date_openDirtyFlag = true ;
    }

    /**
     * 获取 [分配日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_openDirtyFlag(){
        return this.date_openDirtyFlag ;
    }

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [分配天数]
     */
    @JsonProperty("day_open")
    public Double getDay_open(){
        return this.day_open ;
    }

    /**
     * 设置 [分配天数]
     */
    @JsonProperty("day_open")
    public void setDay_open(Double  day_open){
        this.day_open = day_open ;
        this.day_openDirtyFlag = true ;
    }

    /**
     * 获取 [分配天数]脏标记
     */
    @JsonIgnore
    public boolean getDay_openDirtyFlag(){
        return this.day_openDirtyFlag ;
    }

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [报价单的数量]
     */
    @JsonProperty("sale_number")
    public Integer getSale_number(){
        return this.sale_number ;
    }

    /**
     * 设置 [报价单的数量]
     */
    @JsonProperty("sale_number")
    public void setSale_number(Integer  sale_number){
        this.sale_number = sale_number ;
        this.sale_numberDirtyFlag = true ;
    }

    /**
     * 获取 [报价单的数量]脏标记
     */
    @JsonIgnore
    public boolean getSale_numberDirtyFlag(){
        return this.sale_numberDirtyFlag ;
    }

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }

    /**
     * 获取 [订单]
     */
    @JsonProperty("order_ids")
    public String getOrder_ids(){
        return this.order_ids ;
    }

    /**
     * 设置 [订单]
     */
    @JsonProperty("order_ids")
    public void setOrder_ids(String  order_ids){
        this.order_ids = order_ids ;
        this.order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [订单]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idsDirtyFlag(){
        return this.order_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴联系姓名]
     */
    @JsonProperty("partner_address_name")
    public String getPartner_address_name(){
        return this.partner_address_name ;
    }

    /**
     * 设置 [业务伙伴联系姓名]
     */
    @JsonProperty("partner_address_name")
    public void setPartner_address_name(String  partner_address_name){
        this.partner_address_name = partner_address_name ;
        this.partner_address_nameDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴联系姓名]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_nameDirtyFlag(){
        return this.partner_address_nameDirtyFlag ;
    }

    /**
     * 获取 [媒介]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒介]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [媒介]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("company_currency")
    public Integer getCompany_currency(){
        return this.company_currency ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("company_currency")
    public void setCompany_currency(Integer  company_currency){
        this.company_currency = company_currency ;
        this.company_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currencyDirtyFlag(){
        return this.company_currencyDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }

    /**
     * 获取 [Partner Contact Mobile]
     */
    @JsonProperty("partner_address_mobile")
    public String getPartner_address_mobile(){
        return this.partner_address_mobile ;
    }

    /**
     * 设置 [Partner Contact Mobile]
     */
    @JsonProperty("partner_address_mobile")
    public void setPartner_address_mobile(String  partner_address_mobile){
        this.partner_address_mobile = partner_address_mobile ;
        this.partner_address_mobileDirtyFlag = true ;
    }

    /**
     * 获取 [Partner Contact Mobile]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_mobileDirtyFlag(){
        return this.partner_address_mobileDirtyFlag ;
    }

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [用户EMail]
     */
    @JsonProperty("user_email")
    public String getUser_email(){
        return this.user_email ;
    }

    /**
     * 设置 [用户EMail]
     */
    @JsonProperty("user_email")
    public void setUser_email(String  user_email){
        this.user_email = user_email ;
        this.user_emailDirtyFlag = true ;
    }

    /**
     * 获取 [用户EMail]脏标记
     */
    @JsonIgnore
    public boolean getUser_emailDirtyFlag(){
        return this.user_emailDirtyFlag ;
    }

    /**
     * 获取 [用户 登录]
     */
    @JsonProperty("user_login")
    public String getUser_login(){
        return this.user_login ;
    }

    /**
     * 设置 [用户 登录]
     */
    @JsonProperty("user_login")
    public void setUser_login(String  user_login){
        this.user_login = user_login ;
        this.user_loginDirtyFlag = true ;
    }

    /**
     * 获取 [用户 登录]脏标记
     */
    @JsonIgnore
    public boolean getUser_loginDirtyFlag(){
        return this.user_loginDirtyFlag ;
    }

    /**
     * 获取 [合作伙伴联系电话]
     */
    @JsonProperty("partner_address_phone")
    public String getPartner_address_phone(){
        return this.partner_address_phone ;
    }

    /**
     * 设置 [合作伙伴联系电话]
     */
    @JsonProperty("partner_address_phone")
    public void setPartner_address_phone(String  partner_address_phone){
        this.partner_address_phone = partner_address_phone ;
        this.partner_address_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [合作伙伴联系电话]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_phoneDirtyFlag(){
        return this.partner_address_phoneDirtyFlag ;
    }

    /**
     * 获取 [省份]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return this.state_id_text ;
    }

    /**
     * 设置 [省份]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [省份]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return this.state_id_textDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [合作伙伴黑名单]
     */
    @JsonProperty("partner_is_blacklisted")
    public String getPartner_is_blacklisted(){
        return this.partner_is_blacklisted ;
    }

    /**
     * 设置 [合作伙伴黑名单]
     */
    @JsonProperty("partner_is_blacklisted")
    public void setPartner_is_blacklisted(String  partner_is_blacklisted){
        this.partner_is_blacklisted = partner_is_blacklisted ;
        this.partner_is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [合作伙伴黑名单]脏标记
     */
    @JsonIgnore
    public boolean getPartner_is_blacklistedDirtyFlag(){
        return this.partner_is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [业务伙伴联系EMail]
     */
    @JsonProperty("partner_address_email")
    public String getPartner_address_email(){
        return this.partner_address_email ;
    }

    /**
     * 设置 [业务伙伴联系EMail]
     */
    @JsonProperty("partner_address_email")
    public void setPartner_address_email(String  partner_address_email){
        this.partner_address_email = partner_address_email ;
        this.partner_address_emailDirtyFlag = true ;
    }

    /**
     * 获取 [业务伙伴联系EMail]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_emailDirtyFlag(){
        return this.partner_address_emailDirtyFlag ;
    }

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title_text")
    public String getTitle_text(){
        return this.title_text ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title_text")
    public void setTitle_text(String  title_text){
        this.title_text = title_text ;
        this.title_textDirtyFlag = true ;
    }

    /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitle_textDirtyFlag(){
        return this.title_textDirtyFlag ;
    }

    /**
     * 获取 [失去原因]
     */
    @JsonProperty("lost_reason_text")
    public String getLost_reason_text(){
        return this.lost_reason_text ;
    }

    /**
     * 设置 [失去原因]
     */
    @JsonProperty("lost_reason_text")
    public void setLost_reason_text(String  lost_reason_text){
        this.lost_reason_text = lost_reason_text ;
        this.lost_reason_textDirtyFlag = true ;
    }

    /**
     * 获取 [失去原因]脏标记
     */
    @JsonIgnore
    public boolean getLost_reason_textDirtyFlag(){
        return this.lost_reason_textDirtyFlag ;
    }

    /**
     * 获取 [失去原因]
     */
    @JsonProperty("lost_reason")
    public Integer getLost_reason(){
        return this.lost_reason ;
    }

    /**
     * 设置 [失去原因]
     */
    @JsonProperty("lost_reason")
    public void setLost_reason(Integer  lost_reason){
        this.lost_reason = lost_reason ;
        this.lost_reasonDirtyFlag = true ;
    }

    /**
     * 获取 [失去原因]脏标记
     */
    @JsonIgnore
    public boolean getLost_reasonDirtyFlag(){
        return this.lost_reasonDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [省份]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [省份]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [省份]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }

    /**
     * 获取 [媒介]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒介]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [媒介]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return this.title ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return this.titleDirtyFlag ;
    }



}

package cn.ibizlab.odoo.mob.odoo_product.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.mob.odoo_product.feign.Product_categoryFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Product_categoryService {

    Product_categoryFeignClient client;

    @Autowired
    public Product_categoryService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_categoryFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_categoryFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer product_category_id) {
        return client.remove( product_category_id);
    }

	public Product_category create(Product_category product_category) {
        return client.create(product_category);
    }

    public Product_category updateBatch(Integer product_category_id, Product_category product_category) {
        return client.updateBatch(product_category_id, product_category);
    }

    public Product_category removeBatch(Integer product_category_id, Product_category product_category) {
        return client.removeBatch(product_category_id, product_category);
    }

    public Product_category update(Integer product_category_id, Product_category product_category) {
        return client.update(product_category_id, product_category);
    }

    public Product_category save(Integer product_category_id, Product_category product_category) {
        return client.save(product_category_id, product_category);
    }

    public Product_category getDraft(Integer product_category_id, Product_category product_category) {
        return client.getDraft(product_category_id, product_category);
    }

    public Product_category createBatch(Integer product_category_id, Product_category product_category) {
        return client.createBatch(product_category_id, product_category);
    }

    public boolean checkKey(Product_category product_category) {
        return client.checkKey(product_category);
    }

    public Product_category get( Integer product_category_id) {
        return client.get( product_category_id);
    }

	public Page<Product_category> fetchDefault(Product_categorySearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}

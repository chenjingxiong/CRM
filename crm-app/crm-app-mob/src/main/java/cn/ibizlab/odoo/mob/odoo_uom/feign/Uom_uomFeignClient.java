package cn.ibizlab.odoo.mob.odoo_uom.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.mob.odoo_uom.filter.*;


public interface Uom_uomFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/uom_uoms/{uom_uom_id}")
    public Boolean remove(@PathVariable("uom_uom_id") Integer uom_uom_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/uom_uoms/{uom_uom_id}/getdraft")
    public Uom_uom getDraft(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/uom_uoms/{uom_uom_id}")
    public Uom_uom update(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/uom_uoms/{uom_uom_id}/createbatch")
    public Uom_uom createBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/uom_uoms/{uom_uom_id}")
    public Uom_uom get(@PathVariable("uom_uom_id") Integer uom_uom_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/uom_uoms/{uom_uom_id}/updatebatch")
    public Uom_uom updateBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/uom_uoms/{uom_uom_id}/removebatch")
    public Uom_uom removeBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/uom_uoms")
    public Uom_uom create(@RequestBody Uom_uom uom_uom) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/uom_uoms/fetchdefault")
	public Page<Uom_uom> fetchDefault(Uom_uomSearchContext searchContext) ;
}

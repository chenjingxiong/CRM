package cn.ibizlab.odoo.mob.odoo_account.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;


public interface Account_invoiceFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoices/{account_invoice_id}")
    public Boolean remove(@PathVariable("account_invoice_id") Integer account_invoice_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/checkkey")
    public Boolean checkKey(@RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices")
    public Account_invoice create(@RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoices/{account_invoice_id}")
    public Account_invoice update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoices/{account_invoice_id}/updatebatch")
    public Account_invoice updateBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoices/{account_invoice_id}")
    public Account_invoice get(@PathVariable("account_invoice_id") Integer account_invoice_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/{account_invoice_id}/createbatch")
    public Account_invoice createBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoices/{account_invoice_id}/getdraft")
    public Account_invoice getDraft(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/{account_invoice_id}/save")
    public Account_invoice save(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoices/{account_invoice_id}/removebatch")
    public Account_invoice removeBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/account_invoices/fetchdefault")
	public Page<Account_invoice> fetchDefault(Account_invoiceSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    public Boolean removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/checkkey")
    public Boolean checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices")
    public Account_invoice createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    public Account_invoice updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/updatebatch")
    public Account_invoice updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    public Account_invoice getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/createbatch")
    public Account_invoice createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/getdraft")
    public Account_invoice getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/save")
    public Account_invoice saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/removebatch")
    public Account_invoice removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/account_invoices/fetchdefault")
	public Page<Account_invoice> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Account_invoiceSearchContext searchContext) ;
}

package cn.ibizlab.odoo.mob.r7rt_dyna.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.odoo.mob.r7rt_dyna.filter.*;


public interface DynaDashboardFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards/{dynadashboard_id}/save")
    public DynaDashboard save(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/dynadashboards/{dynadashboard_id}")
    public DynaDashboard update(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/dynadashboards/{dynadashboard_id}")
    public DynaDashboard get(@PathVariable("dynadashboard_id") String dynadashboard_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/dynadashboards/{dynadashboard_id}")
    public Boolean remove(@PathVariable("dynadashboard_id") String dynadashboard_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards/checkkey")
    public Boolean checkKey(@RequestBody DynaDashboard dynadashboard) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/dynadashboards/{dynadashboard_id}/getdraft")
    public DynaDashboard getDraft(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards")
    public DynaDashboard create(@RequestBody DynaDashboard dynadashboard) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/dynadashboards/fetchdefault")
	public Page<DynaDashboard> fetchDefault(DynaDashboardSearchContext searchContext) ;
}

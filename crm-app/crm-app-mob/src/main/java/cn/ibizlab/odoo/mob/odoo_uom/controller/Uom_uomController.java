package cn.ibizlab.odoo.mob.odoo_uom.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_uom.service.Uom_uomService;
import cn.ibizlab.odoo.mob.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.mob.odoo_uom.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Uom_uomController {
	@Autowired
    Uom_uomService uom_uomservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/{uom_uom_id}")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("uom_uom_id") Integer uom_uom_id) {
        boolean b = uom_uomservice.remove( uom_uom_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/getdraft")
    @PreAuthorize("@uom_uom_pms.check('CREATE')")
    public ResponseEntity<Uom_uom> getDraft() {
        //Uom_uom uom_uom = uom_uomservice.getDraft( uom_uom_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Uom_uom());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/{uom_uom_id}")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'UPDATE')")
    public ResponseEntity<Uom_uom> update(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.update(uom_uom_id, uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/uom_uoms/{uom_uom_id}")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'UPDATE')")
    public ResponseEntity<Uom_uom> api_update(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.update(uom_uom_id, uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/{uom_uom_id}/createbatch")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'CREATE')")
    public ResponseEntity<Uom_uom> createBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.createBatch(uom_uom_id, uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/{uom_uom_id}")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'READ')")
    public ResponseEntity<Uom_uom> get(@PathVariable("uom_uom_id") Integer uom_uom_id) {
        Uom_uom uom_uom = uom_uomservice.get( uom_uom_id);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/{uom_uom_id}/updatebatch")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'UPDATE')")
    public ResponseEntity<Uom_uom> updateBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.updateBatch(uom_uom_id, uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/{uom_uom_id}/removebatch")
    @PreAuthorize("@uom_uom_pms.check(#uom_uom_id,'DELETE')")
    public ResponseEntity<Uom_uom> removeBatch(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.removeBatch(uom_uom_id, uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms")
    @PreAuthorize("@uom_uom_pms.check('CREATE')")
    public ResponseEntity<Uom_uom> create(@RequestBody Uom_uom uom_uom) {
        Uom_uom uom_uom2 = uom_uomservice.create(uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uom2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/uom_uoms/fetchdefault")
    @PreAuthorize("@uom_uom_pms.check('READ')")
	public ResponseEntity<List<Uom_uom>> fetchDefault(Uom_uomSearchContext searchContext,Pageable pageable) {
        
        Page<Uom_uom> page = uom_uomservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

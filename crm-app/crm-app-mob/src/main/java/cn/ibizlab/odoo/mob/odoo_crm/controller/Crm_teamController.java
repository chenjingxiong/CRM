package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_teamService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_teamController {
	@Autowired
    Crm_teamService crm_teamservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'READ')")
    public ResponseEntity<Crm_team> get(@PathVariable("crm_team_id") Integer crm_team_id) {
        Crm_team crm_team = crm_teamservice.get( crm_team_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/checkkey")
    @PreAuthorize("@crm_team_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_team crm_team) {
        boolean b = crm_teamservice.checkKey(crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/getdraft")
    @PreAuthorize("@crm_team_pms.check('CREATE')")
    public ResponseEntity<Crm_team> getDraft() {
        //Crm_team crm_team = crm_teamservice.getDraft( crm_team_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_team());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}/removebatch")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'DELETE')")
    public ResponseEntity<Crm_team> removeBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.removeBatch(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/createbatch")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'CREATE')")
    public ResponseEntity<Crm_team> createBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.createBatch(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'UPDATE')")
    public ResponseEntity<Crm_team> update(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.update(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_teams/{crm_team_id}")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'UPDATE')")
    public ResponseEntity<Crm_team> api_update(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.update(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_team_id") Integer crm_team_id) {
        boolean b = crm_teamservice.remove( crm_team_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams")
    @PreAuthorize("@crm_team_pms.check('CREATE')")
    public ResponseEntity<Crm_team> create(@RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.create(crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}/updatebatch")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'UPDATE')")
    public ResponseEntity<Crm_team> updateBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.updateBatch(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/{crm_team_id}/save")
    @PreAuthorize("@crm_team_pms.check(#crm_team_id,'')")
    public ResponseEntity<Crm_team> save(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) {
        Crm_team crm_team2 = crm_teamservice.save(crm_team_id, crm_team);
        return ResponseEntity.status(HttpStatus.OK).body(crm_team2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_teams/fetchdefault")
    @PreAuthorize("@crm_team_pms.check('READ')")
	public ResponseEntity<List<Crm_team>> fetchDefault(Crm_teamSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_team> page = crm_teamservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

package cn.ibizlab.odoo.mob.r7rt_dyna.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.r7rt_dyna.service.DynaChartService;
import cn.ibizlab.odoo.mob.r7rt_dyna.domain.DynaChart;
import cn.ibizlab.odoo.mob.r7rt_dyna.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class DynaChartController {
	@Autowired
    DynaChartService dynachartservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/dynacharts/{dynachart_id}")
    @PreAuthorize("@dynachart_pms.check(#dynachart_id,'READ')")
    public ResponseEntity<DynaChart> get(@PathVariable("dynachart_id") String dynachart_id) {
        DynaChart dynachart = dynachartservice.get( dynachart_id);
        return ResponseEntity.status(HttpStatus.OK).body(dynachart);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/dynacharts/getdraft")
    @PreAuthorize("@dynachart_pms.check('CREATE')")
    public ResponseEntity<DynaChart> getDraft() {
        //DynaChart dynachart = dynachartservice.getDraft( dynachart_id);
        return ResponseEntity.status(HttpStatus.OK).body(new DynaChart());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynacharts/checkkey")
    @PreAuthorize("@dynachart_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody DynaChart dynachart) {
        boolean b = dynachartservice.checkKey(dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/dynacharts/{dynachart_id}")
    @PreAuthorize("@dynachart_pms.check(#dynachart_id,'UPDATE')")
    public ResponseEntity<DynaChart> update(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) {
        DynaChart dynachart2 = dynachartservice.update(dynachart_id, dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(dynachart2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/dynacharts/{dynachart_id}")
    @PreAuthorize("@dynachart_pms.check(#dynachart_id,'UPDATE')")
    public ResponseEntity<DynaChart> api_update(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) {
        DynaChart dynachart2 = dynachartservice.update(dynachart_id, dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(dynachart2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/dynacharts/{dynachart_id}")
    @PreAuthorize("@dynachart_pms.check(#dynachart_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("dynachart_id") String dynachart_id) {
        boolean b = dynachartservice.remove( dynachart_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynacharts")
    @PreAuthorize("@dynachart_pms.check('CREATE')")
    public ResponseEntity<DynaChart> create(@RequestBody DynaChart dynachart) {
        DynaChart dynachart2 = dynachartservice.create(dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(dynachart2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynacharts/{dynachart_id}/save")
    @PreAuthorize("@dynachart_pms.check(#dynachart_id,'')")
    public ResponseEntity<DynaChart> save(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChart dynachart) {
        DynaChart dynachart2 = dynachartservice.save(dynachart_id, dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(dynachart2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/dynacharts/fetchdefault")
    @PreAuthorize("@dynachart_pms.check('READ')")
	public ResponseEntity<List<DynaChart>> fetchDefault(DynaChartSearchContext searchContext,Pageable pageable) {
        
        Page<DynaChart> page = dynachartservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}

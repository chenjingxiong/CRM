package cn.ibizlab.odoo.mob.odoo_account.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;


public interface Account_journalFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/account_journals/{account_journal_id}/createbatch")
    public Account_journal createBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_journals/{account_journal_id}")
    public Account_journal get(@PathVariable("account_journal_id") Integer account_journal_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/account_journals/{account_journal_id}/getdraft")
    public Account_journal getDraft(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/account_journals")
    public Account_journal create(@RequestBody Account_journal account_journal) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_journals/{account_journal_id}/removebatch")
    public Account_journal removeBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_journals/{account_journal_id}")
    public Account_journal update(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_journals/{account_journal_id}")
    public Boolean remove(@PathVariable("account_journal_id") Integer account_journal_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_journals/{account_journal_id}/updatebatch")
    public Account_journal updateBatch(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journal account_journal) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/account_journals/fetchdefault")
	public Page<Account_journal> fetchDefault(Account_journalSearchContext searchContext) ;
}

package cn.ibizlab.odoo.mob.odoo_base.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;


public interface Res_partnerFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}")
    public Boolean remove(@PathVariable("res_partner_id") Integer res_partner_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}")
    public Res_partner get(@PathVariable("res_partner_id") Integer res_partner_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/checkkey")
    public Boolean checkKey(@RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/save")
    public Res_partner save(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}")
    public Res_partner update(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/createbatch")
    public Res_partner createBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/updatebatch")
    public Res_partner updateBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/getdraft")
    public Res_partner getDraft(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners")
    public Res_partner create(@RequestBody Res_partner res_partner) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/removebatch")
    public Res_partner removeBatch(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner res_partner) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/fetchcontacts")
	public Page<Res_partner> fetchContacts(Res_partnerSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/fetchcompany")
	public Page<Res_partner> fetchCompany(Res_partnerSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/fetchdefault")
	public Page<Res_partner> fetchDefault(Res_partnerSearchContext searchContext) ;
}

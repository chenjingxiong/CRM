import Res_usersUIActionBase from './res-users-ui-action-base';

/**
 * 用户UI服务对象
 *
 * @export
 * @class Res_usersUIAction
 * @extends {Res_usersUIActionBase}
 */
export class Res_usersUIAction extends Res_usersUIActionBase { }
// 默认导出
export default Res_usersUIAction;

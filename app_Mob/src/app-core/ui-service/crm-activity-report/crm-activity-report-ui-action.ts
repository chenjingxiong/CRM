import { Crm_activity_reportUIActionBase } from './crm-activity-report-ui-action-base';

/**
 * CRM活动分析UI服务对象
 *
 * @export
 * @class Crm_activity_reportUIAction
 * @extends {Crm_activity_reportUIActionBase}
 */
export class Crm_activity_reportUIAction extends Crm_activity_reportUIActionBase { }
// 默认导出
export default Crm_activity_reportUIAction;
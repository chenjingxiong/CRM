import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 日记账UI服务对象基类
 *
 * @export
 * @class Account_journalUIActionBase
 * @extends {UIActionBase}
 */
export class Account_journalUIActionBase extends UIActionBase {

}
import { Crm_lead_lostUIActionBase } from './crm-lead-lost-ui-action-base';

/**
 * 获取丢失原因UI服务对象
 *
 * @export
 * @class Crm_lead_lostUIAction
 * @extends {Crm_lead_lostUIActionBase}
 */
export class Crm_lead_lostUIAction extends Crm_lead_lostUIActionBase { }
// 默认导出
export default Crm_lead_lostUIAction;
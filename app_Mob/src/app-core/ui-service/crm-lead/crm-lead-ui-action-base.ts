import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 线索/商机UI服务对象基类
 *
 * @export
 * @class Crm_leadUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_leadUIActionBase extends UIActionBase {

}
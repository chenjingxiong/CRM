import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 价格表UI服务对象基类
 *
 * @export
 * @class Product_pricelistUIActionBase
 * @extends {UIActionBase}
 */
export class Product_pricelistUIActionBase extends UIActionBase {

}
import { Account_invoice_lineUIActionBase } from './account-invoice-line-ui-action-base';

/**
 * 发票行UI服务对象
 *
 * @export
 * @class Account_invoice_lineUIAction
 * @extends {Account_invoice_lineUIActionBase}
 */
export class Account_invoice_lineUIAction extends Account_invoice_lineUIActionBase { }
// 默认导出
export default Account_invoice_lineUIAction;
import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 活动UI服务对象基类
 *
 * @export
 * @class Calendar_eventUIActionBase
 * @extends {UIActionBase}
 */
export class Calendar_eventUIActionBase extends UIActionBase {

}
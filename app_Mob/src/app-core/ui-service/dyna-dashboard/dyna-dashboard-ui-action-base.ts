import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 动态数据看板UI服务对象基类
 *
 * @export
 * @class DynaDashboardUIActionBase
 * @extends {UIActionBase}
 */
export class DynaDashboardUIActionBase extends UIActionBase {

}
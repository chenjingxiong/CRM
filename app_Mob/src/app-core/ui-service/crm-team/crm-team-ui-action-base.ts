import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 销售渠道UI服务对象基类
 *
 * @export
 * @class Crm_teamUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_teamUIActionBase extends UIActionBase {

}
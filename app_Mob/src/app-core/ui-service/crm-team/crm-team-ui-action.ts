import { Crm_teamUIActionBase } from './crm-team-ui-action-base';

/**
 * 销售渠道UI服务对象
 *
 * @export
 * @class Crm_teamUIAction
 * @extends {Crm_teamUIActionBase}
 */
export class Crm_teamUIAction extends Crm_teamUIActionBase { }
// 默认导出
export default Crm_teamUIAction;
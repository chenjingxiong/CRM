import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 产品计量单位UI服务对象基类
 *
 * @export
 * @class Uom_uomUIActionBase
 * @extends {UIActionBase}
 */
export class Uom_uomUIActionBase extends UIActionBase {

}
import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 合并商机UI服务对象基类
 *
 * @export
 * @class Crm_merge_opportunityUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_merge_opportunityUIActionBase extends UIActionBase {

}
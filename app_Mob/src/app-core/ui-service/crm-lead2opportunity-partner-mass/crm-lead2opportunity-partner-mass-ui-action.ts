import { Crm_lead2opportunity_partner_massUIActionBase } from './crm-lead2opportunity-partner-mass-ui-action-base';

/**
 * 转化线索为商机（批量）UI服务对象
 *
 * @export
 * @class Crm_lead2opportunity_partner_massUIAction
 * @extends {Crm_lead2opportunity_partner_massUIActionBase}
 */
export class Crm_lead2opportunity_partner_massUIAction extends Crm_lead2opportunity_partner_massUIActionBase { }
// 默认导出
export default Crm_lead2opportunity_partner_massUIAction;
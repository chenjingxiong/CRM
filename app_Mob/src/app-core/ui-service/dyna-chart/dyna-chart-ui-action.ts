import { DynaChartUIActionBase } from './dyna-chart-ui-action-base';

/**
 * 动态图表UI服务对象
 *
 * @export
 * @class DynaChartUIAction
 * @extends {DynaChartUIActionBase}
 */
export class DynaChartUIAction extends DynaChartUIActionBase { }
// 默认导出
export default DynaChartUIAction;
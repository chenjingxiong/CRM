import { Mail_activity_typeUIActionBase } from './mail-activity-type-ui-action-base';

/**
 * 活动类型UI服务对象
 *
 * @export
 * @class Mail_activity_typeUIAction
 * @extends {Mail_activity_typeUIActionBase}
 */
export class Mail_activity_typeUIAction extends Mail_activity_typeUIActionBase { }
// 默认导出
export default Mail_activity_typeUIAction;
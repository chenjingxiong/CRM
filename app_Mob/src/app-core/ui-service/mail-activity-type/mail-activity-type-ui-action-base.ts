import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 活动类型UI服务对象基类
 *
 * @export
 * @class Mail_activity_typeUIActionBase
 * @extends {UIActionBase}
 */
export class Mail_activity_typeUIActionBase extends UIActionBase {

}
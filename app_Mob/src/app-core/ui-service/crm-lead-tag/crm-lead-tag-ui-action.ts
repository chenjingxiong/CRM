import { Crm_lead_tagUIActionBase } from './crm-lead-tag-ui-action-base';

/**
 * 线索标签UI服务对象
 *
 * @export
 * @class Crm_lead_tagUIAction
 * @extends {Crm_lead_tagUIActionBase}
 */
export class Crm_lead_tagUIAction extends Crm_lead_tagUIActionBase { }
// 默认导出
export default Crm_lead_tagUIAction;
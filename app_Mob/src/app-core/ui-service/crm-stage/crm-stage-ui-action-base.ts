import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * CRM 阶段UI服务对象基类
 *
 * @export
 * @class Crm_stageUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_stageUIActionBase extends UIActionBase {

}
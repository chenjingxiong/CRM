import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 丢单原因UI服务对象基类
 *
 * @export
 * @class Crm_lost_reasonUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_lost_reasonUIActionBase extends UIActionBase {

}
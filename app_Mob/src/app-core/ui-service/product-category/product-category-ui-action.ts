import { Product_categoryUIActionBase } from './product-category-ui-action-base';

/**
 * 产品种类UI服务对象
 *
 * @export
 * @class Product_categoryUIAction
 * @extends {Product_categoryUIActionBase}
 */
export class Product_categoryUIAction extends Product_categoryUIActionBase { }
// 默认导出
export default Product_categoryUIAction;
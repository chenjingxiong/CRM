import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 发票UI服务对象基类
 *
 * @export
 * @class Account_invoiceUIActionBase
 * @extends {UIActionBase}
 */
export class Account_invoiceUIActionBase extends UIActionBase {

}
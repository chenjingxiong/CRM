import { Product_price_listUIActionBase } from './product-price-list-ui-action-base';

/**
 * 基于价格列表版本的单位产品价格UI服务对象
 *
 * @export
 * @class Product_price_listUIAction
 * @extends {Product_price_listUIActionBase}
 */
export class Product_price_listUIAction extends Product_price_listUIActionBase { }
// 默认导出
export default Product_price_listUIAction;
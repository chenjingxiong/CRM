import { Crm_lead2opportunity_partnerUIActionBase } from './crm-lead2opportunity-partner-ui-action-base';

/**
 * 转化线索为商机（单个）UI服务对象
 *
 * @export
 * @class Crm_lead2opportunity_partnerUIAction
 * @extends {Crm_lead2opportunity_partnerUIActionBase}
 */
export class Crm_lead2opportunity_partnerUIAction extends Crm_lead2opportunity_partnerUIActionBase { }
// 默认导出
export default Crm_lead2opportunity_partnerUIAction;
import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 转化线索为商机（单个）UI服务对象基类
 *
 * @export
 * @class Crm_lead2opportunity_partnerUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_lead2opportunity_partnerUIActionBase extends UIActionBase {

}
/**
 * MobMainInfoPanel 部件模型
 *
 * @export
 * @class MobMainInfoPanelModel
 */
export class MobMainInfoPanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MobMainInfoPanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'activity_user_id',
      },
      {
        name: 'meeting_count',
      },
      {
        name: 'ibizfunction',
      },
      {
        name: 'message_bounce',
      },
      {
        name: 'city',
      },
      {
        name: 'day_close',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'expected_revenue',
      },
      {
        name: 'date_closed',
      },
      {
        name: 'create_date',
      },
      {
        name: 'email_cc',
      },
      {
        name: 'contact_name',
      },
      {
        name: 'date_last_stage_update',
      },
      {
        name: 'planned_revenue',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'date_deadline',
      },
      {
        name: 'zip',
      },
      {
        name: 'name',
      },
      {
        name: 'crm_lead',
        prop: 'id',
      },
      {
        name: 'description',
      },
      {
        name: 'mobile',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'type',
      },
      {
        name: 'website',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'email_from',
      },
      {
        name: 'date_conversion',
      },
      {
        name: 'partner_name',
      },
      {
        name: 'sale_amount_total',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'message_needaction',
      },
      {
        name: '__last_update',
      },
      {
        name: 'kanban_state',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'referred',
      },
      {
        name: 'probability',
      },
      {
        name: 'date_action_last',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'date_open',
      },
      {
        name: 'phone',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'day_open',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'sale_number',
      },
      {
        name: 'street2',
      },
      {
        name: 'order_ids',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'street',
      },
      {
        name: 'color',
      },
      {
        name: 'write_date',
      },
      {
        name: 'active',
      },
      {
        name: 'display_name',
      },
      {
        name: 'tag_ids',
      },
      {
        name: 'is_blacklisted',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'priority',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'source_id_text',
      },
      {
        name: 'partner_address_name',
      },
      {
        name: 'medium_id_text',
      },
      {
        name: 'company_currency',
      },
      {
        name: 'team_id_text',
      },
      {
        name: 'partner_address_mobile',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'user_email',
      },
      {
        name: 'user_login',
      },
      {
        name: 'partner_address_phone',
      },
      {
        name: 'state_id_text',
      },
      {
        name: 'campaign_id_text',
      },
      {
        name: 'partner_is_blacklisted',
      },
      {
        name: 'partner_address_email',
      },
      {
        name: 'stage_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'country_id_text',
      },
      {
        name: 'title_text',
      },
      {
        name: 'lost_reason_text',
      },
      {
        name: 'lost_reason',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'company_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'state_id',
      },
      {
        name: 'medium_id',
      },
      {
        name: 'stage_id',
      },
      {
        name: 'source_id',
      },
      {
        name: 'country_id',
      },
      {
        name: 'campaign_id',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'team_id',
      },
      {
        name: 'title',
      },
    ]
  }

}
// 默认导出
export default MobMainInfoPanelModel;
/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'team_id',
				prop: 'team_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'account_id',
				prop: 'account_id',
				dataType: 'PICKUP',
			},
			{
				name: 'medium_id',
				prop: 'medium_id',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'move_id',
				prop: 'move_id',
				dataType: 'PICKUP',
			},
			{
				name: 'payment_term_id',
				prop: 'payment_term_id',
				dataType: 'PICKUP',
			},
			{
				name: 'journal_id',
				prop: 'journal_id',
				dataType: 'PICKUP',
			},
			{
				name: 'incoterm_id',
				prop: 'incoterm_id',
				dataType: 'PICKUP',
			},
			{
				name: 'purchase_id',
				prop: 'purchase_id',
				dataType: 'PICKUP',
			},
			{
				name: 'fiscal_position_id',
				prop: 'fiscal_position_id',
				dataType: 'PICKUP',
			},
			{
				name: 'refund_invoice_id',
				prop: 'refund_invoice_id',
				dataType: 'PICKUP',
			},
			{
				name: 'currency_id',
				prop: 'currency_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_id',
				prop: 'partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_bank_id',
				prop: 'partner_bank_id',
				dataType: 'PICKUP',
			},
			{
				name: 'cash_rounding_id',
				prop: 'cash_rounding_id',
				dataType: 'PICKUP',
			},
			{
				name: 'commercial_partner_id',
				prop: 'commercial_partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_shipping_id',
				prop: 'partner_shipping_id',
				dataType: 'PICKUP',
			},
			{
				name: 'incoterms_id',
				prop: 'incoterms_id',
				dataType: 'PICKUP',
			},
			{
				name: 'source_id',
				prop: 'source_id',
				dataType: 'PICKUP',
			},
			{
				name: 'vendor_bill_id',
				prop: 'vendor_bill_id',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'vendor_bill_purchase_id',
				prop: 'vendor_bill_purchase_id',
				dataType: 'PICKUP',
			},
			{
				name: 'campaign_id',
				prop: 'campaign_id',
				dataType: 'PICKUP',
			},
			{
				name: 'account_invoice',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;
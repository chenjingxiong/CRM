/**
 * MobMain 部件模型
 *
 * @export
 * @class MobMainModel
 */
export class MobMainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MobMainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'client_order_ref',
        prop: 'client_order_ref',
        dataType: 'TEXT',
      },
      {
        name: 'partner_id_text',
        prop: 'partner_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_order',
        prop: 'date_order',
        dataType: 'DATETIME',
      },
      {
        name: 'state',
        prop: 'state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'amount_total',
        prop: 'amount_total',
        dataType: 'DECIMAL',
      },
      {
        name: 'order_line',
        prop: 'order_line',
        dataType: 'LONGTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'partner_id',
        prop: 'partner_id',
        dataType: 'PICKUP',
      },
      {
        name: 'sale_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
// 默认导出
export default MobMainModel;
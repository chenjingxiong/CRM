/**
 * OrderBar 部件模型
 *
 * @export
 * @class OrderBarModel
 */
export class OrderBarModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof OrderBarDb_sysportlet2_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name:'query',
				prop:'query'
			},
		]
	}

}
// 默认导出
export default OrderBarModel;
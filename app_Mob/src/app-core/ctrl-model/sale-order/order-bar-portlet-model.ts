/**
 * OrderBar 部件模型
 *
 * @export
 * @class OrderBarModel
 */
export class OrderBarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof OrderBarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'date_order',
      },
      {
        name: 'invoice_ids',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'amount_untaxed',
      },
      {
        name: 'validity_date',
      },
      {
        name: 'amount_undiscounted',
      },
      {
        name: 'invoice_count',
      },
      {
        name: 'access_warning',
      },
      {
        name: 'warning_stock',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'effective_date',
      },
      {
        name: 'website_id',
      },
      {
        name: 'require_signature',
      },
      {
        name: 'currency_rate',
      },
      {
        name: 'picking_policy',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'signature',
      },
      {
        name: 'procurement_group_id',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'cart_quantity',
      },
      {
        name: 'type_name',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'delivery_count',
      },
      {
        name: 'signed_by',
      },
      {
        name: 'order_line',
      },
      {
        name: 'create_date',
      },
      {
        name: 'require_payment',
      },
      {
        name: 'only_services',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'website_order_line',
      },
      {
        name: 'reference',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'write_date',
      },
      {
        name: 'invoice_status',
      },
      {
        name: 'tag_ids',
      },
      {
        name: 'sale_order',
        prop: 'id',
      },
      {
        name: 'transaction_ids',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'is_abandoned_cart',
      },
      {
        name: 'state',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'expense_count',
      },
      {
        name: 'authorized_transaction_ids',
      },
      {
        name: 'commitment_date',
      },
      {
        name: 'is_expired',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'remaining_validity_days',
      },
      {
        name: 'expected_date',
      },
      {
        name: 'purchase_order_count',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'access_url',
      },
      {
        name: 'name',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'sale_order_option_ids',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'origin',
      },
      {
        name: 'confirmation_date',
      },
      {
        name: 'note',
      },
      {
        name: 'amount_by_group',
      },
      {
        name: 'picking_ids',
      },
      {
        name: '__last_update',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'access_token',
      },
      {
        name: 'amount_total',
      },
      {
        name: 'client_order_ref',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'cart_recovery_email_sent',
      },
      {
        name: 'display_name',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'amount_tax',
      },
      {
        name: 'expense_ids',
      },
      {
        name: 'partner_invoice_id_text',
      },
      {
        name: 'incoterm_text',
      },
      {
        name: 'team_id_text',
      },
      {
        name: 'analytic_account_id_text',
      },
      {
        name: 'partner_id_text',
      },
      {
        name: 'campaign_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'partner_shipping_id_text',
      },
      {
        name: 'warehouse_id_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'opportunity_id_text',
      },
      {
        name: 'source_id_text',
      },
      {
        name: 'medium_id_text',
      },
      {
        name: 'fiscal_position_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'pricelist_id_text',
      },
      {
        name: 'payment_term_id_text',
      },
      {
        name: 'sale_order_template_id_text',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'team_id',
      },
      {
        name: 'partner_invoice_id',
      },
      {
        name: 'fiscal_position_id',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'campaign_id',
      },
      {
        name: 'source_id',
      },
      {
        name: 'sale_order_template_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'company_id',
      },
      {
        name: 'medium_id',
      },
      {
        name: 'analytic_account_id',
      },
      {
        name: 'payment_term_id',
      },
      {
        name: 'partner_shipping_id',
      },
      {
        name: 'opportunity_id',
      },
      {
        name: 'incoterm',
      },
      {
        name: 'pricelist_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'warehouse_id',
      },
    ]
  }

}
// 默认导出
export default OrderBarModel;
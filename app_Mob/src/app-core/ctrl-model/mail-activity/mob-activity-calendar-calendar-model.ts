/**
 * MobActivityCalendar 部件模型
 *
 * @export
 * @class MobActivityCalendarModel
 */
export class MobActivityCalendarModel {

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof MobActivityCalendarCalendarMode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobActivityCalendarCalendarMode
	 */
	public getDataItems(): any[] {
        let dataItems: any = [
            {
                name: 'queryStart',
                prop: 'n_start_gtandeq'
            },
            {
                name: 'queryEnd',
                prop: 'n_end_ltandeq'
            },
            {
                name: 'color',
            },
            {
                name: 'textColor',
            },
            {
                name: 'itemType',
            },
        ];
        switch (this.itemType) {
            case "activity2":
                dataItems = 
                    [
                        ...dataItems,
                        {
                            name: 'mail_activity',
                            prop: 'id'
                        },
                        {
                            name: 'title',
                            prop: 'display_name'
                        },
                        {
                            name:'start',
                            prop:'n_create_date_gtandeq'
                        },
                        {
                            name:'end',
                            prop:'n_date_deadline_ltandeq'
                        },
                    ];
                break;
            case "activity":
                dataItems = 
                    [
                        ...dataItems,
                        {
                            name: 'mail_activity',
                            prop: 'id'
                        },
                        {
                            name: 'title',
                            prop: 'display_name'
                        },
                        {
                            name:'start',
                            prop:'n_create_date_gtandeq'
                        },
                        {
                            name:'end',
                            prop:'n_date_deadline_ltandeq'
                        },
                    ];
                break;
        }
        return dataItems;
	}

}
// 默认导出
export default MobActivityCalendarModel;
import { TabExpServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobTabExpView9tabexppanelModel } from '@/app-core/ctrl-model/crm-lead/mob-tab-exp-view9tabexppanel-tabexppanel-model';


/**
 * MobTabExpView9tabexppanel 部件服务对象
 *
 * @export
 * @class MobTabExpView9tabexppanelService
 * @extends {TabExpServiceBase}
 */
export class MobTabExpView9tabexppanelService extends TabExpServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobTabExpView9tabexppanelModel}
     * @memberof ControlServiceBase
     */
    protected model: MobTabExpView9tabexppanelModel = new MobTabExpView9tabexppanelModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabexppanelService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabexppanelService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobTabExpView9tabexppanelService;
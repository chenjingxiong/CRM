import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobMainTabViewModel } from '@/app-core/ctrl-model/crm-lead/mob-main-tab-view-portlet-model';


/**
 * MobMainTabView 部件服务对象
 *
 * @export
 * @class MobMainTabViewService
 * @extends {PortletServiceBase}
 */
export class MobMainTabViewService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobMainTabViewModel}
     * @memberof ControlServiceBase
     */
    protected model: MobMainTabViewModel = new MobMainTabViewModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobMainTabViewService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobMainTabViewService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobMainTabViewService;
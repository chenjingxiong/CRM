import { ChartServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobFunnelModel } from '@/app-core/ctrl-model/crm-lead/mob-funnel-chart-model';


/**
 * MobFunnel 部件服务对象
 *
 * @export
 * @class MobFunnelService
 * @extends {ChartServiceBase}
 */
export class MobFunnelService extends ChartServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobFunnelModel}
     * @memberof ControlServiceBase
     */
    protected model: MobFunnelModel = new MobFunnelModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobFunnelService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobFunnelService
     */
    protected appDeKey: string = 'id';

    /**
     * 生成图表数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof MobFunnelService
     */
    public handleResponse(action: string, response: any): any {
        if (response.status !== 200) {
            console.log(response.error.message);
            return;
        }
        let chartOption:any = {};
        let catalogFields: any = ["stage_id_text",];
        let valueFields: any = [[ "id", "ID" ],];
        let otherFields: any = ['planned_revenue'];
        // 数据按分类属性分组处理
        let xFields:any = [];
        let yFields:any = [];
        let oFields: any = [];
        valueFields.forEach((field: any,index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                let num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
                oFields[num] += item[otherFields[0]];
            }else{
                xFields.push(item[catalogFields[0]]);
                oFields.push(item[otherFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        let series: any = [];
        valueFields.forEach((field: any,index: number) => {
            let yData: any = [];
            xFields.forEach((item:any, num: number) => {
                yData.push({value: (100 - (100 / xFields.length) * num), name: item, sum: yFields[index][num], total: oFields[num]});
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
                name:field[1],
                type:"funnel",
                data:yData,
                minSize: '60%',
                left: 10,
                right: 10,
                label: {
                    position: 'center',
                    formatter: (params: any) => {
                        return `${params.data.name}（${params.data.sum}个, 金额￥${params.data.total}）`
                    }
                },
            });
        });
        chartOption.series = series;
        return new HttpResponse(response.status, chartOption);
    }

}
// 默认导出
export default MobFunnelService;

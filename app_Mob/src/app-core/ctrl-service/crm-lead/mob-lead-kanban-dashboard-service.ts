import { DashboardServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobLeadKanbanModel } from '@/app-core/ctrl-model/crm-lead/mob-lead-kanban-dashboard-model';


/**
 * MobLeadKanban 部件服务对象
 *
 * @export
 * @class MobLeadKanbanService
 * @extends {DashboardServiceBase}
 */
export class MobLeadKanbanService extends DashboardServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobLeadKanbanModel}
     * @memberof ControlServiceBase
     */
    protected model: MobLeadKanbanModel = new MobLeadKanbanModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobLeadKanbanService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobLeadKanbanService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobLeadKanbanService;
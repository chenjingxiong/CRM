import { DashboardServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobMainModel } from '@/app-core/ctrl-model/res-partner/mob-main-dashboard-model';


/**
 * MobMain 部件服务对象
 *
 * @export
 * @class MobMainService
 * @extends {DashboardServiceBase}
 */
export class MobMainService extends DashboardServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobMainModel}
     * @memberof ControlServiceBase
     */
    protected model: MobMainModel = new MobMainModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobMainService
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobMainService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobMainService;
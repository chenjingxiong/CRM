import { TabViewPanelServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobTabExpView9tabviewpanel2Model } from '@/app-core/ctrl-model/res-partner/mob-tab-exp-view9tabviewpanel2-tabviewpanel-model';


/**
 * MobTabExpView9tabviewpanel2 部件服务对象
 *
 * @export
 * @class MobTabExpView9tabviewpanel2Service
 * @extends {TabViewPanelServiceBase}
 */
export class MobTabExpView9tabviewpanel2Service extends TabViewPanelServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobTabExpView9tabviewpanel2Model}
     * @memberof ControlServiceBase
     */
    protected model: MobTabExpView9tabviewpanel2Model = new MobTabExpView9tabviewpanel2Model();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanel2Service
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanel2Service
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobTabExpView9tabviewpanel2Service;
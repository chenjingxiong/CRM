import { FormServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobMain2Model } from '@/app-core/ctrl-model/res-partner/mob-main2-form-model';


/**
 * MobMain2 部件服务对象
 *
 * @export
 * @class MobMain2Service
 * @extends {FormServiceBase}
 */
export class MobMain2Service extends FormServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobMain2Model}
     * @memberof ControlServiceBase
     */
    protected model: MobMain2Model = new MobMain2Model();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobMain2Service
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobMain2Service
     */
    protected appDeKey: string = 'id';

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} [context]
     * @param {*} [data]
     * @param {boolean} [isLoading]
     * @returns {Promise<any[]>}
     * @memberof  MobMain2Service
     */
    public async getItems(serviceName: string, interfaceName: string, context?: any, data?: any, isLoading?: boolean): Promise<any[]> {
        return [];
    }

    /**
     * 合并配置的默认值
     *
     * @protected
     * @param {*} [response={}]
     * @memberof MobMain2Service
     */
    public mergeDefaults(response:any = {}): void {
        if (response.data) {
        }
    }

}
// 默认导出
export default MobMain2Service;
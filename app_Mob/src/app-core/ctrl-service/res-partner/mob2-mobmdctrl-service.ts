import { MdServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { Mob2Model } from '@/app-core/ctrl-model/res-partner/mob2-mobmdctrl-model';


/**
 * Mob2 部件服务对象
 *
 * @export
 * @class Mob2Service
 * @extends {MdServiceBase}
 */
export class Mob2Service extends MdServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {Mob2Model}
     * @memberof ControlServiceBase
     */
    protected model: Mob2Model = new Mob2Model();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mob2Service
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof Mob2Service
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default Mob2Service;
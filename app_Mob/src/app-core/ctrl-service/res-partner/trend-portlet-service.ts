import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { TrendModel } from '@/app-core/ctrl-model/res-partner/trend-portlet-model';


/**
 * Trend 部件服务对象
 *
 * @export
 * @class TrendService
 * @extends {PortletServiceBase}
 */
export class TrendService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {TrendModel}
     * @memberof ControlServiceBase
     */
    protected model: TrendModel = new TrendModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof TrendService
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof TrendService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default TrendService;
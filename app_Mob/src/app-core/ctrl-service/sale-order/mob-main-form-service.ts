import { FormServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobMainModel } from '@/app-core/ctrl-model/sale-order/mob-main-form-model';


/**
 * MobMain 部件服务对象
 *
 * @export
 * @class MobMainService
 * @extends {FormServiceBase}
 */
export class MobMainService extends FormServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobMainModel}
     * @memberof ControlServiceBase
     */
    protected model: MobMainModel = new MobMainModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobMainService
     */
    protected appDEName: string = 'sale_order';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobMainService
     */
    protected appDeKey: string = 'id';

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} [context]
     * @param {*} [data]
     * @param {boolean} [isLoading]
     * @returns {Promise<any[]>}
     * @memberof  MobMainService
     */
    public async getItems(serviceName: string, interfaceName: string, context?: any, data?: any, isLoading?: boolean): Promise<any[]> {
        if (Object.is(serviceName, 'Res_partnerService') && Object.is(interfaceName, 'FetchDefault')) {
            const service: any = await this.getService('res_partner');
            await this.onBeforeAction(interfaceName, context, data, isLoading);
            const response: any = await service.FetchDefault(data);
            await this.onAfterAction(interfaceName, context, response);
            return this.doItems(response);
        }
        return [];
    }

    /**
     * 合并配置的默认值
     *
     * @protected
     * @param {*} [response={}]
     * @memberof MobMainService
     */
    public mergeDefaults(response:any = {}): void {
        if (response.data) {
        }
    }

}
// 默认导出
export default MobMainService;
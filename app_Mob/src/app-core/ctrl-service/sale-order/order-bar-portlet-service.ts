import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { OrderBarModel } from '@/app-core/ctrl-model/sale-order/order-bar-portlet-model';


/**
 * OrderBar 部件服务对象
 *
 * @export
 * @class OrderBarService
 * @extends {PortletServiceBase}
 */
export class OrderBarService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {OrderBarModel}
     * @memberof ControlServiceBase
     */
    protected model: OrderBarModel = new OrderBarModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OrderBarService
     */
    protected appDEName: string = 'sale_order';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof OrderBarService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default OrderBarService;
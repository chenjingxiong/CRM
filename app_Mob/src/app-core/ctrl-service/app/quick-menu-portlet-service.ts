import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { QuickMenuModel } from '@/app-core/ctrl-model/app/quick-menu-portlet-model';


/**
 * QuickMenu 部件服务对象
 *
 * @export
 * @class QuickMenuService
 * @extends {PortletServiceBase}
 */
export class QuickMenuService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {QuickMenuModel}
     * @memberof ControlServiceBase
     */
    protected model: QuickMenuModel = new QuickMenuModel();
}
// 默认导出
export default QuickMenuService;
import { AppMenuServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { QuickMenuModel } from '@/app-core/ctrl-model/app/quick-menu-appmenu-model';


/**
 * QuickMenu 部件服务对象
 *
 * @export
 * @class QuickMenuService
 * @extends {AppMenuServiceBase}
 */
export class QuickMenuService extends AppMenuServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {QuickMenuModel}
     * @memberof ControlServiceBase
     */
    protected model: QuickMenuModel = new QuickMenuModel();

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof QuickMenu
     */
    public get(params: any = {}): Promise<any> {
        return this.http.get('v7/quick-menuappmenu', params);
    }

}
// 默认导出
export default QuickMenuService;
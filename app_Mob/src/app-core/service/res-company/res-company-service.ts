import { Res_companyServiceBase } from './res-company-service-base';

/**
 * 公司服务对象
 *
 * @export
 * @class Res_companyService
 * @extends {Res_companyServiceBase}
 */
export class Res_companyService extends Res_companyServiceBase { }
// 默认导出
export default Res_companyService;
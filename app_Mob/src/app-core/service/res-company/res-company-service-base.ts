import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 公司服务对象基类
 *
 * @export
 * @class Res_companyServiceBase
 * @extends {EntityServiceBase}
 */
export class Res_companyServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_companyServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_companyServiceBase
     */
    protected readonly dePath: string = 'res_companies';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_companyServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Res_companyServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Res_companyServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('res_company');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/res_companies/${context.res_company}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/res_companies/${context.res_company}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/res_companies/getdraft`);
            res.data.res_company = context.res_company;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/res_companies/${context.res_company}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/res_companies/${context.res_company}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.put(`/res_companies/${context.res_company}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            data.res_company = null;
            const res: any = await this.http.post(`/res_companies`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/res_companies/${context.res_company}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/res_companies/${context.res_company}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_companyServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.get(`/res_companies/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}
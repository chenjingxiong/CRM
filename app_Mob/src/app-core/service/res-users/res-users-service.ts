import { Res_usersServiceBase } from './res-users-service-base';

/**
 * 用户服务对象
 *
 * @export
 * @class Res_usersService
 * @extends {Res_usersServiceBase}
 */
export class Res_usersService extends Res_usersServiceBase { }
// 默认导出
export default Res_usersService;
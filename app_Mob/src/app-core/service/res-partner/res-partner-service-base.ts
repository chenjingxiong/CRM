import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 客户服务对象基类
 *
 * @export
 * @class Res_partnerServiceBase
 * @extends {EntityServiceBase}
 */
export class Res_partnerServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_partnerServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_partnerServiceBase
     */
    protected readonly dePath: string = 'res_partners';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_partnerServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Res_partnerServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Res_partnerServiceBase
     */
    protected allMinorAppEntity: any = {
        'account_payments': {
            name: 'account_payment'
        },
        'sale_orders': {
            name: 'sale_order'
        },
        'crm_leads': {
            name: 'crm_lead'
        },
        'account_invoices': {
            name: 'account_invoice'
        },
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('res_partner');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/res_partners/${context.res_partner}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/res_partners/${context.res_partner}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/res_partners/${context.res_partner}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/res_partners/${context.res_partner}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/res_partners/${context.res_partner}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/res_partners/${context.res_partner}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/res_partners/${context.res_partner}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.put(`/res_partners/${context.res_partner}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/res_partners/getdraft`);
            res.data.res_partner = context.res_partner;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            data.res_partner = null;
            const res: any = await this.http.post(`/res_partners`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/res_partners/${context.res_partner}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchContacts接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchContacts(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.get(`/res_partners/fetchcontacts`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchCompany接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchCompany(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.get(`/res_partners/fetchcompany`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.get(`/res_partners/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}
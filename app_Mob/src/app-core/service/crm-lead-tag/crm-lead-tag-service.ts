import { Crm_lead_tagServiceBase } from './crm-lead-tag-service-base';

/**
 * 线索标签服务对象
 *
 * @export
 * @class Crm_lead_tagService
 * @extends {Crm_lead_tagServiceBase}
 */
export class Crm_lead_tagService extends Crm_lead_tagServiceBase { }
// 默认导出
export default Crm_lead_tagService;
import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 付款服务对象基类
 *
 * @export
 * @class Account_paymentServiceBase
 * @extends {EntityServiceBase}
 */
export class Account_paymentServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_paymentServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_paymentServiceBase
     */
    protected readonly dePath: string = 'account_payments';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_paymentServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Account_paymentServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Account_paymentServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('account_payment');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/select`, data);
            }
            return await this.http.post(`/account_payments/${context.account_payment}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/checkkey`, data);
            }
            return await this.http.post(`/account_payments/${context.account_payment}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.put(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/account_payments/${context.account_payment}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_payments/getdraft`);
            }
            const res: any = await this.http.get(`/account_payments/getdraft`);
            res.data.account_payment = context.account_payment;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.delete(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`);
            }
            return await this.http.delete(`/account_payments/${context.account_payment}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.delete(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/removebatch`);
            }
            return await this.http.delete(`/account_payments/${context.account_payment}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/save`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/account_payments/${context.account_payment}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.put(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/updatebatch`, data);
            }
            return await this.http.put(`/account_payments/${context.account_payment}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_payments`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            data.account_payment = null;
            const res: any = await this.http.post(`/account_payments`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}`);
            }
            const res: any = await this.http.get(`/account_payments/${context.account_payment}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_payment) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_payments/${context.account_payment}/createbatch`, data);
            }
            return await this.http.post(`/account_payments/${context.account_payment}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_paymentServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_payments/fetchdefault`, data);
            }
            return await this.http.get(`/account_payments/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}
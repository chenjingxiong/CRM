import { Crm_activity_reportServiceBase } from './crm-activity-report-service-base';

/**
 * CRM活动分析服务对象
 *
 * @export
 * @class Crm_activity_reportService
 * @extends {Crm_activity_reportServiceBase}
 */
export class Crm_activity_reportService extends Crm_activity_reportServiceBase { }
// 默认导出
export default Crm_activity_reportService;
import { Product_categoryServiceBase } from './product-category-service-base';

/**
 * 产品种类服务对象
 *
 * @export
 * @class Product_categoryService
 * @extends {Product_categoryServiceBase}
 */
export class Product_categoryService extends Product_categoryServiceBase { }
// 默认导出
export default Product_categoryService;
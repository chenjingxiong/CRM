import { Crm_lead2opportunity_partner_massServiceBase } from './crm-lead2opportunity-partner-mass-service-base';

/**
 * 转化线索为商机（批量）服务对象
 *
 * @export
 * @class Crm_lead2opportunity_partner_massService
 * @extends {Crm_lead2opportunity_partner_massServiceBase}
 */
export class Crm_lead2opportunity_partner_massService extends Crm_lead2opportunity_partner_massServiceBase { }
// 默认导出
export default Crm_lead2opportunity_partner_massService;
import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 活动类型服务对象基类
 *
 * @export
 * @class Mail_activity_typeServiceBase
 * @extends {EntityServiceBase}
 */
export class Mail_activity_typeServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Mail_activity_typeServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Mail_activity_typeServiceBase
     */
    protected readonly dePath: string = 'mail_activity_types';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Mail_activity_typeServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Mail_activity_typeServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Mail_activity_typeServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('mail_activity_type');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/mail_activity_types/${context.mail_activity_type}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/mail_activity_types/${context.mail_activity_type}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/mail_activity_types/${context.mail_activity_type}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.delete(`/mail_activity_types/${context.mail_activity_type}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/mail_activity_types/${context.mail_activity_type}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            const res: any = await this.http.get(`/mail_activity_types/getdraft`);
            res.data.mail_activity_type = context.mail_activity_type;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.post(`/mail_activity_types/${context.mail_activity_type}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            data.mail_activity_type = null;
            const res: any = await this.http.post(`/mail_activity_types`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.put(`/mail_activity_types/${context.mail_activity_type}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/mail_activity_types/${context.mail_activity_type}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/mail_activity_types/${context.mail_activity_type}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Mail_activity_typeServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            return await this.http.get(`/mail_activity_types/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}
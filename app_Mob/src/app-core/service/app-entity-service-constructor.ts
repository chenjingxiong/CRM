import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 应用实体服务
 *
 * @export
 * @class AppEntityServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export class AppEntityServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof AppEntityServiceConstructor
     */
    protected init(): void {
        this.allService.set('dynachart', () => import('@/app-core/service/dyna-chart/dyna-chart-service'));
        this.allService.set('crm_lost_reason', () => import('@/app-core/service/crm-lost-reason/crm-lost-reason-service'));
        this.allService.set('product_pricelist', () => import('@/app-core/service/product-pricelist/product-pricelist-service'));
        this.allService.set('product_price_list', () => import('@/app-core/service/product-price-list/product-price-list-service'));
        this.allService.set('crm_lead2opportunity_partner', () => import('@/app-core/service/crm-lead2opportunity-partner/crm-lead2opportunity-partner-service'));
        this.allService.set('mail_activity', () => import('@/app-core/service/mail-activity/mail-activity-service'));
        this.allService.set('crm_lead2opportunity_partner_mass', () => import('@/app-core/service/crm-lead2opportunity-partner-mass/crm-lead2opportunity-partner-mass-service'));
        this.allService.set('res_users', () => import('@/app-core/service/res-users/res-users-service'));
        this.allService.set('crm_stage', () => import('@/app-core/service/crm-stage/crm-stage-service'));
        this.allService.set('mail_message', () => import('@/app-core/service/mail-message/mail-message-service'));
        this.allService.set('res_partner', () => import('@/app-core/service/res-partner/res-partner-service'));
        this.allService.set('mail_activity_type', () => import('@/app-core/service/mail-activity-type/mail-activity-type-service'));
        this.allService.set('crm_partner_binding', () => import('@/app-core/service/crm-partner-binding/crm-partner-binding-service'));
        this.allService.set('crm_merge_opportunity', () => import('@/app-core/service/crm-merge-opportunity/crm-merge-opportunity-service'));
        this.allService.set('calendar_event', () => import('@/app-core/service/calendar-event/calendar-event-service'));
        this.allService.set('account_invoice_line', () => import('@/app-core/service/account-invoice-line/account-invoice-line-service'));
        this.allService.set('account_invoice', () => import('@/app-core/service/account-invoice/account-invoice-service'));
        this.allService.set('crm_lead_lost', () => import('@/app-core/service/crm-lead-lost/crm-lead-lost-service'));
        this.allService.set('uom_uom', () => import('@/app-core/service/uom-uom/uom-uom-service'));
        this.allService.set('crm_team', () => import('@/app-core/service/crm-team/crm-team-service'));
        this.allService.set('account_journal', () => import('@/app-core/service/account-journal/account-journal-service'));
        this.allService.set('account_register_payments', () => import('@/app-core/service/account-register-payments/account-register-payments-service'));
        this.allService.set('dynadashboard', () => import('@/app-core/service/dyna-dashboard/dyna-dashboard-service'));
        this.allService.set('product_category', () => import('@/app-core/service/product-category/product-category-service'));
        this.allService.set('crm_lead_tag', () => import('@/app-core/service/crm-lead-tag/crm-lead-tag-service'));
        this.allService.set('crm_lead', () => import('@/app-core/service/crm-lead/crm-lead-service'));
        this.allService.set('crm_activity_report', () => import('@/app-core/service/crm-activity-report/crm-activity-report-service'));
        this.allService.set('sale_order_line', () => import('@/app-core/service/sale-order-line/sale-order-line-service'));
        this.allService.set('product_pricelist_item', () => import('@/app-core/service/product-pricelist-item/product-pricelist-item-service'));
        this.allService.set('res_company', () => import('@/app-core/service/res-company/res-company-service'));
        this.allService.set('product_product', () => import('@/app-core/service/product-product/product-product-service'));
        this.allService.set('sale_order', () => import('@/app-core/service/sale-order/sale-order-service'));
        this.allService.set('account_payment', () => import('@/app-core/service/account-payment/account-payment-service'));
    }

}

/**
 * 应用实体服务构造器
 */
export const appEntityServiceConstructor: AppEntityServiceConstructor = new AppEntityServiceConstructor();
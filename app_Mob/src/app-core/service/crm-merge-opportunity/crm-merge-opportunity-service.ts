import { Crm_merge_opportunityServiceBase } from './crm-merge-opportunity-service-base';

/**
 * 合并商机服务对象
 *
 * @export
 * @class Crm_merge_opportunityService
 * @extends {Crm_merge_opportunityServiceBase}
 */
export class Crm_merge_opportunityService extends Crm_merge_opportunityServiceBase { }
// 默认导出
export default Crm_merge_opportunityService;
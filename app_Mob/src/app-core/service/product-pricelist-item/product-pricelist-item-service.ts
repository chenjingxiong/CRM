import { Product_pricelist_itemServiceBase } from './product-pricelist-item-service-base';

/**
 * 价格表明细服务对象
 *
 * @export
 * @class Product_pricelist_itemService
 * @extends {Product_pricelist_itemServiceBase}
 */
export class Product_pricelist_itemService extends Product_pricelist_itemServiceBase { }
// 默认导出
export default Product_pricelist_itemService;
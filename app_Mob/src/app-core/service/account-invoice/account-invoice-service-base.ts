import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 发票服务对象基类
 *
 * @export
 * @class Account_invoiceServiceBase
 * @extends {EntityServiceBase}
 */
export class Account_invoiceServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_invoiceServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_invoiceServiceBase
     */
    protected readonly dePath: string = 'account_invoices';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Account_invoiceServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Account_invoiceServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Account_invoiceServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('account_invoice');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/select`, data);
            }
            return await this.http.post(`/account_invoices/${context.account_invoice}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.delete(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`);
            }
            return await this.http.delete(`/account_invoices/${context.account_invoice}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/checkkey`, data);
            }
            return await this.http.post(`/account_invoices/${context.account_invoice}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_invoices`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            data.account_invoice = null;
            const res: any = await this.http.post(`/account_invoices`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.put(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/account_invoices/${context.account_invoice}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.put(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/updatebatch`, data);
            }
            return await this.http.put(`/account_invoices/${context.account_invoice}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`);
            }
            const res: any = await this.http.get(`/account_invoices/${context.account_invoice}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/createbatch`, data);
            }
            return await this.http.post(`/account_invoices/${context.account_invoice}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_invoices/getdraft`);
            }
            const res: any = await this.http.get(`/account_invoices/getdraft`);
            res.data.account_invoice = context.account_invoice;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/save`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/account_invoices/${context.account_invoice}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.account_invoice) {
                return await this.http.delete(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/removebatch`);
            }
            return await this.http.delete(`/account_invoices/${context.account_invoice}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Account_invoiceServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/account_invoices/fetchdefault`, data);
            }
            return await this.http.get(`/account_invoices/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}
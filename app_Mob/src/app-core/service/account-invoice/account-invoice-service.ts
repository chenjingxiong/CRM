import { Account_invoiceServiceBase } from './account-invoice-service-base';

/**
 * 发票服务对象
 *
 * @export
 * @class Account_invoiceService
 * @extends {Account_invoiceServiceBase}
 */
export class Account_invoiceService extends Account_invoiceServiceBase { }
// 默认导出
export default Account_invoiceService;
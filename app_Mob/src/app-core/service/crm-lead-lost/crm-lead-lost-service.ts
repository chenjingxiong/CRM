import { Crm_lead_lostServiceBase } from './crm-lead-lost-service-base';

/**
 * 获取丢失原因服务对象
 *
 * @export
 * @class Crm_lead_lostService
 * @extends {Crm_lead_lostServiceBase}
 */
export class Crm_lead_lostService extends Crm_lead_lostServiceBase { }
// 默认导出
export default Crm_lead_lostService;
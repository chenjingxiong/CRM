import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 计数器服务注册中心
 *
 * @export
 * @class CounterServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export class CounterServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof CounterServiceConstructor
     */
    protected init(): void {
        this.allService.set('portal', () => import('@/app-core/counter/portal/portal-counter'));
        this.allService.set('partnercounter', () => import('@/app-core/counter/partner-counter/partner-counter-counter'));
    }

}
/**
 * 计数器服务构造器
 */
export const counterServiceConstructor: CounterServiceConstructor = new CounterServiceConstructor();
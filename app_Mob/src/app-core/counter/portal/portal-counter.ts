import { PortalCounterServiceBase } from './portal-counter-base';

/**
 * 门户界面计数器计数器服务对象
 *
 * @export
 * @class PortalCounterService
 * @extends {PortalCounterServiceBase}
 */
export class PortalCounterService extends PortalCounterServiceBase { }
// 默认导出
export default PortalCounterService;
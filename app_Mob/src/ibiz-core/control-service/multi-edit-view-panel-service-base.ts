import { ControlServiceBase } from './control-service-base';

/**
 * 多编辑视图面板部件服务基类
 *
 * @export
 * @class MultiEditViewPanelServiceBase
 * @extends {ControlServiceBase}
 */
export class MultiEditViewPanelServiceBase extends ControlServiceBase {

}
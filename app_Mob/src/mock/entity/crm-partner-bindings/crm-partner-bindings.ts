import { MockAdapter } from '@/mock/mock-adapter';
import { Util } from '@/ibiz-core/utils';
const mock = MockAdapter.getInstance();

// 模拟数据
const mockDatas: Array<any> = [
];

/**
 * 解析参数
 * 
 * @param paramArray 参数数组
 * @param regStr 路径正则
 * @param url 访问路径
 */
const parsingParameters = (paramArray: Array<string>, regStr: RegExp, url: string): any => {
    let params: any = {};
    const matchArray: any = new RegExp(regStr).exec(url);
    if (matchArray && matchArray.length > 1 && paramArray && paramArray.length > 0) {
        paramArray.forEach((item: any, index: number) => {
            Object.defineProperty(params, item, {
                enumerable: true,
                value: matchArray[index + 1],
            });
        });
    }
    return params;
};

/**
 * 排队路径参数是否存在 null、undefined 或者空字符串等异常情况 
 * 
 * @param paramArray 
 * @param params 
 */
const judgmentParametersException = (paramArray: Array<string>, params: any): boolean => {
    let isParametersException = false;
    if (paramArray.length > 0) {
        let state = paramArray.some((key: string) => {
            let _value = params[key];
            if (!_value || Object.is(_value, '')) {
                return true;
            }
            return false;
        });
        if (state) {
            isParametersException = true;;
        }
    }
    return isParametersException;
};



//UpdateBatch
mock.onPut(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/updatebatch$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/updatebatch$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    return [status, {
    }];
});

//GetDraft
mock.onGet(new RegExp(/^\/crm_partner_bindings\/getdraft$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = [];
    let regStr: RegExp = /^\/crm_partner_bindings\/getdraft$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    return [status, {}, config.headers, config];
});

//Get
mock.onGet(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    // 遍历键值，获取数据
    let items: Array<any> = [];
    items = Util.deepCopy(mockDatas);
    let state2 = Object.keys(params).some((key: string) => {
        let _value = params[key];
        let _items = items.filter((data: any) => Object.is(data[key], _value));
        if (_items.length === 0) {
            return true;
        }
        items = [];
        items = Util.deepCopy(_items);
        return false;
    });
    if (state2) {
        return [403, null, config.headers, config];
    }
    return [status, items[0], config.headers, config];
});

//CreateBatch
mock.onPost(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/createbatch$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/createbatch$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    return [status, {
    }];
});

//RemoveBatch
mock.onDelete(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/removebatch$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})\/removebatch$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    return [status, {
    }];
});

//Update
mock.onPut(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    // 遍历键值，获取数据
    let items: Array<any> = [];
    items = Util.deepCopy(mockDatas);
    let state2 = Object.keys(params).some((key: string) => {
        let _value = params[key];
        let _items = items.filter((data: any) => Object.is(data[key], _value));
        if (_items.length === 0) {
            return true;
        }
        items = [];
        items = Util.deepCopy(_items);
        return false;
    });
    if (state2) {
        return [403, null, config.headers, config];
    }

    let et_id = Object.keys(params)[Object.keys(params).length - 1]
    let et_id_value = params[et_id];
    let index = mockDatas.findIndex((data: any) => Object.is(data[et_id], et_id_value));

    let data: any = JSON.parse(config.data);
    Object.keys(mockDatas[index]).forEach((key: string) => {
        if (data.hasOwnProperty(key)) {
            Object.assign(mockDatas[index], { [key]: data[key] })
        }
    });

    return [status, mockDatas[index], config.headers, config];
});

//Create
mock.onPost(new RegExp(/^\/crm_partner_bindings$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 
    let paramArray: Array<string> = [];
    let regStr: RegExp = /^\/crm_partner_bindings$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    let data: any = JSON.parse(config.data);
    mockDatas.push(Object.assign({}, data, { crm_partner_bindingid: Util.createUUID() }))

    return [status, mockDatas[mockDatas.length - 1], config.headers, config];
});

//Remove
mock.onDelete(new RegExp(/^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 'crm_partner_bindingid'
    let paramArray: Array<string> = ['crm_partner_bindingid'];
    let regStr: RegExp = /^\/crm_partner_bindings\/([a-zA-Z0-9\-\;]{1,35})$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }

    let et_id = Object.keys(params)[Object.keys(params).length - 1]
    let et_id_value = params[et_id];
    let index = mockDatas.findIndex((data: any) => Object.is(data[et_id], et_id_value));
    mockDatas.splice(index, 1);

    return [status, null, config.headers, config];
});

//fetchDefault
mock.onGet(new RegExp(/^\/crm_partner_bindings\/fetchdefault(\?[\w-./?%&=]*)*$/)).reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    if (status !== 200) {
        return [status, null];
    }
    // 
    let paramArray: Array<string> = [];
    let regStr: RegExp = /^\/crm_partner_bindings\/fetchdefault(\?[\w-./?%&=]*)*$/;
    let url = config.url;
    let params = parsingParameters(paramArray, regStr, url);
    console.log(params);

    let state = judgmentParametersException(paramArray, params);
    if (state) {
        return [403, null, config.headers, config];
    }
    
    let { headers } = config;
    let xTotal = mockDatas.length;
    let records: Array<any> = [];
    if (config.url.includes('page')) {
        let url = config.url;
        let xPage = url.split('page=')[1];
        let xPerPage = url.split('size=')[1].split('&')[0];
        Object.assign(headers, { 'x-page': xPage + '', 'x-per-page': xPerPage + '', 'x-total': xTotal + '' });
        let start = xTotal - (xPage * xPerPage);
        records = mockDatas.slice(xPage * xPerPage, xPerPage);
    } else {
        records = [...mockDatas];
    }
    return [status, records, config.headers, config];
});



import ViewEngine from './view-engine';

/**
 * 实体移动端编辑视图界面引擎
 *
 * @export
 * @class MobEditViewEngine
 * @extends {ViewEngine}
 */
export default class MobEditViewEngine extends ViewEngine {

    /**
     * 表单部件
     *
     * @protected
     * @type {*}
     * @memberof MobEditViewEngine
     */
    protected form: any;

    /**
     * 初始化编辑视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobEditViewEngine
     */
    public init(options: any = {}): void {
        this.form = options.form;
        super.init(options);
    }

    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobEditViewEngine
     */
    public load(opts: any = {}): void {
        super.load(opts);
        if (this.getForm()) {
            const tag = this.getForm().name;
            let action: string = '';
            // 实体主键字段有值时load该记录数据，否则loaddraft加载草稿
            if ((this.keyPSDEField && this.view.context[this.keyPSDEField] && !Object.is(this.view.context[this.keyPSDEField], ''))) {
                action = 'load';
            } else {
                action = 'loaddraft';
            }
            this.setViewState2({ tag: tag, action: action, viewdata: { ...this.view.viewparams } });
        }
    }

    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
        super.onCtrlEvent(ctrlName, eventName, args);
        if (Object.is(ctrlName, 'form')) {
            this.formEvent(eventName, args);
        }
    }

    /**
     * 表单事件
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    public formEvent(eventName: string, args: any): void {
        if (Object.is(eventName, 'load')) {
            this.onFormLoad(args);
        }
        if (Object.is(eventName, 'save')) {
            this.onFormSave(args);
        }
        if (Object.is(eventName, 'remove')) {
            this.onFormRemove(args);
        }
    }

    /**
     * 表单加载完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    public onFormLoad(arg: any): void {
        this.view.$emit('load', arg);
        this.view.$emit('viewdataschange', JSON.stringify({ action: 'load', status: 'success', data: arg }));
    }

    /**
     * 表单保存完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    public onFormSave(arg: any): void {
        this.view.$emit('save', arg);
        this.view.$emit('viewdataschange', JSON.stringify({ action: 'save', status: 'success', data: arg }));
    }

    /**
     * 表单删除完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    public onFormRemove(arg: any): void {
        this.view.$emit('remove', arg);
        this.view.$emit('viewdataschange', JSON.stringify({ action: 'remove', status: 'success', data: arg }));
    }

    /**
     * 获取表单对象
     *
     * @returns {*}
     * @memberof MobEditViewEngine
     */
    public getForm(): any {
        return this.form;
    }

}

export default {
	views: {
		mobeditview: {
			caption: '销售订单',
		},
		mobmdview: {
			caption: '销售订单',
		},
	},
	mobmain_form: {
		details: {
			druipart1: '', 
			grouppanel1: '订单行', 
			group1: '基本信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '订单关联', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			client_order_ref: '报价单号', 
			partner_id_text: '客户', 
			date_order: '单据日期', 
			state: '状态', 
			amount_total: '总计', 
			order_line: '订单行', 
			id: 'ID', 
			partner_id: '客户', 
		},
		uiactions: {
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
	},
};
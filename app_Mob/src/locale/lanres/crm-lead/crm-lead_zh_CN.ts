export default {
	views: {
		mobtabexpview9: {
			caption: '线索/商机',
		},
		mobmaindashboardview: {
			caption: '线索/商机',
		},
		mobpanelview9: {
			caption: '线索/商机',
		},
		mobmdview9: {
			caption: '线索/商机',
		},
		mobmainview9: {
			caption: '线索/商机',
		},
		mobeditview: {
			caption: '线索/商机',
		},
		mobmdview: {
			caption: '线索/商机',
		},
	},
	mobmain_form: {
		details: {
			group1: '线索/商机', 
			group2: '市场', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '商机', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '商机', 
			stage_id_text: '阶段', 
			planned_revenue: '预期收益', 
			probability: '概率', 
			partner_name: '客户名称', 
			date_deadline: '预期结束', 
			priority: '优先级', 
			campaign_id_text: '营销', 
			medium_id_text: '媒介', 
			source_id_text: '来源', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '保存',
			tip: '保存',
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '新建',
			tip: '新建',
		},
	},
};

export default {
	views: {
		mobpickupview: {
			caption: '客户',
		},
		mobmaindashboardview: {
			caption: '客户',
		},
		mobmdview: {
			caption: '客户',
		},
		mobeditview: {
			caption: '客户',
		},
		mobmainview9_editmode: {
			caption: '客户',
		},
		mobtabexpview9: {
			caption: '客户',
		},
		mobmainview9: {
			caption: '客户',
		},
		mobtabexpview: {
			caption: '客户',
		},
		mobpickupmdview: {
			caption: '客户',
		},
	},
	mobmain2_form: {
		details: {
			group1: '客户基本信息', 
			grouppanel1: '地址', 
			grouppanel2: '联系方式', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			country_id_text: '国家/地区', 
			state_id_text: '省/ 州', 
			city: '城市', 
			zip: '邮政编码', 
			street: '街道', 
			street2: '街道 2', 
			phone: '电话', 
			mobile: '手机', 
			email: 'EMail', 
			id: 'ID', 
			state_id: '省/ 州', 
			country_id: '国家/地区', 
		},
		uiactions: {
		},
	},
	mobmain_form: {
		details: {
			group1: '客户名称', 
			grouppanel2: '联系方式', 
			grouppanel1: '更多信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			country_id_text: '国家/地区', 
			state_id_text: '省/ 州', 
			city: '城市', 
			zip: '邮政编码', 
			street: '街道', 
			street2: '街道 2', 
			phone: '电话', 
			mobile: '手机', 
			email: 'EMail', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobmainview9_editmoderighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
		tbitem2: {
			caption: '关闭',
			tip: 'tbitem2',
		},
	},
	mobmaindashboardviewtoolbar_toolbar: {
		deuiaction1: {
			caption: '编辑',
			tip: 'deuiaction1',
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
	},
};
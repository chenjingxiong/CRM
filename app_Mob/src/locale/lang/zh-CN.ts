import dynachart_zh_CN from '@locale/lanres/dyna-chart/dyna-chart_zh_CN';
import crm_lost_reason_zh_CN from '@locale/lanres/crm-lost-reason/crm-lost-reason_zh_CN';
import product_pricelist_zh_CN from '@locale/lanres/product-pricelist/product-pricelist_zh_CN';
import product_price_list_zh_CN from '@locale/lanres/product-price-list/product-price-list_zh_CN';
import crm_lead2opportunity_partner_zh_CN from '@locale/lanres/crm-lead2opportunity-partner/crm-lead2opportunity-partner_zh_CN';
import mail_activity_zh_CN from '@locale/lanres/mail-activity/mail-activity_zh_CN';
import crm_lead2opportunity_partner_mass_zh_CN from '@locale/lanres/crm-lead2opportunity-partner-mass/crm-lead2opportunity-partner-mass_zh_CN';
import res_users_zh_CN from '@locale/lanres/res-users/res-users_zh_CN';
import crm_stage_zh_CN from '@locale/lanres/crm-stage/crm-stage_zh_CN';
import mail_message_zh_CN from '@locale/lanres/mail-message/mail-message_zh_CN';
import res_partner_zh_CN from '@locale/lanres/res-partner/res-partner_zh_CN';
import mail_activity_type_zh_CN from '@locale/lanres/mail-activity-type/mail-activity-type_zh_CN';
import crm_partner_binding_zh_CN from '@locale/lanres/crm-partner-binding/crm-partner-binding_zh_CN';
import crm_merge_opportunity_zh_CN from '@locale/lanres/crm-merge-opportunity/crm-merge-opportunity_zh_CN';
import calendar_event_zh_CN from '@locale/lanres/calendar-event/calendar-event_zh_CN';
import account_invoice_line_zh_CN from '@locale/lanres/account-invoice-line/account-invoice-line_zh_CN';
import account_invoice_zh_CN from '@locale/lanres/account-invoice/account-invoice_zh_CN';
import crm_lead_lost_zh_CN from '@locale/lanres/crm-lead-lost/crm-lead-lost_zh_CN';
import uom_uom_zh_CN from '@locale/lanres/uom-uom/uom-uom_zh_CN';
import crm_team_zh_CN from '@locale/lanres/crm-team/crm-team_zh_CN';
import account_journal_zh_CN from '@locale/lanres/account-journal/account-journal_zh_CN';
import account_register_payments_zh_CN from '@locale/lanres/account-register-payments/account-register-payments_zh_CN';
import dynadashboard_zh_CN from '@locale/lanres/dyna-dashboard/dyna-dashboard_zh_CN';
import product_category_zh_CN from '@locale/lanres/product-category/product-category_zh_CN';
import crm_lead_tag_zh_CN from '@locale/lanres/crm-lead-tag/crm-lead-tag_zh_CN';
import crm_lead_zh_CN from '@locale/lanres/crm-lead/crm-lead_zh_CN';
import crm_activity_report_zh_CN from '@locale/lanres/crm-activity-report/crm-activity-report_zh_CN';
import sale_order_line_zh_CN from '@locale/lanres/sale-order-line/sale-order-line_zh_CN';
import product_pricelist_item_zh_CN from '@locale/lanres/product-pricelist-item/product-pricelist-item_zh_CN';
import res_company_zh_CN from '@locale/lanres/res-company/res-company_zh_CN';
import product_product_zh_CN from '@locale/lanres/product-product/product-product_zh_CN';
import sale_order_zh_CN from '@locale/lanres/sale-order/sale-order_zh_CN';
import account_payment_zh_CN from '@locale/lanres/account-payment/account-payment_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

export default {
    app: {
        gridpage: {
            choicecolumns: '选择列',
            refresh: '刷新',
            show: '显示',
            records: '条',
            totle: '共',
        },
        tabpage: {
            sureclosetip: {
                title: '关闭提醒',
                content: '表单数据已经修改，确定要关闭？',
            },
            closeall: '关闭所有',
            closeother: '关闭其他',
        },
        fileUpload: {
            caption: '上传',
        },
        searchButton: {
            search: '搜索',
            reset: '重置',
        },
        // 非实体视图
        views: {
            appportalview: {
                caption: '工作台',
            },
            appindexview: {
                caption: 'CRM标准系统',
            },
        },
        menus: {
            appindexview: {
                menuitem3: '工作台',
                menuitem4: '活动',
                menuitem5: '我的',
            },
            quickmenu: {
                menuitem1: '客户信息',
                menuitem2: '商机',
                menuitem4: '账单',
                menuitem3: '开票单',
                menuitem7: '合同',
                menuitem5: '付款登记',
                menuitem6: '产品种类',
            },
        },
        components: {
            app_icon_menu: {
                statusValue_open: '展开',
                statusValue_close: '收回',
            }
        },

        button: {
            cancel: '取消',
            confirm: '确认',
            back: '返回',
            loadmore: '加载更多'
        },
        loadding: '加载中',
        fastsearch: '快速搜索',
        pulling_text: '下拉刷新',
        statusMessage:{
            200: '服务器成功返回请求的数据。',
            201: '新建或修改数据成功。',
            202: '一个请求已经进入后台排队（异步任务）。',
            204: '删除数据成功。',
            400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
            401: '用户没有权限（令牌、用户名、密码错误）。',
            403: '用户得到授权，但是访问是被禁止的。',
            404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
            405: '方法不被允许',
            406: '请求的格式不可得。',
            410: '请求的资源被永久删除，且不会再得到的。',
            422: '当创建一个对象时，发生一个验证错误。',
            500: '服务器发生错误，请检查服务器。',
            502: '网关错误。',
            503: '服务不可用，服务器暂时过载或维护。',
            504: '网关超时。',
        },
        errorMessage: {
            100: '未知',
            101: '请求发生错误',
            5001: '数据不存在',
            5002: '数据已存在，无法重复创建',
            5003: '新建失败',
            5004: '数据不存在，无法保存',
            5005: '数据删除失败'
        }
    },
    dynachart: dynachart_zh_CN,
    crm_lost_reason: crm_lost_reason_zh_CN,
    product_pricelist: product_pricelist_zh_CN,
    product_price_list: product_price_list_zh_CN,
    crm_lead2opportunity_partner: crm_lead2opportunity_partner_zh_CN,
    mail_activity: mail_activity_zh_CN,
    crm_lead2opportunity_partner_mass: crm_lead2opportunity_partner_mass_zh_CN,
    res_users: res_users_zh_CN,
    crm_stage: crm_stage_zh_CN,
    mail_message: mail_message_zh_CN,
    res_partner: res_partner_zh_CN,
    mail_activity_type: mail_activity_type_zh_CN,
    crm_partner_binding: crm_partner_binding_zh_CN,
    crm_merge_opportunity: crm_merge_opportunity_zh_CN,
    calendar_event: calendar_event_zh_CN,
    account_invoice_line: account_invoice_line_zh_CN,
    account_invoice: account_invoice_zh_CN,
    crm_lead_lost: crm_lead_lost_zh_CN,
    uom_uom: uom_uom_zh_CN,
    crm_team: crm_team_zh_CN,
    account_journal: account_journal_zh_CN,
    account_register_payments: account_register_payments_zh_CN,
    dynadashboard: dynadashboard_zh_CN,
    product_category: product_category_zh_CN,
    crm_lead_tag: crm_lead_tag_zh_CN,
    crm_lead: crm_lead_zh_CN,
    crm_activity_report: crm_activity_report_zh_CN,
    sale_order_line: sale_order_line_zh_CN,
    product_pricelist_item: product_pricelist_item_zh_CN,
    res_company: res_company_zh_CN,
    product_product: product_product_zh_CN,
    sale_order: sale_order_zh_CN,
    account_payment: account_payment_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN
};
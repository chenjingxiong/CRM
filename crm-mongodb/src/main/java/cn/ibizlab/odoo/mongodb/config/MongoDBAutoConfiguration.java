package cn.ibizlab.odoo.mongodb.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(MongoDBConfiguration.class)
@ConditionalOnWebApplication
public class MongoDBAutoConfiguration {

	
}

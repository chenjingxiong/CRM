package cn.ibizlab.odoo.mongodb.util;

import org.springframework.core.convert.converter.Converter;
import java.sql.Timestamp;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TimestampConverter implements Converter<Date, Timestamp> {

	public Timestamp convert(Date date) {
		if (date != null) {
			return new Timestamp(date.getTime());
		}
		return null;
	}
}

package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_merge_opportunityService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;




@Slf4j
@Api(tags = {"Crm_merge_opportunity" })
@RestController("WebApi-crm_merge_opportunity")
@RequestMapping("")
public class Crm_merge_opportunityResource {

    @Autowired
    private ICrm_merge_opportunityService crm_merge_opportunityService;

    @Autowired
    @Lazy
    private Crm_merge_opportunityMapping crm_merge_opportunityMapping;







    @ApiOperation(value = "获取草稿数据", tags = {"Crm_merge_opportunity" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/getdraft")
    public ResponseEntity<Crm_merge_opportunityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityMapping.toDto(crm_merge_opportunityService.getDraft(new Crm_merge_opportunity())));
    }




    @PreAuthorize("hasPermission(#crm_merge_opportunity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_merge_opportunity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")

    public ResponseEntity<Crm_merge_opportunityDTO> update(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
		Crm_merge_opportunity domain = crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto);
        domain.setId(crm_merge_opportunity_id);
		crm_merge_opportunityService.update(domain);
		Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_merge_opportunity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_merge_opportunity" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        crm_merge_opportunityService.updateBatch(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_merge_opportunity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities")

    public ResponseEntity<Crm_merge_opportunityDTO> create(@RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        Crm_merge_opportunity domain = crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto);
		crm_merge_opportunityService.create(domain);
        Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_merge_opportunity" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        crm_merge_opportunityService.createBatch(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#crm_merge_opportunity_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_merge_opportunity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityService.remove(crm_merge_opportunity_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_merge_opportunity" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_merge_opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#crm_merge_opportunity_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_merge_opportunity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public ResponseEntity<Crm_merge_opportunityDTO> get(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) {
        Crm_merge_opportunity domain = crm_merge_opportunityService.get(crm_merge_opportunity_id);
        Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_merge_opportunity-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_merge_opportunity" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_merge_opportunities/fetchdefault")
	public ResponseEntity<List<Crm_merge_opportunityDTO>> fetchDefault(Crm_merge_opportunitySearchContext context) {
        Page<Crm_merge_opportunity> domains = crm_merge_opportunityService.searchDefault(context) ;
        List<Crm_merge_opportunityDTO> list = crm_merge_opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_merge_opportunity-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_merge_opportunity" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_merge_opportunities/searchdefault")
	public ResponseEntity<Page<Crm_merge_opportunityDTO>> searchDefault(@RequestBody Crm_merge_opportunitySearchContext context) {
        Page<Crm_merge_opportunity> domains = crm_merge_opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_merge_opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_merge_opportunity getEntity(){
        return new Crm_merge_opportunity();
    }

}

package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Payment_acquirerDTO]
 */
@Data
public class Payment_acquirerDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FEES_IMPLEMENTED]
     *
     */
    @JSONField(name = "fees_implemented")
    @JsonProperty("fees_implemented")
    private String feesImplemented;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [DONE_MSG]
     *
     */
    @JSONField(name = "done_msg")
    @JsonProperty("done_msg")
    private String doneMsg;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PAYMENT_ICON_IDS]
     *
     */
    @JSONField(name = "payment_icon_ids")
    @JsonProperty("payment_icon_ids")
    private String paymentIconIds;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [FEES_INT_VAR]
     *
     */
    @JSONField(name = "fees_int_var")
    @JsonProperty("fees_int_var")
    private Double feesIntVar;

    /**
     * 属性 [FEES_ACTIVE]
     *
     */
    @JSONField(name = "fees_active")
    @JsonProperty("fees_active")
    private String feesActive;

    /**
     * 属性 [REGISTRATION_VIEW_TEMPLATE_ID]
     *
     */
    @JSONField(name = "registration_view_template_id")
    @JsonProperty("registration_view_template_id")
    private Integer registrationViewTemplateId;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [POST_MSG]
     *
     */
    @JSONField(name = "post_msg")
    @JsonProperty("post_msg")
    private String postMsg;

    /**
     * 属性 [ENVIRONMENT]
     *
     */
    @JSONField(name = "environment")
    @JsonProperty("environment")
    private String environment;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [TOKEN_IMPLEMENTED]
     *
     */
    @JSONField(name = "token_implemented")
    @JsonProperty("token_implemented")
    private String tokenImplemented;

    /**
     * 属性 [FEES_DOM_FIXED]
     *
     */
    @JSONField(name = "fees_dom_fixed")
    @JsonProperty("fees_dom_fixed")
    private Double feesDomFixed;

    /**
     * 属性 [FEES_INT_FIXED]
     *
     */
    @JSONField(name = "fees_int_fixed")
    @JsonProperty("fees_int_fixed")
    private Double feesIntFixed;

    /**
     * 属性 [MODULE_ID]
     *
     */
    @JSONField(name = "module_id")
    @JsonProperty("module_id")
    private Integer moduleId;

    /**
     * 属性 [AUTHORIZE_IMPLEMENTED]
     *
     */
    @JSONField(name = "authorize_implemented")
    @JsonProperty("authorize_implemented")
    private String authorizeImplemented;

    /**
     * 属性 [PAYMENT_FLOW]
     *
     */
    @JSONField(name = "payment_flow")
    @JsonProperty("payment_flow")
    private String paymentFlow;

    /**
     * 属性 [SPECIFIC_COUNTRIES]
     *
     */
    @JSONField(name = "specific_countries")
    @JsonProperty("specific_countries")
    private String specificCountries;

    /**
     * 属性 [FEES_DOM_VAR]
     *
     */
    @JSONField(name = "fees_dom_var")
    @JsonProperty("fees_dom_var")
    private Double feesDomVar;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PRE_MSG]
     *
     */
    @JSONField(name = "pre_msg")
    @JsonProperty("pre_msg")
    private String preMsg;

    /**
     * 属性 [COUNTRY_IDS]
     *
     */
    @JSONField(name = "country_ids")
    @JsonProperty("country_ids")
    private String countryIds;

    /**
     * 属性 [QR_CODE]
     *
     */
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 属性 [PENDING_MSG]
     *
     */
    @JSONField(name = "pending_msg")
    @JsonProperty("pending_msg")
    private String pendingMsg;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [VIEW_TEMPLATE_ID]
     *
     */
    @JSONField(name = "view_template_id")
    @JsonProperty("view_template_id")
    private Integer viewTemplateId;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;

    /**
     * 属性 [SO_REFERENCE_TYPE]
     *
     */
    @JSONField(name = "so_reference_type")
    @JsonProperty("so_reference_type")
    private String soReferenceType;

    /**
     * 属性 [MODULE_STATE]
     *
     */
    @JSONField(name = "module_state")
    @JsonProperty("module_state")
    private String moduleState;

    /**
     * 属性 [CANCEL_MSG]
     *
     */
    @JSONField(name = "cancel_msg")
    @JsonProperty("cancel_msg")
    private String cancelMsg;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PROVIDER]
     *
     */
    @JSONField(name = "provider")
    @JsonProperty("provider")
    private String provider;

    /**
     * 属性 [SAVE_TOKEN]
     *
     */
    @JSONField(name = "save_token")
    @JsonProperty("save_token")
    private String saveToken;

    /**
     * 属性 [CAPTURE_MANUALLY]
     *
     */
    @JSONField(name = "capture_manually")
    @JsonProperty("capture_manually")
    private String captureManually;

    /**
     * 属性 [ERROR_MSG]
     *
     */
    @JSONField(name = "error_msg")
    @JsonProperty("error_msg")
    private String errorMsg;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [DONE_MSG]
     */
    public void setDoneMsg(String  doneMsg){
        this.doneMsg = doneMsg ;
        this.modify("done_msg",doneMsg);
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    public void setWebsitePublished(String  websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }

    /**
     * 设置 [FEES_INT_VAR]
     */
    public void setFeesIntVar(Double  feesIntVar){
        this.feesIntVar = feesIntVar ;
        this.modify("fees_int_var",feesIntVar);
    }

    /**
     * 设置 [FEES_ACTIVE]
     */
    public void setFeesActive(String  feesActive){
        this.feesActive = feesActive ;
        this.modify("fees_active",feesActive);
    }

    /**
     * 设置 [REGISTRATION_VIEW_TEMPLATE_ID]
     */
    public void setRegistrationViewTemplateId(Integer  registrationViewTemplateId){
        this.registrationViewTemplateId = registrationViewTemplateId ;
        this.modify("registration_view_template_id",registrationViewTemplateId);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [POST_MSG]
     */
    public void setPostMsg(String  postMsg){
        this.postMsg = postMsg ;
        this.modify("post_msg",postMsg);
    }

    /**
     * 设置 [ENVIRONMENT]
     */
    public void setEnvironment(String  environment){
        this.environment = environment ;
        this.modify("environment",environment);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [FEES_DOM_FIXED]
     */
    public void setFeesDomFixed(Double  feesDomFixed){
        this.feesDomFixed = feesDomFixed ;
        this.modify("fees_dom_fixed",feesDomFixed);
    }

    /**
     * 设置 [FEES_INT_FIXED]
     */
    public void setFeesIntFixed(Double  feesIntFixed){
        this.feesIntFixed = feesIntFixed ;
        this.modify("fees_int_fixed",feesIntFixed);
    }

    /**
     * 设置 [MODULE_ID]
     */
    public void setModuleId(Integer  moduleId){
        this.moduleId = moduleId ;
        this.modify("module_id",moduleId);
    }

    /**
     * 设置 [PAYMENT_FLOW]
     */
    public void setPaymentFlow(String  paymentFlow){
        this.paymentFlow = paymentFlow ;
        this.modify("payment_flow",paymentFlow);
    }

    /**
     * 设置 [SPECIFIC_COUNTRIES]
     */
    public void setSpecificCountries(String  specificCountries){
        this.specificCountries = specificCountries ;
        this.modify("specific_countries",specificCountries);
    }

    /**
     * 设置 [FEES_DOM_VAR]
     */
    public void setFeesDomVar(Double  feesDomVar){
        this.feesDomVar = feesDomVar ;
        this.modify("fees_dom_var",feesDomVar);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRE_MSG]
     */
    public void setPreMsg(String  preMsg){
        this.preMsg = preMsg ;
        this.modify("pre_msg",preMsg);
    }

    /**
     * 设置 [QR_CODE]
     */
    public void setQrCode(String  qrCode){
        this.qrCode = qrCode ;
        this.modify("qr_code",qrCode);
    }

    /**
     * 设置 [PENDING_MSG]
     */
    public void setPendingMsg(String  pendingMsg){
        this.pendingMsg = pendingMsg ;
        this.modify("pending_msg",pendingMsg);
    }

    /**
     * 设置 [VIEW_TEMPLATE_ID]
     */
    public void setViewTemplateId(Integer  viewTemplateId){
        this.viewTemplateId = viewTemplateId ;
        this.modify("view_template_id",viewTemplateId);
    }

    /**
     * 设置 [SO_REFERENCE_TYPE]
     */
    public void setSoReferenceType(String  soReferenceType){
        this.soReferenceType = soReferenceType ;
        this.modify("so_reference_type",soReferenceType);
    }

    /**
     * 设置 [CANCEL_MSG]
     */
    public void setCancelMsg(String  cancelMsg){
        this.cancelMsg = cancelMsg ;
        this.modify("cancel_msg",cancelMsg);
    }

    /**
     * 设置 [PROVIDER]
     */
    public void setProvider(String  provider){
        this.provider = provider ;
        this.modify("provider",provider);
    }

    /**
     * 设置 [SAVE_TOKEN]
     */
    public void setSaveToken(String  saveToken){
        this.saveToken = saveToken ;
        this.modify("save_token",saveToken);
    }

    /**
     * 设置 [CAPTURE_MANUALLY]
     */
    public void setCaptureManually(String  captureManually){
        this.captureManually = captureManually ;
        this.modify("capture_manually",captureManually);
    }

    /**
     * 设置 [ERROR_MSG]
     */
    public void setErrorMsg(String  errorMsg){
        this.errorMsg = errorMsg ;
        this.modify("error_msg",errorMsg);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Integer  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}


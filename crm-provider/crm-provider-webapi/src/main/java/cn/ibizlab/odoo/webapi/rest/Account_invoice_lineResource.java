package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_lineService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_line" })
@RestController("WebApi-account_invoice_line")
@RequestMapping("")
public class Account_invoice_lineResource {

    @Autowired
    private IAccount_invoice_lineService account_invoice_lineService;

    @Autowired
    @Lazy
    private Account_invoice_lineMapping account_invoice_lineMapping;




    @PreAuthorize("hasPermission(#account_invoice_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/{account_invoice_line_id}")

    public ResponseEntity<Account_invoice_lineDTO> update(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_lineDTO account_invoice_linedto) {
		Account_invoice_line domain = account_invoice_lineMapping.toDomain(account_invoice_linedto);
        domain.setId(account_invoice_line_id);
		account_invoice_lineService.update(domain);
		Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.updateBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "CheckKey", tags = {"Account_invoice_line" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.checkKey(account_invoice_lineMapping.toDomain(account_invoice_linedto)));
    }













    @PreAuthorize("hasPermission(#account_invoice_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/{account_invoice_line_id}")
    public ResponseEntity<Account_invoice_lineDTO> get(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
        Account_invoice_line domain = account_invoice_lineService.get(account_invoice_line_id);
        Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Account_invoice_line" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/getdraft")
    public ResponseEntity<Account_invoice_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineMapping.toDto(account_invoice_lineService.getDraft(new Account_invoice_line())));
    }




    @ApiOperation(value = "Save", tags = {"Account_invoice_line" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.save(account_invoice_lineMapping.toDomain(account_invoice_linedto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Account_invoice_line" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.saveBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_invoice_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/{account_invoice_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.remove(account_invoice_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines")

    public ResponseEntity<Account_invoice_lineDTO> create(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        Account_invoice_line domain = account_invoice_lineMapping.toDomain(account_invoice_linedto);
		account_invoice_lineService.create(domain);
        Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.createBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_invoice_line-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_lines/fetchdefault")
	public ResponseEntity<List<Account_invoice_lineDTO>> fetchDefault(Account_invoice_lineSearchContext context) {
        Page<Account_invoice_line> domains = account_invoice_lineService.searchDefault(context) ;
        List<Account_invoice_lineDTO> list = account_invoice_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_invoice_line-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_lines/searchdefault")
	public ResponseEntity<Page<Account_invoice_lineDTO>> searchDefault(@RequestBody Account_invoice_lineSearchContext context) {
        Page<Account_invoice_line> domains = account_invoice_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_line getEntity(){
        return new Account_invoice_line();
    }

}

package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Res_companyDTO]
 */
@Data
public class Res_companyDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REPORT_HEADER]
     *
     */
    @JSONField(name = "report_header")
    @JsonProperty("report_header")
    private String reportHeader;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 属性 [SALE_QUOTATION_ONBOARDING_STATE]
     *
     */
    @JSONField(name = "sale_quotation_onboarding_state")
    @JsonProperty("sale_quotation_onboarding_state")
    private String saleQuotationOnboardingState;

    /**
     * 属性 [QUOTATION_VALIDITY_DAYS]
     *
     */
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    private Integer quotationValidityDays;

    /**
     * 属性 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     *
     */
    @JSONField(name = "account_onboarding_invoice_layout_state")
    @JsonProperty("account_onboarding_invoice_layout_state")
    private String accountOnboardingInvoiceLayoutState;

    /**
     * 属性 [ACCOUNT_NO]
     *
     */
    @JSONField(name = "account_no")
    @JsonProperty("account_no")
    private String accountNo;

    /**
     * 属性 [BANK_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    private String bankAccountCodePrefix;

    /**
     * 属性 [BANK_JOURNAL_IDS]
     *
     */
    @JSONField(name = "bank_journal_ids")
    @JsonProperty("bank_journal_ids")
    private String bankJournalIds;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 属性 [PERIOD_LOCK_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "period_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("period_lock_date")
    private Timestamp periodLockDate;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 属性 [RESOURCE_CALENDAR_IDS]
     *
     */
    @JSONField(name = "resource_calendar_ids")
    @JsonProperty("resource_calendar_ids")
    private String resourceCalendarIds;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;

    /**
     * 属性 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     *
     */
    @JSONField(name = "account_dashboard_onboarding_state")
    @JsonProperty("account_dashboard_onboarding_state")
    private String accountDashboardOnboardingState;

    /**
     * 属性 [PROPAGATION_MINIMUM_DELTA]
     *
     */
    @JSONField(name = "propagation_minimum_delta")
    @JsonProperty("propagation_minimum_delta")
    private Integer propagationMinimumDelta;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SNAILMAIL_COLOR]
     *
     */
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    private String snailmailColor;

    /**
     * 属性 [OVERDUE_MSG]
     *
     */
    @JSONField(name = "overdue_msg")
    @JsonProperty("overdue_msg")
    private String overdueMsg;

    /**
     * 属性 [CITY]
     *
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 属性 [ACCOUNT_SETUP_COA_STATE]
     *
     */
    @JSONField(name = "account_setup_coa_state")
    @JsonProperty("account_setup_coa_state")
    private String accountSetupCoaState;

    /**
     * 属性 [INVOICE_REFERENCE_TYPE]
     *
     */
    @JSONField(name = "invoice_reference_type")
    @JsonProperty("invoice_reference_type")
    private String invoiceReferenceType;

    /**
     * 属性 [CATCHALL]
     *
     */
    @JSONField(name = "catchall")
    @JsonProperty("catchall")
    private String catchall;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ANGLO_SAXON_ACCOUNTING]
     *
     */
    @JSONField(name = "anglo_saxon_accounting")
    @JsonProperty("anglo_saxon_accounting")
    private String angloSaxonAccounting;

    /**
     * 属性 [SNAILMAIL_DUPLEX]
     *
     */
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    private String snailmailDuplex;

    /**
     * 属性 [SOCIAL_GITHUB]
     *
     */
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;

    /**
     * 属性 [ACCOUNT_SETUP_BANK_DATA_STATE]
     *
     */
    @JSONField(name = "account_setup_bank_data_state")
    @JsonProperty("account_setup_bank_data_state")
    private String accountSetupBankDataState;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [STREET2]
     *
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 属性 [EXPECTS_CHART_OF_ACCOUNTS]
     *
     */
    @JSONField(name = "expects_chart_of_accounts")
    @JsonProperty("expects_chart_of_accounts")
    private String expectsChartOfAccounts;

    /**
     * 属性 [TRANSFER_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    private String transferAccountCodePrefix;

    /**
     * 属性 [FISCALYEAR_LAST_DAY]
     *
     */
    @JSONField(name = "fiscalyear_last_day")
    @JsonProperty("fiscalyear_last_day")
    private Integer fiscalyearLastDay;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 属性 [ACCOUNT_BANK_RECONCILIATION_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;

    /**
     * 属性 [PORTAL_CONFIRMATION_PAY]
     *
     */
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    private String portalConfirmationPay;

    /**
     * 属性 [QR_CODE]
     *
     */
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 属性 [STREET]
     *
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 属性 [ACCOUNT_INVOICE_ONBOARDING_STATE]
     *
     */
    @JSONField(name = "account_invoice_onboarding_state")
    @JsonProperty("account_invoice_onboarding_state")
    private String accountInvoiceOnboardingState;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [NOMENCLATURE_ID]
     *
     */
    @JSONField(name = "nomenclature_id")
    @JsonProperty("nomenclature_id")
    private Integer nomenclatureId;

    /**
     * 属性 [SOCIAL_GOOGLEPLUS]
     *
     */
    @JSONField(name = "social_googleplus")
    @JsonProperty("social_googleplus")
    private String socialGoogleplus;

    /**
     * 属性 [PAYMENT_ACQUIRER_ONBOARDING_STATE]
     *
     */
    @JSONField(name = "payment_acquirer_onboarding_state")
    @JsonProperty("payment_acquirer_onboarding_state")
    private String paymentAcquirerOnboardingState;

    /**
     * 属性 [REPORT_FOOTER]
     *
     */
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    private String reportFooter;

    /**
     * 属性 [PAYMENT_ONBOARDING_PAYMENT_METHOD]
     *
     */
    @JSONField(name = "payment_onboarding_payment_method")
    @JsonProperty("payment_onboarding_payment_method")
    private String paymentOnboardingPaymentMethod;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [PO_DOUBLE_VALIDATION]
     *
     */
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    private String poDoubleValidation;

    /**
     * 属性 [PO_LEAD]
     *
     */
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    private Double poLead;

    /**
     * 属性 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     *
     */
    @JSONField(name = "sale_onboarding_sample_quotation_state")
    @JsonProperty("sale_onboarding_sample_quotation_state")
    private String saleOnboardingSampleQuotationState;

    /**
     * 属性 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     *
     */
    @JSONField(name = "sale_onboarding_order_confirmation_state")
    @JsonProperty("sale_onboarding_order_confirmation_state")
    private String saleOnboardingOrderConfirmationState;

    /**
     * 属性 [EXTERNAL_REPORT_LAYOUT_ID]
     *
     */
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    private Integer externalReportLayoutId;

    /**
     * 属性 [SALE_ONBOARDING_PAYMENT_METHOD]
     *
     */
    @JSONField(name = "sale_onboarding_payment_method")
    @JsonProperty("sale_onboarding_payment_method")
    private String saleOnboardingPaymentMethod;

    /**
     * 属性 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     *
     */
    @JSONField(name = "account_onboarding_sample_invoice_state")
    @JsonProperty("account_onboarding_sample_invoice_state")
    private String accountOnboardingSampleInvoiceState;

    /**
     * 属性 [BASE_ONBOARDING_COMPANY_STATE]
     *
     */
    @JSONField(name = "base_onboarding_company_state")
    @JsonProperty("base_onboarding_company_state")
    private String baseOnboardingCompanyState;

    /**
     * 属性 [SOCIAL_LINKEDIN]
     *
     */
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;

    /**
     * 属性 [MANUFACTURING_LEAD]
     *
     */
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    private Double manufacturingLead;

    /**
     * 属性 [SALE_NOTE]
     *
     */
    @JSONField(name = "sale_note")
    @JsonProperty("sale_note")
    private String saleNote;

    /**
     * 属性 [PO_DOUBLE_VALIDATION_AMOUNT]
     *
     */
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    private Double poDoubleValidationAmount;

    /**
     * 属性 [PO_LOCK]
     *
     */
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    private String poLock;

    /**
     * 属性 [SOCIAL_TWITTER]
     *
     */
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;

    /**
     * 属性 [SOCIAL_INSTAGRAM]
     *
     */
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;

    /**
     * 属性 [ACCOUNT_SETUP_FY_DATA_STATE]
     *
     */
    @JSONField(name = "account_setup_fy_data_state")
    @JsonProperty("account_setup_fy_data_state")
    private String accountSetupFyDataState;

    /**
     * 属性 [TAX_CALCULATION_ROUNDING_METHOD]
     *
     */
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;

    /**
     * 属性 [CASH_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    private String cashAccountCodePrefix;

    /**
     * 属性 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     *
     */
    @JSONField(name = "account_onboarding_sale_tax_state")
    @JsonProperty("account_onboarding_sale_tax_state")
    private String accountOnboardingSaleTaxState;

    /**
     * 属性 [SECURITY_LEAD]
     *
     */
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    private Double securityLead;

    /**
     * 属性 [WEBSITE_THEME_ONBOARDING_DONE]
     *
     */
    @JSONField(name = "website_theme_onboarding_done")
    @JsonProperty("website_theme_onboarding_done")
    private String websiteThemeOnboardingDone;

    /**
     * 属性 [INVOICE_IS_PRINT]
     *
     */
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    private String invoiceIsPrint;

    /**
     * 属性 [COMPANY_REGISTRY]
     *
     */
    @JSONField(name = "company_registry")
    @JsonProperty("company_registry")
    private String companyRegistry;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [LOGO_WEB]
     *
     */
    @JSONField(name = "logo_web")
    @JsonProperty("logo_web")
    private byte[] logoWeb;

    /**
     * 属性 [FISCALYEAR_LOCK_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "fiscalyear_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("fiscalyear_lock_date")
    private Timestamp fiscalyearLockDate;

    /**
     * 属性 [INVOICE_IS_SNAILMAIL]
     *
     */
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    private String invoiceIsSnailmail;

    /**
     * 属性 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     *
     */
    @JSONField(name = "website_sale_onboarding_payment_acquirer_state")
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    private String websiteSaleOnboardingPaymentAcquirerState;

    /**
     * 属性 [SOCIAL_FACEBOOK]
     *
     */
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;

    /**
     * 属性 [PORTAL_CONFIRMATION_SIGN]
     *
     */
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    private String portalConfirmationSign;

    /**
     * 属性 [PAPERFORMAT_ID]
     *
     */
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    private Integer paperformatId;

    /**
     * 属性 [FISCALYEAR_LAST_MONTH]
     *
     */
    @JSONField(name = "fiscalyear_last_month")
    @JsonProperty("fiscalyear_last_month")
    private String fiscalyearLastMonth;

    /**
     * 属性 [INVOICE_IS_EMAIL]
     *
     */
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    private String invoiceIsEmail;

    /**
     * 属性 [SOCIAL_YOUTUBE]
     *
     */
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    private Integer expenseCurrencyExchangeAccountId;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 属性 [PHONE]
     *
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 属性 [LOGO]
     *
     */
    @JSONField(name = "logo")
    @JsonProperty("logo")
    private byte[] logo;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    private String propertyStockAccountInputCategIdText;

    /**
     * 属性 [ACCOUNT_PURCHASE_TAX_ID_TEXT]
     *
     */
    @JSONField(name = "account_purchase_tax_id_text")
    @JsonProperty("account_purchase_tax_id_text")
    private String accountPurchaseTaxIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [INCOTERM_ID_TEXT]
     *
     */
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    private String incotermIdText;

    /**
     * 属性 [ACCOUNT_OPENING_JOURNAL_ID]
     *
     */
    @JSONField(name = "account_opening_journal_id")
    @JsonProperty("account_opening_journal_id")
    private Integer accountOpeningJournalId;

    /**
     * 属性 [TRANSFER_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "transfer_account_id_text")
    @JsonProperty("transfer_account_id_text")
    private String transferAccountIdText;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    private Integer incomeCurrencyExchangeAccountId;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [ACCOUNT_OPENING_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_opening_date" , format="yyyy-MM-dd")
    @JsonProperty("account_opening_date")
    private Timestamp accountOpeningDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 属性 [ACCOUNT_SALE_TAX_ID_TEXT]
     *
     */
    @JSONField(name = "account_sale_tax_id_text")
    @JsonProperty("account_sale_tax_id_text")
    private String accountSaleTaxIdText;

    /**
     * 属性 [ACCOUNT_OPENING_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "account_opening_move_id_text")
    @JsonProperty("account_opening_move_id_text")
    private String accountOpeningMoveIdText;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    private String propertyStockAccountOutputCategIdText;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    private String propertyStockValuationAccountIdText;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "tax_cash_basis_journal_id_text")
    @JsonProperty("tax_cash_basis_journal_id_text")
    private String taxCashBasisJournalIdText;

    /**
     * 属性 [INTERNAL_TRANSIT_LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "internal_transit_location_id_text")
    @JsonProperty("internal_transit_location_id_text")
    private String internalTransitLocationIdText;

    /**
     * 属性 [WEBSITE]
     *
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 属性 [VAT]
     *
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "currency_exchange_journal_id_text")
    @JsonProperty("currency_exchange_journal_id_text")
    private String currencyExchangeJournalIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 属性 [ACCOUNT_OPENING_MOVE_ID]
     *
     */
    @JSONField(name = "account_opening_move_id")
    @JsonProperty("account_opening_move_id")
    private Integer accountOpeningMoveId;

    /**
     * 属性 [INTERNAL_TRANSIT_LOCATION_ID]
     *
     */
    @JSONField(name = "internal_transit_location_id")
    @JsonProperty("internal_transit_location_id")
    private Integer internalTransitLocationId;

    /**
     * 属性 [ACCOUNT_PURCHASE_TAX_ID]
     *
     */
    @JSONField(name = "account_purchase_tax_id")
    @JsonProperty("account_purchase_tax_id")
    private Integer accountPurchaseTaxId;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 属性 [ACCOUNT_SALE_TAX_ID]
     *
     */
    @JSONField(name = "account_sale_tax_id")
    @JsonProperty("account_sale_tax_id")
    private Integer accountSaleTaxId;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID]
     *
     */
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    private Integer taxCashBasisJournalId;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [INCOTERM_ID]
     *
     */
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    private Integer incotermId;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 属性 [TRANSFER_ACCOUNT_ID]
     *
     */
    @JSONField(name = "transfer_account_id")
    @JsonProperty("transfer_account_id")
    private Integer transferAccountId;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID]
     *
     */
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    private Integer currencyExchangeJournalId;


    /**
     * 设置 [REPORT_HEADER]
     */
    public void setReportHeader(String  reportHeader){
        this.reportHeader = reportHeader ;
        this.modify("report_header",reportHeader);
    }

    /**
     * 设置 [SALE_QUOTATION_ONBOARDING_STATE]
     */
    public void setSaleQuotationOnboardingState(String  saleQuotationOnboardingState){
        this.saleQuotationOnboardingState = saleQuotationOnboardingState ;
        this.modify("sale_quotation_onboarding_state",saleQuotationOnboardingState);
    }

    /**
     * 设置 [QUOTATION_VALIDITY_DAYS]
     */
    public void setQuotationValidityDays(Integer  quotationValidityDays){
        this.quotationValidityDays = quotationValidityDays ;
        this.modify("quotation_validity_days",quotationValidityDays);
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_INVOICE_LAYOUT_STATE]
     */
    public void setAccountOnboardingInvoiceLayoutState(String  accountOnboardingInvoiceLayoutState){
        this.accountOnboardingInvoiceLayoutState = accountOnboardingInvoiceLayoutState ;
        this.modify("account_onboarding_invoice_layout_state",accountOnboardingInvoiceLayoutState);
    }

    /**
     * 设置 [ACCOUNT_NO]
     */
    public void setAccountNo(String  accountNo){
        this.accountNo = accountNo ;
        this.modify("account_no",accountNo);
    }

    /**
     * 设置 [BANK_ACCOUNT_CODE_PREFIX]
     */
    public void setBankAccountCodePrefix(String  bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }

    /**
     * 设置 [PERIOD_LOCK_DATE]
     */
    public void setPeriodLockDate(Timestamp  periodLockDate){
        this.periodLockDate = periodLockDate ;
        this.modify("period_lock_date",periodLockDate);
    }

    /**
     * 设置 [TAX_EXIGIBILITY]
     */
    public void setTaxExigibility(String  taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }

    /**
     * 设置 [ACCOUNT_DASHBOARD_ONBOARDING_STATE]
     */
    public void setAccountDashboardOnboardingState(String  accountDashboardOnboardingState){
        this.accountDashboardOnboardingState = accountDashboardOnboardingState ;
        this.modify("account_dashboard_onboarding_state",accountDashboardOnboardingState);
    }

    /**
     * 设置 [PROPAGATION_MINIMUM_DELTA]
     */
    public void setPropagationMinimumDelta(Integer  propagationMinimumDelta){
        this.propagationMinimumDelta = propagationMinimumDelta ;
        this.modify("propagation_minimum_delta",propagationMinimumDelta);
    }

    /**
     * 设置 [SNAILMAIL_COLOR]
     */
    public void setSnailmailColor(String  snailmailColor){
        this.snailmailColor = snailmailColor ;
        this.modify("snailmail_color",snailmailColor);
    }

    /**
     * 设置 [OVERDUE_MSG]
     */
    public void setOverdueMsg(String  overdueMsg){
        this.overdueMsg = overdueMsg ;
        this.modify("overdue_msg",overdueMsg);
    }

    /**
     * 设置 [ACCOUNT_SETUP_COA_STATE]
     */
    public void setAccountSetupCoaState(String  accountSetupCoaState){
        this.accountSetupCoaState = accountSetupCoaState ;
        this.modify("account_setup_coa_state",accountSetupCoaState);
    }

    /**
     * 设置 [INVOICE_REFERENCE_TYPE]
     */
    public void setInvoiceReferenceType(String  invoiceReferenceType){
        this.invoiceReferenceType = invoiceReferenceType ;
        this.modify("invoice_reference_type",invoiceReferenceType);
    }

    /**
     * 设置 [ANGLO_SAXON_ACCOUNTING]
     */
    public void setAngloSaxonAccounting(String  angloSaxonAccounting){
        this.angloSaxonAccounting = angloSaxonAccounting ;
        this.modify("anglo_saxon_accounting",angloSaxonAccounting);
    }

    /**
     * 设置 [SNAILMAIL_DUPLEX]
     */
    public void setSnailmailDuplex(String  snailmailDuplex){
        this.snailmailDuplex = snailmailDuplex ;
        this.modify("snailmail_duplex",snailmailDuplex);
    }

    /**
     * 设置 [SOCIAL_GITHUB]
     */
    public void setSocialGithub(String  socialGithub){
        this.socialGithub = socialGithub ;
        this.modify("social_github",socialGithub);
    }

    /**
     * 设置 [ACCOUNT_SETUP_BANK_DATA_STATE]
     */
    public void setAccountSetupBankDataState(String  accountSetupBankDataState){
        this.accountSetupBankDataState = accountSetupBankDataState ;
        this.modify("account_setup_bank_data_state",accountSetupBankDataState);
    }

    /**
     * 设置 [EXPECTS_CHART_OF_ACCOUNTS]
     */
    public void setExpectsChartOfAccounts(String  expectsChartOfAccounts){
        this.expectsChartOfAccounts = expectsChartOfAccounts ;
        this.modify("expects_chart_of_accounts",expectsChartOfAccounts);
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    public void setTransferAccountCodePrefix(String  transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }

    /**
     * 设置 [FISCALYEAR_LAST_DAY]
     */
    public void setFiscalyearLastDay(Integer  fiscalyearLastDay){
        this.fiscalyearLastDay = fiscalyearLastDay ;
        this.modify("fiscalyear_last_day",fiscalyearLastDay);
    }

    /**
     * 设置 [ACCOUNT_BANK_RECONCILIATION_START]
     */
    public void setAccountBankReconciliationStart(Timestamp  accountBankReconciliationStart){
        this.accountBankReconciliationStart = accountBankReconciliationStart ;
        this.modify("account_bank_reconciliation_start",accountBankReconciliationStart);
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_PAY]
     */
    public void setPortalConfirmationPay(String  portalConfirmationPay){
        this.portalConfirmationPay = portalConfirmationPay ;
        this.modify("portal_confirmation_pay",portalConfirmationPay);
    }

    /**
     * 设置 [QR_CODE]
     */
    public void setQrCode(String  qrCode){
        this.qrCode = qrCode ;
        this.modify("qr_code",qrCode);
    }

    /**
     * 设置 [ACCOUNT_INVOICE_ONBOARDING_STATE]
     */
    public void setAccountInvoiceOnboardingState(String  accountInvoiceOnboardingState){
        this.accountInvoiceOnboardingState = accountInvoiceOnboardingState ;
        this.modify("account_invoice_onboarding_state",accountInvoiceOnboardingState);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [NOMENCLATURE_ID]
     */
    public void setNomenclatureId(Integer  nomenclatureId){
        this.nomenclatureId = nomenclatureId ;
        this.modify("nomenclature_id",nomenclatureId);
    }

    /**
     * 设置 [SOCIAL_GOOGLEPLUS]
     */
    public void setSocialGoogleplus(String  socialGoogleplus){
        this.socialGoogleplus = socialGoogleplus ;
        this.modify("social_googleplus",socialGoogleplus);
    }

    /**
     * 设置 [PAYMENT_ACQUIRER_ONBOARDING_STATE]
     */
    public void setPaymentAcquirerOnboardingState(String  paymentAcquirerOnboardingState){
        this.paymentAcquirerOnboardingState = paymentAcquirerOnboardingState ;
        this.modify("payment_acquirer_onboarding_state",paymentAcquirerOnboardingState);
    }

    /**
     * 设置 [REPORT_FOOTER]
     */
    public void setReportFooter(String  reportFooter){
        this.reportFooter = reportFooter ;
        this.modify("report_footer",reportFooter);
    }

    /**
     * 设置 [PAYMENT_ONBOARDING_PAYMENT_METHOD]
     */
    public void setPaymentOnboardingPaymentMethod(String  paymentOnboardingPaymentMethod){
        this.paymentOnboardingPaymentMethod = paymentOnboardingPaymentMethod ;
        this.modify("payment_onboarding_payment_method",paymentOnboardingPaymentMethod);
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION]
     */
    public void setPoDoubleValidation(String  poDoubleValidation){
        this.poDoubleValidation = poDoubleValidation ;
        this.modify("po_double_validation",poDoubleValidation);
    }

    /**
     * 设置 [PO_LEAD]
     */
    public void setPoLead(Double  poLead){
        this.poLead = poLead ;
        this.modify("po_lead",poLead);
    }

    /**
     * 设置 [SALE_ONBOARDING_SAMPLE_QUOTATION_STATE]
     */
    public void setSaleOnboardingSampleQuotationState(String  saleOnboardingSampleQuotationState){
        this.saleOnboardingSampleQuotationState = saleOnboardingSampleQuotationState ;
        this.modify("sale_onboarding_sample_quotation_state",saleOnboardingSampleQuotationState);
    }

    /**
     * 设置 [SALE_ONBOARDING_ORDER_CONFIRMATION_STATE]
     */
    public void setSaleOnboardingOrderConfirmationState(String  saleOnboardingOrderConfirmationState){
        this.saleOnboardingOrderConfirmationState = saleOnboardingOrderConfirmationState ;
        this.modify("sale_onboarding_order_confirmation_state",saleOnboardingOrderConfirmationState);
    }

    /**
     * 设置 [EXTERNAL_REPORT_LAYOUT_ID]
     */
    public void setExternalReportLayoutId(Integer  externalReportLayoutId){
        this.externalReportLayoutId = externalReportLayoutId ;
        this.modify("external_report_layout_id",externalReportLayoutId);
    }

    /**
     * 设置 [SALE_ONBOARDING_PAYMENT_METHOD]
     */
    public void setSaleOnboardingPaymentMethod(String  saleOnboardingPaymentMethod){
        this.saleOnboardingPaymentMethod = saleOnboardingPaymentMethod ;
        this.modify("sale_onboarding_payment_method",saleOnboardingPaymentMethod);
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_SAMPLE_INVOICE_STATE]
     */
    public void setAccountOnboardingSampleInvoiceState(String  accountOnboardingSampleInvoiceState){
        this.accountOnboardingSampleInvoiceState = accountOnboardingSampleInvoiceState ;
        this.modify("account_onboarding_sample_invoice_state",accountOnboardingSampleInvoiceState);
    }

    /**
     * 设置 [BASE_ONBOARDING_COMPANY_STATE]
     */
    public void setBaseOnboardingCompanyState(String  baseOnboardingCompanyState){
        this.baseOnboardingCompanyState = baseOnboardingCompanyState ;
        this.modify("base_onboarding_company_state",baseOnboardingCompanyState);
    }

    /**
     * 设置 [SOCIAL_LINKEDIN]
     */
    public void setSocialLinkedin(String  socialLinkedin){
        this.socialLinkedin = socialLinkedin ;
        this.modify("social_linkedin",socialLinkedin);
    }

    /**
     * 设置 [MANUFACTURING_LEAD]
     */
    public void setManufacturingLead(Double  manufacturingLead){
        this.manufacturingLead = manufacturingLead ;
        this.modify("manufacturing_lead",manufacturingLead);
    }

    /**
     * 设置 [SALE_NOTE]
     */
    public void setSaleNote(String  saleNote){
        this.saleNote = saleNote ;
        this.modify("sale_note",saleNote);
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION_AMOUNT]
     */
    public void setPoDoubleValidationAmount(Double  poDoubleValidationAmount){
        this.poDoubleValidationAmount = poDoubleValidationAmount ;
        this.modify("po_double_validation_amount",poDoubleValidationAmount);
    }

    /**
     * 设置 [PO_LOCK]
     */
    public void setPoLock(String  poLock){
        this.poLock = poLock ;
        this.modify("po_lock",poLock);
    }

    /**
     * 设置 [SOCIAL_TWITTER]
     */
    public void setSocialTwitter(String  socialTwitter){
        this.socialTwitter = socialTwitter ;
        this.modify("social_twitter",socialTwitter);
    }

    /**
     * 设置 [SOCIAL_INSTAGRAM]
     */
    public void setSocialInstagram(String  socialInstagram){
        this.socialInstagram = socialInstagram ;
        this.modify("social_instagram",socialInstagram);
    }

    /**
     * 设置 [ACCOUNT_SETUP_FY_DATA_STATE]
     */
    public void setAccountSetupFyDataState(String  accountSetupFyDataState){
        this.accountSetupFyDataState = accountSetupFyDataState ;
        this.modify("account_setup_fy_data_state",accountSetupFyDataState);
    }

    /**
     * 设置 [TAX_CALCULATION_ROUNDING_METHOD]
     */
    public void setTaxCalculationRoundingMethod(String  taxCalculationRoundingMethod){
        this.taxCalculationRoundingMethod = taxCalculationRoundingMethod ;
        this.modify("tax_calculation_rounding_method",taxCalculationRoundingMethod);
    }

    /**
     * 设置 [CASH_ACCOUNT_CODE_PREFIX]
     */
    public void setCashAccountCodePrefix(String  cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }

    /**
     * 设置 [ACCOUNT_ONBOARDING_SALE_TAX_STATE]
     */
    public void setAccountOnboardingSaleTaxState(String  accountOnboardingSaleTaxState){
        this.accountOnboardingSaleTaxState = accountOnboardingSaleTaxState ;
        this.modify("account_onboarding_sale_tax_state",accountOnboardingSaleTaxState);
    }

    /**
     * 设置 [SECURITY_LEAD]
     */
    public void setSecurityLead(Double  securityLead){
        this.securityLead = securityLead ;
        this.modify("security_lead",securityLead);
    }

    /**
     * 设置 [INVOICE_IS_PRINT]
     */
    public void setInvoiceIsPrint(String  invoiceIsPrint){
        this.invoiceIsPrint = invoiceIsPrint ;
        this.modify("invoice_is_print",invoiceIsPrint);
    }

    /**
     * 设置 [COMPANY_REGISTRY]
     */
    public void setCompanyRegistry(String  companyRegistry){
        this.companyRegistry = companyRegistry ;
        this.modify("company_registry",companyRegistry);
    }

    /**
     * 设置 [FISCALYEAR_LOCK_DATE]
     */
    public void setFiscalyearLockDate(Timestamp  fiscalyearLockDate){
        this.fiscalyearLockDate = fiscalyearLockDate ;
        this.modify("fiscalyear_lock_date",fiscalyearLockDate);
    }

    /**
     * 设置 [INVOICE_IS_SNAILMAIL]
     */
    public void setInvoiceIsSnailmail(String  invoiceIsSnailmail){
        this.invoiceIsSnailmail = invoiceIsSnailmail ;
        this.modify("invoice_is_snailmail",invoiceIsSnailmail);
    }

    /**
     * 设置 [WEBSITE_SALE_ONBOARDING_PAYMENT_ACQUIRER_STATE]
     */
    public void setWebsiteSaleOnboardingPaymentAcquirerState(String  websiteSaleOnboardingPaymentAcquirerState){
        this.websiteSaleOnboardingPaymentAcquirerState = websiteSaleOnboardingPaymentAcquirerState ;
        this.modify("website_sale_onboarding_payment_acquirer_state",websiteSaleOnboardingPaymentAcquirerState);
    }

    /**
     * 设置 [SOCIAL_FACEBOOK]
     */
    public void setSocialFacebook(String  socialFacebook){
        this.socialFacebook = socialFacebook ;
        this.modify("social_facebook",socialFacebook);
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_SIGN]
     */
    public void setPortalConfirmationSign(String  portalConfirmationSign){
        this.portalConfirmationSign = portalConfirmationSign ;
        this.modify("portal_confirmation_sign",portalConfirmationSign);
    }

    /**
     * 设置 [PAPERFORMAT_ID]
     */
    public void setPaperformatId(Integer  paperformatId){
        this.paperformatId = paperformatId ;
        this.modify("paperformat_id",paperformatId);
    }

    /**
     * 设置 [FISCALYEAR_LAST_MONTH]
     */
    public void setFiscalyearLastMonth(String  fiscalyearLastMonth){
        this.fiscalyearLastMonth = fiscalyearLastMonth ;
        this.modify("fiscalyear_last_month",fiscalyearLastMonth);
    }

    /**
     * 设置 [INVOICE_IS_EMAIL]
     */
    public void setInvoiceIsEmail(String  invoiceIsEmail){
        this.invoiceIsEmail = invoiceIsEmail ;
        this.modify("invoice_is_email",invoiceIsEmail);
    }

    /**
     * 设置 [SOCIAL_YOUTUBE]
     */
    public void setSocialYoutube(String  socialYoutube){
        this.socialYoutube = socialYoutube ;
        this.modify("social_youtube",socialYoutube);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Integer  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    public void setPropertyStockAccountOutputCategId(Integer  propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    public void setPropertyStockValuationAccountId(Integer  propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }

    /**
     * 设置 [ACCOUNT_OPENING_MOVE_ID]
     */
    public void setAccountOpeningMoveId(Integer  accountOpeningMoveId){
        this.accountOpeningMoveId = accountOpeningMoveId ;
        this.modify("account_opening_move_id",accountOpeningMoveId);
    }

    /**
     * 设置 [INTERNAL_TRANSIT_LOCATION_ID]
     */
    public void setInternalTransitLocationId(Integer  internalTransitLocationId){
        this.internalTransitLocationId = internalTransitLocationId ;
        this.modify("internal_transit_location_id",internalTransitLocationId);
    }

    /**
     * 设置 [ACCOUNT_PURCHASE_TAX_ID]
     */
    public void setAccountPurchaseTaxId(Integer  accountPurchaseTaxId){
        this.accountPurchaseTaxId = accountPurchaseTaxId ;
        this.modify("account_purchase_tax_id",accountPurchaseTaxId);
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    public void setChartTemplateId(Integer  chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [ACCOUNT_SALE_TAX_ID]
     */
    public void setAccountSaleTaxId(Integer  accountSaleTaxId){
        this.accountSaleTaxId = accountSaleTaxId ;
        this.modify("account_sale_tax_id",accountSaleTaxId);
    }

    /**
     * 设置 [TAX_CASH_BASIS_JOURNAL_ID]
     */
    public void setTaxCashBasisJournalId(Integer  taxCashBasisJournalId){
        this.taxCashBasisJournalId = taxCashBasisJournalId ;
        this.modify("tax_cash_basis_journal_id",taxCashBasisJournalId);
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    public void setPropertyStockAccountInputCategId(Integer  propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [INCOTERM_ID]
     */
    public void setIncotermId(Integer  incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    public void setResourceCalendarId(Integer  resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_ID]
     */
    public void setTransferAccountId(Integer  transferAccountId){
        this.transferAccountId = transferAccountId ;
        this.modify("transfer_account_id",transferAccountId);
    }

    /**
     * 设置 [CURRENCY_EXCHANGE_JOURNAL_ID]
     */
    public void setCurrencyExchangeJournalId(Integer  currencyExchangeJournalId){
        this.currencyExchangeJournalId = currencyExchangeJournalId ;
        this.modify("currency_exchange_journal_id",currencyExchangeJournalId);
    }


}


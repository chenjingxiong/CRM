package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_messageService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;




@Slf4j
@Api(tags = {"Mail_message" })
@RestController("WebApi-mail_message")
@RequestMapping("")
public class Mail_messageResource {

    @Autowired
    private IMail_messageService mail_messageService;

    @Autowired
    @Lazy
    private Mail_messageMapping mail_messageMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages")

    public ResponseEntity<Mail_messageDTO> create(@RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message domain = mail_messageMapping.toDomain(mail_messagedto);
		mail_messageService.create(domain);
        Mail_messageDTO dto = mail_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_message" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.createBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_message_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{mail_message_id}")
    public ResponseEntity<Mail_messageDTO> get(@PathVariable("mail_message_id") Integer mail_message_id) {
        Mail_message domain = mail_messageService.get(mail_message_id);
        Mail_messageDTO dto = mail_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "CheckKey", tags = {"Mail_message" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_messageDTO mail_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_messageService.checkKey(mail_messageMapping.toDomain(mail_messagedto)));
    }







    @ApiOperation(value = "获取草稿数据", tags = {"Mail_message" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/getdraft")
    public ResponseEntity<Mail_messageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_messageMapping.toDto(mail_messageService.getDraft(new Mail_message())));
    }










    @PreAuthorize("hasPermission('Remove',{#mail_message_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_id") Integer mail_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_messageService.remove(mail_message_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_message" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}")

    public ResponseEntity<Mail_messageDTO> update(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
		Mail_message domain = mail_messageMapping.toDomain(mail_messagedto);
        domain.setId(mail_message_id);
		mail_messageService.update(domain);
		Mail_messageDTO dto = mail_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_message" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.updateBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "Save", tags = {"Mail_message" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_messageDTO mail_messagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_messageService.save(mail_messageMapping.toDomain(mail_messagedto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Mail_message" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.saveBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_message-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_message" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_messages/fetchdefault")
	public ResponseEntity<List<Mail_messageDTO>> fetchDefault(Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchDefault(context) ;
        List<Mail_messageDTO> list = mail_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_message-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Mail_message" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/mail_messages/searchdefault")
	public ResponseEntity<Page<Mail_messageDTO>> searchDefault(@RequestBody Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_message getEntity(){
        return new Mail_message();
    }

}

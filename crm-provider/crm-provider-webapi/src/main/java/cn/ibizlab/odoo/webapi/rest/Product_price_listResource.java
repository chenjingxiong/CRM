package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_listService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;




@Slf4j
@Api(tags = {"Product_price_list" })
@RestController("WebApi-product_price_list")
@RequestMapping("")
public class Product_price_listResource {

    @Autowired
    private IProduct_price_listService product_price_listService;

    @Autowired
    @Lazy
    private Product_price_listMapping product_price_listMapping;




    @PreAuthorize("hasPermission('Remove',{#product_price_list_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_price_list" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/{product_price_list_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_price_list_id") Integer product_price_list_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_price_listService.remove(product_price_list_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_price_list" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_price_listService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_price_list_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_price_list" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/{product_price_list_id}")

    public ResponseEntity<Product_price_listDTO> update(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_listDTO product_price_listdto) {
		Product_price_list domain = product_price_listMapping.toDomain(product_price_listdto);
        domain.setId(product_price_list_id);
		product_price_listService.update(domain);
		Product_price_listDTO dto = product_price_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_price_list_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_price_list" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        product_price_listService.updateBatch(product_price_listMapping.toDomain(product_price_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_price_list" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists")

    public ResponseEntity<Product_price_listDTO> create(@RequestBody Product_price_listDTO product_price_listdto) {
        Product_price_list domain = product_price_listMapping.toDomain(product_price_listdto);
		product_price_listService.create(domain);
        Product_price_listDTO dto = product_price_listMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_price_list" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_price_listDTO> product_price_listdtos) {
        product_price_listService.createBatch(product_price_listMapping.toDomain(product_price_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_price_list_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_price_list" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/{product_price_list_id}")
    public ResponseEntity<Product_price_listDTO> get(@PathVariable("product_price_list_id") Integer product_price_list_id) {
        Product_price_list domain = product_price_listService.get(product_price_list_id);
        Product_price_listDTO dto = product_price_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Product_price_list" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/getdraft")
    public ResponseEntity<Product_price_listDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_price_listMapping.toDto(product_price_listService.getDraft(new Product_price_list())));
    }







    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_price_list-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_price_list" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_price_lists/fetchdefault")
	public ResponseEntity<List<Product_price_listDTO>> fetchDefault(Product_price_listSearchContext context) {
        Page<Product_price_list> domains = product_price_listService.searchDefault(context) ;
        List<Product_price_listDTO> list = product_price_listMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_price_list-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Product_price_list" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/product_price_lists/searchdefault")
	public ResponseEntity<Page<Product_price_listDTO>> searchDefault(@RequestBody Product_price_listSearchContext context) {
        Page<Product_price_list> domains = product_price_listService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_price_listMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_price_list getEntity(){
        return new Product_price_list();
    }

}

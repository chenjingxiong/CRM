package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partnerService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;




@Slf4j
@Api(tags = {"Res_partner_p" })
@RestController("WebApi-res_partner_p")
@RequestMapping("")
public class Res_partner_pResource {

    @Autowired
    private IRes_partnerService res_partnerService;

    @Autowired
    @Lazy
    private Res_partner_pMapping res_partner_pMapping;




    @PreAuthorize("hasPermission('Remove',{#res_partner_p_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner_p" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.remove(res_partner_p_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner_p" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_ps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_p_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner_p" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_ps/{res_partner_p_id}")
    public ResponseEntity<Res_partner_pDTO> get(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner domain = res_partnerService.get(res_partner_p_id);
        Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "CheckKey", tags = {"Res_partner_p" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_pDTO res_partner_pdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partnerService.checkKey(res_partner_pMapping.toDomain(res_partner_pdto)));
    }




    @ApiOperation(value = "Save", tags = {"Res_partner_p" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_pDTO res_partner_pdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.save(res_partner_pMapping.toDomain(res_partner_pdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Res_partner_p" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        res_partnerService.saveBatch(res_partner_pMapping.toDomain(res_partner_pdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_p_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner_p" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Res_partner_pDTO> update(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
		Res_partner domain = res_partner_pMapping.toDomain(res_partner_pdto);
        domain.setId(res_partner_p_id);
		res_partnerService.update(domain);
		Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_p_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner_p" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_ps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        res_partnerService.updateBatch(res_partner_pMapping.toDomain(res_partner_pdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @ApiOperation(value = "获取草稿数据", tags = {"Res_partner_p" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_ps/getdraft")
    public ResponseEntity<Res_partner_pDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_pMapping.toDto(res_partnerService.getDraft(new Res_partner())));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner_p" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps")

    public ResponseEntity<Res_partner_pDTO> create(@RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner domain = res_partner_pMapping.toDomain(res_partner_pdto);
		res_partnerService.create(domain);
        Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner_p" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        res_partnerService.createBatch(res_partner_pMapping.toDomain(res_partner_pdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Contacts-all')")
	@ApiOperation(value = "fetch联系人（人）", tags = {"Res_partner_p" } ,notes = "fetch联系人（人）")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchcontacts")
	public ResponseEntity<List<Res_partner_pDTO>> fetchContacts(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Contacts-all')")
	@ApiOperation(value = "search联系人（人）", tags = {"Res_partner_p" } ,notes = "search联系人（人）")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_ps/searchcontacts")
	public ResponseEntity<Page<Res_partner_pDTO>> searchContacts(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Company-all')")
	@ApiOperation(value = "fetch联系人（公司）", tags = {"Res_partner_p" } ,notes = "fetch联系人（公司）")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchcompany")
	public ResponseEntity<List<Res_partner_pDTO>> fetchCompany(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Company-all')")
	@ApiOperation(value = "search联系人（公司）", tags = {"Res_partner_p" } ,notes = "search联系人（公司）")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_ps/searchcompany")
	public ResponseEntity<Page<Res_partner_pDTO>> searchCompany(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner_p" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchdefault")
	public ResponseEntity<List<Res_partner_pDTO>> fetchDefault(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner_p" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_ps/searchdefault")
	public ResponseEntity<Page<Res_partner_pDTO>> searchDefault(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @ApiOperation(value = "删除数据ByRes_partner", tags = {"Res_partner_p" },  notes = "删除数据ByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
		return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.remove(res_partner_p_id));
    }

    @ApiOperation(value = "RemoveBatchByRes_partner", tags = {"Res_partner_p" },  notes = "RemoveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/res_partner_ps/batch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@RequestBody List<Integer> ids) {
        res_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据ByRes_partner", tags = {"Res_partner_p" },  notes = "获取数据ByRes_partner")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    public ResponseEntity<Res_partner_pDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner domain = res_partnerService.get(res_partner_p_id);
        Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKeyByRes_partner", tags = {"Res_partner_p" },  notes = "CheckKeyByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partnerService.checkKey(res_partner_pMapping.toDomain(res_partner_pdto)));
    }

    @ApiOperation(value = "SaveByRes_partner", tags = {"Res_partner_p" },  notes = "SaveByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner domain = res_partner_pMapping.toDomain(res_partner_pdto);
        domain.setParentId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.save(domain));
    }

    @ApiOperation(value = "SaveBatchByRes_partner", tags = {"Res_partner_p" },  notes = "SaveBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        List<Res_partner> domainlist=res_partner_pMapping.toDomain(res_partner_pdtos);
        for(Res_partner domain:domainlist){
             domain.setParentId(res_partner_id);
        }
        res_partnerService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据ByRes_partner", tags = {"Res_partner_p" },  notes = "更新数据ByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Res_partner_pDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner domain = res_partner_pMapping.toDomain(res_partner_pdto);
        domain.setParentId(res_partner_id);
        domain.setId(res_partner_p_id);
		res_partnerService.update(domain);
        Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "UpdateBatchByRes_partner", tags = {"Res_partner_p" },  notes = "UpdateBatchByRes_partner")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/res_partner_ps/batch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        List<Res_partner> domainlist=res_partner_pMapping.toDomain(res_partner_pdtos);
        for(Res_partner domain:domainlist){
            domain.setParentId(res_partner_id);
        }
        res_partnerService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据ByRes_partner", tags = {"Res_partner_p" },  notes = "获取草稿数据ByRes_partner")
    @RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/res_partner_ps/getdraft")
    public ResponseEntity<Res_partner_pDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id) {
        Res_partner domain = new Res_partner();
        domain.setParentId(res_partner_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_pMapping.toDto(res_partnerService.getDraft(domain)));
    }

    @ApiOperation(value = "建立数据ByRes_partner", tags = {"Res_partner_p" },  notes = "建立数据ByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps")

    public ResponseEntity<Res_partner_pDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner domain = res_partner_pMapping.toDomain(res_partner_pdto);
        domain.setParentId(res_partner_id);
		res_partnerService.create(domain);
        Res_partner_pDTO dto = res_partner_pMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "createBatchByRes_partner", tags = {"Res_partner_p" },  notes = "createBatchByRes_partner")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/batch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        List<Res_partner> domainlist=res_partner_pMapping.toDomain(res_partner_pdtos);
        for(Res_partner domain:domainlist){
            domain.setParentId(res_partner_id);
        }
        res_partnerService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "fetch联系人（人）ByRes_partner", tags = {"Res_partner_p" } ,notes = "fetch联系人（人）ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchcontacts")
	public ResponseEntity<List<Res_partner_pDTO>> fetchRes_partner_pContactsByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search联系人（人）ByRes_partner", tags = {"Res_partner_p" } ,notes = "search联系人（人）ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/res_partner_ps/searchcontacts")
	public ResponseEntity<Page<Res_partner_pDTO>> searchRes_partner_pContactsByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "fetch联系人（公司）ByRes_partner", tags = {"Res_partner_p" } ,notes = "fetch联系人（公司）ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchcompany")
	public ResponseEntity<List<Res_partner_pDTO>> fetchRes_partner_pCompanyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search联系人（公司）ByRes_partner", tags = {"Res_partner_p" } ,notes = "search联系人（公司）ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/res_partner_ps/searchcompany")
	public ResponseEntity<Page<Res_partner_pDTO>> searchRes_partner_pCompanyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "fetch默认查询ByRes_partner", tags = {"Res_partner_p" } ,notes = "fetch默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchdefault")
	public ResponseEntity<List<Res_partner_pDTO>> fetchRes_partner_pDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        List<Res_partner_pDTO> list = res_partner_pMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

	@ApiOperation(value = "search默认查询ByRes_partner", tags = {"Res_partner_p" } ,notes = "search默认查询ByRes_partner")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/{res_partner_id}/res_partner_ps/searchdefault")
	public ResponseEntity<Page<Res_partner_pDTO>> searchRes_partner_pDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_pMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner getEntity(){
        return new Res_partner();
    }

}

package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lost_reasonService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;




@Slf4j
@Api(tags = {"Crm_lost_reason" })
@RestController("WebApi-crm_lost_reason")
@RequestMapping("")
public class Crm_lost_reasonResource {

    @Autowired
    private ICrm_lost_reasonService crm_lost_reasonService;

    @Autowired
    @Lazy
    private Crm_lost_reasonMapping crm_lost_reasonMapping;




    @PreAuthorize("hasPermission(#crm_lost_reason_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lost_reason" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/{crm_lost_reason_id}")
    public ResponseEntity<Crm_lost_reasonDTO> get(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
        Crm_lost_reason domain = crm_lost_reasonService.get(crm_lost_reason_id);
        Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#crm_lost_reason_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lost_reason" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/{crm_lost_reason_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonService.remove(crm_lost_reason_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lost_reason" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_lost_reasonService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lost_reason_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lost_reason" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/{crm_lost_reason_id}")

    public ResponseEntity<Crm_lost_reasonDTO> update(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
		Crm_lost_reason domain = crm_lost_reasonMapping.toDomain(crm_lost_reasondto);
        domain.setId(crm_lost_reason_id);
		crm_lost_reasonService.update(domain);
		Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lost_reason_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lost_reason" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        crm_lost_reasonService.updateBatch(crm_lost_reasonMapping.toDomain(crm_lost_reasondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lost_reason" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons")

    public ResponseEntity<Crm_lost_reasonDTO> create(@RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        Crm_lost_reason domain = crm_lost_reasonMapping.toDomain(crm_lost_reasondto);
		crm_lost_reasonService.create(domain);
        Crm_lost_reasonDTO dto = crm_lost_reasonMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lost_reason" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        crm_lost_reasonService.createBatch(crm_lost_reasonMapping.toDomain(crm_lost_reasondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lost_reason" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/getdraft")
    public ResponseEntity<Crm_lost_reasonDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasonMapping.toDto(crm_lost_reasonService.getDraft(new Crm_lost_reason())));
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lost_reason-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lost_reason" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lost_reasons/fetchdefault")
	public ResponseEntity<List<Crm_lost_reasonDTO>> fetchDefault(Crm_lost_reasonSearchContext context) {
        Page<Crm_lost_reason> domains = crm_lost_reasonService.searchDefault(context) ;
        List<Crm_lost_reasonDTO> list = crm_lost_reasonMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lost_reason-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lost_reason" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lost_reasons/searchdefault")
	public ResponseEntity<Page<Crm_lost_reasonDTO>> searchDefault(@RequestBody Crm_lost_reasonSearchContext context) {
        Page<Crm_lost_reason> domains = crm_lost_reasonService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lost_reasonMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lost_reason getEntity(){
        return new Crm_lost_reason();
    }

}

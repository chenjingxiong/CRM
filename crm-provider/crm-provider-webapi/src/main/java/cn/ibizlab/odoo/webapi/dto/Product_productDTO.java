package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Product_productDTO]
 */
@Data
public class Product_productDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VARIANT_SELLER_IDS]
     *
     */
    @JSONField(name = "variant_seller_ids")
    @JsonProperty("variant_seller_ids")
    private String variantSellerIds;

    /**
     * 属性 [PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "product_template_attribute_value_ids")
    @JsonProperty("product_template_attribute_value_ids")
    private String productTemplateAttributeValueIds;

    /**
     * 属性 [PRODUCT_VARIANT_IDS]
     *
     */
    @JSONField(name = "product_variant_ids")
    @JsonProperty("product_variant_ids")
    private String productVariantIds;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [VOLUME]
     *
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * 属性 [LST_PRICE]
     *
     */
    @JSONField(name = "lst_price")
    @JsonProperty("lst_price")
    private Double lstPrice;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_ids")
    @JsonProperty("valid_product_attribute_ids")
    private String validProductAttributeIds;

    /**
     * 属性 [STOCK_FIFO_MANUAL_MOVE_IDS]
     *
     */
    @JSONField(name = "stock_fifo_manual_move_ids")
    @JsonProperty("stock_fifo_manual_move_ids")
    private String stockFifoManualMoveIds;

    /**
     * 属性 [STOCK_QUANT_IDS]
     *
     */
    @JSONField(name = "stock_quant_ids")
    @JsonProperty("stock_quant_ids")
    private String stockQuantIds;

    /**
     * 属性 [SUPPLIER_TAXES_ID]
     *
     */
    @JSONField(name = "supplier_taxes_id")
    @JsonProperty("supplier_taxes_id")
    private String supplierTaxesId;

    /**
     * 属性 [PRICELIST_ITEM_IDS]
     *
     */
    @JSONField(name = "pricelist_item_ids")
    @JsonProperty("pricelist_item_ids")
    private String pricelistItemIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACCESSORY_PRODUCT_IDS]
     *
     */
    @JSONField(name = "accessory_product_ids")
    @JsonProperty("accessory_product_ids")
    private String accessoryProductIds;

    /**
     * 属性 [SELLER_IDS]
     *
     */
    @JSONField(name = "seller_ids")
    @JsonProperty("seller_ids")
    private String sellerIds;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_wnva_ids")
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    private String validProductAttributeValueWnvaIds;

    /**
     * 属性 [PARTNER_REF]
     *
     */
    @JSONField(name = "partner_ref")
    @JsonProperty("partner_ref")
    private String partnerRef;

    /**
     * 属性 [PRODUCT_IMAGE_IDS]
     *
     */
    @JSONField(name = "product_image_ids")
    @JsonProperty("product_image_ids")
    private String productImageIds;

    /**
     * 属性 [MRP_PRODUCT_QTY]
     *
     */
    @JSONField(name = "mrp_product_qty")
    @JsonProperty("mrp_product_qty")
    private Double mrpProductQty;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]
     *
     */
    @JSONField(name = "valid_product_template_attribute_line_ids")
    @JsonProperty("valid_product_template_attribute_line_ids")
    private String validProductTemplateAttributeLineIds;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [PUBLIC_CATEG_IDS]
     *
     */
    @JSONField(name = "public_categ_ids")
    @JsonProperty("public_categ_ids")
    private String publicCategIds;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [ATTRIBUTE_LINE_IDS]
     *
     */
    @JSONField(name = "attribute_line_ids")
    @JsonProperty("attribute_line_ids")
    private String attributeLineIds;

    /**
     * 属性 [VIRTUAL_AVAILABLE]
     *
     */
    @JSONField(name = "virtual_available")
    @JsonProperty("virtual_available")
    private Double virtualAvailable;

    /**
     * 属性 [NBR_REORDERING_RULES]
     *
     */
    @JSONField(name = "nbr_reordering_rules")
    @JsonProperty("nbr_reordering_rules")
    private Integer nbrReorderingRules;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WEBSITE_PRICE_DIFFERENCE]
     *
     */
    @JSONField(name = "website_price_difference")
    @JsonProperty("website_price_difference")
    private String websitePriceDifference;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [CART_QTY]
     *
     */
    @JSONField(name = "cart_qty")
    @JsonProperty("cart_qty")
    private Integer cartQty;

    /**
     * 属性 [WEBSITE_PUBLIC_PRICE]
     *
     */
    @JSONField(name = "website_public_price")
    @JsonProperty("website_public_price")
    private Double websitePublicPrice;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 属性 [BOM_LINE_IDS]
     *
     */
    @JSONField(name = "bom_line_ids")
    @JsonProperty("bom_line_ids")
    private String bomLineIds;

    /**
     * 属性 [WEBSITE_PRICE]
     *
     */
    @JSONField(name = "website_price")
    @JsonProperty("website_price")
    private Double websitePrice;

    /**
     * 属性 [OUTGOING_QTY]
     *
     */
    @JSONField(name = "outgoing_qty")
    @JsonProperty("outgoing_qty")
    private Double outgoingQty;

    /**
     * 属性 [SALES_COUNT]
     *
     */
    @JSONField(name = "sales_count")
    @JsonProperty("sales_count")
    private Double salesCount;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_wnva_ids")
    @JsonProperty("valid_product_attribute_wnva_ids")
    private String validProductAttributeWnvaIds;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [VALID_EXISTING_VARIANT_IDS]
     *
     */
    @JSONField(name = "valid_existing_variant_ids")
    @JsonProperty("valid_existing_variant_ids")
    private String validExistingVariantIds;

    /**
     * 属性 [STOCK_VALUE_CURRENCY_ID]
     *
     */
    @JSONField(name = "stock_value_currency_id")
    @JsonProperty("stock_value_currency_id")
    private Integer stockValueCurrencyId;

    /**
     * 属性 [STOCK_VALUE]
     *
     */
    @JSONField(name = "stock_value")
    @JsonProperty("stock_value")
    private Double stockValue;

    /**
     * 属性 [WEBSITE_STYLE_IDS]
     *
     */
    @JSONField(name = "website_style_ids")
    @JsonProperty("website_style_ids")
    private String websiteStyleIds;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [WEIGHT]
     *
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 属性 [BOM_IDS]
     *
     */
    @JSONField(name = "bom_ids")
    @JsonProperty("bom_ids")
    private String bomIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]
     *
     */
    @JSONField(name = "valid_product_template_attribute_line_wnva_ids")
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    private String validProductTemplateAttributeLineWnvaIds;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    private String validProductAttributeValueIds;

    /**
     * 属性 [QTY_AVAILABLE]
     *
     */
    @JSONField(name = "qty_available")
    @JsonProperty("qty_available")
    private Double qtyAvailable;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [IMAGE_VARIANT]
     *
     */
    @JSONField(name = "image_variant")
    @JsonProperty("image_variant")
    private byte[] imageVariant;

    /**
     * 属性 [STOCK_MOVE_IDS]
     *
     */
    @JSONField(name = "stock_move_ids")
    @JsonProperty("stock_move_ids")
    private String stockMoveIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [STOCK_FIFO_REAL_TIME_AML_IDS]
     *
     */
    @JSONField(name = "stock_fifo_real_time_aml_ids")
    @JsonProperty("stock_fifo_real_time_aml_ids")
    private String stockFifoRealTimeAmlIds;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [REORDERING_MIN_QTY]
     *
     */
    @JSONField(name = "reordering_min_qty")
    @JsonProperty("reordering_min_qty")
    private Double reorderingMinQty;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 属性 [TAXES_ID]
     *
     */
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;

    /**
     * 属性 [BOM_COUNT]
     *
     */
    @JSONField(name = "bom_count")
    @JsonProperty("bom_count")
    private Integer bomCount;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [PACKAGING_IDS]
     *
     */
    @JSONField(name = "packaging_ids")
    @JsonProperty("packaging_ids")
    private String packagingIds;

    /**
     * 属性 [VALID_ARCHIVED_VARIANT_IDS]
     *
     */
    @JSONField(name = "valid_archived_variant_ids")
    @JsonProperty("valid_archived_variant_ids")
    private String validArchivedVariantIds;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [ITEM_IDS]
     *
     */
    @JSONField(name = "item_ids")
    @JsonProperty("item_ids")
    private String itemIds;

    /**
     * 属性 [PURCHASED_PRODUCT_QTY]
     *
     */
    @JSONField(name = "purchased_product_qty")
    @JsonProperty("purchased_product_qty")
    private Double purchasedProductQty;

    /**
     * 属性 [REORDERING_MAX_QTY]
     *
     */
    @JSONField(name = "reordering_max_qty")
    @JsonProperty("reordering_max_qty")
    private Double reorderingMaxQty;

    /**
     * 属性 [ORDERPOINT_IDS]
     *
     */
    @JSONField(name = "orderpoint_ids")
    @JsonProperty("orderpoint_ids")
    private String orderpointIds;

    /**
     * 属性 [OPTIONAL_PRODUCT_IDS]
     *
     */
    @JSONField(name = "optional_product_ids")
    @JsonProperty("optional_product_ids")
    private String optionalProductIds;

    /**
     * 属性 [IS_PRODUCT_VARIANT]
     *
     */
    @JSONField(name = "is_product_variant")
    @JsonProperty("is_product_variant")
    private String isProductVariant;

    /**
     * 属性 [USED_IN_BOM_COUNT]
     *
     */
    @JSONField(name = "used_in_bom_count")
    @JsonProperty("used_in_bom_count")
    private Integer usedInBomCount;

    /**
     * 属性 [QTY_AT_DATE]
     *
     */
    @JSONField(name = "qty_at_date")
    @JsonProperty("qty_at_date")
    private Double qtyAtDate;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [STANDARD_PRICE]
     *
     */
    @JSONField(name = "standard_price")
    @JsonProperty("standard_price")
    private Double standardPrice;

    /**
     * 属性 [ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "attribute_value_ids")
    @JsonProperty("attribute_value_ids")
    private String attributeValueIds;

    /**
     * 属性 [PRICE_EXTRA]
     *
     */
    @JSONField(name = "price_extra")
    @JsonProperty("price_extra")
    private Double priceExtra;

    /**
     * 属性 [VARIANT_BOM_IDS]
     *
     */
    @JSONField(name = "variant_bom_ids")
    @JsonProperty("variant_bom_ids")
    private String variantBomIds;

    /**
     * 属性 [ALTERNATIVE_PRODUCT_IDS]
     *
     */
    @JSONField(name = "alternative_product_ids")
    @JsonProperty("alternative_product_ids")
    private String alternativeProductIds;

    /**
     * 属性 [DEFAULT_CODE]
     *
     */
    @JSONField(name = "default_code")
    @JsonProperty("default_code")
    private String defaultCode;

    /**
     * 属性 [ROUTE_FROM_CATEG_IDS]
     *
     */
    @JSONField(name = "route_from_categ_ids")
    @JsonProperty("route_from_categ_ids")
    private String routeFromCategIds;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [INCOMING_QTY]
     *
     */
    @JSONField(name = "incoming_qty")
    @JsonProperty("incoming_qty")
    private Double incomingQty;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [TRACKING]
     *
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 属性 [DESCRIPTION_PICKING]
     *
     */
    @JSONField(name = "description_picking")
    @JsonProperty("description_picking")
    private String descriptionPicking;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT]
     *
     */
    @JSONField(name = "property_stock_account_output")
    @JsonProperty("property_stock_account_output")
    private Integer propertyStockAccountOutput;

    /**
     * 属性 [SALE_OK]
     *
     */
    @JSONField(name = "sale_ok")
    @JsonProperty("sale_ok")
    private String saleOk;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [TO_WEIGHT]
     *
     */
    @JSONField(name = "to_weight")
    @JsonProperty("to_weight")
    private String toWeight;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [DESCRIPTION_PICKINGIN]
     *
     */
    @JSONField(name = "description_pickingin")
    @JsonProperty("description_pickingin")
    private String descriptionPickingin;

    /**
     * 属性 [LIST_PRICE]
     *
     */
    @JSONField(name = "list_price")
    @JsonProperty("list_price")
    private Double listPrice;

    /**
     * 属性 [HIDE_EXPENSE_POLICY]
     *
     */
    @JSONField(name = "hide_expense_policy")
    @JsonProperty("hide_expense_policy")
    private String hideExpensePolicy;

    /**
     * 属性 [DESCRIPTION_SALE]
     *
     */
    @JSONField(name = "description_sale")
    @JsonProperty("description_sale")
    private String descriptionSale;

    /**
     * 属性 [COST_METHOD]
     *
     */
    @JSONField(name = "cost_method")
    @JsonProperty("cost_method")
    private String costMethod;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [SALE_LINE_WARN_MSG]
     *
     */
    @JSONField(name = "sale_line_warn_msg")
    @JsonProperty("sale_line_warn_msg")
    private String saleLineWarnMsg;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 属性 [RENTAL]
     *
     */
    @JSONField(name = "rental")
    @JsonProperty("rental")
    private String rental;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]
     *
     */
    @JSONField(name = "property_account_creditor_price_difference")
    @JsonProperty("property_account_creditor_price_difference")
    private Integer propertyAccountCreditorPriceDifference;

    /**
     * 属性 [WEIGHT_UOM_NAME]
     *
     */
    @JSONField(name = "weight_uom_name")
    @JsonProperty("weight_uom_name")
    private String weightUomName;

    /**
     * 属性 [COST_CURRENCY_ID]
     *
     */
    @JSONField(name = "cost_currency_id")
    @JsonProperty("cost_currency_id")
    private Integer costCurrencyId;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT]
     *
     */
    @JSONField(name = "property_stock_account_input")
    @JsonProperty("property_stock_account_input")
    private Integer propertyStockAccountInput;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PRODUCE_DELAY]
     *
     */
    @JSONField(name = "produce_delay")
    @JsonProperty("produce_delay")
    private Double produceDelay;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;

    /**
     * 属性 [WEBSITE_SIZE_Y]
     *
     */
    @JSONField(name = "website_size_y")
    @JsonProperty("website_size_y")
    private Integer websiteSizeY;

    /**
     * 属性 [EVENT_OK]
     *
     */
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private String eventOk;

    /**
     * 属性 [INVENTORY_AVAILABILITY]
     *
     */
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;

    /**
     * 属性 [PURCHASE_OK]
     *
     */
    @JSONField(name = "purchase_ok")
    @JsonProperty("purchase_ok")
    private String purchaseOk;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 属性 [DESCRIPTION_PURCHASE]
     *
     */
    @JSONField(name = "description_purchase")
    @JsonProperty("description_purchase")
    private String descriptionPurchase;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [CAN_BE_EXPENSED]
     *
     */
    @JSONField(name = "can_be_expensed")
    @JsonProperty("can_be_expensed")
    private String canBeExpensed;

    /**
     * 属性 [SALE_LINE_WARN]
     *
     */
    @JSONField(name = "sale_line_warn")
    @JsonProperty("sale_line_warn")
    private String saleLineWarn;

    /**
     * 属性 [WEBSITE_SIZE_X]
     *
     */
    @JSONField(name = "website_size_x")
    @JsonProperty("website_size_x")
    private Integer websiteSizeX;

    /**
     * 属性 [SERVICE_TO_PURCHASE]
     *
     */
    @JSONField(name = "service_to_purchase")
    @JsonProperty("service_to_purchase")
    private String serviceToPurchase;

    /**
     * 属性 [WEBSITE_SEQUENCE]
     *
     */
    @JSONField(name = "website_sequence")
    @JsonProperty("website_sequence")
    private Integer websiteSequence;

    /**
     * 属性 [PROPERTY_STOCK_INVENTORY]
     *
     */
    @JSONField(name = "property_stock_inventory")
    @JsonProperty("property_stock_inventory")
    private Integer propertyStockInventory;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    private String propertyValuation;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 属性 [EXPENSE_POLICY]
     *
     */
    @JSONField(name = "expense_policy")
    @JsonProperty("expense_policy")
    private String expensePolicy;

    /**
     * 属性 [WEIGHT_UOM_ID]
     *
     */
    @JSONField(name = "weight_uom_id")
    @JsonProperty("weight_uom_id")
    private Integer weightUomId;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [PROPERTY_STOCK_PRODUCTION]
     *
     */
    @JSONField(name = "property_stock_production")
    @JsonProperty("property_stock_production")
    private Integer propertyStockProduction;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 属性 [DESCRIPTION_PICKINGOUT]
     *
     */
    @JSONField(name = "description_pickingout")
    @JsonProperty("description_pickingout")
    private String descriptionPickingout;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Integer pricelistId;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 属性 [VALUATION]
     *
     */
    @JSONField(name = "valuation")
    @JsonProperty("valuation")
    private String valuation;

    /**
     * 属性 [INVOICE_POLICY]
     *
     */
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    private String invoicePolicy;

    /**
     * 属性 [PURCHASE_LINE_WARN_MSG]
     *
     */
    @JSONField(name = "purchase_line_warn_msg")
    @JsonProperty("purchase_line_warn_msg")
    private String purchaseLineWarnMsg;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID]
     *
     */
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Integer propertyAccountIncomeId;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    private String propertyCostMethod;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * 属性 [ISPARTS]
     *
     */
    @JSONField(name = "isparts")
    @JsonProperty("isparts")
    private String isparts;

    /**
     * 属性 [UOM_ID]
     *
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    private Integer uomId;

    /**
     * 属性 [PRODUCT_VARIANT_ID]
     *
     */
    @JSONField(name = "product_variant_id")
    @JsonProperty("product_variant_id")
    private Integer productVariantId;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [PURCHASE_METHOD]
     *
     */
    @JSONField(name = "purchase_method")
    @JsonProperty("purchase_method")
    private String purchaseMethod;

    /**
     * 属性 [RESPONSIBLE_ID]
     *
     */
    @JSONField(name = "responsible_id")
    @JsonProperty("responsible_id")
    private Integer responsibleId;

    /**
     * 属性 [SERVICE_TYPE]
     *
     */
    @JSONField(name = "service_type")
    @JsonProperty("service_type")
    private String serviceType;

    /**
     * 属性 [UOM_NAME]
     *
     */
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    private String uomName;

    /**
     * 属性 [AVAILABLE_THRESHOLD]
     *
     */
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 属性 [PURCHASE_LINE_WARN]
     *
     */
    @JSONField(name = "purchase_line_warn")
    @JsonProperty("purchase_line_warn")
    private String purchaseLineWarn;

    /**
     * 属性 [PRODUCT_VARIANT_COUNT]
     *
     */
    @JSONField(name = "product_variant_count")
    @JsonProperty("product_variant_count")
    private Integer productVariantCount;

    /**
     * 属性 [POS_CATEG_ID]
     *
     */
    @JSONField(name = "pos_categ_id")
    @JsonProperty("pos_categ_id")
    private Integer posCategId;

    /**
     * 属性 [CUSTOM_MESSAGE]
     *
     */
    @JSONField(name = "custom_message")
    @JsonProperty("custom_message")
    private String customMessage;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID]
     *
     */
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Integer propertyAccountExpenseId;

    /**
     * 属性 [SALE_DELAY]
     *
     */
    @JSONField(name = "sale_delay")
    @JsonProperty("sale_delay")
    private Double saleDelay;

    /**
     * 属性 [UOM_PO_ID]
     *
     */
    @JSONField(name = "uom_po_id")
    @JsonProperty("uom_po_id")
    private Integer uomPoId;

    /**
     * 属性 [AVAILABLE_IN_POS]
     *
     */
    @JSONField(name = "available_in_pos")
    @JsonProperty("available_in_pos")
    private String availableInPos;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;


    /**
     * 设置 [VOLUME]
     */
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [WEIGHT]
     */
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [DEFAULT_CODE]
     */
    public void setDefaultCode(String  defaultCode){
        this.defaultCode = defaultCode ;
        this.modify("default_code",defaultCode);
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    public void setProductTmplId(Integer  productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }


}


package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_lostService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;




@Slf4j
@Api(tags = {"Crm_lead_lost" })
@RestController("WebApi-crm_lead_lost")
@RequestMapping("")
public class Crm_lead_lostResource {

    @Autowired
    private ICrm_lead_lostService crm_lead_lostService;

    @Autowired
    @Lazy
    private Crm_lead_lostMapping crm_lead_lostMapping;







    @PreAuthorize("hasPermission('Remove',{#crm_lead_lost_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead_lost" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/{crm_lead_lost_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostService.remove(crm_lead_lost_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead_lost" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_lead_lostService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead_lost_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead_lost" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/{crm_lead_lost_id}")

    public ResponseEntity<Crm_lead_lostDTO> update(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
		Crm_lead_lost domain = crm_lead_lostMapping.toDomain(crm_lead_lostdto);
        domain.setId(crm_lead_lost_id);
		crm_lead_lostService.update(domain);
		Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead_lost_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead_lost" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        crm_lead_lostService.updateBatch(crm_lead_lostMapping.toDomain(crm_lead_lostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead_lost" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/getdraft")
    public ResponseEntity<Crm_lead_lostDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostMapping.toDto(crm_lead_lostService.getDraft(new Crm_lead_lost())));
    }




    @PreAuthorize("hasPermission(#crm_lead_lost_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead_lost" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/{crm_lead_lost_id}")
    public ResponseEntity<Crm_lead_lostDTO> get(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
        Crm_lead_lost domain = crm_lead_lostService.get(crm_lead_lost_id);
        Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead_lost" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts")

    public ResponseEntity<Crm_lead_lostDTO> create(@RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        Crm_lead_lost domain = crm_lead_lostMapping.toDomain(crm_lead_lostdto);
		crm_lead_lostService.create(domain);
        Crm_lead_lostDTO dto = crm_lead_lostMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead_lost" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        crm_lead_lostService.createBatch(crm_lead_lostMapping.toDomain(crm_lead_lostdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead_lost-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead_lost" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead_losts/fetchdefault")
	public ResponseEntity<List<Crm_lead_lostDTO>> fetchDefault(Crm_lead_lostSearchContext context) {
        Page<Crm_lead_lost> domains = crm_lead_lostService.searchDefault(context) ;
        List<Crm_lead_lostDTO> list = crm_lead_lostMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead_lost-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead_lost" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead_losts/searchdefault")
	public ResponseEntity<Page<Crm_lead_lostDTO>> searchDefault(@RequestBody Crm_lead_lostSearchContext context) {
        Page<Crm_lead_lost> domains = crm_lead_lostService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead_lostMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead_lost getEntity(){
        return new Crm_lead_lost();
    }

}

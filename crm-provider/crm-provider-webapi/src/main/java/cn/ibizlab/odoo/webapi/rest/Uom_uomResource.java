package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_uomService;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;




@Slf4j
@Api(tags = {"Uom_uom" })
@RestController("WebApi-uom_uom")
@RequestMapping("")
public class Uom_uomResource {

    @Autowired
    private IUom_uomService uom_uomService;

    @Autowired
    @Lazy
    private Uom_uomMapping uom_uomMapping;




    @PreAuthorize("hasPermission('Remove',{#uom_uom_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Uom_uom" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/{uom_uom_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("uom_uom_id") Integer uom_uom_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uom_uomService.remove(uom_uom_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Uom_uom" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        uom_uomService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Uom_uom" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/getdraft")
    public ResponseEntity<Uom_uomDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(uom_uomMapping.toDto(uom_uomService.getDraft(new Uom_uom())));
    }




    @PreAuthorize("hasPermission(#uom_uom_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Uom_uom" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/{uom_uom_id}")

    public ResponseEntity<Uom_uomDTO> update(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uomDTO uom_uomdto) {
		Uom_uom domain = uom_uomMapping.toDomain(uom_uomdto);
        domain.setId(uom_uom_id);
		uom_uomService.update(domain);
		Uom_uomDTO dto = uom_uomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#uom_uom_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Uom_uom" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        uom_uomService.updateBatch(uom_uomMapping.toDomain(uom_uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#uom_uom_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Uom_uom" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_uoms/{uom_uom_id}")
    public ResponseEntity<Uom_uomDTO> get(@PathVariable("uom_uom_id") Integer uom_uom_id) {
        Uom_uom domain = uom_uomService.get(uom_uom_id);
        Uom_uomDTO dto = uom_uomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Uom_uom" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms")

    public ResponseEntity<Uom_uomDTO> create(@RequestBody Uom_uomDTO uom_uomdto) {
        Uom_uom domain = uom_uomMapping.toDomain(uom_uomdto);
		uom_uomService.create(domain);
        Uom_uomDTO dto = uom_uomMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Uom_uom" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_uoms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        uom_uomService.createBatch(uom_uomMapping.toDomain(uom_uomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Uom_uom-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Uom_uom" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/uom_uoms/fetchdefault")
	public ResponseEntity<List<Uom_uomDTO>> fetchDefault(Uom_uomSearchContext context) {
        Page<Uom_uom> domains = uom_uomService.searchDefault(context) ;
        List<Uom_uomDTO> list = uom_uomMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Uom_uom-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Uom_uom" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/uom_uoms/searchdefault")
	public ResponseEntity<Page<Uom_uomDTO>> searchDefault(@RequestBody Uom_uomSearchContext context) {
        Page<Uom_uom> domains = uom_uomService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uom_uomMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Uom_uom getEntity(){
        return new Uom_uom();
    }

}

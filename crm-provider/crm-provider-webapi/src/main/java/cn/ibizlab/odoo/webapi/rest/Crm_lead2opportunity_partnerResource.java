package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partnerService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;




@Slf4j
@Api(tags = {"Crm_lead2opportunity_partner" })
@RestController("WebApi-crm_lead2opportunity_partner")
@RequestMapping("")
public class Crm_lead2opportunity_partnerResource {

    @Autowired
    private ICrm_lead2opportunity_partnerService crm_lead2opportunity_partnerService;

    @Autowired
    @Lazy
    private Crm_lead2opportunity_partnerMapping crm_lead2opportunity_partnerMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead2opportunity_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners")

    public ResponseEntity<Crm_lead2opportunity_partnerDTO> create(@RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto);
		crm_lead2opportunity_partnerService.create(domain);
        Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead2opportunity_partner" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        crm_lead2opportunity_partnerService.createBatch(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#crm_lead2opportunity_partner_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead2opportunity_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerService.remove(crm_lead2opportunity_partner_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead2opportunity_partner" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_lead2opportunity_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead2opportunity_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> get(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerService.get(crm_lead2opportunity_partner_id);
        Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead2opportunity_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")

    public ResponseEntity<Crm_lead2opportunity_partnerDTO> update(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
		Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto);
        domain.setId(crm_lead2opportunity_partner_id);
		crm_lead2opportunity_partnerService.update(domain);
		Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead2opportunity_partner" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        crm_lead2opportunity_partnerService.updateBatch(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead2opportunity_partner" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/getdraft")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerMapping.toDto(crm_lead2opportunity_partnerService.getDraft(new Crm_lead2opportunity_partner())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead2opportunity_partner-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead2opportunity_partner" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partners/fetchdefault")
	public ResponseEntity<List<Crm_lead2opportunity_partnerDTO>> fetchDefault(Crm_lead2opportunity_partnerSearchContext context) {
        Page<Crm_lead2opportunity_partner> domains = crm_lead2opportunity_partnerService.searchDefault(context) ;
        List<Crm_lead2opportunity_partnerDTO> list = crm_lead2opportunity_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead2opportunity_partner-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead2opportunity_partner" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead2opportunity_partners/searchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partnerDTO>> searchDefault(@RequestBody Crm_lead2opportunity_partnerSearchContext context) {
        Page<Crm_lead2opportunity_partner> domains = crm_lead2opportunity_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead2opportunity_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead2opportunity_partner getEntity(){
        return new Crm_lead2opportunity_partner();
    }

}

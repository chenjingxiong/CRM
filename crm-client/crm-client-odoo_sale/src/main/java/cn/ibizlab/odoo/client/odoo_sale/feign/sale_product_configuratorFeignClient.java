package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_product_configurator;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_product_configuratorImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_product_configurator] 服务对象接口
 */
public interface sale_product_configuratorFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_product_configurators")
    public sale_product_configuratorImpl create(@RequestBody sale_product_configuratorImpl sale_product_configurator);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_product_configurators/createbatch")
    public sale_product_configuratorImpl createBatch(@RequestBody List<sale_product_configuratorImpl> sale_product_configurators);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_product_configurators/removebatch")
    public sale_product_configuratorImpl removeBatch(@RequestBody List<sale_product_configuratorImpl> sale_product_configurators);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_product_configurators/{id}")
    public sale_product_configuratorImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_product_configurators/{id}")
    public sale_product_configuratorImpl update(@PathVariable("id") Integer id,@RequestBody sale_product_configuratorImpl sale_product_configurator);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_product_configurators/updatebatch")
    public sale_product_configuratorImpl updateBatch(@RequestBody List<sale_product_configuratorImpl> sale_product_configurators);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_product_configurators/fetchdefault")
    public Page<sale_product_configuratorImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_product_configurators/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_product_configurators/select")
    public Page<sale_product_configuratorImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_product_configurators/{id}/getdraft")
    public sale_product_configuratorImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_product_configuratorImpl sale_product_configurator);



}

package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_order_templateClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_templateImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_order_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_order_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_order_templateClientServiceImpl implements Isale_order_templateClientService {

    sale_order_templateFeignClient sale_order_templateFeignClient;

    @Autowired
    public sale_order_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_templateFeignClient = nameBuilder.target(sale_order_templateFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_templateFeignClient = nameBuilder.target(sale_order_templateFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_order_template createModel() {
		return new sale_order_templateImpl();
	}


    public void create(Isale_order_template sale_order_template){
        Isale_order_template clientModel = sale_order_templateFeignClient.create((sale_order_templateImpl)sale_order_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_template.getClass(), false);
        copier.copy(clientModel, sale_order_template, null);
    }


    public void remove(Isale_order_template sale_order_template){
        sale_order_templateFeignClient.remove(sale_order_template.getId()) ;
    }


    public void updateBatch(List<Isale_order_template> sale_order_templates){
        if(sale_order_templates!=null){
            List<sale_order_templateImpl> list = new ArrayList<sale_order_templateImpl>();
            for(Isale_order_template isale_order_template :sale_order_templates){
                list.add((sale_order_templateImpl)isale_order_template) ;
            }
            sale_order_templateFeignClient.updateBatch(list) ;
        }
    }


    public void update(Isale_order_template sale_order_template){
        Isale_order_template clientModel = sale_order_templateFeignClient.update(sale_order_template.getId(),(sale_order_templateImpl)sale_order_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_template.getClass(), false);
        copier.copy(clientModel, sale_order_template, null);
    }


    public void get(Isale_order_template sale_order_template){
        Isale_order_template clientModel = sale_order_templateFeignClient.get(sale_order_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_template.getClass(), false);
        copier.copy(clientModel, sale_order_template, null);
    }


    public void removeBatch(List<Isale_order_template> sale_order_templates){
        if(sale_order_templates!=null){
            List<sale_order_templateImpl> list = new ArrayList<sale_order_templateImpl>();
            for(Isale_order_template isale_order_template :sale_order_templates){
                list.add((sale_order_templateImpl)isale_order_template) ;
            }
            sale_order_templateFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isale_order_template> fetchDefault(SearchContext context){
        Page<sale_order_templateImpl> page = this.sale_order_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Isale_order_template> sale_order_templates){
        if(sale_order_templates!=null){
            List<sale_order_templateImpl> list = new ArrayList<sale_order_templateImpl>();
            for(Isale_order_template isale_order_template :sale_order_templates){
                list.add((sale_order_templateImpl)isale_order_template) ;
            }
            sale_order_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Isale_order_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_order_template sale_order_template){
        Isale_order_template clientModel = sale_order_templateFeignClient.getDraft(sale_order_template.getId(),(sale_order_templateImpl)sale_order_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_template.getClass(), false);
        copier.copy(clientModel, sale_order_template, null);
    }



}


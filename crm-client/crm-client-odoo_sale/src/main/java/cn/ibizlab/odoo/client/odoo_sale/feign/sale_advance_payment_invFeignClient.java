package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_advance_payment_invImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
public interface sale_advance_payment_invFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_advance_payment_invs")
    public sale_advance_payment_invImpl create(@RequestBody sale_advance_payment_invImpl sale_advance_payment_inv);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_advance_payment_invs/removebatch")
    public sale_advance_payment_invImpl removeBatch(@RequestBody List<sale_advance_payment_invImpl> sale_advance_payment_invs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_advance_payment_invs/fetchdefault")
    public Page<sale_advance_payment_invImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_advance_payment_invs/updatebatch")
    public sale_advance_payment_invImpl updateBatch(@RequestBody List<sale_advance_payment_invImpl> sale_advance_payment_invs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_advance_payment_invs/{id}")
    public sale_advance_payment_invImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_advance_payment_invs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_advance_payment_invs/{id}")
    public sale_advance_payment_invImpl update(@PathVariable("id") Integer id,@RequestBody sale_advance_payment_invImpl sale_advance_payment_inv);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_advance_payment_invs/createbatch")
    public sale_advance_payment_invImpl createBatch(@RequestBody List<sale_advance_payment_invImpl> sale_advance_payment_invs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_advance_payment_invs/select")
    public Page<sale_advance_payment_invImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_advance_payment_invs/{id}/getdraft")
    public sale_advance_payment_invImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_advance_payment_invImpl sale_advance_payment_inv);



}

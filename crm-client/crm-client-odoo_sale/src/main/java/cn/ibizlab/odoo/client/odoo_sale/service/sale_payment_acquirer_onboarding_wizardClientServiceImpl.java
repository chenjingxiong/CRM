package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_payment_acquirer_onboarding_wizardClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_payment_acquirer_onboarding_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_payment_acquirer_onboarding_wizardClientServiceImpl implements Isale_payment_acquirer_onboarding_wizardClientService {

    sale_payment_acquirer_onboarding_wizardFeignClient sale_payment_acquirer_onboarding_wizardFeignClient;

    @Autowired
    public sale_payment_acquirer_onboarding_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(sale_payment_acquirer_onboarding_wizardFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(sale_payment_acquirer_onboarding_wizardFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_payment_acquirer_onboarding_wizard createModel() {
		return new sale_payment_acquirer_onboarding_wizardImpl();
	}


    public void update(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
        Isale_payment_acquirer_onboarding_wizard clientModel = sale_payment_acquirer_onboarding_wizardFeignClient.update(sale_payment_acquirer_onboarding_wizard.getId(),(sale_payment_acquirer_onboarding_wizardImpl)sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, sale_payment_acquirer_onboarding_wizard, null);
    }


    public void createBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards){
        if(sale_payment_acquirer_onboarding_wizards!=null){
            List<sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<sale_payment_acquirer_onboarding_wizardImpl>();
            for(Isale_payment_acquirer_onboarding_wizard isale_payment_acquirer_onboarding_wizard :sale_payment_acquirer_onboarding_wizards){
                list.add((sale_payment_acquirer_onboarding_wizardImpl)isale_payment_acquirer_onboarding_wizard) ;
            }
            sale_payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
        }
    }


    public void remove(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
        sale_payment_acquirer_onboarding_wizardFeignClient.remove(sale_payment_acquirer_onboarding_wizard.getId()) ;
    }


    public void get(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
        Isale_payment_acquirer_onboarding_wizard clientModel = sale_payment_acquirer_onboarding_wizardFeignClient.get(sale_payment_acquirer_onboarding_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, sale_payment_acquirer_onboarding_wizard, null);
    }


    public Page<Isale_payment_acquirer_onboarding_wizard> fetchDefault(SearchContext context){
        Page<sale_payment_acquirer_onboarding_wizardImpl> page = this.sale_payment_acquirer_onboarding_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards){
        if(sale_payment_acquirer_onboarding_wizards!=null){
            List<sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<sale_payment_acquirer_onboarding_wizardImpl>();
            for(Isale_payment_acquirer_onboarding_wizard isale_payment_acquirer_onboarding_wizard :sale_payment_acquirer_onboarding_wizards){
                list.add((sale_payment_acquirer_onboarding_wizardImpl)isale_payment_acquirer_onboarding_wizard) ;
            }
            sale_payment_acquirer_onboarding_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void create(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
        Isale_payment_acquirer_onboarding_wizard clientModel = sale_payment_acquirer_onboarding_wizardFeignClient.create((sale_payment_acquirer_onboarding_wizardImpl)sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, sale_payment_acquirer_onboarding_wizard, null);
    }


    public void updateBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards){
        if(sale_payment_acquirer_onboarding_wizards!=null){
            List<sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<sale_payment_acquirer_onboarding_wizardImpl>();
            for(Isale_payment_acquirer_onboarding_wizard isale_payment_acquirer_onboarding_wizard :sale_payment_acquirer_onboarding_wizards){
                list.add((sale_payment_acquirer_onboarding_wizardImpl)isale_payment_acquirer_onboarding_wizard) ;
            }
            sale_payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
        }
    }


    public Page<Isale_payment_acquirer_onboarding_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard){
        Isale_payment_acquirer_onboarding_wizard clientModel = sale_payment_acquirer_onboarding_wizardFeignClient.getDraft(sale_payment_acquirer_onboarding_wizard.getId(),(sale_payment_acquirer_onboarding_wizardImpl)sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, sale_payment_acquirer_onboarding_wizard, null);
    }



}


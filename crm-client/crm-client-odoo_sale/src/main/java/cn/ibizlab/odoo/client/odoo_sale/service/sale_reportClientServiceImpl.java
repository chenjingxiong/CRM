package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_report;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_reportClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_reportImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_reportClientServiceImpl implements Isale_reportClientService {

    sale_reportFeignClient sale_reportFeignClient;

    @Autowired
    public sale_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_reportFeignClient = nameBuilder.target(sale_reportFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_reportFeignClient = nameBuilder.target(sale_reportFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_report createModel() {
		return new sale_reportImpl();
	}


    public void get(Isale_report sale_report){
        Isale_report clientModel = sale_reportFeignClient.get(sale_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_report.getClass(), false);
        copier.copy(clientModel, sale_report, null);
    }


    public void removeBatch(List<Isale_report> sale_reports){
        if(sale_reports!=null){
            List<sale_reportImpl> list = new ArrayList<sale_reportImpl>();
            for(Isale_report isale_report :sale_reports){
                list.add((sale_reportImpl)isale_report) ;
            }
            sale_reportFeignClient.removeBatch(list) ;
        }
    }


    public void update(Isale_report sale_report){
        Isale_report clientModel = sale_reportFeignClient.update(sale_report.getId(),(sale_reportImpl)sale_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_report.getClass(), false);
        copier.copy(clientModel, sale_report, null);
    }


    public Page<Isale_report> fetchDefault(SearchContext context){
        Page<sale_reportImpl> page = this.sale_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isale_report sale_report){
        sale_reportFeignClient.remove(sale_report.getId()) ;
    }


    public void create(Isale_report sale_report){
        Isale_report clientModel = sale_reportFeignClient.create((sale_reportImpl)sale_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_report.getClass(), false);
        copier.copy(clientModel, sale_report, null);
    }


    public void createBatch(List<Isale_report> sale_reports){
        if(sale_reports!=null){
            List<sale_reportImpl> list = new ArrayList<sale_reportImpl>();
            for(Isale_report isale_report :sale_reports){
                list.add((sale_reportImpl)isale_report) ;
            }
            sale_reportFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Isale_report> sale_reports){
        if(sale_reports!=null){
            List<sale_reportImpl> list = new ArrayList<sale_reportImpl>();
            for(Isale_report isale_report :sale_reports){
                list.add((sale_reportImpl)isale_report) ;
            }
            sale_reportFeignClient.updateBatch(list) ;
        }
    }


    public Page<Isale_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_report sale_report){
        Isale_report clientModel = sale_reportFeignClient.getDraft(sale_report.getId(),(sale_reportImpl)sale_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_report.getClass(), false);
        copier.copy(clientModel, sale_report, null);
    }



}


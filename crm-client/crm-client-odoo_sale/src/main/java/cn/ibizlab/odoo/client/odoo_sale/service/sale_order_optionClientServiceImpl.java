package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_option;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_order_optionClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_optionImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_order_optionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_order_option] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_order_optionClientServiceImpl implements Isale_order_optionClientService {

    sale_order_optionFeignClient sale_order_optionFeignClient;

    @Autowired
    public sale_order_optionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_optionFeignClient = nameBuilder.target(sale_order_optionFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_order_optionFeignClient = nameBuilder.target(sale_order_optionFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_order_option createModel() {
		return new sale_order_optionImpl();
	}


    public void remove(Isale_order_option sale_order_option){
        sale_order_optionFeignClient.remove(sale_order_option.getId()) ;
    }


    public void createBatch(List<Isale_order_option> sale_order_options){
        if(sale_order_options!=null){
            List<sale_order_optionImpl> list = new ArrayList<sale_order_optionImpl>();
            for(Isale_order_option isale_order_option :sale_order_options){
                list.add((sale_order_optionImpl)isale_order_option) ;
            }
            sale_order_optionFeignClient.createBatch(list) ;
        }
    }


    public Page<Isale_order_option> fetchDefault(SearchContext context){
        Page<sale_order_optionImpl> page = this.sale_order_optionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Isale_order_option> sale_order_options){
        if(sale_order_options!=null){
            List<sale_order_optionImpl> list = new ArrayList<sale_order_optionImpl>();
            for(Isale_order_option isale_order_option :sale_order_options){
                list.add((sale_order_optionImpl)isale_order_option) ;
            }
            sale_order_optionFeignClient.removeBatch(list) ;
        }
    }


    public void update(Isale_order_option sale_order_option){
        Isale_order_option clientModel = sale_order_optionFeignClient.update(sale_order_option.getId(),(sale_order_optionImpl)sale_order_option) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_option.getClass(), false);
        copier.copy(clientModel, sale_order_option, null);
    }


    public void updateBatch(List<Isale_order_option> sale_order_options){
        if(sale_order_options!=null){
            List<sale_order_optionImpl> list = new ArrayList<sale_order_optionImpl>();
            for(Isale_order_option isale_order_option :sale_order_options){
                list.add((sale_order_optionImpl)isale_order_option) ;
            }
            sale_order_optionFeignClient.updateBatch(list) ;
        }
    }


    public void get(Isale_order_option sale_order_option){
        Isale_order_option clientModel = sale_order_optionFeignClient.get(sale_order_option.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_option.getClass(), false);
        copier.copy(clientModel, sale_order_option, null);
    }


    public void create(Isale_order_option sale_order_option){
        Isale_order_option clientModel = sale_order_optionFeignClient.create((sale_order_optionImpl)sale_order_option) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_option.getClass(), false);
        copier.copy(clientModel, sale_order_option, null);
    }


    public Page<Isale_order_option> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_order_option sale_order_option){
        Isale_order_option clientModel = sale_order_optionFeignClient.getDraft(sale_order_option.getId(),(sale_order_optionImpl)sale_order_option) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order_option.getClass(), false);
        copier.copy(clientModel, sale_order_option, null);
    }



}


package cn.ibizlab.odoo.client.odoo_sale.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isale_order_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[sale_order_line] 对象
 */
public class sale_order_lineImpl implements Isale_order_line,Serializable{

    /**
     * 分析明细行
     */
    public String analytic_line_ids;

    @JsonIgnore
    public boolean analytic_line_idsDirtyFlag;
    
    /**
     * 分析标签
     */
    public String analytic_tag_ids;

    @JsonIgnore
    public boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 交货提前时间
     */
    public Double customer_lead;

    @JsonIgnore
    public boolean customer_leadDirtyFlag;
    
    /**
     * 折扣(%)
     */
    public Double discount;

    @JsonIgnore
    public boolean discountDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 显示类型
     */
    public String display_type;

    @JsonIgnore
    public boolean display_typeDirtyFlag;
    
    /**
     * 活动
     */
    public Integer event_id;

    @JsonIgnore
    public boolean event_idDirtyFlag;
    
    /**
     * 活动
     */
    public String event_id_text;

    @JsonIgnore
    public boolean event_id_textDirtyFlag;
    
    /**
     * 是一张活动票吗？
     */
    public String event_ok;

    @JsonIgnore
    public boolean event_okDirtyFlag;
    
    /**
     * 活动入场券
     */
    public Integer event_ticket_id;

    @JsonIgnore
    public boolean event_ticket_idDirtyFlag;
    
    /**
     * 活动入场券
     */
    public String event_ticket_id_text;

    @JsonIgnore
    public boolean event_ticket_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票行
     */
    public String invoice_lines;

    @JsonIgnore
    public boolean invoice_linesDirtyFlag;
    
    /**
     * 发票状态
     */
    public String invoice_status;

    @JsonIgnore
    public boolean invoice_statusDirtyFlag;
    
    /**
     * 是首付款吗？
     */
    public String is_downpayment;

    @JsonIgnore
    public boolean is_downpaymentDirtyFlag;
    
    /**
     * 可报销
     */
    public String is_expense;

    @JsonIgnore
    public boolean is_expenseDirtyFlag;
    
    /**
     * 链接的订单明细
     */
    public Integer linked_line_id;

    @JsonIgnore
    public boolean linked_line_idDirtyFlag;
    
    /**
     * 链接的订单明细
     */
    public String linked_line_id_text;

    @JsonIgnore
    public boolean linked_line_id_textDirtyFlag;
    
    /**
     * 库存移动
     */
    public String move_ids;

    @JsonIgnore
    public boolean move_idsDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 姓名简称
     */
    public String name_short;

    @JsonIgnore
    public boolean name_shortDirtyFlag;
    
    /**
     * 链接选项
     */
    public String option_line_ids;

    @JsonIgnore
    public boolean option_line_idsDirtyFlag;
    
    /**
     * 订单关联
     */
    public Integer order_id;

    @JsonIgnore
    public boolean order_idDirtyFlag;
    
    /**
     * 订单关联
     */
    public String order_id_text;

    @JsonIgnore
    public boolean order_id_textDirtyFlag;
    
    /**
     * 客户
     */
    public Integer order_partner_id;

    @JsonIgnore
    public boolean order_partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String order_partner_id_text;

    @JsonIgnore
    public boolean order_partner_id_textDirtyFlag;
    
    /**
     * 降价
     */
    public Double price_reduce;

    @JsonIgnore
    public boolean price_reduceDirtyFlag;
    
    /**
     * 不含税价
     */
    public Double price_reduce_taxexcl;

    @JsonIgnore
    public boolean price_reduce_taxexclDirtyFlag;
    
    /**
     * 减税后价格
     */
    public Double price_reduce_taxinc;

    @JsonIgnore
    public boolean price_reduce_taxincDirtyFlag;
    
    /**
     * 小计
     */
    public Double price_subtotal;

    @JsonIgnore
    public boolean price_subtotalDirtyFlag;
    
    /**
     * 税额总计
     */
    public Double price_tax;

    @JsonIgnore
    public boolean price_taxDirtyFlag;
    
    /**
     * 总计
     */
    public Double price_total;

    @JsonIgnore
    public boolean price_totalDirtyFlag;
    
    /**
     * 单价
     */
    public Double price_unit;

    @JsonIgnore
    public boolean price_unitDirtyFlag;
    
    /**
     * 用户输入自定义产品属性值
     */
    public String product_custom_attribute_value_ids;

    @JsonIgnore
    public boolean product_custom_attribute_value_idsDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品图片
     */
    public byte[] product_image;

    @JsonIgnore
    public boolean product_imageDirtyFlag;
    
    /**
     * 没有创建变量的产品属性值
     */
    public String product_no_variant_attribute_value_ids;

    @JsonIgnore
    public boolean product_no_variant_attribute_value_idsDirtyFlag;
    
    /**
     * 包裹
     */
    public Integer product_packaging;

    @JsonIgnore
    public boolean product_packagingDirtyFlag;
    
    /**
     * 包裹
     */
    public String product_packaging_text;

    @JsonIgnore
    public boolean product_packaging_textDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 订购数量
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 允许编辑
     */
    public String product_updatable;

    @JsonIgnore
    public boolean product_updatableDirtyFlag;
    
    /**
     * 生成采购订单号
     */
    public Integer purchase_line_count;

    @JsonIgnore
    public boolean purchase_line_countDirtyFlag;
    
    /**
     * 生成采购订单明细行
     */
    public String purchase_line_ids;

    @JsonIgnore
    public boolean purchase_line_idsDirtyFlag;
    
    /**
     * 已交货数量
     */
    public Double qty_delivered;

    @JsonIgnore
    public boolean qty_deliveredDirtyFlag;
    
    /**
     * 手动发货
     */
    public Double qty_delivered_manual;

    @JsonIgnore
    public boolean qty_delivered_manualDirtyFlag;
    
    /**
     * 更新数量的方法
     */
    public String qty_delivered_method;

    @JsonIgnore
    public boolean qty_delivered_methodDirtyFlag;
    
    /**
     * 已开发票数量
     */
    public Double qty_invoiced;

    @JsonIgnore
    public boolean qty_invoicedDirtyFlag;
    
    /**
     * 发票数量
     */
    public Double qty_to_invoice;

    @JsonIgnore
    public boolean qty_to_invoiceDirtyFlag;
    
    /**
     * 路线
     */
    public Integer route_id;

    @JsonIgnore
    public boolean route_idDirtyFlag;
    
    /**
     * 路线
     */
    public String route_id_text;

    @JsonIgnore
    public boolean route_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer salesman_id;

    @JsonIgnore
    public boolean salesman_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String salesman_id_text;

    @JsonIgnore
    public boolean salesman_id_textDirtyFlag;
    
    /**
     * 可选产品行
     */
    public String sale_order_option_ids;

    @JsonIgnore
    public boolean sale_order_option_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 订单状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 税率
     */
    public String tax_id;

    @JsonIgnore
    public boolean tax_idDirtyFlag;
    
    /**
     * 未含税的发票金额
     */
    public Double untaxed_amount_invoiced;

    @JsonIgnore
    public boolean untaxed_amount_invoicedDirtyFlag;
    
    /**
     * 不含税待开票金额
     */
    public Double untaxed_amount_to_invoice;

    @JsonIgnore
    public boolean untaxed_amount_to_invoiceDirtyFlag;
    
    /**
     * 警告
     */
    public String warning_stock;

    @JsonIgnore
    public boolean warning_stockDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析明细行]
     */
    @JsonProperty("analytic_line_ids")
    public String getAnalytic_line_ids(){
        return this.analytic_line_ids ;
    }

    /**
     * 设置 [分析明细行]
     */
    @JsonProperty("analytic_line_ids")
    public void setAnalytic_line_ids(String  analytic_line_ids){
        this.analytic_line_ids = analytic_line_ids ;
        this.analytic_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析明细行]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_line_idsDirtyFlag(){
        return this.analytic_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [交货提前时间]
     */
    @JsonProperty("customer_lead")
    public Double getCustomer_lead(){
        return this.customer_lead ;
    }

    /**
     * 设置 [交货提前时间]
     */
    @JsonProperty("customer_lead")
    public void setCustomer_lead(Double  customer_lead){
        this.customer_lead = customer_lead ;
        this.customer_leadDirtyFlag = true ;
    }

     /**
     * 获取 [交货提前时间]脏标记
     */
    @JsonIgnore
    public boolean getCustomer_leadDirtyFlag(){
        return this.customer_leadDirtyFlag ;
    }   

    /**
     * 获取 [折扣(%)]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return this.discount ;
    }

    /**
     * 设置 [折扣(%)]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

     /**
     * 获取 [折扣(%)]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return this.discountDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [显示类型]
     */
    @JsonProperty("display_type")
    public String getDisplay_type(){
        return this.display_type ;
    }

    /**
     * 设置 [显示类型]
     */
    @JsonProperty("display_type")
    public void setDisplay_type(String  display_type){
        this.display_type = display_type ;
        this.display_typeDirtyFlag = true ;
    }

     /**
     * 获取 [显示类型]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_typeDirtyFlag(){
        return this.display_typeDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return this.event_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return this.event_idDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return this.event_id_text ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return this.event_id_textDirtyFlag ;
    }   

    /**
     * 获取 [是一张活动票吗？]
     */
    @JsonProperty("event_ok")
    public String getEvent_ok(){
        return this.event_ok ;
    }

    /**
     * 设置 [是一张活动票吗？]
     */
    @JsonProperty("event_ok")
    public void setEvent_ok(String  event_ok){
        this.event_ok = event_ok ;
        this.event_okDirtyFlag = true ;
    }

     /**
     * 获取 [是一张活动票吗？]脏标记
     */
    @JsonIgnore
    public boolean getEvent_okDirtyFlag(){
        return this.event_okDirtyFlag ;
    }   

    /**
     * 获取 [活动入场券]
     */
    @JsonProperty("event_ticket_id")
    public Integer getEvent_ticket_id(){
        return this.event_ticket_id ;
    }

    /**
     * 设置 [活动入场券]
     */
    @JsonProperty("event_ticket_id")
    public void setEvent_ticket_id(Integer  event_ticket_id){
        this.event_ticket_id = event_ticket_id ;
        this.event_ticket_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动入场券]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idDirtyFlag(){
        return this.event_ticket_idDirtyFlag ;
    }   

    /**
     * 获取 [活动入场券]
     */
    @JsonProperty("event_ticket_id_text")
    public String getEvent_ticket_id_text(){
        return this.event_ticket_id_text ;
    }

    /**
     * 设置 [活动入场券]
     */
    @JsonProperty("event_ticket_id_text")
    public void setEvent_ticket_id_text(String  event_ticket_id_text){
        this.event_ticket_id_text = event_ticket_id_text ;
        this.event_ticket_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动入场券]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_id_textDirtyFlag(){
        return this.event_ticket_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票行]
     */
    @JsonProperty("invoice_lines")
    public String getInvoice_lines(){
        return this.invoice_lines ;
    }

    /**
     * 设置 [发票行]
     */
    @JsonProperty("invoice_lines")
    public void setInvoice_lines(String  invoice_lines){
        this.invoice_lines = invoice_lines ;
        this.invoice_linesDirtyFlag = true ;
    }

     /**
     * 获取 [发票行]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_linesDirtyFlag(){
        return this.invoice_linesDirtyFlag ;
    }   

    /**
     * 获取 [发票状态]
     */
    @JsonProperty("invoice_status")
    public String getInvoice_status(){
        return this.invoice_status ;
    }

    /**
     * 设置 [发票状态]
     */
    @JsonProperty("invoice_status")
    public void setInvoice_status(String  invoice_status){
        this.invoice_status = invoice_status ;
        this.invoice_statusDirtyFlag = true ;
    }

     /**
     * 获取 [发票状态]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_statusDirtyFlag(){
        return this.invoice_statusDirtyFlag ;
    }   

    /**
     * 获取 [是首付款吗？]
     */
    @JsonProperty("is_downpayment")
    public String getIs_downpayment(){
        return this.is_downpayment ;
    }

    /**
     * 设置 [是首付款吗？]
     */
    @JsonProperty("is_downpayment")
    public void setIs_downpayment(String  is_downpayment){
        this.is_downpayment = is_downpayment ;
        this.is_downpaymentDirtyFlag = true ;
    }

     /**
     * 获取 [是首付款吗？]脏标记
     */
    @JsonIgnore
    public boolean getIs_downpaymentDirtyFlag(){
        return this.is_downpaymentDirtyFlag ;
    }   

    /**
     * 获取 [可报销]
     */
    @JsonProperty("is_expense")
    public String getIs_expense(){
        return this.is_expense ;
    }

    /**
     * 设置 [可报销]
     */
    @JsonProperty("is_expense")
    public void setIs_expense(String  is_expense){
        this.is_expense = is_expense ;
        this.is_expenseDirtyFlag = true ;
    }

     /**
     * 获取 [可报销]脏标记
     */
    @JsonIgnore
    public boolean getIs_expenseDirtyFlag(){
        return this.is_expenseDirtyFlag ;
    }   

    /**
     * 获取 [链接的订单明细]
     */
    @JsonProperty("linked_line_id")
    public Integer getLinked_line_id(){
        return this.linked_line_id ;
    }

    /**
     * 设置 [链接的订单明细]
     */
    @JsonProperty("linked_line_id")
    public void setLinked_line_id(Integer  linked_line_id){
        this.linked_line_id = linked_line_id ;
        this.linked_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [链接的订单明细]脏标记
     */
    @JsonIgnore
    public boolean getLinked_line_idDirtyFlag(){
        return this.linked_line_idDirtyFlag ;
    }   

    /**
     * 获取 [链接的订单明细]
     */
    @JsonProperty("linked_line_id_text")
    public String getLinked_line_id_text(){
        return this.linked_line_id_text ;
    }

    /**
     * 设置 [链接的订单明细]
     */
    @JsonProperty("linked_line_id_text")
    public void setLinked_line_id_text(String  linked_line_id_text){
        this.linked_line_id_text = linked_line_id_text ;
        this.linked_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [链接的订单明细]脏标记
     */
    @JsonIgnore
    public boolean getLinked_line_id_textDirtyFlag(){
        return this.linked_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return this.move_ids ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return this.move_idsDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [姓名简称]
     */
    @JsonProperty("name_short")
    public String getName_short(){
        return this.name_short ;
    }

    /**
     * 设置 [姓名简称]
     */
    @JsonProperty("name_short")
    public void setName_short(String  name_short){
        this.name_short = name_short ;
        this.name_shortDirtyFlag = true ;
    }

     /**
     * 获取 [姓名简称]脏标记
     */
    @JsonIgnore
    public boolean getName_shortDirtyFlag(){
        return this.name_shortDirtyFlag ;
    }   

    /**
     * 获取 [链接选项]
     */
    @JsonProperty("option_line_ids")
    public String getOption_line_ids(){
        return this.option_line_ids ;
    }

    /**
     * 设置 [链接选项]
     */
    @JsonProperty("option_line_ids")
    public void setOption_line_ids(String  option_line_ids){
        this.option_line_ids = option_line_ids ;
        this.option_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [链接选项]脏标记
     */
    @JsonIgnore
    public boolean getOption_line_idsDirtyFlag(){
        return this.option_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return this.order_id ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return this.order_idDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return this.order_id_text ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return this.order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("order_partner_id")
    public Integer getOrder_partner_id(){
        return this.order_partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("order_partner_id")
    public void setOrder_partner_id(Integer  order_partner_id){
        this.order_partner_id = order_partner_id ;
        this.order_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getOrder_partner_idDirtyFlag(){
        return this.order_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("order_partner_id_text")
    public String getOrder_partner_id_text(){
        return this.order_partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("order_partner_id_text")
    public void setOrder_partner_id_text(String  order_partner_id_text){
        this.order_partner_id_text = order_partner_id_text ;
        this.order_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getOrder_partner_id_textDirtyFlag(){
        return this.order_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [降价]
     */
    @JsonProperty("price_reduce")
    public Double getPrice_reduce(){
        return this.price_reduce ;
    }

    /**
     * 设置 [降价]
     */
    @JsonProperty("price_reduce")
    public void setPrice_reduce(Double  price_reduce){
        this.price_reduce = price_reduce ;
        this.price_reduceDirtyFlag = true ;
    }

     /**
     * 获取 [降价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduceDirtyFlag(){
        return this.price_reduceDirtyFlag ;
    }   

    /**
     * 获取 [不含税价]
     */
    @JsonProperty("price_reduce_taxexcl")
    public Double getPrice_reduce_taxexcl(){
        return this.price_reduce_taxexcl ;
    }

    /**
     * 设置 [不含税价]
     */
    @JsonProperty("price_reduce_taxexcl")
    public void setPrice_reduce_taxexcl(Double  price_reduce_taxexcl){
        this.price_reduce_taxexcl = price_reduce_taxexcl ;
        this.price_reduce_taxexclDirtyFlag = true ;
    }

     /**
     * 获取 [不含税价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxexclDirtyFlag(){
        return this.price_reduce_taxexclDirtyFlag ;
    }   

    /**
     * 获取 [减税后价格]
     */
    @JsonProperty("price_reduce_taxinc")
    public Double getPrice_reduce_taxinc(){
        return this.price_reduce_taxinc ;
    }

    /**
     * 设置 [减税后价格]
     */
    @JsonProperty("price_reduce_taxinc")
    public void setPrice_reduce_taxinc(Double  price_reduce_taxinc){
        this.price_reduce_taxinc = price_reduce_taxinc ;
        this.price_reduce_taxincDirtyFlag = true ;
    }

     /**
     * 获取 [减税后价格]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxincDirtyFlag(){
        return this.price_reduce_taxincDirtyFlag ;
    }   

    /**
     * 获取 [小计]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return this.price_subtotal ;
    }

    /**
     * 设置 [小计]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [小计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return this.price_subtotalDirtyFlag ;
    }   

    /**
     * 获取 [税额总计]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return this.price_tax ;
    }

    /**
     * 设置 [税额总计]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

     /**
     * 获取 [税额总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return this.price_taxDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }   

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }   

    /**
     * 获取 [用户输入自定义产品属性值]
     */
    @JsonProperty("product_custom_attribute_value_ids")
    public String getProduct_custom_attribute_value_ids(){
        return this.product_custom_attribute_value_ids ;
    }

    /**
     * 设置 [用户输入自定义产品属性值]
     */
    @JsonProperty("product_custom_attribute_value_ids")
    public void setProduct_custom_attribute_value_ids(String  product_custom_attribute_value_ids){
        this.product_custom_attribute_value_ids = product_custom_attribute_value_ids ;
        this.product_custom_attribute_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [用户输入自定义产品属性值]脏标记
     */
    @JsonIgnore
    public boolean getProduct_custom_attribute_value_idsDirtyFlag(){
        return this.product_custom_attribute_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品图片]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return this.product_image ;
    }

    /**
     * 设置 [产品图片]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

     /**
     * 获取 [产品图片]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return this.product_imageDirtyFlag ;
    }   

    /**
     * 获取 [没有创建变量的产品属性值]
     */
    @JsonProperty("product_no_variant_attribute_value_ids")
    public String getProduct_no_variant_attribute_value_ids(){
        return this.product_no_variant_attribute_value_ids ;
    }

    /**
     * 设置 [没有创建变量的产品属性值]
     */
    @JsonProperty("product_no_variant_attribute_value_ids")
    public void setProduct_no_variant_attribute_value_ids(String  product_no_variant_attribute_value_ids){
        this.product_no_variant_attribute_value_ids = product_no_variant_attribute_value_ids ;
        this.product_no_variant_attribute_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [没有创建变量的产品属性值]脏标记
     */
    @JsonIgnore
    public boolean getProduct_no_variant_attribute_value_idsDirtyFlag(){
        return this.product_no_variant_attribute_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("product_packaging")
    public Integer getProduct_packaging(){
        return this.product_packaging ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("product_packaging")
    public void setProduct_packaging(Integer  product_packaging){
        this.product_packaging = product_packaging ;
        this.product_packagingDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packagingDirtyFlag(){
        return this.product_packagingDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("product_packaging_text")
    public String getProduct_packaging_text(){
        return this.product_packaging_text ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("product_packaging_text")
    public void setProduct_packaging_text(String  product_packaging_text){
        this.product_packaging_text = product_packaging_text ;
        this.product_packaging_textDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packaging_textDirtyFlag(){
        return this.product_packaging_textDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [订购数量]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [订购数量]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [订购数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [允许编辑]
     */
    @JsonProperty("product_updatable")
    public String getProduct_updatable(){
        return this.product_updatable ;
    }

    /**
     * 设置 [允许编辑]
     */
    @JsonProperty("product_updatable")
    public void setProduct_updatable(String  product_updatable){
        this.product_updatable = product_updatable ;
        this.product_updatableDirtyFlag = true ;
    }

     /**
     * 获取 [允许编辑]脏标记
     */
    @JsonIgnore
    public boolean getProduct_updatableDirtyFlag(){
        return this.product_updatableDirtyFlag ;
    }   

    /**
     * 获取 [生成采购订单号]
     */
    @JsonProperty("purchase_line_count")
    public Integer getPurchase_line_count(){
        return this.purchase_line_count ;
    }

    /**
     * 设置 [生成采购订单号]
     */
    @JsonProperty("purchase_line_count")
    public void setPurchase_line_count(Integer  purchase_line_count){
        this.purchase_line_count = purchase_line_count ;
        this.purchase_line_countDirtyFlag = true ;
    }

     /**
     * 获取 [生成采购订单号]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_countDirtyFlag(){
        return this.purchase_line_countDirtyFlag ;
    }   

    /**
     * 获取 [生成采购订单明细行]
     */
    @JsonProperty("purchase_line_ids")
    public String getPurchase_line_ids(){
        return this.purchase_line_ids ;
    }

    /**
     * 设置 [生成采购订单明细行]
     */
    @JsonProperty("purchase_line_ids")
    public void setPurchase_line_ids(String  purchase_line_ids){
        this.purchase_line_ids = purchase_line_ids ;
        this.purchase_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [生成采购订单明细行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idsDirtyFlag(){
        return this.purchase_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [已交货数量]
     */
    @JsonProperty("qty_delivered")
    public Double getQty_delivered(){
        return this.qty_delivered ;
    }

    /**
     * 设置 [已交货数量]
     */
    @JsonProperty("qty_delivered")
    public void setQty_delivered(Double  qty_delivered){
        this.qty_delivered = qty_delivered ;
        this.qty_deliveredDirtyFlag = true ;
    }

     /**
     * 获取 [已交货数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_deliveredDirtyFlag(){
        return this.qty_deliveredDirtyFlag ;
    }   

    /**
     * 获取 [手动发货]
     */
    @JsonProperty("qty_delivered_manual")
    public Double getQty_delivered_manual(){
        return this.qty_delivered_manual ;
    }

    /**
     * 设置 [手动发货]
     */
    @JsonProperty("qty_delivered_manual")
    public void setQty_delivered_manual(Double  qty_delivered_manual){
        this.qty_delivered_manual = qty_delivered_manual ;
        this.qty_delivered_manualDirtyFlag = true ;
    }

     /**
     * 获取 [手动发货]脏标记
     */
    @JsonIgnore
    public boolean getQty_delivered_manualDirtyFlag(){
        return this.qty_delivered_manualDirtyFlag ;
    }   

    /**
     * 获取 [更新数量的方法]
     */
    @JsonProperty("qty_delivered_method")
    public String getQty_delivered_method(){
        return this.qty_delivered_method ;
    }

    /**
     * 设置 [更新数量的方法]
     */
    @JsonProperty("qty_delivered_method")
    public void setQty_delivered_method(String  qty_delivered_method){
        this.qty_delivered_method = qty_delivered_method ;
        this.qty_delivered_methodDirtyFlag = true ;
    }

     /**
     * 获取 [更新数量的方法]脏标记
     */
    @JsonIgnore
    public boolean getQty_delivered_methodDirtyFlag(){
        return this.qty_delivered_methodDirtyFlag ;
    }   

    /**
     * 获取 [已开发票数量]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return this.qty_invoiced ;
    }

    /**
     * 设置 [已开发票数量]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [已开发票数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return this.qty_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [发票数量]
     */
    @JsonProperty("qty_to_invoice")
    public Double getQty_to_invoice(){
        return this.qty_to_invoice ;
    }

    /**
     * 设置 [发票数量]
     */
    @JsonProperty("qty_to_invoice")
    public void setQty_to_invoice(Double  qty_to_invoice){
        this.qty_to_invoice = qty_to_invoice ;
        this.qty_to_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [发票数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_to_invoiceDirtyFlag(){
        return this.qty_to_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_id")
    public Integer getRoute_id(){
        return this.route_id ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_id")
    public void setRoute_id(Integer  route_id){
        this.route_id = route_id ;
        this.route_idDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idDirtyFlag(){
        return this.route_idDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_id_text")
    public String getRoute_id_text(){
        return this.route_id_text ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_id_text")
    public void setRoute_id_text(String  route_id_text){
        this.route_id_text = route_id_text ;
        this.route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_id_textDirtyFlag(){
        return this.route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("salesman_id")
    public Integer getSalesman_id(){
        return this.salesman_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("salesman_id")
    public void setSalesman_id(Integer  salesman_id){
        this.salesman_id = salesman_id ;
        this.salesman_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getSalesman_idDirtyFlag(){
        return this.salesman_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("salesman_id_text")
    public String getSalesman_id_text(){
        return this.salesman_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("salesman_id_text")
    public void setSalesman_id_text(String  salesman_id_text){
        this.salesman_id_text = salesman_id_text ;
        this.salesman_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getSalesman_id_textDirtyFlag(){
        return this.salesman_id_textDirtyFlag ;
    }   

    /**
     * 获取 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public String getSale_order_option_ids(){
        return this.sale_order_option_ids ;
    }

    /**
     * 设置 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public void setSale_order_option_ids(String  sale_order_option_ids){
        this.sale_order_option_ids = sale_order_option_ids ;
        this.sale_order_option_idsDirtyFlag = true ;
    }

     /**
     * 获取 [可选产品行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_option_idsDirtyFlag(){
        return this.sale_order_option_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [订单状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [订单状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [订单状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("tax_id")
    public String getTax_id(){
        return this.tax_id ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("tax_id")
    public void setTax_id(String  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return this.tax_idDirtyFlag ;
    }   

    /**
     * 获取 [未含税的发票金额]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public Double getUntaxed_amount_invoiced(){
        return this.untaxed_amount_invoiced ;
    }

    /**
     * 设置 [未含税的发票金额]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public void setUntaxed_amount_invoiced(Double  untaxed_amount_invoiced){
        this.untaxed_amount_invoiced = untaxed_amount_invoiced ;
        this.untaxed_amount_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [未含税的发票金额]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_invoicedDirtyFlag(){
        return this.untaxed_amount_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [不含税待开票金额]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public Double getUntaxed_amount_to_invoice(){
        return this.untaxed_amount_to_invoice ;
    }

    /**
     * 设置 [不含税待开票金额]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public void setUntaxed_amount_to_invoice(Double  untaxed_amount_to_invoice){
        this.untaxed_amount_to_invoice = untaxed_amount_to_invoice ;
        this.untaxed_amount_to_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [不含税待开票金额]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_to_invoiceDirtyFlag(){
        return this.untaxed_amount_to_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [警告]
     */
    @JsonProperty("warning_stock")
    public String getWarning_stock(){
        return this.warning_stock ;
    }

    /**
     * 设置 [警告]
     */
    @JsonProperty("warning_stock")
    public void setWarning_stock(String  warning_stock){
        this.warning_stock = warning_stock ;
        this.warning_stockDirtyFlag = true ;
    }

     /**
     * 获取 [警告]脏标记
     */
    @JsonIgnore
    public boolean getWarning_stockDirtyFlag(){
        return this.warning_stockDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

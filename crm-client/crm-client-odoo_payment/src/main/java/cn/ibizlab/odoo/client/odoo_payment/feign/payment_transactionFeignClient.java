package cn.ibizlab.odoo.client.odoo_payment.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_transactionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[payment_transaction] 服务对象接口
 */
public interface payment_transactionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_transactions/removebatch")
    public payment_transactionImpl removeBatch(@RequestBody List<payment_transactionImpl> payment_transactions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_transactions")
    public payment_transactionImpl create(@RequestBody payment_transactionImpl payment_transaction);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_transactions/fetchdefault")
    public Page<payment_transactionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_transactions/createbatch")
    public payment_transactionImpl createBatch(@RequestBody List<payment_transactionImpl> payment_transactions);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_transactions/updatebatch")
    public payment_transactionImpl updateBatch(@RequestBody List<payment_transactionImpl> payment_transactions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_transactions/{id}")
    public payment_transactionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_transactions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_transactions/{id}")
    public payment_transactionImpl update(@PathVariable("id") Integer id,@RequestBody payment_transactionImpl payment_transaction);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_transactions/select")
    public Page<payment_transactionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_transactions/{id}/getdraft")
    public payment_transactionImpl getDraft(@PathVariable("id") Integer id,@RequestBody payment_transactionImpl payment_transaction);



}

package cn.ibizlab.odoo.client.odoo_payment.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface payment_acquirer_onboarding_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirer_onboarding_wizards/updatebatch")
    public payment_acquirer_onboarding_wizardImpl updateBatch(@RequestBody List<payment_acquirer_onboarding_wizardImpl> payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirer_onboarding_wizards")
    public payment_acquirer_onboarding_wizardImpl create(@RequestBody payment_acquirer_onboarding_wizardImpl payment_acquirer_onboarding_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirer_onboarding_wizards/fetchdefault")
    public Page<payment_acquirer_onboarding_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{id}")
    public payment_acquirer_onboarding_wizardImpl update(@PathVariable("id") Integer id,@RequestBody payment_acquirer_onboarding_wizardImpl payment_acquirer_onboarding_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirer_onboarding_wizards/createbatch")
    public payment_acquirer_onboarding_wizardImpl createBatch(@RequestBody List<payment_acquirer_onboarding_wizardImpl> payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{id}")
    public payment_acquirer_onboarding_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirer_onboarding_wizards/removebatch")
    public payment_acquirer_onboarding_wizardImpl removeBatch(@RequestBody List<payment_acquirer_onboarding_wizardImpl> payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirer_onboarding_wizards/select")
    public Page<payment_acquirer_onboarding_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{id}/getdraft")
    public payment_acquirer_onboarding_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody payment_acquirer_onboarding_wizardImpl payment_acquirer_onboarding_wizard);



}

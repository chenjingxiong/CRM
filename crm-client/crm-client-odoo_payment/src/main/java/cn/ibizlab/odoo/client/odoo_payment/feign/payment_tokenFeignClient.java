package cn.ibizlab.odoo.client.odoo_payment.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipayment_token;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_tokenImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[payment_token] 服务对象接口
 */
public interface payment_tokenFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_tokens/{id}")
    public payment_tokenImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_tokens/updatebatch")
    public payment_tokenImpl updateBatch(@RequestBody List<payment_tokenImpl> payment_tokens);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_tokens/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_tokens/createbatch")
    public payment_tokenImpl createBatch(@RequestBody List<payment_tokenImpl> payment_tokens);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_tokens")
    public payment_tokenImpl create(@RequestBody payment_tokenImpl payment_token);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_tokens/removebatch")
    public payment_tokenImpl removeBatch(@RequestBody List<payment_tokenImpl> payment_tokens);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_tokens/fetchdefault")
    public Page<payment_tokenImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_tokens/{id}")
    public payment_tokenImpl update(@PathVariable("id") Integer id,@RequestBody payment_tokenImpl payment_token);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_tokens/select")
    public Page<payment_tokenImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_tokens/{id}/getdraft")
    public payment_tokenImpl getDraft(@PathVariable("id") Integer id,@RequestBody payment_tokenImpl payment_token);



}

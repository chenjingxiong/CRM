package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;
import cn.ibizlab.odoo.client.odoo_payment.config.odoo_paymentClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipayment_acquirerClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirerImpl;
import cn.ibizlab.odoo.client.odoo_payment.feign.payment_acquirerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[payment_acquirer] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class payment_acquirerClientServiceImpl implements Ipayment_acquirerClientService {

    payment_acquirerFeignClient payment_acquirerFeignClient;

    @Autowired
    public payment_acquirerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_paymentClientProperties odoo_paymentClientProperties) {
        if (odoo_paymentClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_acquirerFeignClient = nameBuilder.target(payment_acquirerFeignClient.class,"http://"+odoo_paymentClientProperties.getServiceId()+"/") ;
		}else if (odoo_paymentClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_acquirerFeignClient = nameBuilder.target(payment_acquirerFeignClient.class,odoo_paymentClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipayment_acquirer createModel() {
		return new payment_acquirerImpl();
	}


    public void update(Ipayment_acquirer payment_acquirer){
        Ipayment_acquirer clientModel = payment_acquirerFeignClient.update(payment_acquirer.getId(),(payment_acquirerImpl)payment_acquirer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer.getClass(), false);
        copier.copy(clientModel, payment_acquirer, null);
    }


    public void create(Ipayment_acquirer payment_acquirer){
        Ipayment_acquirer clientModel = payment_acquirerFeignClient.create((payment_acquirerImpl)payment_acquirer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer.getClass(), false);
        copier.copy(clientModel, payment_acquirer, null);
    }


    public void createBatch(List<Ipayment_acquirer> payment_acquirers){
        if(payment_acquirers!=null){
            List<payment_acquirerImpl> list = new ArrayList<payment_acquirerImpl>();
            for(Ipayment_acquirer ipayment_acquirer :payment_acquirers){
                list.add((payment_acquirerImpl)ipayment_acquirer) ;
            }
            payment_acquirerFeignClient.createBatch(list) ;
        }
    }


    public void get(Ipayment_acquirer payment_acquirer){
        Ipayment_acquirer clientModel = payment_acquirerFeignClient.get(payment_acquirer.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer.getClass(), false);
        copier.copy(clientModel, payment_acquirer, null);
    }


    public void removeBatch(List<Ipayment_acquirer> payment_acquirers){
        if(payment_acquirers!=null){
            List<payment_acquirerImpl> list = new ArrayList<payment_acquirerImpl>();
            for(Ipayment_acquirer ipayment_acquirer :payment_acquirers){
                list.add((payment_acquirerImpl)ipayment_acquirer) ;
            }
            payment_acquirerFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ipayment_acquirer payment_acquirer){
        payment_acquirerFeignClient.remove(payment_acquirer.getId()) ;
    }


    public void updateBatch(List<Ipayment_acquirer> payment_acquirers){
        if(payment_acquirers!=null){
            List<payment_acquirerImpl> list = new ArrayList<payment_acquirerImpl>();
            for(Ipayment_acquirer ipayment_acquirer :payment_acquirers){
                list.add((payment_acquirerImpl)ipayment_acquirer) ;
            }
            payment_acquirerFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ipayment_acquirer> fetchDefault(SearchContext context){
        Page<payment_acquirerImpl> page = this.payment_acquirerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ipayment_acquirer> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipayment_acquirer payment_acquirer){
        Ipayment_acquirer clientModel = payment_acquirerFeignClient.getDraft(payment_acquirer.getId(),(payment_acquirerImpl)payment_acquirer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer.getClass(), false);
        copier.copy(clientModel, payment_acquirer, null);
    }



}


package cn.ibizlab.odoo.client.odoo_sms.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isms_api;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_apiImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sms_api] 服务对象接口
 */
public interface sms_apiFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_apis")
    public sms_apiImpl create(@RequestBody sms_apiImpl sms_api);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_apis/updatebatch")
    public sms_apiImpl updateBatch(@RequestBody List<sms_apiImpl> sms_apis);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_apis/removebatch")
    public sms_apiImpl removeBatch(@RequestBody List<sms_apiImpl> sms_apis);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_apis/fetchdefault")
    public Page<sms_apiImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_apis/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_apis/{id}")
    public sms_apiImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_apis/{id}")
    public sms_apiImpl update(@PathVariable("id") Integer id,@RequestBody sms_apiImpl sms_api);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_apis/createbatch")
    public sms_apiImpl createBatch(@RequestBody List<sms_apiImpl> sms_apis);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_apis/select")
    public Page<sms_apiImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_apis/{id}/getdraft")
    public sms_apiImpl getDraft(@PathVariable("id") Integer id,@RequestBody sms_apiImpl sms_api);



}

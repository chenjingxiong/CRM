package cn.ibizlab.odoo.client.odoo_sms.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isms_send_sms;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_send_smsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sms_send_sms] 服务对象接口
 */
public interface sms_send_smsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_send_sms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_send_sms/updatebatch")
    public sms_send_smsImpl updateBatch(@RequestBody List<sms_send_smsImpl> sms_send_sms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_send_sms/{id}")
    public sms_send_smsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_send_sms")
    public sms_send_smsImpl create(@RequestBody sms_send_smsImpl sms_send_sms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_send_sms/removebatch")
    public sms_send_smsImpl removeBatch(@RequestBody List<sms_send_smsImpl> sms_send_sms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_send_sms/{id}")
    public sms_send_smsImpl update(@PathVariable("id") Integer id,@RequestBody sms_send_smsImpl sms_send_sms);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_send_sms/createbatch")
    public sms_send_smsImpl createBatch(@RequestBody List<sms_send_smsImpl> sms_send_sms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_send_sms/fetchdefault")
    public Page<sms_send_smsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_send_sms/select")
    public Page<sms_send_smsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_send_sms/{id}/getdraft")
    public sms_send_smsImpl getDraft(@PathVariable("id") Integer id,@RequestBody sms_send_smsImpl sms_send_sms);



}

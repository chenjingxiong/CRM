package cn.ibizlab.odoo.client.odoo_rating.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_ratingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[rating_rating] 服务对象接口
 */
public interface rating_ratingFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_ratings/{id}")
    public rating_ratingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_ratings/{id}")
    public rating_ratingImpl update(@PathVariable("id") Integer id,@RequestBody rating_ratingImpl rating_rating);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_ratings")
    public rating_ratingImpl create(@RequestBody rating_ratingImpl rating_rating);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_ratings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_ratings/updatebatch")
    public rating_ratingImpl updateBatch(@RequestBody List<rating_ratingImpl> rating_ratings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_ratings/removebatch")
    public rating_ratingImpl removeBatch(@RequestBody List<rating_ratingImpl> rating_ratings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_ratings/fetchdefault")
    public Page<rating_ratingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_ratings/createbatch")
    public rating_ratingImpl createBatch(@RequestBody List<rating_ratingImpl> rating_ratings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_ratings/select")
    public Page<rating_ratingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_ratings/{id}/getdraft")
    public rating_ratingImpl getDraft(@PathVariable("id") Integer id,@RequestBody rating_ratingImpl rating_rating);



}

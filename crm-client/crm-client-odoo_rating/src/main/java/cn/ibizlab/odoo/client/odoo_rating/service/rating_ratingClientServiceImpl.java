package cn.ibizlab.odoo.client.odoo_rating.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.client.odoo_rating.config.odoo_ratingClientProperties;
import cn.ibizlab.odoo.core.client.service.Irating_ratingClientService;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_ratingImpl;
import cn.ibizlab.odoo.client.odoo_rating.feign.rating_ratingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[rating_rating] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class rating_ratingClientServiceImpl implements Irating_ratingClientService {

    rating_ratingFeignClient rating_ratingFeignClient;

    @Autowired
    public rating_ratingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_ratingClientProperties odoo_ratingClientProperties) {
        if (odoo_ratingClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.rating_ratingFeignClient = nameBuilder.target(rating_ratingFeignClient.class,"http://"+odoo_ratingClientProperties.getServiceId()+"/") ;
		}else if (odoo_ratingClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.rating_ratingFeignClient = nameBuilder.target(rating_ratingFeignClient.class,odoo_ratingClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irating_rating createModel() {
		return new rating_ratingImpl();
	}


    public void get(Irating_rating rating_rating){
        Irating_rating clientModel = rating_ratingFeignClient.get(rating_rating.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_rating.getClass(), false);
        copier.copy(clientModel, rating_rating, null);
    }


    public void update(Irating_rating rating_rating){
        Irating_rating clientModel = rating_ratingFeignClient.update(rating_rating.getId(),(rating_ratingImpl)rating_rating) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_rating.getClass(), false);
        copier.copy(clientModel, rating_rating, null);
    }


    public void create(Irating_rating rating_rating){
        Irating_rating clientModel = rating_ratingFeignClient.create((rating_ratingImpl)rating_rating) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_rating.getClass(), false);
        copier.copy(clientModel, rating_rating, null);
    }


    public void remove(Irating_rating rating_rating){
        rating_ratingFeignClient.remove(rating_rating.getId()) ;
    }


    public void updateBatch(List<Irating_rating> rating_ratings){
        if(rating_ratings!=null){
            List<rating_ratingImpl> list = new ArrayList<rating_ratingImpl>();
            for(Irating_rating irating_rating :rating_ratings){
                list.add((rating_ratingImpl)irating_rating) ;
            }
            rating_ratingFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Irating_rating> rating_ratings){
        if(rating_ratings!=null){
            List<rating_ratingImpl> list = new ArrayList<rating_ratingImpl>();
            for(Irating_rating irating_rating :rating_ratings){
                list.add((rating_ratingImpl)irating_rating) ;
            }
            rating_ratingFeignClient.removeBatch(list) ;
        }
    }


    public Page<Irating_rating> fetchDefault(SearchContext context){
        Page<rating_ratingImpl> page = this.rating_ratingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Irating_rating> rating_ratings){
        if(rating_ratings!=null){
            List<rating_ratingImpl> list = new ArrayList<rating_ratingImpl>();
            for(Irating_rating irating_rating :rating_ratings){
                list.add((rating_ratingImpl)irating_rating) ;
            }
            rating_ratingFeignClient.createBatch(list) ;
        }
    }


    public Page<Irating_rating> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irating_rating rating_rating){
        Irating_rating clientModel = rating_ratingFeignClient.getDraft(rating_rating.getId(),(rating_ratingImpl)rating_rating) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_rating.getClass(), false);
        copier.copy(clientModel, rating_rating, null);
    }



}


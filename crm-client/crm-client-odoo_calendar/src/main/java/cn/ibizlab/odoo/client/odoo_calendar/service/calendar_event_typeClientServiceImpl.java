package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_event_type;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_event_typeClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_event_typeImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_event_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_event_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_event_typeClientServiceImpl implements Icalendar_event_typeClientService {

    calendar_event_typeFeignClient calendar_event_typeFeignClient;

    @Autowired
    public calendar_event_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_event_typeFeignClient = nameBuilder.target(calendar_event_typeFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_event_typeFeignClient = nameBuilder.target(calendar_event_typeFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_event_type createModel() {
		return new calendar_event_typeImpl();
	}


    public void createBatch(List<Icalendar_event_type> calendar_event_types){
        if(calendar_event_types!=null){
            List<calendar_event_typeImpl> list = new ArrayList<calendar_event_typeImpl>();
            for(Icalendar_event_type icalendar_event_type :calendar_event_types){
                list.add((calendar_event_typeImpl)icalendar_event_type) ;
            }
            calendar_event_typeFeignClient.createBatch(list) ;
        }
    }


    public Page<Icalendar_event_type> fetchDefault(SearchContext context){
        Page<calendar_event_typeImpl> page = this.calendar_event_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Icalendar_event_type calendar_event_type){
        Icalendar_event_type clientModel = calendar_event_typeFeignClient.update(calendar_event_type.getId(),(calendar_event_typeImpl)calendar_event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event_type.getClass(), false);
        copier.copy(clientModel, calendar_event_type, null);
    }


    public void get(Icalendar_event_type calendar_event_type){
        Icalendar_event_type clientModel = calendar_event_typeFeignClient.get(calendar_event_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event_type.getClass(), false);
        copier.copy(clientModel, calendar_event_type, null);
    }


    public void create(Icalendar_event_type calendar_event_type){
        Icalendar_event_type clientModel = calendar_event_typeFeignClient.create((calendar_event_typeImpl)calendar_event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event_type.getClass(), false);
        copier.copy(clientModel, calendar_event_type, null);
    }


    public void remove(Icalendar_event_type calendar_event_type){
        calendar_event_typeFeignClient.remove(calendar_event_type.getId()) ;
    }


    public void updateBatch(List<Icalendar_event_type> calendar_event_types){
        if(calendar_event_types!=null){
            List<calendar_event_typeImpl> list = new ArrayList<calendar_event_typeImpl>();
            for(Icalendar_event_type icalendar_event_type :calendar_event_types){
                list.add((calendar_event_typeImpl)icalendar_event_type) ;
            }
            calendar_event_typeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Icalendar_event_type> calendar_event_types){
        if(calendar_event_types!=null){
            List<calendar_event_typeImpl> list = new ArrayList<calendar_event_typeImpl>();
            for(Icalendar_event_type icalendar_event_type :calendar_event_types){
                list.add((calendar_event_typeImpl)icalendar_event_type) ;
            }
            calendar_event_typeFeignClient.removeBatch(list) ;
        }
    }


    public Page<Icalendar_event_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_event_type calendar_event_type){
        Icalendar_event_type clientModel = calendar_event_typeFeignClient.getDraft(calendar_event_type.getId(),(calendar_event_typeImpl)calendar_event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_event_type.getClass(), false);
        copier.copy(clientModel, calendar_event_type, null);
    }



}


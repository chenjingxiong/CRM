package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarmImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_alarm] 服务对象接口
 */
public interface calendar_alarmFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarms/updatebatch")
    public calendar_alarmImpl updateBatch(@RequestBody List<calendar_alarmImpl> calendar_alarms);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarms")
    public calendar_alarmImpl create(@RequestBody calendar_alarmImpl calendar_alarm);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarms/{id}")
    public calendar_alarmImpl update(@PathVariable("id") Integer id,@RequestBody calendar_alarmImpl calendar_alarm);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarms/createbatch")
    public calendar_alarmImpl createBatch(@RequestBody List<calendar_alarmImpl> calendar_alarms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarms/fetchdefault")
    public Page<calendar_alarmImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarms/removebatch")
    public calendar_alarmImpl removeBatch(@RequestBody List<calendar_alarmImpl> calendar_alarms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarms/{id}")
    public calendar_alarmImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarms/select")
    public Page<calendar_alarmImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarms/{id}/getdraft")
    public calendar_alarmImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_alarmImpl calendar_alarm);



}

package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_attendee;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_attendeeClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_attendeeImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_attendeeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_attendee] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_attendeeClientServiceImpl implements Icalendar_attendeeClientService {

    calendar_attendeeFeignClient calendar_attendeeFeignClient;

    @Autowired
    public calendar_attendeeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_attendeeFeignClient = nameBuilder.target(calendar_attendeeFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_attendeeFeignClient = nameBuilder.target(calendar_attendeeFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_attendee createModel() {
		return new calendar_attendeeImpl();
	}


    public void update(Icalendar_attendee calendar_attendee){
        Icalendar_attendee clientModel = calendar_attendeeFeignClient.update(calendar_attendee.getId(),(calendar_attendeeImpl)calendar_attendee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_attendee.getClass(), false);
        copier.copy(clientModel, calendar_attendee, null);
    }


    public void createBatch(List<Icalendar_attendee> calendar_attendees){
        if(calendar_attendees!=null){
            List<calendar_attendeeImpl> list = new ArrayList<calendar_attendeeImpl>();
            for(Icalendar_attendee icalendar_attendee :calendar_attendees){
                list.add((calendar_attendeeImpl)icalendar_attendee) ;
            }
            calendar_attendeeFeignClient.createBatch(list) ;
        }
    }


    public void create(Icalendar_attendee calendar_attendee){
        Icalendar_attendee clientModel = calendar_attendeeFeignClient.create((calendar_attendeeImpl)calendar_attendee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_attendee.getClass(), false);
        copier.copy(clientModel, calendar_attendee, null);
    }


    public void removeBatch(List<Icalendar_attendee> calendar_attendees){
        if(calendar_attendees!=null){
            List<calendar_attendeeImpl> list = new ArrayList<calendar_attendeeImpl>();
            for(Icalendar_attendee icalendar_attendee :calendar_attendees){
                list.add((calendar_attendeeImpl)icalendar_attendee) ;
            }
            calendar_attendeeFeignClient.removeBatch(list) ;
        }
    }


    public void get(Icalendar_attendee calendar_attendee){
        Icalendar_attendee clientModel = calendar_attendeeFeignClient.get(calendar_attendee.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_attendee.getClass(), false);
        copier.copy(clientModel, calendar_attendee, null);
    }


    public Page<Icalendar_attendee> fetchDefault(SearchContext context){
        Page<calendar_attendeeImpl> page = this.calendar_attendeeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Icalendar_attendee> calendar_attendees){
        if(calendar_attendees!=null){
            List<calendar_attendeeImpl> list = new ArrayList<calendar_attendeeImpl>();
            for(Icalendar_attendee icalendar_attendee :calendar_attendees){
                list.add((calendar_attendeeImpl)icalendar_attendee) ;
            }
            calendar_attendeeFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Icalendar_attendee calendar_attendee){
        calendar_attendeeFeignClient.remove(calendar_attendee.getId()) ;
    }


    public Page<Icalendar_attendee> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_attendee calendar_attendee){
        Icalendar_attendee clientModel = calendar_attendeeFeignClient.getDraft(calendar_attendee.getId(),(calendar_attendeeImpl)calendar_attendee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_attendee.getClass(), false);
        copier.copy(clientModel, calendar_attendee, null);
    }



}


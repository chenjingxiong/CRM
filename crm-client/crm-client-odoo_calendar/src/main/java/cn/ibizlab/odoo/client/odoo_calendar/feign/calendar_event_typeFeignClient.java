package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_event_type;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_event_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_event_type] 服务对象接口
 */
public interface calendar_event_typeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_event_types/createbatch")
    public calendar_event_typeImpl createBatch(@RequestBody List<calendar_event_typeImpl> calendar_event_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_event_types/fetchdefault")
    public Page<calendar_event_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_event_types/{id}")
    public calendar_event_typeImpl update(@PathVariable("id") Integer id,@RequestBody calendar_event_typeImpl calendar_event_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_event_types/{id}")
    public calendar_event_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_event_types")
    public calendar_event_typeImpl create(@RequestBody calendar_event_typeImpl calendar_event_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_event_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_event_types/updatebatch")
    public calendar_event_typeImpl updateBatch(@RequestBody List<calendar_event_typeImpl> calendar_event_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_event_types/removebatch")
    public calendar_event_typeImpl removeBatch(@RequestBody List<calendar_event_typeImpl> calendar_event_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_event_types/select")
    public Page<calendar_event_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_event_types/{id}/getdraft")
    public calendar_event_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_event_typeImpl calendar_event_type);



}

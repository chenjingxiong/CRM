package cn.ibizlab.odoo.client.odoo_calendar.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(odoo_calendarClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(odoo_calendarClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class odoo_calendarAutoConfiguration {

}

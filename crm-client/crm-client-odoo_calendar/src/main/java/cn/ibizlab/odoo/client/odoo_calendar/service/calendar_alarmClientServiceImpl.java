package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_alarmClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarmImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_alarmFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_alarm] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_alarmClientServiceImpl implements Icalendar_alarmClientService {

    calendar_alarmFeignClient calendar_alarmFeignClient;

    @Autowired
    public calendar_alarmClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_alarmFeignClient = nameBuilder.target(calendar_alarmFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_alarmFeignClient = nameBuilder.target(calendar_alarmFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_alarm createModel() {
		return new calendar_alarmImpl();
	}


    public void updateBatch(List<Icalendar_alarm> calendar_alarms){
        if(calendar_alarms!=null){
            List<calendar_alarmImpl> list = new ArrayList<calendar_alarmImpl>();
            for(Icalendar_alarm icalendar_alarm :calendar_alarms){
                list.add((calendar_alarmImpl)icalendar_alarm) ;
            }
            calendar_alarmFeignClient.updateBatch(list) ;
        }
    }


    public void create(Icalendar_alarm calendar_alarm){
        Icalendar_alarm clientModel = calendar_alarmFeignClient.create((calendar_alarmImpl)calendar_alarm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm.getClass(), false);
        copier.copy(clientModel, calendar_alarm, null);
    }


    public void update(Icalendar_alarm calendar_alarm){
        Icalendar_alarm clientModel = calendar_alarmFeignClient.update(calendar_alarm.getId(),(calendar_alarmImpl)calendar_alarm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm.getClass(), false);
        copier.copy(clientModel, calendar_alarm, null);
    }


    public void remove(Icalendar_alarm calendar_alarm){
        calendar_alarmFeignClient.remove(calendar_alarm.getId()) ;
    }


    public void createBatch(List<Icalendar_alarm> calendar_alarms){
        if(calendar_alarms!=null){
            List<calendar_alarmImpl> list = new ArrayList<calendar_alarmImpl>();
            for(Icalendar_alarm icalendar_alarm :calendar_alarms){
                list.add((calendar_alarmImpl)icalendar_alarm) ;
            }
            calendar_alarmFeignClient.createBatch(list) ;
        }
    }


    public Page<Icalendar_alarm> fetchDefault(SearchContext context){
        Page<calendar_alarmImpl> page = this.calendar_alarmFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Icalendar_alarm> calendar_alarms){
        if(calendar_alarms!=null){
            List<calendar_alarmImpl> list = new ArrayList<calendar_alarmImpl>();
            for(Icalendar_alarm icalendar_alarm :calendar_alarms){
                list.add((calendar_alarmImpl)icalendar_alarm) ;
            }
            calendar_alarmFeignClient.removeBatch(list) ;
        }
    }


    public void get(Icalendar_alarm calendar_alarm){
        Icalendar_alarm clientModel = calendar_alarmFeignClient.get(calendar_alarm.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm.getClass(), false);
        copier.copy(clientModel, calendar_alarm, null);
    }


    public Page<Icalendar_alarm> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_alarm calendar_alarm){
        Icalendar_alarm clientModel = calendar_alarmFeignClient.getDraft(calendar_alarm.getId(),(calendar_alarmImpl)calendar_alarm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm.getClass(), false);
        copier.copy(clientModel, calendar_alarm, null);
    }



}


package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_contacts;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_contactsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_contacts] 服务对象接口
 */
public interface calendar_contactsFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_contacts/{id}")
    public calendar_contactsImpl update(@PathVariable("id") Integer id,@RequestBody calendar_contactsImpl calendar_contacts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_contacts")
    public calendar_contactsImpl create(@RequestBody calendar_contactsImpl calendar_contacts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_contacts/fetchdefault")
    public Page<calendar_contactsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_contacts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_contacts/{id}")
    public calendar_contactsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_contacts/createbatch")
    public calendar_contactsImpl createBatch(@RequestBody List<calendar_contactsImpl> calendar_contacts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_contacts/updatebatch")
    public calendar_contactsImpl updateBatch(@RequestBody List<calendar_contactsImpl> calendar_contacts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_contacts/removebatch")
    public calendar_contactsImpl removeBatch(@RequestBody List<calendar_contactsImpl> calendar_contacts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_contacts/select")
    public Page<calendar_contactsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_contacts/{id}/getdraft")
    public calendar_contactsImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_contactsImpl calendar_contacts);



}

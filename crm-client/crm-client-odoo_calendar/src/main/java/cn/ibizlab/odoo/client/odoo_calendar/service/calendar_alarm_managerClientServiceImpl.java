package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm_manager;
import cn.ibizlab.odoo.client.odoo_calendar.config.odoo_calendarClientProperties;
import cn.ibizlab.odoo.core.client.service.Icalendar_alarm_managerClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarm_managerImpl;
import cn.ibizlab.odoo.client.odoo_calendar.feign.calendar_alarm_managerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class calendar_alarm_managerClientServiceImpl implements Icalendar_alarm_managerClientService {

    calendar_alarm_managerFeignClient calendar_alarm_managerFeignClient;

    @Autowired
    public calendar_alarm_managerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_calendarClientProperties odoo_calendarClientProperties) {
        if (odoo_calendarClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_alarm_managerFeignClient = nameBuilder.target(calendar_alarm_managerFeignClient.class,"http://"+odoo_calendarClientProperties.getServiceId()+"/") ;
		}else if (odoo_calendarClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.calendar_alarm_managerFeignClient = nameBuilder.target(calendar_alarm_managerFeignClient.class,odoo_calendarClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icalendar_alarm_manager createModel() {
		return new calendar_alarm_managerImpl();
	}


    public void updateBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
        if(calendar_alarm_managers!=null){
            List<calendar_alarm_managerImpl> list = new ArrayList<calendar_alarm_managerImpl>();
            for(Icalendar_alarm_manager icalendar_alarm_manager :calendar_alarm_managers){
                list.add((calendar_alarm_managerImpl)icalendar_alarm_manager) ;
            }
            calendar_alarm_managerFeignClient.updateBatch(list) ;
        }
    }


    public void update(Icalendar_alarm_manager calendar_alarm_manager){
        Icalendar_alarm_manager clientModel = calendar_alarm_managerFeignClient.update(calendar_alarm_manager.getId(),(calendar_alarm_managerImpl)calendar_alarm_manager) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm_manager.getClass(), false);
        copier.copy(clientModel, calendar_alarm_manager, null);
    }


    public void removeBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
        if(calendar_alarm_managers!=null){
            List<calendar_alarm_managerImpl> list = new ArrayList<calendar_alarm_managerImpl>();
            for(Icalendar_alarm_manager icalendar_alarm_manager :calendar_alarm_managers){
                list.add((calendar_alarm_managerImpl)icalendar_alarm_manager) ;
            }
            calendar_alarm_managerFeignClient.removeBatch(list) ;
        }
    }


    public void get(Icalendar_alarm_manager calendar_alarm_manager){
        Icalendar_alarm_manager clientModel = calendar_alarm_managerFeignClient.get(calendar_alarm_manager.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm_manager.getClass(), false);
        copier.copy(clientModel, calendar_alarm_manager, null);
    }


    public void remove(Icalendar_alarm_manager calendar_alarm_manager){
        calendar_alarm_managerFeignClient.remove(calendar_alarm_manager.getId()) ;
    }


    public void create(Icalendar_alarm_manager calendar_alarm_manager){
        Icalendar_alarm_manager clientModel = calendar_alarm_managerFeignClient.create((calendar_alarm_managerImpl)calendar_alarm_manager) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm_manager.getClass(), false);
        copier.copy(clientModel, calendar_alarm_manager, null);
    }


    public Page<Icalendar_alarm_manager> fetchDefault(SearchContext context){
        Page<calendar_alarm_managerImpl> page = this.calendar_alarm_managerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Icalendar_alarm_manager> calendar_alarm_managers){
        if(calendar_alarm_managers!=null){
            List<calendar_alarm_managerImpl> list = new ArrayList<calendar_alarm_managerImpl>();
            for(Icalendar_alarm_manager icalendar_alarm_manager :calendar_alarm_managers){
                list.add((calendar_alarm_managerImpl)icalendar_alarm_manager) ;
            }
            calendar_alarm_managerFeignClient.createBatch(list) ;
        }
    }


    public Page<Icalendar_alarm_manager> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icalendar_alarm_manager calendar_alarm_manager){
        Icalendar_alarm_manager clientModel = calendar_alarm_managerFeignClient.getDraft(calendar_alarm_manager.getId(),(calendar_alarm_managerImpl)calendar_alarm_manager) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), calendar_alarm_manager.getClass(), false);
        copier.copy(clientModel, calendar_alarm_manager, null);
    }



}


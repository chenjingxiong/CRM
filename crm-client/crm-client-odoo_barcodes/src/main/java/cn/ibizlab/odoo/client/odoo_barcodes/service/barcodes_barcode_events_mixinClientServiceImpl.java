package cn.ibizlab.odoo.client.odoo_barcodes.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibarcodes_barcode_events_mixin;
import cn.ibizlab.odoo.client.odoo_barcodes.config.odoo_barcodesClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibarcodes_barcode_events_mixinClientService;
import cn.ibizlab.odoo.client.odoo_barcodes.model.barcodes_barcode_events_mixinImpl;
import cn.ibizlab.odoo.client.odoo_barcodes.feign.barcodes_barcode_events_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class barcodes_barcode_events_mixinClientServiceImpl implements Ibarcodes_barcode_events_mixinClientService {

    barcodes_barcode_events_mixinFeignClient barcodes_barcode_events_mixinFeignClient;

    @Autowired
    public barcodes_barcode_events_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_barcodesClientProperties odoo_barcodesClientProperties) {
        if (odoo_barcodesClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.barcodes_barcode_events_mixinFeignClient = nameBuilder.target(barcodes_barcode_events_mixinFeignClient.class,"http://"+odoo_barcodesClientProperties.getServiceId()+"/") ;
		}else if (odoo_barcodesClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.barcodes_barcode_events_mixinFeignClient = nameBuilder.target(barcodes_barcode_events_mixinFeignClient.class,odoo_barcodesClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibarcodes_barcode_events_mixin createModel() {
		return new barcodes_barcode_events_mixinImpl();
	}


    public void updateBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
        if(barcodes_barcode_events_mixins!=null){
            List<barcodes_barcode_events_mixinImpl> list = new ArrayList<barcodes_barcode_events_mixinImpl>();
            for(Ibarcodes_barcode_events_mixin ibarcodes_barcode_events_mixin :barcodes_barcode_events_mixins){
                list.add((barcodes_barcode_events_mixinImpl)ibarcodes_barcode_events_mixin) ;
            }
            barcodes_barcode_events_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
        Ibarcodes_barcode_events_mixin clientModel = barcodes_barcode_events_mixinFeignClient.create((barcodes_barcode_events_mixinImpl)barcodes_barcode_events_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), barcodes_barcode_events_mixin.getClass(), false);
        copier.copy(clientModel, barcodes_barcode_events_mixin, null);
    }


    public void update(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
        Ibarcodes_barcode_events_mixin clientModel = barcodes_barcode_events_mixinFeignClient.update(barcodes_barcode_events_mixin.getId(),(barcodes_barcode_events_mixinImpl)barcodes_barcode_events_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), barcodes_barcode_events_mixin.getClass(), false);
        copier.copy(clientModel, barcodes_barcode_events_mixin, null);
    }


    public void remove(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
        barcodes_barcode_events_mixinFeignClient.remove(barcodes_barcode_events_mixin.getId()) ;
    }


    public void get(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
        Ibarcodes_barcode_events_mixin clientModel = barcodes_barcode_events_mixinFeignClient.get(barcodes_barcode_events_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), barcodes_barcode_events_mixin.getClass(), false);
        copier.copy(clientModel, barcodes_barcode_events_mixin, null);
    }


    public void removeBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
        if(barcodes_barcode_events_mixins!=null){
            List<barcodes_barcode_events_mixinImpl> list = new ArrayList<barcodes_barcode_events_mixinImpl>();
            for(Ibarcodes_barcode_events_mixin ibarcodes_barcode_events_mixin :barcodes_barcode_events_mixins){
                list.add((barcodes_barcode_events_mixinImpl)ibarcodes_barcode_events_mixin) ;
            }
            barcodes_barcode_events_mixinFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibarcodes_barcode_events_mixin> fetchDefault(SearchContext context){
        Page<barcodes_barcode_events_mixinImpl> page = this.barcodes_barcode_events_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
        if(barcodes_barcode_events_mixins!=null){
            List<barcodes_barcode_events_mixinImpl> list = new ArrayList<barcodes_barcode_events_mixinImpl>();
            for(Ibarcodes_barcode_events_mixin ibarcodes_barcode_events_mixin :barcodes_barcode_events_mixins){
                list.add((barcodes_barcode_events_mixinImpl)ibarcodes_barcode_events_mixin) ;
            }
            barcodes_barcode_events_mixinFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibarcodes_barcode_events_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
        Ibarcodes_barcode_events_mixin clientModel = barcodes_barcode_events_mixinFeignClient.getDraft(barcodes_barcode_events_mixin.getId(),(barcodes_barcode_events_mixinImpl)barcodes_barcode_events_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), barcodes_barcode_events_mixin.getClass(), false);
        copier.copy(clientModel, barcodes_barcode_events_mixin, null);
    }



}


package cn.ibizlab.odoo.client.odoo_barcodes.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibarcodes_barcode_events_mixin;
import cn.ibizlab.odoo.client.odoo_barcodes.model.barcodes_barcode_events_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
public interface barcodes_barcode_events_mixinFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_barcodes/barcodes_barcode_events_mixins/updatebatch")
    public barcodes_barcode_events_mixinImpl updateBatch(@RequestBody List<barcodes_barcode_events_mixinImpl> barcodes_barcode_events_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_barcodes/barcodes_barcode_events_mixins")
    public barcodes_barcode_events_mixinImpl create(@RequestBody barcodes_barcode_events_mixinImpl barcodes_barcode_events_mixin);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{id}")
    public barcodes_barcode_events_mixinImpl update(@PathVariable("id") Integer id,@RequestBody barcodes_barcode_events_mixinImpl barcodes_barcode_events_mixin);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{id}")
    public barcodes_barcode_events_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_barcodes/barcodes_barcode_events_mixins/removebatch")
    public barcodes_barcode_events_mixinImpl removeBatch(@RequestBody List<barcodes_barcode_events_mixinImpl> barcodes_barcode_events_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_barcodes/barcodes_barcode_events_mixins/fetchdefault")
    public Page<barcodes_barcode_events_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_barcodes/barcodes_barcode_events_mixins/createbatch")
    public barcodes_barcode_events_mixinImpl createBatch(@RequestBody List<barcodes_barcode_events_mixinImpl> barcodes_barcode_events_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_barcodes/barcodes_barcode_events_mixins/select")
    public Page<barcodes_barcode_events_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{id}/getdraft")
    public barcodes_barcode_events_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody barcodes_barcode_events_mixinImpl barcodes_barcode_events_mixin);



}

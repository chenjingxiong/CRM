package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_parameter;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_parameterImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
public interface mro_pm_parameterFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_parameters/fetchdefault")
    public Page<mro_pm_parameterImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_parameters/removebatch")
    public mro_pm_parameterImpl removeBatch(@RequestBody List<mro_pm_parameterImpl> mro_pm_parameters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_parameters/{id}")
    public mro_pm_parameterImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_parameters")
    public mro_pm_parameterImpl create(@RequestBody mro_pm_parameterImpl mro_pm_parameter);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_parameters/updatebatch")
    public mro_pm_parameterImpl updateBatch(@RequestBody List<mro_pm_parameterImpl> mro_pm_parameters);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_parameters/createbatch")
    public mro_pm_parameterImpl createBatch(@RequestBody List<mro_pm_parameterImpl> mro_pm_parameters);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_parameters/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_parameters/{id}")
    public mro_pm_parameterImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_parameterImpl mro_pm_parameter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_parameters/select")
    public Page<mro_pm_parameterImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_parameters/{id}/getdraft")
    public mro_pm_parameterImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_parameterImpl mro_pm_parameter);



}

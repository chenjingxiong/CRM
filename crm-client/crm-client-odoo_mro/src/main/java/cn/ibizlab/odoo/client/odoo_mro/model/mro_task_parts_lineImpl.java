package cn.ibizlab.odoo.client.odoo_mro.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imro_task_parts_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mro_task_parts_line] 对象
 */
public class mro_task_parts_lineImpl implements Imro_task_parts_line,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 零件
     */
    public Integer parts_id;

    @JsonIgnore
    public boolean parts_idDirtyFlag;
    
    /**
     * 零件
     */
    public String parts_id_text;

    @JsonIgnore
    public boolean parts_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double parts_qty;

    @JsonIgnore
    public boolean parts_qtyDirtyFlag;
    
    /**
     * 单位
     */
    public Integer parts_uom;

    @JsonIgnore
    public boolean parts_uomDirtyFlag;
    
    /**
     * 单位
     */
    public String parts_uom_text;

    @JsonIgnore
    public boolean parts_uom_textDirtyFlag;
    
    /**
     * Maintenance Task
     */
    public Integer task_id;

    @JsonIgnore
    public boolean task_idDirtyFlag;
    
    /**
     * Maintenance Task
     */
    public String task_id_text;

    @JsonIgnore
    public boolean task_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [零件]
     */
    @JsonProperty("parts_id")
    public Integer getParts_id(){
        return this.parts_id ;
    }

    /**
     * 设置 [零件]
     */
    @JsonProperty("parts_id")
    public void setParts_id(Integer  parts_id){
        this.parts_id = parts_id ;
        this.parts_idDirtyFlag = true ;
    }

     /**
     * 获取 [零件]脏标记
     */
    @JsonIgnore
    public boolean getParts_idDirtyFlag(){
        return this.parts_idDirtyFlag ;
    }   

    /**
     * 获取 [零件]
     */
    @JsonProperty("parts_id_text")
    public String getParts_id_text(){
        return this.parts_id_text ;
    }

    /**
     * 设置 [零件]
     */
    @JsonProperty("parts_id_text")
    public void setParts_id_text(String  parts_id_text){
        this.parts_id_text = parts_id_text ;
        this.parts_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [零件]脏标记
     */
    @JsonIgnore
    public boolean getParts_id_textDirtyFlag(){
        return this.parts_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("parts_qty")
    public Double getParts_qty(){
        return this.parts_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("parts_qty")
    public void setParts_qty(Double  parts_qty){
        this.parts_qty = parts_qty ;
        this.parts_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getParts_qtyDirtyFlag(){
        return this.parts_qtyDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("parts_uom")
    public Integer getParts_uom(){
        return this.parts_uom ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("parts_uom")
    public void setParts_uom(Integer  parts_uom){
        this.parts_uom = parts_uom ;
        this.parts_uomDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getParts_uomDirtyFlag(){
        return this.parts_uomDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("parts_uom_text")
    public String getParts_uom_text(){
        return this.parts_uom_text ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("parts_uom_text")
    public void setParts_uom_text(String  parts_uom_text){
        this.parts_uom_text = parts_uom_text ;
        this.parts_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getParts_uom_textDirtyFlag(){
        return this.parts_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [Maintenance Task]
     */
    @JsonProperty("task_id")
    public Integer getTask_id(){
        return this.task_id ;
    }

    /**
     * 设置 [Maintenance Task]
     */
    @JsonProperty("task_id")
    public void setTask_id(Integer  task_id){
        this.task_id = task_id ;
        this.task_idDirtyFlag = true ;
    }

     /**
     * 获取 [Maintenance Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_idDirtyFlag(){
        return this.task_idDirtyFlag ;
    }   

    /**
     * 获取 [Maintenance Task]
     */
    @JsonProperty("task_id_text")
    public String getTask_id_text(){
        return this.task_id_text ;
    }

    /**
     * 设置 [Maintenance Task]
     */
    @JsonProperty("task_id_text")
    public void setTask_id_text(String  task_id_text){
        this.task_id_text = task_id_text ;
        this.task_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Maintenance Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_id_textDirtyFlag(){
        return this.task_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

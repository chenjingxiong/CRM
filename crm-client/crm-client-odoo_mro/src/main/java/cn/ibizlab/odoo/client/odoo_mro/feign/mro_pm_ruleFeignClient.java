package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_ruleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_rule] 服务对象接口
 */
public interface mro_pm_ruleFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rules")
    public mro_pm_ruleImpl create(@RequestBody mro_pm_ruleImpl mro_pm_rule);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rules/{id}")
    public mro_pm_ruleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rules/removebatch")
    public mro_pm_ruleImpl removeBatch(@RequestBody List<mro_pm_ruleImpl> mro_pm_rules);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rules/createbatch")
    public mro_pm_ruleImpl createBatch(@RequestBody List<mro_pm_ruleImpl> mro_pm_rules);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rules/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rules/{id}")
    public mro_pm_ruleImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_ruleImpl mro_pm_rule);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rules/fetchdefault")
    public Page<mro_pm_ruleImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rules/updatebatch")
    public mro_pm_ruleImpl updateBatch(@RequestBody List<mro_pm_ruleImpl> mro_pm_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rules/select")
    public Page<mro_pm_ruleImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rules/{id}/getdraft")
    public mro_pm_ruleImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_ruleImpl mro_pm_rule);



}

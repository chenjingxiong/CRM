package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_task;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_taskClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_taskImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_taskFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_task] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_taskClientServiceImpl implements Imro_taskClientService {

    mro_taskFeignClient mro_taskFeignClient;

    @Autowired
    public mro_taskClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_taskFeignClient = nameBuilder.target(mro_taskFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_taskFeignClient = nameBuilder.target(mro_taskFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_task createModel() {
		return new mro_taskImpl();
	}


    public void updateBatch(List<Imro_task> mro_tasks){
        if(mro_tasks!=null){
            List<mro_taskImpl> list = new ArrayList<mro_taskImpl>();
            for(Imro_task imro_task :mro_tasks){
                list.add((mro_taskImpl)imro_task) ;
            }
            mro_taskFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imro_task> mro_tasks){
        if(mro_tasks!=null){
            List<mro_taskImpl> list = new ArrayList<mro_taskImpl>();
            for(Imro_task imro_task :mro_tasks){
                list.add((mro_taskImpl)imro_task) ;
            }
            mro_taskFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_task> fetchDefault(SearchContext context){
        Page<mro_taskImpl> page = this.mro_taskFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imro_task mro_task){
        Imro_task clientModel = mro_taskFeignClient.get(mro_task.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task.getClass(), false);
        copier.copy(clientModel, mro_task, null);
    }


    public void create(Imro_task mro_task){
        Imro_task clientModel = mro_taskFeignClient.create((mro_taskImpl)mro_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task.getClass(), false);
        copier.copy(clientModel, mro_task, null);
    }


    public void update(Imro_task mro_task){
        Imro_task clientModel = mro_taskFeignClient.update(mro_task.getId(),(mro_taskImpl)mro_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task.getClass(), false);
        copier.copy(clientModel, mro_task, null);
    }


    public void remove(Imro_task mro_task){
        mro_taskFeignClient.remove(mro_task.getId()) ;
    }


    public void removeBatch(List<Imro_task> mro_tasks){
        if(mro_tasks!=null){
            List<mro_taskImpl> list = new ArrayList<mro_taskImpl>();
            for(Imro_task imro_task :mro_tasks){
                list.add((mro_taskImpl)imro_task) ;
            }
            mro_taskFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imro_task> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_task mro_task){
        Imro_task clientModel = mro_taskFeignClient.getDraft(mro_task.getId(),(mro_taskImpl)mro_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task.getClass(), false);
        copier.copy(clientModel, mro_task, null);
    }



}


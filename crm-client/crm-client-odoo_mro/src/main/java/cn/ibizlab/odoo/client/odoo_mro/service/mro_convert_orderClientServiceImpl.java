package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_convert_order;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_convert_orderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_convert_orderImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_convert_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_convert_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_convert_orderClientServiceImpl implements Imro_convert_orderClientService {

    mro_convert_orderFeignClient mro_convert_orderFeignClient;

    @Autowired
    public mro_convert_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_convert_orderFeignClient = nameBuilder.target(mro_convert_orderFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_convert_orderFeignClient = nameBuilder.target(mro_convert_orderFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_convert_order createModel() {
		return new mro_convert_orderImpl();
	}


    public void create(Imro_convert_order mro_convert_order){
        Imro_convert_order clientModel = mro_convert_orderFeignClient.create((mro_convert_orderImpl)mro_convert_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_convert_order.getClass(), false);
        copier.copy(clientModel, mro_convert_order, null);
    }


    public void updateBatch(List<Imro_convert_order> mro_convert_orders){
        if(mro_convert_orders!=null){
            List<mro_convert_orderImpl> list = new ArrayList<mro_convert_orderImpl>();
            for(Imro_convert_order imro_convert_order :mro_convert_orders){
                list.add((mro_convert_orderImpl)imro_convert_order) ;
            }
            mro_convert_orderFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imro_convert_order mro_convert_order){
        Imro_convert_order clientModel = mro_convert_orderFeignClient.get(mro_convert_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_convert_order.getClass(), false);
        copier.copy(clientModel, mro_convert_order, null);
    }


    public void remove(Imro_convert_order mro_convert_order){
        mro_convert_orderFeignClient.remove(mro_convert_order.getId()) ;
    }


    public Page<Imro_convert_order> fetchDefault(SearchContext context){
        Page<mro_convert_orderImpl> page = this.mro_convert_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imro_convert_order mro_convert_order){
        Imro_convert_order clientModel = mro_convert_orderFeignClient.update(mro_convert_order.getId(),(mro_convert_orderImpl)mro_convert_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_convert_order.getClass(), false);
        copier.copy(clientModel, mro_convert_order, null);
    }


    public void removeBatch(List<Imro_convert_order> mro_convert_orders){
        if(mro_convert_orders!=null){
            List<mro_convert_orderImpl> list = new ArrayList<mro_convert_orderImpl>();
            for(Imro_convert_order imro_convert_order :mro_convert_orders){
                list.add((mro_convert_orderImpl)imro_convert_order) ;
            }
            mro_convert_orderFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imro_convert_order> mro_convert_orders){
        if(mro_convert_orders!=null){
            List<mro_convert_orderImpl> list = new ArrayList<mro_convert_orderImpl>();
            for(Imro_convert_order imro_convert_order :mro_convert_orders){
                list.add((mro_convert_orderImpl)imro_convert_order) ;
            }
            mro_convert_orderFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_convert_order> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_convert_order mro_convert_order){
        Imro_convert_order clientModel = mro_convert_orderFeignClient.getDraft(mro_convert_order.getId(),(mro_convert_orderImpl)mro_convert_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_convert_order.getClass(), false);
        copier.copy(clientModel, mro_convert_order, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_order_parts_line;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_order_parts_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_order_parts_line] 服务对象接口
 */
public interface mro_order_parts_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_order_parts_lines/{id}")
    public mro_order_parts_lineImpl update(@PathVariable("id") Integer id,@RequestBody mro_order_parts_lineImpl mro_order_parts_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_order_parts_lines/fetchdefault")
    public Page<mro_order_parts_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_order_parts_lines/createbatch")
    public mro_order_parts_lineImpl createBatch(@RequestBody List<mro_order_parts_lineImpl> mro_order_parts_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_order_parts_lines/{id}")
    public mro_order_parts_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_order_parts_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_order_parts_lines")
    public mro_order_parts_lineImpl create(@RequestBody mro_order_parts_lineImpl mro_order_parts_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_order_parts_lines/updatebatch")
    public mro_order_parts_lineImpl updateBatch(@RequestBody List<mro_order_parts_lineImpl> mro_order_parts_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_order_parts_lines/removebatch")
    public mro_order_parts_lineImpl removeBatch(@RequestBody List<mro_order_parts_lineImpl> mro_order_parts_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_order_parts_lines/select")
    public Page<mro_order_parts_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_order_parts_lines/{id}/getdraft")
    public mro_order_parts_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_order_parts_lineImpl mro_order_parts_line);



}

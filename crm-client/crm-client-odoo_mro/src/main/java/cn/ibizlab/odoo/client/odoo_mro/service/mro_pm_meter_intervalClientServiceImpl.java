package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_interval;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meter_intervalClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_intervalImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_meter_intervalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_meter_intervalClientServiceImpl implements Imro_pm_meter_intervalClientService {

    mro_pm_meter_intervalFeignClient mro_pm_meter_intervalFeignClient;

    @Autowired
    public mro_pm_meter_intervalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meter_intervalFeignClient = nameBuilder.target(mro_pm_meter_intervalFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meter_intervalFeignClient = nameBuilder.target(mro_pm_meter_intervalFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_meter_interval createModel() {
		return new mro_pm_meter_intervalImpl();
	}


    public void update(Imro_pm_meter_interval mro_pm_meter_interval){
        Imro_pm_meter_interval clientModel = mro_pm_meter_intervalFeignClient.update(mro_pm_meter_interval.getId(),(mro_pm_meter_intervalImpl)mro_pm_meter_interval) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_interval.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_interval, null);
    }


    public Page<Imro_pm_meter_interval> fetchDefault(SearchContext context){
        Page<mro_pm_meter_intervalImpl> page = this.mro_pm_meter_intervalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
        if(mro_pm_meter_intervals!=null){
            List<mro_pm_meter_intervalImpl> list = new ArrayList<mro_pm_meter_intervalImpl>();
            for(Imro_pm_meter_interval imro_pm_meter_interval :mro_pm_meter_intervals){
                list.add((mro_pm_meter_intervalImpl)imro_pm_meter_interval) ;
            }
            mro_pm_meter_intervalFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imro_pm_meter_interval mro_pm_meter_interval){
        mro_pm_meter_intervalFeignClient.remove(mro_pm_meter_interval.getId()) ;
    }


    public void createBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
        if(mro_pm_meter_intervals!=null){
            List<mro_pm_meter_intervalImpl> list = new ArrayList<mro_pm_meter_intervalImpl>();
            for(Imro_pm_meter_interval imro_pm_meter_interval :mro_pm_meter_intervals){
                list.add((mro_pm_meter_intervalImpl)imro_pm_meter_interval) ;
            }
            mro_pm_meter_intervalFeignClient.createBatch(list) ;
        }
    }


    public void create(Imro_pm_meter_interval mro_pm_meter_interval){
        Imro_pm_meter_interval clientModel = mro_pm_meter_intervalFeignClient.create((mro_pm_meter_intervalImpl)mro_pm_meter_interval) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_interval.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_interval, null);
    }


    public void get(Imro_pm_meter_interval mro_pm_meter_interval){
        Imro_pm_meter_interval clientModel = mro_pm_meter_intervalFeignClient.get(mro_pm_meter_interval.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_interval.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_interval, null);
    }


    public void updateBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
        if(mro_pm_meter_intervals!=null){
            List<mro_pm_meter_intervalImpl> list = new ArrayList<mro_pm_meter_intervalImpl>();
            for(Imro_pm_meter_interval imro_pm_meter_interval :mro_pm_meter_intervals){
                list.add((mro_pm_meter_intervalImpl)imro_pm_meter_interval) ;
            }
            mro_pm_meter_intervalFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imro_pm_meter_interval> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_meter_interval mro_pm_meter_interval){
        Imro_pm_meter_interval clientModel = mro_pm_meter_intervalFeignClient.getDraft(mro_pm_meter_interval.getId(),(mro_pm_meter_intervalImpl)mro_pm_meter_interval) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter_interval.getClass(), false);
        copier.copy(clientModel, mro_pm_meter_interval, null);
    }



}


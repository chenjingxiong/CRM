package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meterClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meterImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_meterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_meter] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_meterClientServiceImpl implements Imro_pm_meterClientService {

    mro_pm_meterFeignClient mro_pm_meterFeignClient;

    @Autowired
    public mro_pm_meterClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meterFeignClient = nameBuilder.target(mro_pm_meterFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_meterFeignClient = nameBuilder.target(mro_pm_meterFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_meter createModel() {
		return new mro_pm_meterImpl();
	}


    public void update(Imro_pm_meter mro_pm_meter){
        Imro_pm_meter clientModel = mro_pm_meterFeignClient.update(mro_pm_meter.getId(),(mro_pm_meterImpl)mro_pm_meter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter.getClass(), false);
        copier.copy(clientModel, mro_pm_meter, null);
    }


    public void removeBatch(List<Imro_pm_meter> mro_pm_meters){
        if(mro_pm_meters!=null){
            List<mro_pm_meterImpl> list = new ArrayList<mro_pm_meterImpl>();
            for(Imro_pm_meter imro_pm_meter :mro_pm_meters){
                list.add((mro_pm_meterImpl)imro_pm_meter) ;
            }
            mro_pm_meterFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imro_pm_meter mro_pm_meter){
        Imro_pm_meter clientModel = mro_pm_meterFeignClient.create((mro_pm_meterImpl)mro_pm_meter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter.getClass(), false);
        copier.copy(clientModel, mro_pm_meter, null);
    }


    public void get(Imro_pm_meter mro_pm_meter){
        Imro_pm_meter clientModel = mro_pm_meterFeignClient.get(mro_pm_meter.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter.getClass(), false);
        copier.copy(clientModel, mro_pm_meter, null);
    }


    public void updateBatch(List<Imro_pm_meter> mro_pm_meters){
        if(mro_pm_meters!=null){
            List<mro_pm_meterImpl> list = new ArrayList<mro_pm_meterImpl>();
            for(Imro_pm_meter imro_pm_meter :mro_pm_meters){
                list.add((mro_pm_meterImpl)imro_pm_meter) ;
            }
            mro_pm_meterFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imro_pm_meter> fetchDefault(SearchContext context){
        Page<mro_pm_meterImpl> page = this.mro_pm_meterFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imro_pm_meter mro_pm_meter){
        mro_pm_meterFeignClient.remove(mro_pm_meter.getId()) ;
    }


    public void createBatch(List<Imro_pm_meter> mro_pm_meters){
        if(mro_pm_meters!=null){
            List<mro_pm_meterImpl> list = new ArrayList<mro_pm_meterImpl>();
            for(Imro_pm_meter imro_pm_meter :mro_pm_meters){
                list.add((mro_pm_meterImpl)imro_pm_meter) ;
            }
            mro_pm_meterFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_pm_meter> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_meter mro_pm_meter){
        Imro_pm_meter clientModel = mro_pm_meterFeignClient.getDraft(mro_pm_meter.getId(),(mro_pm_meterImpl)mro_pm_meter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_meter.getClass(), false);
        copier.copy(clientModel, mro_pm_meter, null);
    }



}


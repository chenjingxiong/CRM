package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_ruleClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_ruleImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_ruleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_rule] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_ruleClientServiceImpl implements Imro_pm_ruleClientService {

    mro_pm_ruleFeignClient mro_pm_ruleFeignClient;

    @Autowired
    public mro_pm_ruleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_ruleFeignClient = nameBuilder.target(mro_pm_ruleFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_ruleFeignClient = nameBuilder.target(mro_pm_ruleFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_rule createModel() {
		return new mro_pm_ruleImpl();
	}


    public void create(Imro_pm_rule mro_pm_rule){
        Imro_pm_rule clientModel = mro_pm_ruleFeignClient.create((mro_pm_ruleImpl)mro_pm_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule.getClass(), false);
        copier.copy(clientModel, mro_pm_rule, null);
    }


    public void get(Imro_pm_rule mro_pm_rule){
        Imro_pm_rule clientModel = mro_pm_ruleFeignClient.get(mro_pm_rule.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule.getClass(), false);
        copier.copy(clientModel, mro_pm_rule, null);
    }


    public void removeBatch(List<Imro_pm_rule> mro_pm_rules){
        if(mro_pm_rules!=null){
            List<mro_pm_ruleImpl> list = new ArrayList<mro_pm_ruleImpl>();
            for(Imro_pm_rule imro_pm_rule :mro_pm_rules){
                list.add((mro_pm_ruleImpl)imro_pm_rule) ;
            }
            mro_pm_ruleFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imro_pm_rule> mro_pm_rules){
        if(mro_pm_rules!=null){
            List<mro_pm_ruleImpl> list = new ArrayList<mro_pm_ruleImpl>();
            for(Imro_pm_rule imro_pm_rule :mro_pm_rules){
                list.add((mro_pm_ruleImpl)imro_pm_rule) ;
            }
            mro_pm_ruleFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imro_pm_rule mro_pm_rule){
        mro_pm_ruleFeignClient.remove(mro_pm_rule.getId()) ;
    }


    public void update(Imro_pm_rule mro_pm_rule){
        Imro_pm_rule clientModel = mro_pm_ruleFeignClient.update(mro_pm_rule.getId(),(mro_pm_ruleImpl)mro_pm_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule.getClass(), false);
        copier.copy(clientModel, mro_pm_rule, null);
    }


    public Page<Imro_pm_rule> fetchDefault(SearchContext context){
        Page<mro_pm_ruleImpl> page = this.mro_pm_ruleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imro_pm_rule> mro_pm_rules){
        if(mro_pm_rules!=null){
            List<mro_pm_ruleImpl> list = new ArrayList<mro_pm_ruleImpl>();
            for(Imro_pm_rule imro_pm_rule :mro_pm_rules){
                list.add((mro_pm_ruleImpl)imro_pm_rule) ;
            }
            mro_pm_ruleFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imro_pm_rule> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_rule mro_pm_rule){
        Imro_pm_rule clientModel = mro_pm_ruleFeignClient.getDraft(mro_pm_rule.getId(),(mro_pm_ruleImpl)mro_pm_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule.getClass(), false);
        copier.copy(clientModel, mro_pm_rule, null);
    }



}


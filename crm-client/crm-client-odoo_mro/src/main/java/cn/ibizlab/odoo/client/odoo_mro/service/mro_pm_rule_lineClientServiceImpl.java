package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_rule_line;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_rule_lineClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_rule_lineImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_rule_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_rule_lineClientServiceImpl implements Imro_pm_rule_lineClientService {

    mro_pm_rule_lineFeignClient mro_pm_rule_lineFeignClient;

    @Autowired
    public mro_pm_rule_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_rule_lineFeignClient = nameBuilder.target(mro_pm_rule_lineFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_rule_lineFeignClient = nameBuilder.target(mro_pm_rule_lineFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_rule_line createModel() {
		return new mro_pm_rule_lineImpl();
	}


    public void remove(Imro_pm_rule_line mro_pm_rule_line){
        mro_pm_rule_lineFeignClient.remove(mro_pm_rule_line.getId()) ;
    }


    public void updateBatch(List<Imro_pm_rule_line> mro_pm_rule_lines){
        if(mro_pm_rule_lines!=null){
            List<mro_pm_rule_lineImpl> list = new ArrayList<mro_pm_rule_lineImpl>();
            for(Imro_pm_rule_line imro_pm_rule_line :mro_pm_rule_lines){
                list.add((mro_pm_rule_lineImpl)imro_pm_rule_line) ;
            }
            mro_pm_rule_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imro_pm_rule_line> mro_pm_rule_lines){
        if(mro_pm_rule_lines!=null){
            List<mro_pm_rule_lineImpl> list = new ArrayList<mro_pm_rule_lineImpl>();
            for(Imro_pm_rule_line imro_pm_rule_line :mro_pm_rule_lines){
                list.add((mro_pm_rule_lineImpl)imro_pm_rule_line) ;
            }
            mro_pm_rule_lineFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imro_pm_rule_line mro_pm_rule_line){
        Imro_pm_rule_line clientModel = mro_pm_rule_lineFeignClient.create((mro_pm_rule_lineImpl)mro_pm_rule_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule_line.getClass(), false);
        copier.copy(clientModel, mro_pm_rule_line, null);
    }


    public void createBatch(List<Imro_pm_rule_line> mro_pm_rule_lines){
        if(mro_pm_rule_lines!=null){
            List<mro_pm_rule_lineImpl> list = new ArrayList<mro_pm_rule_lineImpl>();
            for(Imro_pm_rule_line imro_pm_rule_line :mro_pm_rule_lines){
                list.add((mro_pm_rule_lineImpl)imro_pm_rule_line) ;
            }
            mro_pm_rule_lineFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_pm_rule_line> fetchDefault(SearchContext context){
        Page<mro_pm_rule_lineImpl> page = this.mro_pm_rule_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imro_pm_rule_line mro_pm_rule_line){
        Imro_pm_rule_line clientModel = mro_pm_rule_lineFeignClient.update(mro_pm_rule_line.getId(),(mro_pm_rule_lineImpl)mro_pm_rule_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule_line.getClass(), false);
        copier.copy(clientModel, mro_pm_rule_line, null);
    }


    public void get(Imro_pm_rule_line mro_pm_rule_line){
        Imro_pm_rule_line clientModel = mro_pm_rule_lineFeignClient.get(mro_pm_rule_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule_line.getClass(), false);
        copier.copy(clientModel, mro_pm_rule_line, null);
    }


    public Page<Imro_pm_rule_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_rule_line mro_pm_rule_line){
        Imro_pm_rule_line clientModel = mro_pm_rule_lineFeignClient.getDraft(mro_pm_rule_line.getId(),(mro_pm_rule_lineImpl)mro_pm_rule_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_rule_line.getClass(), false);
        copier.copy(clientModel, mro_pm_rule_line, null);
    }



}


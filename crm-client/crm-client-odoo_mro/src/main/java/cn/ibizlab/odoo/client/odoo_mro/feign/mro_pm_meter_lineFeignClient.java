package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_line;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
public interface mro_pm_meter_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_lines")
    public mro_pm_meter_lineImpl create(@RequestBody mro_pm_meter_lineImpl mro_pm_meter_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_lines/{id}")
    public mro_pm_meter_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_lines/removebatch")
    public mro_pm_meter_lineImpl removeBatch(@RequestBody List<mro_pm_meter_lineImpl> mro_pm_meter_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_lines/{id}")
    public mro_pm_meter_lineImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_lineImpl mro_pm_meter_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_lines/fetchdefault")
    public Page<mro_pm_meter_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_lines/updatebatch")
    public mro_pm_meter_lineImpl updateBatch(@RequestBody List<mro_pm_meter_lineImpl> mro_pm_meter_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_lines/createbatch")
    public mro_pm_meter_lineImpl createBatch(@RequestBody List<mro_pm_meter_lineImpl> mro_pm_meter_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_lines/select")
    public Page<mro_pm_meter_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_lines/{id}/getdraft")
    public mro_pm_meter_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_lineImpl mro_pm_meter_line);



}

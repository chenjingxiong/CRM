package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_task_parts_line;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_task_parts_lineClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_task_parts_lineImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_task_parts_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_task_parts_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_task_parts_lineClientServiceImpl implements Imro_task_parts_lineClientService {

    mro_task_parts_lineFeignClient mro_task_parts_lineFeignClient;

    @Autowired
    public mro_task_parts_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_task_parts_lineFeignClient = nameBuilder.target(mro_task_parts_lineFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_task_parts_lineFeignClient = nameBuilder.target(mro_task_parts_lineFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_task_parts_line createModel() {
		return new mro_task_parts_lineImpl();
	}


    public void updateBatch(List<Imro_task_parts_line> mro_task_parts_lines){
        if(mro_task_parts_lines!=null){
            List<mro_task_parts_lineImpl> list = new ArrayList<mro_task_parts_lineImpl>();
            for(Imro_task_parts_line imro_task_parts_line :mro_task_parts_lines){
                list.add((mro_task_parts_lineImpl)imro_task_parts_line) ;
            }
            mro_task_parts_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imro_task_parts_line> mro_task_parts_lines){
        if(mro_task_parts_lines!=null){
            List<mro_task_parts_lineImpl> list = new ArrayList<mro_task_parts_lineImpl>();
            for(Imro_task_parts_line imro_task_parts_line :mro_task_parts_lines){
                list.add((mro_task_parts_lineImpl)imro_task_parts_line) ;
            }
            mro_task_parts_lineFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imro_task_parts_line mro_task_parts_line){
        Imro_task_parts_line clientModel = mro_task_parts_lineFeignClient.create((mro_task_parts_lineImpl)mro_task_parts_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task_parts_line.getClass(), false);
        copier.copy(clientModel, mro_task_parts_line, null);
    }


    public Page<Imro_task_parts_line> fetchDefault(SearchContext context){
        Page<mro_task_parts_lineImpl> page = this.mro_task_parts_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imro_task_parts_line mro_task_parts_line){
        Imro_task_parts_line clientModel = mro_task_parts_lineFeignClient.get(mro_task_parts_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task_parts_line.getClass(), false);
        copier.copy(clientModel, mro_task_parts_line, null);
    }


    public void createBatch(List<Imro_task_parts_line> mro_task_parts_lines){
        if(mro_task_parts_lines!=null){
            List<mro_task_parts_lineImpl> list = new ArrayList<mro_task_parts_lineImpl>();
            for(Imro_task_parts_line imro_task_parts_line :mro_task_parts_lines){
                list.add((mro_task_parts_lineImpl)imro_task_parts_line) ;
            }
            mro_task_parts_lineFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imro_task_parts_line mro_task_parts_line){
        mro_task_parts_lineFeignClient.remove(mro_task_parts_line.getId()) ;
    }


    public void update(Imro_task_parts_line mro_task_parts_line){
        Imro_task_parts_line clientModel = mro_task_parts_lineFeignClient.update(mro_task_parts_line.getId(),(mro_task_parts_lineImpl)mro_task_parts_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task_parts_line.getClass(), false);
        copier.copy(clientModel, mro_task_parts_line, null);
    }


    public Page<Imro_task_parts_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_task_parts_line mro_task_parts_line){
        Imro_task_parts_line clientModel = mro_task_parts_lineFeignClient.getDraft(mro_task_parts_line.getId(),(mro_task_parts_lineImpl)mro_task_parts_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_task_parts_line.getClass(), false);
        copier.copy(clientModel, mro_task_parts_line, null);
    }



}


package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_packaging;
import cn.ibizlab.odoo.client.odoo_product.model.product_packagingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_packaging] 服务对象接口
 */
public interface product_packagingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_packagings/updatebatch")
    public product_packagingImpl updateBatch(@RequestBody List<product_packagingImpl> product_packagings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_packagings/createbatch")
    public product_packagingImpl createBatch(@RequestBody List<product_packagingImpl> product_packagings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_packagings/{id}")
    public product_packagingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_packagings/removebatch")
    public product_packagingImpl removeBatch(@RequestBody List<product_packagingImpl> product_packagings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_packagings")
    public product_packagingImpl create(@RequestBody product_packagingImpl product_packaging);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_packagings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_packagings/{id}")
    public product_packagingImpl update(@PathVariable("id") Integer id,@RequestBody product_packagingImpl product_packaging);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_packagings/fetchdefault")
    public Page<product_packagingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_packagings/select")
    public Page<product_packagingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_packagings/{id}/getdraft")
    public product_packagingImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_packagingImpl product_packaging);



}

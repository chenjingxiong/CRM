package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_templateClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_templateImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_templateClientServiceImpl implements Iproduct_templateClientService {

    product_templateFeignClient product_templateFeignClient;

    @Autowired
    public product_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_templateFeignClient = nameBuilder.target(product_templateFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_templateFeignClient = nameBuilder.target(product_templateFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_template createModel() {
		return new product_templateImpl();
	}


    public void remove(Iproduct_template product_template){
        product_templateFeignClient.remove(product_template.getId()) ;
    }


    public Page<Iproduct_template> fetchDefault(SearchContext context){
        Page<product_templateImpl> page = this.product_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iproduct_template product_template){
        Iproduct_template clientModel = product_templateFeignClient.create((product_templateImpl)product_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template.getClass(), false);
        copier.copy(clientModel, product_template, null);
    }


    public void update(Iproduct_template product_template){
        Iproduct_template clientModel = product_templateFeignClient.update(product_template.getId(),(product_templateImpl)product_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template.getClass(), false);
        copier.copy(clientModel, product_template, null);
    }


    public void removeBatch(List<Iproduct_template> product_templates){
        if(product_templates!=null){
            List<product_templateImpl> list = new ArrayList<product_templateImpl>();
            for(Iproduct_template iproduct_template :product_templates){
                list.add((product_templateImpl)iproduct_template) ;
            }
            product_templateFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iproduct_template> product_templates){
        if(product_templates!=null){
            List<product_templateImpl> list = new ArrayList<product_templateImpl>();
            for(Iproduct_template iproduct_template :product_templates){
                list.add((product_templateImpl)iproduct_template) ;
            }
            product_templateFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iproduct_template product_template){
        Iproduct_template clientModel = product_templateFeignClient.get(product_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template.getClass(), false);
        copier.copy(clientModel, product_template, null);
    }


    public void createBatch(List<Iproduct_template> product_templates){
        if(product_templates!=null){
            List<product_templateImpl> list = new ArrayList<product_templateImpl>();
            for(Iproduct_template iproduct_template :product_templates){
                list.add((product_templateImpl)iproduct_template) ;
            }
            product_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Iproduct_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_template product_template){
        Iproduct_template clientModel = product_templateFeignClient.getDraft(product_template.getId(),(product_templateImpl)product_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template.getClass(), false);
        copier.copy(clientModel, product_template, null);
    }



}


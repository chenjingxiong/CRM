package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[product_pricelist_item] 对象
 */
public class product_pricelist_itemImpl implements Iproduct_pricelist_item,Serializable{

    /**
     * 应用于
     */
    public String applied_on;

    @JsonIgnore
    public boolean applied_onDirtyFlag;
    
    /**
     * 基于
     */
    public String base;

    @JsonIgnore
    public boolean baseDirtyFlag;
    
    /**
     * 其他价格表
     */
    public Integer base_pricelist_id;

    @JsonIgnore
    public boolean base_pricelist_idDirtyFlag;
    
    /**
     * 其他价格表
     */
    public String base_pricelist_id_text;

    @JsonIgnore
    public boolean base_pricelist_id_textDirtyFlag;
    
    /**
     * 产品种类
     */
    public Integer categ_id;

    @JsonIgnore
    public boolean categ_idDirtyFlag;
    
    /**
     * 产品种类
     */
    public String categ_id_text;

    @JsonIgnore
    public boolean categ_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 计算价格
     */
    public String compute_price;

    @JsonIgnore
    public boolean compute_priceDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp date_end;

    @JsonIgnore
    public boolean date_endDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 固定价格
     */
    public Double fixed_price;

    @JsonIgnore
    public boolean fixed_priceDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最小数量
     */
    public Integer min_quantity;

    @JsonIgnore
    public boolean min_quantityDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 百分比价格
     */
    public Double percent_price;

    @JsonIgnore
    public boolean percent_priceDirtyFlag;
    
    /**
     * 价格
     */
    public String price;

    @JsonIgnore
    public boolean priceDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer pricelist_id;

    @JsonIgnore
    public boolean pricelist_idDirtyFlag;
    
    /**
     * 价格表
     */
    public String pricelist_id_text;

    @JsonIgnore
    public boolean pricelist_id_textDirtyFlag;
    
    /**
     * 价格折扣
     */
    public Double price_discount;

    @JsonIgnore
    public boolean price_discountDirtyFlag;
    
    /**
     * 最大价格毛利
     */
    public Double price_max_margin;

    @JsonIgnore
    public boolean price_max_marginDirtyFlag;
    
    /**
     * 最小价格毛利
     */
    public Double price_min_margin;

    @JsonIgnore
    public boolean price_min_marginDirtyFlag;
    
    /**
     * 价格舍入
     */
    public Double price_round;

    @JsonIgnore
    public boolean price_roundDirtyFlag;
    
    /**
     * 价格附加费用
     */
    public Double price_surcharge;

    @JsonIgnore
    public boolean price_surchargeDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [应用于]
     */
    @JsonProperty("applied_on")
    public String getApplied_on(){
        return this.applied_on ;
    }

    /**
     * 设置 [应用于]
     */
    @JsonProperty("applied_on")
    public void setApplied_on(String  applied_on){
        this.applied_on = applied_on ;
        this.applied_onDirtyFlag = true ;
    }

     /**
     * 获取 [应用于]脏标记
     */
    @JsonIgnore
    public boolean getApplied_onDirtyFlag(){
        return this.applied_onDirtyFlag ;
    }   

    /**
     * 获取 [基于]
     */
    @JsonProperty("base")
    public String getBase(){
        return this.base ;
    }

    /**
     * 设置 [基于]
     */
    @JsonProperty("base")
    public void setBase(String  base){
        this.base = base ;
        this.baseDirtyFlag = true ;
    }

     /**
     * 获取 [基于]脏标记
     */
    @JsonIgnore
    public boolean getBaseDirtyFlag(){
        return this.baseDirtyFlag ;
    }   

    /**
     * 获取 [其他价格表]
     */
    @JsonProperty("base_pricelist_id")
    public Integer getBase_pricelist_id(){
        return this.base_pricelist_id ;
    }

    /**
     * 设置 [其他价格表]
     */
    @JsonProperty("base_pricelist_id")
    public void setBase_pricelist_id(Integer  base_pricelist_id){
        this.base_pricelist_id = base_pricelist_id ;
        this.base_pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [其他价格表]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_idDirtyFlag(){
        return this.base_pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [其他价格表]
     */
    @JsonProperty("base_pricelist_id_text")
    public String getBase_pricelist_id_text(){
        return this.base_pricelist_id_text ;
    }

    /**
     * 设置 [其他价格表]
     */
    @JsonProperty("base_pricelist_id_text")
    public void setBase_pricelist_id_text(String  base_pricelist_id_text){
        this.base_pricelist_id_text = base_pricelist_id_text ;
        this.base_pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [其他价格表]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_id_textDirtyFlag(){
        return this.base_pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return this.categ_id_text ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return this.categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计算价格]
     */
    @JsonProperty("compute_price")
    public String getCompute_price(){
        return this.compute_price ;
    }

    /**
     * 设置 [计算价格]
     */
    @JsonProperty("compute_price")
    public void setCompute_price(String  compute_price){
        this.compute_price = compute_price ;
        this.compute_priceDirtyFlag = true ;
    }

     /**
     * 获取 [计算价格]脏标记
     */
    @JsonIgnore
    public boolean getCompute_priceDirtyFlag(){
        return this.compute_priceDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return this.date_end ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return this.date_endDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [固定价格]
     */
    @JsonProperty("fixed_price")
    public Double getFixed_price(){
        return this.fixed_price ;
    }

    /**
     * 设置 [固定价格]
     */
    @JsonProperty("fixed_price")
    public void setFixed_price(Double  fixed_price){
        this.fixed_price = fixed_price ;
        this.fixed_priceDirtyFlag = true ;
    }

     /**
     * 获取 [固定价格]脏标记
     */
    @JsonIgnore
    public boolean getFixed_priceDirtyFlag(){
        return this.fixed_priceDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最小数量]
     */
    @JsonProperty("min_quantity")
    public Integer getMin_quantity(){
        return this.min_quantity ;
    }

    /**
     * 设置 [最小数量]
     */
    @JsonProperty("min_quantity")
    public void setMin_quantity(Integer  min_quantity){
        this.min_quantity = min_quantity ;
        this.min_quantityDirtyFlag = true ;
    }

     /**
     * 获取 [最小数量]脏标记
     */
    @JsonIgnore
    public boolean getMin_quantityDirtyFlag(){
        return this.min_quantityDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [百分比价格]
     */
    @JsonProperty("percent_price")
    public Double getPercent_price(){
        return this.percent_price ;
    }

    /**
     * 设置 [百分比价格]
     */
    @JsonProperty("percent_price")
    public void setPercent_price(Double  percent_price){
        this.percent_price = percent_price ;
        this.percent_priceDirtyFlag = true ;
    }

     /**
     * 获取 [百分比价格]脏标记
     */
    @JsonIgnore
    public boolean getPercent_priceDirtyFlag(){
        return this.percent_priceDirtyFlag ;
    }   

    /**
     * 获取 [价格]
     */
    @JsonProperty("price")
    public String getPrice(){
        return this.price ;
    }

    /**
     * 设置 [价格]
     */
    @JsonProperty("price")
    public void setPrice(String  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

     /**
     * 获取 [价格]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return this.priceDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [价格折扣]
     */
    @JsonProperty("price_discount")
    public Double getPrice_discount(){
        return this.price_discount ;
    }

    /**
     * 设置 [价格折扣]
     */
    @JsonProperty("price_discount")
    public void setPrice_discount(Double  price_discount){
        this.price_discount = price_discount ;
        this.price_discountDirtyFlag = true ;
    }

     /**
     * 获取 [价格折扣]脏标记
     */
    @JsonIgnore
    public boolean getPrice_discountDirtyFlag(){
        return this.price_discountDirtyFlag ;
    }   

    /**
     * 获取 [最大价格毛利]
     */
    @JsonProperty("price_max_margin")
    public Double getPrice_max_margin(){
        return this.price_max_margin ;
    }

    /**
     * 设置 [最大价格毛利]
     */
    @JsonProperty("price_max_margin")
    public void setPrice_max_margin(Double  price_max_margin){
        this.price_max_margin = price_max_margin ;
        this.price_max_marginDirtyFlag = true ;
    }

     /**
     * 获取 [最大价格毛利]脏标记
     */
    @JsonIgnore
    public boolean getPrice_max_marginDirtyFlag(){
        return this.price_max_marginDirtyFlag ;
    }   

    /**
     * 获取 [最小价格毛利]
     */
    @JsonProperty("price_min_margin")
    public Double getPrice_min_margin(){
        return this.price_min_margin ;
    }

    /**
     * 设置 [最小价格毛利]
     */
    @JsonProperty("price_min_margin")
    public void setPrice_min_margin(Double  price_min_margin){
        this.price_min_margin = price_min_margin ;
        this.price_min_marginDirtyFlag = true ;
    }

     /**
     * 获取 [最小价格毛利]脏标记
     */
    @JsonIgnore
    public boolean getPrice_min_marginDirtyFlag(){
        return this.price_min_marginDirtyFlag ;
    }   

    /**
     * 获取 [价格舍入]
     */
    @JsonProperty("price_round")
    public Double getPrice_round(){
        return this.price_round ;
    }

    /**
     * 设置 [价格舍入]
     */
    @JsonProperty("price_round")
    public void setPrice_round(Double  price_round){
        this.price_round = price_round ;
        this.price_roundDirtyFlag = true ;
    }

     /**
     * 获取 [价格舍入]脏标记
     */
    @JsonIgnore
    public boolean getPrice_roundDirtyFlag(){
        return this.price_roundDirtyFlag ;
    }   

    /**
     * 获取 [价格附加费用]
     */
    @JsonProperty("price_surcharge")
    public Double getPrice_surcharge(){
        return this.price_surcharge ;
    }

    /**
     * 设置 [价格附加费用]
     */
    @JsonProperty("price_surcharge")
    public void setPrice_surcharge(Double  price_surcharge){
        this.price_surcharge = price_surcharge ;
        this.price_surchargeDirtyFlag = true ;
    }

     /**
     * 获取 [价格附加费用]脏标记
     */
    @JsonIgnore
    public boolean getPrice_surchargeDirtyFlag(){
        return this.price_surchargeDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

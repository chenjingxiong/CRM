package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_value;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_valueImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_template_attribute_value] 服务对象接口
 */
public interface product_template_attribute_valueFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_values/updatebatch")
    public product_template_attribute_valueImpl updateBatch(@RequestBody List<product_template_attribute_valueImpl> product_template_attribute_values);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_values/createbatch")
    public product_template_attribute_valueImpl createBatch(@RequestBody List<product_template_attribute_valueImpl> product_template_attribute_values);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_values/{id}")
    public product_template_attribute_valueImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_values/removebatch")
    public product_template_attribute_valueImpl removeBatch(@RequestBody List<product_template_attribute_valueImpl> product_template_attribute_values);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_values")
    public product_template_attribute_valueImpl create(@RequestBody product_template_attribute_valueImpl product_template_attribute_value);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_values/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_values/fetchdefault")
    public Page<product_template_attribute_valueImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_values/{id}")
    public product_template_attribute_valueImpl update(@PathVariable("id") Integer id,@RequestBody product_template_attribute_valueImpl product_template_attribute_value);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_values/select")
    public Page<product_template_attribute_valueImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_values/{id}/getdraft")
    public product_template_attribute_valueImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_template_attribute_valueImpl product_template_attribute_value);



}

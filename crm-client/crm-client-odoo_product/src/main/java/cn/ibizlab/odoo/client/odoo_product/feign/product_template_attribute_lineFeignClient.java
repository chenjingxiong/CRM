package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_line;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_template_attribute_line] 服务对象接口
 */
public interface product_template_attribute_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_lines/removebatch")
    public product_template_attribute_lineImpl removeBatch(@RequestBody List<product_template_attribute_lineImpl> product_template_attribute_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_lines/fetchdefault")
    public Page<product_template_attribute_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_lines/createbatch")
    public product_template_attribute_lineImpl createBatch(@RequestBody List<product_template_attribute_lineImpl> product_template_attribute_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_lines/{id}")
    public product_template_attribute_lineImpl update(@PathVariable("id") Integer id,@RequestBody product_template_attribute_lineImpl product_template_attribute_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_lines")
    public product_template_attribute_lineImpl create(@RequestBody product_template_attribute_lineImpl product_template_attribute_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_lines/updatebatch")
    public product_template_attribute_lineImpl updateBatch(@RequestBody List<product_template_attribute_lineImpl> product_template_attribute_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_lines/{id}")
    public product_template_attribute_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_lines/select")
    public Page<product_template_attribute_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_lines/{id}/getdraft")
    public product_template_attribute_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_template_attribute_lineImpl product_template_attribute_line);



}

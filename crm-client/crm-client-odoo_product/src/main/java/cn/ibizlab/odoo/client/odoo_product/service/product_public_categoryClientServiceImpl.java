package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_public_categoryClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_public_categoryImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_public_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_public_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_public_categoryClientServiceImpl implements Iproduct_public_categoryClientService {

    product_public_categoryFeignClient product_public_categoryFeignClient;

    @Autowired
    public product_public_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_public_categoryFeignClient = nameBuilder.target(product_public_categoryFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_public_categoryFeignClient = nameBuilder.target(product_public_categoryFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_public_category createModel() {
		return new product_public_categoryImpl();
	}


    public void get(Iproduct_public_category product_public_category){
        Iproduct_public_category clientModel = product_public_categoryFeignClient.get(product_public_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_public_category.getClass(), false);
        copier.copy(clientModel, product_public_category, null);
    }


    public void create(Iproduct_public_category product_public_category){
        Iproduct_public_category clientModel = product_public_categoryFeignClient.create((product_public_categoryImpl)product_public_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_public_category.getClass(), false);
        copier.copy(clientModel, product_public_category, null);
    }


    public void removeBatch(List<Iproduct_public_category> product_public_categories){
        if(product_public_categories!=null){
            List<product_public_categoryImpl> list = new ArrayList<product_public_categoryImpl>();
            for(Iproduct_public_category iproduct_public_category :product_public_categories){
                list.add((product_public_categoryImpl)iproduct_public_category) ;
            }
            product_public_categoryFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproduct_public_category> fetchDefault(SearchContext context){
        Page<product_public_categoryImpl> page = this.product_public_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iproduct_public_category> product_public_categories){
        if(product_public_categories!=null){
            List<product_public_categoryImpl> list = new ArrayList<product_public_categoryImpl>();
            for(Iproduct_public_category iproduct_public_category :product_public_categories){
                list.add((product_public_categoryImpl)iproduct_public_category) ;
            }
            product_public_categoryFeignClient.createBatch(list) ;
        }
    }


    public void update(Iproduct_public_category product_public_category){
        Iproduct_public_category clientModel = product_public_categoryFeignClient.update(product_public_category.getId(),(product_public_categoryImpl)product_public_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_public_category.getClass(), false);
        copier.copy(clientModel, product_public_category, null);
    }


    public void updateBatch(List<Iproduct_public_category> product_public_categories){
        if(product_public_categories!=null){
            List<product_public_categoryImpl> list = new ArrayList<product_public_categoryImpl>();
            for(Iproduct_public_category iproduct_public_category :product_public_categories){
                list.add((product_public_categoryImpl)iproduct_public_category) ;
            }
            product_public_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iproduct_public_category product_public_category){
        product_public_categoryFeignClient.remove(product_public_category.getId()) ;
    }


    public Page<Iproduct_public_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_public_category product_public_category){
        Iproduct_public_category clientModel = product_public_categoryFeignClient.getDraft(product_public_category.getId(),(product_public_categoryImpl)product_public_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_public_category.getClass(), false);
        copier.copy(clientModel, product_public_category, null);
    }



}


package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_custom_value;
import cn.ibizlab.odoo.client.odoo_product.model.product_attribute_custom_valueImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
public interface product_attribute_custom_valueFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attribute_custom_values/{id}")
    public product_attribute_custom_valueImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attribute_custom_values/fetchdefault")
    public Page<product_attribute_custom_valueImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attribute_custom_values/createbatch")
    public product_attribute_custom_valueImpl createBatch(@RequestBody List<product_attribute_custom_valueImpl> product_attribute_custom_values);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attribute_custom_values/removebatch")
    public product_attribute_custom_valueImpl removeBatch(@RequestBody List<product_attribute_custom_valueImpl> product_attribute_custom_values);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attribute_custom_values")
    public product_attribute_custom_valueImpl create(@RequestBody product_attribute_custom_valueImpl product_attribute_custom_value);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attribute_custom_values/updatebatch")
    public product_attribute_custom_valueImpl updateBatch(@RequestBody List<product_attribute_custom_valueImpl> product_attribute_custom_values);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attribute_custom_values/{id}")
    public product_attribute_custom_valueImpl update(@PathVariable("id") Integer id,@RequestBody product_attribute_custom_valueImpl product_attribute_custom_value);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attribute_custom_values/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attribute_custom_values/select")
    public Page<product_attribute_custom_valueImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attribute_custom_values/{id}/getdraft")
    public product_attribute_custom_valueImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_attribute_custom_valueImpl product_attribute_custom_value);



}

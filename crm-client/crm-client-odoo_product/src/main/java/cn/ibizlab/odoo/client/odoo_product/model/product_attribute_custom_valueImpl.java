package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_custom_value;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[product_attribute_custom_value] 对象
 */
public class product_attribute_custom_valueImpl implements Iproduct_attribute_custom_value,Serializable{

    /**
     * 属性
     */
    public Integer attribute_value_id;

    @JsonIgnore
    public boolean attribute_value_idDirtyFlag;
    
    /**
     * 属性
     */
    public String attribute_value_id_text;

    @JsonIgnore
    public boolean attribute_value_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 自定义值
     */
    public String custom_value;

    @JsonIgnore
    public boolean custom_valueDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 销售订单行
     */
    public Integer sale_order_line_id;

    @JsonIgnore
    public boolean sale_order_line_idDirtyFlag;
    
    /**
     * 销售订单行
     */
    public String sale_order_line_id_text;

    @JsonIgnore
    public boolean sale_order_line_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [属性]
     */
    @JsonProperty("attribute_value_id")
    public Integer getAttribute_value_id(){
        return this.attribute_value_id ;
    }

    /**
     * 设置 [属性]
     */
    @JsonProperty("attribute_value_id")
    public void setAttribute_value_id(Integer  attribute_value_id){
        this.attribute_value_id = attribute_value_id ;
        this.attribute_value_idDirtyFlag = true ;
    }

     /**
     * 获取 [属性]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_idDirtyFlag(){
        return this.attribute_value_idDirtyFlag ;
    }   

    /**
     * 获取 [属性]
     */
    @JsonProperty("attribute_value_id_text")
    public String getAttribute_value_id_text(){
        return this.attribute_value_id_text ;
    }

    /**
     * 设置 [属性]
     */
    @JsonProperty("attribute_value_id_text")
    public void setAttribute_value_id_text(String  attribute_value_id_text){
        this.attribute_value_id_text = attribute_value_id_text ;
        this.attribute_value_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [属性]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_id_textDirtyFlag(){
        return this.attribute_value_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [自定义值]
     */
    @JsonProperty("custom_value")
    public String getCustom_value(){
        return this.custom_value ;
    }

    /**
     * 设置 [自定义值]
     */
    @JsonProperty("custom_value")
    public void setCustom_value(String  custom_value){
        this.custom_value = custom_value ;
        this.custom_valueDirtyFlag = true ;
    }

     /**
     * 获取 [自定义值]脏标记
     */
    @JsonIgnore
    public boolean getCustom_valueDirtyFlag(){
        return this.custom_valueDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单行]
     */
    @JsonProperty("sale_order_line_id")
    public Integer getSale_order_line_id(){
        return this.sale_order_line_id ;
    }

    /**
     * 设置 [销售订单行]
     */
    @JsonProperty("sale_order_line_id")
    public void setSale_order_line_id(Integer  sale_order_line_id){
        this.sale_order_line_id = sale_order_line_id ;
        this.sale_order_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_line_idDirtyFlag(){
        return this.sale_order_line_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单行]
     */
    @JsonProperty("sale_order_line_id_text")
    public String getSale_order_line_id_text(){
        return this.sale_order_line_id_text ;
    }

    /**
     * 设置 [销售订单行]
     */
    @JsonProperty("sale_order_line_id_text")
    public void setSale_order_line_id_text(String  sale_order_line_id_text){
        this.sale_order_line_id_text = sale_order_line_id_text ;
        this.sale_order_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_line_id_textDirtyFlag(){
        return this.sale_order_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

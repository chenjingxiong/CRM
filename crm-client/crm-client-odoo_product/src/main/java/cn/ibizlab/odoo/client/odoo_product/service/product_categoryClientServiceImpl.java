package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_category;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_categoryClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_categoryImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_categoryClientServiceImpl implements Iproduct_categoryClientService {

    product_categoryFeignClient product_categoryFeignClient;

    @Autowired
    public product_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_categoryFeignClient = nameBuilder.target(product_categoryFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_categoryFeignClient = nameBuilder.target(product_categoryFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_category createModel() {
		return new product_categoryImpl();
	}


    public void update(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.update(product_category.getId(),(product_categoryImpl)product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }


    public void removeBatch(List<Iproduct_category> product_categories){
        if(product_categories!=null){
            List<product_categoryImpl> list = new ArrayList<product_categoryImpl>();
            for(Iproduct_category iproduct_category :product_categories){
                list.add((product_categoryImpl)iproduct_category) ;
            }
            product_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.create((product_categoryImpl)product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }


    public void createBatch(List<Iproduct_category> product_categories){
        if(product_categories!=null){
            List<product_categoryImpl> list = new ArrayList<product_categoryImpl>();
            for(Iproduct_category iproduct_category :product_categories){
                list.add((product_categoryImpl)iproduct_category) ;
            }
            product_categoryFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iproduct_category product_category){
        product_categoryFeignClient.remove(product_category.getId()) ;
    }


    public void get(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.get(product_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }


    public Page<Iproduct_category> fetchDefault(SearchContext context){
        Page<product_categoryImpl> page = this.product_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iproduct_category> product_categories){
        if(product_categories!=null){
            List<product_categoryImpl> list = new ArrayList<product_categoryImpl>();
            for(Iproduct_category iproduct_category :product_categories){
                list.add((product_categoryImpl)iproduct_category) ;
            }
            product_categoryFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_category> select(SearchContext context){
        return null ;
    }


    public void save(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.save(product_category.getId(),(product_categoryImpl)product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }


    public void getDraft(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.getDraft(product_category.getId(),(product_categoryImpl)product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }


    public void checkKey(Iproduct_category product_category){
        Iproduct_category clientModel = product_categoryFeignClient.checkKey(product_category.getId(),(product_categoryImpl)product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_category.getClass(), false);
        copier.copy(clientModel, product_category, null);
    }



}


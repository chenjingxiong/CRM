package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_product;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_productClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_productImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_productFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_product] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_productClientServiceImpl implements Iproduct_productClientService {

    product_productFeignClient product_productFeignClient;

    @Autowired
    public product_productClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_productFeignClient = nameBuilder.target(product_productFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_productFeignClient = nameBuilder.target(product_productFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_product createModel() {
		return new product_productImpl();
	}


    public void create(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.create((product_productImpl)product_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }


    public void get(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.get(product_product.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }


    public Page<Iproduct_product> fetchDefault(SearchContext context){
        Page<product_productImpl> page = this.product_productFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iproduct_product> product_products){
        if(product_products!=null){
            List<product_productImpl> list = new ArrayList<product_productImpl>();
            for(Iproduct_product iproduct_product :product_products){
                list.add((product_productImpl)iproduct_product) ;
            }
            product_productFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_product> product_products){
        if(product_products!=null){
            List<product_productImpl> list = new ArrayList<product_productImpl>();
            for(Iproduct_product iproduct_product :product_products){
                list.add((product_productImpl)iproduct_product) ;
            }
            product_productFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iproduct_product product_product){
        product_productFeignClient.remove(product_product.getId()) ;
    }


    public void updateBatch(List<Iproduct_product> product_products){
        if(product_products!=null){
            List<product_productImpl> list = new ArrayList<product_productImpl>();
            for(Iproduct_product iproduct_product :product_products){
                list.add((product_productImpl)iproduct_product) ;
            }
            product_productFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.update(product_product.getId(),(product_productImpl)product_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }


    public Page<Iproduct_product> select(SearchContext context){
        return null ;
    }


    public void save(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.save(product_product.getId(),(product_productImpl)product_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }


    public void checkKey(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.checkKey(product_product.getId(),(product_productImpl)product_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }


    public void getDraft(Iproduct_product product_product){
        Iproduct_product clientModel = product_productFeignClient.getDraft(product_product.getId(),(product_productImpl)product_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_product.getClass(), false);
        copier.copy(clientModel, product_product, null);
    }



}


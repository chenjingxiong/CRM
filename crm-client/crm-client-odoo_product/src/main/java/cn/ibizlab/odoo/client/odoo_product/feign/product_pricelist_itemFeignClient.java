package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelist_itemImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_pricelist_item] 服务对象接口
 */
public interface product_pricelist_itemFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelist_items/removebatch")
    public product_pricelist_itemImpl removeBatch(@RequestBody List<product_pricelist_itemImpl> product_pricelist_items);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelist_items/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelist_items/{id}")
    public product_pricelist_itemImpl update(@PathVariable("id") Integer id,@RequestBody product_pricelist_itemImpl product_pricelist_item);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelist_items/{id}")
    public product_pricelist_itemImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelist_items")
    public product_pricelist_itemImpl create(@RequestBody product_pricelist_itemImpl product_pricelist_item);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelist_items/updatebatch")
    public product_pricelist_itemImpl updateBatch(@RequestBody List<product_pricelist_itemImpl> product_pricelist_items);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelist_items/fetchdefault")
    public Page<product_pricelist_itemImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelist_items/createbatch")
    public product_pricelist_itemImpl createBatch(@RequestBody List<product_pricelist_itemImpl> product_pricelist_items);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelist_items/select")
    public Page<product_pricelist_itemImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelist_items/{id}/getdraft")
    public product_pricelist_itemImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_pricelist_itemImpl product_pricelist_item);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelist_items/{id}/checkkey")
    public product_pricelist_itemImpl checkKey(@PathVariable("id") Integer id,@RequestBody product_pricelist_itemImpl product_pricelist_item);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelist_items/{id}/save")
    public product_pricelist_itemImpl save(@PathVariable("id") Integer id,@RequestBody product_pricelist_itemImpl product_pricelist_item);



}

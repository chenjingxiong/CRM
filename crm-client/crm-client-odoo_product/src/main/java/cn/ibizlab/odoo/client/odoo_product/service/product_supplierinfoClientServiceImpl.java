package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_supplierinfoClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_supplierinfoImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_supplierinfoFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_supplierinfo] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_supplierinfoClientServiceImpl implements Iproduct_supplierinfoClientService {

    product_supplierinfoFeignClient product_supplierinfoFeignClient;

    @Autowired
    public product_supplierinfoClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_supplierinfoFeignClient = nameBuilder.target(product_supplierinfoFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_supplierinfoFeignClient = nameBuilder.target(product_supplierinfoFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_supplierinfo createModel() {
		return new product_supplierinfoImpl();
	}


    public Page<Iproduct_supplierinfo> fetchDefault(SearchContext context){
        Page<product_supplierinfoImpl> page = this.product_supplierinfoFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iproduct_supplierinfo> product_supplierinfos){
        if(product_supplierinfos!=null){
            List<product_supplierinfoImpl> list = new ArrayList<product_supplierinfoImpl>();
            for(Iproduct_supplierinfo iproduct_supplierinfo :product_supplierinfos){
                list.add((product_supplierinfoImpl)iproduct_supplierinfo) ;
            }
            product_supplierinfoFeignClient.createBatch(list) ;
        }
    }


    public void update(Iproduct_supplierinfo product_supplierinfo){
        Iproduct_supplierinfo clientModel = product_supplierinfoFeignClient.update(product_supplierinfo.getId(),(product_supplierinfoImpl)product_supplierinfo) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_supplierinfo.getClass(), false);
        copier.copy(clientModel, product_supplierinfo, null);
    }


    public void remove(Iproduct_supplierinfo product_supplierinfo){
        product_supplierinfoFeignClient.remove(product_supplierinfo.getId()) ;
    }


    public void removeBatch(List<Iproduct_supplierinfo> product_supplierinfos){
        if(product_supplierinfos!=null){
            List<product_supplierinfoImpl> list = new ArrayList<product_supplierinfoImpl>();
            for(Iproduct_supplierinfo iproduct_supplierinfo :product_supplierinfos){
                list.add((product_supplierinfoImpl)iproduct_supplierinfo) ;
            }
            product_supplierinfoFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iproduct_supplierinfo> product_supplierinfos){
        if(product_supplierinfos!=null){
            List<product_supplierinfoImpl> list = new ArrayList<product_supplierinfoImpl>();
            for(Iproduct_supplierinfo iproduct_supplierinfo :product_supplierinfos){
                list.add((product_supplierinfoImpl)iproduct_supplierinfo) ;
            }
            product_supplierinfoFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iproduct_supplierinfo product_supplierinfo){
        Iproduct_supplierinfo clientModel = product_supplierinfoFeignClient.create((product_supplierinfoImpl)product_supplierinfo) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_supplierinfo.getClass(), false);
        copier.copy(clientModel, product_supplierinfo, null);
    }


    public void get(Iproduct_supplierinfo product_supplierinfo){
        Iproduct_supplierinfo clientModel = product_supplierinfoFeignClient.get(product_supplierinfo.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_supplierinfo.getClass(), false);
        copier.copy(clientModel, product_supplierinfo, null);
    }


    public Page<Iproduct_supplierinfo> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_supplierinfo product_supplierinfo){
        Iproduct_supplierinfo clientModel = product_supplierinfoFeignClient.getDraft(product_supplierinfo.getId(),(product_supplierinfoImpl)product_supplierinfo) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_supplierinfo.getClass(), false);
        copier.copy(clientModel, product_supplierinfo, null);
    }



}


package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_confirm;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_confirmClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_confirmImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_confirmFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_confirm] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_confirmClientServiceImpl implements Ievent_confirmClientService {

    event_confirmFeignClient event_confirmFeignClient;

    @Autowired
    public event_confirmClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_confirmFeignClient = nameBuilder.target(event_confirmFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_confirmFeignClient = nameBuilder.target(event_confirmFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_confirm createModel() {
		return new event_confirmImpl();
	}


    public void create(Ievent_confirm event_confirm){
        Ievent_confirm clientModel = event_confirmFeignClient.create((event_confirmImpl)event_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_confirm.getClass(), false);
        copier.copy(clientModel, event_confirm, null);
    }


    public void update(Ievent_confirm event_confirm){
        Ievent_confirm clientModel = event_confirmFeignClient.update(event_confirm.getId(),(event_confirmImpl)event_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_confirm.getClass(), false);
        copier.copy(clientModel, event_confirm, null);
    }


    public Page<Ievent_confirm> fetchDefault(SearchContext context){
        Page<event_confirmImpl> page = this.event_confirmFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ievent_confirm> event_confirms){
        if(event_confirms!=null){
            List<event_confirmImpl> list = new ArrayList<event_confirmImpl>();
            for(Ievent_confirm ievent_confirm :event_confirms){
                list.add((event_confirmImpl)ievent_confirm) ;
            }
            event_confirmFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ievent_confirm> event_confirms){
        if(event_confirms!=null){
            List<event_confirmImpl> list = new ArrayList<event_confirmImpl>();
            for(Ievent_confirm ievent_confirm :event_confirms){
                list.add((event_confirmImpl)ievent_confirm) ;
            }
            event_confirmFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ievent_confirm event_confirm){
        Ievent_confirm clientModel = event_confirmFeignClient.get(event_confirm.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_confirm.getClass(), false);
        copier.copy(clientModel, event_confirm, null);
    }


    public void remove(Ievent_confirm event_confirm){
        event_confirmFeignClient.remove(event_confirm.getId()) ;
    }


    public void createBatch(List<Ievent_confirm> event_confirms){
        if(event_confirms!=null){
            List<event_confirmImpl> list = new ArrayList<event_confirmImpl>();
            for(Ievent_confirm ievent_confirm :event_confirms){
                list.add((event_confirmImpl)ievent_confirm) ;
            }
            event_confirmFeignClient.createBatch(list) ;
        }
    }


    public Page<Ievent_confirm> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_confirm event_confirm){
        Ievent_confirm clientModel = event_confirmFeignClient.getDraft(event_confirm.getId(),(event_confirmImpl)event_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_confirm.getClass(), false);
        copier.copy(clientModel, event_confirm, null);
    }



}


package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_mail;
import cn.ibizlab.odoo.client.odoo_event.model.event_mailImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_mail] 服务对象接口
 */
public interface event_mailFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mails/removebatch")
    public event_mailImpl removeBatch(@RequestBody List<event_mailImpl> event_mails);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mails/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mails")
    public event_mailImpl create(@RequestBody event_mailImpl event_mail);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mails/createbatch")
    public event_mailImpl createBatch(@RequestBody List<event_mailImpl> event_mails);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mails/updatebatch")
    public event_mailImpl updateBatch(@RequestBody List<event_mailImpl> event_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mails/fetchdefault")
    public Page<event_mailImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mails/{id}")
    public event_mailImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mails/{id}")
    public event_mailImpl update(@PathVariable("id") Integer id,@RequestBody event_mailImpl event_mail);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mails/select")
    public Page<event_mailImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mails/{id}/getdraft")
    public event_mailImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_mailImpl event_mail);



}

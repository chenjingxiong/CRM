package cn.ibizlab.odoo.client.odoo_event.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.service.odoo.event")
@Data
public class odoo_eventClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

	private String serviceUrl ;
    	
    private String serviceId ;

}

package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_type;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_typeClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_typeImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_typeClientServiceImpl implements Ievent_typeClientService {

    event_typeFeignClient event_typeFeignClient;

    @Autowired
    public event_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_typeFeignClient = nameBuilder.target(event_typeFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_typeFeignClient = nameBuilder.target(event_typeFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_type createModel() {
		return new event_typeImpl();
	}


    public void updateBatch(List<Ievent_type> event_types){
        if(event_types!=null){
            List<event_typeImpl> list = new ArrayList<event_typeImpl>();
            for(Ievent_type ievent_type :event_types){
                list.add((event_typeImpl)ievent_type) ;
            }
            event_typeFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ievent_type> event_types){
        if(event_types!=null){
            List<event_typeImpl> list = new ArrayList<event_typeImpl>();
            for(Ievent_type ievent_type :event_types){
                list.add((event_typeImpl)ievent_type) ;
            }
            event_typeFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ievent_type> event_types){
        if(event_types!=null){
            List<event_typeImpl> list = new ArrayList<event_typeImpl>();
            for(Ievent_type ievent_type :event_types){
                list.add((event_typeImpl)ievent_type) ;
            }
            event_typeFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ievent_type event_type){
        Ievent_type clientModel = event_typeFeignClient.get(event_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type.getClass(), false);
        copier.copy(clientModel, event_type, null);
    }


    public Page<Ievent_type> fetchDefault(SearchContext context){
        Page<event_typeImpl> page = this.event_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ievent_type event_type){
        Ievent_type clientModel = event_typeFeignClient.create((event_typeImpl)event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type.getClass(), false);
        copier.copy(clientModel, event_type, null);
    }


    public void remove(Ievent_type event_type){
        event_typeFeignClient.remove(event_type.getId()) ;
    }


    public void update(Ievent_type event_type){
        Ievent_type clientModel = event_typeFeignClient.update(event_type.getId(),(event_typeImpl)event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type.getClass(), false);
        copier.copy(clientModel, event_type, null);
    }


    public Page<Ievent_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_type event_type){
        Ievent_type clientModel = event_typeFeignClient.getDraft(event_type.getId(),(event_typeImpl)event_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_type.getClass(), false);
        copier.copy(clientModel, event_type, null);
    }



}


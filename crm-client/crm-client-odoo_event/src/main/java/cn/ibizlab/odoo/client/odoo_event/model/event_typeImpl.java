package cn.ibizlab.odoo.client.odoo_event.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ievent_type;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[event_type] 对象
 */
public class event_typeImpl implements Ievent_type,Serializable{

    /**
     * 自动确认注册
     */
    public String auto_confirm;

    @JsonIgnore
    public boolean auto_confirmDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * Twitter主题标签
     */
    public String default_hashtag;

    @JsonIgnore
    public boolean default_hashtagDirtyFlag;
    
    /**
     * 最大注册数
     */
    public Integer default_registration_max;

    @JsonIgnore
    public boolean default_registration_maxDirtyFlag;
    
    /**
     * 最小注册数
     */
    public Integer default_registration_min;

    @JsonIgnore
    public boolean default_registration_minDirtyFlag;
    
    /**
     * 时区
     */
    public String default_timezone;

    @JsonIgnore
    public boolean default_timezoneDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 入场券
     */
    public String event_ticket_ids;

    @JsonIgnore
    public boolean event_ticket_idsDirtyFlag;
    
    /**
     * 邮件排程
     */
    public String event_type_mail_ids;

    @JsonIgnore
    public boolean event_type_mail_idsDirtyFlag;
    
    /**
     * 有限名额
     */
    public String has_seats_limitation;

    @JsonIgnore
    public boolean has_seats_limitationDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 在线活动
     */
    public String is_online;

    @JsonIgnore
    public boolean is_onlineDirtyFlag;
    
    /**
     * 活动类别
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 使用默认主题标签
     */
    public String use_hashtag;

    @JsonIgnore
    public boolean use_hashtagDirtyFlag;
    
    /**
     * 自动发送EMail
     */
    public String use_mail_schedule;

    @JsonIgnore
    public boolean use_mail_scheduleDirtyFlag;
    
    /**
     * 出票
     */
    public String use_ticketing;

    @JsonIgnore
    public boolean use_ticketingDirtyFlag;
    
    /**
     * 使用默认时区
     */
    public String use_timezone;

    @JsonIgnore
    public boolean use_timezoneDirtyFlag;
    
    /**
     * 在网站上显示专用菜单
     */
    public String website_menu;

    @JsonIgnore
    public boolean website_menuDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [自动确认注册]
     */
    @JsonProperty("auto_confirm")
    public String getAuto_confirm(){
        return this.auto_confirm ;
    }

    /**
     * 设置 [自动确认注册]
     */
    @JsonProperty("auto_confirm")
    public void setAuto_confirm(String  auto_confirm){
        this.auto_confirm = auto_confirm ;
        this.auto_confirmDirtyFlag = true ;
    }

     /**
     * 获取 [自动确认注册]脏标记
     */
    @JsonIgnore
    public boolean getAuto_confirmDirtyFlag(){
        return this.auto_confirmDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [Twitter主题标签]
     */
    @JsonProperty("default_hashtag")
    public String getDefault_hashtag(){
        return this.default_hashtag ;
    }

    /**
     * 设置 [Twitter主题标签]
     */
    @JsonProperty("default_hashtag")
    public void setDefault_hashtag(String  default_hashtag){
        this.default_hashtag = default_hashtag ;
        this.default_hashtagDirtyFlag = true ;
    }

     /**
     * 获取 [Twitter主题标签]脏标记
     */
    @JsonIgnore
    public boolean getDefault_hashtagDirtyFlag(){
        return this.default_hashtagDirtyFlag ;
    }   

    /**
     * 获取 [最大注册数]
     */
    @JsonProperty("default_registration_max")
    public Integer getDefault_registration_max(){
        return this.default_registration_max ;
    }

    /**
     * 设置 [最大注册数]
     */
    @JsonProperty("default_registration_max")
    public void setDefault_registration_max(Integer  default_registration_max){
        this.default_registration_max = default_registration_max ;
        this.default_registration_maxDirtyFlag = true ;
    }

     /**
     * 获取 [最大注册数]脏标记
     */
    @JsonIgnore
    public boolean getDefault_registration_maxDirtyFlag(){
        return this.default_registration_maxDirtyFlag ;
    }   

    /**
     * 获取 [最小注册数]
     */
    @JsonProperty("default_registration_min")
    public Integer getDefault_registration_min(){
        return this.default_registration_min ;
    }

    /**
     * 设置 [最小注册数]
     */
    @JsonProperty("default_registration_min")
    public void setDefault_registration_min(Integer  default_registration_min){
        this.default_registration_min = default_registration_min ;
        this.default_registration_minDirtyFlag = true ;
    }

     /**
     * 获取 [最小注册数]脏标记
     */
    @JsonIgnore
    public boolean getDefault_registration_minDirtyFlag(){
        return this.default_registration_minDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("default_timezone")
    public String getDefault_timezone(){
        return this.default_timezone ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("default_timezone")
    public void setDefault_timezone(String  default_timezone){
        this.default_timezone = default_timezone ;
        this.default_timezoneDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getDefault_timezoneDirtyFlag(){
        return this.default_timezoneDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [入场券]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return this.event_ticket_ids ;
    }

    /**
     * 设置 [入场券]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

     /**
     * 获取 [入场券]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return this.event_ticket_idsDirtyFlag ;
    }   

    /**
     * 获取 [邮件排程]
     */
    @JsonProperty("event_type_mail_ids")
    public String getEvent_type_mail_ids(){
        return this.event_type_mail_ids ;
    }

    /**
     * 设置 [邮件排程]
     */
    @JsonProperty("event_type_mail_ids")
    public void setEvent_type_mail_ids(String  event_type_mail_ids){
        this.event_type_mail_ids = event_type_mail_ids ;
        this.event_type_mail_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件排程]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_mail_idsDirtyFlag(){
        return this.event_type_mail_idsDirtyFlag ;
    }   

    /**
     * 获取 [有限名额]
     */
    @JsonProperty("has_seats_limitation")
    public String getHas_seats_limitation(){
        return this.has_seats_limitation ;
    }

    /**
     * 设置 [有限名额]
     */
    @JsonProperty("has_seats_limitation")
    public void setHas_seats_limitation(String  has_seats_limitation){
        this.has_seats_limitation = has_seats_limitation ;
        this.has_seats_limitationDirtyFlag = true ;
    }

     /**
     * 获取 [有限名额]脏标记
     */
    @JsonIgnore
    public boolean getHas_seats_limitationDirtyFlag(){
        return this.has_seats_limitationDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [在线活动]
     */
    @JsonProperty("is_online")
    public String getIs_online(){
        return this.is_online ;
    }

    /**
     * 设置 [在线活动]
     */
    @JsonProperty("is_online")
    public void setIs_online(String  is_online){
        this.is_online = is_online ;
        this.is_onlineDirtyFlag = true ;
    }

     /**
     * 获取 [在线活动]脏标记
     */
    @JsonIgnore
    public boolean getIs_onlineDirtyFlag(){
        return this.is_onlineDirtyFlag ;
    }   

    /**
     * 获取 [活动类别]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [活动类别]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [活动类别]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [使用默认主题标签]
     */
    @JsonProperty("use_hashtag")
    public String getUse_hashtag(){
        return this.use_hashtag ;
    }

    /**
     * 设置 [使用默认主题标签]
     */
    @JsonProperty("use_hashtag")
    public void setUse_hashtag(String  use_hashtag){
        this.use_hashtag = use_hashtag ;
        this.use_hashtagDirtyFlag = true ;
    }

     /**
     * 获取 [使用默认主题标签]脏标记
     */
    @JsonIgnore
    public boolean getUse_hashtagDirtyFlag(){
        return this.use_hashtagDirtyFlag ;
    }   

    /**
     * 获取 [自动发送EMail]
     */
    @JsonProperty("use_mail_schedule")
    public String getUse_mail_schedule(){
        return this.use_mail_schedule ;
    }

    /**
     * 设置 [自动发送EMail]
     */
    @JsonProperty("use_mail_schedule")
    public void setUse_mail_schedule(String  use_mail_schedule){
        this.use_mail_schedule = use_mail_schedule ;
        this.use_mail_scheduleDirtyFlag = true ;
    }

     /**
     * 获取 [自动发送EMail]脏标记
     */
    @JsonIgnore
    public boolean getUse_mail_scheduleDirtyFlag(){
        return this.use_mail_scheduleDirtyFlag ;
    }   

    /**
     * 获取 [出票]
     */
    @JsonProperty("use_ticketing")
    public String getUse_ticketing(){
        return this.use_ticketing ;
    }

    /**
     * 设置 [出票]
     */
    @JsonProperty("use_ticketing")
    public void setUse_ticketing(String  use_ticketing){
        this.use_ticketing = use_ticketing ;
        this.use_ticketingDirtyFlag = true ;
    }

     /**
     * 获取 [出票]脏标记
     */
    @JsonIgnore
    public boolean getUse_ticketingDirtyFlag(){
        return this.use_ticketingDirtyFlag ;
    }   

    /**
     * 获取 [使用默认时区]
     */
    @JsonProperty("use_timezone")
    public String getUse_timezone(){
        return this.use_timezone ;
    }

    /**
     * 设置 [使用默认时区]
     */
    @JsonProperty("use_timezone")
    public void setUse_timezone(String  use_timezone){
        this.use_timezone = use_timezone ;
        this.use_timezoneDirtyFlag = true ;
    }

     /**
     * 获取 [使用默认时区]脏标记
     */
    @JsonIgnore
    public boolean getUse_timezoneDirtyFlag(){
        return this.use_timezoneDirtyFlag ;
    }   

    /**
     * 获取 [在网站上显示专用菜单]
     */
    @JsonProperty("website_menu")
    public String getWebsite_menu(){
        return this.website_menu ;
    }

    /**
     * 设置 [在网站上显示专用菜单]
     */
    @JsonProperty("website_menu")
    public void setWebsite_menu(String  website_menu){
        this.website_menu = website_menu ;
        this.website_menuDirtyFlag = true ;
    }

     /**
     * 获取 [在网站上显示专用菜单]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_menuDirtyFlag(){
        return this.website_menuDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

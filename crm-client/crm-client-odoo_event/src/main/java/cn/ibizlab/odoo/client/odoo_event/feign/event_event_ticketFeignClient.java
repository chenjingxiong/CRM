package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;
import cn.ibizlab.odoo.client.odoo_event.model.event_event_ticketImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_event_ticket] 服务对象接口
 */
public interface event_event_ticketFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_event_tickets")
    public event_event_ticketImpl create(@RequestBody event_event_ticketImpl event_event_ticket);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_event_tickets/{id}")
    public event_event_ticketImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_event_tickets/{id}")
    public event_event_ticketImpl update(@PathVariable("id") Integer id,@RequestBody event_event_ticketImpl event_event_ticket);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_event_tickets/removebatch")
    public event_event_ticketImpl removeBatch(@RequestBody List<event_event_ticketImpl> event_event_tickets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_event_tickets/fetchdefault")
    public Page<event_event_ticketImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_event_tickets/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_event_tickets/createbatch")
    public event_event_ticketImpl createBatch(@RequestBody List<event_event_ticketImpl> event_event_tickets);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_event_tickets/updatebatch")
    public event_event_ticketImpl updateBatch(@RequestBody List<event_event_ticketImpl> event_event_tickets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_event_tickets/select")
    public Page<event_event_ticketImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_event_tickets/{id}/getdraft")
    public event_event_ticketImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_event_ticketImpl event_event_ticket);



}

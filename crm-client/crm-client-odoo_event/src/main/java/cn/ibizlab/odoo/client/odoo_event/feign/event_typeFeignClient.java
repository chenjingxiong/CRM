package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_type;
import cn.ibizlab.odoo.client.odoo_event.model.event_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_type] 服务对象接口
 */
public interface event_typeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_types/updatebatch")
    public event_typeImpl updateBatch(@RequestBody List<event_typeImpl> event_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_types/createbatch")
    public event_typeImpl createBatch(@RequestBody List<event_typeImpl> event_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_types/removebatch")
    public event_typeImpl removeBatch(@RequestBody List<event_typeImpl> event_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_types/{id}")
    public event_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_types/fetchdefault")
    public Page<event_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_types")
    public event_typeImpl create(@RequestBody event_typeImpl event_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_types/{id}")
    public event_typeImpl update(@PathVariable("id") Integer id,@RequestBody event_typeImpl event_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_types/select")
    public Page<event_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_types/{id}/getdraft")
    public event_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_typeImpl event_type);



}

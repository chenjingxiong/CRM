package cn.ibizlab.odoo.client.odoo_event.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ievent_mail_registration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[event_mail_registration] 对象
 */
public class event_mail_registrationImpl implements Ievent_mail_registration,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * EMail发送
     */
    public String mail_sent;

    @JsonIgnore
    public boolean mail_sentDirtyFlag;
    
    /**
     * 出席者
     */
    public Integer registration_id;

    @JsonIgnore
    public boolean registration_idDirtyFlag;
    
    /**
     * 出席者
     */
    public String registration_id_text;

    @JsonIgnore
    public boolean registration_id_textDirtyFlag;
    
    /**
     * 定期时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 邮件调度
     */
    public Integer scheduler_id;

    @JsonIgnore
    public boolean scheduler_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [EMail发送]
     */
    @JsonProperty("mail_sent")
    public String getMail_sent(){
        return this.mail_sent ;
    }

    /**
     * 设置 [EMail发送]
     */
    @JsonProperty("mail_sent")
    public void setMail_sent(String  mail_sent){
        this.mail_sent = mail_sent ;
        this.mail_sentDirtyFlag = true ;
    }

     /**
     * 获取 [EMail发送]脏标记
     */
    @JsonIgnore
    public boolean getMail_sentDirtyFlag(){
        return this.mail_sentDirtyFlag ;
    }   

    /**
     * 获取 [出席者]
     */
    @JsonProperty("registration_id")
    public Integer getRegistration_id(){
        return this.registration_id ;
    }

    /**
     * 设置 [出席者]
     */
    @JsonProperty("registration_id")
    public void setRegistration_id(Integer  registration_id){
        this.registration_id = registration_id ;
        this.registration_idDirtyFlag = true ;
    }

     /**
     * 获取 [出席者]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idDirtyFlag(){
        return this.registration_idDirtyFlag ;
    }   

    /**
     * 获取 [出席者]
     */
    @JsonProperty("registration_id_text")
    public String getRegistration_id_text(){
        return this.registration_id_text ;
    }

    /**
     * 设置 [出席者]
     */
    @JsonProperty("registration_id_text")
    public void setRegistration_id_text(String  registration_id_text){
        this.registration_id_text = registration_id_text ;
        this.registration_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [出席者]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_id_textDirtyFlag(){
        return this.registration_id_textDirtyFlag ;
    }   

    /**
     * 获取 [定期时间]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [定期时间]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [定期时间]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [邮件调度]
     */
    @JsonProperty("scheduler_id")
    public Integer getScheduler_id(){
        return this.scheduler_id ;
    }

    /**
     * 设置 [邮件调度]
     */
    @JsonProperty("scheduler_id")
    public void setScheduler_id(Integer  scheduler_id){
        this.scheduler_id = scheduler_id ;
        this.scheduler_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件调度]脏标记
     */
    @JsonIgnore
    public boolean getScheduler_idDirtyFlag(){
        return this.scheduler_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

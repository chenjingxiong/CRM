package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_confirm;
import cn.ibizlab.odoo.client.odoo_event.model.event_confirmImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_confirm] 服务对象接口
 */
public interface event_confirmFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_confirms")
    public event_confirmImpl create(@RequestBody event_confirmImpl event_confirm);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_confirms/{id}")
    public event_confirmImpl update(@PathVariable("id") Integer id,@RequestBody event_confirmImpl event_confirm);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_confirms/fetchdefault")
    public Page<event_confirmImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_confirms/removebatch")
    public event_confirmImpl removeBatch(@RequestBody List<event_confirmImpl> event_confirms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_confirms/updatebatch")
    public event_confirmImpl updateBatch(@RequestBody List<event_confirmImpl> event_confirms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_confirms/{id}")
    public event_confirmImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_confirms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_confirms/createbatch")
    public event_confirmImpl createBatch(@RequestBody List<event_confirmImpl> event_confirms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_confirms/select")
    public Page<event_confirmImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_confirms/{id}/getdraft")
    public event_confirmImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_confirmImpl event_confirm);



}

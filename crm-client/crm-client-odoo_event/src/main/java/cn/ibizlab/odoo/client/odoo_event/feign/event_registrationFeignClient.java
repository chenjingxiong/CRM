package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_registration;
import cn.ibizlab.odoo.client.odoo_event.model.event_registrationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_registration] 服务对象接口
 */
public interface event_registrationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_registrations/{id}")
    public event_registrationImpl update(@PathVariable("id") Integer id,@RequestBody event_registrationImpl event_registration);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_registrations/updatebatch")
    public event_registrationImpl updateBatch(@RequestBody List<event_registrationImpl> event_registrations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_registrations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_registrations/{id}")
    public event_registrationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_registrations")
    public event_registrationImpl create(@RequestBody event_registrationImpl event_registration);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_registrations/fetchdefault")
    public Page<event_registrationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_registrations/createbatch")
    public event_registrationImpl createBatch(@RequestBody List<event_registrationImpl> event_registrations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_registrations/removebatch")
    public event_registrationImpl removeBatch(@RequestBody List<event_registrationImpl> event_registrations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_registrations/select")
    public Page<event_registrationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_registrations/{id}/getdraft")
    public event_registrationImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_registrationImpl event_registration);



}

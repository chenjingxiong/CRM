package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_order_make_invoice;
import cn.ibizlab.odoo.client.odoo_repair.config.odoo_repairClientProperties;
import cn.ibizlab.odoo.core.client.service.Irepair_order_make_invoiceClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_order_make_invoiceImpl;
import cn.ibizlab.odoo.client.odoo_repair.feign.repair_order_make_invoiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class repair_order_make_invoiceClientServiceImpl implements Irepair_order_make_invoiceClientService {

    repair_order_make_invoiceFeignClient repair_order_make_invoiceFeignClient;

    @Autowired
    public repair_order_make_invoiceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_repairClientProperties odoo_repairClientProperties) {
        if (odoo_repairClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_order_make_invoiceFeignClient = nameBuilder.target(repair_order_make_invoiceFeignClient.class,"http://"+odoo_repairClientProperties.getServiceId()+"/") ;
		}else if (odoo_repairClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_order_make_invoiceFeignClient = nameBuilder.target(repair_order_make_invoiceFeignClient.class,odoo_repairClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irepair_order_make_invoice createModel() {
		return new repair_order_make_invoiceImpl();
	}


    public void update(Irepair_order_make_invoice repair_order_make_invoice){
        Irepair_order_make_invoice clientModel = repair_order_make_invoiceFeignClient.update(repair_order_make_invoice.getId(),(repair_order_make_invoiceImpl)repair_order_make_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order_make_invoice.getClass(), false);
        copier.copy(clientModel, repair_order_make_invoice, null);
    }


    public void create(Irepair_order_make_invoice repair_order_make_invoice){
        Irepair_order_make_invoice clientModel = repair_order_make_invoiceFeignClient.create((repair_order_make_invoiceImpl)repair_order_make_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order_make_invoice.getClass(), false);
        copier.copy(clientModel, repair_order_make_invoice, null);
    }


    public void get(Irepair_order_make_invoice repair_order_make_invoice){
        Irepair_order_make_invoice clientModel = repair_order_make_invoiceFeignClient.get(repair_order_make_invoice.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order_make_invoice.getClass(), false);
        copier.copy(clientModel, repair_order_make_invoice, null);
    }


    public void updateBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
        if(repair_order_make_invoices!=null){
            List<repair_order_make_invoiceImpl> list = new ArrayList<repair_order_make_invoiceImpl>();
            for(Irepair_order_make_invoice irepair_order_make_invoice :repair_order_make_invoices){
                list.add((repair_order_make_invoiceImpl)irepair_order_make_invoice) ;
            }
            repair_order_make_invoiceFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
        if(repair_order_make_invoices!=null){
            List<repair_order_make_invoiceImpl> list = new ArrayList<repair_order_make_invoiceImpl>();
            for(Irepair_order_make_invoice irepair_order_make_invoice :repair_order_make_invoices){
                list.add((repair_order_make_invoiceImpl)irepair_order_make_invoice) ;
            }
            repair_order_make_invoiceFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Irepair_order_make_invoice> repair_order_make_invoices){
        if(repair_order_make_invoices!=null){
            List<repair_order_make_invoiceImpl> list = new ArrayList<repair_order_make_invoiceImpl>();
            for(Irepair_order_make_invoice irepair_order_make_invoice :repair_order_make_invoices){
                list.add((repair_order_make_invoiceImpl)irepair_order_make_invoice) ;
            }
            repair_order_make_invoiceFeignClient.removeBatch(list) ;
        }
    }


    public Page<Irepair_order_make_invoice> fetchDefault(SearchContext context){
        Page<repair_order_make_invoiceImpl> page = this.repair_order_make_invoiceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Irepair_order_make_invoice repair_order_make_invoice){
        repair_order_make_invoiceFeignClient.remove(repair_order_make_invoice.getId()) ;
    }


    public Page<Irepair_order_make_invoice> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irepair_order_make_invoice repair_order_make_invoice){
        Irepair_order_make_invoice clientModel = repair_order_make_invoiceFeignClient.getDraft(repair_order_make_invoice.getId(),(repair_order_make_invoiceImpl)repair_order_make_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order_make_invoice.getClass(), false);
        copier.copy(clientModel, repair_order_make_invoice, null);
    }



}


package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_cancel;
import cn.ibizlab.odoo.client.odoo_repair.config.odoo_repairClientProperties;
import cn.ibizlab.odoo.core.client.service.Irepair_cancelClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_cancelImpl;
import cn.ibizlab.odoo.client.odoo_repair.feign.repair_cancelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[repair_cancel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class repair_cancelClientServiceImpl implements Irepair_cancelClientService {

    repair_cancelFeignClient repair_cancelFeignClient;

    @Autowired
    public repair_cancelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_repairClientProperties odoo_repairClientProperties) {
        if (odoo_repairClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_cancelFeignClient = nameBuilder.target(repair_cancelFeignClient.class,"http://"+odoo_repairClientProperties.getServiceId()+"/") ;
		}else if (odoo_repairClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_cancelFeignClient = nameBuilder.target(repair_cancelFeignClient.class,odoo_repairClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irepair_cancel createModel() {
		return new repair_cancelImpl();
	}


    public void create(Irepair_cancel repair_cancel){
        Irepair_cancel clientModel = repair_cancelFeignClient.create((repair_cancelImpl)repair_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_cancel.getClass(), false);
        copier.copy(clientModel, repair_cancel, null);
    }


    public void createBatch(List<Irepair_cancel> repair_cancels){
        if(repair_cancels!=null){
            List<repair_cancelImpl> list = new ArrayList<repair_cancelImpl>();
            for(Irepair_cancel irepair_cancel :repair_cancels){
                list.add((repair_cancelImpl)irepair_cancel) ;
            }
            repair_cancelFeignClient.createBatch(list) ;
        }
    }


    public void get(Irepair_cancel repair_cancel){
        Irepair_cancel clientModel = repair_cancelFeignClient.get(repair_cancel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_cancel.getClass(), false);
        copier.copy(clientModel, repair_cancel, null);
    }


    public Page<Irepair_cancel> fetchDefault(SearchContext context){
        Page<repair_cancelImpl> page = this.repair_cancelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Irepair_cancel repair_cancel){
        Irepair_cancel clientModel = repair_cancelFeignClient.update(repair_cancel.getId(),(repair_cancelImpl)repair_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_cancel.getClass(), false);
        copier.copy(clientModel, repair_cancel, null);
    }


    public void updateBatch(List<Irepair_cancel> repair_cancels){
        if(repair_cancels!=null){
            List<repair_cancelImpl> list = new ArrayList<repair_cancelImpl>();
            for(Irepair_cancel irepair_cancel :repair_cancels){
                list.add((repair_cancelImpl)irepair_cancel) ;
            }
            repair_cancelFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Irepair_cancel repair_cancel){
        repair_cancelFeignClient.remove(repair_cancel.getId()) ;
    }


    public void removeBatch(List<Irepair_cancel> repair_cancels){
        if(repair_cancels!=null){
            List<repair_cancelImpl> list = new ArrayList<repair_cancelImpl>();
            for(Irepair_cancel irepair_cancel :repair_cancels){
                list.add((repair_cancelImpl)irepair_cancel) ;
            }
            repair_cancelFeignClient.removeBatch(list) ;
        }
    }


    public Page<Irepair_cancel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irepair_cancel repair_cancel){
        Irepair_cancel clientModel = repair_cancelFeignClient.getDraft(repair_cancel.getId(),(repair_cancelImpl)repair_cancel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_cancel.getClass(), false);
        copier.copy(clientModel, repair_cancel, null);
    }



}


package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_fee;
import cn.ibizlab.odoo.client.odoo_repair.config.odoo_repairClientProperties;
import cn.ibizlab.odoo.core.client.service.Irepair_feeClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_feeImpl;
import cn.ibizlab.odoo.client.odoo_repair.feign.repair_feeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[repair_fee] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class repair_feeClientServiceImpl implements Irepair_feeClientService {

    repair_feeFeignClient repair_feeFeignClient;

    @Autowired
    public repair_feeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_repairClientProperties odoo_repairClientProperties) {
        if (odoo_repairClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_feeFeignClient = nameBuilder.target(repair_feeFeignClient.class,"http://"+odoo_repairClientProperties.getServiceId()+"/") ;
		}else if (odoo_repairClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_feeFeignClient = nameBuilder.target(repair_feeFeignClient.class,odoo_repairClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irepair_fee createModel() {
		return new repair_feeImpl();
	}


    public void get(Irepair_fee repair_fee){
        Irepair_fee clientModel = repair_feeFeignClient.get(repair_fee.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_fee.getClass(), false);
        copier.copy(clientModel, repair_fee, null);
    }


    public void create(Irepair_fee repair_fee){
        Irepair_fee clientModel = repair_feeFeignClient.create((repair_feeImpl)repair_fee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_fee.getClass(), false);
        copier.copy(clientModel, repair_fee, null);
    }


    public void updateBatch(List<Irepair_fee> repair_fees){
        if(repair_fees!=null){
            List<repair_feeImpl> list = new ArrayList<repair_feeImpl>();
            for(Irepair_fee irepair_fee :repair_fees){
                list.add((repair_feeImpl)irepair_fee) ;
            }
            repair_feeFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Irepair_fee> repair_fees){
        if(repair_fees!=null){
            List<repair_feeImpl> list = new ArrayList<repair_feeImpl>();
            for(Irepair_fee irepair_fee :repair_fees){
                list.add((repair_feeImpl)irepair_fee) ;
            }
            repair_feeFeignClient.createBatch(list) ;
        }
    }


    public void update(Irepair_fee repair_fee){
        Irepair_fee clientModel = repair_feeFeignClient.update(repair_fee.getId(),(repair_feeImpl)repair_fee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_fee.getClass(), false);
        copier.copy(clientModel, repair_fee, null);
    }


    public void removeBatch(List<Irepair_fee> repair_fees){
        if(repair_fees!=null){
            List<repair_feeImpl> list = new ArrayList<repair_feeImpl>();
            for(Irepair_fee irepair_fee :repair_fees){
                list.add((repair_feeImpl)irepair_fee) ;
            }
            repair_feeFeignClient.removeBatch(list) ;
        }
    }


    public Page<Irepair_fee> fetchDefault(SearchContext context){
        Page<repair_feeImpl> page = this.repair_feeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Irepair_fee repair_fee){
        repair_feeFeignClient.remove(repair_fee.getId()) ;
    }


    public Page<Irepair_fee> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irepair_fee repair_fee){
        Irepair_fee clientModel = repair_feeFeignClient.getDraft(repair_fee.getId(),(repair_feeImpl)repair_fee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_fee.getClass(), false);
        copier.copy(clientModel, repair_fee, null);
    }



}


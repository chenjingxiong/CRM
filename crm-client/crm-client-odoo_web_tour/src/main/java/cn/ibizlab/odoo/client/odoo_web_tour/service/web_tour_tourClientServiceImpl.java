package cn.ibizlab.odoo.client.odoo_web_tour.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_tour_tour;
import cn.ibizlab.odoo.client.odoo_web_tour.config.odoo_web_tourClientProperties;
import cn.ibizlab.odoo.core.client.service.Iweb_tour_tourClientService;
import cn.ibizlab.odoo.client.odoo_web_tour.model.web_tour_tourImpl;
import cn.ibizlab.odoo.client.odoo_web_tour.feign.web_tour_tourFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[web_tour_tour] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class web_tour_tourClientServiceImpl implements Iweb_tour_tourClientService {

    web_tour_tourFeignClient web_tour_tourFeignClient;

    @Autowired
    public web_tour_tourClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_web_tourClientProperties odoo_web_tourClientProperties) {
        if (odoo_web_tourClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_tour_tourFeignClient = nameBuilder.target(web_tour_tourFeignClient.class,"http://"+odoo_web_tourClientProperties.getServiceId()+"/") ;
		}else if (odoo_web_tourClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_tour_tourFeignClient = nameBuilder.target(web_tour_tourFeignClient.class,odoo_web_tourClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iweb_tour_tour createModel() {
		return new web_tour_tourImpl();
	}


    public void createBatch(List<Iweb_tour_tour> web_tour_tours){
        if(web_tour_tours!=null){
            List<web_tour_tourImpl> list = new ArrayList<web_tour_tourImpl>();
            for(Iweb_tour_tour iweb_tour_tour :web_tour_tours){
                list.add((web_tour_tourImpl)iweb_tour_tour) ;
            }
            web_tour_tourFeignClient.createBatch(list) ;
        }
    }


    public void get(Iweb_tour_tour web_tour_tour){
        Iweb_tour_tour clientModel = web_tour_tourFeignClient.get(web_tour_tour.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_tour_tour.getClass(), false);
        copier.copy(clientModel, web_tour_tour, null);
    }


    public void removeBatch(List<Iweb_tour_tour> web_tour_tours){
        if(web_tour_tours!=null){
            List<web_tour_tourImpl> list = new ArrayList<web_tour_tourImpl>();
            for(Iweb_tour_tour iweb_tour_tour :web_tour_tours){
                list.add((web_tour_tourImpl)iweb_tour_tour) ;
            }
            web_tour_tourFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iweb_tour_tour> web_tour_tours){
        if(web_tour_tours!=null){
            List<web_tour_tourImpl> list = new ArrayList<web_tour_tourImpl>();
            for(Iweb_tour_tour iweb_tour_tour :web_tour_tours){
                list.add((web_tour_tourImpl)iweb_tour_tour) ;
            }
            web_tour_tourFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iweb_tour_tour web_tour_tour){
        Iweb_tour_tour clientModel = web_tour_tourFeignClient.update(web_tour_tour.getId(),(web_tour_tourImpl)web_tour_tour) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_tour_tour.getClass(), false);
        copier.copy(clientModel, web_tour_tour, null);
    }


    public Page<Iweb_tour_tour> fetchDefault(SearchContext context){
        Page<web_tour_tourImpl> page = this.web_tour_tourFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iweb_tour_tour web_tour_tour){
        web_tour_tourFeignClient.remove(web_tour_tour.getId()) ;
    }


    public void create(Iweb_tour_tour web_tour_tour){
        Iweb_tour_tour clientModel = web_tour_tourFeignClient.create((web_tour_tourImpl)web_tour_tour) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_tour_tour.getClass(), false);
        copier.copy(clientModel, web_tour_tour, null);
    }


    public Page<Iweb_tour_tour> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iweb_tour_tour web_tour_tour){
        Iweb_tour_tour clientModel = web_tour_tourFeignClient.getDraft(web_tour_tour.getId(),(web_tour_tourImpl)web_tour_tour) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_tour_tour.getClass(), false);
        copier.copy(clientModel, web_tour_tour, null);
    }



}


package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_challengeClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challengeImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_challengeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_challenge] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_challengeClientServiceImpl implements Igamification_challengeClientService {

    gamification_challengeFeignClient gamification_challengeFeignClient;

    @Autowired
    public gamification_challengeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_challengeFeignClient = nameBuilder.target(gamification_challengeFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_challengeFeignClient = nameBuilder.target(gamification_challengeFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_challenge createModel() {
		return new gamification_challengeImpl();
	}


    public void create(Igamification_challenge gamification_challenge){
        Igamification_challenge clientModel = gamification_challengeFeignClient.create((gamification_challengeImpl)gamification_challenge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge.getClass(), false);
        copier.copy(clientModel, gamification_challenge, null);
    }


    public void createBatch(List<Igamification_challenge> gamification_challenges){
        if(gamification_challenges!=null){
            List<gamification_challengeImpl> list = new ArrayList<gamification_challengeImpl>();
            for(Igamification_challenge igamification_challenge :gamification_challenges){
                list.add((gamification_challengeImpl)igamification_challenge) ;
            }
            gamification_challengeFeignClient.createBatch(list) ;
        }
    }


    public void update(Igamification_challenge gamification_challenge){
        Igamification_challenge clientModel = gamification_challengeFeignClient.update(gamification_challenge.getId(),(gamification_challengeImpl)gamification_challenge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge.getClass(), false);
        copier.copy(clientModel, gamification_challenge, null);
    }


    public void get(Igamification_challenge gamification_challenge){
        Igamification_challenge clientModel = gamification_challengeFeignClient.get(gamification_challenge.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge.getClass(), false);
        copier.copy(clientModel, gamification_challenge, null);
    }


    public Page<Igamification_challenge> fetchDefault(SearchContext context){
        Page<gamification_challengeImpl> page = this.gamification_challengeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Igamification_challenge gamification_challenge){
        gamification_challengeFeignClient.remove(gamification_challenge.getId()) ;
    }


    public void updateBatch(List<Igamification_challenge> gamification_challenges){
        if(gamification_challenges!=null){
            List<gamification_challengeImpl> list = new ArrayList<gamification_challengeImpl>();
            for(Igamification_challenge igamification_challenge :gamification_challenges){
                list.add((gamification_challengeImpl)igamification_challenge) ;
            }
            gamification_challengeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Igamification_challenge> gamification_challenges){
        if(gamification_challenges!=null){
            List<gamification_challengeImpl> list = new ArrayList<gamification_challengeImpl>();
            for(Igamification_challenge igamification_challenge :gamification_challenges){
                list.add((gamification_challengeImpl)igamification_challenge) ;
            }
            gamification_challengeFeignClient.removeBatch(list) ;
        }
    }


    public Page<Igamification_challenge> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_challenge gamification_challenge){
        Igamification_challenge clientModel = gamification_challengeFeignClient.getDraft(gamification_challenge.getId(),(gamification_challengeImpl)gamification_challenge) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge.getClass(), false);
        copier.copy(clientModel, gamification_challenge, null);
    }



}


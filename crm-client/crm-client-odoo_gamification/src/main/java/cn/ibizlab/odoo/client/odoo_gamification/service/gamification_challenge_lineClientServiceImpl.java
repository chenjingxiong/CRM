package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_challenge_lineClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challenge_lineImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_challenge_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_challenge_lineClientServiceImpl implements Igamification_challenge_lineClientService {

    gamification_challenge_lineFeignClient gamification_challenge_lineFeignClient;

    @Autowired
    public gamification_challenge_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_challenge_lineFeignClient = nameBuilder.target(gamification_challenge_lineFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_challenge_lineFeignClient = nameBuilder.target(gamification_challenge_lineFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_challenge_line createModel() {
		return new gamification_challenge_lineImpl();
	}


    public void create(Igamification_challenge_line gamification_challenge_line){
        Igamification_challenge_line clientModel = gamification_challenge_lineFeignClient.create((gamification_challenge_lineImpl)gamification_challenge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge_line.getClass(), false);
        copier.copy(clientModel, gamification_challenge_line, null);
    }


    public void update(Igamification_challenge_line gamification_challenge_line){
        Igamification_challenge_line clientModel = gamification_challenge_lineFeignClient.update(gamification_challenge_line.getId(),(gamification_challenge_lineImpl)gamification_challenge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge_line.getClass(), false);
        copier.copy(clientModel, gamification_challenge_line, null);
    }


    public void updateBatch(List<Igamification_challenge_line> gamification_challenge_lines){
        if(gamification_challenge_lines!=null){
            List<gamification_challenge_lineImpl> list = new ArrayList<gamification_challenge_lineImpl>();
            for(Igamification_challenge_line igamification_challenge_line :gamification_challenge_lines){
                list.add((gamification_challenge_lineImpl)igamification_challenge_line) ;
            }
            gamification_challenge_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Igamification_challenge_line> gamification_challenge_lines){
        if(gamification_challenge_lines!=null){
            List<gamification_challenge_lineImpl> list = new ArrayList<gamification_challenge_lineImpl>();
            for(Igamification_challenge_line igamification_challenge_line :gamification_challenge_lines){
                list.add((gamification_challenge_lineImpl)igamification_challenge_line) ;
            }
            gamification_challenge_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Igamification_challenge_line> fetchDefault(SearchContext context){
        Page<gamification_challenge_lineImpl> page = this.gamification_challenge_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Igamification_challenge_line gamification_challenge_line){
        Igamification_challenge_line clientModel = gamification_challenge_lineFeignClient.get(gamification_challenge_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge_line.getClass(), false);
        copier.copy(clientModel, gamification_challenge_line, null);
    }


    public void createBatch(List<Igamification_challenge_line> gamification_challenge_lines){
        if(gamification_challenge_lines!=null){
            List<gamification_challenge_lineImpl> list = new ArrayList<gamification_challenge_lineImpl>();
            for(Igamification_challenge_line igamification_challenge_line :gamification_challenge_lines){
                list.add((gamification_challenge_lineImpl)igamification_challenge_line) ;
            }
            gamification_challenge_lineFeignClient.createBatch(list) ;
        }
    }


    public void remove(Igamification_challenge_line gamification_challenge_line){
        gamification_challenge_lineFeignClient.remove(gamification_challenge_line.getId()) ;
    }


    public Page<Igamification_challenge_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_challenge_line gamification_challenge_line){
        Igamification_challenge_line clientModel = gamification_challenge_lineFeignClient.getDraft(gamification_challenge_line.getId(),(gamification_challenge_lineImpl)gamification_challenge_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_challenge_line.getClass(), false);
        copier.copy(clientModel, gamification_challenge_line, null);
    }



}


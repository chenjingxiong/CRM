package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challengeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_challenge] 服务对象接口
 */
public interface gamification_challengeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenges")
    public gamification_challengeImpl create(@RequestBody gamification_challengeImpl gamification_challenge);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenges/createbatch")
    public gamification_challengeImpl createBatch(@RequestBody List<gamification_challengeImpl> gamification_challenges);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenges/{id}")
    public gamification_challengeImpl update(@PathVariable("id") Integer id,@RequestBody gamification_challengeImpl gamification_challenge);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenges/{id}")
    public gamification_challengeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenges/fetchdefault")
    public Page<gamification_challengeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenges/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenges/updatebatch")
    public gamification_challengeImpl updateBatch(@RequestBody List<gamification_challengeImpl> gamification_challenges);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenges/removebatch")
    public gamification_challengeImpl removeBatch(@RequestBody List<gamification_challengeImpl> gamification_challenges);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenges/select")
    public Page<gamification_challengeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenges/{id}/getdraft")
    public gamification_challengeImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_challengeImpl gamification_challenge);



}

package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_badge;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badgeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_badge] 服务对象接口
 */
public interface gamification_badgeFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badges/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badges/{id}")
    public gamification_badgeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badges/removebatch")
    public gamification_badgeImpl removeBatch(@RequestBody List<gamification_badgeImpl> gamification_badges);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badges/createbatch")
    public gamification_badgeImpl createBatch(@RequestBody List<gamification_badgeImpl> gamification_badges);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badges/updatebatch")
    public gamification_badgeImpl updateBatch(@RequestBody List<gamification_badgeImpl> gamification_badges);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badges/{id}")
    public gamification_badgeImpl update(@PathVariable("id") Integer id,@RequestBody gamification_badgeImpl gamification_badge);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badges/fetchdefault")
    public Page<gamification_badgeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badges")
    public gamification_badgeImpl create(@RequestBody gamification_badgeImpl gamification_badge);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badges/select")
    public Page<gamification_badgeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badges/{id}/getdraft")
    public gamification_badgeImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_badgeImpl gamification_badge);



}

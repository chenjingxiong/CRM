package cn.ibizlab.odoo.client.odoo_maintenance.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imaintenance_request;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_requestImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[maintenance_request] 服务对象接口
 */
public interface maintenance_requestFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_requests/updatebatch")
    public maintenance_requestImpl updateBatch(@RequestBody List<maintenance_requestImpl> maintenance_requests);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_requests")
    public maintenance_requestImpl create(@RequestBody maintenance_requestImpl maintenance_request);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_requests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_requests/createbatch")
    public maintenance_requestImpl createBatch(@RequestBody List<maintenance_requestImpl> maintenance_requests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_requests/{id}")
    public maintenance_requestImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_requests/fetchdefault")
    public Page<maintenance_requestImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_requests/{id}")
    public maintenance_requestImpl update(@PathVariable("id") Integer id,@RequestBody maintenance_requestImpl maintenance_request);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_requests/removebatch")
    public maintenance_requestImpl removeBatch(@RequestBody List<maintenance_requestImpl> maintenance_requests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_requests/select")
    public Page<maintenance_requestImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_requests/{id}/getdraft")
    public maintenance_requestImpl getDraft(@PathVariable("id") Integer id,@RequestBody maintenance_requestImpl maintenance_request);



}

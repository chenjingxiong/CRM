package cn.ibizlab.odoo.client.odoo_maintenance.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[maintenance_equipment] 对象
 */
public class maintenance_equipmentImpl implements Imaintenance_equipment,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 分配日期
     */
    public Timestamp assign_date;

    @JsonIgnore
    public boolean assign_dateDirtyFlag;
    
    /**
     * 设备类别
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 设备类别
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 成本
     */
    public Double cost;

    @JsonIgnore
    public boolean costDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 分配到部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 分配到部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 实际日期
     */
    public Timestamp effective_date;

    @JsonIgnore
    public boolean effective_dateDirtyFlag;
    
    /**
     * 分配到员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 分配到员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * 用于
     */
    public String equipment_assign_to;

    @JsonIgnore
    public boolean equipment_assign_toDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 地点
     */
    public String location;

    @JsonIgnore
    public boolean locationDirtyFlag;
    
    /**
     * 维修统计
     */
    public Integer maintenance_count;

    @JsonIgnore
    public boolean maintenance_countDirtyFlag;
    
    /**
     * 保养时长
     */
    public Double maintenance_duration;

    @JsonIgnore
    public boolean maintenance_durationDirtyFlag;
    
    /**
     * 保养
     */
    public String maintenance_ids;

    @JsonIgnore
    public boolean maintenance_idsDirtyFlag;
    
    /**
     * 当前维护
     */
    public Integer maintenance_open_count;

    @JsonIgnore
    public boolean maintenance_open_countDirtyFlag;
    
    /**
     * 保养团队
     */
    public Integer maintenance_team_id;

    @JsonIgnore
    public boolean maintenance_team_idDirtyFlag;
    
    /**
     * 保养团队
     */
    public String maintenance_team_id_text;

    @JsonIgnore
    public boolean maintenance_team_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 型号
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 设备名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 下次预防维护日期
     */
    public Timestamp next_action_date;

    @JsonIgnore
    public boolean next_action_dateDirtyFlag;
    
    /**
     * 笔记
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_user_id;

    @JsonIgnore
    public boolean owner_user_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_user_id_text;

    @JsonIgnore
    public boolean owner_user_id_textDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 供应商参考
     */
    public String partner_ref;

    @JsonIgnore
    public boolean partner_refDirtyFlag;
    
    /**
     * 预防维护间隔天数
     */
    public Integer period;

    @JsonIgnore
    public boolean periodDirtyFlag;
    
    /**
     * 报废日期
     */
    public Timestamp scrap_date;

    @JsonIgnore
    public boolean scrap_dateDirtyFlag;
    
    /**
     * 序列号
     */
    public String serial_no;

    @JsonIgnore
    public boolean serial_noDirtyFlag;
    
    /**
     * 技术员
     */
    public Integer technician_user_id;

    @JsonIgnore
    public boolean technician_user_idDirtyFlag;
    
    /**
     * 技术员
     */
    public String technician_user_id_text;

    @JsonIgnore
    public boolean technician_user_id_textDirtyFlag;
    
    /**
     * 保修截止日期
     */
    public Timestamp warranty_date;

    @JsonIgnore
    public boolean warranty_dateDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [分配日期]
     */
    @JsonProperty("assign_date")
    public Timestamp getAssign_date(){
        return this.assign_date ;
    }

    /**
     * 设置 [分配日期]
     */
    @JsonProperty("assign_date")
    public void setAssign_date(Timestamp  assign_date){
        this.assign_date = assign_date ;
        this.assign_dateDirtyFlag = true ;
    }

     /**
     * 获取 [分配日期]脏标记
     */
    @JsonIgnore
    public boolean getAssign_dateDirtyFlag(){
        return this.assign_dateDirtyFlag ;
    }   

    /**
     * 获取 [设备类别]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [设备类别]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [设备类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [设备类别]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [设备类别]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [设备类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [成本]
     */
    @JsonProperty("cost")
    public Double getCost(){
        return this.cost ;
    }

    /**
     * 设置 [成本]
     */
    @JsonProperty("cost")
    public void setCost(Double  cost){
        this.cost = cost ;
        this.costDirtyFlag = true ;
    }

     /**
     * 获取 [成本]脏标记
     */
    @JsonIgnore
    public boolean getCostDirtyFlag(){
        return this.costDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [分配到部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [分配到部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [分配到部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [分配到部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [分配到部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分配到部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [实际日期]
     */
    @JsonProperty("effective_date")
    public Timestamp getEffective_date(){
        return this.effective_date ;
    }

    /**
     * 设置 [实际日期]
     */
    @JsonProperty("effective_date")
    public void setEffective_date(Timestamp  effective_date){
        this.effective_date = effective_date ;
        this.effective_dateDirtyFlag = true ;
    }

     /**
     * 获取 [实际日期]脏标记
     */
    @JsonIgnore
    public boolean getEffective_dateDirtyFlag(){
        return this.effective_dateDirtyFlag ;
    }   

    /**
     * 获取 [分配到员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [分配到员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [分配到员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [分配到员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [分配到员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分配到员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [用于]
     */
    @JsonProperty("equipment_assign_to")
    public String getEquipment_assign_to(){
        return this.equipment_assign_to ;
    }

    /**
     * 设置 [用于]
     */
    @JsonProperty("equipment_assign_to")
    public void setEquipment_assign_to(String  equipment_assign_to){
        this.equipment_assign_to = equipment_assign_to ;
        this.equipment_assign_toDirtyFlag = true ;
    }

     /**
     * 获取 [用于]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_assign_toDirtyFlag(){
        return this.equipment_assign_toDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("location")
    public String getLocation(){
        return this.location ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return this.locationDirtyFlag ;
    }   

    /**
     * 获取 [维修统计]
     */
    @JsonProperty("maintenance_count")
    public Integer getMaintenance_count(){
        return this.maintenance_count ;
    }

    /**
     * 设置 [维修统计]
     */
    @JsonProperty("maintenance_count")
    public void setMaintenance_count(Integer  maintenance_count){
        this.maintenance_count = maintenance_count ;
        this.maintenance_countDirtyFlag = true ;
    }

     /**
     * 获取 [维修统计]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_countDirtyFlag(){
        return this.maintenance_countDirtyFlag ;
    }   

    /**
     * 获取 [保养时长]
     */
    @JsonProperty("maintenance_duration")
    public Double getMaintenance_duration(){
        return this.maintenance_duration ;
    }

    /**
     * 设置 [保养时长]
     */
    @JsonProperty("maintenance_duration")
    public void setMaintenance_duration(Double  maintenance_duration){
        this.maintenance_duration = maintenance_duration ;
        this.maintenance_durationDirtyFlag = true ;
    }

     /**
     * 获取 [保养时长]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_durationDirtyFlag(){
        return this.maintenance_durationDirtyFlag ;
    }   

    /**
     * 获取 [保养]
     */
    @JsonProperty("maintenance_ids")
    public String getMaintenance_ids(){
        return this.maintenance_ids ;
    }

    /**
     * 设置 [保养]
     */
    @JsonProperty("maintenance_ids")
    public void setMaintenance_ids(String  maintenance_ids){
        this.maintenance_ids = maintenance_ids ;
        this.maintenance_idsDirtyFlag = true ;
    }

     /**
     * 获取 [保养]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_idsDirtyFlag(){
        return this.maintenance_idsDirtyFlag ;
    }   

    /**
     * 获取 [当前维护]
     */
    @JsonProperty("maintenance_open_count")
    public Integer getMaintenance_open_count(){
        return this.maintenance_open_count ;
    }

    /**
     * 设置 [当前维护]
     */
    @JsonProperty("maintenance_open_count")
    public void setMaintenance_open_count(Integer  maintenance_open_count){
        this.maintenance_open_count = maintenance_open_count ;
        this.maintenance_open_countDirtyFlag = true ;
    }

     /**
     * 获取 [当前维护]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_open_countDirtyFlag(){
        return this.maintenance_open_countDirtyFlag ;
    }   

    /**
     * 获取 [保养团队]
     */
    @JsonProperty("maintenance_team_id")
    public Integer getMaintenance_team_id(){
        return this.maintenance_team_id ;
    }

    /**
     * 设置 [保养团队]
     */
    @JsonProperty("maintenance_team_id")
    public void setMaintenance_team_id(Integer  maintenance_team_id){
        this.maintenance_team_id = maintenance_team_id ;
        this.maintenance_team_idDirtyFlag = true ;
    }

     /**
     * 获取 [保养团队]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_idDirtyFlag(){
        return this.maintenance_team_idDirtyFlag ;
    }   

    /**
     * 获取 [保养团队]
     */
    @JsonProperty("maintenance_team_id_text")
    public String getMaintenance_team_id_text(){
        return this.maintenance_team_id_text ;
    }

    /**
     * 设置 [保养团队]
     */
    @JsonProperty("maintenance_team_id_text")
    public void setMaintenance_team_id_text(String  maintenance_team_id_text){
        this.maintenance_team_id_text = maintenance_team_id_text ;
        this.maintenance_team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [保养团队]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_id_textDirtyFlag(){
        return this.maintenance_team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [型号]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [型号]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [型号]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [设备名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [设备名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [设备名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [下次预防维护日期]
     */
    @JsonProperty("next_action_date")
    public Timestamp getNext_action_date(){
        return this.next_action_date ;
    }

    /**
     * 设置 [下次预防维护日期]
     */
    @JsonProperty("next_action_date")
    public void setNext_action_date(Timestamp  next_action_date){
        this.next_action_date = next_action_date ;
        this.next_action_dateDirtyFlag = true ;
    }

     /**
     * 获取 [下次预防维护日期]脏标记
     */
    @JsonIgnore
    public boolean getNext_action_dateDirtyFlag(){
        return this.next_action_dateDirtyFlag ;
    }   

    /**
     * 获取 [笔记]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [笔记]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [笔记]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_user_id")
    public Integer getOwner_user_id(){
        return this.owner_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_user_id")
    public void setOwner_user_id(Integer  owner_user_id){
        this.owner_user_id = owner_user_id ;
        this.owner_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_idDirtyFlag(){
        return this.owner_user_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_user_id_text")
    public String getOwner_user_id_text(){
        return this.owner_user_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_user_id_text")
    public void setOwner_user_id_text(String  owner_user_id_text){
        this.owner_user_id_text = owner_user_id_text ;
        this.owner_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_id_textDirtyFlag(){
        return this.owner_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商参考]
     */
    @JsonProperty("partner_ref")
    public String getPartner_ref(){
        return this.partner_ref ;
    }

    /**
     * 设置 [供应商参考]
     */
    @JsonProperty("partner_ref")
    public void setPartner_ref(String  partner_ref){
        this.partner_ref = partner_ref ;
        this.partner_refDirtyFlag = true ;
    }

     /**
     * 获取 [供应商参考]脏标记
     */
    @JsonIgnore
    public boolean getPartner_refDirtyFlag(){
        return this.partner_refDirtyFlag ;
    }   

    /**
     * 获取 [预防维护间隔天数]
     */
    @JsonProperty("period")
    public Integer getPeriod(){
        return this.period ;
    }

    /**
     * 设置 [预防维护间隔天数]
     */
    @JsonProperty("period")
    public void setPeriod(Integer  period){
        this.period = period ;
        this.periodDirtyFlag = true ;
    }

     /**
     * 获取 [预防维护间隔天数]脏标记
     */
    @JsonIgnore
    public boolean getPeriodDirtyFlag(){
        return this.periodDirtyFlag ;
    }   

    /**
     * 获取 [报废日期]
     */
    @JsonProperty("scrap_date")
    public Timestamp getScrap_date(){
        return this.scrap_date ;
    }

    /**
     * 设置 [报废日期]
     */
    @JsonProperty("scrap_date")
    public void setScrap_date(Timestamp  scrap_date){
        this.scrap_date = scrap_date ;
        this.scrap_dateDirtyFlag = true ;
    }

     /**
     * 获取 [报废日期]脏标记
     */
    @JsonIgnore
    public boolean getScrap_dateDirtyFlag(){
        return this.scrap_dateDirtyFlag ;
    }   

    /**
     * 获取 [序列号]
     */
    @JsonProperty("serial_no")
    public String getSerial_no(){
        return this.serial_no ;
    }

    /**
     * 设置 [序列号]
     */
    @JsonProperty("serial_no")
    public void setSerial_no(String  serial_no){
        this.serial_no = serial_no ;
        this.serial_noDirtyFlag = true ;
    }

     /**
     * 获取 [序列号]脏标记
     */
    @JsonIgnore
    public boolean getSerial_noDirtyFlag(){
        return this.serial_noDirtyFlag ;
    }   

    /**
     * 获取 [技术员]
     */
    @JsonProperty("technician_user_id")
    public Integer getTechnician_user_id(){
        return this.technician_user_id ;
    }

    /**
     * 设置 [技术员]
     */
    @JsonProperty("technician_user_id")
    public void setTechnician_user_id(Integer  technician_user_id){
        this.technician_user_id = technician_user_id ;
        this.technician_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [技术员]脏标记
     */
    @JsonIgnore
    public boolean getTechnician_user_idDirtyFlag(){
        return this.technician_user_idDirtyFlag ;
    }   

    /**
     * 获取 [技术员]
     */
    @JsonProperty("technician_user_id_text")
    public String getTechnician_user_id_text(){
        return this.technician_user_id_text ;
    }

    /**
     * 设置 [技术员]
     */
    @JsonProperty("technician_user_id_text")
    public void setTechnician_user_id_text(String  technician_user_id_text){
        this.technician_user_id_text = technician_user_id_text ;
        this.technician_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [技术员]脏标记
     */
    @JsonIgnore
    public boolean getTechnician_user_id_textDirtyFlag(){
        return this.technician_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [保修截止日期]
     */
    @JsonProperty("warranty_date")
    public Timestamp getWarranty_date(){
        return this.warranty_date ;
    }

    /**
     * 设置 [保修截止日期]
     */
    @JsonProperty("warranty_date")
    public void setWarranty_date(Timestamp  warranty_date){
        this.warranty_date = warranty_date ;
        this.warranty_dateDirtyFlag = true ;
    }

     /**
     * 获取 [保修截止日期]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_dateDirtyFlag(){
        return this.warranty_dateDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

package cn.ibizlab.odoo.client.r7rt_dyna.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.IDynaChart;
import cn.ibizlab.odoo.client.r7rt_dyna.model.DynaChartImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[DynaChart] 服务对象接口
 */
public interface DynaChartFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynacharts/select")
    public Page<DynaChartImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/r7rt_dyna/dynacharts/{dynachartid}")
    public DynaChartImpl get(@PathVariable("dynachartid") String dynachartid);


    @RequestMapping(method = RequestMethod.GET, value = "/r7rt_dyna/dynacharts/{dynachartid}/getdraft")
    public DynaChartImpl getDraft(@PathVariable("dynachartid") String dynachartid,@RequestBody DynaChartImpl dynachart);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynacharts/{dynachartid}/checkkey")
    public DynaChartImpl checkKey(@PathVariable("dynachartid") String dynachartid,@RequestBody DynaChartImpl dynachart);


    @RequestMapping(method = RequestMethod.PUT, value = "/r7rt_dyna/dynacharts/{dynachartid}")
    public DynaChartImpl update(@PathVariable("dynachartid") String dynachartid,@RequestBody DynaChartImpl dynachart);


    @RequestMapping(method = RequestMethod.DELETE, value = "/r7rt_dyna/dynacharts/{dynachartid}")
    public Boolean remove(@PathVariable("dynachartid") String dynachartid);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynacharts")
    public DynaChartImpl create(@RequestBody DynaChartImpl dynachart);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynacharts/{dynachartid}/save")
    public DynaChartImpl save(@PathVariable("dynachartid") String dynachartid,@RequestBody DynaChartImpl dynachart);


    @RequestMapping(method = RequestMethod.POST, value = "/r7rt_dyna/dynacharts/fetchdefault")
    public Page<DynaChartImpl> fetchDefault(SearchContext context);



}

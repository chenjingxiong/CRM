package cn.ibizlab.odoo.client.r7rt_dyna.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.IDynaDashboard;
import cn.ibizlab.odoo.client.r7rt_dyna.config.r7rt_dynaClientProperties;
import cn.ibizlab.odoo.core.client.service.IDynaDashboardClientService;
import cn.ibizlab.odoo.client.r7rt_dyna.model.DynaDashboardImpl;
import cn.ibizlab.odoo.client.r7rt_dyna.feign.DynaDashboardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[DynaDashboard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class DynaDashboardClientServiceImpl implements IDynaDashboardClientService {

    DynaDashboardFeignClient dynaDashboardFeignClient;

    @Autowired
    public DynaDashboardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,r7rt_dynaClientProperties r7rt_dynaClientProperties) {
        if (r7rt_dynaClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.dynaDashboardFeignClient = nameBuilder.target(DynaDashboardFeignClient.class,"http://"+r7rt_dynaClientProperties.getServiceId()+"/") ;
		}else if (r7rt_dynaClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.dynaDashboardFeignClient = nameBuilder.target(DynaDashboardFeignClient.class,r7rt_dynaClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public IDynaDashboard createModel() {
		return new DynaDashboardImpl();
	}


    public Page<IDynaDashboard> select(SearchContext context){
        return null ;
    }


    public void save(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.save(dynadashboard.getDynaDashboardId(),(DynaDashboardImpl)dynadashboard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public void update(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.update(dynadashboard.getDynaDashboardId(),(DynaDashboardImpl)dynadashboard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public void get(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.get(dynadashboard.getDynaDashboardId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public void remove(IDynaDashboard dynadashboard){
        dynaDashboardFeignClient.remove(dynadashboard.getDynaDashboardId()) ;
    }


    public void checkKey(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.checkKey(dynadashboard.getDynaDashboardId(),(DynaDashboardImpl)dynadashboard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public void getDraft(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.getDraft(dynadashboard.getDynaDashboardId(),(DynaDashboardImpl)dynadashboard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public void create(IDynaDashboard dynadashboard){
        IDynaDashboard clientModel = dynaDashboardFeignClient.create((DynaDashboardImpl)dynadashboard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynadashboard.getClass(), false);
        copier.copy(clientModel, dynadashboard, null);
    }


    public Page<IDynaDashboard> fetchDefault(SearchContext context){
        Page<DynaDashboardImpl> page = this.dynaDashboardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }



}


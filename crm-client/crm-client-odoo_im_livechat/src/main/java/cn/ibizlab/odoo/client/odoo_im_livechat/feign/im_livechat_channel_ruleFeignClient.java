package cn.ibizlab.odoo.client.odoo_im_livechat.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel_rule;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_channel_ruleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
public interface im_livechat_channel_ruleFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channel_rules/createbatch")
    public im_livechat_channel_ruleImpl createBatch(@RequestBody List<im_livechat_channel_ruleImpl> im_livechat_channel_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channel_rules/fetchdefault")
    public Page<im_livechat_channel_ruleImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channel_rules/removebatch")
    public im_livechat_channel_ruleImpl removeBatch(@RequestBody List<im_livechat_channel_ruleImpl> im_livechat_channel_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channel_rules/{id}")
    public im_livechat_channel_ruleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channel_rules/{id}")
    public im_livechat_channel_ruleImpl update(@PathVariable("id") Integer id,@RequestBody im_livechat_channel_ruleImpl im_livechat_channel_rule);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channel_rules")
    public im_livechat_channel_ruleImpl create(@RequestBody im_livechat_channel_ruleImpl im_livechat_channel_rule);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channel_rules/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channel_rules/updatebatch")
    public im_livechat_channel_ruleImpl updateBatch(@RequestBody List<im_livechat_channel_ruleImpl> im_livechat_channel_rules);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channel_rules/select")
    public Page<im_livechat_channel_ruleImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channel_rules/{id}/getdraft")
    public im_livechat_channel_ruleImpl getDraft(@PathVariable("id") Integer id,@RequestBody im_livechat_channel_ruleImpl im_livechat_channel_rule);



}

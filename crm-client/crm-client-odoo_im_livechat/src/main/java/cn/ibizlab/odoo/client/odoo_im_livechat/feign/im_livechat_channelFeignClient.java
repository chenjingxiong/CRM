package cn.ibizlab.odoo.client.odoo_im_livechat.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_channelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[im_livechat_channel] 服务对象接口
 */
public interface im_livechat_channelFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channels/createbatch")
    public im_livechat_channelImpl createBatch(@RequestBody List<im_livechat_channelImpl> im_livechat_channels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channels/fetchdefault")
    public Page<im_livechat_channelImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channels")
    public im_livechat_channelImpl create(@RequestBody im_livechat_channelImpl im_livechat_channel);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channels/removebatch")
    public im_livechat_channelImpl removeBatch(@RequestBody List<im_livechat_channelImpl> im_livechat_channels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channels/{id}")
    public im_livechat_channelImpl update(@PathVariable("id") Integer id,@RequestBody im_livechat_channelImpl im_livechat_channel);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channels/updatebatch")
    public im_livechat_channelImpl updateBatch(@RequestBody List<im_livechat_channelImpl> im_livechat_channels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channels/{id}")
    public im_livechat_channelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channels/select")
    public Page<im_livechat_channelImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channels/{id}/getdraft")
    public im_livechat_channelImpl getDraft(@PathVariable("id") Integer id,@RequestBody im_livechat_channelImpl im_livechat_channel);



}

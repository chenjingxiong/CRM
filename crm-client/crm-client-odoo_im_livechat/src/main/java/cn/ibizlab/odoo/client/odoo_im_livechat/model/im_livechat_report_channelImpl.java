package cn.ibizlab.odoo.client.odoo_im_livechat.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_channel;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[im_livechat_report_channel] 对象
 */
public class im_livechat_report_channelImpl implements Iim_livechat_report_channel,Serializable{

    /**
     * 对话
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 对话
     */
    public String channel_id_text;

    @JsonIgnore
    public boolean channel_id_textDirtyFlag;
    
    /**
     * 渠道名称
     */
    public String channel_name;

    @JsonIgnore
    public boolean channel_nameDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 平均时间
     */
    public Double duration;

    @JsonIgnore
    public boolean durationDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 渠道
     */
    public Integer livechat_channel_id;

    @JsonIgnore
    public boolean livechat_channel_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String livechat_channel_id_text;

    @JsonIgnore
    public boolean livechat_channel_id_textDirtyFlag;
    
    /**
     * 每个消息
     */
    public Integer nbr_message;

    @JsonIgnore
    public boolean nbr_messageDirtyFlag;
    
    /**
     * 讲解人
     */
    public Integer nbr_speaker;

    @JsonIgnore
    public boolean nbr_speakerDirtyFlag;
    
    /**
     * 运算符
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 运算符
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 会话的开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 会话开始的小时数
     */
    public String start_date_hour;

    @JsonIgnore
    public boolean start_date_hourDirtyFlag;
    
    /**
     * 代号
     */
    public String technical_name;

    @JsonIgnore
    public boolean technical_nameDirtyFlag;
    
    /**
     * UUID
     */
    public String uuid;

    @JsonIgnore
    public boolean uuidDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [对话]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [对话]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [对话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [对话]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return this.channel_id_text ;
    }

    /**
     * 设置 [对话]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [对话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return this.channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [渠道名称]
     */
    @JsonProperty("channel_name")
    public String getChannel_name(){
        return this.channel_name ;
    }

    /**
     * 设置 [渠道名称]
     */
    @JsonProperty("channel_name")
    public void setChannel_name(String  channel_name){
        this.channel_name = channel_name ;
        this.channel_nameDirtyFlag = true ;
    }

     /**
     * 获取 [渠道名称]脏标记
     */
    @JsonIgnore
    public boolean getChannel_nameDirtyFlag(){
        return this.channel_nameDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [平均时间]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [平均时间]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

     /**
     * 获取 [平均时间]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public Integer getLivechat_channel_id(){
        return this.livechat_channel_id ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public void setLivechat_channel_id(Integer  livechat_channel_id){
        this.livechat_channel_id = livechat_channel_id ;
        this.livechat_channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_idDirtyFlag(){
        return this.livechat_channel_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public String getLivechat_channel_id_text(){
        return this.livechat_channel_id_text ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public void setLivechat_channel_id_text(String  livechat_channel_id_text){
        this.livechat_channel_id_text = livechat_channel_id_text ;
        this.livechat_channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_id_textDirtyFlag(){
        return this.livechat_channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [每个消息]
     */
    @JsonProperty("nbr_message")
    public Integer getNbr_message(){
        return this.nbr_message ;
    }

    /**
     * 设置 [每个消息]
     */
    @JsonProperty("nbr_message")
    public void setNbr_message(Integer  nbr_message){
        this.nbr_message = nbr_message ;
        this.nbr_messageDirtyFlag = true ;
    }

     /**
     * 获取 [每个消息]脏标记
     */
    @JsonIgnore
    public boolean getNbr_messageDirtyFlag(){
        return this.nbr_messageDirtyFlag ;
    }   

    /**
     * 获取 [讲解人]
     */
    @JsonProperty("nbr_speaker")
    public Integer getNbr_speaker(){
        return this.nbr_speaker ;
    }

    /**
     * 设置 [讲解人]
     */
    @JsonProperty("nbr_speaker")
    public void setNbr_speaker(Integer  nbr_speaker){
        this.nbr_speaker = nbr_speaker ;
        this.nbr_speakerDirtyFlag = true ;
    }

     /**
     * 获取 [讲解人]脏标记
     */
    @JsonIgnore
    public boolean getNbr_speakerDirtyFlag(){
        return this.nbr_speakerDirtyFlag ;
    }   

    /**
     * 获取 [运算符]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [运算符]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [运算符]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [运算符]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [运算符]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [运算符]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [会话的开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [会话的开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [会话的开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [会话开始的小时数]
     */
    @JsonProperty("start_date_hour")
    public String getStart_date_hour(){
        return this.start_date_hour ;
    }

    /**
     * 设置 [会话开始的小时数]
     */
    @JsonProperty("start_date_hour")
    public void setStart_date_hour(String  start_date_hour){
        this.start_date_hour = start_date_hour ;
        this.start_date_hourDirtyFlag = true ;
    }

     /**
     * 获取 [会话开始的小时数]脏标记
     */
    @JsonIgnore
    public boolean getStart_date_hourDirtyFlag(){
        return this.start_date_hourDirtyFlag ;
    }   

    /**
     * 获取 [代号]
     */
    @JsonProperty("technical_name")
    public String getTechnical_name(){
        return this.technical_name ;
    }

    /**
     * 设置 [代号]
     */
    @JsonProperty("technical_name")
    public void setTechnical_name(String  technical_name){
        this.technical_name = technical_name ;
        this.technical_nameDirtyFlag = true ;
    }

     /**
     * 获取 [代号]脏标记
     */
    @JsonIgnore
    public boolean getTechnical_nameDirtyFlag(){
        return this.technical_nameDirtyFlag ;
    }   

    /**
     * 获取 [UUID]
     */
    @JsonProperty("uuid")
    public String getUuid(){
        return this.uuid ;
    }

    /**
     * 设置 [UUID]
     */
    @JsonProperty("uuid")
    public void setUuid(String  uuid){
        this.uuid = uuid ;
        this.uuidDirtyFlag = true ;
    }

     /**
     * 获取 [UUID]脏标记
     */
    @JsonIgnore
    public boolean getUuidDirtyFlag(){
        return this.uuidDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

package cn.ibizlab.odoo.client.odoo_im_livechat.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_channel;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_report_channelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[im_livechat_report_channel] 服务对象接口
 */
public interface im_livechat_report_channelFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_channels/createbatch")
    public im_livechat_report_channelImpl createBatch(@RequestBody List<im_livechat_report_channelImpl> im_livechat_report_channels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_channels/{id}")
    public im_livechat_report_channelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_channels")
    public im_livechat_report_channelImpl create(@RequestBody im_livechat_report_channelImpl im_livechat_report_channel);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_channels/{id}")
    public im_livechat_report_channelImpl update(@PathVariable("id") Integer id,@RequestBody im_livechat_report_channelImpl im_livechat_report_channel);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_channels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_channels/updatebatch")
    public im_livechat_report_channelImpl updateBatch(@RequestBody List<im_livechat_report_channelImpl> im_livechat_report_channels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_channels/fetchdefault")
    public Page<im_livechat_report_channelImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_channels/removebatch")
    public im_livechat_report_channelImpl removeBatch(@RequestBody List<im_livechat_report_channelImpl> im_livechat_report_channels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_channels/select")
    public Page<im_livechat_report_channelImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_channels/{id}/getdraft")
    public im_livechat_report_channelImpl getDraft(@PathVariable("id") Integer id,@RequestBody im_livechat_report_channelImpl im_livechat_report_channel);



}

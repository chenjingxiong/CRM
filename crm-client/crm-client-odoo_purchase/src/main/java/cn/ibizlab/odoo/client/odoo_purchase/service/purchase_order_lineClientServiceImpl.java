package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.client.odoo_purchase.config.odoo_purchaseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipurchase_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_purchase.feign.purchase_order_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[purchase_order_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class purchase_order_lineClientServiceImpl implements Ipurchase_order_lineClientService {

    purchase_order_lineFeignClient purchase_order_lineFeignClient;

    @Autowired
    public purchase_order_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_purchaseClientProperties odoo_purchaseClientProperties) {
        if (odoo_purchaseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_order_lineFeignClient = nameBuilder.target(purchase_order_lineFeignClient.class,"http://"+odoo_purchaseClientProperties.getServiceId()+"/") ;
		}else if (odoo_purchaseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_order_lineFeignClient = nameBuilder.target(purchase_order_lineFeignClient.class,odoo_purchaseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipurchase_order_line createModel() {
		return new purchase_order_lineImpl();
	}


    public void createBatch(List<Ipurchase_order_line> purchase_order_lines){
        if(purchase_order_lines!=null){
            List<purchase_order_lineImpl> list = new ArrayList<purchase_order_lineImpl>();
            for(Ipurchase_order_line ipurchase_order_line :purchase_order_lines){
                list.add((purchase_order_lineImpl)ipurchase_order_line) ;
            }
            purchase_order_lineFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ipurchase_order_line> purchase_order_lines){
        if(purchase_order_lines!=null){
            List<purchase_order_lineImpl> list = new ArrayList<purchase_order_lineImpl>();
            for(Ipurchase_order_line ipurchase_order_line :purchase_order_lines){
                list.add((purchase_order_lineImpl)ipurchase_order_line) ;
            }
            purchase_order_lineFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ipurchase_order_line purchase_order_line){
        purchase_order_lineFeignClient.remove(purchase_order_line.getId()) ;
    }


    public Page<Ipurchase_order_line> fetchDefault(SearchContext context){
        Page<purchase_order_lineImpl> page = this.purchase_order_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ipurchase_order_line> purchase_order_lines){
        if(purchase_order_lines!=null){
            List<purchase_order_lineImpl> list = new ArrayList<purchase_order_lineImpl>();
            for(Ipurchase_order_line ipurchase_order_line :purchase_order_lines){
                list.add((purchase_order_lineImpl)ipurchase_order_line) ;
            }
            purchase_order_lineFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ipurchase_order_line purchase_order_line){
        Ipurchase_order_line clientModel = purchase_order_lineFeignClient.get(purchase_order_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order_line.getClass(), false);
        copier.copy(clientModel, purchase_order_line, null);
    }


    public void create(Ipurchase_order_line purchase_order_line){
        Ipurchase_order_line clientModel = purchase_order_lineFeignClient.create((purchase_order_lineImpl)purchase_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order_line.getClass(), false);
        copier.copy(clientModel, purchase_order_line, null);
    }


    public void update(Ipurchase_order_line purchase_order_line){
        Ipurchase_order_line clientModel = purchase_order_lineFeignClient.update(purchase_order_line.getId(),(purchase_order_lineImpl)purchase_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order_line.getClass(), false);
        copier.copy(clientModel, purchase_order_line, null);
    }


    public Page<Ipurchase_order_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipurchase_order_line purchase_order_line){
        Ipurchase_order_line clientModel = purchase_order_lineFeignClient.getDraft(purchase_order_line.getId(),(purchase_order_lineImpl)purchase_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_order_line.getClass(), false);
        copier.copy(clientModel, purchase_order_line, null);
    }



}


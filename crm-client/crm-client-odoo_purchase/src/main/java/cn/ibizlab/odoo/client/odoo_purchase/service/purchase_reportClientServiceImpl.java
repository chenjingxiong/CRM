package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_report;
import cn.ibizlab.odoo.client.odoo_purchase.config.odoo_purchaseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipurchase_reportClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_reportImpl;
import cn.ibizlab.odoo.client.odoo_purchase.feign.purchase_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[purchase_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class purchase_reportClientServiceImpl implements Ipurchase_reportClientService {

    purchase_reportFeignClient purchase_reportFeignClient;

    @Autowired
    public purchase_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_purchaseClientProperties odoo_purchaseClientProperties) {
        if (odoo_purchaseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_reportFeignClient = nameBuilder.target(purchase_reportFeignClient.class,"http://"+odoo_purchaseClientProperties.getServiceId()+"/") ;
		}else if (odoo_purchaseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_reportFeignClient = nameBuilder.target(purchase_reportFeignClient.class,odoo_purchaseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipurchase_report createModel() {
		return new purchase_reportImpl();
	}


    public void removeBatch(List<Ipurchase_report> purchase_reports){
        if(purchase_reports!=null){
            List<purchase_reportImpl> list = new ArrayList<purchase_reportImpl>();
            for(Ipurchase_report ipurchase_report :purchase_reports){
                list.add((purchase_reportImpl)ipurchase_report) ;
            }
            purchase_reportFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ipurchase_report purchase_report){
        Ipurchase_report clientModel = purchase_reportFeignClient.update(purchase_report.getId(),(purchase_reportImpl)purchase_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_report.getClass(), false);
        copier.copy(clientModel, purchase_report, null);
    }


    public void updateBatch(List<Ipurchase_report> purchase_reports){
        if(purchase_reports!=null){
            List<purchase_reportImpl> list = new ArrayList<purchase_reportImpl>();
            for(Ipurchase_report ipurchase_report :purchase_reports){
                list.add((purchase_reportImpl)ipurchase_report) ;
            }
            purchase_reportFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ipurchase_report purchase_report){
        Ipurchase_report clientModel = purchase_reportFeignClient.get(purchase_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_report.getClass(), false);
        copier.copy(clientModel, purchase_report, null);
    }


    public void create(Ipurchase_report purchase_report){
        Ipurchase_report clientModel = purchase_reportFeignClient.create((purchase_reportImpl)purchase_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_report.getClass(), false);
        copier.copy(clientModel, purchase_report, null);
    }


    public Page<Ipurchase_report> fetchDefault(SearchContext context){
        Page<purchase_reportImpl> page = this.purchase_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ipurchase_report> purchase_reports){
        if(purchase_reports!=null){
            List<purchase_reportImpl> list = new ArrayList<purchase_reportImpl>();
            for(Ipurchase_report ipurchase_report :purchase_reports){
                list.add((purchase_reportImpl)ipurchase_report) ;
            }
            purchase_reportFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ipurchase_report purchase_report){
        purchase_reportFeignClient.remove(purchase_report.getId()) ;
    }


    public Page<Ipurchase_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipurchase_report purchase_report){
        Ipurchase_report clientModel = purchase_reportFeignClient.getDraft(purchase_report.getId(),(purchase_reportImpl)purchase_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_report.getClass(), false);
        copier.copy(clientModel, purchase_report, null);
    }



}


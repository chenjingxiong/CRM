package cn.ibizlab.odoo.client.odoo_purchase.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipurchase_report;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[purchase_report] 对象
 */
public class purchase_reportImpl implements Ipurchase_report,Serializable{

    /**
     * 分析账户
     */
    public Integer account_analytic_id;

    @JsonIgnore
    public boolean account_analytic_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String account_analytic_id_text;

    @JsonIgnore
    public boolean account_analytic_id_textDirtyFlag;
    
    /**
     * 产品种类
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 产品种类
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 商业实体
     */
    public Integer commercial_partner_id;

    @JsonIgnore
    public boolean commercial_partner_idDirtyFlag;
    
    /**
     * 商业实体
     */
    public String commercial_partner_id_text;

    @JsonIgnore
    public boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 业务伙伴国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 业务伙伴国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 批准日期
     */
    public Timestamp date_approve;

    @JsonIgnore
    public boolean date_approveDirtyFlag;
    
    /**
     * 单据日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_order;

    @JsonIgnore
    public boolean date_orderDirtyFlag;
    
    /**
     * 验证天数
     */
    public Double delay;

    @JsonIgnore
    public boolean delayDirtyFlag;
    
    /**
     * 交货天数
     */
    public Double delay_pass;

    @JsonIgnore
    public boolean delay_passDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer fiscal_position_id;

    @JsonIgnore
    public boolean fiscal_position_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String fiscal_position_id_text;

    @JsonIgnore
    public boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * # 明细行
     */
    public Integer nbr_lines;

    @JsonIgnore
    public boolean nbr_linesDirtyFlag;
    
    /**
     * 采购 - 标准单价
     */
    public Double negociation;

    @JsonIgnore
    public boolean negociationDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 平均价格
     */
    public Double price_average;

    @JsonIgnore
    public boolean price_averageDirtyFlag;
    
    /**
     * 产品价值
     */
    public Double price_standard;

    @JsonIgnore
    public boolean price_standardDirtyFlag;
    
    /**
     * 总价
     */
    public Double price_total;

    @JsonIgnore
    public boolean price_totalDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 参考计量单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 参考计量单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 订单状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 数量
     */
    public Double unit_quantity;

    @JsonIgnore
    public boolean unit_quantityDirtyFlag;
    
    /**
     * 采购员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 采购员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 体积
     */
    public Double volume;

    @JsonIgnore
    public boolean volumeDirtyFlag;
    
    /**
     * 毛重
     */
    public Double weight;

    @JsonIgnore
    public boolean weightDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return this.account_analytic_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return this.account_analytic_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return this.account_analytic_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return this.account_analytic_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [业务伙伴国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [业务伙伴国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批准日期]
     */
    @JsonProperty("date_approve")
    public Timestamp getDate_approve(){
        return this.date_approve ;
    }

    /**
     * 设置 [批准日期]
     */
    @JsonProperty("date_approve")
    public void setDate_approve(Timestamp  date_approve){
        this.date_approve = date_approve ;
        this.date_approveDirtyFlag = true ;
    }

     /**
     * 获取 [批准日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_approveDirtyFlag(){
        return this.date_approveDirtyFlag ;
    }   

    /**
     * 获取 [单据日期]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return this.date_order ;
    }

    /**
     * 设置 [单据日期]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

     /**
     * 获取 [单据日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return this.date_orderDirtyFlag ;
    }   

    /**
     * 获取 [验证天数]
     */
    @JsonProperty("delay")
    public Double getDelay(){
        return this.delay ;
    }

    /**
     * 设置 [验证天数]
     */
    @JsonProperty("delay")
    public void setDelay(Double  delay){
        this.delay = delay ;
        this.delayDirtyFlag = true ;
    }

     /**
     * 获取 [验证天数]脏标记
     */
    @JsonIgnore
    public boolean getDelayDirtyFlag(){
        return this.delayDirtyFlag ;
    }   

    /**
     * 获取 [交货天数]
     */
    @JsonProperty("delay_pass")
    public Double getDelay_pass(){
        return this.delay_pass ;
    }

    /**
     * 设置 [交货天数]
     */
    @JsonProperty("delay_pass")
    public void setDelay_pass(Double  delay_pass){
        this.delay_pass = delay_pass ;
        this.delay_passDirtyFlag = true ;
    }

     /**
     * 获取 [交货天数]脏标记
     */
    @JsonIgnore
    public boolean getDelay_passDirtyFlag(){
        return this.delay_passDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [# 明细行]
     */
    @JsonProperty("nbr_lines")
    public Integer getNbr_lines(){
        return this.nbr_lines ;
    }

    /**
     * 设置 [# 明细行]
     */
    @JsonProperty("nbr_lines")
    public void setNbr_lines(Integer  nbr_lines){
        this.nbr_lines = nbr_lines ;
        this.nbr_linesDirtyFlag = true ;
    }

     /**
     * 获取 [# 明细行]脏标记
     */
    @JsonIgnore
    public boolean getNbr_linesDirtyFlag(){
        return this.nbr_linesDirtyFlag ;
    }   

    /**
     * 获取 [采购 - 标准单价]
     */
    @JsonProperty("negociation")
    public Double getNegociation(){
        return this.negociation ;
    }

    /**
     * 设置 [采购 - 标准单价]
     */
    @JsonProperty("negociation")
    public void setNegociation(Double  negociation){
        this.negociation = negociation ;
        this.negociationDirtyFlag = true ;
    }

     /**
     * 获取 [采购 - 标准单价]脏标记
     */
    @JsonIgnore
    public boolean getNegociationDirtyFlag(){
        return this.negociationDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [平均价格]
     */
    @JsonProperty("price_average")
    public Double getPrice_average(){
        return this.price_average ;
    }

    /**
     * 设置 [平均价格]
     */
    @JsonProperty("price_average")
    public void setPrice_average(Double  price_average){
        this.price_average = price_average ;
        this.price_averageDirtyFlag = true ;
    }

     /**
     * 获取 [平均价格]脏标记
     */
    @JsonIgnore
    public boolean getPrice_averageDirtyFlag(){
        return this.price_averageDirtyFlag ;
    }   

    /**
     * 获取 [产品价值]
     */
    @JsonProperty("price_standard")
    public Double getPrice_standard(){
        return this.price_standard ;
    }

    /**
     * 设置 [产品价值]
     */
    @JsonProperty("price_standard")
    public void setPrice_standard(Double  price_standard){
        this.price_standard = price_standard ;
        this.price_standardDirtyFlag = true ;
    }

     /**
     * 获取 [产品价值]脏标记
     */
    @JsonIgnore
    public boolean getPrice_standardDirtyFlag(){
        return this.price_standardDirtyFlag ;
    }   

    /**
     * 获取 [总价]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [总价]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [参考计量单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [参考计量单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [参考计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [参考计量单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [参考计量单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [参考计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [订单状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [订单状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [订单状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("unit_quantity")
    public Double getUnit_quantity(){
        return this.unit_quantity ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("unit_quantity")
    public void setUnit_quantity(Double  unit_quantity){
        this.unit_quantity = unit_quantity ;
        this.unit_quantityDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getUnit_quantityDirtyFlag(){
        return this.unit_quantityDirtyFlag ;
    }   

    /**
     * 获取 [采购员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [采购员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [采购员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [采购员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [采购员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [体积]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return this.volume ;
    }

    /**
     * 设置 [体积]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

     /**
     * 获取 [体积]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return this.volumeDirtyFlag ;
    }   

    /**
     * 获取 [毛重]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return this.weight ;
    }

    /**
     * 设置 [毛重]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

     /**
     * 获取 [毛重]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return this.weightDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

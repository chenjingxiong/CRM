package cn.ibizlab.odoo.client.odoo_purchase.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipurchase_report;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[purchase_report] 服务对象接口
 */
public interface purchase_reportFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_reports/removebatch")
    public purchase_reportImpl removeBatch(@RequestBody List<purchase_reportImpl> purchase_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_reports/{id}")
    public purchase_reportImpl update(@PathVariable("id") Integer id,@RequestBody purchase_reportImpl purchase_report);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_reports/updatebatch")
    public purchase_reportImpl updateBatch(@RequestBody List<purchase_reportImpl> purchase_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_reports/{id}")
    public purchase_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_reports")
    public purchase_reportImpl create(@RequestBody purchase_reportImpl purchase_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_reports/fetchdefault")
    public Page<purchase_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_reports/createbatch")
    public purchase_reportImpl createBatch(@RequestBody List<purchase_reportImpl> purchase_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_reports/select")
    public Page<purchase_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_reports/{id}/getdraft")
    public purchase_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody purchase_reportImpl purchase_report);



}

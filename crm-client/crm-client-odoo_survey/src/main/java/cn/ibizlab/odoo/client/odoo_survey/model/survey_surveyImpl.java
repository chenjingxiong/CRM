package cn.ibizlab.odoo.client.odoo_survey.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isurvey_survey;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[survey_survey] 对象
 */
public class survey_surveyImpl implements Isurvey_survey,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 需要登录
     */
    public String auth_required;

    @JsonIgnore
    public boolean auth_requiredDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 已经设计了
     */
    public String designed;

    @JsonIgnore
    public boolean designedDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 邮件模板
     */
    public Integer email_template_id;

    @JsonIgnore
    public boolean email_template_idDirtyFlag;
    
    /**
     * 邮件模板
     */
    public String email_template_id_text;

    @JsonIgnore
    public boolean email_template_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 已关闭
     */
    public String is_closed;

    @JsonIgnore
    public boolean is_closedDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 页面
     */
    public String page_ids;

    @JsonIgnore
    public boolean page_idsDirtyFlag;
    
    /**
     * 打印链接
     */
    public String print_url;

    @JsonIgnore
    public boolean print_urlDirtyFlag;
    
    /**
     * 公开链接
     */
    public String public_url;

    @JsonIgnore
    public boolean public_urlDirtyFlag;
    
    /**
     * 公开链接（HTML版）
     */
    public String public_url_html;

    @JsonIgnore
    public boolean public_url_htmlDirtyFlag;
    
    /**
     * 测验模式
     */
    public String quizz_mode;

    @JsonIgnore
    public boolean quizz_modeDirtyFlag;
    
    /**
     * 结果链接
     */
    public String result_url;

    @JsonIgnore
    public boolean result_urlDirtyFlag;
    
    /**
     * 阶段
     */
    public Integer stage_id;

    @JsonIgnore
    public boolean stage_idDirtyFlag;
    
    /**
     * 阶段
     */
    public String stage_id_text;

    @JsonIgnore
    public boolean stage_id_textDirtyFlag;
    
    /**
     * 感谢留言
     */
    public String thank_you_message;

    @JsonIgnore
    public boolean thank_you_messageDirtyFlag;
    
    /**
     * 称谓
     */
    public String title;

    @JsonIgnore
    public boolean titleDirtyFlag;
    
    /**
     * 已完成的调查数量
     */
    public Integer tot_comp_survey;

    @JsonIgnore
    public boolean tot_comp_surveyDirtyFlag;
    
    /**
     * 已发送调查者数量
     */
    public Integer tot_sent_survey;

    @JsonIgnore
    public boolean tot_sent_surveyDirtyFlag;
    
    /**
     * 已开始的调查数量
     */
    public Integer tot_start_survey;

    @JsonIgnore
    public boolean tot_start_surveyDirtyFlag;
    
    /**
     * 用户可返回
     */
    public String users_can_go_back;

    @JsonIgnore
    public boolean users_can_go_backDirtyFlag;
    
    /**
     * 用户回应
     */
    public String user_input_ids;

    @JsonIgnore
    public boolean user_input_idsDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [需要登录]
     */
    @JsonProperty("auth_required")
    public String getAuth_required(){
        return this.auth_required ;
    }

    /**
     * 设置 [需要登录]
     */
    @JsonProperty("auth_required")
    public void setAuth_required(String  auth_required){
        this.auth_required = auth_required ;
        this.auth_requiredDirtyFlag = true ;
    }

     /**
     * 获取 [需要登录]脏标记
     */
    @JsonIgnore
    public boolean getAuth_requiredDirtyFlag(){
        return this.auth_requiredDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [已经设计了]
     */
    @JsonProperty("designed")
    public String getDesigned(){
        return this.designed ;
    }

    /**
     * 设置 [已经设计了]
     */
    @JsonProperty("designed")
    public void setDesigned(String  designed){
        this.designed = designed ;
        this.designedDirtyFlag = true ;
    }

     /**
     * 获取 [已经设计了]脏标记
     */
    @JsonIgnore
    public boolean getDesignedDirtyFlag(){
        return this.designedDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("email_template_id")
    public Integer getEmail_template_id(){
        return this.email_template_id ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("email_template_id")
    public void setEmail_template_id(Integer  email_template_id){
        this.email_template_id = email_template_id ;
        this.email_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getEmail_template_idDirtyFlag(){
        return this.email_template_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件模板]
     */
    @JsonProperty("email_template_id_text")
    public String getEmail_template_id_text(){
        return this.email_template_id_text ;
    }

    /**
     * 设置 [邮件模板]
     */
    @JsonProperty("email_template_id_text")
    public void setEmail_template_id_text(String  email_template_id_text){
        this.email_template_id_text = email_template_id_text ;
        this.email_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getEmail_template_id_textDirtyFlag(){
        return this.email_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [已关闭]
     */
    @JsonProperty("is_closed")
    public String getIs_closed(){
        return this.is_closed ;
    }

    /**
     * 设置 [已关闭]
     */
    @JsonProperty("is_closed")
    public void setIs_closed(String  is_closed){
        this.is_closed = is_closed ;
        this.is_closedDirtyFlag = true ;
    }

     /**
     * 获取 [已关闭]脏标记
     */
    @JsonIgnore
    public boolean getIs_closedDirtyFlag(){
        return this.is_closedDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [页面]
     */
    @JsonProperty("page_ids")
    public String getPage_ids(){
        return this.page_ids ;
    }

    /**
     * 设置 [页面]
     */
    @JsonProperty("page_ids")
    public void setPage_ids(String  page_ids){
        this.page_ids = page_ids ;
        this.page_idsDirtyFlag = true ;
    }

     /**
     * 获取 [页面]脏标记
     */
    @JsonIgnore
    public boolean getPage_idsDirtyFlag(){
        return this.page_idsDirtyFlag ;
    }   

    /**
     * 获取 [打印链接]
     */
    @JsonProperty("print_url")
    public String getPrint_url(){
        return this.print_url ;
    }

    /**
     * 设置 [打印链接]
     */
    @JsonProperty("print_url")
    public void setPrint_url(String  print_url){
        this.print_url = print_url ;
        this.print_urlDirtyFlag = true ;
    }

     /**
     * 获取 [打印链接]脏标记
     */
    @JsonIgnore
    public boolean getPrint_urlDirtyFlag(){
        return this.print_urlDirtyFlag ;
    }   

    /**
     * 获取 [公开链接]
     */
    @JsonProperty("public_url")
    public String getPublic_url(){
        return this.public_url ;
    }

    /**
     * 设置 [公开链接]
     */
    @JsonProperty("public_url")
    public void setPublic_url(String  public_url){
        this.public_url = public_url ;
        this.public_urlDirtyFlag = true ;
    }

     /**
     * 获取 [公开链接]脏标记
     */
    @JsonIgnore
    public boolean getPublic_urlDirtyFlag(){
        return this.public_urlDirtyFlag ;
    }   

    /**
     * 获取 [公开链接（HTML版）]
     */
    @JsonProperty("public_url_html")
    public String getPublic_url_html(){
        return this.public_url_html ;
    }

    /**
     * 设置 [公开链接（HTML版）]
     */
    @JsonProperty("public_url_html")
    public void setPublic_url_html(String  public_url_html){
        this.public_url_html = public_url_html ;
        this.public_url_htmlDirtyFlag = true ;
    }

     /**
     * 获取 [公开链接（HTML版）]脏标记
     */
    @JsonIgnore
    public boolean getPublic_url_htmlDirtyFlag(){
        return this.public_url_htmlDirtyFlag ;
    }   

    /**
     * 获取 [测验模式]
     */
    @JsonProperty("quizz_mode")
    public String getQuizz_mode(){
        return this.quizz_mode ;
    }

    /**
     * 设置 [测验模式]
     */
    @JsonProperty("quizz_mode")
    public void setQuizz_mode(String  quizz_mode){
        this.quizz_mode = quizz_mode ;
        this.quizz_modeDirtyFlag = true ;
    }

     /**
     * 获取 [测验模式]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_modeDirtyFlag(){
        return this.quizz_modeDirtyFlag ;
    }   

    /**
     * 获取 [结果链接]
     */
    @JsonProperty("result_url")
    public String getResult_url(){
        return this.result_url ;
    }

    /**
     * 设置 [结果链接]
     */
    @JsonProperty("result_url")
    public void setResult_url(String  result_url){
        this.result_url = result_url ;
        this.result_urlDirtyFlag = true ;
    }

     /**
     * 获取 [结果链接]脏标记
     */
    @JsonIgnore
    public boolean getResult_urlDirtyFlag(){
        return this.result_urlDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }   

    /**
     * 获取 [感谢留言]
     */
    @JsonProperty("thank_you_message")
    public String getThank_you_message(){
        return this.thank_you_message ;
    }

    /**
     * 设置 [感谢留言]
     */
    @JsonProperty("thank_you_message")
    public void setThank_you_message(String  thank_you_message){
        this.thank_you_message = thank_you_message ;
        this.thank_you_messageDirtyFlag = true ;
    }

     /**
     * 获取 [感谢留言]脏标记
     */
    @JsonIgnore
    public boolean getThank_you_messageDirtyFlag(){
        return this.thank_you_messageDirtyFlag ;
    }   

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title")
    public String getTitle(){
        return this.title ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title")
    public void setTitle(String  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

     /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return this.titleDirtyFlag ;
    }   

    /**
     * 获取 [已完成的调查数量]
     */
    @JsonProperty("tot_comp_survey")
    public Integer getTot_comp_survey(){
        return this.tot_comp_survey ;
    }

    /**
     * 设置 [已完成的调查数量]
     */
    @JsonProperty("tot_comp_survey")
    public void setTot_comp_survey(Integer  tot_comp_survey){
        this.tot_comp_survey = tot_comp_survey ;
        this.tot_comp_surveyDirtyFlag = true ;
    }

     /**
     * 获取 [已完成的调查数量]脏标记
     */
    @JsonIgnore
    public boolean getTot_comp_surveyDirtyFlag(){
        return this.tot_comp_surveyDirtyFlag ;
    }   

    /**
     * 获取 [已发送调查者数量]
     */
    @JsonProperty("tot_sent_survey")
    public Integer getTot_sent_survey(){
        return this.tot_sent_survey ;
    }

    /**
     * 设置 [已发送调查者数量]
     */
    @JsonProperty("tot_sent_survey")
    public void setTot_sent_survey(Integer  tot_sent_survey){
        this.tot_sent_survey = tot_sent_survey ;
        this.tot_sent_surveyDirtyFlag = true ;
    }

     /**
     * 获取 [已发送调查者数量]脏标记
     */
    @JsonIgnore
    public boolean getTot_sent_surveyDirtyFlag(){
        return this.tot_sent_surveyDirtyFlag ;
    }   

    /**
     * 获取 [已开始的调查数量]
     */
    @JsonProperty("tot_start_survey")
    public Integer getTot_start_survey(){
        return this.tot_start_survey ;
    }

    /**
     * 设置 [已开始的调查数量]
     */
    @JsonProperty("tot_start_survey")
    public void setTot_start_survey(Integer  tot_start_survey){
        this.tot_start_survey = tot_start_survey ;
        this.tot_start_surveyDirtyFlag = true ;
    }

     /**
     * 获取 [已开始的调查数量]脏标记
     */
    @JsonIgnore
    public boolean getTot_start_surveyDirtyFlag(){
        return this.tot_start_surveyDirtyFlag ;
    }   

    /**
     * 获取 [用户可返回]
     */
    @JsonProperty("users_can_go_back")
    public String getUsers_can_go_back(){
        return this.users_can_go_back ;
    }

    /**
     * 设置 [用户可返回]
     */
    @JsonProperty("users_can_go_back")
    public void setUsers_can_go_back(String  users_can_go_back){
        this.users_can_go_back = users_can_go_back ;
        this.users_can_go_backDirtyFlag = true ;
    }

     /**
     * 获取 [用户可返回]脏标记
     */
    @JsonIgnore
    public boolean getUsers_can_go_backDirtyFlag(){
        return this.users_can_go_backDirtyFlag ;
    }   

    /**
     * 获取 [用户回应]
     */
    @JsonProperty("user_input_ids")
    public String getUser_input_ids(){
        return this.user_input_ids ;
    }

    /**
     * 设置 [用户回应]
     */
    @JsonProperty("user_input_ids")
    public void setUser_input_ids(String  user_input_ids){
        this.user_input_ids = user_input_ids ;
        this.user_input_idsDirtyFlag = true ;
    }

     /**
     * 获取 [用户回应]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_idsDirtyFlag(){
        return this.user_input_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

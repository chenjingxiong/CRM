package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_inputImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_user_input] 服务对象接口
 */
public interface survey_user_inputFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_inputs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_inputs/fetchdefault")
    public Page<survey_user_inputImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_inputs")
    public survey_user_inputImpl create(@RequestBody survey_user_inputImpl survey_user_input);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_inputs/createbatch")
    public survey_user_inputImpl createBatch(@RequestBody List<survey_user_inputImpl> survey_user_inputs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_inputs/{id}")
    public survey_user_inputImpl update(@PathVariable("id") Integer id,@RequestBody survey_user_inputImpl survey_user_input);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_inputs/updatebatch")
    public survey_user_inputImpl updateBatch(@RequestBody List<survey_user_inputImpl> survey_user_inputs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_inputs/{id}")
    public survey_user_inputImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_inputs/removebatch")
    public survey_user_inputImpl removeBatch(@RequestBody List<survey_user_inputImpl> survey_user_inputs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_inputs/select")
    public Page<survey_user_inputImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_inputs/{id}/getdraft")
    public survey_user_inputImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_user_inputImpl survey_user_input);



}

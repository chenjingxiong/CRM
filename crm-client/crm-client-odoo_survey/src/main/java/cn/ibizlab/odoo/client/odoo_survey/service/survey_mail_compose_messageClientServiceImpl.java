package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_mail_compose_message;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_mail_compose_messageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_mail_compose_messageImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_mail_compose_messageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_mail_compose_messageClientServiceImpl implements Isurvey_mail_compose_messageClientService {

    survey_mail_compose_messageFeignClient survey_mail_compose_messageFeignClient;

    @Autowired
    public survey_mail_compose_messageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_mail_compose_messageFeignClient = nameBuilder.target(survey_mail_compose_messageFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_mail_compose_messageFeignClient = nameBuilder.target(survey_mail_compose_messageFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_mail_compose_message createModel() {
		return new survey_mail_compose_messageImpl();
	}


    public void remove(Isurvey_mail_compose_message survey_mail_compose_message){
        survey_mail_compose_messageFeignClient.remove(survey_mail_compose_message.getId()) ;
    }


    public void createBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
        if(survey_mail_compose_messages!=null){
            List<survey_mail_compose_messageImpl> list = new ArrayList<survey_mail_compose_messageImpl>();
            for(Isurvey_mail_compose_message isurvey_mail_compose_message :survey_mail_compose_messages){
                list.add((survey_mail_compose_messageImpl)isurvey_mail_compose_message) ;
            }
            survey_mail_compose_messageFeignClient.createBatch(list) ;
        }
    }


    public void update(Isurvey_mail_compose_message survey_mail_compose_message){
        Isurvey_mail_compose_message clientModel = survey_mail_compose_messageFeignClient.update(survey_mail_compose_message.getId(),(survey_mail_compose_messageImpl)survey_mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_mail_compose_message.getClass(), false);
        copier.copy(clientModel, survey_mail_compose_message, null);
    }


    public void get(Isurvey_mail_compose_message survey_mail_compose_message){
        Isurvey_mail_compose_message clientModel = survey_mail_compose_messageFeignClient.get(survey_mail_compose_message.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_mail_compose_message.getClass(), false);
        copier.copy(clientModel, survey_mail_compose_message, null);
    }


    public void removeBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
        if(survey_mail_compose_messages!=null){
            List<survey_mail_compose_messageImpl> list = new ArrayList<survey_mail_compose_messageImpl>();
            for(Isurvey_mail_compose_message isurvey_mail_compose_message :survey_mail_compose_messages){
                list.add((survey_mail_compose_messageImpl)isurvey_mail_compose_message) ;
            }
            survey_mail_compose_messageFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Isurvey_mail_compose_message> survey_mail_compose_messages){
        if(survey_mail_compose_messages!=null){
            List<survey_mail_compose_messageImpl> list = new ArrayList<survey_mail_compose_messageImpl>();
            for(Isurvey_mail_compose_message isurvey_mail_compose_message :survey_mail_compose_messages){
                list.add((survey_mail_compose_messageImpl)isurvey_mail_compose_message) ;
            }
            survey_mail_compose_messageFeignClient.updateBatch(list) ;
        }
    }


    public void create(Isurvey_mail_compose_message survey_mail_compose_message){
        Isurvey_mail_compose_message clientModel = survey_mail_compose_messageFeignClient.create((survey_mail_compose_messageImpl)survey_mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_mail_compose_message.getClass(), false);
        copier.copy(clientModel, survey_mail_compose_message, null);
    }


    public Page<Isurvey_mail_compose_message> fetchDefault(SearchContext context){
        Page<survey_mail_compose_messageImpl> page = this.survey_mail_compose_messageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Isurvey_mail_compose_message> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_mail_compose_message survey_mail_compose_message){
        Isurvey_mail_compose_message clientModel = survey_mail_compose_messageFeignClient.getDraft(survey_mail_compose_message.getId(),(survey_mail_compose_messageImpl)survey_mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_mail_compose_message.getClass(), false);
        copier.copy(clientModel, survey_mail_compose_message, null);
    }



}


package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_page;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_pageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_pageImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_pageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_page] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_pageClientServiceImpl implements Isurvey_pageClientService {

    survey_pageFeignClient survey_pageFeignClient;

    @Autowired
    public survey_pageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_pageFeignClient = nameBuilder.target(survey_pageFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_pageFeignClient = nameBuilder.target(survey_pageFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_page createModel() {
		return new survey_pageImpl();
	}


    public void get(Isurvey_page survey_page){
        Isurvey_page clientModel = survey_pageFeignClient.get(survey_page.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_page.getClass(), false);
        copier.copy(clientModel, survey_page, null);
    }


    public void updateBatch(List<Isurvey_page> survey_pages){
        if(survey_pages!=null){
            List<survey_pageImpl> list = new ArrayList<survey_pageImpl>();
            for(Isurvey_page isurvey_page :survey_pages){
                list.add((survey_pageImpl)isurvey_page) ;
            }
            survey_pageFeignClient.updateBatch(list) ;
        }
    }


    public void update(Isurvey_page survey_page){
        Isurvey_page clientModel = survey_pageFeignClient.update(survey_page.getId(),(survey_pageImpl)survey_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_page.getClass(), false);
        copier.copy(clientModel, survey_page, null);
    }


    public void createBatch(List<Isurvey_page> survey_pages){
        if(survey_pages!=null){
            List<survey_pageImpl> list = new ArrayList<survey_pageImpl>();
            for(Isurvey_page isurvey_page :survey_pages){
                list.add((survey_pageImpl)isurvey_page) ;
            }
            survey_pageFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Isurvey_page> survey_pages){
        if(survey_pages!=null){
            List<survey_pageImpl> list = new ArrayList<survey_pageImpl>();
            for(Isurvey_page isurvey_page :survey_pages){
                list.add((survey_pageImpl)isurvey_page) ;
            }
            survey_pageFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isurvey_page> fetchDefault(SearchContext context){
        Page<survey_pageImpl> page = this.survey_pageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isurvey_page survey_page){
        survey_pageFeignClient.remove(survey_page.getId()) ;
    }


    public void create(Isurvey_page survey_page){
        Isurvey_page clientModel = survey_pageFeignClient.create((survey_pageImpl)survey_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_page.getClass(), false);
        copier.copy(clientModel, survey_page, null);
    }


    public Page<Isurvey_page> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_page survey_page){
        Isurvey_page clientModel = survey_pageFeignClient.getDraft(survey_page.getId(),(survey_pageImpl)survey_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_page.getClass(), false);
        copier.copy(clientModel, survey_page, null);
    }



}


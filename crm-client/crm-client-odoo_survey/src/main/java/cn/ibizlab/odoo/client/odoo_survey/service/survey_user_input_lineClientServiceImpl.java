package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_user_input_lineClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_input_lineImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_user_input_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_user_input_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_user_input_lineClientServiceImpl implements Isurvey_user_input_lineClientService {

    survey_user_input_lineFeignClient survey_user_input_lineFeignClient;

    @Autowired
    public survey_user_input_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_user_input_lineFeignClient = nameBuilder.target(survey_user_input_lineFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_user_input_lineFeignClient = nameBuilder.target(survey_user_input_lineFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_user_input_line createModel() {
		return new survey_user_input_lineImpl();
	}


    public Page<Isurvey_user_input_line> fetchDefault(SearchContext context){
        Page<survey_user_input_lineImpl> page = this.survey_user_input_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isurvey_user_input_line survey_user_input_line){
        survey_user_input_lineFeignClient.remove(survey_user_input_line.getId()) ;
    }


    public void updateBatch(List<Isurvey_user_input_line> survey_user_input_lines){
        if(survey_user_input_lines!=null){
            List<survey_user_input_lineImpl> list = new ArrayList<survey_user_input_lineImpl>();
            for(Isurvey_user_input_line isurvey_user_input_line :survey_user_input_lines){
                list.add((survey_user_input_lineImpl)isurvey_user_input_line) ;
            }
            survey_user_input_lineFeignClient.updateBatch(list) ;
        }
    }


    public void get(Isurvey_user_input_line survey_user_input_line){
        Isurvey_user_input_line clientModel = survey_user_input_lineFeignClient.get(survey_user_input_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input_line.getClass(), false);
        copier.copy(clientModel, survey_user_input_line, null);
    }


    public void update(Isurvey_user_input_line survey_user_input_line){
        Isurvey_user_input_line clientModel = survey_user_input_lineFeignClient.update(survey_user_input_line.getId(),(survey_user_input_lineImpl)survey_user_input_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input_line.getClass(), false);
        copier.copy(clientModel, survey_user_input_line, null);
    }


    public void createBatch(List<Isurvey_user_input_line> survey_user_input_lines){
        if(survey_user_input_lines!=null){
            List<survey_user_input_lineImpl> list = new ArrayList<survey_user_input_lineImpl>();
            for(Isurvey_user_input_line isurvey_user_input_line :survey_user_input_lines){
                list.add((survey_user_input_lineImpl)isurvey_user_input_line) ;
            }
            survey_user_input_lineFeignClient.createBatch(list) ;
        }
    }


    public void create(Isurvey_user_input_line survey_user_input_line){
        Isurvey_user_input_line clientModel = survey_user_input_lineFeignClient.create((survey_user_input_lineImpl)survey_user_input_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input_line.getClass(), false);
        copier.copy(clientModel, survey_user_input_line, null);
    }


    public void removeBatch(List<Isurvey_user_input_line> survey_user_input_lines){
        if(survey_user_input_lines!=null){
            List<survey_user_input_lineImpl> list = new ArrayList<survey_user_input_lineImpl>();
            for(Isurvey_user_input_line isurvey_user_input_line :survey_user_input_lines){
                list.add((survey_user_input_lineImpl)isurvey_user_input_line) ;
            }
            survey_user_input_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isurvey_user_input_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_user_input_line survey_user_input_line){
        Isurvey_user_input_line clientModel = survey_user_input_lineFeignClient.getDraft(survey_user_input_line.getId(),(survey_user_input_lineImpl)survey_user_input_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input_line.getClass(), false);
        copier.copy(clientModel, survey_user_input_line, null);
    }



}


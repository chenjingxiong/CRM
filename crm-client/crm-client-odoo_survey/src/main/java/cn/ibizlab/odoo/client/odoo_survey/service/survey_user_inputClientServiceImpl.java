package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_user_inputClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_inputImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_user_inputFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_user_input] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_user_inputClientServiceImpl implements Isurvey_user_inputClientService {

    survey_user_inputFeignClient survey_user_inputFeignClient;

    @Autowired
    public survey_user_inputClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_user_inputFeignClient = nameBuilder.target(survey_user_inputFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_user_inputFeignClient = nameBuilder.target(survey_user_inputFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_user_input createModel() {
		return new survey_user_inputImpl();
	}


    public void remove(Isurvey_user_input survey_user_input){
        survey_user_inputFeignClient.remove(survey_user_input.getId()) ;
    }


    public Page<Isurvey_user_input> fetchDefault(SearchContext context){
        Page<survey_user_inputImpl> page = this.survey_user_inputFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Isurvey_user_input survey_user_input){
        Isurvey_user_input clientModel = survey_user_inputFeignClient.create((survey_user_inputImpl)survey_user_input) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input.getClass(), false);
        copier.copy(clientModel, survey_user_input, null);
    }


    public void createBatch(List<Isurvey_user_input> survey_user_inputs){
        if(survey_user_inputs!=null){
            List<survey_user_inputImpl> list = new ArrayList<survey_user_inputImpl>();
            for(Isurvey_user_input isurvey_user_input :survey_user_inputs){
                list.add((survey_user_inputImpl)isurvey_user_input) ;
            }
            survey_user_inputFeignClient.createBatch(list) ;
        }
    }


    public void update(Isurvey_user_input survey_user_input){
        Isurvey_user_input clientModel = survey_user_inputFeignClient.update(survey_user_input.getId(),(survey_user_inputImpl)survey_user_input) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input.getClass(), false);
        copier.copy(clientModel, survey_user_input, null);
    }


    public void updateBatch(List<Isurvey_user_input> survey_user_inputs){
        if(survey_user_inputs!=null){
            List<survey_user_inputImpl> list = new ArrayList<survey_user_inputImpl>();
            for(Isurvey_user_input isurvey_user_input :survey_user_inputs){
                list.add((survey_user_inputImpl)isurvey_user_input) ;
            }
            survey_user_inputFeignClient.updateBatch(list) ;
        }
    }


    public void get(Isurvey_user_input survey_user_input){
        Isurvey_user_input clientModel = survey_user_inputFeignClient.get(survey_user_input.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input.getClass(), false);
        copier.copy(clientModel, survey_user_input, null);
    }


    public void removeBatch(List<Isurvey_user_input> survey_user_inputs){
        if(survey_user_inputs!=null){
            List<survey_user_inputImpl> list = new ArrayList<survey_user_inputImpl>();
            for(Isurvey_user_input isurvey_user_input :survey_user_inputs){
                list.add((survey_user_inputImpl)isurvey_user_input) ;
            }
            survey_user_inputFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isurvey_user_input> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_user_input survey_user_input){
        Isurvey_user_input clientModel = survey_user_inputFeignClient.getDraft(survey_user_input.getId(),(survey_user_inputImpl)survey_user_input) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_user_input.getClass(), false);
        copier.copy(clientModel, survey_user_input, null);
    }



}


package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_page;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_pageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_page] 服务对象接口
 */
public interface survey_pageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_pages/{id}")
    public survey_pageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_pages/updatebatch")
    public survey_pageImpl updateBatch(@RequestBody List<survey_pageImpl> survey_pages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_pages/{id}")
    public survey_pageImpl update(@PathVariable("id") Integer id,@RequestBody survey_pageImpl survey_page);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_pages/createbatch")
    public survey_pageImpl createBatch(@RequestBody List<survey_pageImpl> survey_pages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_pages/removebatch")
    public survey_pageImpl removeBatch(@RequestBody List<survey_pageImpl> survey_pages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_pages/fetchdefault")
    public Page<survey_pageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_pages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_pages")
    public survey_pageImpl create(@RequestBody survey_pageImpl survey_page);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_pages/select")
    public Page<survey_pageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_pages/{id}/getdraft")
    public survey_pageImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_pageImpl survey_page);



}

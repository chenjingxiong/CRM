package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_input_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_user_input_line] 服务对象接口
 */
public interface survey_user_input_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_input_lines/fetchdefault")
    public Page<survey_user_input_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_input_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_input_lines/updatebatch")
    public survey_user_input_lineImpl updateBatch(@RequestBody List<survey_user_input_lineImpl> survey_user_input_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_input_lines/{id}")
    public survey_user_input_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_input_lines/{id}")
    public survey_user_input_lineImpl update(@PathVariable("id") Integer id,@RequestBody survey_user_input_lineImpl survey_user_input_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_input_lines/createbatch")
    public survey_user_input_lineImpl createBatch(@RequestBody List<survey_user_input_lineImpl> survey_user_input_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_input_lines")
    public survey_user_input_lineImpl create(@RequestBody survey_user_input_lineImpl survey_user_input_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_input_lines/removebatch")
    public survey_user_input_lineImpl removeBatch(@RequestBody List<survey_user_input_lineImpl> survey_user_input_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_input_lines/select")
    public Page<survey_user_input_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_input_lines/{id}/getdraft")
    public survey_user_input_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_user_input_lineImpl survey_user_input_line);



}

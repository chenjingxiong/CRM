package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_question;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_questionClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_questionImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_questionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_question] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_questionClientServiceImpl implements Isurvey_questionClientService {

    survey_questionFeignClient survey_questionFeignClient;

    @Autowired
    public survey_questionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_questionFeignClient = nameBuilder.target(survey_questionFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_questionFeignClient = nameBuilder.target(survey_questionFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_question createModel() {
		return new survey_questionImpl();
	}


    public Page<Isurvey_question> fetchDefault(SearchContext context){
        Page<survey_questionImpl> page = this.survey_questionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isurvey_question survey_question){
        survey_questionFeignClient.remove(survey_question.getId()) ;
    }


    public void update(Isurvey_question survey_question){
        Isurvey_question clientModel = survey_questionFeignClient.update(survey_question.getId(),(survey_questionImpl)survey_question) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_question.getClass(), false);
        copier.copy(clientModel, survey_question, null);
    }


    public void get(Isurvey_question survey_question){
        Isurvey_question clientModel = survey_questionFeignClient.get(survey_question.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_question.getClass(), false);
        copier.copy(clientModel, survey_question, null);
    }


    public void create(Isurvey_question survey_question){
        Isurvey_question clientModel = survey_questionFeignClient.create((survey_questionImpl)survey_question) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_question.getClass(), false);
        copier.copy(clientModel, survey_question, null);
    }


    public void createBatch(List<Isurvey_question> survey_questions){
        if(survey_questions!=null){
            List<survey_questionImpl> list = new ArrayList<survey_questionImpl>();
            for(Isurvey_question isurvey_question :survey_questions){
                list.add((survey_questionImpl)isurvey_question) ;
            }
            survey_questionFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Isurvey_question> survey_questions){
        if(survey_questions!=null){
            List<survey_questionImpl> list = new ArrayList<survey_questionImpl>();
            for(Isurvey_question isurvey_question :survey_questions){
                list.add((survey_questionImpl)isurvey_question) ;
            }
            survey_questionFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Isurvey_question> survey_questions){
        if(survey_questions!=null){
            List<survey_questionImpl> list = new ArrayList<survey_questionImpl>();
            for(Isurvey_question isurvey_question :survey_questions){
                list.add((survey_questionImpl)isurvey_question) ;
            }
            survey_questionFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isurvey_question> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_question survey_question){
        Isurvey_question clientModel = survey_questionFeignClient.getDraft(survey_question.getId(),(survey_questionImpl)survey_question) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_question.getClass(), false);
        copier.copy(clientModel, survey_question, null);
    }



}


package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_mail_compose_message;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_mail_compose_messageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
public interface survey_mail_compose_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_mail_compose_messages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_mail_compose_messages/createbatch")
    public survey_mail_compose_messageImpl createBatch(@RequestBody List<survey_mail_compose_messageImpl> survey_mail_compose_messages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_mail_compose_messages/{id}")
    public survey_mail_compose_messageImpl update(@PathVariable("id") Integer id,@RequestBody survey_mail_compose_messageImpl survey_mail_compose_message);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_mail_compose_messages/{id}")
    public survey_mail_compose_messageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_mail_compose_messages/removebatch")
    public survey_mail_compose_messageImpl removeBatch(@RequestBody List<survey_mail_compose_messageImpl> survey_mail_compose_messages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_mail_compose_messages/updatebatch")
    public survey_mail_compose_messageImpl updateBatch(@RequestBody List<survey_mail_compose_messageImpl> survey_mail_compose_messages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_mail_compose_messages")
    public survey_mail_compose_messageImpl create(@RequestBody survey_mail_compose_messageImpl survey_mail_compose_message);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_mail_compose_messages/fetchdefault")
    public Page<survey_mail_compose_messageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_mail_compose_messages/select")
    public Page<survey_mail_compose_messageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_mail_compose_messages/{id}/getdraft")
    public survey_mail_compose_messageImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_mail_compose_messageImpl survey_mail_compose_message);



}

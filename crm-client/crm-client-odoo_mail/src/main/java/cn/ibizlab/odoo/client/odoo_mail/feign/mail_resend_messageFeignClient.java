package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_resend_message;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_messageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_resend_message] 服务对象接口
 */
public interface mail_resend_messageFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_messages/updatebatch")
    public mail_resend_messageImpl updateBatch(@RequestBody List<mail_resend_messageImpl> mail_resend_messages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_messages/{id}")
    public mail_resend_messageImpl update(@PathVariable("id") Integer id,@RequestBody mail_resend_messageImpl mail_resend_message);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_messages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_messages/createbatch")
    public mail_resend_messageImpl createBatch(@RequestBody List<mail_resend_messageImpl> mail_resend_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_messages/{id}")
    public mail_resend_messageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_messages/fetchdefault")
    public Page<mail_resend_messageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_messages/removebatch")
    public mail_resend_messageImpl removeBatch(@RequestBody List<mail_resend_messageImpl> mail_resend_messages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_messages")
    public mail_resend_messageImpl create(@RequestBody mail_resend_messageImpl mail_resend_message);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_messages/select")
    public Page<mail_resend_messageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_messages/{id}/getdraft")
    public mail_resend_messageImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_resend_messageImpl mail_resend_message);



}

package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_channel_partner;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_channel_partnerClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channel_partnerImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_channel_partnerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_channel_partner] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_channel_partnerClientServiceImpl implements Imail_channel_partnerClientService {

    mail_channel_partnerFeignClient mail_channel_partnerFeignClient;

    @Autowired
    public mail_channel_partnerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_channel_partnerFeignClient = nameBuilder.target(mail_channel_partnerFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_channel_partnerFeignClient = nameBuilder.target(mail_channel_partnerFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_channel_partner createModel() {
		return new mail_channel_partnerImpl();
	}


    public void updateBatch(List<Imail_channel_partner> mail_channel_partners){
        if(mail_channel_partners!=null){
            List<mail_channel_partnerImpl> list = new ArrayList<mail_channel_partnerImpl>();
            for(Imail_channel_partner imail_channel_partner :mail_channel_partners){
                list.add((mail_channel_partnerImpl)imail_channel_partner) ;
            }
            mail_channel_partnerFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_channel_partner> mail_channel_partners){
        if(mail_channel_partners!=null){
            List<mail_channel_partnerImpl> list = new ArrayList<mail_channel_partnerImpl>();
            for(Imail_channel_partner imail_channel_partner :mail_channel_partners){
                list.add((mail_channel_partnerImpl)imail_channel_partner) ;
            }
            mail_channel_partnerFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imail_channel_partner> mail_channel_partners){
        if(mail_channel_partners!=null){
            List<mail_channel_partnerImpl> list = new ArrayList<mail_channel_partnerImpl>();
            for(Imail_channel_partner imail_channel_partner :mail_channel_partners){
                list.add((mail_channel_partnerImpl)imail_channel_partner) ;
            }
            mail_channel_partnerFeignClient.createBatch(list) ;
        }
    }


    public void create(Imail_channel_partner mail_channel_partner){
        Imail_channel_partner clientModel = mail_channel_partnerFeignClient.create((mail_channel_partnerImpl)mail_channel_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel_partner.getClass(), false);
        copier.copy(clientModel, mail_channel_partner, null);
    }


    public void update(Imail_channel_partner mail_channel_partner){
        Imail_channel_partner clientModel = mail_channel_partnerFeignClient.update(mail_channel_partner.getId(),(mail_channel_partnerImpl)mail_channel_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel_partner.getClass(), false);
        copier.copy(clientModel, mail_channel_partner, null);
    }


    public Page<Imail_channel_partner> fetchDefault(SearchContext context){
        Page<mail_channel_partnerImpl> page = this.mail_channel_partnerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imail_channel_partner mail_channel_partner){
        mail_channel_partnerFeignClient.remove(mail_channel_partner.getId()) ;
    }


    public void get(Imail_channel_partner mail_channel_partner){
        Imail_channel_partner clientModel = mail_channel_partnerFeignClient.get(mail_channel_partner.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel_partner.getClass(), false);
        copier.copy(clientModel, mail_channel_partner, null);
    }


    public Page<Imail_channel_partner> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_channel_partner mail_channel_partner){
        Imail_channel_partner clientModel = mail_channel_partnerFeignClient.getDraft(mail_channel_partner.getId(),(mail_channel_partnerImpl)mail_channel_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_channel_partner.getClass(), false);
        copier.copy(clientModel, mail_channel_partner, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_message_subtype;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_message_subtypeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_message_subtype] 服务对象接口
 */
public interface mail_message_subtypeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_message_subtypes/updatebatch")
    public mail_message_subtypeImpl updateBatch(@RequestBody List<mail_message_subtypeImpl> mail_message_subtypes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_message_subtypes/{id}")
    public mail_message_subtypeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_message_subtypes/{id}")
    public mail_message_subtypeImpl update(@PathVariable("id") Integer id,@RequestBody mail_message_subtypeImpl mail_message_subtype);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_message_subtypes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_message_subtypes/removebatch")
    public mail_message_subtypeImpl removeBatch(@RequestBody List<mail_message_subtypeImpl> mail_message_subtypes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_message_subtypes")
    public mail_message_subtypeImpl create(@RequestBody mail_message_subtypeImpl mail_message_subtype);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_message_subtypes/createbatch")
    public mail_message_subtypeImpl createBatch(@RequestBody List<mail_message_subtypeImpl> mail_message_subtypes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_message_subtypes/fetchdefault")
    public Page<mail_message_subtypeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_message_subtypes/select")
    public Page<mail_message_subtypeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_message_subtypes/{id}/getdraft")
    public mail_message_subtypeImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_message_subtypeImpl mail_message_subtype);



}

package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_shortcode;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_shortcodeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_shortcodeImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_shortcodeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_shortcode] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_shortcodeClientServiceImpl implements Imail_shortcodeClientService {

    mail_shortcodeFeignClient mail_shortcodeFeignClient;

    @Autowired
    public mail_shortcodeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_shortcodeFeignClient = nameBuilder.target(mail_shortcodeFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_shortcodeFeignClient = nameBuilder.target(mail_shortcodeFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_shortcode createModel() {
		return new mail_shortcodeImpl();
	}


    public void updateBatch(List<Imail_shortcode> mail_shortcodes){
        if(mail_shortcodes!=null){
            List<mail_shortcodeImpl> list = new ArrayList<mail_shortcodeImpl>();
            for(Imail_shortcode imail_shortcode :mail_shortcodes){
                list.add((mail_shortcodeImpl)imail_shortcode) ;
            }
            mail_shortcodeFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_shortcode mail_shortcode){
        Imail_shortcode clientModel = mail_shortcodeFeignClient.get(mail_shortcode.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_shortcode.getClass(), false);
        copier.copy(clientModel, mail_shortcode, null);
    }


    public Page<Imail_shortcode> fetchDefault(SearchContext context){
        Page<mail_shortcodeImpl> page = this.mail_shortcodeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imail_shortcode> mail_shortcodes){
        if(mail_shortcodes!=null){
            List<mail_shortcodeImpl> list = new ArrayList<mail_shortcodeImpl>();
            for(Imail_shortcode imail_shortcode :mail_shortcodes){
                list.add((mail_shortcodeImpl)imail_shortcode) ;
            }
            mail_shortcodeFeignClient.createBatch(list) ;
        }
    }


    public void create(Imail_shortcode mail_shortcode){
        Imail_shortcode clientModel = mail_shortcodeFeignClient.create((mail_shortcodeImpl)mail_shortcode) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_shortcode.getClass(), false);
        copier.copy(clientModel, mail_shortcode, null);
    }


    public void removeBatch(List<Imail_shortcode> mail_shortcodes){
        if(mail_shortcodes!=null){
            List<mail_shortcodeImpl> list = new ArrayList<mail_shortcodeImpl>();
            for(Imail_shortcode imail_shortcode :mail_shortcodes){
                list.add((mail_shortcodeImpl)imail_shortcode) ;
            }
            mail_shortcodeFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_shortcode mail_shortcode){
        Imail_shortcode clientModel = mail_shortcodeFeignClient.update(mail_shortcode.getId(),(mail_shortcodeImpl)mail_shortcode) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_shortcode.getClass(), false);
        copier.copy(clientModel, mail_shortcode, null);
    }


    public void remove(Imail_shortcode mail_shortcode){
        mail_shortcodeFeignClient.remove(mail_shortcode.getId()) ;
    }


    public Page<Imail_shortcode> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_shortcode mail_shortcode){
        Imail_shortcode clientModel = mail_shortcodeFeignClient.getDraft(mail_shortcode.getId(),(mail_shortcodeImpl)mail_shortcode) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_shortcode.getClass(), false);
        copier.copy(clientModel, mail_shortcode, null);
    }



}


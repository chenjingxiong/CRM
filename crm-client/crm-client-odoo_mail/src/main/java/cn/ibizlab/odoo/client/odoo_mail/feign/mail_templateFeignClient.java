package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_template;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_template] 服务对象接口
 */
public interface mail_templateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_templates/updatebatch")
    public mail_templateImpl updateBatch(@RequestBody List<mail_templateImpl> mail_templates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_templates")
    public mail_templateImpl create(@RequestBody mail_templateImpl mail_template);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_templates/createbatch")
    public mail_templateImpl createBatch(@RequestBody List<mail_templateImpl> mail_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_templates/fetchdefault")
    public Page<mail_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_templates/{id}")
    public mail_templateImpl update(@PathVariable("id") Integer id,@RequestBody mail_templateImpl mail_template);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_templates/{id}")
    public mail_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_templates/removebatch")
    public mail_templateImpl removeBatch(@RequestBody List<mail_templateImpl> mail_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_templates/select")
    public Page<mail_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_templates/{id}/getdraft")
    public mail_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_templateImpl mail_template);



}

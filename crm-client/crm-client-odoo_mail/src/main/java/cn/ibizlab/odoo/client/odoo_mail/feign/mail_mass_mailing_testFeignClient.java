package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_test;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_testImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
public interface mail_mass_mailing_testFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tests/{id}")
    public mail_mass_mailing_testImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_testImpl mail_mass_mailing_test);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tests")
    public mail_mass_mailing_testImpl create(@RequestBody mail_mass_mailing_testImpl mail_mass_mailing_test);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tests/createbatch")
    public mail_mass_mailing_testImpl createBatch(@RequestBody List<mail_mass_mailing_testImpl> mail_mass_mailing_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tests/fetchdefault")
    public Page<mail_mass_mailing_testImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tests/removebatch")
    public mail_mass_mailing_testImpl removeBatch(@RequestBody List<mail_mass_mailing_testImpl> mail_mass_mailing_tests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tests/updatebatch")
    public mail_mass_mailing_testImpl updateBatch(@RequestBody List<mail_mass_mailing_testImpl> mail_mass_mailing_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tests/{id}")
    public mail_mass_mailing_testImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tests/select")
    public Page<mail_mass_mailing_testImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tests/{id}/getdraft")
    public mail_mass_mailing_testImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_testImpl mail_mass_mailing_test);



}

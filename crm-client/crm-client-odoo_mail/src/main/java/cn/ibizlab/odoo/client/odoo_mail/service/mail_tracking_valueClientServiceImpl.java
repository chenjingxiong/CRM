package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_tracking_valueClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_tracking_valueImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_tracking_valueFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_tracking_value] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_tracking_valueClientServiceImpl implements Imail_tracking_valueClientService {

    mail_tracking_valueFeignClient mail_tracking_valueFeignClient;

    @Autowired
    public mail_tracking_valueClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_tracking_valueFeignClient = nameBuilder.target(mail_tracking_valueFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_tracking_valueFeignClient = nameBuilder.target(mail_tracking_valueFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_tracking_value createModel() {
		return new mail_tracking_valueImpl();
	}


    public Page<Imail_tracking_value> fetchDefault(SearchContext context){
        Page<mail_tracking_valueImpl> page = this.mail_tracking_valueFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_tracking_value mail_tracking_value){
        Imail_tracking_value clientModel = mail_tracking_valueFeignClient.get(mail_tracking_value.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_tracking_value.getClass(), false);
        copier.copy(clientModel, mail_tracking_value, null);
    }


    public void update(Imail_tracking_value mail_tracking_value){
        Imail_tracking_value clientModel = mail_tracking_valueFeignClient.update(mail_tracking_value.getId(),(mail_tracking_valueImpl)mail_tracking_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_tracking_value.getClass(), false);
        copier.copy(clientModel, mail_tracking_value, null);
    }


    public void removeBatch(List<Imail_tracking_value> mail_tracking_values){
        if(mail_tracking_values!=null){
            List<mail_tracking_valueImpl> list = new ArrayList<mail_tracking_valueImpl>();
            for(Imail_tracking_value imail_tracking_value :mail_tracking_values){
                list.add((mail_tracking_valueImpl)imail_tracking_value) ;
            }
            mail_tracking_valueFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_tracking_value mail_tracking_value){
        mail_tracking_valueFeignClient.remove(mail_tracking_value.getId()) ;
    }


    public void createBatch(List<Imail_tracking_value> mail_tracking_values){
        if(mail_tracking_values!=null){
            List<mail_tracking_valueImpl> list = new ArrayList<mail_tracking_valueImpl>();
            for(Imail_tracking_value imail_tracking_value :mail_tracking_values){
                list.add((mail_tracking_valueImpl)imail_tracking_value) ;
            }
            mail_tracking_valueFeignClient.createBatch(list) ;
        }
    }


    public void create(Imail_tracking_value mail_tracking_value){
        Imail_tracking_value clientModel = mail_tracking_valueFeignClient.create((mail_tracking_valueImpl)mail_tracking_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_tracking_value.getClass(), false);
        copier.copy(clientModel, mail_tracking_value, null);
    }


    public void updateBatch(List<Imail_tracking_value> mail_tracking_values){
        if(mail_tracking_values!=null){
            List<mail_tracking_valueImpl> list = new ArrayList<mail_tracking_valueImpl>();
            for(Imail_tracking_value imail_tracking_value :mail_tracking_values){
                list.add((mail_tracking_valueImpl)imail_tracking_value) ;
            }
            mail_tracking_valueFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_tracking_value> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_tracking_value mail_tracking_value){
        Imail_tracking_value clientModel = mail_tracking_valueFeignClient.getDraft(mail_tracking_value.getId(),(mail_tracking_valueImpl)mail_tracking_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_tracking_value.getClass(), false);
        copier.copy(clientModel, mail_tracking_value, null);
    }



}


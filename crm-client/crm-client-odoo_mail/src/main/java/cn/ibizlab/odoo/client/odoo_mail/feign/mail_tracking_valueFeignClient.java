package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_tracking_valueImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_tracking_value] 服务对象接口
 */
public interface mail_tracking_valueFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_tracking_values/fetchdefault")
    public Page<mail_tracking_valueImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_tracking_values/{id}")
    public mail_tracking_valueImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_tracking_values/{id}")
    public mail_tracking_valueImpl update(@PathVariable("id") Integer id,@RequestBody mail_tracking_valueImpl mail_tracking_value);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_tracking_values/removebatch")
    public mail_tracking_valueImpl removeBatch(@RequestBody List<mail_tracking_valueImpl> mail_tracking_values);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_tracking_values/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_tracking_values/createbatch")
    public mail_tracking_valueImpl createBatch(@RequestBody List<mail_tracking_valueImpl> mail_tracking_values);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_tracking_values")
    public mail_tracking_valueImpl create(@RequestBody mail_tracking_valueImpl mail_tracking_value);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_tracking_values/updatebatch")
    public mail_tracking_valueImpl updateBatch(@RequestBody List<mail_tracking_valueImpl> mail_tracking_values);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_tracking_values/select")
    public Page<mail_tracking_valueImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_tracking_values/{id}/getdraft")
    public mail_tracking_valueImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_tracking_valueImpl mail_tracking_value);



}

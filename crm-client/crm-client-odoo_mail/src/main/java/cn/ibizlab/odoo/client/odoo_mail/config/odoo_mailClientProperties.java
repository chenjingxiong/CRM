package cn.ibizlab.odoo.client.odoo_mail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.service.odoo.mail")
@Data
public class odoo_mailClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

	private String serviceUrl ;
    	
    private String serviceId ;

}

package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_alias_mixin;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_alias_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_alias_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_alias_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_alias_mixinClientServiceImpl implements Imail_alias_mixinClientService {

    mail_alias_mixinFeignClient mail_alias_mixinFeignClient;

    @Autowired
    public mail_alias_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_alias_mixinFeignClient = nameBuilder.target(mail_alias_mixinFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_alias_mixinFeignClient = nameBuilder.target(mail_alias_mixinFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_alias_mixin createModel() {
		return new mail_alias_mixinImpl();
	}


    public void updateBatch(List<Imail_alias_mixin> mail_alias_mixins){
        if(mail_alias_mixins!=null){
            List<mail_alias_mixinImpl> list = new ArrayList<mail_alias_mixinImpl>();
            for(Imail_alias_mixin imail_alias_mixin :mail_alias_mixins){
                list.add((mail_alias_mixinImpl)imail_alias_mixin) ;
            }
            mail_alias_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Imail_alias_mixin mail_alias_mixin){
        mail_alias_mixinFeignClient.remove(mail_alias_mixin.getId()) ;
    }


    public Page<Imail_alias_mixin> fetchDefault(SearchContext context){
        Page<mail_alias_mixinImpl> page = this.mail_alias_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_alias_mixin> mail_alias_mixins){
        if(mail_alias_mixins!=null){
            List<mail_alias_mixinImpl> list = new ArrayList<mail_alias_mixinImpl>();
            for(Imail_alias_mixin imail_alias_mixin :mail_alias_mixins){
                list.add((mail_alias_mixinImpl)imail_alias_mixin) ;
            }
            mail_alias_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_alias_mixin mail_alias_mixin){
        Imail_alias_mixin clientModel = mail_alias_mixinFeignClient.update(mail_alias_mixin.getId(),(mail_alias_mixinImpl)mail_alias_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias_mixin.getClass(), false);
        copier.copy(clientModel, mail_alias_mixin, null);
    }


    public void create(Imail_alias_mixin mail_alias_mixin){
        Imail_alias_mixin clientModel = mail_alias_mixinFeignClient.create((mail_alias_mixinImpl)mail_alias_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias_mixin.getClass(), false);
        copier.copy(clientModel, mail_alias_mixin, null);
    }


    public void get(Imail_alias_mixin mail_alias_mixin){
        Imail_alias_mixin clientModel = mail_alias_mixinFeignClient.get(mail_alias_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias_mixin.getClass(), false);
        copier.copy(clientModel, mail_alias_mixin, null);
    }


    public void createBatch(List<Imail_alias_mixin> mail_alias_mixins){
        if(mail_alias_mixins!=null){
            List<mail_alias_mixinImpl> list = new ArrayList<mail_alias_mixinImpl>();
            for(Imail_alias_mixin imail_alias_mixin :mail_alias_mixins){
                list.add((mail_alias_mixinImpl)imail_alias_mixin) ;
            }
            mail_alias_mixinFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_alias_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_alias_mixin mail_alias_mixin){
        Imail_alias_mixin clientModel = mail_alias_mixinFeignClient.getDraft(mail_alias_mixin.getId(),(mail_alias_mixinImpl)mail_alias_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias_mixin.getClass(), false);
        copier.copy(clientModel, mail_alias_mixin, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_bot;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_botImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_bot] 服务对象接口
 */
public interface mail_botFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_bots/{id}")
    public mail_botImpl update(@PathVariable("id") Integer id,@RequestBody mail_botImpl mail_bot);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_bots")
    public mail_botImpl create(@RequestBody mail_botImpl mail_bot);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_bots/createbatch")
    public mail_botImpl createBatch(@RequestBody List<mail_botImpl> mail_bots);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_bots/fetchdefault")
    public Page<mail_botImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_bots/{id}")
    public mail_botImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_bots/removebatch")
    public mail_botImpl removeBatch(@RequestBody List<mail_botImpl> mail_bots);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_bots/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_bots/updatebatch")
    public mail_botImpl updateBatch(@RequestBody List<mail_botImpl> mail_bots);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_bots/select")
    public Page<mail_botImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_bots/{id}/getdraft")
    public mail_botImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_botImpl mail_bot);



}

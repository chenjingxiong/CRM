package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_resend_partner;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_partnerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_resend_partner] 服务对象接口
 */
public interface mail_resend_partnerFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_partners/{id}")
    public mail_resend_partnerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_partners/{id}")
    public mail_resend_partnerImpl update(@PathVariable("id") Integer id,@RequestBody mail_resend_partnerImpl mail_resend_partner);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_partners/updatebatch")
    public mail_resend_partnerImpl updateBatch(@RequestBody List<mail_resend_partnerImpl> mail_resend_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_partners/removebatch")
    public mail_resend_partnerImpl removeBatch(@RequestBody List<mail_resend_partnerImpl> mail_resend_partners);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_partners/fetchdefault")
    public Page<mail_resend_partnerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_partners")
    public mail_resend_partnerImpl create(@RequestBody mail_resend_partnerImpl mail_resend_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_partners/createbatch")
    public mail_resend_partnerImpl createBatch(@RequestBody List<mail_resend_partnerImpl> mail_resend_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_partners/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_partners/select")
    public Page<mail_resend_partnerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_partners/{id}/getdraft")
    public mail_resend_partnerImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_resend_partnerImpl mail_resend_partner);



}

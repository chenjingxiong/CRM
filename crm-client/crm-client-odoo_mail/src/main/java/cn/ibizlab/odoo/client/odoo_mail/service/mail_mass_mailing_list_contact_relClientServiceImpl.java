package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_list_contact_relClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_list_contact_relImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_list_contact_relFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_list_contact_relClientServiceImpl implements Imail_mass_mailing_list_contact_relClientService {

    mail_mass_mailing_list_contact_relFeignClient mail_mass_mailing_list_contact_relFeignClient;

    @Autowired
    public mail_mass_mailing_list_contact_relClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_list_contact_relFeignClient = nameBuilder.target(mail_mass_mailing_list_contact_relFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_list_contact_relFeignClient = nameBuilder.target(mail_mass_mailing_list_contact_relFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_list_contact_rel createModel() {
		return new mail_mass_mailing_list_contact_relImpl();
	}


    public Page<Imail_mass_mailing_list_contact_rel> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_list_contact_relImpl> page = this.mail_mass_mailing_list_contact_relFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
        Imail_mass_mailing_list_contact_rel clientModel = mail_mass_mailing_list_contact_relFeignClient.create((mail_mass_mailing_list_contact_relImpl)mail_mass_mailing_list_contact_rel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list_contact_rel.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list_contact_rel, null);
    }


    public void remove(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
        mail_mass_mailing_list_contact_relFeignClient.remove(mail_mass_mailing_list_contact_rel.getId()) ;
    }


    public void removeBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
        if(mail_mass_mailing_list_contact_rels!=null){
            List<mail_mass_mailing_list_contact_relImpl> list = new ArrayList<mail_mass_mailing_list_contact_relImpl>();
            for(Imail_mass_mailing_list_contact_rel imail_mass_mailing_list_contact_rel :mail_mass_mailing_list_contact_rels){
                list.add((mail_mass_mailing_list_contact_relImpl)imail_mass_mailing_list_contact_rel) ;
            }
            mail_mass_mailing_list_contact_relFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
        if(mail_mass_mailing_list_contact_rels!=null){
            List<mail_mass_mailing_list_contact_relImpl> list = new ArrayList<mail_mass_mailing_list_contact_relImpl>();
            for(Imail_mass_mailing_list_contact_rel imail_mass_mailing_list_contact_rel :mail_mass_mailing_list_contact_rels){
                list.add((mail_mass_mailing_list_contact_relImpl)imail_mass_mailing_list_contact_rel) ;
            }
            mail_mass_mailing_list_contact_relFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
        Imail_mass_mailing_list_contact_rel clientModel = mail_mass_mailing_list_contact_relFeignClient.update(mail_mass_mailing_list_contact_rel.getId(),(mail_mass_mailing_list_contact_relImpl)mail_mass_mailing_list_contact_rel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list_contact_rel.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list_contact_rel, null);
    }


    public void updateBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
        if(mail_mass_mailing_list_contact_rels!=null){
            List<mail_mass_mailing_list_contact_relImpl> list = new ArrayList<mail_mass_mailing_list_contact_relImpl>();
            for(Imail_mass_mailing_list_contact_rel imail_mass_mailing_list_contact_rel :mail_mass_mailing_list_contact_rels){
                list.add((mail_mass_mailing_list_contact_relImpl)imail_mass_mailing_list_contact_rel) ;
            }
            mail_mass_mailing_list_contact_relFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
        Imail_mass_mailing_list_contact_rel clientModel = mail_mass_mailing_list_contact_relFeignClient.get(mail_mass_mailing_list_contact_rel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list_contact_rel.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list_contact_rel, null);
    }


    public Page<Imail_mass_mailing_list_contact_rel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
        Imail_mass_mailing_list_contact_rel clientModel = mail_mass_mailing_list_contact_relFeignClient.getDraft(mail_mass_mailing_list_contact_rel.getId(),(mail_mass_mailing_list_contact_relImpl)mail_mass_mailing_list_contact_rel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list_contact_rel.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list_contact_rel, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mail;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mailClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mailImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mailFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mail] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mailClientServiceImpl implements Imail_mailClientService {

    mail_mailFeignClient mail_mailFeignClient;

    @Autowired
    public mail_mailClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mailFeignClient = nameBuilder.target(mail_mailFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mailFeignClient = nameBuilder.target(mail_mailFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mail createModel() {
		return new mail_mailImpl();
	}


    public void removeBatch(List<Imail_mail> mail_mails){
        if(mail_mails!=null){
            List<mail_mailImpl> list = new ArrayList<mail_mailImpl>();
            for(Imail_mail imail_mail :mail_mails){
                list.add((mail_mailImpl)imail_mail) ;
            }
            mail_mailFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_mail> mail_mails){
        if(mail_mails!=null){
            List<mail_mailImpl> list = new ArrayList<mail_mailImpl>();
            for(Imail_mail imail_mail :mail_mails){
                list.add((mail_mailImpl)imail_mail) ;
            }
            mail_mailFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Imail_mail mail_mail){
        mail_mailFeignClient.remove(mail_mail.getId()) ;
    }


    public void update(Imail_mail mail_mail){
        Imail_mail clientModel = mail_mailFeignClient.update(mail_mail.getId(),(mail_mailImpl)mail_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail.getClass(), false);
        copier.copy(clientModel, mail_mail, null);
    }


    public void createBatch(List<Imail_mail> mail_mails){
        if(mail_mails!=null){
            List<mail_mailImpl> list = new ArrayList<mail_mailImpl>();
            for(Imail_mail imail_mail :mail_mails){
                list.add((mail_mailImpl)imail_mail) ;
            }
            mail_mailFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_mail> fetchDefault(SearchContext context){
        Page<mail_mailImpl> page = this.mail_mailFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_mail mail_mail){
        Imail_mail clientModel = mail_mailFeignClient.get(mail_mail.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail.getClass(), false);
        copier.copy(clientModel, mail_mail, null);
    }


    public void create(Imail_mail mail_mail){
        Imail_mail clientModel = mail_mailFeignClient.create((mail_mailImpl)mail_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail.getClass(), false);
        copier.copy(clientModel, mail_mail, null);
    }


    public Page<Imail_mail> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mail mail_mail){
        Imail_mail clientModel = mail_mailFeignClient.getDraft(mail_mail.getId(),(mail_mailImpl)mail_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mail.getClass(), false);
        copier.copy(clientModel, mail_mail, null);
    }



}


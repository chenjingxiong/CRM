package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_compose_message;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_compose_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_compose_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_compose_messageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_compose_message] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_compose_messageClientServiceImpl implements Imail_compose_messageClientService {

    mail_compose_messageFeignClient mail_compose_messageFeignClient;

    @Autowired
    public mail_compose_messageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_compose_messageFeignClient = nameBuilder.target(mail_compose_messageFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_compose_messageFeignClient = nameBuilder.target(mail_compose_messageFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_compose_message createModel() {
		return new mail_compose_messageImpl();
	}


    public void remove(Imail_compose_message mail_compose_message){
        mail_compose_messageFeignClient.remove(mail_compose_message.getId()) ;
    }


    public void update(Imail_compose_message mail_compose_message){
        Imail_compose_message clientModel = mail_compose_messageFeignClient.update(mail_compose_message.getId(),(mail_compose_messageImpl)mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_compose_message.getClass(), false);
        copier.copy(clientModel, mail_compose_message, null);
    }


    public void removeBatch(List<Imail_compose_message> mail_compose_messages){
        if(mail_compose_messages!=null){
            List<mail_compose_messageImpl> list = new ArrayList<mail_compose_messageImpl>();
            for(Imail_compose_message imail_compose_message :mail_compose_messages){
                list.add((mail_compose_messageImpl)imail_compose_message) ;
            }
            mail_compose_messageFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_compose_message> fetchDefault(SearchContext context){
        Page<mail_compose_messageImpl> page = this.mail_compose_messageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imail_compose_message> mail_compose_messages){
        if(mail_compose_messages!=null){
            List<mail_compose_messageImpl> list = new ArrayList<mail_compose_messageImpl>();
            for(Imail_compose_message imail_compose_message :mail_compose_messages){
                list.add((mail_compose_messageImpl)imail_compose_message) ;
            }
            mail_compose_messageFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_compose_message mail_compose_message){
        Imail_compose_message clientModel = mail_compose_messageFeignClient.create((mail_compose_messageImpl)mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_compose_message.getClass(), false);
        copier.copy(clientModel, mail_compose_message, null);
    }


    public void createBatch(List<Imail_compose_message> mail_compose_messages){
        if(mail_compose_messages!=null){
            List<mail_compose_messageImpl> list = new ArrayList<mail_compose_messageImpl>();
            for(Imail_compose_message imail_compose_message :mail_compose_messages){
                list.add((mail_compose_messageImpl)imail_compose_message) ;
            }
            mail_compose_messageFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_compose_message mail_compose_message){
        Imail_compose_message clientModel = mail_compose_messageFeignClient.get(mail_compose_message.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_compose_message.getClass(), false);
        copier.copy(clientModel, mail_compose_message, null);
    }


    public Page<Imail_compose_message> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_compose_message mail_compose_message){
        Imail_compose_message clientModel = mail_compose_messageFeignClient.getDraft(mail_compose_message.getId(),(mail_compose_messageImpl)mail_compose_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_compose_message.getClass(), false);
        copier.copy(clientModel, mail_compose_message, null);
    }



}


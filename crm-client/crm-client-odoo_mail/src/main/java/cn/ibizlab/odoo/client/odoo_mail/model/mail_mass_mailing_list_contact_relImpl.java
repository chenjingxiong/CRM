package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list_contact_rel;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_mass_mailing_list_contact_rel] 对象
 */
public class mail_mass_mailing_list_contact_relImpl implements Imail_mass_mailing_list_contact_rel,Serializable{

    /**
     * 联系人人数
     */
    public Integer contact_count;

    @JsonIgnore
    public boolean contact_countDirtyFlag;
    
    /**
     * 联系
     */
    public Integer contact_id;

    @JsonIgnore
    public boolean contact_idDirtyFlag;
    
    /**
     * 联系
     */
    public String contact_id_text;

    @JsonIgnore
    public boolean contact_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 黑名单
     */
    public String is_blacklisted;

    @JsonIgnore
    public boolean is_blacklistedDirtyFlag;
    
    /**
     * 邮件列表
     */
    public Integer list_id;

    @JsonIgnore
    public boolean list_idDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String list_id_text;

    @JsonIgnore
    public boolean list_id_textDirtyFlag;
    
    /**
     * 被退回
     */
    public Integer message_bounce;

    @JsonIgnore
    public boolean message_bounceDirtyFlag;
    
    /**
     * 退出
     */
    public String opt_out;

    @JsonIgnore
    public boolean opt_outDirtyFlag;
    
    /**
     * 取消订阅日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp unsubscription_date;

    @JsonIgnore
    public boolean unsubscription_dateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [联系人人数]
     */
    @JsonProperty("contact_count")
    public Integer getContact_count(){
        return this.contact_count ;
    }

    /**
     * 设置 [联系人人数]
     */
    @JsonProperty("contact_count")
    public void setContact_count(Integer  contact_count){
        this.contact_count = contact_count ;
        this.contact_countDirtyFlag = true ;
    }

     /**
     * 获取 [联系人人数]脏标记
     */
    @JsonIgnore
    public boolean getContact_countDirtyFlag(){
        return this.contact_countDirtyFlag ;
    }   

    /**
     * 获取 [联系]
     */
    @JsonProperty("contact_id")
    public Integer getContact_id(){
        return this.contact_id ;
    }

    /**
     * 设置 [联系]
     */
    @JsonProperty("contact_id")
    public void setContact_id(Integer  contact_id){
        this.contact_id = contact_id ;
        this.contact_idDirtyFlag = true ;
    }

     /**
     * 获取 [联系]脏标记
     */
    @JsonIgnore
    public boolean getContact_idDirtyFlag(){
        return this.contact_idDirtyFlag ;
    }   

    /**
     * 获取 [联系]
     */
    @JsonProperty("contact_id_text")
    public String getContact_id_text(){
        return this.contact_id_text ;
    }

    /**
     * 设置 [联系]
     */
    @JsonProperty("contact_id_text")
    public void setContact_id_text(String  contact_id_text){
        this.contact_id_text = contact_id_text ;
        this.contact_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [联系]脏标记
     */
    @JsonIgnore
    public boolean getContact_id_textDirtyFlag(){
        return this.contact_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

     /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("list_id")
    public Integer getList_id(){
        return this.list_id ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("list_id")
    public void setList_id(Integer  list_id){
        this.list_id = list_id ;
        this.list_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getList_idDirtyFlag(){
        return this.list_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("list_id_text")
    public String getList_id_text(){
        return this.list_id_text ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("list_id_text")
    public void setList_id_text(String  list_id_text){
        this.list_id_text = list_id_text ;
        this.list_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getList_id_textDirtyFlag(){
        return this.list_id_textDirtyFlag ;
    }   

    /**
     * 获取 [被退回]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return this.message_bounce ;
    }

    /**
     * 设置 [被退回]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

     /**
     * 获取 [被退回]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return this.message_bounceDirtyFlag ;
    }   

    /**
     * 获取 [退出]
     */
    @JsonProperty("opt_out")
    public String getOpt_out(){
        return this.opt_out ;
    }

    /**
     * 设置 [退出]
     */
    @JsonProperty("opt_out")
    public void setOpt_out(String  opt_out){
        this.opt_out = opt_out ;
        this.opt_outDirtyFlag = true ;
    }

     /**
     * 获取 [退出]脏标记
     */
    @JsonIgnore
    public boolean getOpt_outDirtyFlag(){
        return this.opt_outDirtyFlag ;
    }   

    /**
     * 获取 [取消订阅日期]
     */
    @JsonProperty("unsubscription_date")
    public Timestamp getUnsubscription_date(){
        return this.unsubscription_date ;
    }

    /**
     * 设置 [取消订阅日期]
     */
    @JsonProperty("unsubscription_date")
    public void setUnsubscription_date(Timestamp  unsubscription_date){
        this.unsubscription_date = unsubscription_date ;
        this.unsubscription_dateDirtyFlag = true ;
    }

     /**
     * 获取 [取消订阅日期]脏标记
     */
    @JsonIgnore
    public boolean getUnsubscription_dateDirtyFlag(){
        return this.unsubscription_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

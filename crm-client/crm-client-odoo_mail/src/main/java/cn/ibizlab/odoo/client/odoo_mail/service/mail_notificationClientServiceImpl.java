package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_notificationClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_notificationImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_notificationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_notification] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_notificationClientServiceImpl implements Imail_notificationClientService {

    mail_notificationFeignClient mail_notificationFeignClient;

    @Autowired
    public mail_notificationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_notificationFeignClient = nameBuilder.target(mail_notificationFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_notificationFeignClient = nameBuilder.target(mail_notificationFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_notification createModel() {
		return new mail_notificationImpl();
	}


    public void createBatch(List<Imail_notification> mail_notifications){
        if(mail_notifications!=null){
            List<mail_notificationImpl> list = new ArrayList<mail_notificationImpl>();
            for(Imail_notification imail_notification :mail_notifications){
                list.add((mail_notificationImpl)imail_notification) ;
            }
            mail_notificationFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_notification> mail_notifications){
        if(mail_notifications!=null){
            List<mail_notificationImpl> list = new ArrayList<mail_notificationImpl>();
            for(Imail_notification imail_notification :mail_notifications){
                list.add((mail_notificationImpl)imail_notification) ;
            }
            mail_notificationFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_notification mail_notification){
        Imail_notification clientModel = mail_notificationFeignClient.create((mail_notificationImpl)mail_notification) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_notification.getClass(), false);
        copier.copy(clientModel, mail_notification, null);
    }


    public Page<Imail_notification> fetchDefault(SearchContext context){
        Page<mail_notificationImpl> page = this.mail_notificationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imail_notification mail_notification){
        mail_notificationFeignClient.remove(mail_notification.getId()) ;
    }


    public void get(Imail_notification mail_notification){
        Imail_notification clientModel = mail_notificationFeignClient.get(mail_notification.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_notification.getClass(), false);
        copier.copy(clientModel, mail_notification, null);
    }


    public void update(Imail_notification mail_notification){
        Imail_notification clientModel = mail_notificationFeignClient.update(mail_notification.getId(),(mail_notificationImpl)mail_notification) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_notification.getClass(), false);
        copier.copy(clientModel, mail_notification, null);
    }


    public void removeBatch(List<Imail_notification> mail_notifications){
        if(mail_notifications!=null){
            List<mail_notificationImpl> list = new ArrayList<mail_notificationImpl>();
            for(Imail_notification imail_notification :mail_notifications){
                list.add((mail_notificationImpl)imail_notification) ;
            }
            mail_notificationFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_notification> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_notification mail_notification){
        Imail_notification clientModel = mail_notificationFeignClient.getDraft(mail_notification.getId(),(mail_notificationImpl)mail_notification) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_notification.getClass(), false);
        copier.copy(clientModel, mail_notification, null);
    }



}


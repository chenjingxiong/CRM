package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklistImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_blacklist] 服务对象接口
 */
public interface mail_blacklistFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklists/fetchdefault")
    public Page<mail_blacklistImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklists/createbatch")
    public mail_blacklistImpl createBatch(@RequestBody List<mail_blacklistImpl> mail_blacklists);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklists/{id}")
    public mail_blacklistImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklists/removebatch")
    public mail_blacklistImpl removeBatch(@RequestBody List<mail_blacklistImpl> mail_blacklists);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklists/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklists/{id}")
    public mail_blacklistImpl update(@PathVariable("id") Integer id,@RequestBody mail_blacklistImpl mail_blacklist);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklists")
    public mail_blacklistImpl create(@RequestBody mail_blacklistImpl mail_blacklist);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklists/updatebatch")
    public mail_blacklistImpl updateBatch(@RequestBody List<mail_blacklistImpl> mail_blacklists);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklists/select")
    public Page<mail_blacklistImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklists/{id}/getdraft")
    public mail_blacklistImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_blacklistImpl mail_blacklist);



}

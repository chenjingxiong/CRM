package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_statistics_reportClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_statistics_reportImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_statistics_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_statistics_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_statistics_reportClientServiceImpl implements Imail_statistics_reportClientService {

    mail_statistics_reportFeignClient mail_statistics_reportFeignClient;

    @Autowired
    public mail_statistics_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_statistics_reportFeignClient = nameBuilder.target(mail_statistics_reportFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_statistics_reportFeignClient = nameBuilder.target(mail_statistics_reportFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_statistics_report createModel() {
		return new mail_statistics_reportImpl();
	}


    public void get(Imail_statistics_report mail_statistics_report){
        Imail_statistics_report clientModel = mail_statistics_reportFeignClient.get(mail_statistics_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_statistics_report.getClass(), false);
        copier.copy(clientModel, mail_statistics_report, null);
    }


    public void remove(Imail_statistics_report mail_statistics_report){
        mail_statistics_reportFeignClient.remove(mail_statistics_report.getId()) ;
    }


    public void updateBatch(List<Imail_statistics_report> mail_statistics_reports){
        if(mail_statistics_reports!=null){
            List<mail_statistics_reportImpl> list = new ArrayList<mail_statistics_reportImpl>();
            for(Imail_statistics_report imail_statistics_report :mail_statistics_reports){
                list.add((mail_statistics_reportImpl)imail_statistics_report) ;
            }
            mail_statistics_reportFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_statistics_report> mail_statistics_reports){
        if(mail_statistics_reports!=null){
            List<mail_statistics_reportImpl> list = new ArrayList<mail_statistics_reportImpl>();
            for(Imail_statistics_report imail_statistics_report :mail_statistics_reports){
                list.add((mail_statistics_reportImpl)imail_statistics_report) ;
            }
            mail_statistics_reportFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imail_statistics_report mail_statistics_report){
        Imail_statistics_report clientModel = mail_statistics_reportFeignClient.create((mail_statistics_reportImpl)mail_statistics_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_statistics_report.getClass(), false);
        copier.copy(clientModel, mail_statistics_report, null);
    }


    public void createBatch(List<Imail_statistics_report> mail_statistics_reports){
        if(mail_statistics_reports!=null){
            List<mail_statistics_reportImpl> list = new ArrayList<mail_statistics_reportImpl>();
            for(Imail_statistics_report imail_statistics_report :mail_statistics_reports){
                list.add((mail_statistics_reportImpl)imail_statistics_report) ;
            }
            mail_statistics_reportFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_statistics_report mail_statistics_report){
        Imail_statistics_report clientModel = mail_statistics_reportFeignClient.update(mail_statistics_report.getId(),(mail_statistics_reportImpl)mail_statistics_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_statistics_report.getClass(), false);
        copier.copy(clientModel, mail_statistics_report, null);
    }


    public Page<Imail_statistics_report> fetchDefault(SearchContext context){
        Page<mail_statistics_reportImpl> page = this.mail_statistics_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imail_statistics_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_statistics_report mail_statistics_report){
        Imail_statistics_report clientModel = mail_statistics_reportFeignClient.getDraft(mail_statistics_report.getId(),(mail_statistics_reportImpl)mail_statistics_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_statistics_report.getClass(), false);
        copier.copy(clientModel, mail_statistics_report, null);
    }



}


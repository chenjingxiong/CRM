package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_stage;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
public interface mail_mass_mailing_stageFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_stages")
    public mail_mass_mailing_stageImpl create(@RequestBody mail_mass_mailing_stageImpl mail_mass_mailing_stage);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_stages/updatebatch")
    public mail_mass_mailing_stageImpl updateBatch(@RequestBody List<mail_mass_mailing_stageImpl> mail_mass_mailing_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_stages/createbatch")
    public mail_mass_mailing_stageImpl createBatch(@RequestBody List<mail_mass_mailing_stageImpl> mail_mass_mailing_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_stages/removebatch")
    public mail_mass_mailing_stageImpl removeBatch(@RequestBody List<mail_mass_mailing_stageImpl> mail_mass_mailing_stages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_stages/{id}")
    public mail_mass_mailing_stageImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_stageImpl mail_mass_mailing_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_stages/{id}")
    public mail_mass_mailing_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_stages/fetchdefault")
    public Page<mail_mass_mailing_stageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_stages/select")
    public Page<mail_mass_mailing_stageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_stages/{id}/getdraft")
    public mail_mass_mailing_stageImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_stageImpl mail_mass_mailing_stage);



}

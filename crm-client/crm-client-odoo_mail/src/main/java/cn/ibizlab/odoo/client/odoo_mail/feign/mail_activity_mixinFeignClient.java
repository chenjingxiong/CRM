package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_activity_mixin;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
public interface mail_activity_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_mixins")
    public mail_activity_mixinImpl create(@RequestBody mail_activity_mixinImpl mail_activity_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_mixins/fetchdefault")
    public Page<mail_activity_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_mixins/{id}")
    public mail_activity_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_activity_mixins/createbatch")
    public mail_activity_mixinImpl createBatch(@RequestBody List<mail_activity_mixinImpl> mail_activity_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activity_mixins/{id}")
    public mail_activity_mixinImpl update(@PathVariable("id") Integer id,@RequestBody mail_activity_mixinImpl mail_activity_mixin);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_activity_mixins/updatebatch")
    public mail_activity_mixinImpl updateBatch(@RequestBody List<mail_activity_mixinImpl> mail_activity_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activity_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_activity_mixins/removebatch")
    public mail_activity_mixinImpl removeBatch(@RequestBody List<mail_activity_mixinImpl> mail_activity_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_mixins/select")
    public Page<mail_activity_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_activity_mixins/{id}/getdraft")
    public mail_activity_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_activity_mixinImpl mail_activity_mixin);



}

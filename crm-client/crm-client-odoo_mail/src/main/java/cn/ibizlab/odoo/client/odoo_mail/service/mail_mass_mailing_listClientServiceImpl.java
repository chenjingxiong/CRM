package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_listClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_listImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_listFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_listClientServiceImpl implements Imail_mass_mailing_listClientService {

    mail_mass_mailing_listFeignClient mail_mass_mailing_listFeignClient;

    @Autowired
    public mail_mass_mailing_listClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_listFeignClient = nameBuilder.target(mail_mass_mailing_listFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_listFeignClient = nameBuilder.target(mail_mass_mailing_listFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_list createModel() {
		return new mail_mass_mailing_listImpl();
	}


    public void update(Imail_mass_mailing_list mail_mass_mailing_list){
        Imail_mass_mailing_list clientModel = mail_mass_mailing_listFeignClient.update(mail_mass_mailing_list.getId(),(mail_mass_mailing_listImpl)mail_mass_mailing_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list, null);
    }


    public void create(Imail_mass_mailing_list mail_mass_mailing_list){
        Imail_mass_mailing_list clientModel = mail_mass_mailing_listFeignClient.create((mail_mass_mailing_listImpl)mail_mass_mailing_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list, null);
    }


    public void createBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
        if(mail_mass_mailing_lists!=null){
            List<mail_mass_mailing_listImpl> list = new ArrayList<mail_mass_mailing_listImpl>();
            for(Imail_mass_mailing_list imail_mass_mailing_list :mail_mass_mailing_lists){
                list.add((mail_mass_mailing_listImpl)imail_mass_mailing_list) ;
            }
            mail_mass_mailing_listFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imail_mass_mailing_list mail_mass_mailing_list){
        mail_mass_mailing_listFeignClient.remove(mail_mass_mailing_list.getId()) ;
    }


    public Page<Imail_mass_mailing_list> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_listImpl> page = this.mail_mass_mailing_listFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
        if(mail_mass_mailing_lists!=null){
            List<mail_mass_mailing_listImpl> list = new ArrayList<mail_mass_mailing_listImpl>();
            for(Imail_mass_mailing_list imail_mass_mailing_list :mail_mass_mailing_lists){
                list.add((mail_mass_mailing_listImpl)imail_mass_mailing_list) ;
            }
            mail_mass_mailing_listFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_mass_mailing_list> mail_mass_mailing_lists){
        if(mail_mass_mailing_lists!=null){
            List<mail_mass_mailing_listImpl> list = new ArrayList<mail_mass_mailing_listImpl>();
            for(Imail_mass_mailing_list imail_mass_mailing_list :mail_mass_mailing_lists){
                list.add((mail_mass_mailing_listImpl)imail_mass_mailing_list) ;
            }
            mail_mass_mailing_listFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_list mail_mass_mailing_list){
        Imail_mass_mailing_list clientModel = mail_mass_mailing_listFeignClient.get(mail_mass_mailing_list.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list, null);
    }


    public Page<Imail_mass_mailing_list> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_list mail_mass_mailing_list){
        Imail_mass_mailing_list clientModel = mail_mass_mailing_listFeignClient.getDraft(mail_mass_mailing_list.getId(),(mail_mass_mailing_listImpl)mail_mass_mailing_list) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_list.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_list, null);
    }



}


package cn.ibizlab.odoo.client.odoo_project.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproject_tags;
import cn.ibizlab.odoo.client.odoo_project.model.project_tagsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[project_tags] 服务对象接口
 */
public interface project_tagsFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tags/fetchdefault")
    public Page<project_tagsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tags/{id}")
    public project_tagsImpl update(@PathVariable("id") Integer id,@RequestBody project_tagsImpl project_tags);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tags/removebatch")
    public project_tagsImpl removeBatch(@RequestBody List<project_tagsImpl> project_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tags")
    public project_tagsImpl create(@RequestBody project_tagsImpl project_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tags/createbatch")
    public project_tagsImpl createBatch(@RequestBody List<project_tagsImpl> project_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tags/{id}")
    public project_tagsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tags/updatebatch")
    public project_tagsImpl updateBatch(@RequestBody List<project_tagsImpl> project_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tags/select")
    public Page<project_tagsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tags/{id}/getdraft")
    public project_tagsImpl getDraft(@PathVariable("id") Integer id,@RequestBody project_tagsImpl project_tags);



}

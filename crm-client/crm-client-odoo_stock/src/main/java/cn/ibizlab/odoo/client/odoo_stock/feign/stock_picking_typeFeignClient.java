package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_picking_type;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_picking_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_picking_type] 服务对象接口
 */
public interface stock_picking_typeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_picking_types/createbatch")
    public stock_picking_typeImpl createBatch(@RequestBody List<stock_picking_typeImpl> stock_picking_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_picking_types/fetchdefault")
    public Page<stock_picking_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_picking_types/removebatch")
    public stock_picking_typeImpl removeBatch(@RequestBody List<stock_picking_typeImpl> stock_picking_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_picking_types")
    public stock_picking_typeImpl create(@RequestBody stock_picking_typeImpl stock_picking_type);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_picking_types/{id}")
    public stock_picking_typeImpl update(@PathVariable("id") Integer id,@RequestBody stock_picking_typeImpl stock_picking_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_picking_types/{id}")
    public stock_picking_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_picking_types/updatebatch")
    public stock_picking_typeImpl updateBatch(@RequestBody List<stock_picking_typeImpl> stock_picking_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_picking_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_picking_types/select")
    public Page<stock_picking_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_picking_types/{id}/getdraft")
    public stock_picking_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_picking_typeImpl stock_picking_type);



}

package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_picking;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_pickingClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_pickingImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_pickingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_picking] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_pickingClientServiceImpl implements Istock_pickingClientService {

    stock_pickingFeignClient stock_pickingFeignClient;

    @Autowired
    public stock_pickingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_pickingFeignClient = nameBuilder.target(stock_pickingFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_pickingFeignClient = nameBuilder.target(stock_pickingFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_picking createModel() {
		return new stock_pickingImpl();
	}


    public void remove(Istock_picking stock_picking){
        stock_pickingFeignClient.remove(stock_picking.getId()) ;
    }


    public void create(Istock_picking stock_picking){
        Istock_picking clientModel = stock_pickingFeignClient.create((stock_pickingImpl)stock_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking.getClass(), false);
        copier.copy(clientModel, stock_picking, null);
    }


    public void updateBatch(List<Istock_picking> stock_pickings){
        if(stock_pickings!=null){
            List<stock_pickingImpl> list = new ArrayList<stock_pickingImpl>();
            for(Istock_picking istock_picking :stock_pickings){
                list.add((stock_pickingImpl)istock_picking) ;
            }
            stock_pickingFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_picking stock_picking){
        Istock_picking clientModel = stock_pickingFeignClient.get(stock_picking.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking.getClass(), false);
        copier.copy(clientModel, stock_picking, null);
    }


    public void removeBatch(List<Istock_picking> stock_pickings){
        if(stock_pickings!=null){
            List<stock_pickingImpl> list = new ArrayList<stock_pickingImpl>();
            for(Istock_picking istock_picking :stock_pickings){
                list.add((stock_pickingImpl)istock_picking) ;
            }
            stock_pickingFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_picking stock_picking){
        Istock_picking clientModel = stock_pickingFeignClient.update(stock_picking.getId(),(stock_pickingImpl)stock_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking.getClass(), false);
        copier.copy(clientModel, stock_picking, null);
    }


    public void createBatch(List<Istock_picking> stock_pickings){
        if(stock_pickings!=null){
            List<stock_pickingImpl> list = new ArrayList<stock_pickingImpl>();
            for(Istock_picking istock_picking :stock_pickings){
                list.add((stock_pickingImpl)istock_picking) ;
            }
            stock_pickingFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_picking> fetchDefault(SearchContext context){
        Page<stock_pickingImpl> page = this.stock_pickingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Istock_picking> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_picking stock_picking){
        Istock_picking clientModel = stock_pickingFeignClient.getDraft(stock_picking.getId(),(stock_pickingImpl)stock_picking) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking.getClass(), false);
        copier.copy(clientModel, stock_picking, null);
    }



}


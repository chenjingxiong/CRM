package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_track_confirmation;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_track_confirmationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
public interface stock_track_confirmationFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_confirmations/updatebatch")
    public stock_track_confirmationImpl updateBatch(@RequestBody List<stock_track_confirmationImpl> stock_track_confirmations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_confirmations/createbatch")
    public stock_track_confirmationImpl createBatch(@RequestBody List<stock_track_confirmationImpl> stock_track_confirmations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_confirmations/removebatch")
    public stock_track_confirmationImpl removeBatch(@RequestBody List<stock_track_confirmationImpl> stock_track_confirmations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_confirmations/fetchdefault")
    public Page<stock_track_confirmationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_confirmations")
    public stock_track_confirmationImpl create(@RequestBody stock_track_confirmationImpl stock_track_confirmation);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_confirmations/{id}")
    public stock_track_confirmationImpl update(@PathVariable("id") Integer id,@RequestBody stock_track_confirmationImpl stock_track_confirmation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_confirmations/{id}")
    public stock_track_confirmationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_confirmations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_confirmations/select")
    public Page<stock_track_confirmationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_confirmations/{id}/getdraft")
    public stock_track_confirmationImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_track_confirmationImpl stock_track_confirmation);



}

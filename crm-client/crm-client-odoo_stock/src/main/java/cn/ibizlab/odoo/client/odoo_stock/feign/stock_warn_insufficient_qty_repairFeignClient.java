package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_repairImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warn_insufficient_qty_repair] 服务对象接口
 */
public interface stock_warn_insufficient_qty_repairFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{id}")
    public stock_warn_insufficient_qty_repairImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_repairs")
    public stock_warn_insufficient_qty_repairImpl create(@RequestBody stock_warn_insufficient_qty_repairImpl stock_warn_insufficient_qty_repair);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/createbatch")
    public stock_warn_insufficient_qty_repairImpl createBatch(@RequestBody List<stock_warn_insufficient_qty_repairImpl> stock_warn_insufficient_qty_repairs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/removebatch")
    public stock_warn_insufficient_qty_repairImpl removeBatch(@RequestBody List<stock_warn_insufficient_qty_repairImpl> stock_warn_insufficient_qty_repairs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/fetchdefault")
    public Page<stock_warn_insufficient_qty_repairImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/updatebatch")
    public stock_warn_insufficient_qty_repairImpl updateBatch(@RequestBody List<stock_warn_insufficient_qty_repairImpl> stock_warn_insufficient_qty_repairs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{id}")
    public stock_warn_insufficient_qty_repairImpl update(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_repairImpl stock_warn_insufficient_qty_repair);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/select")
    public Page<stock_warn_insufficient_qty_repairImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{id}/getdraft")
    public stock_warn_insufficient_qty_repairImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_repairImpl stock_warn_insufficient_qty_repair);



}

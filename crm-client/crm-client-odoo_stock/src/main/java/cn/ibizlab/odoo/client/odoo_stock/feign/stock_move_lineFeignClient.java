package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_move_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_move_line] 服务对象接口
 */
public interface stock_move_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_move_lines/removebatch")
    public stock_move_lineImpl removeBatch(@RequestBody List<stock_move_lineImpl> stock_move_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_move_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_move_lines")
    public stock_move_lineImpl create(@RequestBody stock_move_lineImpl stock_move_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_move_lines/createbatch")
    public stock_move_lineImpl createBatch(@RequestBody List<stock_move_lineImpl> stock_move_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_move_lines/fetchdefault")
    public Page<stock_move_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_move_lines/updatebatch")
    public stock_move_lineImpl updateBatch(@RequestBody List<stock_move_lineImpl> stock_move_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_move_lines/{id}")
    public stock_move_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_move_lines/{id}")
    public stock_move_lineImpl update(@PathVariable("id") Integer id,@RequestBody stock_move_lineImpl stock_move_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_move_lines/select")
    public Page<stock_move_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_move_lines/{id}/getdraft")
    public stock_move_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_move_lineImpl stock_move_line);



}

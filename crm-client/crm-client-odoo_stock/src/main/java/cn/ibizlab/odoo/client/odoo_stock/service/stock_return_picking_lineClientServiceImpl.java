package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking_line;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_return_picking_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_picking_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_return_picking_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_return_picking_lineClientServiceImpl implements Istock_return_picking_lineClientService {

    stock_return_picking_lineFeignClient stock_return_picking_lineFeignClient;

    @Autowired
    public stock_return_picking_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_return_picking_lineFeignClient = nameBuilder.target(stock_return_picking_lineFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_return_picking_lineFeignClient = nameBuilder.target(stock_return_picking_lineFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_return_picking_line createModel() {
		return new stock_return_picking_lineImpl();
	}


    public void removeBatch(List<Istock_return_picking_line> stock_return_picking_lines){
        if(stock_return_picking_lines!=null){
            List<stock_return_picking_lineImpl> list = new ArrayList<stock_return_picking_lineImpl>();
            for(Istock_return_picking_line istock_return_picking_line :stock_return_picking_lines){
                list.add((stock_return_picking_lineImpl)istock_return_picking_line) ;
            }
            stock_return_picking_lineFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Istock_return_picking_line stock_return_picking_line){
        stock_return_picking_lineFeignClient.remove(stock_return_picking_line.getId()) ;
    }


    public void createBatch(List<Istock_return_picking_line> stock_return_picking_lines){
        if(stock_return_picking_lines!=null){
            List<stock_return_picking_lineImpl> list = new ArrayList<stock_return_picking_lineImpl>();
            for(Istock_return_picking_line istock_return_picking_line :stock_return_picking_lines){
                list.add((stock_return_picking_lineImpl)istock_return_picking_line) ;
            }
            stock_return_picking_lineFeignClient.createBatch(list) ;
        }
    }


    public void get(Istock_return_picking_line stock_return_picking_line){
        Istock_return_picking_line clientModel = stock_return_picking_lineFeignClient.get(stock_return_picking_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking_line.getClass(), false);
        copier.copy(clientModel, stock_return_picking_line, null);
    }


    public void update(Istock_return_picking_line stock_return_picking_line){
        Istock_return_picking_line clientModel = stock_return_picking_lineFeignClient.update(stock_return_picking_line.getId(),(stock_return_picking_lineImpl)stock_return_picking_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking_line.getClass(), false);
        copier.copy(clientModel, stock_return_picking_line, null);
    }


    public void updateBatch(List<Istock_return_picking_line> stock_return_picking_lines){
        if(stock_return_picking_lines!=null){
            List<stock_return_picking_lineImpl> list = new ArrayList<stock_return_picking_lineImpl>();
            for(Istock_return_picking_line istock_return_picking_line :stock_return_picking_lines){
                list.add((stock_return_picking_lineImpl)istock_return_picking_line) ;
            }
            stock_return_picking_lineFeignClient.updateBatch(list) ;
        }
    }


    public Page<Istock_return_picking_line> fetchDefault(SearchContext context){
        Page<stock_return_picking_lineImpl> page = this.stock_return_picking_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_return_picking_line stock_return_picking_line){
        Istock_return_picking_line clientModel = stock_return_picking_lineFeignClient.create((stock_return_picking_lineImpl)stock_return_picking_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking_line.getClass(), false);
        copier.copy(clientModel, stock_return_picking_line, null);
    }


    public Page<Istock_return_picking_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_return_picking_line stock_return_picking_line){
        Istock_return_picking_line clientModel = stock_return_picking_lineFeignClient.getDraft(stock_return_picking_line.getId(),(stock_return_picking_lineImpl)stock_return_picking_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_return_picking_line.getClass(), false);
        copier.copy(clientModel, stock_return_picking_line, null);
    }



}


package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_package_destination;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_package_destinationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_destinationImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_package_destinationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_package_destination] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_package_destinationClientServiceImpl implements Istock_package_destinationClientService {

    stock_package_destinationFeignClient stock_package_destinationFeignClient;

    @Autowired
    public stock_package_destinationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_package_destinationFeignClient = nameBuilder.target(stock_package_destinationFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_package_destinationFeignClient = nameBuilder.target(stock_package_destinationFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_package_destination createModel() {
		return new stock_package_destinationImpl();
	}


    public Page<Istock_package_destination> fetchDefault(SearchContext context){
        Page<stock_package_destinationImpl> page = this.stock_package_destinationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Istock_package_destination stock_package_destination){
        Istock_package_destination clientModel = stock_package_destinationFeignClient.update(stock_package_destination.getId(),(stock_package_destinationImpl)stock_package_destination) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_destination.getClass(), false);
        copier.copy(clientModel, stock_package_destination, null);
    }


    public void removeBatch(List<Istock_package_destination> stock_package_destinations){
        if(stock_package_destinations!=null){
            List<stock_package_destinationImpl> list = new ArrayList<stock_package_destinationImpl>();
            for(Istock_package_destination istock_package_destination :stock_package_destinations){
                list.add((stock_package_destinationImpl)istock_package_destination) ;
            }
            stock_package_destinationFeignClient.removeBatch(list) ;
        }
    }


    public void get(Istock_package_destination stock_package_destination){
        Istock_package_destination clientModel = stock_package_destinationFeignClient.get(stock_package_destination.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_destination.getClass(), false);
        copier.copy(clientModel, stock_package_destination, null);
    }


    public void create(Istock_package_destination stock_package_destination){
        Istock_package_destination clientModel = stock_package_destinationFeignClient.create((stock_package_destinationImpl)stock_package_destination) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_destination.getClass(), false);
        copier.copy(clientModel, stock_package_destination, null);
    }


    public void remove(Istock_package_destination stock_package_destination){
        stock_package_destinationFeignClient.remove(stock_package_destination.getId()) ;
    }


    public void updateBatch(List<Istock_package_destination> stock_package_destinations){
        if(stock_package_destinations!=null){
            List<stock_package_destinationImpl> list = new ArrayList<stock_package_destinationImpl>();
            for(Istock_package_destination istock_package_destination :stock_package_destinations){
                list.add((stock_package_destinationImpl)istock_package_destination) ;
            }
            stock_package_destinationFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Istock_package_destination> stock_package_destinations){
        if(stock_package_destinations!=null){
            List<stock_package_destinationImpl> list = new ArrayList<stock_package_destinationImpl>();
            for(Istock_package_destination istock_package_destination :stock_package_destinations){
                list.add((stock_package_destinationImpl)istock_package_destination) ;
            }
            stock_package_destinationFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_package_destination> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_package_destination stock_package_destination){
        Istock_package_destination clientModel = stock_package_destinationFeignClient.getDraft(stock_package_destination.getId(),(stock_package_destinationImpl)stock_package_destination) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_destination.getClass(), false);
        copier.copy(clientModel, stock_package_destination, null);
    }



}


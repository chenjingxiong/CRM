package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_change_product_qty;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_change_product_qtyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_product_qtyImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_change_product_qtyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_change_product_qtyClientServiceImpl implements Istock_change_product_qtyClientService {

    stock_change_product_qtyFeignClient stock_change_product_qtyFeignClient;

    @Autowired
    public stock_change_product_qtyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_change_product_qtyFeignClient = nameBuilder.target(stock_change_product_qtyFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_change_product_qtyFeignClient = nameBuilder.target(stock_change_product_qtyFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_change_product_qty createModel() {
		return new stock_change_product_qtyImpl();
	}


    public void createBatch(List<Istock_change_product_qty> stock_change_product_qties){
        if(stock_change_product_qties!=null){
            List<stock_change_product_qtyImpl> list = new ArrayList<stock_change_product_qtyImpl>();
            for(Istock_change_product_qty istock_change_product_qty :stock_change_product_qties){
                list.add((stock_change_product_qtyImpl)istock_change_product_qty) ;
            }
            stock_change_product_qtyFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_change_product_qty> stock_change_product_qties){
        if(stock_change_product_qties!=null){
            List<stock_change_product_qtyImpl> list = new ArrayList<stock_change_product_qtyImpl>();
            for(Istock_change_product_qty istock_change_product_qty :stock_change_product_qties){
                list.add((stock_change_product_qtyImpl)istock_change_product_qty) ;
            }
            stock_change_product_qtyFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_change_product_qty stock_change_product_qty){
        Istock_change_product_qty clientModel = stock_change_product_qtyFeignClient.update(stock_change_product_qty.getId(),(stock_change_product_qtyImpl)stock_change_product_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_product_qty.getClass(), false);
        copier.copy(clientModel, stock_change_product_qty, null);
    }


    public Page<Istock_change_product_qty> fetchDefault(SearchContext context){
        Page<stock_change_product_qtyImpl> page = this.stock_change_product_qtyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_change_product_qty stock_change_product_qty){
        stock_change_product_qtyFeignClient.remove(stock_change_product_qty.getId()) ;
    }


    public void updateBatch(List<Istock_change_product_qty> stock_change_product_qties){
        if(stock_change_product_qties!=null){
            List<stock_change_product_qtyImpl> list = new ArrayList<stock_change_product_qtyImpl>();
            for(Istock_change_product_qty istock_change_product_qty :stock_change_product_qties){
                list.add((stock_change_product_qtyImpl)istock_change_product_qty) ;
            }
            stock_change_product_qtyFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_change_product_qty stock_change_product_qty){
        Istock_change_product_qty clientModel = stock_change_product_qtyFeignClient.get(stock_change_product_qty.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_product_qty.getClass(), false);
        copier.copy(clientModel, stock_change_product_qty, null);
    }


    public void create(Istock_change_product_qty stock_change_product_qty){
        Istock_change_product_qty clientModel = stock_change_product_qtyFeignClient.create((stock_change_product_qtyImpl)stock_change_product_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_product_qty.getClass(), false);
        copier.copy(clientModel, stock_change_product_qty, null);
    }


    public Page<Istock_change_product_qty> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_change_product_qty stock_change_product_qty){
        Istock_change_product_qty clientModel = stock_change_product_qtyFeignClient.getDraft(stock_change_product_qty.getId(),(stock_change_product_qtyImpl)stock_change_product_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_change_product_qty.getClass(), false);
        copier.copy(clientModel, stock_change_product_qty, null);
    }



}


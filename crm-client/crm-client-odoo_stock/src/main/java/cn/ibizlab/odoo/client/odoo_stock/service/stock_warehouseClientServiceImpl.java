package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warehouseClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouseImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warehouseFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warehouse] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warehouseClientServiceImpl implements Istock_warehouseClientService {

    stock_warehouseFeignClient stock_warehouseFeignClient;

    @Autowired
    public stock_warehouseClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warehouseFeignClient = nameBuilder.target(stock_warehouseFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warehouseFeignClient = nameBuilder.target(stock_warehouseFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warehouse createModel() {
		return new stock_warehouseImpl();
	}


    public void updateBatch(List<Istock_warehouse> stock_warehouses){
        if(stock_warehouses!=null){
            List<stock_warehouseImpl> list = new ArrayList<stock_warehouseImpl>();
            for(Istock_warehouse istock_warehouse :stock_warehouses){
                list.add((stock_warehouseImpl)istock_warehouse) ;
            }
            stock_warehouseFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_warehouse stock_warehouse){
        stock_warehouseFeignClient.remove(stock_warehouse.getId()) ;
    }


    public Page<Istock_warehouse> fetchDefault(SearchContext context){
        Page<stock_warehouseImpl> page = this.stock_warehouseFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Istock_warehouse stock_warehouse){
        Istock_warehouse clientModel = stock_warehouseFeignClient.update(stock_warehouse.getId(),(stock_warehouseImpl)stock_warehouse) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse.getClass(), false);
        copier.copy(clientModel, stock_warehouse, null);
    }


    public void create(Istock_warehouse stock_warehouse){
        Istock_warehouse clientModel = stock_warehouseFeignClient.create((stock_warehouseImpl)stock_warehouse) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse.getClass(), false);
        copier.copy(clientModel, stock_warehouse, null);
    }


    public void createBatch(List<Istock_warehouse> stock_warehouses){
        if(stock_warehouses!=null){
            List<stock_warehouseImpl> list = new ArrayList<stock_warehouseImpl>();
            for(Istock_warehouse istock_warehouse :stock_warehouses){
                list.add((stock_warehouseImpl)istock_warehouse) ;
            }
            stock_warehouseFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_warehouse> stock_warehouses){
        if(stock_warehouses!=null){
            List<stock_warehouseImpl> list = new ArrayList<stock_warehouseImpl>();
            for(Istock_warehouse istock_warehouse :stock_warehouses){
                list.add((stock_warehouseImpl)istock_warehouse) ;
            }
            stock_warehouseFeignClient.removeBatch(list) ;
        }
    }


    public void get(Istock_warehouse stock_warehouse){
        Istock_warehouse clientModel = stock_warehouseFeignClient.get(stock_warehouse.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse.getClass(), false);
        copier.copy(clientModel, stock_warehouse, null);
    }


    public Page<Istock_warehouse> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warehouse stock_warehouse){
        Istock_warehouse clientModel = stock_warehouseFeignClient.getDraft(stock_warehouse.getId(),(stock_warehouseImpl)stock_warehouse) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse.getClass(), false);
        copier.copy(clientModel, stock_warehouse, null);
    }



}


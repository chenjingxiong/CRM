package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_location_route;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_location_routeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_location_routeImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_location_routeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_location_route] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_location_routeClientServiceImpl implements Istock_location_routeClientService {

    stock_location_routeFeignClient stock_location_routeFeignClient;

    @Autowired
    public stock_location_routeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_location_routeFeignClient = nameBuilder.target(stock_location_routeFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_location_routeFeignClient = nameBuilder.target(stock_location_routeFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_location_route createModel() {
		return new stock_location_routeImpl();
	}


    public Page<Istock_location_route> fetchDefault(SearchContext context){
        Page<stock_location_routeImpl> page = this.stock_location_routeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_location_route stock_location_route){
        Istock_location_route clientModel = stock_location_routeFeignClient.create((stock_location_routeImpl)stock_location_route) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location_route.getClass(), false);
        copier.copy(clientModel, stock_location_route, null);
    }


    public void createBatch(List<Istock_location_route> stock_location_routes){
        if(stock_location_routes!=null){
            List<stock_location_routeImpl> list = new ArrayList<stock_location_routeImpl>();
            for(Istock_location_route istock_location_route :stock_location_routes){
                list.add((stock_location_routeImpl)istock_location_route) ;
            }
            stock_location_routeFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_location_route> stock_location_routes){
        if(stock_location_routes!=null){
            List<stock_location_routeImpl> list = new ArrayList<stock_location_routeImpl>();
            for(Istock_location_route istock_location_route :stock_location_routes){
                list.add((stock_location_routeImpl)istock_location_route) ;
            }
            stock_location_routeFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Istock_location_route> stock_location_routes){
        if(stock_location_routes!=null){
            List<stock_location_routeImpl> list = new ArrayList<stock_location_routeImpl>();
            for(Istock_location_route istock_location_route :stock_location_routes){
                list.add((stock_location_routeImpl)istock_location_route) ;
            }
            stock_location_routeFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_location_route stock_location_route){
        Istock_location_route clientModel = stock_location_routeFeignClient.get(stock_location_route.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location_route.getClass(), false);
        copier.copy(clientModel, stock_location_route, null);
    }


    public void update(Istock_location_route stock_location_route){
        Istock_location_route clientModel = stock_location_routeFeignClient.update(stock_location_route.getId(),(stock_location_routeImpl)stock_location_route) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location_route.getClass(), false);
        copier.copy(clientModel, stock_location_route, null);
    }


    public void remove(Istock_location_route stock_location_route){
        stock_location_routeFeignClient.remove(stock_location_route.getId()) ;
    }


    public Page<Istock_location_route> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_location_route stock_location_route){
        Istock_location_route clientModel = stock_location_routeFeignClient.getDraft(stock_location_route.getId(),(stock_location_routeImpl)stock_location_route) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_location_route.getClass(), false);
        copier.copy(clientModel, stock_location_route, null);
    }



}


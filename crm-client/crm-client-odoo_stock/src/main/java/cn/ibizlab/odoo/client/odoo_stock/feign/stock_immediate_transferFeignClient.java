package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_immediate_transfer;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_immediate_transferImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
public interface stock_immediate_transferFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_immediate_transfers/createbatch")
    public stock_immediate_transferImpl createBatch(@RequestBody List<stock_immediate_transferImpl> stock_immediate_transfers);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_immediate_transfers")
    public stock_immediate_transferImpl create(@RequestBody stock_immediate_transferImpl stock_immediate_transfer);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_immediate_transfers/{id}")
    public stock_immediate_transferImpl update(@PathVariable("id") Integer id,@RequestBody stock_immediate_transferImpl stock_immediate_transfer);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_immediate_transfers/{id}")
    public stock_immediate_transferImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_immediate_transfers/removebatch")
    public stock_immediate_transferImpl removeBatch(@RequestBody List<stock_immediate_transferImpl> stock_immediate_transfers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_immediate_transfers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_immediate_transfers/fetchdefault")
    public Page<stock_immediate_transferImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_immediate_transfers/updatebatch")
    public stock_immediate_transferImpl updateBatch(@RequestBody List<stock_immediate_transferImpl> stock_immediate_transfers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_immediate_transfers/select")
    public Page<stock_immediate_transferImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_immediate_transfers/{id}/getdraft")
    public stock_immediate_transferImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_immediate_transferImpl stock_immediate_transfer);



}

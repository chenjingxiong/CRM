package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_inventory;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_inventory] 服务对象接口
 */
public interface stock_inventoryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventories/{id}")
    public stock_inventoryImpl update(@PathVariable("id") Integer id,@RequestBody stock_inventoryImpl stock_inventory);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventories/fetchdefault")
    public Page<stock_inventoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventories/updatebatch")
    public stock_inventoryImpl updateBatch(@RequestBody List<stock_inventoryImpl> stock_inventories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventories")
    public stock_inventoryImpl create(@RequestBody stock_inventoryImpl stock_inventory);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventories/removebatch")
    public stock_inventoryImpl removeBatch(@RequestBody List<stock_inventoryImpl> stock_inventories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventories/createbatch")
    public stock_inventoryImpl createBatch(@RequestBody List<stock_inventoryImpl> stock_inventories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventories/{id}")
    public stock_inventoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventories/select")
    public Page<stock_inventoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventories/{id}/getdraft")
    public stock_inventoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_inventoryImpl stock_inventory);



}

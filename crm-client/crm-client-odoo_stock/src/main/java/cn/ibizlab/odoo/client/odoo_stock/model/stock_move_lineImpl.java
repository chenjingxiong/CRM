package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_move_line] 对象
 */
public class stock_move_lineImpl implements Istock_move_line,Serializable{

    /**
     * 消耗行
     */
    public String consume_line_ids;

    @JsonIgnore
    public boolean consume_line_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 移动完成
     */
    public String done_move;

    @JsonIgnore
    public boolean done_moveDirtyFlag;
    
    /**
     * 完成工单
     */
    public String done_wo;

    @JsonIgnore
    public boolean done_woDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 初始需求是否可以编辑
     */
    public String is_initial_demand_editable;

    @JsonIgnore
    public boolean is_initial_demand_editableDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 至
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 至
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 从
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 从
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 批次可见
     */
    public String lots_visible;

    @JsonIgnore
    public boolean lots_visibleDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public Integer lot_id;

    @JsonIgnore
    public boolean lot_idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public String lot_id_text;

    @JsonIgnore
    public boolean lot_id_textDirtyFlag;
    
    /**
     * 批次/序列号 名称
     */
    public String lot_name;

    @JsonIgnore
    public boolean lot_nameDirtyFlag;
    
    /**
     * 完工批次/序列号
     */
    public Integer lot_produced_id;

    @JsonIgnore
    public boolean lot_produced_idDirtyFlag;
    
    /**
     * 完工批次/序列号
     */
    public String lot_produced_id_text;

    @JsonIgnore
    public boolean lot_produced_id_textDirtyFlag;
    
    /**
     * 产成品数量
     */
    public Double lot_produced_qty;

    @JsonIgnore
    public boolean lot_produced_qtyDirtyFlag;
    
    /**
     * 库存移动
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 库存移动
     */
    public String move_id_text;

    @JsonIgnore
    public boolean move_id_textDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_id;

    @JsonIgnore
    public boolean owner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_id_text;

    @JsonIgnore
    public boolean owner_id_textDirtyFlag;
    
    /**
     * 源包裹
     */
    public Integer package_id;

    @JsonIgnore
    public boolean package_idDirtyFlag;
    
    /**
     * 源包裹
     */
    public String package_id_text;

    @JsonIgnore
    public boolean package_id_textDirtyFlag;
    
    /**
     * 包裹层级
     */
    public Integer package_level_id;

    @JsonIgnore
    public boolean package_level_idDirtyFlag;
    
    /**
     * 库存拣货
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 库存拣货
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String picking_type_entire_packs;

    @JsonIgnore
    public boolean picking_type_entire_packsDirtyFlag;
    
    /**
     * 创建新批次/序列号码
     */
    public String picking_type_use_create_lots;

    @JsonIgnore
    public boolean picking_type_use_create_lotsDirtyFlag;
    
    /**
     * 使用已有批次/序列号码
     */
    public String picking_type_use_existing_lots;

    @JsonIgnore
    public boolean picking_type_use_existing_lotsDirtyFlag;
    
    /**
     * 生产行
     */
    public String produce_line_ids;

    @JsonIgnore
    public boolean produce_line_idsDirtyFlag;
    
    /**
     * 生产单
     */
    public Integer production_id;

    @JsonIgnore
    public boolean production_idDirtyFlag;
    
    /**
     * 生产单
     */
    public String production_id_text;

    @JsonIgnore
    public boolean production_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 实际预留数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 已保留
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 完成
     */
    public Double qty_done;

    @JsonIgnore
    public boolean qty_doneDirtyFlag;
    
    /**
     * 编号
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 目的地包裹
     */
    public Integer result_package_id;

    @JsonIgnore
    public boolean result_package_idDirtyFlag;
    
    /**
     * 目的地包裹
     */
    public String result_package_id_text;

    @JsonIgnore
    public boolean result_package_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 追踪
     */
    public String tracking;

    @JsonIgnore
    public boolean trackingDirtyFlag;
    
    /**
     * 工单
     */
    public Integer workorder_id;

    @JsonIgnore
    public boolean workorder_idDirtyFlag;
    
    /**
     * 工单
     */
    public String workorder_id_text;

    @JsonIgnore
    public boolean workorder_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [消耗行]
     */
    @JsonProperty("consume_line_ids")
    public String getConsume_line_ids(){
        return this.consume_line_ids ;
    }

    /**
     * 设置 [消耗行]
     */
    @JsonProperty("consume_line_ids")
    public void setConsume_line_ids(String  consume_line_ids){
        this.consume_line_ids = consume_line_ids ;
        this.consume_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消耗行]脏标记
     */
    @JsonIgnore
    public boolean getConsume_line_idsDirtyFlag(){
        return this.consume_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [移动完成]
     */
    @JsonProperty("done_move")
    public String getDone_move(){
        return this.done_move ;
    }

    /**
     * 设置 [移动完成]
     */
    @JsonProperty("done_move")
    public void setDone_move(String  done_move){
        this.done_move = done_move ;
        this.done_moveDirtyFlag = true ;
    }

     /**
     * 获取 [移动完成]脏标记
     */
    @JsonIgnore
    public boolean getDone_moveDirtyFlag(){
        return this.done_moveDirtyFlag ;
    }   

    /**
     * 获取 [完成工单]
     */
    @JsonProperty("done_wo")
    public String getDone_wo(){
        return this.done_wo ;
    }

    /**
     * 设置 [完成工单]
     */
    @JsonProperty("done_wo")
    public void setDone_wo(String  done_wo){
        this.done_wo = done_wo ;
        this.done_woDirtyFlag = true ;
    }

     /**
     * 获取 [完成工单]脏标记
     */
    @JsonIgnore
    public boolean getDone_woDirtyFlag(){
        return this.done_woDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public String getIs_initial_demand_editable(){
        return this.is_initial_demand_editable ;
    }

    /**
     * 设置 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public void setIs_initial_demand_editable(String  is_initial_demand_editable){
        this.is_initial_demand_editable = is_initial_demand_editable ;
        this.is_initial_demand_editableDirtyFlag = true ;
    }

     /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    @JsonIgnore
    public boolean getIs_initial_demand_editableDirtyFlag(){
        return this.is_initial_demand_editableDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次可见]
     */
    @JsonProperty("lots_visible")
    public String getLots_visible(){
        return this.lots_visible ;
    }

    /**
     * 设置 [批次可见]
     */
    @JsonProperty("lots_visible")
    public void setLots_visible(String  lots_visible){
        this.lots_visible = lots_visible ;
        this.lots_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [批次可见]脏标记
     */
    @JsonIgnore
    public boolean getLots_visibleDirtyFlag(){
        return this.lots_visibleDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return this.lot_id ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return this.lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return this.lot_id_text ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return this.lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号 名称]
     */
    @JsonProperty("lot_name")
    public String getLot_name(){
        return this.lot_name ;
    }

    /**
     * 设置 [批次/序列号 名称]
     */
    @JsonProperty("lot_name")
    public void setLot_name(String  lot_name){
        this.lot_name = lot_name ;
        this.lot_nameDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号 名称]脏标记
     */
    @JsonIgnore
    public boolean getLot_nameDirtyFlag(){
        return this.lot_nameDirtyFlag ;
    }   

    /**
     * 获取 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id")
    public Integer getLot_produced_id(){
        return this.lot_produced_id ;
    }

    /**
     * 设置 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id")
    public void setLot_produced_id(Integer  lot_produced_id){
        this.lot_produced_id = lot_produced_id ;
        this.lot_produced_idDirtyFlag = true ;
    }

     /**
     * 获取 [完工批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_idDirtyFlag(){
        return this.lot_produced_idDirtyFlag ;
    }   

    /**
     * 获取 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id_text")
    public String getLot_produced_id_text(){
        return this.lot_produced_id_text ;
    }

    /**
     * 设置 [完工批次/序列号]
     */
    @JsonProperty("lot_produced_id_text")
    public void setLot_produced_id_text(String  lot_produced_id_text){
        this.lot_produced_id_text = lot_produced_id_text ;
        this.lot_produced_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [完工批次/序列号]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_id_textDirtyFlag(){
        return this.lot_produced_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产成品数量]
     */
    @JsonProperty("lot_produced_qty")
    public Double getLot_produced_qty(){
        return this.lot_produced_qty ;
    }

    /**
     * 设置 [产成品数量]
     */
    @JsonProperty("lot_produced_qty")
    public void setLot_produced_qty(Double  lot_produced_qty){
        this.lot_produced_qty = lot_produced_qty ;
        this.lot_produced_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [产成品数量]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_qtyDirtyFlag(){
        return this.lot_produced_qtyDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return this.move_id_text ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return this.move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return this.owner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return this.owner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return this.owner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return this.owner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源包裹]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return this.package_id ;
    }

    /**
     * 设置 [源包裹]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

     /**
     * 获取 [源包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return this.package_idDirtyFlag ;
    }   

    /**
     * 获取 [源包裹]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return this.package_id_text ;
    }

    /**
     * 设置 [源包裹]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return this.package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public Integer getPackage_level_id(){
        return this.package_level_id ;
    }

    /**
     * 设置 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public void setPackage_level_id(Integer  package_level_id){
        this.package_level_id = package_level_id ;
        this.package_level_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idDirtyFlag(){
        return this.package_level_idDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return this.picking_type_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return this.picking_type_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [创建新批次/序列号码]
     */
    @JsonProperty("picking_type_use_create_lots")
    public String getPicking_type_use_create_lots(){
        return this.picking_type_use_create_lots ;
    }

    /**
     * 设置 [创建新批次/序列号码]
     */
    @JsonProperty("picking_type_use_create_lots")
    public void setPicking_type_use_create_lots(String  picking_type_use_create_lots){
        this.picking_type_use_create_lots = picking_type_use_create_lots ;
        this.picking_type_use_create_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_create_lotsDirtyFlag(){
        return this.picking_type_use_create_lotsDirtyFlag ;
    }   

    /**
     * 获取 [使用已有批次/序列号码]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public String getPicking_type_use_existing_lots(){
        return this.picking_type_use_existing_lots ;
    }

    /**
     * 设置 [使用已有批次/序列号码]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public void setPicking_type_use_existing_lots(String  picking_type_use_existing_lots){
        this.picking_type_use_existing_lots = picking_type_use_existing_lots ;
        this.picking_type_use_existing_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_existing_lotsDirtyFlag(){
        return this.picking_type_use_existing_lotsDirtyFlag ;
    }   

    /**
     * 获取 [生产行]
     */
    @JsonProperty("produce_line_ids")
    public String getProduce_line_ids(){
        return this.produce_line_ids ;
    }

    /**
     * 设置 [生产行]
     */
    @JsonProperty("produce_line_ids")
    public void setProduce_line_ids(String  produce_line_ids){
        this.produce_line_ids = produce_line_ids ;
        this.produce_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [生产行]脏标记
     */
    @JsonIgnore
    public boolean getProduce_line_idsDirtyFlag(){
        return this.produce_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [生产单]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return this.production_id ;
    }

    /**
     * 设置 [生产单]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

     /**
     * 获取 [生产单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return this.production_idDirtyFlag ;
    }   

    /**
     * 获取 [生产单]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return this.production_id_text ;
    }

    /**
     * 设置 [生产单]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [生产单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return this.production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [实际预留数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [实际预留数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [实际预留数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已保留]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [已保留]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [已保留]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [完成]
     */
    @JsonProperty("qty_done")
    public Double getQty_done(){
        return this.qty_done ;
    }

    /**
     * 设置 [完成]
     */
    @JsonProperty("qty_done")
    public void setQty_done(Double  qty_done){
        this.qty_done = qty_done ;
        this.qty_doneDirtyFlag = true ;
    }

     /**
     * 获取 [完成]脏标记
     */
    @JsonIgnore
    public boolean getQty_doneDirtyFlag(){
        return this.qty_doneDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [目的地包裹]
     */
    @JsonProperty("result_package_id")
    public Integer getResult_package_id(){
        return this.result_package_id ;
    }

    /**
     * 设置 [目的地包裹]
     */
    @JsonProperty("result_package_id")
    public void setResult_package_id(Integer  result_package_id){
        this.result_package_id = result_package_id ;
        this.result_package_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的地包裹]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_idDirtyFlag(){
        return this.result_package_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地包裹]
     */
    @JsonProperty("result_package_id_text")
    public String getResult_package_id_text(){
        return this.result_package_id_text ;
    }

    /**
     * 设置 [目的地包裹]
     */
    @JsonProperty("result_package_id_text")
    public void setResult_package_id_text(String  result_package_id_text){
        this.result_package_id_text = result_package_id_text ;
        this.result_package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的地包裹]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_id_textDirtyFlag(){
        return this.result_package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return this.tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return this.trackingDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return this.workorder_id ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return this.workorder_idDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return this.workorder_id_text ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return this.workorder_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

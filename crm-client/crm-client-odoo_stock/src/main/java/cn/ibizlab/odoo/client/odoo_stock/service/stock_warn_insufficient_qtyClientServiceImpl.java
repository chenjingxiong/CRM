package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qtyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qtyImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warn_insufficient_qtyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warn_insufficient_qty] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warn_insufficient_qtyClientServiceImpl implements Istock_warn_insufficient_qtyClientService {

    stock_warn_insufficient_qtyFeignClient stock_warn_insufficient_qtyFeignClient;

    @Autowired
    public stock_warn_insufficient_qtyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qtyFeignClient = nameBuilder.target(stock_warn_insufficient_qtyFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warn_insufficient_qtyFeignClient = nameBuilder.target(stock_warn_insufficient_qtyFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warn_insufficient_qty createModel() {
		return new stock_warn_insufficient_qtyImpl();
	}


    public void removeBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
        if(stock_warn_insufficient_qties!=null){
            List<stock_warn_insufficient_qtyImpl> list = new ArrayList<stock_warn_insufficient_qtyImpl>();
            for(Istock_warn_insufficient_qty istock_warn_insufficient_qty :stock_warn_insufficient_qties){
                list.add((stock_warn_insufficient_qtyImpl)istock_warn_insufficient_qty) ;
            }
            stock_warn_insufficient_qtyFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
        Istock_warn_insufficient_qty clientModel = stock_warn_insufficient_qtyFeignClient.update(stock_warn_insufficient_qty.getId(),(stock_warn_insufficient_qtyImpl)stock_warn_insufficient_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty, null);
    }


    public void updateBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
        if(stock_warn_insufficient_qties!=null){
            List<stock_warn_insufficient_qtyImpl> list = new ArrayList<stock_warn_insufficient_qtyImpl>();
            for(Istock_warn_insufficient_qty istock_warn_insufficient_qty :stock_warn_insufficient_qties){
                list.add((stock_warn_insufficient_qtyImpl)istock_warn_insufficient_qty) ;
            }
            stock_warn_insufficient_qtyFeignClient.updateBatch(list) ;
        }
    }


    public Page<Istock_warn_insufficient_qty> fetchDefault(SearchContext context){
        Page<stock_warn_insufficient_qtyImpl> page = this.stock_warn_insufficient_qtyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
        stock_warn_insufficient_qtyFeignClient.remove(stock_warn_insufficient_qty.getId()) ;
    }


    public void createBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
        if(stock_warn_insufficient_qties!=null){
            List<stock_warn_insufficient_qtyImpl> list = new ArrayList<stock_warn_insufficient_qtyImpl>();
            for(Istock_warn_insufficient_qty istock_warn_insufficient_qty :stock_warn_insufficient_qties){
                list.add((stock_warn_insufficient_qtyImpl)istock_warn_insufficient_qty) ;
            }
            stock_warn_insufficient_qtyFeignClient.createBatch(list) ;
        }
    }


    public void get(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
        Istock_warn_insufficient_qty clientModel = stock_warn_insufficient_qtyFeignClient.get(stock_warn_insufficient_qty.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty, null);
    }


    public void create(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
        Istock_warn_insufficient_qty clientModel = stock_warn_insufficient_qtyFeignClient.create((stock_warn_insufficient_qtyImpl)stock_warn_insufficient_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty, null);
    }


    public Page<Istock_warn_insufficient_qty> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
        Istock_warn_insufficient_qty clientModel = stock_warn_insufficient_qtyFeignClient.getDraft(stock_warn_insufficient_qty.getId(),(stock_warn_insufficient_qtyImpl)stock_warn_insufficient_qty) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warn_insufficient_qty.getClass(), false);
        copier.copy(clientModel, stock_warn_insufficient_qty, null);
    }



}


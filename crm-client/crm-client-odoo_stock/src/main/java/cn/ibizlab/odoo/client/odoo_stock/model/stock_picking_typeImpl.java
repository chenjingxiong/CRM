package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_picking_type;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_picking_type] 对象
 */
public class stock_picking_typeImpl implements Istock_picking_type,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 条码
     */
    public String barcode;

    @JsonIgnore
    public boolean barcodeDirtyFlag;
    
    /**
     * 作业的类型
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 颜色
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 生产单数量延迟
     */
    public Integer count_mo_late;

    @JsonIgnore
    public boolean count_mo_lateDirtyFlag;
    
    /**
     * 生产单的数量
     */
    public Integer count_mo_todo;

    @JsonIgnore
    public boolean count_mo_todoDirtyFlag;
    
    /**
     * 生产单等待的数量
     */
    public Integer count_mo_waiting;

    @JsonIgnore
    public boolean count_mo_waitingDirtyFlag;
    
    /**
     * 拣货个数
     */
    public Integer count_picking;

    @JsonIgnore
    public boolean count_pickingDirtyFlag;
    
    /**
     * 拣货欠单个数
     */
    public Integer count_picking_backorders;

    @JsonIgnore
    public boolean count_picking_backordersDirtyFlag;
    
    /**
     * 草稿拣货个数
     */
    public Integer count_picking_draft;

    @JsonIgnore
    public boolean count_picking_draftDirtyFlag;
    
    /**
     * 迟到拣货个数
     */
    public Integer count_picking_late;

    @JsonIgnore
    public boolean count_picking_lateDirtyFlag;
    
    /**
     * 拣货个数准备好
     */
    public Integer count_picking_ready;

    @JsonIgnore
    public boolean count_picking_readyDirtyFlag;
    
    /**
     * 等待拣货个数
     */
    public Integer count_picking_waiting;

    @JsonIgnore
    public boolean count_picking_waitingDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 默认目的位置
     */
    public Integer default_location_dest_id;

    @JsonIgnore
    public boolean default_location_dest_idDirtyFlag;
    
    /**
     * 默认目的位置
     */
    public String default_location_dest_id_text;

    @JsonIgnore
    public boolean default_location_dest_id_textDirtyFlag;
    
    /**
     * 默认源位置
     */
    public Integer default_location_src_id;

    @JsonIgnore
    public boolean default_location_src_idDirtyFlag;
    
    /**
     * 默认源位置
     */
    public String default_location_src_id_text;

    @JsonIgnore
    public boolean default_location_src_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最近10笔完成的拣货
     */
    public String last_done_picking;

    @JsonIgnore
    public boolean last_done_pickingDirtyFlag;
    
    /**
     * 作业类型
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 欠单比率
     */
    public Integer rate_picking_backorders;

    @JsonIgnore
    public boolean rate_picking_backordersDirtyFlag;
    
    /**
     * 延迟比率
     */
    public Integer rate_picking_late;

    @JsonIgnore
    public boolean rate_picking_lateDirtyFlag;
    
    /**
     * 退回的作业类型
     */
    public Integer return_picking_type_id;

    @JsonIgnore
    public boolean return_picking_type_idDirtyFlag;
    
    /**
     * 退回的作业类型
     */
    public String return_picking_type_id_text;

    @JsonIgnore
    public boolean return_picking_type_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 参考序列
     */
    public Integer sequence_id;

    @JsonIgnore
    public boolean sequence_idDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String show_entire_packs;

    @JsonIgnore
    public boolean show_entire_packsDirtyFlag;
    
    /**
     * 显示详细作业
     */
    public String show_operations;

    @JsonIgnore
    public boolean show_operationsDirtyFlag;
    
    /**
     * 显示预留
     */
    public String show_reserved;

    @JsonIgnore
    public boolean show_reservedDirtyFlag;
    
    /**
     * 创建新批次/序列号码
     */
    public String use_create_lots;

    @JsonIgnore
    public boolean use_create_lotsDirtyFlag;
    
    /**
     * 使用已有批次/序列号码
     */
    public String use_existing_lots;

    @JsonIgnore
    public boolean use_existing_lotsDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer warehouse_id;

    @JsonIgnore
    public boolean warehouse_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_id_text;

    @JsonIgnore
    public boolean warehouse_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [条码]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }   

    /**
     * 获取 [作业的类型]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [作业的类型]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [作业的类型]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [生产单数量延迟]
     */
    @JsonProperty("count_mo_late")
    public Integer getCount_mo_late(){
        return this.count_mo_late ;
    }

    /**
     * 设置 [生产单数量延迟]
     */
    @JsonProperty("count_mo_late")
    public void setCount_mo_late(Integer  count_mo_late){
        this.count_mo_late = count_mo_late ;
        this.count_mo_lateDirtyFlag = true ;
    }

     /**
     * 获取 [生产单数量延迟]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_lateDirtyFlag(){
        return this.count_mo_lateDirtyFlag ;
    }   

    /**
     * 获取 [生产单的数量]
     */
    @JsonProperty("count_mo_todo")
    public Integer getCount_mo_todo(){
        return this.count_mo_todo ;
    }

    /**
     * 设置 [生产单的数量]
     */
    @JsonProperty("count_mo_todo")
    public void setCount_mo_todo(Integer  count_mo_todo){
        this.count_mo_todo = count_mo_todo ;
        this.count_mo_todoDirtyFlag = true ;
    }

     /**
     * 获取 [生产单的数量]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_todoDirtyFlag(){
        return this.count_mo_todoDirtyFlag ;
    }   

    /**
     * 获取 [生产单等待的数量]
     */
    @JsonProperty("count_mo_waiting")
    public Integer getCount_mo_waiting(){
        return this.count_mo_waiting ;
    }

    /**
     * 设置 [生产单等待的数量]
     */
    @JsonProperty("count_mo_waiting")
    public void setCount_mo_waiting(Integer  count_mo_waiting){
        this.count_mo_waiting = count_mo_waiting ;
        this.count_mo_waitingDirtyFlag = true ;
    }

     /**
     * 获取 [生产单等待的数量]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_waitingDirtyFlag(){
        return this.count_mo_waitingDirtyFlag ;
    }   

    /**
     * 获取 [拣货个数]
     */
    @JsonProperty("count_picking")
    public Integer getCount_picking(){
        return this.count_picking ;
    }

    /**
     * 设置 [拣货个数]
     */
    @JsonProperty("count_picking")
    public void setCount_picking(Integer  count_picking){
        this.count_picking = count_picking ;
        this.count_pickingDirtyFlag = true ;
    }

     /**
     * 获取 [拣货个数]脏标记
     */
    @JsonIgnore
    public boolean getCount_pickingDirtyFlag(){
        return this.count_pickingDirtyFlag ;
    }   

    /**
     * 获取 [拣货欠单个数]
     */
    @JsonProperty("count_picking_backorders")
    public Integer getCount_picking_backorders(){
        return this.count_picking_backorders ;
    }

    /**
     * 设置 [拣货欠单个数]
     */
    @JsonProperty("count_picking_backorders")
    public void setCount_picking_backorders(Integer  count_picking_backorders){
        this.count_picking_backorders = count_picking_backorders ;
        this.count_picking_backordersDirtyFlag = true ;
    }

     /**
     * 获取 [拣货欠单个数]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_backordersDirtyFlag(){
        return this.count_picking_backordersDirtyFlag ;
    }   

    /**
     * 获取 [草稿拣货个数]
     */
    @JsonProperty("count_picking_draft")
    public Integer getCount_picking_draft(){
        return this.count_picking_draft ;
    }

    /**
     * 设置 [草稿拣货个数]
     */
    @JsonProperty("count_picking_draft")
    public void setCount_picking_draft(Integer  count_picking_draft){
        this.count_picking_draft = count_picking_draft ;
        this.count_picking_draftDirtyFlag = true ;
    }

     /**
     * 获取 [草稿拣货个数]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_draftDirtyFlag(){
        return this.count_picking_draftDirtyFlag ;
    }   

    /**
     * 获取 [迟到拣货个数]
     */
    @JsonProperty("count_picking_late")
    public Integer getCount_picking_late(){
        return this.count_picking_late ;
    }

    /**
     * 设置 [迟到拣货个数]
     */
    @JsonProperty("count_picking_late")
    public void setCount_picking_late(Integer  count_picking_late){
        this.count_picking_late = count_picking_late ;
        this.count_picking_lateDirtyFlag = true ;
    }

     /**
     * 获取 [迟到拣货个数]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_lateDirtyFlag(){
        return this.count_picking_lateDirtyFlag ;
    }   

    /**
     * 获取 [拣货个数准备好]
     */
    @JsonProperty("count_picking_ready")
    public Integer getCount_picking_ready(){
        return this.count_picking_ready ;
    }

    /**
     * 设置 [拣货个数准备好]
     */
    @JsonProperty("count_picking_ready")
    public void setCount_picking_ready(Integer  count_picking_ready){
        this.count_picking_ready = count_picking_ready ;
        this.count_picking_readyDirtyFlag = true ;
    }

     /**
     * 获取 [拣货个数准备好]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_readyDirtyFlag(){
        return this.count_picking_readyDirtyFlag ;
    }   

    /**
     * 获取 [等待拣货个数]
     */
    @JsonProperty("count_picking_waiting")
    public Integer getCount_picking_waiting(){
        return this.count_picking_waiting ;
    }

    /**
     * 设置 [等待拣货个数]
     */
    @JsonProperty("count_picking_waiting")
    public void setCount_picking_waiting(Integer  count_picking_waiting){
        this.count_picking_waiting = count_picking_waiting ;
        this.count_picking_waitingDirtyFlag = true ;
    }

     /**
     * 获取 [等待拣货个数]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_waitingDirtyFlag(){
        return this.count_picking_waitingDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [默认目的位置]
     */
    @JsonProperty("default_location_dest_id")
    public Integer getDefault_location_dest_id(){
        return this.default_location_dest_id ;
    }

    /**
     * 设置 [默认目的位置]
     */
    @JsonProperty("default_location_dest_id")
    public void setDefault_location_dest_id(Integer  default_location_dest_id){
        this.default_location_dest_id = default_location_dest_id ;
        this.default_location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认目的位置]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_dest_idDirtyFlag(){
        return this.default_location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [默认目的位置]
     */
    @JsonProperty("default_location_dest_id_text")
    public String getDefault_location_dest_id_text(){
        return this.default_location_dest_id_text ;
    }

    /**
     * 设置 [默认目的位置]
     */
    @JsonProperty("default_location_dest_id_text")
    public void setDefault_location_dest_id_text(String  default_location_dest_id_text){
        this.default_location_dest_id_text = default_location_dest_id_text ;
        this.default_location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认目的位置]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_dest_id_textDirtyFlag(){
        return this.default_location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [默认源位置]
     */
    @JsonProperty("default_location_src_id")
    public Integer getDefault_location_src_id(){
        return this.default_location_src_id ;
    }

    /**
     * 设置 [默认源位置]
     */
    @JsonProperty("default_location_src_id")
    public void setDefault_location_src_id(Integer  default_location_src_id){
        this.default_location_src_id = default_location_src_id ;
        this.default_location_src_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认源位置]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_src_idDirtyFlag(){
        return this.default_location_src_idDirtyFlag ;
    }   

    /**
     * 获取 [默认源位置]
     */
    @JsonProperty("default_location_src_id_text")
    public String getDefault_location_src_id_text(){
        return this.default_location_src_id_text ;
    }

    /**
     * 设置 [默认源位置]
     */
    @JsonProperty("default_location_src_id_text")
    public void setDefault_location_src_id_text(String  default_location_src_id_text){
        this.default_location_src_id_text = default_location_src_id_text ;
        this.default_location_src_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认源位置]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_src_id_textDirtyFlag(){
        return this.default_location_src_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最近10笔完成的拣货]
     */
    @JsonProperty("last_done_picking")
    public String getLast_done_picking(){
        return this.last_done_picking ;
    }

    /**
     * 设置 [最近10笔完成的拣货]
     */
    @JsonProperty("last_done_picking")
    public void setLast_done_picking(String  last_done_picking){
        this.last_done_picking = last_done_picking ;
        this.last_done_pickingDirtyFlag = true ;
    }

     /**
     * 获取 [最近10笔完成的拣货]脏标记
     */
    @JsonIgnore
    public boolean getLast_done_pickingDirtyFlag(){
        return this.last_done_pickingDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [欠单比率]
     */
    @JsonProperty("rate_picking_backorders")
    public Integer getRate_picking_backorders(){
        return this.rate_picking_backorders ;
    }

    /**
     * 设置 [欠单比率]
     */
    @JsonProperty("rate_picking_backorders")
    public void setRate_picking_backorders(Integer  rate_picking_backorders){
        this.rate_picking_backorders = rate_picking_backorders ;
        this.rate_picking_backordersDirtyFlag = true ;
    }

     /**
     * 获取 [欠单比率]脏标记
     */
    @JsonIgnore
    public boolean getRate_picking_backordersDirtyFlag(){
        return this.rate_picking_backordersDirtyFlag ;
    }   

    /**
     * 获取 [延迟比率]
     */
    @JsonProperty("rate_picking_late")
    public Integer getRate_picking_late(){
        return this.rate_picking_late ;
    }

    /**
     * 设置 [延迟比率]
     */
    @JsonProperty("rate_picking_late")
    public void setRate_picking_late(Integer  rate_picking_late){
        this.rate_picking_late = rate_picking_late ;
        this.rate_picking_lateDirtyFlag = true ;
    }

     /**
     * 获取 [延迟比率]脏标记
     */
    @JsonIgnore
    public boolean getRate_picking_lateDirtyFlag(){
        return this.rate_picking_lateDirtyFlag ;
    }   

    /**
     * 获取 [退回的作业类型]
     */
    @JsonProperty("return_picking_type_id")
    public Integer getReturn_picking_type_id(){
        return this.return_picking_type_id ;
    }

    /**
     * 设置 [退回的作业类型]
     */
    @JsonProperty("return_picking_type_id")
    public void setReturn_picking_type_id(Integer  return_picking_type_id){
        this.return_picking_type_id = return_picking_type_id ;
        this.return_picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [退回的作业类型]脏标记
     */
    @JsonIgnore
    public boolean getReturn_picking_type_idDirtyFlag(){
        return this.return_picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [退回的作业类型]
     */
    @JsonProperty("return_picking_type_id_text")
    public String getReturn_picking_type_id_text(){
        return this.return_picking_type_id_text ;
    }

    /**
     * 设置 [退回的作业类型]
     */
    @JsonProperty("return_picking_type_id_text")
    public void setReturn_picking_type_id_text(String  return_picking_type_id_text){
        this.return_picking_type_id_text = return_picking_type_id_text ;
        this.return_picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [退回的作业类型]脏标记
     */
    @JsonIgnore
    public boolean getReturn_picking_type_id_textDirtyFlag(){
        return this.return_picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [参考序列]
     */
    @JsonProperty("sequence_id")
    public Integer getSequence_id(){
        return this.sequence_id ;
    }

    /**
     * 设置 [参考序列]
     */
    @JsonProperty("sequence_id")
    public void setSequence_id(Integer  sequence_id){
        this.sequence_id = sequence_id ;
        this.sequence_idDirtyFlag = true ;
    }

     /**
     * 获取 [参考序列]脏标记
     */
    @JsonIgnore
    public boolean getSequence_idDirtyFlag(){
        return this.sequence_idDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("show_entire_packs")
    public String getShow_entire_packs(){
        return this.show_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("show_entire_packs")
    public void setShow_entire_packs(String  show_entire_packs){
        this.show_entire_packs = show_entire_packs ;
        this.show_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getShow_entire_packsDirtyFlag(){
        return this.show_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [显示详细作业]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return this.show_operations ;
    }

    /**
     * 设置 [显示详细作业]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

     /**
     * 获取 [显示详细作业]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return this.show_operationsDirtyFlag ;
    }   

    /**
     * 获取 [显示预留]
     */
    @JsonProperty("show_reserved")
    public String getShow_reserved(){
        return this.show_reserved ;
    }

    /**
     * 设置 [显示预留]
     */
    @JsonProperty("show_reserved")
    public void setShow_reserved(String  show_reserved){
        this.show_reserved = show_reserved ;
        this.show_reservedDirtyFlag = true ;
    }

     /**
     * 获取 [显示预留]脏标记
     */
    @JsonIgnore
    public boolean getShow_reservedDirtyFlag(){
        return this.show_reservedDirtyFlag ;
    }   

    /**
     * 获取 [创建新批次/序列号码]
     */
    @JsonProperty("use_create_lots")
    public String getUse_create_lots(){
        return this.use_create_lots ;
    }

    /**
     * 设置 [创建新批次/序列号码]
     */
    @JsonProperty("use_create_lots")
    public void setUse_create_lots(String  use_create_lots){
        this.use_create_lots = use_create_lots ;
        this.use_create_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getUse_create_lotsDirtyFlag(){
        return this.use_create_lotsDirtyFlag ;
    }   

    /**
     * 获取 [使用已有批次/序列号码]
     */
    @JsonProperty("use_existing_lots")
    public String getUse_existing_lots(){
        return this.use_existing_lots ;
    }

    /**
     * 设置 [使用已有批次/序列号码]
     */
    @JsonProperty("use_existing_lots")
    public void setUse_existing_lots(String  use_existing_lots){
        this.use_existing_lots = use_existing_lots ;
        this.use_existing_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getUse_existing_lotsDirtyFlag(){
        return this.use_existing_lotsDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

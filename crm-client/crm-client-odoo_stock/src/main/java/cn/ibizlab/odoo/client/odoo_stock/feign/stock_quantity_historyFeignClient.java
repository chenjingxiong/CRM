package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_quantity_history;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantity_historyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_quantity_history] 服务对象接口
 */
public interface stock_quantity_historyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quantity_histories/{id}")
    public stock_quantity_historyImpl update(@PathVariable("id") Integer id,@RequestBody stock_quantity_historyImpl stock_quantity_history);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quantity_histories")
    public stock_quantity_historyImpl create(@RequestBody stock_quantity_historyImpl stock_quantity_history);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quantity_histories/updatebatch")
    public stock_quantity_historyImpl updateBatch(@RequestBody List<stock_quantity_historyImpl> stock_quantity_histories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quantity_histories/removebatch")
    public stock_quantity_historyImpl removeBatch(@RequestBody List<stock_quantity_historyImpl> stock_quantity_histories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quantity_histories/fetchdefault")
    public Page<stock_quantity_historyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quantity_histories/{id}")
    public stock_quantity_historyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quantity_histories/createbatch")
    public stock_quantity_historyImpl createBatch(@RequestBody List<stock_quantity_historyImpl> stock_quantity_histories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quantity_histories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quantity_histories/select")
    public Page<stock_quantity_historyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quantity_histories/{id}/getdraft")
    public stock_quantity_historyImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_quantity_historyImpl stock_quantity_history);



}

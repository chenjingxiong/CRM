package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_production_lot;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_production_lotClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_production_lotImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_production_lotFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_production_lot] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_production_lotClientServiceImpl implements Istock_production_lotClientService {

    stock_production_lotFeignClient stock_production_lotFeignClient;

    @Autowired
    public stock_production_lotClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_production_lotFeignClient = nameBuilder.target(stock_production_lotFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_production_lotFeignClient = nameBuilder.target(stock_production_lotFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_production_lot createModel() {
		return new stock_production_lotImpl();
	}


    public void update(Istock_production_lot stock_production_lot){
        Istock_production_lot clientModel = stock_production_lotFeignClient.update(stock_production_lot.getId(),(stock_production_lotImpl)stock_production_lot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_production_lot.getClass(), false);
        copier.copy(clientModel, stock_production_lot, null);
    }


    public void updateBatch(List<Istock_production_lot> stock_production_lots){
        if(stock_production_lots!=null){
            List<stock_production_lotImpl> list = new ArrayList<stock_production_lotImpl>();
            for(Istock_production_lot istock_production_lot :stock_production_lots){
                list.add((stock_production_lotImpl)istock_production_lot) ;
            }
            stock_production_lotFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Istock_production_lot> stock_production_lots){
        if(stock_production_lots!=null){
            List<stock_production_lotImpl> list = new ArrayList<stock_production_lotImpl>();
            for(Istock_production_lot istock_production_lot :stock_production_lots){
                list.add((stock_production_lotImpl)istock_production_lot) ;
            }
            stock_production_lotFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_production_lot> fetchDefault(SearchContext context){
        Page<stock_production_lotImpl> page = this.stock_production_lotFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Istock_production_lot stock_production_lot){
        Istock_production_lot clientModel = stock_production_lotFeignClient.get(stock_production_lot.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_production_lot.getClass(), false);
        copier.copy(clientModel, stock_production_lot, null);
    }


    public void removeBatch(List<Istock_production_lot> stock_production_lots){
        if(stock_production_lots!=null){
            List<stock_production_lotImpl> list = new ArrayList<stock_production_lotImpl>();
            for(Istock_production_lot istock_production_lot :stock_production_lots){
                list.add((stock_production_lotImpl)istock_production_lot) ;
            }
            stock_production_lotFeignClient.removeBatch(list) ;
        }
    }


    public void create(Istock_production_lot stock_production_lot){
        Istock_production_lot clientModel = stock_production_lotFeignClient.create((stock_production_lotImpl)stock_production_lot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_production_lot.getClass(), false);
        copier.copy(clientModel, stock_production_lot, null);
    }


    public void remove(Istock_production_lot stock_production_lot){
        stock_production_lotFeignClient.remove(stock_production_lot.getId()) ;
    }


    public Page<Istock_production_lot> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_production_lot stock_production_lot){
        Istock_production_lot clientModel = stock_production_lotFeignClient.getDraft(stock_production_lot.getId(),(stock_production_lotImpl)stock_production_lot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_production_lot.getClass(), false);
        copier.copy(clientModel, stock_production_lot, null);
    }



}


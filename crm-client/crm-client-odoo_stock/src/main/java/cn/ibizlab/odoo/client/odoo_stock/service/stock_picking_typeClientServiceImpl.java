package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_picking_type;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_picking_typeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_picking_typeImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_picking_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_picking_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_picking_typeClientServiceImpl implements Istock_picking_typeClientService {

    stock_picking_typeFeignClient stock_picking_typeFeignClient;

    @Autowired
    public stock_picking_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_picking_typeFeignClient = nameBuilder.target(stock_picking_typeFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_picking_typeFeignClient = nameBuilder.target(stock_picking_typeFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_picking_type createModel() {
		return new stock_picking_typeImpl();
	}


    public void createBatch(List<Istock_picking_type> stock_picking_types){
        if(stock_picking_types!=null){
            List<stock_picking_typeImpl> list = new ArrayList<stock_picking_typeImpl>();
            for(Istock_picking_type istock_picking_type :stock_picking_types){
                list.add((stock_picking_typeImpl)istock_picking_type) ;
            }
            stock_picking_typeFeignClient.createBatch(list) ;
        }
    }


    public Page<Istock_picking_type> fetchDefault(SearchContext context){
        Page<stock_picking_typeImpl> page = this.stock_picking_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Istock_picking_type> stock_picking_types){
        if(stock_picking_types!=null){
            List<stock_picking_typeImpl> list = new ArrayList<stock_picking_typeImpl>();
            for(Istock_picking_type istock_picking_type :stock_picking_types){
                list.add((stock_picking_typeImpl)istock_picking_type) ;
            }
            stock_picking_typeFeignClient.removeBatch(list) ;
        }
    }


    public void create(Istock_picking_type stock_picking_type){
        Istock_picking_type clientModel = stock_picking_typeFeignClient.create((stock_picking_typeImpl)stock_picking_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking_type.getClass(), false);
        copier.copy(clientModel, stock_picking_type, null);
    }


    public void update(Istock_picking_type stock_picking_type){
        Istock_picking_type clientModel = stock_picking_typeFeignClient.update(stock_picking_type.getId(),(stock_picking_typeImpl)stock_picking_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking_type.getClass(), false);
        copier.copy(clientModel, stock_picking_type, null);
    }


    public void get(Istock_picking_type stock_picking_type){
        Istock_picking_type clientModel = stock_picking_typeFeignClient.get(stock_picking_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking_type.getClass(), false);
        copier.copy(clientModel, stock_picking_type, null);
    }


    public void updateBatch(List<Istock_picking_type> stock_picking_types){
        if(stock_picking_types!=null){
            List<stock_picking_typeImpl> list = new ArrayList<stock_picking_typeImpl>();
            for(Istock_picking_type istock_picking_type :stock_picking_types){
                list.add((stock_picking_typeImpl)istock_picking_type) ;
            }
            stock_picking_typeFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Istock_picking_type stock_picking_type){
        stock_picking_typeFeignClient.remove(stock_picking_type.getId()) ;
    }


    public Page<Istock_picking_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_picking_type stock_picking_type){
        Istock_picking_type clientModel = stock_picking_typeFeignClient.getDraft(stock_picking_type.getId(),(stock_picking_typeImpl)stock_picking_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_picking_type.getClass(), false);
        copier.copy(clientModel, stock_picking_type, null);
    }



}


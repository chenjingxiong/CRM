package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_immediate_transfer;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_immediate_transferClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_immediate_transferImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_immediate_transferFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_immediate_transferClientServiceImpl implements Istock_immediate_transferClientService {

    stock_immediate_transferFeignClient stock_immediate_transferFeignClient;

    @Autowired
    public stock_immediate_transferClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_immediate_transferFeignClient = nameBuilder.target(stock_immediate_transferFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_immediate_transferFeignClient = nameBuilder.target(stock_immediate_transferFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_immediate_transfer createModel() {
		return new stock_immediate_transferImpl();
	}


    public void createBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
        if(stock_immediate_transfers!=null){
            List<stock_immediate_transferImpl> list = new ArrayList<stock_immediate_transferImpl>();
            for(Istock_immediate_transfer istock_immediate_transfer :stock_immediate_transfers){
                list.add((stock_immediate_transferImpl)istock_immediate_transfer) ;
            }
            stock_immediate_transferFeignClient.createBatch(list) ;
        }
    }


    public void create(Istock_immediate_transfer stock_immediate_transfer){
        Istock_immediate_transfer clientModel = stock_immediate_transferFeignClient.create((stock_immediate_transferImpl)stock_immediate_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_immediate_transfer.getClass(), false);
        copier.copy(clientModel, stock_immediate_transfer, null);
    }


    public void update(Istock_immediate_transfer stock_immediate_transfer){
        Istock_immediate_transfer clientModel = stock_immediate_transferFeignClient.update(stock_immediate_transfer.getId(),(stock_immediate_transferImpl)stock_immediate_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_immediate_transfer.getClass(), false);
        copier.copy(clientModel, stock_immediate_transfer, null);
    }


    public void get(Istock_immediate_transfer stock_immediate_transfer){
        Istock_immediate_transfer clientModel = stock_immediate_transferFeignClient.get(stock_immediate_transfer.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_immediate_transfer.getClass(), false);
        copier.copy(clientModel, stock_immediate_transfer, null);
    }


    public void removeBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
        if(stock_immediate_transfers!=null){
            List<stock_immediate_transferImpl> list = new ArrayList<stock_immediate_transferImpl>();
            for(Istock_immediate_transfer istock_immediate_transfer :stock_immediate_transfers){
                list.add((stock_immediate_transferImpl)istock_immediate_transfer) ;
            }
            stock_immediate_transferFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Istock_immediate_transfer stock_immediate_transfer){
        stock_immediate_transferFeignClient.remove(stock_immediate_transfer.getId()) ;
    }


    public Page<Istock_immediate_transfer> fetchDefault(SearchContext context){
        Page<stock_immediate_transferImpl> page = this.stock_immediate_transferFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
        if(stock_immediate_transfers!=null){
            List<stock_immediate_transferImpl> list = new ArrayList<stock_immediate_transferImpl>();
            for(Istock_immediate_transfer istock_immediate_transfer :stock_immediate_transfers){
                list.add((stock_immediate_transferImpl)istock_immediate_transfer) ;
            }
            stock_immediate_transferFeignClient.updateBatch(list) ;
        }
    }


    public Page<Istock_immediate_transfer> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_immediate_transfer stock_immediate_transfer){
        Istock_immediate_transfer clientModel = stock_immediate_transferFeignClient.getDraft(stock_immediate_transfer.getId(),(stock_immediate_transferImpl)stock_immediate_transfer) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_immediate_transfer.getClass(), false);
        copier.copy(clientModel, stock_immediate_transfer, null);
    }



}


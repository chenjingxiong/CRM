package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse_orderpoint;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouse_orderpointImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
public interface stock_warehouse_orderpointFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouse_orderpoints/createbatch")
    public stock_warehouse_orderpointImpl createBatch(@RequestBody List<stock_warehouse_orderpointImpl> stock_warehouse_orderpoints);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouse_orderpoints/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouse_orderpoints/updatebatch")
    public stock_warehouse_orderpointImpl updateBatch(@RequestBody List<stock_warehouse_orderpointImpl> stock_warehouse_orderpoints);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouse_orderpoints/{id}")
    public stock_warehouse_orderpointImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouse_orderpoints/removebatch")
    public stock_warehouse_orderpointImpl removeBatch(@RequestBody List<stock_warehouse_orderpointImpl> stock_warehouse_orderpoints);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouse_orderpoints/{id}")
    public stock_warehouse_orderpointImpl update(@PathVariable("id") Integer id,@RequestBody stock_warehouse_orderpointImpl stock_warehouse_orderpoint);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouse_orderpoints")
    public stock_warehouse_orderpointImpl create(@RequestBody stock_warehouse_orderpointImpl stock_warehouse_orderpoint);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouse_orderpoints/fetchdefault")
    public Page<stock_warehouse_orderpointImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouse_orderpoints/select")
    public Page<stock_warehouse_orderpointImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouse_orderpoints/{id}/getdraft")
    public stock_warehouse_orderpointImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warehouse_orderpointImpl stock_warehouse_orderpoint);



}

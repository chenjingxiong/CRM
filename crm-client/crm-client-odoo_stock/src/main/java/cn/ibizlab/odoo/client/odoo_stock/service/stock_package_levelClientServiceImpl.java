package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_package_level;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_package_levelClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_levelImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_package_levelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_package_level] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_package_levelClientServiceImpl implements Istock_package_levelClientService {

    stock_package_levelFeignClient stock_package_levelFeignClient;

    @Autowired
    public stock_package_levelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_package_levelFeignClient = nameBuilder.target(stock_package_levelFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_package_levelFeignClient = nameBuilder.target(stock_package_levelFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_package_level createModel() {
		return new stock_package_levelImpl();
	}


    public void createBatch(List<Istock_package_level> stock_package_levels){
        if(stock_package_levels!=null){
            List<stock_package_levelImpl> list = new ArrayList<stock_package_levelImpl>();
            for(Istock_package_level istock_package_level :stock_package_levels){
                list.add((stock_package_levelImpl)istock_package_level) ;
            }
            stock_package_levelFeignClient.createBatch(list) ;
        }
    }


    public void create(Istock_package_level stock_package_level){
        Istock_package_level clientModel = stock_package_levelFeignClient.create((stock_package_levelImpl)stock_package_level) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_level.getClass(), false);
        copier.copy(clientModel, stock_package_level, null);
    }


    public void update(Istock_package_level stock_package_level){
        Istock_package_level clientModel = stock_package_levelFeignClient.update(stock_package_level.getId(),(stock_package_levelImpl)stock_package_level) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_level.getClass(), false);
        copier.copy(clientModel, stock_package_level, null);
    }


    public void updateBatch(List<Istock_package_level> stock_package_levels){
        if(stock_package_levels!=null){
            List<stock_package_levelImpl> list = new ArrayList<stock_package_levelImpl>();
            for(Istock_package_level istock_package_level :stock_package_levels){
                list.add((stock_package_levelImpl)istock_package_level) ;
            }
            stock_package_levelFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_package_level> stock_package_levels){
        if(stock_package_levels!=null){
            List<stock_package_levelImpl> list = new ArrayList<stock_package_levelImpl>();
            for(Istock_package_level istock_package_level :stock_package_levels){
                list.add((stock_package_levelImpl)istock_package_level) ;
            }
            stock_package_levelFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_package_level> fetchDefault(SearchContext context){
        Page<stock_package_levelImpl> page = this.stock_package_levelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_package_level stock_package_level){
        stock_package_levelFeignClient.remove(stock_package_level.getId()) ;
    }


    public void get(Istock_package_level stock_package_level){
        Istock_package_level clientModel = stock_package_levelFeignClient.get(stock_package_level.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_level.getClass(), false);
        copier.copy(clientModel, stock_package_level, null);
    }


    public Page<Istock_package_level> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_package_level stock_package_level){
        Istock_package_level clientModel = stock_package_levelFeignClient.getDraft(stock_package_level.getId(),(stock_package_levelImpl)stock_package_level) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_package_level.getClass(), false);
        copier.copy(clientModel, stock_package_level, null);
    }



}


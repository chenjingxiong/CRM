package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_change_standard_price;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_standard_priceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
public interface stock_change_standard_priceFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_standard_prices/{id}")
    public stock_change_standard_priceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_standard_prices")
    public stock_change_standard_priceImpl create(@RequestBody stock_change_standard_priceImpl stock_change_standard_price);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_standard_prices/updatebatch")
    public stock_change_standard_priceImpl updateBatch(@RequestBody List<stock_change_standard_priceImpl> stock_change_standard_prices);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_standard_prices/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_standard_prices/fetchdefault")
    public Page<stock_change_standard_priceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_standard_prices/{id}")
    public stock_change_standard_priceImpl update(@PathVariable("id") Integer id,@RequestBody stock_change_standard_priceImpl stock_change_standard_price);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_standard_prices/removebatch")
    public stock_change_standard_priceImpl removeBatch(@RequestBody List<stock_change_standard_priceImpl> stock_change_standard_prices);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_standard_prices/createbatch")
    public stock_change_standard_priceImpl createBatch(@RequestBody List<stock_change_standard_priceImpl> stock_change_standard_prices);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_standard_prices/select")
    public Page<stock_change_standard_priceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_standard_prices/{id}/getdraft")
    public stock_change_standard_priceImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_change_standard_priceImpl stock_change_standard_price);



}

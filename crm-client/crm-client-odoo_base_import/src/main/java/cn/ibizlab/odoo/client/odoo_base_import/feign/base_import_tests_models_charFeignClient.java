package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_charImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
public interface base_import_tests_models_charFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_chars")
    public base_import_tests_models_charImpl create(@RequestBody base_import_tests_models_charImpl base_import_tests_models_char);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_chars/createbatch")
    public base_import_tests_models_charImpl createBatch(@RequestBody List<base_import_tests_models_charImpl> base_import_tests_models_chars);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_chars/fetchdefault")
    public Page<base_import_tests_models_charImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_chars/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_chars/{id}")
    public base_import_tests_models_charImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_chars/removebatch")
    public base_import_tests_models_charImpl removeBatch(@RequestBody List<base_import_tests_models_charImpl> base_import_tests_models_chars);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_chars/{id}")
    public base_import_tests_models_charImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_charImpl base_import_tests_models_char);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_chars/updatebatch")
    public base_import_tests_models_charImpl updateBatch(@RequestBody List<base_import_tests_models_charImpl> base_import_tests_models_chars);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_chars/select")
    public Page<base_import_tests_models_charImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_chars/{id}/getdraft")
    public base_import_tests_models_charImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_charImpl base_import_tests_models_char);



}

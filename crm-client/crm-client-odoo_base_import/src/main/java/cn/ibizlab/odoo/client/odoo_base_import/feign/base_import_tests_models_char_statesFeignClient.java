package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_states;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_statesImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char_states] 服务对象接口
 */
public interface base_import_tests_models_char_statesFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_states/updatebatch")
    public base_import_tests_models_char_statesImpl updateBatch(@RequestBody List<base_import_tests_models_char_statesImpl> base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_states/removebatch")
    public base_import_tests_models_char_statesImpl removeBatch(@RequestBody List<base_import_tests_models_char_statesImpl> base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_states/createbatch")
    public base_import_tests_models_char_statesImpl createBatch(@RequestBody List<base_import_tests_models_char_statesImpl> base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_states")
    public base_import_tests_models_char_statesImpl create(@RequestBody base_import_tests_models_char_statesImpl base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_states/{id}")
    public base_import_tests_models_char_statesImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_statesImpl base_import_tests_models_char_states);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_states/{id}")
    public base_import_tests_models_char_statesImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_states/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_states/fetchdefault")
    public Page<base_import_tests_models_char_statesImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_states/select")
    public Page<base_import_tests_models_char_statesImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_states/{id}/getdraft")
    public base_import_tests_models_char_statesImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_statesImpl base_import_tests_models_char_states);



}

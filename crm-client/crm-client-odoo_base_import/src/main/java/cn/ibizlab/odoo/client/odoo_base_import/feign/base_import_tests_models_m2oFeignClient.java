package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2oImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_m2o] 服务对象接口
 */
public interface base_import_tests_models_m2oFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2os")
    public base_import_tests_models_m2oImpl create(@RequestBody base_import_tests_models_m2oImpl base_import_tests_models_m2o);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2os/removebatch")
    public base_import_tests_models_m2oImpl removeBatch(@RequestBody List<base_import_tests_models_m2oImpl> base_import_tests_models_m2os);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2os/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2os/{id}")
    public base_import_tests_models_m2oImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2os/{id}")
    public base_import_tests_models_m2oImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_m2oImpl base_import_tests_models_m2o);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2os/updatebatch")
    public base_import_tests_models_m2oImpl updateBatch(@RequestBody List<base_import_tests_models_m2oImpl> base_import_tests_models_m2os);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2os/createbatch")
    public base_import_tests_models_m2oImpl createBatch(@RequestBody List<base_import_tests_models_m2oImpl> base_import_tests_models_m2os);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2os/fetchdefault")
    public Page<base_import_tests_models_m2oImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2os/select")
    public Page<base_import_tests_models_m2oImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2os/{id}/getdraft")
    public base_import_tests_models_m2oImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_m2oImpl base_import_tests_models_m2o);



}

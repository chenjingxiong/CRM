package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2o_requiredClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2o_requiredImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_m2o_requiredFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_m2o_required] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_m2o_requiredClientServiceImpl implements Ibase_import_tests_models_m2o_requiredClientService {

    base_import_tests_models_m2o_requiredFeignClient base_import_tests_models_m2o_requiredFeignClient;

    @Autowired
    public base_import_tests_models_m2o_requiredClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2o_requiredFeignClient = nameBuilder.target(base_import_tests_models_m2o_requiredFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2o_requiredFeignClient = nameBuilder.target(base_import_tests_models_m2o_requiredFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_m2o_required createModel() {
		return new base_import_tests_models_m2o_requiredImpl();
	}


    public void updateBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
        if(base_import_tests_models_m2o_requireds!=null){
            List<base_import_tests_models_m2o_requiredImpl> list = new ArrayList<base_import_tests_models_m2o_requiredImpl>();
            for(Ibase_import_tests_models_m2o_required ibase_import_tests_models_m2o_required :base_import_tests_models_m2o_requireds){
                list.add((base_import_tests_models_m2o_requiredImpl)ibase_import_tests_models_m2o_required) ;
            }
            base_import_tests_models_m2o_requiredFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
        Ibase_import_tests_models_m2o_required clientModel = base_import_tests_models_m2o_requiredFeignClient.create((base_import_tests_models_m2o_requiredImpl)base_import_tests_models_m2o_required) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
        if(base_import_tests_models_m2o_requireds!=null){
            List<base_import_tests_models_m2o_requiredImpl> list = new ArrayList<base_import_tests_models_m2o_requiredImpl>();
            for(Ibase_import_tests_models_m2o_required ibase_import_tests_models_m2o_required :base_import_tests_models_m2o_requireds){
                list.add((base_import_tests_models_m2o_requiredImpl)ibase_import_tests_models_m2o_required) ;
            }
            base_import_tests_models_m2o_requiredFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
        base_import_tests_models_m2o_requiredFeignClient.remove(base_import_tests_models_m2o_required.getId()) ;
    }


    public Page<Ibase_import_tests_models_m2o_required> fetchDefault(SearchContext context){
        Page<base_import_tests_models_m2o_requiredImpl> page = this.base_import_tests_models_m2o_requiredFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ibase_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds){
        if(base_import_tests_models_m2o_requireds!=null){
            List<base_import_tests_models_m2o_requiredImpl> list = new ArrayList<base_import_tests_models_m2o_requiredImpl>();
            for(Ibase_import_tests_models_m2o_required ibase_import_tests_models_m2o_required :base_import_tests_models_m2o_requireds){
                list.add((base_import_tests_models_m2o_requiredImpl)ibase_import_tests_models_m2o_required) ;
            }
            base_import_tests_models_m2o_requiredFeignClient.createBatch(list) ;
        }
    }


    public void update(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
        Ibase_import_tests_models_m2o_required clientModel = base_import_tests_models_m2o_requiredFeignClient.update(base_import_tests_models_m2o_required.getId(),(base_import_tests_models_m2o_requiredImpl)base_import_tests_models_m2o_required) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required, null);
    }


    public void get(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
        Ibase_import_tests_models_m2o_required clientModel = base_import_tests_models_m2o_requiredFeignClient.get(base_import_tests_models_m2o_required.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required, null);
    }


    public Page<Ibase_import_tests_models_m2o_required> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_m2o_required base_import_tests_models_m2o_required){
        Ibase_import_tests_models_m2o_required clientModel = base_import_tests_models_m2o_requiredFeignClient.getDraft(base_import_tests_models_m2o_required.getId(),(base_import_tests_models_m2o_requiredImpl)base_import_tests_models_m2o_required) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required, null);
    }



}


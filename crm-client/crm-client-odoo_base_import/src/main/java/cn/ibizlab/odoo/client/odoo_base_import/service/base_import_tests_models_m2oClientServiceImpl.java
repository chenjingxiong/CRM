package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2oClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2oImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_m2oFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_m2o] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_m2oClientServiceImpl implements Ibase_import_tests_models_m2oClientService {

    base_import_tests_models_m2oFeignClient base_import_tests_models_m2oFeignClient;

    @Autowired
    public base_import_tests_models_m2oClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2oFeignClient = nameBuilder.target(base_import_tests_models_m2oFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2oFeignClient = nameBuilder.target(base_import_tests_models_m2oFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_m2o createModel() {
		return new base_import_tests_models_m2oImpl();
	}


    public void create(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
        Ibase_import_tests_models_m2o clientModel = base_import_tests_models_m2oFeignClient.create((base_import_tests_models_m2oImpl)base_import_tests_models_m2o) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
        if(base_import_tests_models_m2os!=null){
            List<base_import_tests_models_m2oImpl> list = new ArrayList<base_import_tests_models_m2oImpl>();
            for(Ibase_import_tests_models_m2o ibase_import_tests_models_m2o :base_import_tests_models_m2os){
                list.add((base_import_tests_models_m2oImpl)ibase_import_tests_models_m2o) ;
            }
            base_import_tests_models_m2oFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
        base_import_tests_models_m2oFeignClient.remove(base_import_tests_models_m2o.getId()) ;
    }


    public void get(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
        Ibase_import_tests_models_m2o clientModel = base_import_tests_models_m2oFeignClient.get(base_import_tests_models_m2o.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o, null);
    }


    public void update(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
        Ibase_import_tests_models_m2o clientModel = base_import_tests_models_m2oFeignClient.update(base_import_tests_models_m2o.getId(),(base_import_tests_models_m2oImpl)base_import_tests_models_m2o) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
        if(base_import_tests_models_m2os!=null){
            List<base_import_tests_models_m2oImpl> list = new ArrayList<base_import_tests_models_m2oImpl>();
            for(Ibase_import_tests_models_m2o ibase_import_tests_models_m2o :base_import_tests_models_m2os){
                list.add((base_import_tests_models_m2oImpl)ibase_import_tests_models_m2o) ;
            }
            base_import_tests_models_m2oFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
        if(base_import_tests_models_m2os!=null){
            List<base_import_tests_models_m2oImpl> list = new ArrayList<base_import_tests_models_m2oImpl>();
            for(Ibase_import_tests_models_m2o ibase_import_tests_models_m2o :base_import_tests_models_m2os){
                list.add((base_import_tests_models_m2oImpl)ibase_import_tests_models_m2o) ;
            }
            base_import_tests_models_m2oFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_m2o> fetchDefault(SearchContext context){
        Page<base_import_tests_models_m2oImpl> page = this.base_import_tests_models_m2oFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_import_tests_models_m2o> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
        Ibase_import_tests_models_m2o clientModel = base_import_tests_models_m2oFeignClient.getDraft(base_import_tests_models_m2o.getId(),(base_import_tests_models_m2oImpl)base_import_tests_models_m2o) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o, null);
    }



}


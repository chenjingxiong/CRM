package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_mapping;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_mappingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_mapping] 服务对象接口
 */
public interface base_import_mappingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_mappings/{id}")
    public base_import_mappingImpl update(@PathVariable("id") Integer id,@RequestBody base_import_mappingImpl base_import_mapping);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_mappings/createbatch")
    public base_import_mappingImpl createBatch(@RequestBody List<base_import_mappingImpl> base_import_mappings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_mappings/removebatch")
    public base_import_mappingImpl removeBatch(@RequestBody List<base_import_mappingImpl> base_import_mappings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_mappings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_mappings/updatebatch")
    public base_import_mappingImpl updateBatch(@RequestBody List<base_import_mappingImpl> base_import_mappings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_mappings")
    public base_import_mappingImpl create(@RequestBody base_import_mappingImpl base_import_mapping);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_mappings/{id}")
    public base_import_mappingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_mappings/fetchdefault")
    public Page<base_import_mappingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_mappings/select")
    public Page<base_import_mappingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_mappings/{id}/getdraft")
    public base_import_mappingImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_mappingImpl base_import_mapping);



}

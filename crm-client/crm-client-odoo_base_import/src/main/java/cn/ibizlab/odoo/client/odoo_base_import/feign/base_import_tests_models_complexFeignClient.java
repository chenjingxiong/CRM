package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_complex;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_complexImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
public interface base_import_tests_models_complexFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_complices/{id}")
    public base_import_tests_models_complexImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_complexImpl base_import_tests_models_complex);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_complices/fetchdefault")
    public Page<base_import_tests_models_complexImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_complices/createbatch")
    public base_import_tests_models_complexImpl createBatch(@RequestBody List<base_import_tests_models_complexImpl> base_import_tests_models_complices);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_complices/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_complices/{id}")
    public base_import_tests_models_complexImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_complices")
    public base_import_tests_models_complexImpl create(@RequestBody base_import_tests_models_complexImpl base_import_tests_models_complex);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_complices/updatebatch")
    public base_import_tests_models_complexImpl updateBatch(@RequestBody List<base_import_tests_models_complexImpl> base_import_tests_models_complices);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_complices/removebatch")
    public base_import_tests_models_complexImpl removeBatch(@RequestBody List<base_import_tests_models_complexImpl> base_import_tests_models_complices);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_complices/select")
    public Page<base_import_tests_models_complexImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_complices/{id}/getdraft")
    public base_import_tests_models_complexImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_complexImpl base_import_tests_models_complex);



}

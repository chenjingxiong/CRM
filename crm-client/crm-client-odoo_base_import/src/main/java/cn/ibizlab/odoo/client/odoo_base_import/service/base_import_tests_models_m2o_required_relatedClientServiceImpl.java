package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required_related;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2o_required_relatedClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2o_required_relatedImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_m2o_required_relatedFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_m2o_required_related] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_m2o_required_relatedClientServiceImpl implements Ibase_import_tests_models_m2o_required_relatedClientService {

    base_import_tests_models_m2o_required_relatedFeignClient base_import_tests_models_m2o_required_relatedFeignClient;

    @Autowired
    public base_import_tests_models_m2o_required_relatedClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2o_required_relatedFeignClient = nameBuilder.target(base_import_tests_models_m2o_required_relatedFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_m2o_required_relatedFeignClient = nameBuilder.target(base_import_tests_models_m2o_required_relatedFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_m2o_required_related createModel() {
		return new base_import_tests_models_m2o_required_relatedImpl();
	}


    public void updateBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
        if(base_import_tests_models_m2o_required_relateds!=null){
            List<base_import_tests_models_m2o_required_relatedImpl> list = new ArrayList<base_import_tests_models_m2o_required_relatedImpl>();
            for(Ibase_import_tests_models_m2o_required_related ibase_import_tests_models_m2o_required_related :base_import_tests_models_m2o_required_relateds){
                list.add((base_import_tests_models_m2o_required_relatedImpl)ibase_import_tests_models_m2o_required_related) ;
            }
            base_import_tests_models_m2o_required_relatedFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
        if(base_import_tests_models_m2o_required_relateds!=null){
            List<base_import_tests_models_m2o_required_relatedImpl> list = new ArrayList<base_import_tests_models_m2o_required_relatedImpl>();
            for(Ibase_import_tests_models_m2o_required_related ibase_import_tests_models_m2o_required_related :base_import_tests_models_m2o_required_relateds){
                list.add((base_import_tests_models_m2o_required_relatedImpl)ibase_import_tests_models_m2o_required_related) ;
            }
            base_import_tests_models_m2o_required_relatedFeignClient.createBatch(list) ;
        }
    }


    public void create(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
        Ibase_import_tests_models_m2o_required_related clientModel = base_import_tests_models_m2o_required_relatedFeignClient.create((base_import_tests_models_m2o_required_relatedImpl)base_import_tests_models_m2o_required_related) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required_related.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required_related, null);
    }


    public void update(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
        Ibase_import_tests_models_m2o_required_related clientModel = base_import_tests_models_m2o_required_relatedFeignClient.update(base_import_tests_models_m2o_required_related.getId(),(base_import_tests_models_m2o_required_relatedImpl)base_import_tests_models_m2o_required_related) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required_related.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required_related, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
        if(base_import_tests_models_m2o_required_relateds!=null){
            List<base_import_tests_models_m2o_required_relatedImpl> list = new ArrayList<base_import_tests_models_m2o_required_relatedImpl>();
            for(Ibase_import_tests_models_m2o_required_related ibase_import_tests_models_m2o_required_related :base_import_tests_models_m2o_required_relateds){
                list.add((base_import_tests_models_m2o_required_relatedImpl)ibase_import_tests_models_m2o_required_related) ;
            }
            base_import_tests_models_m2o_required_relatedFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
        Ibase_import_tests_models_m2o_required_related clientModel = base_import_tests_models_m2o_required_relatedFeignClient.get(base_import_tests_models_m2o_required_related.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required_related.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required_related, null);
    }


    public Page<Ibase_import_tests_models_m2o_required_related> fetchDefault(SearchContext context){
        Page<base_import_tests_models_m2o_required_relatedImpl> page = this.base_import_tests_models_m2o_required_relatedFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
        base_import_tests_models_m2o_required_relatedFeignClient.remove(base_import_tests_models_m2o_required_related.getId()) ;
    }


    public Page<Ibase_import_tests_models_m2o_required_related> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
        Ibase_import_tests_models_m2o_required_related clientModel = base_import_tests_models_m2o_required_relatedFeignClient.getDraft(base_import_tests_models_m2o_required_related.getId(),(base_import_tests_models_m2o_required_relatedImpl)base_import_tests_models_m2o_required_related) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_m2o_required_related.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_m2o_required_related, null);
    }



}


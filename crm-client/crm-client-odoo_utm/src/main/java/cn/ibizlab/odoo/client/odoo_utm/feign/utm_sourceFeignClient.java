package cn.ibizlab.odoo.client.odoo_utm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iutm_source;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_sourceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[utm_source] 服务对象接口
 */
public interface utm_sourceFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_sources/createbatch")
    public utm_sourceImpl createBatch(@RequestBody List<utm_sourceImpl> utm_sources);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_sources/updatebatch")
    public utm_sourceImpl updateBatch(@RequestBody List<utm_sourceImpl> utm_sources);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_sources")
    public utm_sourceImpl create(@RequestBody utm_sourceImpl utm_source);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_sources/{id}")
    public utm_sourceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_sources/removebatch")
    public utm_sourceImpl removeBatch(@RequestBody List<utm_sourceImpl> utm_sources);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_sources/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_sources/fetchdefault")
    public Page<utm_sourceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_sources/{id}")
    public utm_sourceImpl update(@PathVariable("id") Integer id,@RequestBody utm_sourceImpl utm_source);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_sources/select")
    public Page<utm_sourceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_sources/{id}/getdraft")
    public utm_sourceImpl getDraft(@PathVariable("id") Integer id,@RequestBody utm_sourceImpl utm_source);



}

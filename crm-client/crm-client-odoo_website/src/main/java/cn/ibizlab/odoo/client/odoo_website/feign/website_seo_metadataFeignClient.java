package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_seo_metadata;
import cn.ibizlab.odoo.client.odoo_website.model.website_seo_metadataImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_seo_metadata] 服务对象接口
 */
public interface website_seo_metadataFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_seo_metadata/createbatch")
    public website_seo_metadataImpl createBatch(@RequestBody List<website_seo_metadataImpl> website_seo_metadata);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_seo_metadata/fetchdefault")
    public Page<website_seo_metadataImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_seo_metadata")
    public website_seo_metadataImpl create(@RequestBody website_seo_metadataImpl website_seo_metadata);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_seo_metadata/{id}")
    public website_seo_metadataImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_seo_metadata/updatebatch")
    public website_seo_metadataImpl updateBatch(@RequestBody List<website_seo_metadataImpl> website_seo_metadata);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_seo_metadata/removebatch")
    public website_seo_metadataImpl removeBatch(@RequestBody List<website_seo_metadataImpl> website_seo_metadata);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_seo_metadata/{id}")
    public website_seo_metadataImpl update(@PathVariable("id") Integer id,@RequestBody website_seo_metadataImpl website_seo_metadata);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_seo_metadata/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_seo_metadata/select")
    public Page<website_seo_metadataImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_seo_metadata/{id}/getdraft")
    public website_seo_metadataImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_seo_metadataImpl website_seo_metadata);



}

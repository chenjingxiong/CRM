package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_redirect;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_redirectClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_redirectImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_redirectFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_redirect] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_redirectClientServiceImpl implements Iwebsite_redirectClientService {

    website_redirectFeignClient website_redirectFeignClient;

    @Autowired
    public website_redirectClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_redirectFeignClient = nameBuilder.target(website_redirectFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_redirectFeignClient = nameBuilder.target(website_redirectFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_redirect createModel() {
		return new website_redirectImpl();
	}


    public void remove(Iwebsite_redirect website_redirect){
        website_redirectFeignClient.remove(website_redirect.getId()) ;
    }


    public void update(Iwebsite_redirect website_redirect){
        Iwebsite_redirect clientModel = website_redirectFeignClient.update(website_redirect.getId(),(website_redirectImpl)website_redirect) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_redirect.getClass(), false);
        copier.copy(clientModel, website_redirect, null);
    }


    public Page<Iwebsite_redirect> fetchDefault(SearchContext context){
        Page<website_redirectImpl> page = this.website_redirectFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iwebsite_redirect website_redirect){
        Iwebsite_redirect clientModel = website_redirectFeignClient.get(website_redirect.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_redirect.getClass(), false);
        copier.copy(clientModel, website_redirect, null);
    }


    public void createBatch(List<Iwebsite_redirect> website_redirects){
        if(website_redirects!=null){
            List<website_redirectImpl> list = new ArrayList<website_redirectImpl>();
            for(Iwebsite_redirect iwebsite_redirect :website_redirects){
                list.add((website_redirectImpl)iwebsite_redirect) ;
            }
            website_redirectFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iwebsite_redirect> website_redirects){
        if(website_redirects!=null){
            List<website_redirectImpl> list = new ArrayList<website_redirectImpl>();
            for(Iwebsite_redirect iwebsite_redirect :website_redirects){
                list.add((website_redirectImpl)iwebsite_redirect) ;
            }
            website_redirectFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iwebsite_redirect website_redirect){
        Iwebsite_redirect clientModel = website_redirectFeignClient.create((website_redirectImpl)website_redirect) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_redirect.getClass(), false);
        copier.copy(clientModel, website_redirect, null);
    }


    public void removeBatch(List<Iwebsite_redirect> website_redirects){
        if(website_redirects!=null){
            List<website_redirectImpl> list = new ArrayList<website_redirectImpl>();
            for(Iwebsite_redirect iwebsite_redirect :website_redirects){
                list.add((website_redirectImpl)iwebsite_redirect) ;
            }
            website_redirectFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iwebsite_redirect> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_redirect website_redirect){
        Iwebsite_redirect clientModel = website_redirectFeignClient.getDraft(website_redirect.getId(),(website_redirectImpl)website_redirect) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_redirect.getClass(), false);
        copier.copy(clientModel, website_redirect, null);
    }



}


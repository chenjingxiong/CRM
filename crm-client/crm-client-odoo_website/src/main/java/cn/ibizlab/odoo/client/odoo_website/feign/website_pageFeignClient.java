package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.client.odoo_website.model.website_pageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_page] 服务对象接口
 */
public interface website_pageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_pages/fetchdefault")
    public Page<website_pageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_pages/updatebatch")
    public website_pageImpl updateBatch(@RequestBody List<website_pageImpl> website_pages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_pages/{id}")
    public website_pageImpl update(@PathVariable("id") Integer id,@RequestBody website_pageImpl website_page);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_pages/{id}")
    public website_pageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_pages")
    public website_pageImpl create(@RequestBody website_pageImpl website_page);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_pages/createbatch")
    public website_pageImpl createBatch(@RequestBody List<website_pageImpl> website_pages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_pages/removebatch")
    public website_pageImpl removeBatch(@RequestBody List<website_pageImpl> website_pages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_pages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_pages/select")
    public Page<website_pageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_pages/{id}/getdraft")
    public website_pageImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_pageImpl website_page);



}

package cn.ibizlab.odoo.client.odoo_website.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[website_page] 对象
 */
public class website_pageImpl implements Iwebsite_page,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 视图结构
     */
    public String arch;

    @JsonIgnore
    public boolean archDirtyFlag;
    
    /**
     * 基础视图结构
     */
    public String arch_base;

    @JsonIgnore
    public boolean arch_baseDirtyFlag;
    
    /**
     * Arch Blob
     */
    public String arch_db;

    @JsonIgnore
    public boolean arch_dbDirtyFlag;
    
    /**
     * Arch 文件名
     */
    public String arch_fs;

    @JsonIgnore
    public boolean arch_fsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 作为可选继承显示
     */
    public String customize_show;

    @JsonIgnore
    public boolean customize_showDirtyFlag;
    
    /**
     * 发布日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_publish;

    @JsonIgnore
    public boolean date_publishDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 下级字段
     */
    public String field_parent;

    @JsonIgnore
    public boolean field_parentDirtyFlag;
    
    /**
     * 网站页面
     */
    public Integer first_page_id;

    @JsonIgnore
    public boolean first_page_idDirtyFlag;
    
    /**
     * 群组
     */
    public String groups_id;

    @JsonIgnore
    public boolean groups_idDirtyFlag;
    
    /**
     * 标题颜色
     */
    public String header_color;

    @JsonIgnore
    public boolean header_colorDirtyFlag;
    
    /**
     * 标题覆盖层
     */
    public String header_overlay;

    @JsonIgnore
    public boolean header_overlayDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 继承于此的视图
     */
    public String inherit_children_ids;

    @JsonIgnore
    public boolean inherit_children_idsDirtyFlag;
    
    /**
     * 继承的视图
     */
    public Integer inherit_id;

    @JsonIgnore
    public boolean inherit_idDirtyFlag;
    
    /**
     * 主页
     */
    public String is_homepage;

    @JsonIgnore
    public boolean is_homepageDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 可见
     */
    public String is_visible;

    @JsonIgnore
    public boolean is_visibleDirtyFlag;
    
    /**
     * 键
     */
    public String key;

    @JsonIgnore
    public boolean keyDirtyFlag;
    
    /**
     * 相关菜单
     */
    public String menu_ids;

    @JsonIgnore
    public boolean menu_idsDirtyFlag;
    
    /**
     * 视图继承模式
     */
    public String mode;

    @JsonIgnore
    public boolean modeDirtyFlag;
    
    /**
     * 模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 模型数据
     */
    public Integer model_data_id;

    @JsonIgnore
    public boolean model_data_idDirtyFlag;
    
    /**
     * 模型
     */
    public String model_ids;

    @JsonIgnore
    public boolean model_idsDirtyFlag;
    
    /**
     * 视图名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 页
     */
    public String page_ids;

    @JsonIgnore
    public boolean page_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 主题模板
     */
    public Integer theme_template_id;

    @JsonIgnore
    public boolean theme_template_idDirtyFlag;
    
    /**
     * 视图类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 页面 URL
     */
    public String url;

    @JsonIgnore
    public boolean urlDirtyFlag;
    
    /**
     * 视图
     */
    public Integer view_id;

    @JsonIgnore
    public boolean view_idDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 页面已索引
     */
    public String website_indexed;

    @JsonIgnore
    public boolean website_indexedDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 外部 ID
     */
    public String xml_id;

    @JsonIgnore
    public boolean xml_idDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [视图结构]
     */
    @JsonProperty("arch")
    public String getArch(){
        return this.arch ;
    }

    /**
     * 设置 [视图结构]
     */
    @JsonProperty("arch")
    public void setArch(String  arch){
        this.arch = arch ;
        this.archDirtyFlag = true ;
    }

     /**
     * 获取 [视图结构]脏标记
     */
    @JsonIgnore
    public boolean getArchDirtyFlag(){
        return this.archDirtyFlag ;
    }   

    /**
     * 获取 [基础视图结构]
     */
    @JsonProperty("arch_base")
    public String getArch_base(){
        return this.arch_base ;
    }

    /**
     * 设置 [基础视图结构]
     */
    @JsonProperty("arch_base")
    public void setArch_base(String  arch_base){
        this.arch_base = arch_base ;
        this.arch_baseDirtyFlag = true ;
    }

     /**
     * 获取 [基础视图结构]脏标记
     */
    @JsonIgnore
    public boolean getArch_baseDirtyFlag(){
        return this.arch_baseDirtyFlag ;
    }   

    /**
     * 获取 [Arch Blob]
     */
    @JsonProperty("arch_db")
    public String getArch_db(){
        return this.arch_db ;
    }

    /**
     * 设置 [Arch Blob]
     */
    @JsonProperty("arch_db")
    public void setArch_db(String  arch_db){
        this.arch_db = arch_db ;
        this.arch_dbDirtyFlag = true ;
    }

     /**
     * 获取 [Arch Blob]脏标记
     */
    @JsonIgnore
    public boolean getArch_dbDirtyFlag(){
        return this.arch_dbDirtyFlag ;
    }   

    /**
     * 获取 [Arch 文件名]
     */
    @JsonProperty("arch_fs")
    public String getArch_fs(){
        return this.arch_fs ;
    }

    /**
     * 设置 [Arch 文件名]
     */
    @JsonProperty("arch_fs")
    public void setArch_fs(String  arch_fs){
        this.arch_fs = arch_fs ;
        this.arch_fsDirtyFlag = true ;
    }

     /**
     * 获取 [Arch 文件名]脏标记
     */
    @JsonIgnore
    public boolean getArch_fsDirtyFlag(){
        return this.arch_fsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [作为可选继承显示]
     */
    @JsonProperty("customize_show")
    public String getCustomize_show(){
        return this.customize_show ;
    }

    /**
     * 设置 [作为可选继承显示]
     */
    @JsonProperty("customize_show")
    public void setCustomize_show(String  customize_show){
        this.customize_show = customize_show ;
        this.customize_showDirtyFlag = true ;
    }

     /**
     * 获取 [作为可选继承显示]脏标记
     */
    @JsonIgnore
    public boolean getCustomize_showDirtyFlag(){
        return this.customize_showDirtyFlag ;
    }   

    /**
     * 获取 [发布日期]
     */
    @JsonProperty("date_publish")
    public Timestamp getDate_publish(){
        return this.date_publish ;
    }

    /**
     * 设置 [发布日期]
     */
    @JsonProperty("date_publish")
    public void setDate_publish(Timestamp  date_publish){
        this.date_publish = date_publish ;
        this.date_publishDirtyFlag = true ;
    }

     /**
     * 获取 [发布日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_publishDirtyFlag(){
        return this.date_publishDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [下级字段]
     */
    @JsonProperty("field_parent")
    public String getField_parent(){
        return this.field_parent ;
    }

    /**
     * 设置 [下级字段]
     */
    @JsonProperty("field_parent")
    public void setField_parent(String  field_parent){
        this.field_parent = field_parent ;
        this.field_parentDirtyFlag = true ;
    }

     /**
     * 获取 [下级字段]脏标记
     */
    @JsonIgnore
    public boolean getField_parentDirtyFlag(){
        return this.field_parentDirtyFlag ;
    }   

    /**
     * 获取 [网站页面]
     */
    @JsonProperty("first_page_id")
    public Integer getFirst_page_id(){
        return this.first_page_id ;
    }

    /**
     * 设置 [网站页面]
     */
    @JsonProperty("first_page_id")
    public void setFirst_page_id(Integer  first_page_id){
        this.first_page_id = first_page_id ;
        this.first_page_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站页面]脏标记
     */
    @JsonIgnore
    public boolean getFirst_page_idDirtyFlag(){
        return this.first_page_idDirtyFlag ;
    }   

    /**
     * 获取 [群组]
     */
    @JsonProperty("groups_id")
    public String getGroups_id(){
        return this.groups_id ;
    }

    /**
     * 设置 [群组]
     */
    @JsonProperty("groups_id")
    public void setGroups_id(String  groups_id){
        this.groups_id = groups_id ;
        this.groups_idDirtyFlag = true ;
    }

     /**
     * 获取 [群组]脏标记
     */
    @JsonIgnore
    public boolean getGroups_idDirtyFlag(){
        return this.groups_idDirtyFlag ;
    }   

    /**
     * 获取 [标题颜色]
     */
    @JsonProperty("header_color")
    public String getHeader_color(){
        return this.header_color ;
    }

    /**
     * 设置 [标题颜色]
     */
    @JsonProperty("header_color")
    public void setHeader_color(String  header_color){
        this.header_color = header_color ;
        this.header_colorDirtyFlag = true ;
    }

     /**
     * 获取 [标题颜色]脏标记
     */
    @JsonIgnore
    public boolean getHeader_colorDirtyFlag(){
        return this.header_colorDirtyFlag ;
    }   

    /**
     * 获取 [标题覆盖层]
     */
    @JsonProperty("header_overlay")
    public String getHeader_overlay(){
        return this.header_overlay ;
    }

    /**
     * 设置 [标题覆盖层]
     */
    @JsonProperty("header_overlay")
    public void setHeader_overlay(String  header_overlay){
        this.header_overlay = header_overlay ;
        this.header_overlayDirtyFlag = true ;
    }

     /**
     * 获取 [标题覆盖层]脏标记
     */
    @JsonIgnore
    public boolean getHeader_overlayDirtyFlag(){
        return this.header_overlayDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [继承于此的视图]
     */
    @JsonProperty("inherit_children_ids")
    public String getInherit_children_ids(){
        return this.inherit_children_ids ;
    }

    /**
     * 设置 [继承于此的视图]
     */
    @JsonProperty("inherit_children_ids")
    public void setInherit_children_ids(String  inherit_children_ids){
        this.inherit_children_ids = inherit_children_ids ;
        this.inherit_children_idsDirtyFlag = true ;
    }

     /**
     * 获取 [继承于此的视图]脏标记
     */
    @JsonIgnore
    public boolean getInherit_children_idsDirtyFlag(){
        return this.inherit_children_idsDirtyFlag ;
    }   

    /**
     * 获取 [继承的视图]
     */
    @JsonProperty("inherit_id")
    public Integer getInherit_id(){
        return this.inherit_id ;
    }

    /**
     * 设置 [继承的视图]
     */
    @JsonProperty("inherit_id")
    public void setInherit_id(Integer  inherit_id){
        this.inherit_id = inherit_id ;
        this.inherit_idDirtyFlag = true ;
    }

     /**
     * 获取 [继承的视图]脏标记
     */
    @JsonIgnore
    public boolean getInherit_idDirtyFlag(){
        return this.inherit_idDirtyFlag ;
    }   

    /**
     * 获取 [主页]
     */
    @JsonProperty("is_homepage")
    public String getIs_homepage(){
        return this.is_homepage ;
    }

    /**
     * 设置 [主页]
     */
    @JsonProperty("is_homepage")
    public void setIs_homepage(String  is_homepage){
        this.is_homepage = is_homepage ;
        this.is_homepageDirtyFlag = true ;
    }

     /**
     * 获取 [主页]脏标记
     */
    @JsonIgnore
    public boolean getIs_homepageDirtyFlag(){
        return this.is_homepageDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [可见]
     */
    @JsonProperty("is_visible")
    public String getIs_visible(){
        return this.is_visible ;
    }

    /**
     * 设置 [可见]
     */
    @JsonProperty("is_visible")
    public void setIs_visible(String  is_visible){
        this.is_visible = is_visible ;
        this.is_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [可见]脏标记
     */
    @JsonIgnore
    public boolean getIs_visibleDirtyFlag(){
        return this.is_visibleDirtyFlag ;
    }   

    /**
     * 获取 [键]
     */
    @JsonProperty("key")
    public String getKey(){
        return this.key ;
    }

    /**
     * 设置 [键]
     */
    @JsonProperty("key")
    public void setKey(String  key){
        this.key = key ;
        this.keyDirtyFlag = true ;
    }

     /**
     * 获取 [键]脏标记
     */
    @JsonIgnore
    public boolean getKeyDirtyFlag(){
        return this.keyDirtyFlag ;
    }   

    /**
     * 获取 [相关菜单]
     */
    @JsonProperty("menu_ids")
    public String getMenu_ids(){
        return this.menu_ids ;
    }

    /**
     * 设置 [相关菜单]
     */
    @JsonProperty("menu_ids")
    public void setMenu_ids(String  menu_ids){
        this.menu_ids = menu_ids ;
        this.menu_idsDirtyFlag = true ;
    }

     /**
     * 获取 [相关菜单]脏标记
     */
    @JsonIgnore
    public boolean getMenu_idsDirtyFlag(){
        return this.menu_idsDirtyFlag ;
    }   

    /**
     * 获取 [视图继承模式]
     */
    @JsonProperty("mode")
    public String getMode(){
        return this.mode ;
    }

    /**
     * 设置 [视图继承模式]
     */
    @JsonProperty("mode")
    public void setMode(String  mode){
        this.mode = mode ;
        this.modeDirtyFlag = true ;
    }

     /**
     * 获取 [视图继承模式]脏标记
     */
    @JsonIgnore
    public boolean getModeDirtyFlag(){
        return this.modeDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [模型数据]
     */
    @JsonProperty("model_data_id")
    public Integer getModel_data_id(){
        return this.model_data_id ;
    }

    /**
     * 设置 [模型数据]
     */
    @JsonProperty("model_data_id")
    public void setModel_data_id(Integer  model_data_id){
        this.model_data_id = model_data_id ;
        this.model_data_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型数据]脏标记
     */
    @JsonIgnore
    public boolean getModel_data_idDirtyFlag(){
        return this.model_data_idDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model_ids")
    public String getModel_ids(){
        return this.model_ids ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model_ids")
    public void setModel_ids(String  model_ids){
        this.model_ids = model_ids ;
        this.model_idsDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModel_idsDirtyFlag(){
        return this.model_idsDirtyFlag ;
    }   

    /**
     * 获取 [视图名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [视图名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [视图名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [页]
     */
    @JsonProperty("page_ids")
    public String getPage_ids(){
        return this.page_ids ;
    }

    /**
     * 设置 [页]
     */
    @JsonProperty("page_ids")
    public void setPage_ids(String  page_ids){
        this.page_ids = page_ids ;
        this.page_idsDirtyFlag = true ;
    }

     /**
     * 获取 [页]脏标记
     */
    @JsonIgnore
    public boolean getPage_idsDirtyFlag(){
        return this.page_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("priority")
    public Integer getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("priority")
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return this.theme_template_id ;
    }

    /**
     * 设置 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [主题模板]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return this.theme_template_idDirtyFlag ;
    }   

    /**
     * 获取 [视图类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [视图类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [视图类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [页面 URL]
     */
    @JsonProperty("url")
    public String getUrl(){
        return this.url ;
    }

    /**
     * 设置 [页面 URL]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

     /**
     * 获取 [页面 URL]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return this.urlDirtyFlag ;
    }   

    /**
     * 获取 [视图]
     */
    @JsonProperty("view_id")
    public Integer getView_id(){
        return this.view_id ;
    }

    /**
     * 设置 [视图]
     */
    @JsonProperty("view_id")
    public void setView_id(Integer  view_id){
        this.view_id = view_id ;
        this.view_idDirtyFlag = true ;
    }

     /**
     * 获取 [视图]脏标记
     */
    @JsonIgnore
    public boolean getView_idDirtyFlag(){
        return this.view_idDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [页面已索引]
     */
    @JsonProperty("website_indexed")
    public String getWebsite_indexed(){
        return this.website_indexed ;
    }

    /**
     * 设置 [页面已索引]
     */
    @JsonProperty("website_indexed")
    public void setWebsite_indexed(String  website_indexed){
        this.website_indexed = website_indexed ;
        this.website_indexedDirtyFlag = true ;
    }

     /**
     * 获取 [页面已索引]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_indexedDirtyFlag(){
        return this.website_indexedDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [外部 ID]
     */
    @JsonProperty("xml_id")
    public String getXml_id(){
        return this.xml_id ;
    }

    /**
     * 设置 [外部 ID]
     */
    @JsonProperty("xml_id")
    public void setXml_id(String  xml_id){
        this.xml_id = xml_id ;
        this.xml_idDirtyFlag = true ;
    }

     /**
     * 获取 [外部 ID]脏标记
     */
    @JsonIgnore
    public boolean getXml_idDirtyFlag(){
        return this.xml_idDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

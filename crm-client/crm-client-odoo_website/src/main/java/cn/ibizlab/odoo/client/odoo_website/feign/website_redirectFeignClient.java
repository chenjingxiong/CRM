package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_redirect;
import cn.ibizlab.odoo.client.odoo_website.model.website_redirectImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_redirect] 服务对象接口
 */
public interface website_redirectFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_redirects/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_redirects/{id}")
    public website_redirectImpl update(@PathVariable("id") Integer id,@RequestBody website_redirectImpl website_redirect);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_redirects/fetchdefault")
    public Page<website_redirectImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_redirects/{id}")
    public website_redirectImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_redirects/createbatch")
    public website_redirectImpl createBatch(@RequestBody List<website_redirectImpl> website_redirects);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_redirects/updatebatch")
    public website_redirectImpl updateBatch(@RequestBody List<website_redirectImpl> website_redirects);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_redirects")
    public website_redirectImpl create(@RequestBody website_redirectImpl website_redirect);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_redirects/removebatch")
    public website_redirectImpl removeBatch(@RequestBody List<website_redirectImpl> website_redirects);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_redirects/select")
    public Page<website_redirectImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_redirects/{id}/getdraft")
    public website_redirectImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_redirectImpl website_redirect);



}

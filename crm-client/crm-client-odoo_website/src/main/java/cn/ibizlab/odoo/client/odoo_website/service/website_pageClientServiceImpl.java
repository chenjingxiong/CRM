package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_pageClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_pageImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_pageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_page] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_pageClientServiceImpl implements Iwebsite_pageClientService {

    website_pageFeignClient website_pageFeignClient;

    @Autowired
    public website_pageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_pageFeignClient = nameBuilder.target(website_pageFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_pageFeignClient = nameBuilder.target(website_pageFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_page createModel() {
		return new website_pageImpl();
	}


    public Page<Iwebsite_page> fetchDefault(SearchContext context){
        Page<website_pageImpl> page = this.website_pageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iwebsite_page> website_pages){
        if(website_pages!=null){
            List<website_pageImpl> list = new ArrayList<website_pageImpl>();
            for(Iwebsite_page iwebsite_page :website_pages){
                list.add((website_pageImpl)iwebsite_page) ;
            }
            website_pageFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iwebsite_page website_page){
        Iwebsite_page clientModel = website_pageFeignClient.update(website_page.getId(),(website_pageImpl)website_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_page.getClass(), false);
        copier.copy(clientModel, website_page, null);
    }


    public void get(Iwebsite_page website_page){
        Iwebsite_page clientModel = website_pageFeignClient.get(website_page.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_page.getClass(), false);
        copier.copy(clientModel, website_page, null);
    }


    public void create(Iwebsite_page website_page){
        Iwebsite_page clientModel = website_pageFeignClient.create((website_pageImpl)website_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_page.getClass(), false);
        copier.copy(clientModel, website_page, null);
    }


    public void createBatch(List<Iwebsite_page> website_pages){
        if(website_pages!=null){
            List<website_pageImpl> list = new ArrayList<website_pageImpl>();
            for(Iwebsite_page iwebsite_page :website_pages){
                list.add((website_pageImpl)iwebsite_page) ;
            }
            website_pageFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iwebsite_page> website_pages){
        if(website_pages!=null){
            List<website_pageImpl> list = new ArrayList<website_pageImpl>();
            for(Iwebsite_page iwebsite_page :website_pages){
                list.add((website_pageImpl)iwebsite_page) ;
            }
            website_pageFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iwebsite_page website_page){
        website_pageFeignClient.remove(website_page.getId()) ;
    }


    public Page<Iwebsite_page> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_page website_page){
        Iwebsite_page clientModel = website_pageFeignClient.getDraft(website_page.getId(),(website_pageImpl)website_page) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_page.getClass(), false);
        copier.copy(clientModel, website_page, null);
    }



}


package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_published_mixin;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_published_mixinClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_published_mixinImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_published_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_published_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_published_mixinClientServiceImpl implements Iwebsite_published_mixinClientService {

    website_published_mixinFeignClient website_published_mixinFeignClient;

    @Autowired
    public website_published_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_published_mixinFeignClient = nameBuilder.target(website_published_mixinFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_published_mixinFeignClient = nameBuilder.target(website_published_mixinFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_published_mixin createModel() {
		return new website_published_mixinImpl();
	}


    public Page<Iwebsite_published_mixin> fetchDefault(SearchContext context){
        Page<website_published_mixinImpl> page = this.website_published_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iwebsite_published_mixin website_published_mixin){
        website_published_mixinFeignClient.remove(website_published_mixin.getId()) ;
    }


    public void updateBatch(List<Iwebsite_published_mixin> website_published_mixins){
        if(website_published_mixins!=null){
            List<website_published_mixinImpl> list = new ArrayList<website_published_mixinImpl>();
            for(Iwebsite_published_mixin iwebsite_published_mixin :website_published_mixins){
                list.add((website_published_mixinImpl)iwebsite_published_mixin) ;
            }
            website_published_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iwebsite_published_mixin> website_published_mixins){
        if(website_published_mixins!=null){
            List<website_published_mixinImpl> list = new ArrayList<website_published_mixinImpl>();
            for(Iwebsite_published_mixin iwebsite_published_mixin :website_published_mixins){
                list.add((website_published_mixinImpl)iwebsite_published_mixin) ;
            }
            website_published_mixinFeignClient.createBatch(list) ;
        }
    }


    public void create(Iwebsite_published_mixin website_published_mixin){
        Iwebsite_published_mixin clientModel = website_published_mixinFeignClient.create((website_published_mixinImpl)website_published_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_published_mixin.getClass(), false);
        copier.copy(clientModel, website_published_mixin, null);
    }


    public void update(Iwebsite_published_mixin website_published_mixin){
        Iwebsite_published_mixin clientModel = website_published_mixinFeignClient.update(website_published_mixin.getId(),(website_published_mixinImpl)website_published_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_published_mixin.getClass(), false);
        copier.copy(clientModel, website_published_mixin, null);
    }


    public void get(Iwebsite_published_mixin website_published_mixin){
        Iwebsite_published_mixin clientModel = website_published_mixinFeignClient.get(website_published_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_published_mixin.getClass(), false);
        copier.copy(clientModel, website_published_mixin, null);
    }


    public void removeBatch(List<Iwebsite_published_mixin> website_published_mixins){
        if(website_published_mixins!=null){
            List<website_published_mixinImpl> list = new ArrayList<website_published_mixinImpl>();
            for(Iwebsite_published_mixin iwebsite_published_mixin :website_published_mixins){
                list.add((website_published_mixinImpl)iwebsite_published_mixin) ;
            }
            website_published_mixinFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iwebsite_published_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_published_mixin website_published_mixin){
        Iwebsite_published_mixin clientModel = website_published_mixinFeignClient.getDraft(website_published_mixin.getId(),(website_published_mixinImpl)website_published_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_published_mixin.getClass(), false);
        copier.copy(clientModel, website_published_mixin, null);
    }



}


package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_menuClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_menuImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_menuFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_menu] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_menuClientServiceImpl implements Iwebsite_menuClientService {

    website_menuFeignClient website_menuFeignClient;

    @Autowired
    public website_menuClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_menuFeignClient = nameBuilder.target(website_menuFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_menuFeignClient = nameBuilder.target(website_menuFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_menu createModel() {
		return new website_menuImpl();
	}


    public void get(Iwebsite_menu website_menu){
        Iwebsite_menu clientModel = website_menuFeignClient.get(website_menu.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_menu.getClass(), false);
        copier.copy(clientModel, website_menu, null);
    }


    public void removeBatch(List<Iwebsite_menu> website_menus){
        if(website_menus!=null){
            List<website_menuImpl> list = new ArrayList<website_menuImpl>();
            for(Iwebsite_menu iwebsite_menu :website_menus){
                list.add((website_menuImpl)iwebsite_menu) ;
            }
            website_menuFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iwebsite_menu> website_menus){
        if(website_menus!=null){
            List<website_menuImpl> list = new ArrayList<website_menuImpl>();
            for(Iwebsite_menu iwebsite_menu :website_menus){
                list.add((website_menuImpl)iwebsite_menu) ;
            }
            website_menuFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iwebsite_menu website_menu){
        Iwebsite_menu clientModel = website_menuFeignClient.create((website_menuImpl)website_menu) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_menu.getClass(), false);
        copier.copy(clientModel, website_menu, null);
    }


    public void remove(Iwebsite_menu website_menu){
        website_menuFeignClient.remove(website_menu.getId()) ;
    }


    public void update(Iwebsite_menu website_menu){
        Iwebsite_menu clientModel = website_menuFeignClient.update(website_menu.getId(),(website_menuImpl)website_menu) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_menu.getClass(), false);
        copier.copy(clientModel, website_menu, null);
    }


    public Page<Iwebsite_menu> fetchDefault(SearchContext context){
        Page<website_menuImpl> page = this.website_menuFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iwebsite_menu> website_menus){
        if(website_menus!=null){
            List<website_menuImpl> list = new ArrayList<website_menuImpl>();
            for(Iwebsite_menu iwebsite_menu :website_menus){
                list.add((website_menuImpl)iwebsite_menu) ;
            }
            website_menuFeignClient.createBatch(list) ;
        }
    }


    public Page<Iwebsite_menu> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_menu website_menu){
        Iwebsite_menu clientModel = website_menuFeignClient.getDraft(website_menu.getId(),(website_menuImpl)website_menu) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_menu.getClass(), false);
        copier.copy(clientModel, website_menu, null);
    }



}


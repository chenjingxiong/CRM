package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_seo_metadata;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_seo_metadataClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_seo_metadataImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_seo_metadataFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_seo_metadata] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_seo_metadataClientServiceImpl implements Iwebsite_seo_metadataClientService {

    website_seo_metadataFeignClient website_seo_metadataFeignClient;

    @Autowired
    public website_seo_metadataClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_seo_metadataFeignClient = nameBuilder.target(website_seo_metadataFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_seo_metadataFeignClient = nameBuilder.target(website_seo_metadataFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_seo_metadata createModel() {
		return new website_seo_metadataImpl();
	}


    public void createBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
        if(website_seo_metadata!=null){
            List<website_seo_metadataImpl> list = new ArrayList<website_seo_metadataImpl>();
            for(Iwebsite_seo_metadata iwebsite_seo_metadata :website_seo_metadata){
                list.add((website_seo_metadataImpl)iwebsite_seo_metadata) ;
            }
            website_seo_metadataFeignClient.createBatch(list) ;
        }
    }


    public Page<Iwebsite_seo_metadata> fetchDefault(SearchContext context){
        Page<website_seo_metadataImpl> page = this.website_seo_metadataFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iwebsite_seo_metadata website_seo_metadata){
        Iwebsite_seo_metadata clientModel = website_seo_metadataFeignClient.create((website_seo_metadataImpl)website_seo_metadata) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_seo_metadata.getClass(), false);
        copier.copy(clientModel, website_seo_metadata, null);
    }


    public void get(Iwebsite_seo_metadata website_seo_metadata){
        Iwebsite_seo_metadata clientModel = website_seo_metadataFeignClient.get(website_seo_metadata.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_seo_metadata.getClass(), false);
        copier.copy(clientModel, website_seo_metadata, null);
    }


    public void updateBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
        if(website_seo_metadata!=null){
            List<website_seo_metadataImpl> list = new ArrayList<website_seo_metadataImpl>();
            for(Iwebsite_seo_metadata iwebsite_seo_metadata :website_seo_metadata){
                list.add((website_seo_metadataImpl)iwebsite_seo_metadata) ;
            }
            website_seo_metadataFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iwebsite_seo_metadata> website_seo_metadata){
        if(website_seo_metadata!=null){
            List<website_seo_metadataImpl> list = new ArrayList<website_seo_metadataImpl>();
            for(Iwebsite_seo_metadata iwebsite_seo_metadata :website_seo_metadata){
                list.add((website_seo_metadataImpl)iwebsite_seo_metadata) ;
            }
            website_seo_metadataFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iwebsite_seo_metadata website_seo_metadata){
        Iwebsite_seo_metadata clientModel = website_seo_metadataFeignClient.update(website_seo_metadata.getId(),(website_seo_metadataImpl)website_seo_metadata) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_seo_metadata.getClass(), false);
        copier.copy(clientModel, website_seo_metadata, null);
    }


    public void remove(Iwebsite_seo_metadata website_seo_metadata){
        website_seo_metadataFeignClient.remove(website_seo_metadata.getId()) ;
    }


    public Page<Iwebsite_seo_metadata> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_seo_metadata website_seo_metadata){
        Iwebsite_seo_metadata clientModel = website_seo_metadataFeignClient.getDraft(website_seo_metadata.getId(),(website_seo_metadataImpl)website_seo_metadata) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_seo_metadata.getClass(), false);
        copier.copy(clientModel, website_seo_metadata, null);
    }



}


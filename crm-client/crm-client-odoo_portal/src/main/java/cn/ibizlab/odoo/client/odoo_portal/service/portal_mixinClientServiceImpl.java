package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_mixin;
import cn.ibizlab.odoo.client.odoo_portal.config.odoo_portalClientProperties;
import cn.ibizlab.odoo.core.client.service.Iportal_mixinClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_mixinImpl;
import cn.ibizlab.odoo.client.odoo_portal.feign.portal_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[portal_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class portal_mixinClientServiceImpl implements Iportal_mixinClientService {

    portal_mixinFeignClient portal_mixinFeignClient;

    @Autowired
    public portal_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_portalClientProperties odoo_portalClientProperties) {
        if (odoo_portalClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_mixinFeignClient = nameBuilder.target(portal_mixinFeignClient.class,"http://"+odoo_portalClientProperties.getServiceId()+"/") ;
		}else if (odoo_portalClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_mixinFeignClient = nameBuilder.target(portal_mixinFeignClient.class,odoo_portalClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iportal_mixin createModel() {
		return new portal_mixinImpl();
	}


    public void create(Iportal_mixin portal_mixin){
        Iportal_mixin clientModel = portal_mixinFeignClient.create((portal_mixinImpl)portal_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_mixin.getClass(), false);
        copier.copy(clientModel, portal_mixin, null);
    }


    public Page<Iportal_mixin> fetchDefault(SearchContext context){
        Page<portal_mixinImpl> page = this.portal_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iportal_mixin> portal_mixins){
        if(portal_mixins!=null){
            List<portal_mixinImpl> list = new ArrayList<portal_mixinImpl>();
            for(Iportal_mixin iportal_mixin :portal_mixins){
                list.add((portal_mixinImpl)iportal_mixin) ;
            }
            portal_mixinFeignClient.createBatch(list) ;
        }
    }


    public void update(Iportal_mixin portal_mixin){
        Iportal_mixin clientModel = portal_mixinFeignClient.update(portal_mixin.getId(),(portal_mixinImpl)portal_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_mixin.getClass(), false);
        copier.copy(clientModel, portal_mixin, null);
    }


    public void remove(Iportal_mixin portal_mixin){
        portal_mixinFeignClient.remove(portal_mixin.getId()) ;
    }


    public void updateBatch(List<Iportal_mixin> portal_mixins){
        if(portal_mixins!=null){
            List<portal_mixinImpl> list = new ArrayList<portal_mixinImpl>();
            for(Iportal_mixin iportal_mixin :portal_mixins){
                list.add((portal_mixinImpl)iportal_mixin) ;
            }
            portal_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iportal_mixin> portal_mixins){
        if(portal_mixins!=null){
            List<portal_mixinImpl> list = new ArrayList<portal_mixinImpl>();
            for(Iportal_mixin iportal_mixin :portal_mixins){
                list.add((portal_mixinImpl)iportal_mixin) ;
            }
            portal_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iportal_mixin portal_mixin){
        Iportal_mixin clientModel = portal_mixinFeignClient.get(portal_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_mixin.getClass(), false);
        copier.copy(clientModel, portal_mixin, null);
    }


    public Page<Iportal_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iportal_mixin portal_mixin){
        Iportal_mixin clientModel = portal_mixinFeignClient.getDraft(portal_mixin.getId(),(portal_mixinImpl)portal_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_mixin.getClass(), false);
        copier.copy(clientModel, portal_mixin, null);
    }



}


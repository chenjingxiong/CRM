package cn.ibizlab.odoo.client.odoo_portal.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[portal_wizard] 服务对象接口
 */
public interface portal_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizards/updatebatch")
    public portal_wizardImpl updateBatch(@RequestBody List<portal_wizardImpl> portal_wizards);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizards/createbatch")
    public portal_wizardImpl createBatch(@RequestBody List<portal_wizardImpl> portal_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizards/removebatch")
    public portal_wizardImpl removeBatch(@RequestBody List<portal_wizardImpl> portal_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizards/fetchdefault")
    public Page<portal_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizards")
    public portal_wizardImpl create(@RequestBody portal_wizardImpl portal_wizard);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizards/{id}")
    public portal_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizards/{id}")
    public portal_wizardImpl update(@PathVariable("id") Integer id,@RequestBody portal_wizardImpl portal_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizards/select")
    public Page<portal_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizards/{id}/getdraft")
    public portal_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody portal_wizardImpl portal_wizard);



}

package cn.ibizlab.odoo.client.odoo_portal.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iportal_mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[portal_mixin] 对象
 */
public class portal_mixinImpl implements Iportal_mixin,Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 门户访问 URL
     */
    public String access_url;

    @JsonIgnore
    public boolean access_urlDirtyFlag;
    
    /**
     * 访问警告
     */
    public String access_warning;

    @JsonIgnore
    public boolean access_warningDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [门户访问 URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问 URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

     /**
     * 获取 [门户访问 URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }   

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

     /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

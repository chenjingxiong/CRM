package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_mixin;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_mixin] 服务对象接口
 */
public interface resource_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_mixins")
    public resource_mixinImpl create(@RequestBody resource_mixinImpl resource_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_mixins/createbatch")
    public resource_mixinImpl createBatch(@RequestBody List<resource_mixinImpl> resource_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_mixins/fetchdefault")
    public Page<resource_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_mixins/removebatch")
    public resource_mixinImpl removeBatch(@RequestBody List<resource_mixinImpl> resource_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_mixins/{id}")
    public resource_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_mixins/{id}")
    public resource_mixinImpl update(@PathVariable("id") Integer id,@RequestBody resource_mixinImpl resource_mixin);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_mixins/updatebatch")
    public resource_mixinImpl updateBatch(@RequestBody List<resource_mixinImpl> resource_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_mixins/select")
    public Page<resource_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_mixins/{id}/getdraft")
    public resource_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_mixinImpl resource_mixin);



}

package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_attendanceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
public interface resource_calendar_attendanceFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_attendances/removebatch")
    public resource_calendar_attendanceImpl removeBatch(@RequestBody List<resource_calendar_attendanceImpl> resource_calendar_attendances);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_attendances/{id}")
    public resource_calendar_attendanceImpl update(@PathVariable("id") Integer id,@RequestBody resource_calendar_attendanceImpl resource_calendar_attendance);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_attendances/{id}")
    public resource_calendar_attendanceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_attendances/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_attendances/updatebatch")
    public resource_calendar_attendanceImpl updateBatch(@RequestBody List<resource_calendar_attendanceImpl> resource_calendar_attendances);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_attendances")
    public resource_calendar_attendanceImpl create(@RequestBody resource_calendar_attendanceImpl resource_calendar_attendance);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_attendances/fetchdefault")
    public Page<resource_calendar_attendanceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_attendances/createbatch")
    public resource_calendar_attendanceImpl createBatch(@RequestBody List<resource_calendar_attendanceImpl> resource_calendar_attendances);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_attendances/select")
    public Page<resource_calendar_attendanceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_attendances/{id}/getdraft")
    public resource_calendar_attendanceImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_calendar_attendanceImpl resource_calendar_attendance);



}

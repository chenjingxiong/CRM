package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_test;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_testClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_testImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_testFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_test] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_testClientServiceImpl implements Iresource_testClientService {

    resource_testFeignClient resource_testFeignClient;

    @Autowired
    public resource_testClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_testFeignClient = nameBuilder.target(resource_testFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_testFeignClient = nameBuilder.target(resource_testFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_test createModel() {
		return new resource_testImpl();
	}


    public void remove(Iresource_test resource_test){
        resource_testFeignClient.remove(resource_test.getId()) ;
    }


    public void createBatch(List<Iresource_test> resource_tests){
        if(resource_tests!=null){
            List<resource_testImpl> list = new ArrayList<resource_testImpl>();
            for(Iresource_test iresource_test :resource_tests){
                list.add((resource_testImpl)iresource_test) ;
            }
            resource_testFeignClient.createBatch(list) ;
        }
    }


    public void create(Iresource_test resource_test){
        Iresource_test clientModel = resource_testFeignClient.create((resource_testImpl)resource_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_test.getClass(), false);
        copier.copy(clientModel, resource_test, null);
    }


    public Page<Iresource_test> fetchDefault(SearchContext context){
        Page<resource_testImpl> page = this.resource_testFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iresource_test resource_test){
        Iresource_test clientModel = resource_testFeignClient.update(resource_test.getId(),(resource_testImpl)resource_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_test.getClass(), false);
        copier.copy(clientModel, resource_test, null);
    }


    public void get(Iresource_test resource_test){
        Iresource_test clientModel = resource_testFeignClient.get(resource_test.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_test.getClass(), false);
        copier.copy(clientModel, resource_test, null);
    }


    public void removeBatch(List<Iresource_test> resource_tests){
        if(resource_tests!=null){
            List<resource_testImpl> list = new ArrayList<resource_testImpl>();
            for(Iresource_test iresource_test :resource_tests){
                list.add((resource_testImpl)iresource_test) ;
            }
            resource_testFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iresource_test> resource_tests){
        if(resource_tests!=null){
            List<resource_testImpl> list = new ArrayList<resource_testImpl>();
            for(Iresource_test iresource_test :resource_tests){
                list.add((resource_testImpl)iresource_test) ;
            }
            resource_testFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iresource_test> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_test resource_test){
        Iresource_test clientModel = resource_testFeignClient.getDraft(resource_test.getId(),(resource_testImpl)resource_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_test.getClass(), false);
        copier.copy(clientModel, resource_test, null);
    }



}


package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_leaves;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_leavesImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_calendar_leaves] 服务对象接口
 */
public interface resource_calendar_leavesFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_leaves/updatebatch")
    public resource_calendar_leavesImpl updateBatch(@RequestBody List<resource_calendar_leavesImpl> resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_leaves/{id}")
    public resource_calendar_leavesImpl update(@PathVariable("id") Integer id,@RequestBody resource_calendar_leavesImpl resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_leaves/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_leaves/{id}")
    public resource_calendar_leavesImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_leaves/fetchdefault")
    public Page<resource_calendar_leavesImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_leaves")
    public resource_calendar_leavesImpl create(@RequestBody resource_calendar_leavesImpl resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_leaves/createbatch")
    public resource_calendar_leavesImpl createBatch(@RequestBody List<resource_calendar_leavesImpl> resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_leaves/removebatch")
    public resource_calendar_leavesImpl removeBatch(@RequestBody List<resource_calendar_leavesImpl> resource_calendar_leaves);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_leaves/select")
    public Page<resource_calendar_leavesImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_leaves/{id}/getdraft")
    public resource_calendar_leavesImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_calendar_leavesImpl resource_calendar_leaves);



}

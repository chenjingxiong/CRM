package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_resource;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_resourceClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_resourceImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_resourceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_resource] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_resourceClientServiceImpl implements Iresource_resourceClientService {

    resource_resourceFeignClient resource_resourceFeignClient;

    @Autowired
    public resource_resourceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_resourceFeignClient = nameBuilder.target(resource_resourceFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_resourceFeignClient = nameBuilder.target(resource_resourceFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_resource createModel() {
		return new resource_resourceImpl();
	}


    public void updateBatch(List<Iresource_resource> resource_resources){
        if(resource_resources!=null){
            List<resource_resourceImpl> list = new ArrayList<resource_resourceImpl>();
            for(Iresource_resource iresource_resource :resource_resources){
                list.add((resource_resourceImpl)iresource_resource) ;
            }
            resource_resourceFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iresource_resource resource_resource){
        resource_resourceFeignClient.remove(resource_resource.getId()) ;
    }


    public void removeBatch(List<Iresource_resource> resource_resources){
        if(resource_resources!=null){
            List<resource_resourceImpl> list = new ArrayList<resource_resourceImpl>();
            for(Iresource_resource iresource_resource :resource_resources){
                list.add((resource_resourceImpl)iresource_resource) ;
            }
            resource_resourceFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iresource_resource resource_resource){
        Iresource_resource clientModel = resource_resourceFeignClient.get(resource_resource.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_resource.getClass(), false);
        copier.copy(clientModel, resource_resource, null);
    }


    public void createBatch(List<Iresource_resource> resource_resources){
        if(resource_resources!=null){
            List<resource_resourceImpl> list = new ArrayList<resource_resourceImpl>();
            for(Iresource_resource iresource_resource :resource_resources){
                list.add((resource_resourceImpl)iresource_resource) ;
            }
            resource_resourceFeignClient.createBatch(list) ;
        }
    }


    public Page<Iresource_resource> fetchDefault(SearchContext context){
        Page<resource_resourceImpl> page = this.resource_resourceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iresource_resource resource_resource){
        Iresource_resource clientModel = resource_resourceFeignClient.update(resource_resource.getId(),(resource_resourceImpl)resource_resource) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_resource.getClass(), false);
        copier.copy(clientModel, resource_resource, null);
    }


    public void create(Iresource_resource resource_resource){
        Iresource_resource clientModel = resource_resourceFeignClient.create((resource_resourceImpl)resource_resource) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_resource.getClass(), false);
        copier.copy(clientModel, resource_resource, null);
    }


    public Page<Iresource_resource> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_resource resource_resource){
        Iresource_resource clientModel = resource_resourceFeignClient.getDraft(resource_resource.getId(),(resource_resourceImpl)resource_resource) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_resource.getClass(), false);
        copier.copy(clientModel, resource_resource, null);
    }



}


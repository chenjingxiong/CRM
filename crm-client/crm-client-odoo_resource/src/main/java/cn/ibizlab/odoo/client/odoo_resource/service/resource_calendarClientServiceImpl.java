package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_calendarClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendarImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_calendarFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_calendar] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_calendarClientServiceImpl implements Iresource_calendarClientService {

    resource_calendarFeignClient resource_calendarFeignClient;

    @Autowired
    public resource_calendarClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendarFeignClient = nameBuilder.target(resource_calendarFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_calendarFeignClient = nameBuilder.target(resource_calendarFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_calendar createModel() {
		return new resource_calendarImpl();
	}


    public void update(Iresource_calendar resource_calendar){
        Iresource_calendar clientModel = resource_calendarFeignClient.update(resource_calendar.getId(),(resource_calendarImpl)resource_calendar) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar.getClass(), false);
        copier.copy(clientModel, resource_calendar, null);
    }


    public void removeBatch(List<Iresource_calendar> resource_calendars){
        if(resource_calendars!=null){
            List<resource_calendarImpl> list = new ArrayList<resource_calendarImpl>();
            for(Iresource_calendar iresource_calendar :resource_calendars){
                list.add((resource_calendarImpl)iresource_calendar) ;
            }
            resource_calendarFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iresource_calendar resource_calendar){
        resource_calendarFeignClient.remove(resource_calendar.getId()) ;
    }


    public Page<Iresource_calendar> fetchDefault(SearchContext context){
        Page<resource_calendarImpl> page = this.resource_calendarFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iresource_calendar resource_calendar){
        Iresource_calendar clientModel = resource_calendarFeignClient.get(resource_calendar.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar.getClass(), false);
        copier.copy(clientModel, resource_calendar, null);
    }


    public void createBatch(List<Iresource_calendar> resource_calendars){
        if(resource_calendars!=null){
            List<resource_calendarImpl> list = new ArrayList<resource_calendarImpl>();
            for(Iresource_calendar iresource_calendar :resource_calendars){
                list.add((resource_calendarImpl)iresource_calendar) ;
            }
            resource_calendarFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iresource_calendar> resource_calendars){
        if(resource_calendars!=null){
            List<resource_calendarImpl> list = new ArrayList<resource_calendarImpl>();
            for(Iresource_calendar iresource_calendar :resource_calendars){
                list.add((resource_calendarImpl)iresource_calendar) ;
            }
            resource_calendarFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iresource_calendar resource_calendar){
        Iresource_calendar clientModel = resource_calendarFeignClient.create((resource_calendarImpl)resource_calendar) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar.getClass(), false);
        copier.copy(clientModel, resource_calendar, null);
    }


    public Page<Iresource_calendar> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_calendar resource_calendar){
        Iresource_calendar clientModel = resource_calendarFeignClient.getDraft(resource_calendar.getId(),(resource_calendarImpl)resource_calendar) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_calendar.getClass(), false);
        copier.copy(clientModel, resource_calendar, null);
    }



}


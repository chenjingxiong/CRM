package cn.ibizlab.odoo.client.odoo_note.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Inote_note;
import cn.ibizlab.odoo.client.odoo_note.model.note_noteImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[note_note] 服务对象接口
 */
public interface note_noteFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_notes/updatebatch")
    public note_noteImpl updateBatch(@RequestBody List<note_noteImpl> note_notes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_notes/{id}")
    public note_noteImpl update(@PathVariable("id") Integer id,@RequestBody note_noteImpl note_note);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_notes/fetchdefault")
    public Page<note_noteImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_notes/{id}")
    public note_noteImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_notes/createbatch")
    public note_noteImpl createBatch(@RequestBody List<note_noteImpl> note_notes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_notes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_notes/removebatch")
    public note_noteImpl removeBatch(@RequestBody List<note_noteImpl> note_notes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_notes")
    public note_noteImpl create(@RequestBody note_noteImpl note_note);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_notes/select")
    public Page<note_noteImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_notes/{id}/getdraft")
    public note_noteImpl getDraft(@PathVariable("id") Integer id,@RequestBody note_noteImpl note_note);



}

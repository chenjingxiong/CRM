package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_team;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_teamImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_team] 服务对象接口
 */
public interface crm_teamFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_teams/fetchdefault")
    public Page<crm_teamImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams")
    public crm_teamImpl create(@RequestBody crm_teamImpl crm_team);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_teams/updatebatch")
    public crm_teamImpl updateBatch(@RequestBody List<crm_teamImpl> crm_teams);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_teams/removebatch")
    public crm_teamImpl removeBatch(@RequestBody List<crm_teamImpl> crm_teams);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams/createbatch")
    public crm_teamImpl createBatch(@RequestBody List<crm_teamImpl> crm_teams);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_teams/{id}")
    public crm_teamImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_teams/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_teams/{id}")
    public crm_teamImpl update(@PathVariable("id") Integer id,@RequestBody crm_teamImpl crm_team);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_teams/select")
    public Page<crm_teamImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams/{id}/checkkey")
    public crm_teamImpl checkKey(@PathVariable("id") Integer id,@RequestBody crm_teamImpl crm_team);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_teams/{id}/getdraft")
    public crm_teamImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_teamImpl crm_team);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams/{id}/save")
    public crm_teamImpl save(@PathVariable("id") Integer id,@RequestBody crm_teamImpl crm_team);



}

package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_lost;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_lostImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lead_lost] 服务对象接口
 */
public interface crm_lead_lostFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead_losts/removebatch")
    public crm_lead_lostImpl removeBatch(@RequestBody List<crm_lead_lostImpl> crm_lead_losts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead_losts/updatebatch")
    public crm_lead_lostImpl updateBatch(@RequestBody List<crm_lead_lostImpl> crm_lead_losts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_losts/{id}")
    public crm_lead_lostImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_losts/fetchdefault")
    public Page<crm_lead_lostImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead_losts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead_losts/{id}")
    public crm_lead_lostImpl update(@PathVariable("id") Integer id,@RequestBody crm_lead_lostImpl crm_lead_lost);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead_losts/createbatch")
    public crm_lead_lostImpl createBatch(@RequestBody List<crm_lead_lostImpl> crm_lead_losts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead_losts")
    public crm_lead_lostImpl create(@RequestBody crm_lead_lostImpl crm_lead_lost);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_losts/select")
    public Page<crm_lead_lostImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_losts/{id}/getdraft")
    public crm_lead_lostImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_lead_lostImpl crm_lead_lost);



}

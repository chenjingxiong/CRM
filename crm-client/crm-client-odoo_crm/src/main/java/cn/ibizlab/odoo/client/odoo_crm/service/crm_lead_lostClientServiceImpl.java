package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_lost;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_lead_lostClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_lostImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_lead_lostFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_lead_lost] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_lead_lostClientServiceImpl implements Icrm_lead_lostClientService {

    crm_lead_lostFeignClient crm_lead_lostFeignClient;

    @Autowired
    public crm_lead_lostClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead_lostFeignClient = nameBuilder.target(crm_lead_lostFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead_lostFeignClient = nameBuilder.target(crm_lead_lostFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_lead_lost createModel() {
		return new crm_lead_lostImpl();
	}


    public void removeBatch(List<Icrm_lead_lost> crm_lead_losts){
        if(crm_lead_losts!=null){
            List<crm_lead_lostImpl> list = new ArrayList<crm_lead_lostImpl>();
            for(Icrm_lead_lost icrm_lead_lost :crm_lead_losts){
                list.add((crm_lead_lostImpl)icrm_lead_lost) ;
            }
            crm_lead_lostFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Icrm_lead_lost> crm_lead_losts){
        if(crm_lead_losts!=null){
            List<crm_lead_lostImpl> list = new ArrayList<crm_lead_lostImpl>();
            for(Icrm_lead_lost icrm_lead_lost :crm_lead_losts){
                list.add((crm_lead_lostImpl)icrm_lead_lost) ;
            }
            crm_lead_lostFeignClient.updateBatch(list) ;
        }
    }


    public void get(Icrm_lead_lost crm_lead_lost){
        Icrm_lead_lost clientModel = crm_lead_lostFeignClient.get(crm_lead_lost.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_lost.getClass(), false);
        copier.copy(clientModel, crm_lead_lost, null);
    }


    public Page<Icrm_lead_lost> fetchDefault(SearchContext context){
        Page<crm_lead_lostImpl> page = this.crm_lead_lostFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Icrm_lead_lost crm_lead_lost){
        crm_lead_lostFeignClient.remove(crm_lead_lost.getId()) ;
    }


    public void update(Icrm_lead_lost crm_lead_lost){
        Icrm_lead_lost clientModel = crm_lead_lostFeignClient.update(crm_lead_lost.getId(),(crm_lead_lostImpl)crm_lead_lost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_lost.getClass(), false);
        copier.copy(clientModel, crm_lead_lost, null);
    }


    public void createBatch(List<Icrm_lead_lost> crm_lead_losts){
        if(crm_lead_losts!=null){
            List<crm_lead_lostImpl> list = new ArrayList<crm_lead_lostImpl>();
            for(Icrm_lead_lost icrm_lead_lost :crm_lead_losts){
                list.add((crm_lead_lostImpl)icrm_lead_lost) ;
            }
            crm_lead_lostFeignClient.createBatch(list) ;
        }
    }


    public void create(Icrm_lead_lost crm_lead_lost){
        Icrm_lead_lost clientModel = crm_lead_lostFeignClient.create((crm_lead_lostImpl)crm_lead_lost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_lost.getClass(), false);
        copier.copy(clientModel, crm_lead_lost, null);
    }


    public Page<Icrm_lead_lost> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_lead_lost crm_lead_lost){
        Icrm_lead_lost clientModel = crm_lead_lostFeignClient.getDraft(crm_lead_lost.getId(),(crm_lead_lostImpl)crm_lead_lost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_lost.getClass(), false);
        copier.copy(clientModel, crm_lead_lost, null);
    }



}


package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_team;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_teamClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_teamImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_teamFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_team] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_teamClientServiceImpl implements Icrm_teamClientService {

    crm_teamFeignClient crm_teamFeignClient;

    @Autowired
    public crm_teamClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_teamFeignClient = nameBuilder.target(crm_teamFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_teamFeignClient = nameBuilder.target(crm_teamFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_team createModel() {
		return new crm_teamImpl();
	}


    public Page<Icrm_team> fetchDefault(SearchContext context){
        Page<crm_teamImpl> page = this.crm_teamFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.create((crm_teamImpl)crm_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }


    public void updateBatch(List<Icrm_team> crm_teams){
        if(crm_teams!=null){
            List<crm_teamImpl> list = new ArrayList<crm_teamImpl>();
            for(Icrm_team icrm_team :crm_teams){
                list.add((crm_teamImpl)icrm_team) ;
            }
            crm_teamFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Icrm_team> crm_teams){
        if(crm_teams!=null){
            List<crm_teamImpl> list = new ArrayList<crm_teamImpl>();
            for(Icrm_team icrm_team :crm_teams){
                list.add((crm_teamImpl)icrm_team) ;
            }
            crm_teamFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Icrm_team> crm_teams){
        if(crm_teams!=null){
            List<crm_teamImpl> list = new ArrayList<crm_teamImpl>();
            for(Icrm_team icrm_team :crm_teams){
                list.add((crm_teamImpl)icrm_team) ;
            }
            crm_teamFeignClient.createBatch(list) ;
        }
    }


    public void get(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.get(crm_team.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }


    public void remove(Icrm_team crm_team){
        crm_teamFeignClient.remove(crm_team.getId()) ;
    }


    public void update(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.update(crm_team.getId(),(crm_teamImpl)crm_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }


    public Page<Icrm_team> select(SearchContext context){
        return null ;
    }


    public void checkKey(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.checkKey(crm_team.getId(),(crm_teamImpl)crm_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }


    public void getDraft(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.getDraft(crm_team.getId(),(crm_teamImpl)crm_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }


    public void save(Icrm_team crm_team){
        Icrm_team clientModel = crm_teamFeignClient.save(crm_team.getId(),(crm_teamImpl)crm_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_team.getClass(), false);
        copier.copy(clientModel, crm_team, null);
    }



}


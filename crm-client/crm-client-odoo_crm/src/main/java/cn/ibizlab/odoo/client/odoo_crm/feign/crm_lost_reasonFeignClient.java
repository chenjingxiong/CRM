package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lost_reason;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lost_reasonImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lost_reason] 服务对象接口
 */
public interface crm_lost_reasonFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lost_reasons/{id}")
    public crm_lost_reasonImpl update(@PathVariable("id") Integer id,@RequestBody crm_lost_reasonImpl crm_lost_reason);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lost_reasons/removebatch")
    public crm_lost_reasonImpl removeBatch(@RequestBody List<crm_lost_reasonImpl> crm_lost_reasons);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lost_reasons/updatebatch")
    public crm_lost_reasonImpl updateBatch(@RequestBody List<crm_lost_reasonImpl> crm_lost_reasons);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lost_reasons/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lost_reasons")
    public crm_lost_reasonImpl create(@RequestBody crm_lost_reasonImpl crm_lost_reason);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lost_reasons/fetchdefault")
    public Page<crm_lost_reasonImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lost_reasons/{id}")
    public crm_lost_reasonImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lost_reasons/createbatch")
    public crm_lost_reasonImpl createBatch(@RequestBody List<crm_lost_reasonImpl> crm_lost_reasons);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lost_reasons/select")
    public Page<crm_lost_reasonImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lost_reasons/{id}/getdraft")
    public crm_lost_reasonImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_lost_reasonImpl crm_lost_reason);



}

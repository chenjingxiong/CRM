package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_stage;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_stage] 服务对象接口
 */
public interface crm_stageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_stages/{id}")
    public crm_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_stages/createbatch")
    public crm_stageImpl createBatch(@RequestBody List<crm_stageImpl> crm_stages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_stages/{id}")
    public crm_stageImpl update(@PathVariable("id") Integer id,@RequestBody crm_stageImpl crm_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_stages/updatebatch")
    public crm_stageImpl updateBatch(@RequestBody List<crm_stageImpl> crm_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_stages")
    public crm_stageImpl create(@RequestBody crm_stageImpl crm_stage);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_stages/fetchdefault")
    public Page<crm_stageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_stages/removebatch")
    public crm_stageImpl removeBatch(@RequestBody List<crm_stageImpl> crm_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_stages/select")
    public Page<crm_stageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_stages/{id}/getdraft")
    public crm_stageImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_stageImpl crm_stage);



}

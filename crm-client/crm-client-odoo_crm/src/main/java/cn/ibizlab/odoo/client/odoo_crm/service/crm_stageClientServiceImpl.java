package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_stage;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_stageClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_stageImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_stageClientServiceImpl implements Icrm_stageClientService {

    crm_stageFeignClient crm_stageFeignClient;

    @Autowired
    public crm_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_stageFeignClient = nameBuilder.target(crm_stageFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_stageFeignClient = nameBuilder.target(crm_stageFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_stage createModel() {
		return new crm_stageImpl();
	}


    public void get(Icrm_stage crm_stage){
        Icrm_stage clientModel = crm_stageFeignClient.get(crm_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_stage.getClass(), false);
        copier.copy(clientModel, crm_stage, null);
    }


    public void createBatch(List<Icrm_stage> crm_stages){
        if(crm_stages!=null){
            List<crm_stageImpl> list = new ArrayList<crm_stageImpl>();
            for(Icrm_stage icrm_stage :crm_stages){
                list.add((crm_stageImpl)icrm_stage) ;
            }
            crm_stageFeignClient.createBatch(list) ;
        }
    }


    public void update(Icrm_stage crm_stage){
        Icrm_stage clientModel = crm_stageFeignClient.update(crm_stage.getId(),(crm_stageImpl)crm_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_stage.getClass(), false);
        copier.copy(clientModel, crm_stage, null);
    }


    public void remove(Icrm_stage crm_stage){
        crm_stageFeignClient.remove(crm_stage.getId()) ;
    }


    public void updateBatch(List<Icrm_stage> crm_stages){
        if(crm_stages!=null){
            List<crm_stageImpl> list = new ArrayList<crm_stageImpl>();
            for(Icrm_stage icrm_stage :crm_stages){
                list.add((crm_stageImpl)icrm_stage) ;
            }
            crm_stageFeignClient.updateBatch(list) ;
        }
    }


    public void create(Icrm_stage crm_stage){
        Icrm_stage clientModel = crm_stageFeignClient.create((crm_stageImpl)crm_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_stage.getClass(), false);
        copier.copy(clientModel, crm_stage, null);
    }


    public Page<Icrm_stage> fetchDefault(SearchContext context){
        Page<crm_stageImpl> page = this.crm_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Icrm_stage> crm_stages){
        if(crm_stages!=null){
            List<crm_stageImpl> list = new ArrayList<crm_stageImpl>();
            for(Icrm_stage icrm_stage :crm_stages){
                list.add((crm_stageImpl)icrm_stage) ;
            }
            crm_stageFeignClient.removeBatch(list) ;
        }
    }


    public Page<Icrm_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_stage crm_stage){
        Icrm_stage clientModel = crm_stageFeignClient.getDraft(crm_stage.getId(),(crm_stageImpl)crm_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_stage.getClass(), false);
        copier.copy(clientModel, crm_stage, null);
    }



}


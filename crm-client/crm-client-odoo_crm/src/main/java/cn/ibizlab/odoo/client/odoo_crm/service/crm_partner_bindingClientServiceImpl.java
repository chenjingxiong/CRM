package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_partner_binding;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_partner_bindingClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_partner_bindingImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_partner_bindingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_partner_binding] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_partner_bindingClientServiceImpl implements Icrm_partner_bindingClientService {

    crm_partner_bindingFeignClient crm_partner_bindingFeignClient;

    @Autowired
    public crm_partner_bindingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_partner_bindingFeignClient = nameBuilder.target(crm_partner_bindingFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_partner_bindingFeignClient = nameBuilder.target(crm_partner_bindingFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_partner_binding createModel() {
		return new crm_partner_bindingImpl();
	}


    public void update(Icrm_partner_binding crm_partner_binding){
        Icrm_partner_binding clientModel = crm_partner_bindingFeignClient.update(crm_partner_binding.getId(),(crm_partner_bindingImpl)crm_partner_binding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_partner_binding.getClass(), false);
        copier.copy(clientModel, crm_partner_binding, null);
    }


    public void updateBatch(List<Icrm_partner_binding> crm_partner_bindings){
        if(crm_partner_bindings!=null){
            List<crm_partner_bindingImpl> list = new ArrayList<crm_partner_bindingImpl>();
            for(Icrm_partner_binding icrm_partner_binding :crm_partner_bindings){
                list.add((crm_partner_bindingImpl)icrm_partner_binding) ;
            }
            crm_partner_bindingFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Icrm_partner_binding> crm_partner_bindings){
        if(crm_partner_bindings!=null){
            List<crm_partner_bindingImpl> list = new ArrayList<crm_partner_bindingImpl>();
            for(Icrm_partner_binding icrm_partner_binding :crm_partner_bindings){
                list.add((crm_partner_bindingImpl)icrm_partner_binding) ;
            }
            crm_partner_bindingFeignClient.removeBatch(list) ;
        }
    }


    public void get(Icrm_partner_binding crm_partner_binding){
        Icrm_partner_binding clientModel = crm_partner_bindingFeignClient.get(crm_partner_binding.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_partner_binding.getClass(), false);
        copier.copy(clientModel, crm_partner_binding, null);
    }


    public void remove(Icrm_partner_binding crm_partner_binding){
        crm_partner_bindingFeignClient.remove(crm_partner_binding.getId()) ;
    }


    public Page<Icrm_partner_binding> fetchDefault(SearchContext context){
        Page<crm_partner_bindingImpl> page = this.crm_partner_bindingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Icrm_partner_binding> crm_partner_bindings){
        if(crm_partner_bindings!=null){
            List<crm_partner_bindingImpl> list = new ArrayList<crm_partner_bindingImpl>();
            for(Icrm_partner_binding icrm_partner_binding :crm_partner_bindings){
                list.add((crm_partner_bindingImpl)icrm_partner_binding) ;
            }
            crm_partner_bindingFeignClient.createBatch(list) ;
        }
    }


    public void create(Icrm_partner_binding crm_partner_binding){
        Icrm_partner_binding clientModel = crm_partner_bindingFeignClient.create((crm_partner_bindingImpl)crm_partner_binding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_partner_binding.getClass(), false);
        copier.copy(clientModel, crm_partner_binding, null);
    }


    public Page<Icrm_partner_binding> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_partner_binding crm_partner_binding){
        Icrm_partner_binding clientModel = crm_partner_bindingFeignClient.getDraft(crm_partner_binding.getId(),(crm_partner_bindingImpl)crm_partner_binding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_partner_binding.getClass(), false);
        copier.copy(clientModel, crm_partner_binding, null);
    }



}


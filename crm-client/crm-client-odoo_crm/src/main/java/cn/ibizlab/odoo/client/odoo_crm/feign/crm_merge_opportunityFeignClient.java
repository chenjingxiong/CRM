package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_merge_opportunity;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_merge_opportunityImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
public interface crm_merge_opportunityFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_merge_opportunities/createbatch")
    public crm_merge_opportunityImpl createBatch(@RequestBody List<crm_merge_opportunityImpl> crm_merge_opportunities);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_merge_opportunities/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_merge_opportunities/updatebatch")
    public crm_merge_opportunityImpl updateBatch(@RequestBody List<crm_merge_opportunityImpl> crm_merge_opportunities);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_merge_opportunities/fetchdefault")
    public Page<crm_merge_opportunityImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_merge_opportunities/{id}")
    public crm_merge_opportunityImpl update(@PathVariable("id") Integer id,@RequestBody crm_merge_opportunityImpl crm_merge_opportunity);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_merge_opportunities/removebatch")
    public crm_merge_opportunityImpl removeBatch(@RequestBody List<crm_merge_opportunityImpl> crm_merge_opportunities);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_merge_opportunities")
    public crm_merge_opportunityImpl create(@RequestBody crm_merge_opportunityImpl crm_merge_opportunity);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_merge_opportunities/{id}")
    public crm_merge_opportunityImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_merge_opportunities/select")
    public Page<crm_merge_opportunityImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_merge_opportunities/{id}/getdraft")
    public crm_merge_opportunityImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_merge_opportunityImpl crm_merge_opportunity);



}

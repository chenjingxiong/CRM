package cn.ibizlab.odoo.client.odoo_digest.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Idigest_digest;
import cn.ibizlab.odoo.client.odoo_digest.config.odoo_digestClientProperties;
import cn.ibizlab.odoo.core.client.service.Idigest_digestClientService;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_digestImpl;
import cn.ibizlab.odoo.client.odoo_digest.feign.digest_digestFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[digest_digest] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class digest_digestClientServiceImpl implements Idigest_digestClientService {

    digest_digestFeignClient digest_digestFeignClient;

    @Autowired
    public digest_digestClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_digestClientProperties odoo_digestClientProperties) {
        if (odoo_digestClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.digest_digestFeignClient = nameBuilder.target(digest_digestFeignClient.class,"http://"+odoo_digestClientProperties.getServiceId()+"/") ;
		}else if (odoo_digestClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.digest_digestFeignClient = nameBuilder.target(digest_digestFeignClient.class,odoo_digestClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Idigest_digest createModel() {
		return new digest_digestImpl();
	}


    public void updateBatch(List<Idigest_digest> digest_digests){
        if(digest_digests!=null){
            List<digest_digestImpl> list = new ArrayList<digest_digestImpl>();
            for(Idigest_digest idigest_digest :digest_digests){
                list.add((digest_digestImpl)idigest_digest) ;
            }
            digest_digestFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Idigest_digest> digest_digests){
        if(digest_digests!=null){
            List<digest_digestImpl> list = new ArrayList<digest_digestImpl>();
            for(Idigest_digest idigest_digest :digest_digests){
                list.add((digest_digestImpl)idigest_digest) ;
            }
            digest_digestFeignClient.createBatch(list) ;
        }
    }


    public void create(Idigest_digest digest_digest){
        Idigest_digest clientModel = digest_digestFeignClient.create((digest_digestImpl)digest_digest) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_digest.getClass(), false);
        copier.copy(clientModel, digest_digest, null);
    }


    public void get(Idigest_digest digest_digest){
        Idigest_digest clientModel = digest_digestFeignClient.get(digest_digest.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_digest.getClass(), false);
        copier.copy(clientModel, digest_digest, null);
    }


    public void removeBatch(List<Idigest_digest> digest_digests){
        if(digest_digests!=null){
            List<digest_digestImpl> list = new ArrayList<digest_digestImpl>();
            for(Idigest_digest idigest_digest :digest_digests){
                list.add((digest_digestImpl)idigest_digest) ;
            }
            digest_digestFeignClient.removeBatch(list) ;
        }
    }


    public void update(Idigest_digest digest_digest){
        Idigest_digest clientModel = digest_digestFeignClient.update(digest_digest.getId(),(digest_digestImpl)digest_digest) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_digest.getClass(), false);
        copier.copy(clientModel, digest_digest, null);
    }


    public Page<Idigest_digest> fetchDefault(SearchContext context){
        Page<digest_digestImpl> page = this.digest_digestFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Idigest_digest digest_digest){
        digest_digestFeignClient.remove(digest_digest.getId()) ;
    }


    public Page<Idigest_digest> select(SearchContext context){
        return null ;
    }


    public void getDraft(Idigest_digest digest_digest){
        Idigest_digest clientModel = digest_digestFeignClient.getDraft(digest_digest.getId(),(digest_digestImpl)digest_digest) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_digest.getClass(), false);
        copier.copy(clientModel, digest_digest, null);
    }



}


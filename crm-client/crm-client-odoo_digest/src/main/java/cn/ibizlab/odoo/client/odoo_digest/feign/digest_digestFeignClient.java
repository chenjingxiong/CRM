package cn.ibizlab.odoo.client.odoo_digest.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Idigest_digest;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_digestImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[digest_digest] 服务对象接口
 */
public interface digest_digestFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_digests/updatebatch")
    public digest_digestImpl updateBatch(@RequestBody List<digest_digestImpl> digest_digests);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_digests/createbatch")
    public digest_digestImpl createBatch(@RequestBody List<digest_digestImpl> digest_digests);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_digests")
    public digest_digestImpl create(@RequestBody digest_digestImpl digest_digest);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_digests/{id}")
    public digest_digestImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_digests/removebatch")
    public digest_digestImpl removeBatch(@RequestBody List<digest_digestImpl> digest_digests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_digests/{id}")
    public digest_digestImpl update(@PathVariable("id") Integer id,@RequestBody digest_digestImpl digest_digest);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_digests/fetchdefault")
    public Page<digest_digestImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_digests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_digests/select")
    public Page<digest_digestImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_digests/{id}/getdraft")
    public digest_digestImpl getDraft(@PathVariable("id") Integer id,@RequestBody digest_digestImpl digest_digest);



}

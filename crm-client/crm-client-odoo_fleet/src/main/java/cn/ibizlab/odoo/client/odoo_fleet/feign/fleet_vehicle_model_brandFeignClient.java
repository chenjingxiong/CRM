package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model_brand;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_model_brandImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
public interface fleet_vehicle_model_brandFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_model_brands/updatebatch")
    public fleet_vehicle_model_brandImpl updateBatch(@RequestBody List<fleet_vehicle_model_brandImpl> fleet_vehicle_model_brands);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_model_brands/createbatch")
    public fleet_vehicle_model_brandImpl createBatch(@RequestBody List<fleet_vehicle_model_brandImpl> fleet_vehicle_model_brands);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_model_brands/removebatch")
    public fleet_vehicle_model_brandImpl removeBatch(@RequestBody List<fleet_vehicle_model_brandImpl> fleet_vehicle_model_brands);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_model_brands/fetchdefault")
    public Page<fleet_vehicle_model_brandImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_model_brands/{id}")
    public fleet_vehicle_model_brandImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_model_brands/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_model_brands/{id}")
    public fleet_vehicle_model_brandImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_model_brandImpl fleet_vehicle_model_brand);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_model_brands")
    public fleet_vehicle_model_brandImpl create(@RequestBody fleet_vehicle_model_brandImpl fleet_vehicle_model_brand);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_model_brands/select")
    public Page<fleet_vehicle_model_brandImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_model_brands/{id}/getdraft")
    public fleet_vehicle_model_brandImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_model_brandImpl fleet_vehicle_model_brand);



}

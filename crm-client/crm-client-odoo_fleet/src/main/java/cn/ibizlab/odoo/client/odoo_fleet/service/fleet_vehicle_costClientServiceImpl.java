package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_cost;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_costClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_costImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_costFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_costClientServiceImpl implements Ifleet_vehicle_costClientService {

    fleet_vehicle_costFeignClient fleet_vehicle_costFeignClient;

    @Autowired
    public fleet_vehicle_costClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_costFeignClient = nameBuilder.target(fleet_vehicle_costFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_costFeignClient = nameBuilder.target(fleet_vehicle_costFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_cost createModel() {
		return new fleet_vehicle_costImpl();
	}


    public Page<Ifleet_vehicle_cost> fetchDefault(SearchContext context){
        Page<fleet_vehicle_costImpl> page = this.fleet_vehicle_costFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
        if(fleet_vehicle_costs!=null){
            List<fleet_vehicle_costImpl> list = new ArrayList<fleet_vehicle_costImpl>();
            for(Ifleet_vehicle_cost ifleet_vehicle_cost :fleet_vehicle_costs){
                list.add((fleet_vehicle_costImpl)ifleet_vehicle_cost) ;
            }
            fleet_vehicle_costFeignClient.createBatch(list) ;
        }
    }


    public void create(Ifleet_vehicle_cost fleet_vehicle_cost){
        Ifleet_vehicle_cost clientModel = fleet_vehicle_costFeignClient.create((fleet_vehicle_costImpl)fleet_vehicle_cost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_cost.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_cost, null);
    }


    public void remove(Ifleet_vehicle_cost fleet_vehicle_cost){
        fleet_vehicle_costFeignClient.remove(fleet_vehicle_cost.getId()) ;
    }


    public void update(Ifleet_vehicle_cost fleet_vehicle_cost){
        Ifleet_vehicle_cost clientModel = fleet_vehicle_costFeignClient.update(fleet_vehicle_cost.getId(),(fleet_vehicle_costImpl)fleet_vehicle_cost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_cost.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_cost, null);
    }


    public void get(Ifleet_vehicle_cost fleet_vehicle_cost){
        Ifleet_vehicle_cost clientModel = fleet_vehicle_costFeignClient.get(fleet_vehicle_cost.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_cost.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_cost, null);
    }


    public void removeBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
        if(fleet_vehicle_costs!=null){
            List<fleet_vehicle_costImpl> list = new ArrayList<fleet_vehicle_costImpl>();
            for(Ifleet_vehicle_cost ifleet_vehicle_cost :fleet_vehicle_costs){
                list.add((fleet_vehicle_costImpl)ifleet_vehicle_cost) ;
            }
            fleet_vehicle_costFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs){
        if(fleet_vehicle_costs!=null){
            List<fleet_vehicle_costImpl> list = new ArrayList<fleet_vehicle_costImpl>();
            for(Ifleet_vehicle_cost ifleet_vehicle_cost :fleet_vehicle_costs){
                list.add((fleet_vehicle_costImpl)ifleet_vehicle_cost) ;
            }
            fleet_vehicle_costFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_cost> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_cost fleet_vehicle_cost){
        Ifleet_vehicle_cost clientModel = fleet_vehicle_costFeignClient.getDraft(fleet_vehicle_cost.getId(),(fleet_vehicle_costImpl)fleet_vehicle_cost) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_cost.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_cost, null);
    }



}


package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_services;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_servicesClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_servicesImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_log_servicesFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_log_servicesClientServiceImpl implements Ifleet_vehicle_log_servicesClientService {

    fleet_vehicle_log_servicesFeignClient fleet_vehicle_log_servicesFeignClient;

    @Autowired
    public fleet_vehicle_log_servicesClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_servicesFeignClient = nameBuilder.target(fleet_vehicle_log_servicesFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_servicesFeignClient = nameBuilder.target(fleet_vehicle_log_servicesFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_log_services createModel() {
		return new fleet_vehicle_log_servicesImpl();
	}


    public void remove(Ifleet_vehicle_log_services fleet_vehicle_log_services){
        fleet_vehicle_log_servicesFeignClient.remove(fleet_vehicle_log_services.getId()) ;
    }


    public void create(Ifleet_vehicle_log_services fleet_vehicle_log_services){
        Ifleet_vehicle_log_services clientModel = fleet_vehicle_log_servicesFeignClient.create((fleet_vehicle_log_servicesImpl)fleet_vehicle_log_services) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_services.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_services, null);
    }


    public void get(Ifleet_vehicle_log_services fleet_vehicle_log_services){
        Ifleet_vehicle_log_services clientModel = fleet_vehicle_log_servicesFeignClient.get(fleet_vehicle_log_services.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_services.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_services, null);
    }


    public void updateBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
        if(fleet_vehicle_log_services!=null){
            List<fleet_vehicle_log_servicesImpl> list = new ArrayList<fleet_vehicle_log_servicesImpl>();
            for(Ifleet_vehicle_log_services ifleet_vehicle_log_services :fleet_vehicle_log_services){
                list.add((fleet_vehicle_log_servicesImpl)ifleet_vehicle_log_services) ;
            }
            fleet_vehicle_log_servicesFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_log_services> fetchDefault(SearchContext context){
        Page<fleet_vehicle_log_servicesImpl> page = this.fleet_vehicle_log_servicesFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
        if(fleet_vehicle_log_services!=null){
            List<fleet_vehicle_log_servicesImpl> list = new ArrayList<fleet_vehicle_log_servicesImpl>();
            for(Ifleet_vehicle_log_services ifleet_vehicle_log_services :fleet_vehicle_log_services){
                list.add((fleet_vehicle_log_servicesImpl)ifleet_vehicle_log_services) ;
            }
            fleet_vehicle_log_servicesFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ifleet_vehicle_log_services fleet_vehicle_log_services){
        Ifleet_vehicle_log_services clientModel = fleet_vehicle_log_servicesFeignClient.update(fleet_vehicle_log_services.getId(),(fleet_vehicle_log_servicesImpl)fleet_vehicle_log_services) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_services.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_services, null);
    }


    public void createBatch(List<Ifleet_vehicle_log_services> fleet_vehicle_log_services){
        if(fleet_vehicle_log_services!=null){
            List<fleet_vehicle_log_servicesImpl> list = new ArrayList<fleet_vehicle_log_servicesImpl>();
            for(Ifleet_vehicle_log_services ifleet_vehicle_log_services :fleet_vehicle_log_services){
                list.add((fleet_vehicle_log_servicesImpl)ifleet_vehicle_log_services) ;
            }
            fleet_vehicle_log_servicesFeignClient.createBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_log_services> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_log_services fleet_vehicle_log_services){
        Ifleet_vehicle_log_services clientModel = fleet_vehicle_log_servicesFeignClient.getDraft(fleet_vehicle_log_services.getId(),(fleet_vehicle_log_servicesImpl)fleet_vehicle_log_services) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_services.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_services, null);
    }



}


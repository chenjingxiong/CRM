package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_odometer;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_odometerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
public interface fleet_vehicle_odometerFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_odometers/removebatch")
    public fleet_vehicle_odometerImpl removeBatch(@RequestBody List<fleet_vehicle_odometerImpl> fleet_vehicle_odometers);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_odometers/createbatch")
    public fleet_vehicle_odometerImpl createBatch(@RequestBody List<fleet_vehicle_odometerImpl> fleet_vehicle_odometers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_odometers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_odometers/updatebatch")
    public fleet_vehicle_odometerImpl updateBatch(@RequestBody List<fleet_vehicle_odometerImpl> fleet_vehicle_odometers);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_odometers")
    public fleet_vehicle_odometerImpl create(@RequestBody fleet_vehicle_odometerImpl fleet_vehicle_odometer);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_odometers/{id}")
    public fleet_vehicle_odometerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_odometers/{id}")
    public fleet_vehicle_odometerImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_odometerImpl fleet_vehicle_odometer);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_odometers/fetchdefault")
    public Page<fleet_vehicle_odometerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_odometers/select")
    public Page<fleet_vehicle_odometerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_odometers/{id}/getdraft")
    public fleet_vehicle_odometerImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_odometerImpl fleet_vehicle_odometer);



}

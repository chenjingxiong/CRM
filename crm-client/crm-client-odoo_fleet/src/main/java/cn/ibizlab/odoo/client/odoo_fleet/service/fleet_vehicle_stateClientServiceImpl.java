package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_state;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_stateClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_stateImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_stateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_stateClientServiceImpl implements Ifleet_vehicle_stateClientService {

    fleet_vehicle_stateFeignClient fleet_vehicle_stateFeignClient;

    @Autowired
    public fleet_vehicle_stateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_stateFeignClient = nameBuilder.target(fleet_vehicle_stateFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_stateFeignClient = nameBuilder.target(fleet_vehicle_stateFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_state createModel() {
		return new fleet_vehicle_stateImpl();
	}


    public void createBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
        if(fleet_vehicle_states!=null){
            List<fleet_vehicle_stateImpl> list = new ArrayList<fleet_vehicle_stateImpl>();
            for(Ifleet_vehicle_state ifleet_vehicle_state :fleet_vehicle_states){
                list.add((fleet_vehicle_stateImpl)ifleet_vehicle_state) ;
            }
            fleet_vehicle_stateFeignClient.createBatch(list) ;
        }
    }


    public void create(Ifleet_vehicle_state fleet_vehicle_state){
        Ifleet_vehicle_state clientModel = fleet_vehicle_stateFeignClient.create((fleet_vehicle_stateImpl)fleet_vehicle_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_state.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_state, null);
    }


    public void updateBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
        if(fleet_vehicle_states!=null){
            List<fleet_vehicle_stateImpl> list = new ArrayList<fleet_vehicle_stateImpl>();
            for(Ifleet_vehicle_state ifleet_vehicle_state :fleet_vehicle_states){
                list.add((fleet_vehicle_stateImpl)ifleet_vehicle_state) ;
            }
            fleet_vehicle_stateFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ifleet_vehicle_state> fleet_vehicle_states){
        if(fleet_vehicle_states!=null){
            List<fleet_vehicle_stateImpl> list = new ArrayList<fleet_vehicle_stateImpl>();
            for(Ifleet_vehicle_state ifleet_vehicle_state :fleet_vehicle_states){
                list.add((fleet_vehicle_stateImpl)ifleet_vehicle_state) ;
            }
            fleet_vehicle_stateFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ifleet_vehicle_state fleet_vehicle_state){
        fleet_vehicle_stateFeignClient.remove(fleet_vehicle_state.getId()) ;
    }


    public Page<Ifleet_vehicle_state> fetchDefault(SearchContext context){
        Page<fleet_vehicle_stateImpl> page = this.fleet_vehicle_stateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ifleet_vehicle_state fleet_vehicle_state){
        Ifleet_vehicle_state clientModel = fleet_vehicle_stateFeignClient.update(fleet_vehicle_state.getId(),(fleet_vehicle_stateImpl)fleet_vehicle_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_state.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_state, null);
    }


    public void get(Ifleet_vehicle_state fleet_vehicle_state){
        Ifleet_vehicle_state clientModel = fleet_vehicle_stateFeignClient.get(fleet_vehicle_state.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_state.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_state, null);
    }


    public Page<Ifleet_vehicle_state> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_state fleet_vehicle_state){
        Ifleet_vehicle_state clientModel = fleet_vehicle_stateFeignClient.getDraft(fleet_vehicle_state.getId(),(fleet_vehicle_stateImpl)fleet_vehicle_state) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_state.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_state, null);
    }



}


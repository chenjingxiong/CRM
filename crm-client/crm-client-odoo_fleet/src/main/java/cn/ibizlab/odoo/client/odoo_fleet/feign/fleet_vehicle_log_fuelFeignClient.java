package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_fuel;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_fuelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
public interface fleet_vehicle_log_fuelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_fuels/removebatch")
    public fleet_vehicle_log_fuelImpl removeBatch(@RequestBody List<fleet_vehicle_log_fuelImpl> fleet_vehicle_log_fuels);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_fuels/{id}")
    public fleet_vehicle_log_fuelImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_log_fuelImpl fleet_vehicle_log_fuel);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_fuels")
    public fleet_vehicle_log_fuelImpl create(@RequestBody fleet_vehicle_log_fuelImpl fleet_vehicle_log_fuel);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_fuels/updatebatch")
    public fleet_vehicle_log_fuelImpl updateBatch(@RequestBody List<fleet_vehicle_log_fuelImpl> fleet_vehicle_log_fuels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_fuels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_fuels/{id}")
    public fleet_vehicle_log_fuelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_fuels/createbatch")
    public fleet_vehicle_log_fuelImpl createBatch(@RequestBody List<fleet_vehicle_log_fuelImpl> fleet_vehicle_log_fuels);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_fuels/fetchdefault")
    public Page<fleet_vehicle_log_fuelImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_fuels/select")
    public Page<fleet_vehicle_log_fuelImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_fuels/{id}/getdraft")
    public fleet_vehicle_log_fuelImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_log_fuelImpl fleet_vehicle_log_fuel);



}

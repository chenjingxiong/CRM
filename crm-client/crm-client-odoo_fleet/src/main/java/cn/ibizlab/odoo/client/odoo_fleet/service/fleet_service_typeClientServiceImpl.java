package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_service_type;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_service_typeClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_service_typeImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_service_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_service_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_service_typeClientServiceImpl implements Ifleet_service_typeClientService {

    fleet_service_typeFeignClient fleet_service_typeFeignClient;

    @Autowired
    public fleet_service_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_service_typeFeignClient = nameBuilder.target(fleet_service_typeFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_service_typeFeignClient = nameBuilder.target(fleet_service_typeFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_service_type createModel() {
		return new fleet_service_typeImpl();
	}


    public void updateBatch(List<Ifleet_service_type> fleet_service_types){
        if(fleet_service_types!=null){
            List<fleet_service_typeImpl> list = new ArrayList<fleet_service_typeImpl>();
            for(Ifleet_service_type ifleet_service_type :fleet_service_types){
                list.add((fleet_service_typeImpl)ifleet_service_type) ;
            }
            fleet_service_typeFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ifleet_service_type> fetchDefault(SearchContext context){
        Page<fleet_service_typeImpl> page = this.fleet_service_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ifleet_service_type fleet_service_type){
        Ifleet_service_type clientModel = fleet_service_typeFeignClient.update(fleet_service_type.getId(),(fleet_service_typeImpl)fleet_service_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_service_type.getClass(), false);
        copier.copy(clientModel, fleet_service_type, null);
    }


    public void removeBatch(List<Ifleet_service_type> fleet_service_types){
        if(fleet_service_types!=null){
            List<fleet_service_typeImpl> list = new ArrayList<fleet_service_typeImpl>();
            for(Ifleet_service_type ifleet_service_type :fleet_service_types){
                list.add((fleet_service_typeImpl)ifleet_service_type) ;
            }
            fleet_service_typeFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ifleet_service_type fleet_service_type){
        Ifleet_service_type clientModel = fleet_service_typeFeignClient.get(fleet_service_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_service_type.getClass(), false);
        copier.copy(clientModel, fleet_service_type, null);
    }


    public void createBatch(List<Ifleet_service_type> fleet_service_types){
        if(fleet_service_types!=null){
            List<fleet_service_typeImpl> list = new ArrayList<fleet_service_typeImpl>();
            for(Ifleet_service_type ifleet_service_type :fleet_service_types){
                list.add((fleet_service_typeImpl)ifleet_service_type) ;
            }
            fleet_service_typeFeignClient.createBatch(list) ;
        }
    }


    public void create(Ifleet_service_type fleet_service_type){
        Ifleet_service_type clientModel = fleet_service_typeFeignClient.create((fleet_service_typeImpl)fleet_service_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_service_type.getClass(), false);
        copier.copy(clientModel, fleet_service_type, null);
    }


    public void remove(Ifleet_service_type fleet_service_type){
        fleet_service_typeFeignClient.remove(fleet_service_type.getId()) ;
    }


    public Page<Ifleet_service_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_service_type fleet_service_type){
        Ifleet_service_type clientModel = fleet_service_typeFeignClient.getDraft(fleet_service_type.getId(),(fleet_service_typeImpl)fleet_service_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_service_type.getClass(), false);
        copier.copy(clientModel, fleet_service_type, null);
    }



}


package cn.ibizlab.odoo.client.odoo_fleet.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_cost;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[fleet_vehicle_cost] 对象
 */
public class fleet_vehicle_costImpl implements Ifleet_vehicle_cost,Serializable{

    /**
     * 总价
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 自动生成
     */
    public String auto_generated;

    @JsonIgnore
    public boolean auto_generatedDirtyFlag;
    
    /**
     * 合同
     */
    public Integer contract_id;

    @JsonIgnore
    public boolean contract_idDirtyFlag;
    
    /**
     * 合同
     */
    public String contract_id_text;

    @JsonIgnore
    public boolean contract_id_textDirtyFlag;
    
    /**
     * 包括服务
     */
    public String cost_ids;

    @JsonIgnore
    public boolean cost_idsDirtyFlag;
    
    /**
     * 类型
     */
    public Integer cost_subtype_id;

    @JsonIgnore
    public boolean cost_subtype_idDirtyFlag;
    
    /**
     * 类型
     */
    public String cost_subtype_id_text;

    @JsonIgnore
    public boolean cost_subtype_id_textDirtyFlag;
    
    /**
     * 费用所属类别
     */
    public String cost_type;

    @JsonIgnore
    public boolean cost_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 成本说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 里程表数值
     */
    public Double odometer;

    @JsonIgnore
    public boolean odometerDirtyFlag;
    
    /**
     * 里程表
     */
    public Integer odometer_id;

    @JsonIgnore
    public boolean odometer_idDirtyFlag;
    
    /**
     * 里程表
     */
    public String odometer_id_text;

    @JsonIgnore
    public boolean odometer_id_textDirtyFlag;
    
    /**
     * 单位
     */
    public String odometer_unit;

    @JsonIgnore
    public boolean odometer_unitDirtyFlag;
    
    /**
     * 上级
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 车辆
     */
    public Integer vehicle_id;

    @JsonIgnore
    public boolean vehicle_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [总价]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [总价]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [总价]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [自动生成]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return this.auto_generated ;
    }

    /**
     * 设置 [自动生成]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

     /**
     * 获取 [自动生成]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return this.auto_generatedDirtyFlag ;
    }   

    /**
     * 获取 [合同]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return this.contract_id ;
    }

    /**
     * 设置 [合同]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

     /**
     * 获取 [合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return this.contract_idDirtyFlag ;
    }   

    /**
     * 获取 [合同]
     */
    @JsonProperty("contract_id_text")
    public String getContract_id_text(){
        return this.contract_id_text ;
    }

    /**
     * 设置 [合同]
     */
    @JsonProperty("contract_id_text")
    public void setContract_id_text(String  contract_id_text){
        this.contract_id_text = contract_id_text ;
        this.contract_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_id_textDirtyFlag(){
        return this.contract_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包括服务]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return this.cost_ids ;
    }

    /**
     * 设置 [包括服务]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包括服务]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return this.cost_idsDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return this.cost_subtype_id ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return this.cost_subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("cost_subtype_id_text")
    public String getCost_subtype_id_text(){
        return this.cost_subtype_id_text ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("cost_subtype_id_text")
    public void setCost_subtype_id_text(String  cost_subtype_id_text){
        this.cost_subtype_id_text = cost_subtype_id_text ;
        this.cost_subtype_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_id_textDirtyFlag(){
        return this.cost_subtype_id_textDirtyFlag ;
    }   

    /**
     * 获取 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return this.cost_type ;
    }

    /**
     * 设置 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

     /**
     * 获取 [费用所属类别]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return this.cost_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [成本说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [成本说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [成本说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [里程表数值]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return this.odometer ;
    }

    /**
     * 设置 [里程表数值]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

     /**
     * 获取 [里程表数值]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return this.odometerDirtyFlag ;
    }   

    /**
     * 获取 [里程表]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return this.odometer_id ;
    }

    /**
     * 设置 [里程表]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

     /**
     * 获取 [里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return this.odometer_idDirtyFlag ;
    }   

    /**
     * 获取 [里程表]
     */
    @JsonProperty("odometer_id_text")
    public String getOdometer_id_text(){
        return this.odometer_id_text ;
    }

    /**
     * 设置 [里程表]
     */
    @JsonProperty("odometer_id_text")
    public void setOdometer_id_text(String  odometer_id_text){
        this.odometer_id_text = odometer_id_text ;
        this.odometer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_id_textDirtyFlag(){
        return this.odometer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return this.odometer_unit ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return this.odometer_unitDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [车辆]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return this.vehicle_id ;
    }

    /**
     * 设置 [车辆]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

     /**
     * 获取 [车辆]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return this.vehicle_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

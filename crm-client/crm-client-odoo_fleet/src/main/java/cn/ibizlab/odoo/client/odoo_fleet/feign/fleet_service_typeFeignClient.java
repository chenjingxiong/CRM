package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_service_type;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_service_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_service_type] 服务对象接口
 */
public interface fleet_service_typeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_service_types/updatebatch")
    public fleet_service_typeImpl updateBatch(@RequestBody List<fleet_service_typeImpl> fleet_service_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_service_types/fetchdefault")
    public Page<fleet_service_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_service_types/{id}")
    public fleet_service_typeImpl update(@PathVariable("id") Integer id,@RequestBody fleet_service_typeImpl fleet_service_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_service_types/removebatch")
    public fleet_service_typeImpl removeBatch(@RequestBody List<fleet_service_typeImpl> fleet_service_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_service_types/{id}")
    public fleet_service_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_service_types/createbatch")
    public fleet_service_typeImpl createBatch(@RequestBody List<fleet_service_typeImpl> fleet_service_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_service_types")
    public fleet_service_typeImpl create(@RequestBody fleet_service_typeImpl fleet_service_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_service_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_service_types/select")
    public Page<fleet_service_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_service_types/{id}/getdraft")
    public fleet_service_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_service_typeImpl fleet_service_type);



}

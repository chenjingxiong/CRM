package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_cost;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_costImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
public interface fleet_vehicle_costFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_costs/fetchdefault")
    public Page<fleet_vehicle_costImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_costs/createbatch")
    public fleet_vehicle_costImpl createBatch(@RequestBody List<fleet_vehicle_costImpl> fleet_vehicle_costs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_costs")
    public fleet_vehicle_costImpl create(@RequestBody fleet_vehicle_costImpl fleet_vehicle_cost);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_costs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_costs/{id}")
    public fleet_vehicle_costImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_costImpl fleet_vehicle_cost);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_costs/{id}")
    public fleet_vehicle_costImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_costs/removebatch")
    public fleet_vehicle_costImpl removeBatch(@RequestBody List<fleet_vehicle_costImpl> fleet_vehicle_costs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_costs/updatebatch")
    public fleet_vehicle_costImpl updateBatch(@RequestBody List<fleet_vehicle_costImpl> fleet_vehicle_costs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_costs/select")
    public Page<fleet_vehicle_costImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_costs/{id}/getdraft")
    public fleet_vehicle_costImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_costImpl fleet_vehicle_cost);



}

package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_lang;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_langClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_langImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_langFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_lang] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_langClientServiceImpl implements Ires_langClientService {

    res_langFeignClient res_langFeignClient;

    @Autowired
    public res_langClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_langFeignClient = nameBuilder.target(res_langFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_langFeignClient = nameBuilder.target(res_langFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_lang createModel() {
		return new res_langImpl();
	}


    public void get(Ires_lang res_lang){
        Ires_lang clientModel = res_langFeignClient.get(res_lang.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_lang.getClass(), false);
        copier.copy(clientModel, res_lang, null);
    }


    public void remove(Ires_lang res_lang){
        res_langFeignClient.remove(res_lang.getId()) ;
    }


    public void update(Ires_lang res_lang){
        Ires_lang clientModel = res_langFeignClient.update(res_lang.getId(),(res_langImpl)res_lang) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_lang.getClass(), false);
        copier.copy(clientModel, res_lang, null);
    }


    public void updateBatch(List<Ires_lang> res_langs){
        if(res_langs!=null){
            List<res_langImpl> list = new ArrayList<res_langImpl>();
            for(Ires_lang ires_lang :res_langs){
                list.add((res_langImpl)ires_lang) ;
            }
            res_langFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_lang> fetchDefault(SearchContext context){
        Page<res_langImpl> page = this.res_langFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ires_lang> res_langs){
        if(res_langs!=null){
            List<res_langImpl> list = new ArrayList<res_langImpl>();
            for(Ires_lang ires_lang :res_langs){
                list.add((res_langImpl)ires_lang) ;
            }
            res_langFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ires_lang res_lang){
        Ires_lang clientModel = res_langFeignClient.create((res_langImpl)res_lang) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_lang.getClass(), false);
        copier.copy(clientModel, res_lang, null);
    }


    public void createBatch(List<Ires_lang> res_langs){
        if(res_langs!=null){
            List<res_langImpl> list = new ArrayList<res_langImpl>();
            for(Ires_lang ires_lang :res_langs){
                list.add((res_langImpl)ires_lang) ;
            }
            res_langFeignClient.createBatch(list) ;
        }
    }


    public Page<Ires_lang> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_lang res_lang){
        Ires_lang clientModel = res_langFeignClient.getDraft(res_lang.getId(),(res_langImpl)res_lang) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_lang.getClass(), false);
        copier.copy(clientModel, res_lang, null);
    }



}


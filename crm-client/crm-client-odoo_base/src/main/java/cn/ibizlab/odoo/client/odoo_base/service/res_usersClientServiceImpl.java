package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_users;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_usersClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_usersImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_usersFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_users] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_usersClientServiceImpl implements Ires_usersClientService {

    res_usersFeignClient res_usersFeignClient;

    @Autowired
    public res_usersClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_usersFeignClient = nameBuilder.target(res_usersFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_usersFeignClient = nameBuilder.target(res_usersFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_users createModel() {
		return new res_usersImpl();
	}


    public void get(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.get(res_users.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }


    public void removeBatch(List<Ires_users> res_users){
        if(res_users!=null){
            List<res_usersImpl> list = new ArrayList<res_usersImpl>();
            for(Ires_users ires_users :res_users){
                list.add((res_usersImpl)ires_users) ;
            }
            res_usersFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_users res_users){
        res_usersFeignClient.remove(res_users.getId()) ;
    }


    public void create(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.create((res_usersImpl)res_users) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }


    public Page<Ires_users> fetchDefault(SearchContext context){
        Page<res_usersImpl> page = this.res_usersFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ires_users> res_users){
        if(res_users!=null){
            List<res_usersImpl> list = new ArrayList<res_usersImpl>();
            for(Ires_users ires_users :res_users){
                list.add((res_usersImpl)ires_users) ;
            }
            res_usersFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ires_users> res_users){
        if(res_users!=null){
            List<res_usersImpl> list = new ArrayList<res_usersImpl>();
            for(Ires_users ires_users :res_users){
                list.add((res_usersImpl)ires_users) ;
            }
            res_usersFeignClient.createBatch(list) ;
        }
    }


    public void update(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.update(res_users.getId(),(res_usersImpl)res_users) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }


    public Page<Ires_users> select(SearchContext context){
        return null ;
    }


    public void save(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.save(res_users.getId(),(res_usersImpl)res_users) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }


    public void checkKey(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.checkKey(res_users.getId(),(res_usersImpl)res_users) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }


    public void getDraft(Ires_users res_users){
        Ires_users clientModel = res_usersFeignClient.getDraft(res_users.getId(),(res_usersImpl)res_users) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users.getClass(), false);
        copier.copy(clientModel, res_users, null);
    }



}


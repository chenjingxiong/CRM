package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_partner;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_partner] 对象
 */
public class res_partnerImpl implements Ires_partner,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一个活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 附加信息
     */
    public String additional_info;

    @JsonIgnore
    public boolean additional_infoDirtyFlag;
    
    /**
     * 银行
     */
    public Integer bank_account_count;

    @JsonIgnore
    public boolean bank_account_countDirtyFlag;
    
    /**
     * 银行
     */
    public String bank_ids;

    @JsonIgnore
    public boolean bank_idsDirtyFlag;
    
    /**
     * 条码
     */
    public String barcode;

    @JsonIgnore
    public boolean barcodeDirtyFlag;
    
    /**
     * 最后的提醒已经标志为已读
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp calendar_last_notif_ack;

    @JsonIgnore
    public boolean calendar_last_notif_ackDirtyFlag;
    
    /**
     * 标签
     */
    public String category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 联系人
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 城市
     */
    public String city;

    @JsonIgnore
    public boolean cityDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 便签
     */
    public String comment;

    @JsonIgnore
    public boolean commentDirtyFlag;
    
    /**
     * 公司名称实体
     */
    public String commercial_company_name;

    @JsonIgnore
    public boolean commercial_company_nameDirtyFlag;
    
    /**
     * 商业实体
     */
    public Integer commercial_partner_id;

    @JsonIgnore
    public boolean commercial_partner_idDirtyFlag;
    
    /**
     * 商业实体
     */
    public String commercial_partner_id_text;

    @JsonIgnore
    public boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 公司名称
     */
    public String company_name;

    @JsonIgnore
    public boolean company_nameDirtyFlag;
    
    /**
     * 公司类别
     */
    public String company_type;

    @JsonIgnore
    public boolean company_typeDirtyFlag;
    
    /**
     * 完整地址
     */
    public String contact_address;

    @JsonIgnore
    public boolean contact_addressDirtyFlag;
    
    /**
     * 合同统计
     */
    public Integer contracts_count;

    @JsonIgnore
    public boolean contracts_countDirtyFlag;
    
    /**
     * 客户合同
     */
    public String contract_ids;

    @JsonIgnore
    public boolean contract_idsDirtyFlag;
    
    /**
     * 国家/地区
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家/地区
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 应收总计
     */
    public Double credit;

    @JsonIgnore
    public boolean creditDirtyFlag;
    
    /**
     * 信用额度
     */
    public Double credit_limit;

    @JsonIgnore
    public boolean credit_limitDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 客户
     */
    public String customer;

    @JsonIgnore
    public boolean customerDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 应付总计
     */
    public Double debit;

    @JsonIgnore
    public boolean debitDirtyFlag;
    
    /**
     * 应付限额
     */
    public Double debit_limit;

    @JsonIgnore
    public boolean debit_limitDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * EMail
     */
    public String email;

    @JsonIgnore
    public boolean emailDirtyFlag;
    
    /**
     * 格式化的邮件
     */
    public String email_formatted;

    @JsonIgnore
    public boolean email_formattedDirtyFlag;
    
    /**
     * 员工
     */
    public String employee;

    @JsonIgnore
    public boolean employeeDirtyFlag;
    
    /**
     * 活动
     */
    public Integer event_count;

    @JsonIgnore
    public boolean event_countDirtyFlag;
    
    /**
     * 有未核销的分录
     */
    public String has_unreconciled_entries;

    @JsonIgnore
    public boolean has_unreconciled_entriesDirtyFlag;
    
    /**
     * 工作岗位
     */
    public String ibizfunction;

    @JsonIgnore
    public boolean ibizfunctionDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * IM的状态
     */
    public String im_status;

    @JsonIgnore
    public boolean im_statusDirtyFlag;
    
    /**
     * 工业
     */
    public Integer industry_id;

    @JsonIgnore
    public boolean industry_idDirtyFlag;
    
    /**
     * 工业
     */
    public String industry_id_text;

    @JsonIgnore
    public boolean industry_id_textDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_warn;

    @JsonIgnore
    public boolean invoice_warnDirtyFlag;
    
    /**
     * 发票消息
     */
    public String invoice_warn_msg;

    @JsonIgnore
    public boolean invoice_warn_msgDirtyFlag;
    
    /**
     * 黑名单
     */
    public String is_blacklisted;

    @JsonIgnore
    public boolean is_blacklistedDirtyFlag;
    
    /**
     * 公司
     */
    public String is_company;

    @JsonIgnore
    public boolean is_companyDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 日记账项目
     */
    public Integer journal_item_count;

    @JsonIgnore
    public boolean journal_item_countDirtyFlag;
    
    /**
     * 语言
     */
    public String lang;

    @JsonIgnore
    public boolean langDirtyFlag;
    
    /**
     * 最近的发票和付款匹配时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp last_time_entries_checked;

    @JsonIgnore
    public boolean last_time_entries_checkedDirtyFlag;
    
    /**
     * 最近的在线销售订单
     */
    public Integer last_website_so_id;

    @JsonIgnore
    public boolean last_website_so_idDirtyFlag;
    
    /**
     * #会议
     */
    public Integer meeting_count;

    @JsonIgnore
    public boolean meeting_countDirtyFlag;
    
    /**
     * 会议
     */
    public String meeting_ids;

    @JsonIgnore
    public boolean meeting_idsDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 退回
     */
    public Integer message_bounce;

    @JsonIgnore
    public boolean message_bounceDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误个数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 前置操作
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作次数
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 手机
     */
    public String mobile;

    @JsonIgnore
    public boolean mobileDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 商机
     */
    public Integer opportunity_count;

    @JsonIgnore
    public boolean opportunity_countDirtyFlag;
    
    /**
     * 商机
     */
    public String opportunity_ids;

    @JsonIgnore
    public boolean opportunity_idsDirtyFlag;
    
    /**
     * 关联公司
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级名称
     */
    public String parent_name;

    @JsonIgnore
    public boolean parent_nameDirtyFlag;
    
    /**
     * 公司数据库ID
     */
    public Integer partner_gid;

    @JsonIgnore
    public boolean partner_gidDirtyFlag;
    
    /**
     * 共享合作伙伴
     */
    public String partner_share;

    @JsonIgnore
    public boolean partner_shareDirtyFlag;
    
    /**
     * 付款令牌计数
     */
    public Integer payment_token_count;

    @JsonIgnore
    public boolean payment_token_countDirtyFlag;
    
    /**
     * 付款令牌
     */
    public String payment_token_ids;

    @JsonIgnore
    public boolean payment_token_idsDirtyFlag;
    
    /**
     * 电话
     */
    public String phone;

    @JsonIgnore
    public boolean phoneDirtyFlag;
    
    /**
     * 库存拣货
     */
    public String picking_warn;

    @JsonIgnore
    public boolean picking_warnDirtyFlag;
    
    /**
     * 库存拣货单消息
     */
    public String picking_warn_msg;

    @JsonIgnore
    public boolean picking_warn_msgDirtyFlag;
    
    /**
     * 销售点订单计数
     */
    public Integer pos_order_count;

    @JsonIgnore
    public boolean pos_order_countDirtyFlag;
    
    /**
     * 应付账款
     */
    public Integer property_account_payable_id;

    @JsonIgnore
    public boolean property_account_payable_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer property_account_position_id;

    @JsonIgnore
    public boolean property_account_position_idDirtyFlag;
    
    /**
     * 应收账款
     */
    public Integer property_account_receivable_id;

    @JsonIgnore
    public boolean property_account_receivable_idDirtyFlag;
    
    /**
     * 客户付款条款
     */
    public Integer property_payment_term_id;

    @JsonIgnore
    public boolean property_payment_term_idDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer property_product_pricelist;

    @JsonIgnore
    public boolean property_product_pricelistDirtyFlag;
    
    /**
     * 供应商货币
     */
    public Integer property_purchase_currency_id;

    @JsonIgnore
    public boolean property_purchase_currency_idDirtyFlag;
    
    /**
     * 客户位置
     */
    public Integer property_stock_customer;

    @JsonIgnore
    public boolean property_stock_customerDirtyFlag;
    
    /**
     * 供应商位置
     */
    public Integer property_stock_supplier;

    @JsonIgnore
    public boolean property_stock_supplierDirtyFlag;
    
    /**
     * 供应商付款条款
     */
    public Integer property_supplier_payment_term_id;

    @JsonIgnore
    public boolean property_supplier_payment_term_idDirtyFlag;
    
    /**
     * 采购订单数
     */
    public Integer purchase_order_count;

    @JsonIgnore
    public boolean purchase_order_countDirtyFlag;
    
    /**
     * 采购订单
     */
    public String purchase_warn;

    @JsonIgnore
    public boolean purchase_warnDirtyFlag;
    
    /**
     * 采购订单消息
     */
    public String purchase_warn_msg;

    @JsonIgnore
    public boolean purchase_warn_msgDirtyFlag;
    
    /**
     * 内部参考
     */
    public String ref;

    @JsonIgnore
    public boolean refDirtyFlag;
    
    /**
     * 公司是指业务伙伴
     */
    public String ref_company_ids;

    @JsonIgnore
    public boolean ref_company_idsDirtyFlag;
    
    /**
     * 销售订单个数
     */
    public Integer sale_order_count;

    @JsonIgnore
    public boolean sale_order_countDirtyFlag;
    
    /**
     * 销售订单
     */
    public String sale_order_ids;

    @JsonIgnore
    public boolean sale_order_idsDirtyFlag;
    
    /**
     * 销售警告
     */
    public String sale_warn;

    @JsonIgnore
    public boolean sale_warnDirtyFlag;
    
    /**
     * 销售订单消息
     */
    public String sale_warn_msg;

    @JsonIgnore
    public boolean sale_warn_msgDirtyFlag;
    
    /**
     * 自己
     */
    public Integer self;

    @JsonIgnore
    public boolean selfDirtyFlag;
    
    /**
     * 注册到期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp signup_expiration;

    @JsonIgnore
    public boolean signup_expirationDirtyFlag;
    
    /**
     * 注册令牌 Token
     */
    public String signup_token;

    @JsonIgnore
    public boolean signup_tokenDirtyFlag;
    
    /**
     * 注册令牌（Token）类型
     */
    public String signup_type;

    @JsonIgnore
    public boolean signup_typeDirtyFlag;
    
    /**
     * 注册网址
     */
    public String signup_url;

    @JsonIgnore
    public boolean signup_urlDirtyFlag;
    
    /**
     * 注册令牌（ Token  ）是有效的
     */
    public String signup_valid;

    @JsonIgnore
    public boolean signup_validDirtyFlag;
    
    /**
     * 省/ 州
     */
    public Integer state_id;

    @JsonIgnore
    public boolean state_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String state_id_text;

    @JsonIgnore
    public boolean state_id_textDirtyFlag;
    
    /**
     * 街道
     */
    public String street;

    @JsonIgnore
    public boolean streetDirtyFlag;
    
    /**
     * 街道 2
     */
    public String street2;

    @JsonIgnore
    public boolean street2DirtyFlag;
    
    /**
     * 供应商
     */
    public String supplier;

    @JsonIgnore
    public boolean supplierDirtyFlag;
    
    /**
     * ＃供应商账单
     */
    public Integer supplier_invoice_count;

    @JsonIgnore
    public boolean supplier_invoice_countDirtyFlag;
    
    /**
     * # 任务
     */
    public Integer task_count;

    @JsonIgnore
    public boolean task_countDirtyFlag;
    
    /**
     * 任务
     */
    public String task_ids;

    @JsonIgnore
    public boolean task_idsDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 称谓
     */
    public Integer title;

    @JsonIgnore
    public boolean titleDirtyFlag;
    
    /**
     * 称谓
     */
    public String title_text;

    @JsonIgnore
    public boolean title_textDirtyFlag;
    
    /**
     * 已开票总计
     */
    public Double total_invoiced;

    @JsonIgnore
    public boolean total_invoicedDirtyFlag;
    
    /**
     * 对此债务人的信任度
     */
    public String trust;

    @JsonIgnore
    public boolean trustDirtyFlag;
    
    /**
     * 地址类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 时区
     */
    public String tz;

    @JsonIgnore
    public boolean tzDirtyFlag;
    
    /**
     * 时区偏移
     */
    public String tz_offset;

    @JsonIgnore
    public boolean tz_offsetDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 税号
     */
    public String vat;

    @JsonIgnore
    public boolean vatDirtyFlag;
    
    /**
     * 网站
     */
    public String website;

    @JsonIgnore
    public boolean websiteDirtyFlag;
    
    /**
     * 网站业务伙伴的详细说明
     */
    public String website_description;

    @JsonIgnore
    public boolean website_descriptionDirtyFlag;
    
    /**
     * 登记网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站业务伙伴简介
     */
    public String website_short_description;

    @JsonIgnore
    public boolean website_short_descriptionDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮政编码
     */
    public String zip;

    @JsonIgnore
    public boolean zipDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一个活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一个活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一个活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [附加信息]
     */
    @JsonProperty("additional_info")
    public String getAdditional_info(){
        return this.additional_info ;
    }

    /**
     * 设置 [附加信息]
     */
    @JsonProperty("additional_info")
    public void setAdditional_info(String  additional_info){
        this.additional_info = additional_info ;
        this.additional_infoDirtyFlag = true ;
    }

     /**
     * 获取 [附加信息]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_infoDirtyFlag(){
        return this.additional_infoDirtyFlag ;
    }   

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_account_count")
    public Integer getBank_account_count(){
        return this.bank_account_count ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_account_count")
    public void setBank_account_count(Integer  bank_account_count){
        this.bank_account_count = bank_account_count ;
        this.bank_account_countDirtyFlag = true ;
    }

     /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_countDirtyFlag(){
        return this.bank_account_countDirtyFlag ;
    }   

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return this.bank_ids ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

     /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return this.bank_idsDirtyFlag ;
    }   

    /**
     * 获取 [条码]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }   

    /**
     * 获取 [最后的提醒已经标志为已读]
     */
    @JsonProperty("calendar_last_notif_ack")
    public Timestamp getCalendar_last_notif_ack(){
        return this.calendar_last_notif_ack ;
    }

    /**
     * 设置 [最后的提醒已经标志为已读]
     */
    @JsonProperty("calendar_last_notif_ack")
    public void setCalendar_last_notif_ack(Timestamp  calendar_last_notif_ack){
        this.calendar_last_notif_ack = calendar_last_notif_ack ;
        this.calendar_last_notif_ackDirtyFlag = true ;
    }

     /**
     * 获取 [最后的提醒已经标志为已读]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_last_notif_ackDirtyFlag(){
        return this.calendar_last_notif_ackDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("category_id")
    public String getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("category_id")
    public void setCategory_id(String  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [联系人]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [联系人]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [联系人]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

     /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [便签]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [便签]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

     /**
     * 获取 [便签]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }   

    /**
     * 获取 [公司名称实体]
     */
    @JsonProperty("commercial_company_name")
    public String getCommercial_company_name(){
        return this.commercial_company_name ;
    }

    /**
     * 设置 [公司名称实体]
     */
    @JsonProperty("commercial_company_name")
    public void setCommercial_company_name(String  commercial_company_name){
        this.commercial_company_name = commercial_company_name ;
        this.commercial_company_nameDirtyFlag = true ;
    }

     /**
     * 获取 [公司名称实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_company_nameDirtyFlag(){
        return this.commercial_company_nameDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司名称]
     */
    @JsonProperty("company_name")
    public String getCompany_name(){
        return this.company_name ;
    }

    /**
     * 设置 [公司名称]
     */
    @JsonProperty("company_name")
    public void setCompany_name(String  company_name){
        this.company_name = company_name ;
        this.company_nameDirtyFlag = true ;
    }

     /**
     * 获取 [公司名称]脏标记
     */
    @JsonIgnore
    public boolean getCompany_nameDirtyFlag(){
        return this.company_nameDirtyFlag ;
    }   

    /**
     * 获取 [公司类别]
     */
    @JsonProperty("company_type")
    public String getCompany_type(){
        return this.company_type ;
    }

    /**
     * 设置 [公司类别]
     */
    @JsonProperty("company_type")
    public void setCompany_type(String  company_type){
        this.company_type = company_type ;
        this.company_typeDirtyFlag = true ;
    }

     /**
     * 获取 [公司类别]脏标记
     */
    @JsonIgnore
    public boolean getCompany_typeDirtyFlag(){
        return this.company_typeDirtyFlag ;
    }   

    /**
     * 获取 [完整地址]
     */
    @JsonProperty("contact_address")
    public String getContact_address(){
        return this.contact_address ;
    }

    /**
     * 设置 [完整地址]
     */
    @JsonProperty("contact_address")
    public void setContact_address(String  contact_address){
        this.contact_address = contact_address ;
        this.contact_addressDirtyFlag = true ;
    }

     /**
     * 获取 [完整地址]脏标记
     */
    @JsonIgnore
    public boolean getContact_addressDirtyFlag(){
        return this.contact_addressDirtyFlag ;
    }   

    /**
     * 获取 [合同统计]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return this.contracts_count ;
    }

    /**
     * 设置 [合同统计]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

     /**
     * 获取 [合同统计]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return this.contracts_countDirtyFlag ;
    }   

    /**
     * 获取 [客户合同]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return this.contract_ids ;
    }

    /**
     * 设置 [客户合同]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

     /**
     * 获取 [客户合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return this.contract_idsDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [应收总计]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return this.credit ;
    }

    /**
     * 设置 [应收总计]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

     /**
     * 获取 [应收总计]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return this.creditDirtyFlag ;
    }   

    /**
     * 获取 [信用额度]
     */
    @JsonProperty("credit_limit")
    public Double getCredit_limit(){
        return this.credit_limit ;
    }

    /**
     * 设置 [信用额度]
     */
    @JsonProperty("credit_limit")
    public void setCredit_limit(Double  credit_limit){
        this.credit_limit = credit_limit ;
        this.credit_limitDirtyFlag = true ;
    }

     /**
     * 获取 [信用额度]脏标记
     */
    @JsonIgnore
    public boolean getCredit_limitDirtyFlag(){
        return this.credit_limitDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("customer")
    public String getCustomer(){
        return this.customer ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("customer")
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.customerDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getCustomerDirtyFlag(){
        return this.customerDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [应付总计]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return this.debit ;
    }

    /**
     * 设置 [应付总计]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

     /**
     * 获取 [应付总计]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return this.debitDirtyFlag ;
    }   

    /**
     * 获取 [应付限额]
     */
    @JsonProperty("debit_limit")
    public Double getDebit_limit(){
        return this.debit_limit ;
    }

    /**
     * 设置 [应付限额]
     */
    @JsonProperty("debit_limit")
    public void setDebit_limit(Double  debit_limit){
        this.debit_limit = debit_limit ;
        this.debit_limitDirtyFlag = true ;
    }

     /**
     * 获取 [应付限额]脏标记
     */
    @JsonIgnore
    public boolean getDebit_limitDirtyFlag(){
        return this.debit_limitDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }   

    /**
     * 获取 [格式化的邮件]
     */
    @JsonProperty("email_formatted")
    public String getEmail_formatted(){
        return this.email_formatted ;
    }

    /**
     * 设置 [格式化的邮件]
     */
    @JsonProperty("email_formatted")
    public void setEmail_formatted(String  email_formatted){
        this.email_formatted = email_formatted ;
        this.email_formattedDirtyFlag = true ;
    }

     /**
     * 获取 [格式化的邮件]脏标记
     */
    @JsonIgnore
    public boolean getEmail_formattedDirtyFlag(){
        return this.email_formattedDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee")
    public String getEmployee(){
        return this.employee ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee")
    public void setEmployee(String  employee){
        this.employee = employee ;
        this.employeeDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployeeDirtyFlag(){
        return this.employeeDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_count")
    public Integer getEvent_count(){
        return this.event_count ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_count")
    public void setEvent_count(Integer  event_count){
        this.event_count = event_count ;
        this.event_countDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_countDirtyFlag(){
        return this.event_countDirtyFlag ;
    }   

    /**
     * 获取 [有未核销的分录]
     */
    @JsonProperty("has_unreconciled_entries")
    public String getHas_unreconciled_entries(){
        return this.has_unreconciled_entries ;
    }

    /**
     * 设置 [有未核销的分录]
     */
    @JsonProperty("has_unreconciled_entries")
    public void setHas_unreconciled_entries(String  has_unreconciled_entries){
        this.has_unreconciled_entries = has_unreconciled_entries ;
        this.has_unreconciled_entriesDirtyFlag = true ;
    }

     /**
     * 获取 [有未核销的分录]脏标记
     */
    @JsonIgnore
    public boolean getHas_unreconciled_entriesDirtyFlag(){
        return this.has_unreconciled_entriesDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return this.ibizfunction ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return this.ibizfunctionDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [IM的状态]
     */
    @JsonProperty("im_status")
    public String getIm_status(){
        return this.im_status ;
    }

    /**
     * 设置 [IM的状态]
     */
    @JsonProperty("im_status")
    public void setIm_status(String  im_status){
        this.im_status = im_status ;
        this.im_statusDirtyFlag = true ;
    }

     /**
     * 获取 [IM的状态]脏标记
     */
    @JsonIgnore
    public boolean getIm_statusDirtyFlag(){
        return this.im_statusDirtyFlag ;
    }   

    /**
     * 获取 [工业]
     */
    @JsonProperty("industry_id")
    public Integer getIndustry_id(){
        return this.industry_id ;
    }

    /**
     * 设置 [工业]
     */
    @JsonProperty("industry_id")
    public void setIndustry_id(Integer  industry_id){
        this.industry_id = industry_id ;
        this.industry_idDirtyFlag = true ;
    }

     /**
     * 获取 [工业]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_idDirtyFlag(){
        return this.industry_idDirtyFlag ;
    }   

    /**
     * 获取 [工业]
     */
    @JsonProperty("industry_id_text")
    public String getIndustry_id_text(){
        return this.industry_id_text ;
    }

    /**
     * 设置 [工业]
     */
    @JsonProperty("industry_id_text")
    public void setIndustry_id_text(String  industry_id_text){
        this.industry_id_text = industry_id_text ;
        this.industry_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工业]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_id_textDirtyFlag(){
        return this.industry_id_textDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_warn")
    public String getInvoice_warn(){
        return this.invoice_warn ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_warn")
    public void setInvoice_warn(String  invoice_warn){
        this.invoice_warn = invoice_warn ;
        this.invoice_warnDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warnDirtyFlag(){
        return this.invoice_warnDirtyFlag ;
    }   

    /**
     * 获取 [发票消息]
     */
    @JsonProperty("invoice_warn_msg")
    public String getInvoice_warn_msg(){
        return this.invoice_warn_msg ;
    }

    /**
     * 设置 [发票消息]
     */
    @JsonProperty("invoice_warn_msg")
    public void setInvoice_warn_msg(String  invoice_warn_msg){
        this.invoice_warn_msg = invoice_warn_msg ;
        this.invoice_warn_msgDirtyFlag = true ;
    }

     /**
     * 获取 [发票消息]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warn_msgDirtyFlag(){
        return this.invoice_warn_msgDirtyFlag ;
    }   

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

     /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("is_company")
    public String getIs_company(){
        return this.is_company ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("is_company")
    public void setIs_company(String  is_company){
        this.is_company = is_company ;
        this.is_companyDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getIs_companyDirtyFlag(){
        return this.is_companyDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目]
     */
    @JsonProperty("journal_item_count")
    public Integer getJournal_item_count(){
        return this.journal_item_count ;
    }

    /**
     * 设置 [日记账项目]
     */
    @JsonProperty("journal_item_count")
    public void setJournal_item_count(Integer  journal_item_count){
        this.journal_item_count = journal_item_count ;
        this.journal_item_countDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目]脏标记
     */
    @JsonIgnore
    public boolean getJournal_item_countDirtyFlag(){
        return this.journal_item_countDirtyFlag ;
    }   

    /**
     * 获取 [语言]
     */
    @JsonProperty("lang")
    public String getLang(){
        return this.lang ;
    }

    /**
     * 设置 [语言]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

     /**
     * 获取 [语言]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return this.langDirtyFlag ;
    }   

    /**
     * 获取 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return this.last_time_entries_checked ;
    }

    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

     /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return this.last_time_entries_checkedDirtyFlag ;
    }   

    /**
     * 获取 [最近的在线销售订单]
     */
    @JsonProperty("last_website_so_id")
    public Integer getLast_website_so_id(){
        return this.last_website_so_id ;
    }

    /**
     * 设置 [最近的在线销售订单]
     */
    @JsonProperty("last_website_so_id")
    public void setLast_website_so_id(Integer  last_website_so_id){
        this.last_website_so_id = last_website_so_id ;
        this.last_website_so_idDirtyFlag = true ;
    }

     /**
     * 获取 [最近的在线销售订单]脏标记
     */
    @JsonIgnore
    public boolean getLast_website_so_idDirtyFlag(){
        return this.last_website_so_idDirtyFlag ;
    }   

    /**
     * 获取 [#会议]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return this.meeting_count ;
    }

    /**
     * 设置 [#会议]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

     /**
     * 获取 [#会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return this.meeting_countDirtyFlag ;
    }   

    /**
     * 获取 [会议]
     */
    @JsonProperty("meeting_ids")
    public String getMeeting_ids(){
        return this.meeting_ids ;
    }

    /**
     * 设置 [会议]
     */
    @JsonProperty("meeting_ids")
    public void setMeeting_ids(String  meeting_ids){
        this.meeting_ids = meeting_ids ;
        this.meeting_idsDirtyFlag = true ;
    }

     /**
     * 获取 [会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_idsDirtyFlag(){
        return this.meeting_idsDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [退回]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return this.message_bounce ;
    }

    /**
     * 设置 [退回]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

     /**
     * 获取 [退回]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return this.message_bounceDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误个数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [前置操作]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [前置操作]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [前置操作]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作次数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [手机]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return this.mobile ;
    }

    /**
     * 设置 [手机]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

     /**
     * 获取 [手机]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return this.mobileDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_count")
    public Integer getOpportunity_count(){
        return this.opportunity_count ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_count")
    public void setOpportunity_count(Integer  opportunity_count){
        this.opportunity_count = opportunity_count ;
        this.opportunity_countDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_countDirtyFlag(){
        return this.opportunity_countDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_ids")
    public String getOpportunity_ids(){
        return this.opportunity_ids ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_ids")
    public void setOpportunity_ids(String  opportunity_ids){
        this.opportunity_ids = opportunity_ids ;
        this.opportunity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idsDirtyFlag(){
        return this.opportunity_idsDirtyFlag ;
    }   

    /**
     * 获取 [关联公司]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [关联公司]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [关联公司]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级名称]
     */
    @JsonProperty("parent_name")
    public String getParent_name(){
        return this.parent_name ;
    }

    /**
     * 设置 [上级名称]
     */
    @JsonProperty("parent_name")
    public void setParent_name(String  parent_name){
        this.parent_name = parent_name ;
        this.parent_nameDirtyFlag = true ;
    }

     /**
     * 获取 [上级名称]脏标记
     */
    @JsonIgnore
    public boolean getParent_nameDirtyFlag(){
        return this.parent_nameDirtyFlag ;
    }   

    /**
     * 获取 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return this.partner_gid ;
    }

    /**
     * 设置 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

     /**
     * 获取 [公司数据库ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return this.partner_gidDirtyFlag ;
    }   

    /**
     * 获取 [共享合作伙伴]
     */
    @JsonProperty("partner_share")
    public String getPartner_share(){
        return this.partner_share ;
    }

    /**
     * 设置 [共享合作伙伴]
     */
    @JsonProperty("partner_share")
    public void setPartner_share(String  partner_share){
        this.partner_share = partner_share ;
        this.partner_shareDirtyFlag = true ;
    }

     /**
     * 获取 [共享合作伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shareDirtyFlag(){
        return this.partner_shareDirtyFlag ;
    }   

    /**
     * 获取 [付款令牌计数]
     */
    @JsonProperty("payment_token_count")
    public Integer getPayment_token_count(){
        return this.payment_token_count ;
    }

    /**
     * 设置 [付款令牌计数]
     */
    @JsonProperty("payment_token_count")
    public void setPayment_token_count(Integer  payment_token_count){
        this.payment_token_count = payment_token_count ;
        this.payment_token_countDirtyFlag = true ;
    }

     /**
     * 获取 [付款令牌计数]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_countDirtyFlag(){
        return this.payment_token_countDirtyFlag ;
    }   

    /**
     * 获取 [付款令牌]
     */
    @JsonProperty("payment_token_ids")
    public String getPayment_token_ids(){
        return this.payment_token_ids ;
    }

    /**
     * 设置 [付款令牌]
     */
    @JsonProperty("payment_token_ids")
    public void setPayment_token_ids(String  payment_token_ids){
        this.payment_token_ids = payment_token_ids ;
        this.payment_token_idsDirtyFlag = true ;
    }

     /**
     * 获取 [付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idsDirtyFlag(){
        return this.payment_token_idsDirtyFlag ;
    }   

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

     /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_warn")
    public String getPicking_warn(){
        return this.picking_warn ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_warn")
    public void setPicking_warn(String  picking_warn){
        this.picking_warn = picking_warn ;
        this.picking_warnDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warnDirtyFlag(){
        return this.picking_warnDirtyFlag ;
    }   

    /**
     * 获取 [库存拣货单消息]
     */
    @JsonProperty("picking_warn_msg")
    public String getPicking_warn_msg(){
        return this.picking_warn_msg ;
    }

    /**
     * 设置 [库存拣货单消息]
     */
    @JsonProperty("picking_warn_msg")
    public void setPicking_warn_msg(String  picking_warn_msg){
        this.picking_warn_msg = picking_warn_msg ;
        this.picking_warn_msgDirtyFlag = true ;
    }

     /**
     * 获取 [库存拣货单消息]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warn_msgDirtyFlag(){
        return this.picking_warn_msgDirtyFlag ;
    }   

    /**
     * 获取 [销售点订单计数]
     */
    @JsonProperty("pos_order_count")
    public Integer getPos_order_count(){
        return this.pos_order_count ;
    }

    /**
     * 设置 [销售点订单计数]
     */
    @JsonProperty("pos_order_count")
    public void setPos_order_count(Integer  pos_order_count){
        this.pos_order_count = pos_order_count ;
        this.pos_order_countDirtyFlag = true ;
    }

     /**
     * 获取 [销售点订单计数]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_countDirtyFlag(){
        return this.pos_order_countDirtyFlag ;
    }   

    /**
     * 获取 [应付账款]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return this.property_account_payable_id ;
    }

    /**
     * 设置 [应付账款]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

     /**
     * 获取 [应付账款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return this.property_account_payable_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("property_account_position_id")
    public Integer getProperty_account_position_id(){
        return this.property_account_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("property_account_position_id")
    public void setProperty_account_position_id(Integer  property_account_position_id){
        this.property_account_position_id = property_account_position_id ;
        this.property_account_position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_position_idDirtyFlag(){
        return this.property_account_position_idDirtyFlag ;
    }   

    /**
     * 获取 [应收账款]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return this.property_account_receivable_id ;
    }

    /**
     * 设置 [应收账款]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

     /**
     * 获取 [应收账款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return this.property_account_receivable_idDirtyFlag ;
    }   

    /**
     * 获取 [客户付款条款]
     */
    @JsonProperty("property_payment_term_id")
    public Integer getProperty_payment_term_id(){
        return this.property_payment_term_id ;
    }

    /**
     * 设置 [客户付款条款]
     */
    @JsonProperty("property_payment_term_id")
    public void setProperty_payment_term_id(Integer  property_payment_term_id){
        this.property_payment_term_id = property_payment_term_id ;
        this.property_payment_term_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户付款条款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_payment_term_idDirtyFlag(){
        return this.property_payment_term_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("property_product_pricelist")
    public Integer getProperty_product_pricelist(){
        return this.property_product_pricelist ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("property_product_pricelist")
    public void setProperty_product_pricelist(Integer  property_product_pricelist){
        this.property_product_pricelist = property_product_pricelist ;
        this.property_product_pricelistDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getProperty_product_pricelistDirtyFlag(){
        return this.property_product_pricelistDirtyFlag ;
    }   

    /**
     * 获取 [供应商货币]
     */
    @JsonProperty("property_purchase_currency_id")
    public Integer getProperty_purchase_currency_id(){
        return this.property_purchase_currency_id ;
    }

    /**
     * 设置 [供应商货币]
     */
    @JsonProperty("property_purchase_currency_id")
    public void setProperty_purchase_currency_id(Integer  property_purchase_currency_id){
        this.property_purchase_currency_id = property_purchase_currency_id ;
        this.property_purchase_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商货币]脏标记
     */
    @JsonIgnore
    public boolean getProperty_purchase_currency_idDirtyFlag(){
        return this.property_purchase_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [客户位置]
     */
    @JsonProperty("property_stock_customer")
    public Integer getProperty_stock_customer(){
        return this.property_stock_customer ;
    }

    /**
     * 设置 [客户位置]
     */
    @JsonProperty("property_stock_customer")
    public void setProperty_stock_customer(Integer  property_stock_customer){
        this.property_stock_customer = property_stock_customer ;
        this.property_stock_customerDirtyFlag = true ;
    }

     /**
     * 获取 [客户位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_customerDirtyFlag(){
        return this.property_stock_customerDirtyFlag ;
    }   

    /**
     * 获取 [供应商位置]
     */
    @JsonProperty("property_stock_supplier")
    public Integer getProperty_stock_supplier(){
        return this.property_stock_supplier ;
    }

    /**
     * 设置 [供应商位置]
     */
    @JsonProperty("property_stock_supplier")
    public void setProperty_stock_supplier(Integer  property_stock_supplier){
        this.property_stock_supplier = property_stock_supplier ;
        this.property_stock_supplierDirtyFlag = true ;
    }

     /**
     * 获取 [供应商位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_supplierDirtyFlag(){
        return this.property_stock_supplierDirtyFlag ;
    }   

    /**
     * 获取 [供应商付款条款]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public Integer getProperty_supplier_payment_term_id(){
        return this.property_supplier_payment_term_id ;
    }

    /**
     * 设置 [供应商付款条款]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public void setProperty_supplier_payment_term_id(Integer  property_supplier_payment_term_id){
        this.property_supplier_payment_term_id = property_supplier_payment_term_id ;
        this.property_supplier_payment_term_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商付款条款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_supplier_payment_term_idDirtyFlag(){
        return this.property_supplier_payment_term_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单数]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return this.purchase_order_count ;
    }

    /**
     * 设置 [采购订单数]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单数]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return this.purchase_order_countDirtyFlag ;
    }   

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_warn")
    public String getPurchase_warn(){
        return this.purchase_warn ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_warn")
    public void setPurchase_warn(String  purchase_warn){
        this.purchase_warn = purchase_warn ;
        this.purchase_warnDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warnDirtyFlag(){
        return this.purchase_warnDirtyFlag ;
    }   

    /**
     * 获取 [采购订单消息]
     */
    @JsonProperty("purchase_warn_msg")
    public String getPurchase_warn_msg(){
        return this.purchase_warn_msg ;
    }

    /**
     * 设置 [采购订单消息]
     */
    @JsonProperty("purchase_warn_msg")
    public void setPurchase_warn_msg(String  purchase_warn_msg){
        this.purchase_warn_msg = purchase_warn_msg ;
        this.purchase_warn_msgDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单消息]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warn_msgDirtyFlag(){
        return this.purchase_warn_msgDirtyFlag ;
    }   

    /**
     * 获取 [内部参考]
     */
    @JsonProperty("ref")
    public String getRef(){
        return this.ref ;
    }

    /**
     * 设置 [内部参考]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

     /**
     * 获取 [内部参考]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return this.refDirtyFlag ;
    }   

    /**
     * 获取 [公司是指业务伙伴]
     */
    @JsonProperty("ref_company_ids")
    public String getRef_company_ids(){
        return this.ref_company_ids ;
    }

    /**
     * 设置 [公司是指业务伙伴]
     */
    @JsonProperty("ref_company_ids")
    public void setRef_company_ids(String  ref_company_ids){
        this.ref_company_ids = ref_company_ids ;
        this.ref_company_idsDirtyFlag = true ;
    }

     /**
     * 获取 [公司是指业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getRef_company_idsDirtyFlag(){
        return this.ref_company_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售订单个数]
     */
    @JsonProperty("sale_order_count")
    public Integer getSale_order_count(){
        return this.sale_order_count ;
    }

    /**
     * 设置 [销售订单个数]
     */
    @JsonProperty("sale_order_count")
    public void setSale_order_count(Integer  sale_order_count){
        this.sale_order_count = sale_order_count ;
        this.sale_order_countDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单个数]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_countDirtyFlag(){
        return this.sale_order_countDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return this.sale_order_ids ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return this.sale_order_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售警告]
     */
    @JsonProperty("sale_warn")
    public String getSale_warn(){
        return this.sale_warn ;
    }

    /**
     * 设置 [销售警告]
     */
    @JsonProperty("sale_warn")
    public void setSale_warn(String  sale_warn){
        this.sale_warn = sale_warn ;
        this.sale_warnDirtyFlag = true ;
    }

     /**
     * 获取 [销售警告]脏标记
     */
    @JsonIgnore
    public boolean getSale_warnDirtyFlag(){
        return this.sale_warnDirtyFlag ;
    }   

    /**
     * 获取 [销售订单消息]
     */
    @JsonProperty("sale_warn_msg")
    public String getSale_warn_msg(){
        return this.sale_warn_msg ;
    }

    /**
     * 设置 [销售订单消息]
     */
    @JsonProperty("sale_warn_msg")
    public void setSale_warn_msg(String  sale_warn_msg){
        this.sale_warn_msg = sale_warn_msg ;
        this.sale_warn_msgDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单消息]脏标记
     */
    @JsonIgnore
    public boolean getSale_warn_msgDirtyFlag(){
        return this.sale_warn_msgDirtyFlag ;
    }   

    /**
     * 获取 [自己]
     */
    @JsonProperty("self")
    public Integer getSelf(){
        return this.self ;
    }

    /**
     * 设置 [自己]
     */
    @JsonProperty("self")
    public void setSelf(Integer  self){
        this.self = self ;
        this.selfDirtyFlag = true ;
    }

     /**
     * 获取 [自己]脏标记
     */
    @JsonIgnore
    public boolean getSelfDirtyFlag(){
        return this.selfDirtyFlag ;
    }   

    /**
     * 获取 [注册到期]
     */
    @JsonProperty("signup_expiration")
    public Timestamp getSignup_expiration(){
        return this.signup_expiration ;
    }

    /**
     * 设置 [注册到期]
     */
    @JsonProperty("signup_expiration")
    public void setSignup_expiration(Timestamp  signup_expiration){
        this.signup_expiration = signup_expiration ;
        this.signup_expirationDirtyFlag = true ;
    }

     /**
     * 获取 [注册到期]脏标记
     */
    @JsonIgnore
    public boolean getSignup_expirationDirtyFlag(){
        return this.signup_expirationDirtyFlag ;
    }   

    /**
     * 获取 [注册令牌 Token]
     */
    @JsonProperty("signup_token")
    public String getSignup_token(){
        return this.signup_token ;
    }

    /**
     * 设置 [注册令牌 Token]
     */
    @JsonProperty("signup_token")
    public void setSignup_token(String  signup_token){
        this.signup_token = signup_token ;
        this.signup_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [注册令牌 Token]脏标记
     */
    @JsonIgnore
    public boolean getSignup_tokenDirtyFlag(){
        return this.signup_tokenDirtyFlag ;
    }   

    /**
     * 获取 [注册令牌（Token）类型]
     */
    @JsonProperty("signup_type")
    public String getSignup_type(){
        return this.signup_type ;
    }

    /**
     * 设置 [注册令牌（Token）类型]
     */
    @JsonProperty("signup_type")
    public void setSignup_type(String  signup_type){
        this.signup_type = signup_type ;
        this.signup_typeDirtyFlag = true ;
    }

     /**
     * 获取 [注册令牌（Token）类型]脏标记
     */
    @JsonIgnore
    public boolean getSignup_typeDirtyFlag(){
        return this.signup_typeDirtyFlag ;
    }   

    /**
     * 获取 [注册网址]
     */
    @JsonProperty("signup_url")
    public String getSignup_url(){
        return this.signup_url ;
    }

    /**
     * 设置 [注册网址]
     */
    @JsonProperty("signup_url")
    public void setSignup_url(String  signup_url){
        this.signup_url = signup_url ;
        this.signup_urlDirtyFlag = true ;
    }

     /**
     * 获取 [注册网址]脏标记
     */
    @JsonIgnore
    public boolean getSignup_urlDirtyFlag(){
        return this.signup_urlDirtyFlag ;
    }   

    /**
     * 获取 [注册令牌（ Token  ）是有效的]
     */
    @JsonProperty("signup_valid")
    public String getSignup_valid(){
        return this.signup_valid ;
    }

    /**
     * 设置 [注册令牌（ Token  ）是有效的]
     */
    @JsonProperty("signup_valid")
    public void setSignup_valid(String  signup_valid){
        this.signup_valid = signup_valid ;
        this.signup_validDirtyFlag = true ;
    }

     /**
     * 获取 [注册令牌（ Token  ）是有效的]脏标记
     */
    @JsonIgnore
    public boolean getSignup_validDirtyFlag(){
        return this.signup_validDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return this.state_id_text ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return this.state_id_textDirtyFlag ;
    }   

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

     /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }   

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

     /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("supplier")
    public String getSupplier(){
        return this.supplier ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("supplier")
    public void setSupplier(String  supplier){
        this.supplier = supplier ;
        this.supplierDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getSupplierDirtyFlag(){
        return this.supplierDirtyFlag ;
    }   

    /**
     * 获取 [＃供应商账单]
     */
    @JsonProperty("supplier_invoice_count")
    public Integer getSupplier_invoice_count(){
        return this.supplier_invoice_count ;
    }

    /**
     * 设置 [＃供应商账单]
     */
    @JsonProperty("supplier_invoice_count")
    public void setSupplier_invoice_count(Integer  supplier_invoice_count){
        this.supplier_invoice_count = supplier_invoice_count ;
        this.supplier_invoice_countDirtyFlag = true ;
    }

     /**
     * 获取 [＃供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_invoice_countDirtyFlag(){
        return this.supplier_invoice_countDirtyFlag ;
    }   

    /**
     * 获取 [# 任务]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return this.task_count ;
    }

    /**
     * 设置 [# 任务]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 任务]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return this.task_countDirtyFlag ;
    }   

    /**
     * 获取 [任务]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return this.task_ids ;
    }

    /**
     * 设置 [任务]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

     /**
     * 获取 [任务]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return this.task_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return this.title ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

     /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return this.titleDirtyFlag ;
    }   

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title_text")
    public String getTitle_text(){
        return this.title_text ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title_text")
    public void setTitle_text(String  title_text){
        this.title_text = title_text ;
        this.title_textDirtyFlag = true ;
    }

     /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitle_textDirtyFlag(){
        return this.title_textDirtyFlag ;
    }   

    /**
     * 获取 [已开票总计]
     */
    @JsonProperty("total_invoiced")
    public Double getTotal_invoiced(){
        return this.total_invoiced ;
    }

    /**
     * 设置 [已开票总计]
     */
    @JsonProperty("total_invoiced")
    public void setTotal_invoiced(Double  total_invoiced){
        this.total_invoiced = total_invoiced ;
        this.total_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [已开票总计]脏标记
     */
    @JsonIgnore
    public boolean getTotal_invoicedDirtyFlag(){
        return this.total_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [对此债务人的信任度]
     */
    @JsonProperty("trust")
    public String getTrust(){
        return this.trust ;
    }

    /**
     * 设置 [对此债务人的信任度]
     */
    @JsonProperty("trust")
    public void setTrust(String  trust){
        this.trust = trust ;
        this.trustDirtyFlag = true ;
    }

     /**
     * 获取 [对此债务人的信任度]脏标记
     */
    @JsonIgnore
    public boolean getTrustDirtyFlag(){
        return this.trustDirtyFlag ;
    }   

    /**
     * 获取 [地址类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [地址类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [地址类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("tz")
    public String getTz(){
        return this.tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return this.tzDirtyFlag ;
    }   

    /**
     * 获取 [时区偏移]
     */
    @JsonProperty("tz_offset")
    public String getTz_offset(){
        return this.tz_offset ;
    }

    /**
     * 设置 [时区偏移]
     */
    @JsonProperty("tz_offset")
    public void setTz_offset(String  tz_offset){
        this.tz_offset = tz_offset ;
        this.tz_offsetDirtyFlag = true ;
    }

     /**
     * 获取 [时区偏移]脏标记
     */
    @JsonIgnore
    public boolean getTz_offsetDirtyFlag(){
        return this.tz_offsetDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税号]
     */
    @JsonProperty("vat")
    public String getVat(){
        return this.vat ;
    }

    /**
     * 设置 [税号]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

     /**
     * 获取 [税号]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return this.vatDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return this.website ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return this.websiteDirtyFlag ;
    }   

    /**
     * 获取 [网站业务伙伴的详细说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站业务伙伴的详细说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站业务伙伴的详细说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [登记网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [登记网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [登记网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站业务伙伴简介]
     */
    @JsonProperty("website_short_description")
    public String getWebsite_short_description(){
        return this.website_short_description ;
    }

    /**
     * 设置 [网站业务伙伴简介]
     */
    @JsonProperty("website_short_description")
    public void setWebsite_short_description(String  website_short_description){
        this.website_short_description = website_short_description ;
        this.website_short_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站业务伙伴简介]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_short_descriptionDirtyFlag(){
        return this.website_short_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

     /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

	private List <cn.ibizlab.odoo.core.client.model.Ires_partner_bank> res_partner_banks = new ArrayList<cn.ibizlab.odoo.core.client.model.Ires_partner_bank>() ;

    /**
     * 获取 [res_partner_banks]
     */
    @JsonProperty("res_partner_banks")
    public List<cn.ibizlab.odoo.core.client.model.Ires_partner_bank> getRes_partner_banks(){
        return res_partner_banks ;
    }

    /**
     * 获取 [res_partner_banks]
     */

    public void setRes_partner_banks(List <cn.ibizlab.odoo.core.client.model.Ires_partner_bank> res_partner_banks){
        this.res_partner_banks = res_partner_banks ;
    }

    /**
     * 反序列化辅助方法 [res_partner_banks]
     */
    @JsonProperty("res_partner_banks")
    public void setRes_partner_banks2(List<cn.ibizlab.odoo.client.odoo_base.model.res_partner_bankImpl> res_partner_banks){
        for (cn.ibizlab.odoo.client.odoo_base.model.res_partner_bankImpl res_partner_bankImpl : res_partner_banks)
        {
            this.res_partner_banks.add(res_partner_bankImpl);
        }
    }
}

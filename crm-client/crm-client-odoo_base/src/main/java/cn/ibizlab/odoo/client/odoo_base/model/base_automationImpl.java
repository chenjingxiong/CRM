package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ibase_automation;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[base_automation] 对象
 */
public class base_automationImpl implements Ibase_automation,Serializable{

    /**
     * 服务器动作
     */
    public Integer action_server_id;

    @JsonIgnore
    public boolean action_server_idDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 截止日期至
     */
    public Integer activity_date_deadline_range;

    @JsonIgnore
    public boolean activity_date_deadline_rangeDirtyFlag;
    
    /**
     * 到期类型
     */
    public String activity_date_deadline_range_type;

    @JsonIgnore
    public boolean activity_date_deadline_range_typeDirtyFlag;
    
    /**
     * 备注
     */
    public String activity_note;

    @JsonIgnore
    public boolean activity_noteDirtyFlag;
    
    /**
     * 摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 活动
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 用户字段名字
     */
    public String activity_user_field_name;

    @JsonIgnore
    public boolean activity_user_field_nameDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 活动用户类型
     */
    public String activity_user_type;

    @JsonIgnore
    public boolean activity_user_typeDirtyFlag;
    
    /**
     * 绑定模型
     */
    public Integer binding_model_id;

    @JsonIgnore
    public boolean binding_model_idDirtyFlag;
    
    /**
     * 绑定类型
     */
    public String binding_type;

    @JsonIgnore
    public boolean binding_typeDirtyFlag;
    
    /**
     * 添加频道
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 下级动作
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * Python 代码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 创建/写目标模型
     */
    public Integer crud_model_id;

    @JsonIgnore
    public boolean crud_model_idDirtyFlag;
    
    /**
     * 目标模型
     */
    public String crud_model_name;

    @JsonIgnore
    public boolean crud_model_nameDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 映射的值
     */
    public String fields_lines;

    @JsonIgnore
    public boolean fields_linesDirtyFlag;
    
    /**
     * 应用于
     */
    public String filter_domain;

    @JsonIgnore
    public boolean filter_domainDirtyFlag;
    
    /**
     * 更新前域表达式
     */
    public String filter_pre_domain;

    @JsonIgnore
    public boolean filter_pre_domainDirtyFlag;
    
    /**
     * 动作说明
     */
    public String help;

    @JsonIgnore
    public boolean helpDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最后运行
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp last_run;

    @JsonIgnore
    public boolean last_runDirtyFlag;
    
    /**
     * 链接使用字段
     */
    public Integer link_field_id;

    @JsonIgnore
    public boolean link_field_idDirtyFlag;
    
    /**
     * 模型
     */
    public Integer model_id;

    @JsonIgnore
    public boolean model_idDirtyFlag;
    
    /**
     * 模型名称
     */
    public String model_name;

    @JsonIgnore
    public boolean model_nameDirtyFlag;
    
    /**
     * 动作名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 变化字段的触发器
     */
    public String on_change_fields;

    @JsonIgnore
    public boolean on_change_fieldsDirtyFlag;
    
    /**
     * 添加关注者
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 待办的行动
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * EMail模板
     */
    public Integer template_id;

    @JsonIgnore
    public boolean template_idDirtyFlag;
    
    /**
     * 使用日历
     */
    public Integer trg_date_calendar_id;

    @JsonIgnore
    public boolean trg_date_calendar_idDirtyFlag;
    
    /**
     * 使用日历
     */
    public String trg_date_calendar_id_text;

    @JsonIgnore
    public boolean trg_date_calendar_id_textDirtyFlag;
    
    /**
     * 触发日期
     */
    public Integer trg_date_id;

    @JsonIgnore
    public boolean trg_date_idDirtyFlag;
    
    /**
     * 触发日期后的延迟
     */
    public Integer trg_date_range;

    @JsonIgnore
    public boolean trg_date_rangeDirtyFlag;
    
    /**
     * 延迟类型
     */
    public String trg_date_range_type;

    @JsonIgnore
    public boolean trg_date_range_typeDirtyFlag;
    
    /**
     * 触发条件
     */
    public String trigger;

    @JsonIgnore
    public boolean triggerDirtyFlag;
    
    /**
     * 动作类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 用途
     */
    public String usage;

    @JsonIgnore
    public boolean usageDirtyFlag;
    
    /**
     * 网站路径
     */
    public String website_path;

    @JsonIgnore
    public boolean website_pathDirtyFlag;
    
    /**
     * 可用于网站
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 外部 ID
     */
    public String xml_id;

    @JsonIgnore
    public boolean xml_idDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [服务器动作]
     */
    @JsonProperty("action_server_id")
    public Integer getAction_server_id(){
        return this.action_server_id ;
    }

    /**
     * 设置 [服务器动作]
     */
    @JsonProperty("action_server_id")
    public void setAction_server_id(Integer  action_server_id){
        this.action_server_id = action_server_id ;
        this.action_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [服务器动作]脏标记
     */
    @JsonIgnore
    public boolean getAction_server_idDirtyFlag(){
        return this.action_server_idDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [截止日期至]
     */
    @JsonProperty("activity_date_deadline_range")
    public Integer getActivity_date_deadline_range(){
        return this.activity_date_deadline_range ;
    }

    /**
     * 设置 [截止日期至]
     */
    @JsonProperty("activity_date_deadline_range")
    public void setActivity_date_deadline_range(Integer  activity_date_deadline_range){
        this.activity_date_deadline_range = activity_date_deadline_range ;
        this.activity_date_deadline_rangeDirtyFlag = true ;
    }

     /**
     * 获取 [截止日期至]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadline_rangeDirtyFlag(){
        return this.activity_date_deadline_rangeDirtyFlag ;
    }   

    /**
     * 获取 [到期类型]
     */
    @JsonProperty("activity_date_deadline_range_type")
    public String getActivity_date_deadline_range_type(){
        return this.activity_date_deadline_range_type ;
    }

    /**
     * 设置 [到期类型]
     */
    @JsonProperty("activity_date_deadline_range_type")
    public void setActivity_date_deadline_range_type(String  activity_date_deadline_range_type){
        this.activity_date_deadline_range_type = activity_date_deadline_range_type ;
        this.activity_date_deadline_range_typeDirtyFlag = true ;
    }

     /**
     * 获取 [到期类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadline_range_typeDirtyFlag(){
        return this.activity_date_deadline_range_typeDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("activity_note")
    public String getActivity_note(){
        return this.activity_note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("activity_note")
    public void setActivity_note(String  activity_note){
        this.activity_note = activity_note ;
        this.activity_noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getActivity_noteDirtyFlag(){
        return this.activity_noteDirtyFlag ;
    }   

    /**
     * 获取 [摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [用户字段名字]
     */
    @JsonProperty("activity_user_field_name")
    public String getActivity_user_field_name(){
        return this.activity_user_field_name ;
    }

    /**
     * 设置 [用户字段名字]
     */
    @JsonProperty("activity_user_field_name")
    public void setActivity_user_field_name(String  activity_user_field_name){
        this.activity_user_field_name = activity_user_field_name ;
        this.activity_user_field_nameDirtyFlag = true ;
    }

     /**
     * 获取 [用户字段名字]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_field_nameDirtyFlag(){
        return this.activity_user_field_nameDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [活动用户类型]
     */
    @JsonProperty("activity_user_type")
    public String getActivity_user_type(){
        return this.activity_user_type ;
    }

    /**
     * 设置 [活动用户类型]
     */
    @JsonProperty("activity_user_type")
    public void setActivity_user_type(String  activity_user_type){
        this.activity_user_type = activity_user_type ;
        this.activity_user_typeDirtyFlag = true ;
    }

     /**
     * 获取 [活动用户类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_typeDirtyFlag(){
        return this.activity_user_typeDirtyFlag ;
    }   

    /**
     * 获取 [绑定模型]
     */
    @JsonProperty("binding_model_id")
    public Integer getBinding_model_id(){
        return this.binding_model_id ;
    }

    /**
     * 设置 [绑定模型]
     */
    @JsonProperty("binding_model_id")
    public void setBinding_model_id(Integer  binding_model_id){
        this.binding_model_id = binding_model_id ;
        this.binding_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [绑定模型]脏标记
     */
    @JsonIgnore
    public boolean getBinding_model_idDirtyFlag(){
        return this.binding_model_idDirtyFlag ;
    }   

    /**
     * 获取 [绑定类型]
     */
    @JsonProperty("binding_type")
    public String getBinding_type(){
        return this.binding_type ;
    }

    /**
     * 设置 [绑定类型]
     */
    @JsonProperty("binding_type")
    public void setBinding_type(String  binding_type){
        this.binding_type = binding_type ;
        this.binding_typeDirtyFlag = true ;
    }

     /**
     * 获取 [绑定类型]脏标记
     */
    @JsonIgnore
    public boolean getBinding_typeDirtyFlag(){
        return this.binding_typeDirtyFlag ;
    }   

    /**
     * 获取 [添加频道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [添加频道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [添加频道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [下级动作]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下级动作]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下级动作]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [Python 代码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [Python 代码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [Python 代码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [创建/写目标模型]
     */
    @JsonProperty("crud_model_id")
    public Integer getCrud_model_id(){
        return this.crud_model_id ;
    }

    /**
     * 设置 [创建/写目标模型]
     */
    @JsonProperty("crud_model_id")
    public void setCrud_model_id(Integer  crud_model_id){
        this.crud_model_id = crud_model_id ;
        this.crud_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建/写目标模型]脏标记
     */
    @JsonIgnore
    public boolean getCrud_model_idDirtyFlag(){
        return this.crud_model_idDirtyFlag ;
    }   

    /**
     * 获取 [目标模型]
     */
    @JsonProperty("crud_model_name")
    public String getCrud_model_name(){
        return this.crud_model_name ;
    }

    /**
     * 设置 [目标模型]
     */
    @JsonProperty("crud_model_name")
    public void setCrud_model_name(String  crud_model_name){
        this.crud_model_name = crud_model_name ;
        this.crud_model_nameDirtyFlag = true ;
    }

     /**
     * 获取 [目标模型]脏标记
     */
    @JsonIgnore
    public boolean getCrud_model_nameDirtyFlag(){
        return this.crud_model_nameDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [映射的值]
     */
    @JsonProperty("fields_lines")
    public String getFields_lines(){
        return this.fields_lines ;
    }

    /**
     * 设置 [映射的值]
     */
    @JsonProperty("fields_lines")
    public void setFields_lines(String  fields_lines){
        this.fields_lines = fields_lines ;
        this.fields_linesDirtyFlag = true ;
    }

     /**
     * 获取 [映射的值]脏标记
     */
    @JsonIgnore
    public boolean getFields_linesDirtyFlag(){
        return this.fields_linesDirtyFlag ;
    }   

    /**
     * 获取 [应用于]
     */
    @JsonProperty("filter_domain")
    public String getFilter_domain(){
        return this.filter_domain ;
    }

    /**
     * 设置 [应用于]
     */
    @JsonProperty("filter_domain")
    public void setFilter_domain(String  filter_domain){
        this.filter_domain = filter_domain ;
        this.filter_domainDirtyFlag = true ;
    }

     /**
     * 获取 [应用于]脏标记
     */
    @JsonIgnore
    public boolean getFilter_domainDirtyFlag(){
        return this.filter_domainDirtyFlag ;
    }   

    /**
     * 获取 [更新前域表达式]
     */
    @JsonProperty("filter_pre_domain")
    public String getFilter_pre_domain(){
        return this.filter_pre_domain ;
    }

    /**
     * 设置 [更新前域表达式]
     */
    @JsonProperty("filter_pre_domain")
    public void setFilter_pre_domain(String  filter_pre_domain){
        this.filter_pre_domain = filter_pre_domain ;
        this.filter_pre_domainDirtyFlag = true ;
    }

     /**
     * 获取 [更新前域表达式]脏标记
     */
    @JsonIgnore
    public boolean getFilter_pre_domainDirtyFlag(){
        return this.filter_pre_domainDirtyFlag ;
    }   

    /**
     * 获取 [动作说明]
     */
    @JsonProperty("help")
    public String getHelp(){
        return this.help ;
    }

    /**
     * 设置 [动作说明]
     */
    @JsonProperty("help")
    public void setHelp(String  help){
        this.help = help ;
        this.helpDirtyFlag = true ;
    }

     /**
     * 获取 [动作说明]脏标记
     */
    @JsonIgnore
    public boolean getHelpDirtyFlag(){
        return this.helpDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最后运行]
     */
    @JsonProperty("last_run")
    public Timestamp getLast_run(){
        return this.last_run ;
    }

    /**
     * 设置 [最后运行]
     */
    @JsonProperty("last_run")
    public void setLast_run(Timestamp  last_run){
        this.last_run = last_run ;
        this.last_runDirtyFlag = true ;
    }

     /**
     * 获取 [最后运行]脏标记
     */
    @JsonIgnore
    public boolean getLast_runDirtyFlag(){
        return this.last_runDirtyFlag ;
    }   

    /**
     * 获取 [链接使用字段]
     */
    @JsonProperty("link_field_id")
    public Integer getLink_field_id(){
        return this.link_field_id ;
    }

    /**
     * 设置 [链接使用字段]
     */
    @JsonProperty("link_field_id")
    public void setLink_field_id(Integer  link_field_id){
        this.link_field_id = link_field_id ;
        this.link_field_idDirtyFlag = true ;
    }

     /**
     * 获取 [链接使用字段]脏标记
     */
    @JsonIgnore
    public boolean getLink_field_idDirtyFlag(){
        return this.link_field_idDirtyFlag ;
    }   

    /**
     * 获取 [模型]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return this.model_id ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return this.model_idDirtyFlag ;
    }   

    /**
     * 获取 [模型名称]
     */
    @JsonProperty("model_name")
    public String getModel_name(){
        return this.model_name ;
    }

    /**
     * 设置 [模型名称]
     */
    @JsonProperty("model_name")
    public void setModel_name(String  model_name){
        this.model_name = model_name ;
        this.model_nameDirtyFlag = true ;
    }

     /**
     * 获取 [模型名称]脏标记
     */
    @JsonIgnore
    public boolean getModel_nameDirtyFlag(){
        return this.model_nameDirtyFlag ;
    }   

    /**
     * 获取 [动作名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [动作名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [动作名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [变化字段的触发器]
     */
    @JsonProperty("on_change_fields")
    public String getOn_change_fields(){
        return this.on_change_fields ;
    }

    /**
     * 设置 [变化字段的触发器]
     */
    @JsonProperty("on_change_fields")
    public void setOn_change_fields(String  on_change_fields){
        this.on_change_fields = on_change_fields ;
        this.on_change_fieldsDirtyFlag = true ;
    }

     /**
     * 获取 [变化字段的触发器]脏标记
     */
    @JsonIgnore
    public boolean getOn_change_fieldsDirtyFlag(){
        return this.on_change_fieldsDirtyFlag ;
    }   

    /**
     * 获取 [添加关注者]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [添加关注者]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [添加关注者]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [待办的行动]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [待办的行动]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [待办的行动]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return this.template_id ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return this.template_idDirtyFlag ;
    }   

    /**
     * 获取 [使用日历]
     */
    @JsonProperty("trg_date_calendar_id")
    public Integer getTrg_date_calendar_id(){
        return this.trg_date_calendar_id ;
    }

    /**
     * 设置 [使用日历]
     */
    @JsonProperty("trg_date_calendar_id")
    public void setTrg_date_calendar_id(Integer  trg_date_calendar_id){
        this.trg_date_calendar_id = trg_date_calendar_id ;
        this.trg_date_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [使用日历]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_calendar_idDirtyFlag(){
        return this.trg_date_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [使用日历]
     */
    @JsonProperty("trg_date_calendar_id_text")
    public String getTrg_date_calendar_id_text(){
        return this.trg_date_calendar_id_text ;
    }

    /**
     * 设置 [使用日历]
     */
    @JsonProperty("trg_date_calendar_id_text")
    public void setTrg_date_calendar_id_text(String  trg_date_calendar_id_text){
        this.trg_date_calendar_id_text = trg_date_calendar_id_text ;
        this.trg_date_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [使用日历]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_calendar_id_textDirtyFlag(){
        return this.trg_date_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [触发日期]
     */
    @JsonProperty("trg_date_id")
    public Integer getTrg_date_id(){
        return this.trg_date_id ;
    }

    /**
     * 设置 [触发日期]
     */
    @JsonProperty("trg_date_id")
    public void setTrg_date_id(Integer  trg_date_id){
        this.trg_date_id = trg_date_id ;
        this.trg_date_idDirtyFlag = true ;
    }

     /**
     * 获取 [触发日期]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_idDirtyFlag(){
        return this.trg_date_idDirtyFlag ;
    }   

    /**
     * 获取 [触发日期后的延迟]
     */
    @JsonProperty("trg_date_range")
    public Integer getTrg_date_range(){
        return this.trg_date_range ;
    }

    /**
     * 设置 [触发日期后的延迟]
     */
    @JsonProperty("trg_date_range")
    public void setTrg_date_range(Integer  trg_date_range){
        this.trg_date_range = trg_date_range ;
        this.trg_date_rangeDirtyFlag = true ;
    }

     /**
     * 获取 [触发日期后的延迟]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_rangeDirtyFlag(){
        return this.trg_date_rangeDirtyFlag ;
    }   

    /**
     * 获取 [延迟类型]
     */
    @JsonProperty("trg_date_range_type")
    public String getTrg_date_range_type(){
        return this.trg_date_range_type ;
    }

    /**
     * 设置 [延迟类型]
     */
    @JsonProperty("trg_date_range_type")
    public void setTrg_date_range_type(String  trg_date_range_type){
        this.trg_date_range_type = trg_date_range_type ;
        this.trg_date_range_typeDirtyFlag = true ;
    }

     /**
     * 获取 [延迟类型]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_range_typeDirtyFlag(){
        return this.trg_date_range_typeDirtyFlag ;
    }   

    /**
     * 获取 [触发条件]
     */
    @JsonProperty("trigger")
    public String getTrigger(){
        return this.trigger ;
    }

    /**
     * 设置 [触发条件]
     */
    @JsonProperty("trigger")
    public void setTrigger(String  trigger){
        this.trigger = trigger ;
        this.triggerDirtyFlag = true ;
    }

     /**
     * 获取 [触发条件]脏标记
     */
    @JsonIgnore
    public boolean getTriggerDirtyFlag(){
        return this.triggerDirtyFlag ;
    }   

    /**
     * 获取 [动作类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [动作类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [动作类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [用途]
     */
    @JsonProperty("usage")
    public String getUsage(){
        return this.usage ;
    }

    /**
     * 设置 [用途]
     */
    @JsonProperty("usage")
    public void setUsage(String  usage){
        this.usage = usage ;
        this.usageDirtyFlag = true ;
    }

     /**
     * 获取 [用途]脏标记
     */
    @JsonIgnore
    public boolean getUsageDirtyFlag(){
        return this.usageDirtyFlag ;
    }   

    /**
     * 获取 [网站路径]
     */
    @JsonProperty("website_path")
    public String getWebsite_path(){
        return this.website_path ;
    }

    /**
     * 设置 [网站路径]
     */
    @JsonProperty("website_path")
    public void setWebsite_path(String  website_path){
        this.website_path = website_path ;
        this.website_pathDirtyFlag = true ;
    }

     /**
     * 获取 [网站路径]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_pathDirtyFlag(){
        return this.website_pathDirtyFlag ;
    }   

    /**
     * 获取 [可用于网站]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [可用于网站]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [可用于网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [外部 ID]
     */
    @JsonProperty("xml_id")
    public String getXml_id(){
        return this.xml_id ;
    }

    /**
     * 设置 [外部 ID]
     */
    @JsonProperty("xml_id")
    public void setXml_id(String  xml_id){
        this.xml_id = xml_id ;
        this.xml_idDirtyFlag = true ;
    }

     /**
     * 获取 [外部 ID]脏标记
     */
    @JsonIgnore
    public boolean getXml_idDirtyFlag(){
        return this.xml_idDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

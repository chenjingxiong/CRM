package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_line;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
public interface base_partner_merge_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_lines")
    public base_partner_merge_lineImpl create(@RequestBody base_partner_merge_lineImpl base_partner_merge_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_lines/{id}")
    public base_partner_merge_lineImpl update(@PathVariable("id") Integer id,@RequestBody base_partner_merge_lineImpl base_partner_merge_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_lines/updatebatch")
    public base_partner_merge_lineImpl updateBatch(@RequestBody List<base_partner_merge_lineImpl> base_partner_merge_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_lines/createbatch")
    public base_partner_merge_lineImpl createBatch(@RequestBody List<base_partner_merge_lineImpl> base_partner_merge_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_lines/removebatch")
    public base_partner_merge_lineImpl removeBatch(@RequestBody List<base_partner_merge_lineImpl> base_partner_merge_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_lines/fetchdefault")
    public Page<base_partner_merge_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_lines/{id}")
    public base_partner_merge_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_lines/select")
    public Page<base_partner_merge_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_lines/{id}/getdraft")
    public base_partner_merge_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_partner_merge_lineImpl base_partner_merge_line);



}

package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_company;
import cn.ibizlab.odoo.client.odoo_base.model.res_companyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_company] 服务对象接口
 */
public interface res_companyFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_companies/removebatch")
    public res_companyImpl removeBatch(@RequestBody List<res_companyImpl> res_companies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_companies")
    public res_companyImpl create(@RequestBody res_companyImpl res_company);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_companies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_companies/updatebatch")
    public res_companyImpl updateBatch(@RequestBody List<res_companyImpl> res_companies);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_companies/{id}")
    public res_companyImpl update(@PathVariable("id") Integer id,@RequestBody res_companyImpl res_company);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_companies/fetchdefault")
    public Page<res_companyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_companies/createbatch")
    public res_companyImpl createBatch(@RequestBody List<res_companyImpl> res_companies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_companies/{id}")
    public res_companyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_companies/select")
    public Page<res_companyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_companies/{id}/getdraft")
    public res_companyImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_companyImpl res_company);



}

package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_country;
import cn.ibizlab.odoo.client.odoo_base.model.res_countryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_country] 服务对象接口
 */
public interface res_countryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_countries/{id}")
    public res_countryImpl update(@PathVariable("id") Integer id,@RequestBody res_countryImpl res_country);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_countries/updatebatch")
    public res_countryImpl updateBatch(@RequestBody List<res_countryImpl> res_countries);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_countries/createbatch")
    public res_countryImpl createBatch(@RequestBody List<res_countryImpl> res_countries);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_countries")
    public res_countryImpl create(@RequestBody res_countryImpl res_country);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_countries/removebatch")
    public res_countryImpl removeBatch(@RequestBody List<res_countryImpl> res_countries);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_countries/fetchdefault")
    public Page<res_countryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_countries/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_countries/{id}")
    public res_countryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_countries/select")
    public Page<res_countryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_countries/{id}/getdraft")
    public res_countryImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_countryImpl res_country);



}

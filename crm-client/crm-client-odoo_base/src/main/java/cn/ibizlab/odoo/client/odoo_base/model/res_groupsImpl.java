package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_groups;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_groups] 对象
 */
public class res_groupsImpl implements Ires_groups,Serializable{

    /**
     * 应用
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 备注
     */
    public String comment;

    @JsonIgnore
    public boolean commentDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 群组名称
     */
    public String full_name;

    @JsonIgnore
    public boolean full_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 继承
     */
    public String implied_ids;

    @JsonIgnore
    public boolean implied_idsDirtyFlag;
    
    /**
     * 访问菜单
     */
    public String menu_access;

    @JsonIgnore
    public boolean menu_accessDirtyFlag;
    
    /**
     * 访问控制
     */
    public String model_access;

    @JsonIgnore
    public boolean model_accessDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 规则
     */
    public String rule_groups;

    @JsonIgnore
    public boolean rule_groupsDirtyFlag;
    
    /**
     * 共享用户组
     */
    public String share;

    @JsonIgnore
    public boolean shareDirtyFlag;
    
    /**
     * 及物继承
     */
    public String trans_implied_ids;

    @JsonIgnore
    public boolean trans_implied_idsDirtyFlag;
    
    /**
     * 用户
     */
    public String users;

    @JsonIgnore
    public boolean usersDirtyFlag;
    
    /**
     * 视图
     */
    public String view_access;

    @JsonIgnore
    public boolean view_accessDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [应用]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [应用]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [应用]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [群组名称]
     */
    @JsonProperty("full_name")
    public String getFull_name(){
        return this.full_name ;
    }

    /**
     * 设置 [群组名称]
     */
    @JsonProperty("full_name")
    public void setFull_name(String  full_name){
        this.full_name = full_name ;
        this.full_nameDirtyFlag = true ;
    }

     /**
     * 获取 [群组名称]脏标记
     */
    @JsonIgnore
    public boolean getFull_nameDirtyFlag(){
        return this.full_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [继承]
     */
    @JsonProperty("implied_ids")
    public String getImplied_ids(){
        return this.implied_ids ;
    }

    /**
     * 设置 [继承]
     */
    @JsonProperty("implied_ids")
    public void setImplied_ids(String  implied_ids){
        this.implied_ids = implied_ids ;
        this.implied_idsDirtyFlag = true ;
    }

     /**
     * 获取 [继承]脏标记
     */
    @JsonIgnore
    public boolean getImplied_idsDirtyFlag(){
        return this.implied_idsDirtyFlag ;
    }   

    /**
     * 获取 [访问菜单]
     */
    @JsonProperty("menu_access")
    public String getMenu_access(){
        return this.menu_access ;
    }

    /**
     * 设置 [访问菜单]
     */
    @JsonProperty("menu_access")
    public void setMenu_access(String  menu_access){
        this.menu_access = menu_access ;
        this.menu_accessDirtyFlag = true ;
    }

     /**
     * 获取 [访问菜单]脏标记
     */
    @JsonIgnore
    public boolean getMenu_accessDirtyFlag(){
        return this.menu_accessDirtyFlag ;
    }   

    /**
     * 获取 [访问控制]
     */
    @JsonProperty("model_access")
    public String getModel_access(){
        return this.model_access ;
    }

    /**
     * 设置 [访问控制]
     */
    @JsonProperty("model_access")
    public void setModel_access(String  model_access){
        this.model_access = model_access ;
        this.model_accessDirtyFlag = true ;
    }

     /**
     * 获取 [访问控制]脏标记
     */
    @JsonIgnore
    public boolean getModel_accessDirtyFlag(){
        return this.model_accessDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [规则]
     */
    @JsonProperty("rule_groups")
    public String getRule_groups(){
        return this.rule_groups ;
    }

    /**
     * 设置 [规则]
     */
    @JsonProperty("rule_groups")
    public void setRule_groups(String  rule_groups){
        this.rule_groups = rule_groups ;
        this.rule_groupsDirtyFlag = true ;
    }

     /**
     * 获取 [规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_groupsDirtyFlag(){
        return this.rule_groupsDirtyFlag ;
    }   

    /**
     * 获取 [共享用户组]
     */
    @JsonProperty("share")
    public String getShare(){
        return this.share ;
    }

    /**
     * 设置 [共享用户组]
     */
    @JsonProperty("share")
    public void setShare(String  share){
        this.share = share ;
        this.shareDirtyFlag = true ;
    }

     /**
     * 获取 [共享用户组]脏标记
     */
    @JsonIgnore
    public boolean getShareDirtyFlag(){
        return this.shareDirtyFlag ;
    }   

    /**
     * 获取 [及物继承]
     */
    @JsonProperty("trans_implied_ids")
    public String getTrans_implied_ids(){
        return this.trans_implied_ids ;
    }

    /**
     * 设置 [及物继承]
     */
    @JsonProperty("trans_implied_ids")
    public void setTrans_implied_ids(String  trans_implied_ids){
        this.trans_implied_ids = trans_implied_ids ;
        this.trans_implied_idsDirtyFlag = true ;
    }

     /**
     * 获取 [及物继承]脏标记
     */
    @JsonIgnore
    public boolean getTrans_implied_idsDirtyFlag(){
        return this.trans_implied_idsDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("users")
    public String getUsers(){
        return this.users ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("users")
    public void setUsers(String  users){
        this.users = users ;
        this.usersDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUsersDirtyFlag(){
        return this.usersDirtyFlag ;
    }   

    /**
     * 获取 [视图]
     */
    @JsonProperty("view_access")
    public String getView_access(){
        return this.view_access ;
    }

    /**
     * 设置 [视图]
     */
    @JsonProperty("view_access")
    public void setView_access(String  view_access){
        this.view_access = view_access ;
        this.view_accessDirtyFlag = true ;
    }

     /**
     * 获取 [视图]脏标记
     */
    @JsonIgnore
    public boolean getView_accessDirtyFlag(){
        return this.view_accessDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

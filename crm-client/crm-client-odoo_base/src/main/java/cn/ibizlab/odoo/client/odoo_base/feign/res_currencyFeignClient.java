package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_currency;
import cn.ibizlab.odoo.client.odoo_base.model.res_currencyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_currency] 服务对象接口
 */
public interface res_currencyFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currencies/{id}")
    public res_currencyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currencies/updatebatch")
    public res_currencyImpl updateBatch(@RequestBody List<res_currencyImpl> res_currencies);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currencies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currencies/{id}")
    public res_currencyImpl update(@PathVariable("id") Integer id,@RequestBody res_currencyImpl res_currency);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currencies/fetchdefault")
    public Page<res_currencyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currencies")
    public res_currencyImpl create(@RequestBody res_currencyImpl res_currency);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currencies/removebatch")
    public res_currencyImpl removeBatch(@RequestBody List<res_currencyImpl> res_currencies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currencies/createbatch")
    public res_currencyImpl createBatch(@RequestBody List<res_currencyImpl> res_currencies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currencies/select")
    public Page<res_currencyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currencies/{id}/getdraft")
    public res_currencyImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_currencyImpl res_currency);



}

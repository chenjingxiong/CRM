package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_autocomplete_sync;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partner_autocomplete_syncClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_autocomplete_syncImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partner_autocomplete_syncFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partner_autocomplete_syncClientServiceImpl implements Ires_partner_autocomplete_syncClientService {

    res_partner_autocomplete_syncFeignClient res_partner_autocomplete_syncFeignClient;

    @Autowired
    public res_partner_autocomplete_syncClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_autocomplete_syncFeignClient = nameBuilder.target(res_partner_autocomplete_syncFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_autocomplete_syncFeignClient = nameBuilder.target(res_partner_autocomplete_syncFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner_autocomplete_sync createModel() {
		return new res_partner_autocomplete_syncImpl();
	}


    public void update(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
        Ires_partner_autocomplete_sync clientModel = res_partner_autocomplete_syncFeignClient.update(res_partner_autocomplete_sync.getId(),(res_partner_autocomplete_syncImpl)res_partner_autocomplete_sync) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_autocomplete_sync.getClass(), false);
        copier.copy(clientModel, res_partner_autocomplete_sync, null);
    }


    public void updateBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
        if(res_partner_autocomplete_syncs!=null){
            List<res_partner_autocomplete_syncImpl> list = new ArrayList<res_partner_autocomplete_syncImpl>();
            for(Ires_partner_autocomplete_sync ires_partner_autocomplete_sync :res_partner_autocomplete_syncs){
                list.add((res_partner_autocomplete_syncImpl)ires_partner_autocomplete_sync) ;
            }
            res_partner_autocomplete_syncFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
        Ires_partner_autocomplete_sync clientModel = res_partner_autocomplete_syncFeignClient.get(res_partner_autocomplete_sync.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_autocomplete_sync.getClass(), false);
        copier.copy(clientModel, res_partner_autocomplete_sync, null);
    }


    public void create(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
        Ires_partner_autocomplete_sync clientModel = res_partner_autocomplete_syncFeignClient.create((res_partner_autocomplete_syncImpl)res_partner_autocomplete_sync) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_autocomplete_sync.getClass(), false);
        copier.copy(clientModel, res_partner_autocomplete_sync, null);
    }


    public void createBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
        if(res_partner_autocomplete_syncs!=null){
            List<res_partner_autocomplete_syncImpl> list = new ArrayList<res_partner_autocomplete_syncImpl>();
            for(Ires_partner_autocomplete_sync ires_partner_autocomplete_sync :res_partner_autocomplete_syncs){
                list.add((res_partner_autocomplete_syncImpl)ires_partner_autocomplete_sync) ;
            }
            res_partner_autocomplete_syncFeignClient.createBatch(list) ;
        }
    }


    public Page<Ires_partner_autocomplete_sync> fetchDefault(SearchContext context){
        Page<res_partner_autocomplete_syncImpl> page = this.res_partner_autocomplete_syncFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
        res_partner_autocomplete_syncFeignClient.remove(res_partner_autocomplete_sync.getId()) ;
    }


    public void removeBatch(List<Ires_partner_autocomplete_sync> res_partner_autocomplete_syncs){
        if(res_partner_autocomplete_syncs!=null){
            List<res_partner_autocomplete_syncImpl> list = new ArrayList<res_partner_autocomplete_syncImpl>();
            for(Ires_partner_autocomplete_sync ires_partner_autocomplete_sync :res_partner_autocomplete_syncs){
                list.add((res_partner_autocomplete_syncImpl)ires_partner_autocomplete_sync) ;
            }
            res_partner_autocomplete_syncFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ires_partner_autocomplete_sync> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_partner_autocomplete_sync res_partner_autocomplete_sync){
        Ires_partner_autocomplete_sync clientModel = res_partner_autocomplete_syncFeignClient.getDraft(res_partner_autocomplete_sync.getId(),(res_partner_autocomplete_syncImpl)res_partner_autocomplete_sync) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_autocomplete_sync.getClass(), false);
        copier.copy(clientModel, res_partner_autocomplete_sync, null);
    }



}


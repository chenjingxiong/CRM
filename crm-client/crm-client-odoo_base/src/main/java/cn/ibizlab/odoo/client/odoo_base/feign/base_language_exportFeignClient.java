package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_language_export;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_exportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_language_export] 服务对象接口
 */
public interface base_language_exportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_exports")
    public base_language_exportImpl create(@RequestBody base_language_exportImpl base_language_export);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_exports/removebatch")
    public base_language_exportImpl removeBatch(@RequestBody List<base_language_exportImpl> base_language_exports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_exports/createbatch")
    public base_language_exportImpl createBatch(@RequestBody List<base_language_exportImpl> base_language_exports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_exports/fetchdefault")
    public Page<base_language_exportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_exports/{id}")
    public base_language_exportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_exports/{id}")
    public base_language_exportImpl update(@PathVariable("id") Integer id,@RequestBody base_language_exportImpl base_language_export);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_exports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_exports/updatebatch")
    public base_language_exportImpl updateBatch(@RequestBody List<base_language_exportImpl> base_language_exports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_exports/select")
    public Page<base_language_exportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_exports/{id}/getdraft")
    public base_language_exportImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_language_exportImpl base_language_export);



}

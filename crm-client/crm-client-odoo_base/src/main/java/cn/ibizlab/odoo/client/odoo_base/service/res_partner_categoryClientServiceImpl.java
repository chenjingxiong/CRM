package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_category;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partner_categoryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_categoryImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partner_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partner_categoryClientServiceImpl implements Ires_partner_categoryClientService {

    res_partner_categoryFeignClient res_partner_categoryFeignClient;

    @Autowired
    public res_partner_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_categoryFeignClient = nameBuilder.target(res_partner_categoryFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_categoryFeignClient = nameBuilder.target(res_partner_categoryFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner_category createModel() {
		return new res_partner_categoryImpl();
	}


    public void get(Ires_partner_category res_partner_category){
        Ires_partner_category clientModel = res_partner_categoryFeignClient.get(res_partner_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_category.getClass(), false);
        copier.copy(clientModel, res_partner_category, null);
    }


    public void createBatch(List<Ires_partner_category> res_partner_categories){
        if(res_partner_categories!=null){
            List<res_partner_categoryImpl> list = new ArrayList<res_partner_categoryImpl>();
            for(Ires_partner_category ires_partner_category :res_partner_categories){
                list.add((res_partner_categoryImpl)ires_partner_category) ;
            }
            res_partner_categoryFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ires_partner_category> res_partner_categories){
        if(res_partner_categories!=null){
            List<res_partner_categoryImpl> list = new ArrayList<res_partner_categoryImpl>();
            for(Ires_partner_category ires_partner_category :res_partner_categories){
                list.add((res_partner_categoryImpl)ires_partner_category) ;
            }
            res_partner_categoryFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_partner_category> fetchDefault(SearchContext context){
        Page<res_partner_categoryImpl> page = this.res_partner_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ires_partner_category res_partner_category){
        Ires_partner_category clientModel = res_partner_categoryFeignClient.update(res_partner_category.getId(),(res_partner_categoryImpl)res_partner_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_category.getClass(), false);
        copier.copy(clientModel, res_partner_category, null);
    }


    public void removeBatch(List<Ires_partner_category> res_partner_categories){
        if(res_partner_categories!=null){
            List<res_partner_categoryImpl> list = new ArrayList<res_partner_categoryImpl>();
            for(Ires_partner_category ires_partner_category :res_partner_categories){
                list.add((res_partner_categoryImpl)ires_partner_category) ;
            }
            res_partner_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ires_partner_category res_partner_category){
        Ires_partner_category clientModel = res_partner_categoryFeignClient.create((res_partner_categoryImpl)res_partner_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_category.getClass(), false);
        copier.copy(clientModel, res_partner_category, null);
    }


    public void remove(Ires_partner_category res_partner_category){
        res_partner_categoryFeignClient.remove(res_partner_category.getId()) ;
    }


    public Page<Ires_partner_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_partner_category res_partner_category){
        Ires_partner_category clientModel = res_partner_categoryFeignClient.getDraft(res_partner_category.getId(),(res_partner_categoryImpl)res_partner_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_category.getClass(), false);
        copier.copy(clientModel, res_partner_category, null);
    }



}


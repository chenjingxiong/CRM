package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_users_log;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_users_logClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_users_logImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_users_logFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_users_log] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_users_logClientServiceImpl implements Ires_users_logClientService {

    res_users_logFeignClient res_users_logFeignClient;

    @Autowired
    public res_users_logClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_users_logFeignClient = nameBuilder.target(res_users_logFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_users_logFeignClient = nameBuilder.target(res_users_logFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_users_log createModel() {
		return new res_users_logImpl();
	}


    public void removeBatch(List<Ires_users_log> res_users_logs){
        if(res_users_logs!=null){
            List<res_users_logImpl> list = new ArrayList<res_users_logImpl>();
            for(Ires_users_log ires_users_log :res_users_logs){
                list.add((res_users_logImpl)ires_users_log) ;
            }
            res_users_logFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_users_log res_users_log){
        res_users_logFeignClient.remove(res_users_log.getId()) ;
    }


    public void create(Ires_users_log res_users_log){
        Ires_users_log clientModel = res_users_logFeignClient.create((res_users_logImpl)res_users_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users_log.getClass(), false);
        copier.copy(clientModel, res_users_log, null);
    }


    public Page<Ires_users_log> fetchDefault(SearchContext context){
        Page<res_users_logImpl> page = this.res_users_logFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ires_users_log res_users_log){
        Ires_users_log clientModel = res_users_logFeignClient.update(res_users_log.getId(),(res_users_logImpl)res_users_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users_log.getClass(), false);
        copier.copy(clientModel, res_users_log, null);
    }


    public void get(Ires_users_log res_users_log){
        Ires_users_log clientModel = res_users_logFeignClient.get(res_users_log.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users_log.getClass(), false);
        copier.copy(clientModel, res_users_log, null);
    }


    public void updateBatch(List<Ires_users_log> res_users_logs){
        if(res_users_logs!=null){
            List<res_users_logImpl> list = new ArrayList<res_users_logImpl>();
            for(Ires_users_log ires_users_log :res_users_logs){
                list.add((res_users_logImpl)ires_users_log) ;
            }
            res_users_logFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ires_users_log> res_users_logs){
        if(res_users_logs!=null){
            List<res_users_logImpl> list = new ArrayList<res_users_logImpl>();
            for(Ires_users_log ires_users_log :res_users_logs){
                list.add((res_users_logImpl)ires_users_log) ;
            }
            res_users_logFeignClient.createBatch(list) ;
        }
    }


    public Page<Ires_users_log> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_users_log res_users_log){
        Ires_users_log clientModel = res_users_logFeignClient.getDraft(res_users_log.getId(),(res_users_logImpl)res_users_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_users_log.getClass(), false);
        copier.copy(clientModel, res_users_log, null);
    }



}


package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country_group;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_country_groupClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_groupImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_country_groupFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_country_group] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_country_groupClientServiceImpl implements Ires_country_groupClientService {

    res_country_groupFeignClient res_country_groupFeignClient;

    @Autowired
    public res_country_groupClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_country_groupFeignClient = nameBuilder.target(res_country_groupFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_country_groupFeignClient = nameBuilder.target(res_country_groupFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_country_group createModel() {
		return new res_country_groupImpl();
	}


    public Page<Ires_country_group> fetchDefault(SearchContext context){
        Page<res_country_groupImpl> page = this.res_country_groupFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ires_country_group res_country_group){
        Ires_country_group clientModel = res_country_groupFeignClient.get(res_country_group.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_group.getClass(), false);
        copier.copy(clientModel, res_country_group, null);
    }


    public void update(Ires_country_group res_country_group){
        Ires_country_group clientModel = res_country_groupFeignClient.update(res_country_group.getId(),(res_country_groupImpl)res_country_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_group.getClass(), false);
        copier.copy(clientModel, res_country_group, null);
    }


    public void removeBatch(List<Ires_country_group> res_country_groups){
        if(res_country_groups!=null){
            List<res_country_groupImpl> list = new ArrayList<res_country_groupImpl>();
            for(Ires_country_group ires_country_group :res_country_groups){
                list.add((res_country_groupImpl)ires_country_group) ;
            }
            res_country_groupFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ires_country_group> res_country_groups){
        if(res_country_groups!=null){
            List<res_country_groupImpl> list = new ArrayList<res_country_groupImpl>();
            for(Ires_country_group ires_country_group :res_country_groups){
                list.add((res_country_groupImpl)ires_country_group) ;
            }
            res_country_groupFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ires_country_group res_country_group){
        res_country_groupFeignClient.remove(res_country_group.getId()) ;
    }


    public void updateBatch(List<Ires_country_group> res_country_groups){
        if(res_country_groups!=null){
            List<res_country_groupImpl> list = new ArrayList<res_country_groupImpl>();
            for(Ires_country_group ires_country_group :res_country_groups){
                list.add((res_country_groupImpl)ires_country_group) ;
            }
            res_country_groupFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ires_country_group res_country_group){
        Ires_country_group clientModel = res_country_groupFeignClient.create((res_country_groupImpl)res_country_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_group.getClass(), false);
        copier.copy(clientModel, res_country_group, null);
    }


    public Page<Ires_country_group> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_country_group res_country_group){
        Ires_country_group clientModel = res_country_groupFeignClient.getDraft(res_country_group.getId(),(res_country_groupImpl)res_country_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country_group.getClass(), false);
        copier.copy(clientModel, res_country_group, null);
    }



}


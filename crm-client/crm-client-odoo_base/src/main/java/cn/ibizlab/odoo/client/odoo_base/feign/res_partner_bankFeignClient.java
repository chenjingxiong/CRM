package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner_bank;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_bankImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner_bank] 服务对象接口
 */
public interface res_partner_bankFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_banks/{id}")
    public res_partner_bankImpl update(@PathVariable("id") Integer id,@RequestBody res_partner_bankImpl res_partner_bank);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_banks/removebatch")
    public res_partner_bankImpl removeBatch(@RequestBody List<res_partner_bankImpl> res_partner_banks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_banks/{id}")
    public res_partner_bankImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_banks/updatebatch")
    public res_partner_bankImpl updateBatch(@RequestBody List<res_partner_bankImpl> res_partner_banks);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_banks/fetchdefault")
    public Page<res_partner_bankImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_banks/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_banks/createbatch")
    public res_partner_bankImpl createBatch(@RequestBody List<res_partner_bankImpl> res_partner_banks);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_banks")
    public res_partner_bankImpl create(@RequestBody res_partner_bankImpl res_partner_bank);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_banks/select")
    public Page<res_partner_bankImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_banks/{id}/getdraft")
    public res_partner_bankImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partner_bankImpl res_partner_bank);



}

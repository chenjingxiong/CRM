package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner_industry;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_industryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner_industry] 服务对象接口
 */
public interface res_partner_industryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_industries")
    public res_partner_industryImpl create(@RequestBody res_partner_industryImpl res_partner_industry);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_industries/updatebatch")
    public res_partner_industryImpl updateBatch(@RequestBody List<res_partner_industryImpl> res_partner_industries);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_industries/{id}")
    public res_partner_industryImpl update(@PathVariable("id") Integer id,@RequestBody res_partner_industryImpl res_partner_industry);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_industries/createbatch")
    public res_partner_industryImpl createBatch(@RequestBody List<res_partner_industryImpl> res_partner_industries);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_industries/removebatch")
    public res_partner_industryImpl removeBatch(@RequestBody List<res_partner_industryImpl> res_partner_industries);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_industries/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_industries/fetchdefault")
    public Page<res_partner_industryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_industries/{id}")
    public res_partner_industryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_industries/select")
    public Page<res_partner_industryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_industries/{id}/getdraft")
    public res_partner_industryImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partner_industryImpl res_partner_industry);



}

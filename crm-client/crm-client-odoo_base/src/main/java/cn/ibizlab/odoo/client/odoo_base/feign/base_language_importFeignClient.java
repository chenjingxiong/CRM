package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_language_import;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_importImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_language_import] 服务对象接口
 */
public interface base_language_importFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_imports/removebatch")
    public base_language_importImpl removeBatch(@RequestBody List<base_language_importImpl> base_language_imports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_imports")
    public base_language_importImpl create(@RequestBody base_language_importImpl base_language_import);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_imports/{id}")
    public base_language_importImpl update(@PathVariable("id") Integer id,@RequestBody base_language_importImpl base_language_import);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_imports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_imports/{id}")
    public base_language_importImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_imports/createbatch")
    public base_language_importImpl createBatch(@RequestBody List<base_language_importImpl> base_language_imports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_imports/updatebatch")
    public base_language_importImpl updateBatch(@RequestBody List<base_language_importImpl> base_language_imports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_imports/fetchdefault")
    public Page<base_language_importImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_imports/select")
    public Page<base_language_importImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_imports/{id}/getdraft")
    public base_language_importImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_language_importImpl base_language_import);



}

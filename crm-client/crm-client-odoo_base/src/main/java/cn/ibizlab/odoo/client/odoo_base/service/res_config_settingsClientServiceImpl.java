package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config_settings;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_config_settingsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_settingsImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_config_settingsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_config_settings] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_config_settingsClientServiceImpl implements Ires_config_settingsClientService {

    res_config_settingsFeignClient res_config_settingsFeignClient;

    @Autowired
    public res_config_settingsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_config_settingsFeignClient = nameBuilder.target(res_config_settingsFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_config_settingsFeignClient = nameBuilder.target(res_config_settingsFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_config_settings createModel() {
		return new res_config_settingsImpl();
	}


    public void createBatch(List<Ires_config_settings> res_config_settings){
        if(res_config_settings!=null){
            List<res_config_settingsImpl> list = new ArrayList<res_config_settingsImpl>();
            for(Ires_config_settings ires_config_settings :res_config_settings){
                list.add((res_config_settingsImpl)ires_config_settings) ;
            }
            res_config_settingsFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ires_config_settings> res_config_settings){
        if(res_config_settings!=null){
            List<res_config_settingsImpl> list = new ArrayList<res_config_settingsImpl>();
            for(Ires_config_settings ires_config_settings :res_config_settings){
                list.add((res_config_settingsImpl)ires_config_settings) ;
            }
            res_config_settingsFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ires_config_settings> fetchDefault(SearchContext context){
        Page<res_config_settingsImpl> page = this.res_config_settingsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ires_config_settings> res_config_settings){
        if(res_config_settings!=null){
            List<res_config_settingsImpl> list = new ArrayList<res_config_settingsImpl>();
            for(Ires_config_settings ires_config_settings :res_config_settings){
                list.add((res_config_settingsImpl)ires_config_settings) ;
            }
            res_config_settingsFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ires_config_settings res_config_settings){
        Ires_config_settings clientModel = res_config_settingsFeignClient.update(res_config_settings.getId(),(res_config_settingsImpl)res_config_settings) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_settings.getClass(), false);
        copier.copy(clientModel, res_config_settings, null);
    }


    public void remove(Ires_config_settings res_config_settings){
        res_config_settingsFeignClient.remove(res_config_settings.getId()) ;
    }


    public void create(Ires_config_settings res_config_settings){
        Ires_config_settings clientModel = res_config_settingsFeignClient.create((res_config_settingsImpl)res_config_settings) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_settings.getClass(), false);
        copier.copy(clientModel, res_config_settings, null);
    }


    public void get(Ires_config_settings res_config_settings){
        Ires_config_settings clientModel = res_config_settingsFeignClient.get(res_config_settings.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_settings.getClass(), false);
        copier.copy(clientModel, res_config_settings, null);
    }


    public Page<Ires_config_settings> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_config_settings res_config_settings){
        Ires_config_settings clientModel = res_config_settingsFeignClient.getDraft(res_config_settings.getId(),(res_config_settingsImpl)res_config_settings) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config_settings.getClass(), false);
        copier.copy(clientModel, res_config_settings, null);
    }



}


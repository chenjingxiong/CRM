package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_groups;
import cn.ibizlab.odoo.client.odoo_base.model.res_groupsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_groups] 服务对象接口
 */
public interface res_groupsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_groups/removebatch")
    public res_groupsImpl removeBatch(@RequestBody List<res_groupsImpl> res_groups);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_groups/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_groups")
    public res_groupsImpl create(@RequestBody res_groupsImpl res_groups);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_groups/{id}")
    public res_groupsImpl update(@PathVariable("id") Integer id,@RequestBody res_groupsImpl res_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_groups/createbatch")
    public res_groupsImpl createBatch(@RequestBody List<res_groupsImpl> res_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_groups/{id}")
    public res_groupsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_groups/updatebatch")
    public res_groupsImpl updateBatch(@RequestBody List<res_groupsImpl> res_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_groups/fetchdefault")
    public Page<res_groupsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_groups/select")
    public Page<res_groupsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_groups/{id}/getdraft")
    public res_groupsImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_groupsImpl res_groups);



}

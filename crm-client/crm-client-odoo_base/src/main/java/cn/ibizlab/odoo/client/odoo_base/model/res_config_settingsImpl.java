package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_config_settings;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_config_settings] 对象
 */
public class res_config_settingsImpl implements Ires_config_settings,Serializable{

    /**
     * 银行核销阈值
     */
    public Timestamp account_bank_reconciliation_start;

    @JsonIgnore
    public boolean account_bank_reconciliation_startDirtyFlag;
    
    /**
     * 别名域
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 允许在登录页开启密码重置功能
     */
    public String auth_signup_reset_password;

    @JsonIgnore
    public boolean auth_signup_reset_passwordDirtyFlag;
    
    /**
     * 用作通过注册创建的新用户的模版
     */
    public Integer auth_signup_template_user_id;

    @JsonIgnore
    public boolean auth_signup_template_user_idDirtyFlag;
    
    /**
     * 用作通过注册创建的新用户的模版
     */
    public String auth_signup_template_user_id_text;

    @JsonIgnore
    public boolean auth_signup_template_user_id_textDirtyFlag;
    
    /**
     * 顾客账号
     */
    public String auth_signup_uninvited;

    @JsonIgnore
    public boolean auth_signup_uninvitedDirtyFlag;
    
    /**
     * 自动开票
     */
    public String automatic_invoice;

    @JsonIgnore
    public boolean automatic_invoiceDirtyFlag;
    
    /**
     * 锁定
     */
    public String auto_done_setting;

    @JsonIgnore
    public boolean auto_done_settingDirtyFlag;
    
    /**
     * 可用阈值
     */
    public Double available_threshold;

    @JsonIgnore
    public boolean available_thresholdDirtyFlag;
    
    /**
     * 放弃时长
     */
    public Double cart_abandoned_delay;

    @JsonIgnore
    public boolean cart_abandoned_delayDirtyFlag;
    
    /**
     * 购物车恢复EMail
     */
    public Integer cart_recovery_mail_template;

    @JsonIgnore
    public boolean cart_recovery_mail_templateDirtyFlag;
    
    /**
     * 内容发布网络 (CDN)
     */
    public String cdn_activated;

    @JsonIgnore
    public boolean cdn_activatedDirtyFlag;
    
    /**
     * CDN筛选
     */
    public String cdn_filters;

    @JsonIgnore
    public boolean cdn_filtersDirtyFlag;
    
    /**
     * CDN基本网址
     */
    public String cdn_url;

    @JsonIgnore
    public boolean cdn_urlDirtyFlag;
    
    /**
     * 网站直播频道
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 模板
     */
    public Integer chart_template_id;

    @JsonIgnore
    public boolean chart_template_idDirtyFlag;
    
    /**
     * 模板
     */
    public String chart_template_id_text;

    @JsonIgnore
    public boolean chart_template_id_textDirtyFlag;
    
    /**
     * 公司货币
     */
    public Integer company_currency_id;

    @JsonIgnore
    public boolean company_currency_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 多公司间所有公司的公共用户
     */
    public String company_share_partner;

    @JsonIgnore
    public boolean company_share_partnerDirtyFlag;
    
    /**
     * 分享产品 给所有公司
     */
    public String company_share_product;

    @JsonIgnore
    public boolean company_share_productDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 默认线索别名
     */
    public String crm_alias_prefix;

    @JsonIgnore
    public boolean crm_alias_prefixDirtyFlag;
    
    /**
     * 默认销售团队
     */
    public Integer crm_default_team_id;

    @JsonIgnore
    public boolean crm_default_team_idDirtyFlag;
    
    /**
     * 默认销售人员
     */
    public Integer crm_default_user_id;

    @JsonIgnore
    public boolean crm_default_user_idDirtyFlag;
    
    /**
     * 汇兑损益
     */
    public Integer currency_exchange_journal_id;

    @JsonIgnore
    public boolean currency_exchange_journal_idDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 开票策略
     */
    public String default_invoice_policy;

    @JsonIgnore
    public boolean default_invoice_policyDirtyFlag;
    
    /**
     * 拣货策略
     */
    public String default_picking_policy;

    @JsonIgnore
    public boolean default_picking_policyDirtyFlag;
    
    /**
     * 账单控制
     */
    public String default_purchase_method;

    @JsonIgnore
    public boolean default_purchase_methodDirtyFlag;
    
    /**
     * 默认模板
     */
    public Integer default_sale_order_template_id;

    @JsonIgnore
    public boolean default_sale_order_template_idDirtyFlag;
    
    /**
     * 默认模板
     */
    public String default_sale_order_template_id_text;

    @JsonIgnore
    public boolean default_sale_order_template_id_textDirtyFlag;
    
    /**
     * 押金产品
     */
    public Integer deposit_default_product_id;

    @JsonIgnore
    public boolean deposit_default_product_idDirtyFlag;
    
    /**
     * 押金产品
     */
    public String deposit_default_product_id_text;

    @JsonIgnore
    public boolean deposit_default_product_id_textDirtyFlag;
    
    /**
     * 摘要邮件
     */
    public String digest_emails;

    @JsonIgnore
    public boolean digest_emailsDirtyFlag;
    
    /**
     * 摘要邮件
     */
    public Integer digest_id;

    @JsonIgnore
    public boolean digest_idDirtyFlag;
    
    /**
     * 摘要邮件
     */
    public String digest_id_text;

    @JsonIgnore
    public boolean digest_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 默认的费用别名
     */
    public String expense_alias_prefix;

    @JsonIgnore
    public boolean expense_alias_prefixDirtyFlag;
    
    /**
     * 外部邮件服务器
     */
    public String external_email_server_default;

    @JsonIgnore
    public boolean external_email_server_defaultDirtyFlag;
    
    /**
     * 文档模板
     */
    public Integer external_report_layout_id;

    @JsonIgnore
    public boolean external_report_layout_idDirtyFlag;
    
    /**
     * 失败的邮件
     */
    public Integer fail_counter;

    @JsonIgnore
    public boolean fail_counterDirtyFlag;
    
    /**
     * 图标
     */
    public byte[] favicon;

    @JsonIgnore
    public boolean faviconDirtyFlag;
    
    /**
     * 手动分配EMail
     */
    public String generate_lead_from_alias;

    @JsonIgnore
    public boolean generate_lead_from_aliasDirtyFlag;
    
    /**
     * 谷歌分析密钥
     */
    public String google_analytics_key;

    @JsonIgnore
    public boolean google_analytics_keyDirtyFlag;
    
    /**
     * Google 客户 ID
     */
    public String google_management_client_id;

    @JsonIgnore
    public boolean google_management_client_idDirtyFlag;
    
    /**
     * Google 客户端密钥
     */
    public String google_management_client_secret;

    @JsonIgnore
    public boolean google_management_client_secretDirtyFlag;
    
    /**
     * Google 地图 API 密钥
     */
    public String google_maps_api_key;

    @JsonIgnore
    public boolean google_maps_api_keyDirtyFlag;
    
    /**
     * 分析会计
     */
    public String group_analytic_accounting;

    @JsonIgnore
    public boolean group_analytic_accountingDirtyFlag;
    
    /**
     * 分析标签
     */
    public String group_analytic_tags;

    @JsonIgnore
    public boolean group_analytic_tagsDirtyFlag;
    
    /**
     * 员工 PIN
     */
    public String group_attendance_use_pin;

    @JsonIgnore
    public boolean group_attendance_use_pinDirtyFlag;
    
    /**
     * 现金舍入
     */
    public String group_cash_rounding;

    @JsonIgnore
    public boolean group_cash_roundingDirtyFlag;
    
    /**
     * 送货地址
     */
    public String group_delivery_invoice_address;

    @JsonIgnore
    public boolean group_delivery_invoice_addressDirtyFlag;
    
    /**
     * 折扣
     */
    public String group_discount_per_so_line;

    @JsonIgnore
    public boolean group_discount_per_so_lineDirtyFlag;
    
    /**
     * 贸易条款
     */
    public String group_display_incoterm;

    @JsonIgnore
    public boolean group_display_incotermDirtyFlag;
    
    /**
     * 财年
     */
    public String group_fiscal_year;

    @JsonIgnore
    public boolean group_fiscal_yearDirtyFlag;
    
    /**
     * 显示批次 / 序列号
     */
    public String group_lot_on_delivery_slip;

    @JsonIgnore
    public boolean group_lot_on_delivery_slipDirtyFlag;
    
    /**
     * 供应商价格表
     */
    public String group_manage_vendor_price;

    @JsonIgnore
    public boolean group_manage_vendor_priceDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public String group_mass_mailing_campaign;

    @JsonIgnore
    public boolean group_mass_mailing_campaignDirtyFlag;
    
    /**
     * MRP 工单
     */
    public String group_mrp_routings;

    @JsonIgnore
    public boolean group_mrp_routingsDirtyFlag;
    
    /**
     * 管理多公司
     */
    public String group_multi_company;

    @JsonIgnore
    public boolean group_multi_companyDirtyFlag;
    
    /**
     * 多币种
     */
    public String group_multi_currency;

    @JsonIgnore
    public boolean group_multi_currencyDirtyFlag;
    
    /**
     * 多网站
     */
    public String group_multi_website;

    @JsonIgnore
    public boolean group_multi_websiteDirtyFlag;
    
    /**
     * 给客户显示价目表
     */
    public String group_pricelist_item;

    @JsonIgnore
    public boolean group_pricelist_itemDirtyFlag;
    
    /**
     * 使用供应商单据里的产品
     */
    public String group_products_in_bills;

    @JsonIgnore
    public boolean group_products_in_billsDirtyFlag;
    
    /**
     * 显示产品的价目表
     */
    public String group_product_pricelist;

    @JsonIgnore
    public boolean group_product_pricelistDirtyFlag;
    
    /**
     * 变体和选项
     */
    public String group_product_variant;

    @JsonIgnore
    public boolean group_product_variantDirtyFlag;
    
    /**
     * 形式发票
     */
    public String group_proforma_sales;

    @JsonIgnore
    public boolean group_proforma_salesDirtyFlag;
    
    /**
     * 使用项目评级
     */
    public String group_project_rating;

    @JsonIgnore
    public boolean group_project_ratingDirtyFlag;
    
    /**
     * 订单特定路线
     */
    public String group_route_so_lines;

    @JsonIgnore
    public boolean group_route_so_linesDirtyFlag;
    
    /**
     * 客户地址
     */
    public String group_sale_delivery_address;

    @JsonIgnore
    public boolean group_sale_delivery_addressDirtyFlag;
    
    /**
     * 交货日期
     */
    public String group_sale_order_dates;

    @JsonIgnore
    public boolean group_sale_order_datesDirtyFlag;
    
    /**
     * 报价单模板
     */
    public String group_sale_order_template;

    @JsonIgnore
    public boolean group_sale_order_templateDirtyFlag;
    
    /**
     * 为每个客户使用价格表来适配您的价格
     */
    public String group_sale_pricelist;

    @JsonIgnore
    public boolean group_sale_pricelistDirtyFlag;
    
    /**
     * 明细行汇总含税(B2B).
     */
    public String group_show_line_subtotals_tax_excluded;

    @JsonIgnore
    public boolean group_show_line_subtotals_tax_excludedDirtyFlag;
    
    /**
     * 显示含税明细行在汇总表(B2B).
     */
    public String group_show_line_subtotals_tax_included;

    @JsonIgnore
    public boolean group_show_line_subtotals_tax_includedDirtyFlag;
    
    /**
     * 多步路由
     */
    public String group_stock_adv_location;

    @JsonIgnore
    public boolean group_stock_adv_locationDirtyFlag;
    
    /**
     * 储存位置
     */
    public String group_stock_multi_locations;

    @JsonIgnore
    public boolean group_stock_multi_locationsDirtyFlag;
    
    /**
     * 多仓库
     */
    public String group_stock_multi_warehouses;

    @JsonIgnore
    public boolean group_stock_multi_warehousesDirtyFlag;
    
    /**
     * 产品包装
     */
    public String group_stock_packaging;

    @JsonIgnore
    public boolean group_stock_packagingDirtyFlag;
    
    /**
     * 批次和序列号
     */
    public String group_stock_production_lot;

    @JsonIgnore
    public boolean group_stock_production_lotDirtyFlag;
    
    /**
     * 交货包裹
     */
    public String group_stock_tracking_lot;

    @JsonIgnore
    public boolean group_stock_tracking_lotDirtyFlag;
    
    /**
     * 寄售
     */
    public String group_stock_tracking_owner;

    @JsonIgnore
    public boolean group_stock_tracking_ownerDirtyFlag;
    
    /**
     * 子任务
     */
    public String group_subtask_project;

    @JsonIgnore
    public boolean group_subtask_projectDirtyFlag;
    
    /**
     * 计量单位
     */
    public String group_uom;

    @JsonIgnore
    public boolean group_uomDirtyFlag;
    
    /**
     * 线索
     */
    public String group_use_lead;

    @JsonIgnore
    public boolean group_use_leadDirtyFlag;
    
    /**
     * 发票警告
     */
    public String group_warning_account;

    @JsonIgnore
    public boolean group_warning_accountDirtyFlag;
    
    /**
     * 采购警告
     */
    public String group_warning_purchase;

    @JsonIgnore
    public boolean group_warning_purchaseDirtyFlag;
    
    /**
     * 销售订单警告
     */
    public String group_warning_sale;

    @JsonIgnore
    public boolean group_warning_saleDirtyFlag;
    
    /**
     * 库存警报
     */
    public String group_warning_stock;

    @JsonIgnore
    public boolean group_warning_stockDirtyFlag;
    
    /**
     * 网站弹出窗口
     */
    public String group_website_popup_on_exit;

    @JsonIgnore
    public boolean group_website_popup_on_exitDirtyFlag;
    
    /**
     * 有会计分录
     */
    public String has_accounting_entries;

    @JsonIgnore
    public boolean has_accounting_entriesDirtyFlag;
    
    /**
     * 公司有科目表
     */
    public String has_chart_of_accounts;

    @JsonIgnore
    public boolean has_chart_of_accountsDirtyFlag;
    
    /**
     * Google 分析
     */
    public String has_google_analytics;

    @JsonIgnore
    public boolean has_google_analyticsDirtyFlag;
    
    /**
     * 谷歌分析仪表板
     */
    public String has_google_analytics_dashboard;

    @JsonIgnore
    public boolean has_google_analytics_dashboardDirtyFlag;
    
    /**
     * Google 地图
     */
    public String has_google_maps;

    @JsonIgnore
    public boolean has_google_mapsDirtyFlag;
    
    /**
     * 设置社交平台
     */
    public String has_social_network;

    @JsonIgnore
    public boolean has_social_networkDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 库存可用性
     */
    public String inventory_availability;

    @JsonIgnore
    public boolean inventory_availabilityDirtyFlag;
    
    /**
     * 发送EMail
     */
    public String invoice_is_email;

    @JsonIgnore
    public boolean invoice_is_emailDirtyFlag;
    
    /**
     * 打印
     */
    public String invoice_is_print;

    @JsonIgnore
    public boolean invoice_is_printDirtyFlag;
    
    /**
     * 透过邮递
     */
    public String invoice_is_snailmail;

    @JsonIgnore
    public boolean invoice_is_snailmailDirtyFlag;
    
    /**
     * 交流
     */
    public String invoice_reference_type;

    @JsonIgnore
    public boolean invoice_reference_typeDirtyFlag;
    
    /**
     * 销售模块是否已安装
     */
    public String is_installed_sale;

    @JsonIgnore
    public boolean is_installed_saleDirtyFlag;
    
    /**
     * 语言数量
     */
    public Integer language_count;

    @JsonIgnore
    public boolean language_countDirtyFlag;
    
    /**
     * 语言
     */
    public String language_ids;

    @JsonIgnore
    public boolean language_idsDirtyFlag;
    
    /**
     * 锁定确认订单
     */
    public String lock_confirmed_po;

    @JsonIgnore
    public boolean lock_confirmed_poDirtyFlag;
    
    /**
     * 制造提前期(日)
     */
    public Double manufacturing_lead;

    @JsonIgnore
    public boolean manufacturing_leadDirtyFlag;
    
    /**
     * 邮件服务器
     */
    public Integer mass_mailing_mail_server_id;

    @JsonIgnore
    public boolean mass_mailing_mail_server_idDirtyFlag;
    
    /**
     * 指定邮件服务器
     */
    public String mass_mailing_outgoing_mail_server;

    @JsonIgnore
    public boolean mass_mailing_outgoing_mail_serverDirtyFlag;
    
    /**
     * 开票
     */
    public String module_account;

    @JsonIgnore
    public boolean module_accountDirtyFlag;
    
    /**
     * 三方匹配:采购，收货和发票
     */
    public String module_account_3way_match;

    @JsonIgnore
    public boolean module_account_3way_matchDirtyFlag;
    
    /**
     * Accounting
     */
    public String module_account_accountant;

    @JsonIgnore
    public boolean module_account_accountantDirtyFlag;
    
    /**
     * 资产管理
     */
    public String module_account_asset;

    @JsonIgnore
    public boolean module_account_assetDirtyFlag;
    
    /**
     * 用 CAMT.053 格式导入
     */
    public String module_account_bank_statement_import_camt;

    @JsonIgnore
    public boolean module_account_bank_statement_import_camtDirtyFlag;
    
    /**
     * 以 CSV 格式导入
     */
    public String module_account_bank_statement_import_csv;

    @JsonIgnore
    public boolean module_account_bank_statement_import_csvDirtyFlag;
    
    /**
     * 导入.ofx格式
     */
    public String module_account_bank_statement_import_ofx;

    @JsonIgnore
    public boolean module_account_bank_statement_import_ofxDirtyFlag;
    
    /**
     * 导入.qif 文件
     */
    public String module_account_bank_statement_import_qif;

    @JsonIgnore
    public boolean module_account_bank_statement_import_qifDirtyFlag;
    
    /**
     * 使用批量付款
     */
    public String module_account_batch_payment;

    @JsonIgnore
    public boolean module_account_batch_paymentDirtyFlag;
    
    /**
     * 预算管理
     */
    public String module_account_budget;

    @JsonIgnore
    public boolean module_account_budgetDirtyFlag;
    
    /**
     * 允许支票打印和存款
     */
    public String module_account_check_printing;

    @JsonIgnore
    public boolean module_account_check_printingDirtyFlag;
    
    /**
     * 收入识别
     */
    public String module_account_deferred_revenue;

    @JsonIgnore
    public boolean module_account_deferred_revenueDirtyFlag;
    
    /**
     * 国际贸易统计组织
     */
    public String module_account_intrastat;

    @JsonIgnore
    public boolean module_account_intrastatDirtyFlag;
    
    /**
     * 自动票据处理
     */
    public String module_account_invoice_extract;

    @JsonIgnore
    public boolean module_account_invoice_extractDirtyFlag;
    
    /**
     * 发票在线付款
     */
    public String module_account_payment;

    @JsonIgnore
    public boolean module_account_paymentDirtyFlag;
    
    /**
     * Plaid 接口
     */
    public String module_account_plaid;

    @JsonIgnore
    public boolean module_account_plaidDirtyFlag;
    
    /**
     * 动态报告
     */
    public String module_account_reports;

    @JsonIgnore
    public boolean module_account_reportsDirtyFlag;
    
    /**
     * 催款等级
     */
    public String module_account_reports_followup;

    @JsonIgnore
    public boolean module_account_reports_followupDirtyFlag;
    
    /**
     * SEPA贷记交易
     */
    public String module_account_sepa;

    @JsonIgnore
    public boolean module_account_sepaDirtyFlag;
    
    /**
     * 使用SEPA直接计入借方
     */
    public String module_account_sepa_direct_debit;

    @JsonIgnore
    public boolean module_account_sepa_direct_debitDirtyFlag;
    
    /**
     * 科目税
     */
    public String module_account_taxcloud;

    @JsonIgnore
    public boolean module_account_taxcloudDirtyFlag;
    
    /**
     * 银行接口－自动同步银行费用
     */
    public String module_account_yodlee;

    @JsonIgnore
    public boolean module_account_yodleeDirtyFlag;
    
    /**
     * LDAP认证
     */
    public String module_auth_ldap;

    @JsonIgnore
    public boolean module_auth_ldapDirtyFlag;
    
    /**
     * 使用外部验证提供者 (OAuth)
     */
    public String module_auth_oauth;

    @JsonIgnore
    public boolean module_auth_oauthDirtyFlag;
    
    /**
     * 用Gengo翻译您的网站
     */
    public String module_base_gengo;

    @JsonIgnore
    public boolean module_base_gengoDirtyFlag;
    
    /**
     * 允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据
     */
    public String module_base_import;

    @JsonIgnore
    public boolean module_base_importDirtyFlag;
    
    /**
     * 号码格式
     */
    public String module_crm_phone_validation;

    @JsonIgnore
    public boolean module_crm_phone_validationDirtyFlag;
    
    /**
     * 从你的网站流量创建线索/商机
     */
    public String module_crm_reveal;

    @JsonIgnore
    public boolean module_crm_revealDirtyFlag;
    
    /**
     * 自动汇率
     */
    public String module_currency_rate_live;

    @JsonIgnore
    public boolean module_currency_rate_liveDirtyFlag;
    
    /**
     * 运输成本
     */
    public String module_delivery;

    @JsonIgnore
    public boolean module_deliveryDirtyFlag;
    
    /**
     * bpost 接口
     */
    public String module_delivery_bpost;

    @JsonIgnore
    public boolean module_delivery_bpostDirtyFlag;
    
    /**
     * DHL 接口
     */
    public String module_delivery_dhl;

    @JsonIgnore
    public boolean module_delivery_dhlDirtyFlag;
    
    /**
     * Easypost 接口
     */
    public String module_delivery_easypost;

    @JsonIgnore
    public boolean module_delivery_easypostDirtyFlag;
    
    /**
     * FedEx 接口
     */
    public String module_delivery_fedex;

    @JsonIgnore
    public boolean module_delivery_fedexDirtyFlag;
    
    /**
     * UPS 接口
     */
    public String module_delivery_ups;

    @JsonIgnore
    public boolean module_delivery_upsDirtyFlag;
    
    /**
     * USPS 接口
     */
    public String module_delivery_usps;

    @JsonIgnore
    public boolean module_delivery_uspsDirtyFlag;
    
    /**
     * 条码
     */
    public String module_event_barcode;

    @JsonIgnore
    public boolean module_event_barcodeDirtyFlag;
    
    /**
     * 入场券
     */
    public String module_event_sale;

    @JsonIgnore
    public boolean module_event_saleDirtyFlag;
    
    /**
     * 允许用户同步Google日历
     */
    public String module_google_calendar;

    @JsonIgnore
    public boolean module_google_calendarDirtyFlag;
    
    /**
     * 附加Google文档到记录
     */
    public String module_google_drive;

    @JsonIgnore
    public boolean module_google_driveDirtyFlag;
    
    /**
     * Google 电子表格
     */
    public String module_google_spreadsheet;

    @JsonIgnore
    public boolean module_google_spreadsheetDirtyFlag;
    
    /**
     * 显示组织架构图
     */
    public String module_hr_org_chart;

    @JsonIgnore
    public boolean module_hr_org_chartDirtyFlag;
    
    /**
     * 面试表单
     */
    public String module_hr_recruitment_survey;

    @JsonIgnore
    public boolean module_hr_recruitment_surveyDirtyFlag;
    
    /**
     * 任务日志
     */
    public String module_hr_timesheet;

    @JsonIgnore
    public boolean module_hr_timesheetDirtyFlag;
    
    /**
     * 管理公司间交易
     */
    public String module_inter_company_rules;

    @JsonIgnore
    public boolean module_inter_company_rulesDirtyFlag;
    
    /**
     * 欧盟数字商品增值税
     */
    public String module_l10n_eu_service;

    @JsonIgnore
    public boolean module_l10n_eu_serviceDirtyFlag;
    
    /**
     * 副产品
     */
    public String module_mrp_byproduct;

    @JsonIgnore
    public boolean module_mrp_byproductDirtyFlag;
    
    /**
     * 主生产排程
     */
    public String module_mrp_mps;

    @JsonIgnore
    public boolean module_mrp_mpsDirtyFlag;
    
    /**
     * 产品生命周期管理 (PLM)
     */
    public String module_mrp_plm;

    @JsonIgnore
    public boolean module_mrp_plmDirtyFlag;
    
    /**
     * 工单
     */
    public String module_mrp_workorder;

    @JsonIgnore
    public boolean module_mrp_workorderDirtyFlag;
    
    /**
     * 协作pad
     */
    public String module_pad;

    @JsonIgnore
    public boolean module_padDirtyFlag;
    
    /**
     * 自动填充公司数据
     */
    public String module_partner_autocomplete;

    @JsonIgnore
    public boolean module_partner_autocompleteDirtyFlag;
    
    /**
     * 集成卡支付
     */
    public String module_pos_mercury;

    @JsonIgnore
    public boolean module_pos_mercuryDirtyFlag;
    
    /**
     * 保留
     */
    public String module_procurement_jit;

    @JsonIgnore
    public boolean module_procurement_jitDirtyFlag;
    
    /**
     * 特定的EMail
     */
    public String module_product_email_template;

    @JsonIgnore
    public boolean module_product_email_templateDirtyFlag;
    
    /**
     * 到期日
     */
    public String module_product_expiry;

    @JsonIgnore
    public boolean module_product_expiryDirtyFlag;
    
    /**
     * 允许产品毛利
     */
    public String module_product_margin;

    @JsonIgnore
    public boolean module_product_marginDirtyFlag;
    
    /**
     * 预测
     */
    public String module_project_forecast;

    @JsonIgnore
    public boolean module_project_forecastDirtyFlag;
    
    /**
     * 采购招标
     */
    public String module_purchase_requisition;

    @JsonIgnore
    public boolean module_purchase_requisitionDirtyFlag;
    
    /**
     * 质量
     */
    public String module_quality_control;

    @JsonIgnore
    public boolean module_quality_controlDirtyFlag;
    
    /**
     * 优惠券和促销
     */
    public String module_sale_coupon;

    @JsonIgnore
    public boolean module_sale_couponDirtyFlag;
    
    /**
     * 毛利
     */
    public String module_sale_margin;

    @JsonIgnore
    public boolean module_sale_marginDirtyFlag;
    
    /**
     * 报价单生成器
     */
    public String module_sale_quotation_builder;

    @JsonIgnore
    public boolean module_sale_quotation_builderDirtyFlag;
    
    /**
     * 条码扫描器
     */
    public String module_stock_barcode;

    @JsonIgnore
    public boolean module_stock_barcodeDirtyFlag;
    
    /**
     * 代发货
     */
    public String module_stock_dropshipping;

    @JsonIgnore
    public boolean module_stock_dropshippingDirtyFlag;
    
    /**
     * 到岸成本
     */
    public String module_stock_landed_costs;

    @JsonIgnore
    public boolean module_stock_landed_costsDirtyFlag;
    
    /**
     * 批量拣货
     */
    public String module_stock_picking_batch;

    @JsonIgnore
    public boolean module_stock_picking_batchDirtyFlag;
    
    /**
     * Asterisk (开源VoIP平台)
     */
    public String module_voip;

    @JsonIgnore
    public boolean module_voipDirtyFlag;
    
    /**
     * 调查登记
     */
    public String module_website_event_questions;

    @JsonIgnore
    public boolean module_website_event_questionsDirtyFlag;
    
    /**
     * 在线票务
     */
    public String module_website_event_sale;

    @JsonIgnore
    public boolean module_website_event_saleDirtyFlag;
    
    /**
     * 耿宗并计划
     */
    public String module_website_event_track;

    @JsonIgnore
    public boolean module_website_event_trackDirtyFlag;
    
    /**
     * 在线发布
     */
    public String module_website_hr_recruitment;

    @JsonIgnore
    public boolean module_website_hr_recruitmentDirtyFlag;
    
    /**
     * 链接跟踪器
     */
    public String module_website_links;

    @JsonIgnore
    public boolean module_website_linksDirtyFlag;
    
    /**
     * 产品比较工具
     */
    public String module_website_sale_comparison;

    @JsonIgnore
    public boolean module_website_sale_comparisonDirtyFlag;
    
    /**
     * 电商物流成本
     */
    public String module_website_sale_delivery;

    @JsonIgnore
    public boolean module_website_sale_deliveryDirtyFlag;
    
    /**
     * 数字内容
     */
    public String module_website_sale_digital;

    @JsonIgnore
    public boolean module_website_sale_digitalDirtyFlag;
    
    /**
     * 库存
     */
    public String module_website_sale_stock;

    @JsonIgnore
    public boolean module_website_sale_stockDirtyFlag;
    
    /**
     * 心愿单
     */
    public String module_website_sale_wishlist;

    @JsonIgnore
    public boolean module_website_sale_wishlistDirtyFlag;
    
    /**
     * A / B测试
     */
    public String module_website_version;

    @JsonIgnore
    public boolean module_website_versionDirtyFlag;
    
    /**
     * Unsplash图像库
     */
    public String module_web_unsplash;

    @JsonIgnore
    public boolean module_web_unsplashDirtyFlag;
    
    /**
     * 每个产品的多种销售价格
     */
    public String multi_sales_price;

    @JsonIgnore
    public boolean multi_sales_priceDirtyFlag;
    
    /**
     * 计价方法
     */
    public String multi_sales_price_method;

    @JsonIgnore
    public boolean multi_sales_price_methodDirtyFlag;
    
    /**
     * 纸张格式
     */
    public Integer paperformat_id;

    @JsonIgnore
    public boolean paperformat_idDirtyFlag;
    
    /**
     * 信用不足
     */
    public String partner_autocomplete_insufficient_credit;

    @JsonIgnore
    public boolean partner_autocomplete_insufficient_creditDirtyFlag;
    
    /**
     * 在线支付
     */
    public String portal_confirmation_pay;

    @JsonIgnore
    public boolean portal_confirmation_payDirtyFlag;
    
    /**
     * 在线签名
     */
    public String portal_confirmation_sign;

    @JsonIgnore
    public boolean portal_confirmation_signDirtyFlag;
    
    /**
     * POS价格表
     */
    public String pos_pricelist_setting;

    @JsonIgnore
    public boolean pos_pricelist_settingDirtyFlag;
    
    /**
     * 产品复价
     */
    public String pos_sales_price;

    @JsonIgnore
    public boolean pos_sales_priceDirtyFlag;
    
    /**
     * 审批层级 *
     */
    public String po_double_validation;

    @JsonIgnore
    public boolean po_double_validationDirtyFlag;
    
    /**
     * 最小金额
     */
    public Double po_double_validation_amount;

    @JsonIgnore
    public boolean po_double_validation_amountDirtyFlag;
    
    /**
     * 采购提前时间
     */
    public Double po_lead;

    @JsonIgnore
    public boolean po_leadDirtyFlag;
    
    /**
     * 采购订单修改 *
     */
    public String po_lock;

    @JsonIgnore
    public boolean po_lockDirtyFlag;
    
    /**
     * 采购订单批准
     */
    public String po_order_approval;

    @JsonIgnore
    public boolean po_order_approvalDirtyFlag;
    
    /**
     * 重量单位
     */
    public String product_weight_in_lbs;

    @JsonIgnore
    public boolean product_weight_in_lbsDirtyFlag;
    
    /**
     * 传播的最小差值
     */
    public Integer propagation_minimum_delta;

    @JsonIgnore
    public boolean propagation_minimum_deltaDirtyFlag;
    
    /**
     * 默认进项税
     */
    public Integer purchase_tax_id;

    @JsonIgnore
    public boolean purchase_tax_idDirtyFlag;
    
    /**
     * 显示SEPA QR码
     */
    public String qr_code;

    @JsonIgnore
    public boolean qr_codeDirtyFlag;
    
    /**
     * 默认报价有效期（日）
     */
    public Integer quotation_validity_days;

    @JsonIgnore
    public boolean quotation_validity_daysDirtyFlag;
    
    /**
     * 自定义报表页脚
     */
    public String report_footer;

    @JsonIgnore
    public boolean report_footerDirtyFlag;
    
    /**
     * 公司的上班时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer salesperson_id;

    @JsonIgnore
    public boolean salesperson_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer salesteam_id;

    @JsonIgnore
    public boolean salesteam_idDirtyFlag;
    
    /**
     * 送货管理
     */
    public String sale_delivery_settings;

    @JsonIgnore
    public boolean sale_delivery_settingsDirtyFlag;
    
    /**
     * 条款及条件
     */
    public String sale_note;

    @JsonIgnore
    public boolean sale_noteDirtyFlag;
    
    /**
     * 价格表
     */
    public String sale_pricelist_setting;

    @JsonIgnore
    public boolean sale_pricelist_settingDirtyFlag;
    
    /**
     * 默认销售税
     */
    public Integer sale_tax_id;

    @JsonIgnore
    public boolean sale_tax_idDirtyFlag;
    
    /**
     * 安全时间
     */
    public Double security_lead;

    @JsonIgnore
    public boolean security_leadDirtyFlag;
    
    /**
     * 在取消订阅页面上显示黑名单按钮
     */
    public String show_blacklist_buttons;

    @JsonIgnore
    public boolean show_blacklist_buttonsDirtyFlag;
    
    /**
     * 显示效果
     */
    public String show_effect;

    @JsonIgnore
    public boolean show_effectDirtyFlag;
    
    /**
     * 税目汇总表
     */
    public String show_line_subtotals_tax_selection;

    @JsonIgnore
    public boolean show_line_subtotals_tax_selectionDirtyFlag;
    
    /**
     * 彩色打印
     */
    public String snailmail_color;

    @JsonIgnore
    public boolean snailmail_colorDirtyFlag;
    
    /**
     * 双面打印
     */
    public String snailmail_duplex;

    @JsonIgnore
    public boolean snailmail_duplexDirtyFlag;
    
    /**
     * 默认社交分享图片
     */
    public byte[] social_default_image;

    @JsonIgnore
    public boolean social_default_imageDirtyFlag;
    
    /**
     * 脸书账号
     */
    public String social_facebook;

    @JsonIgnore
    public boolean social_facebookDirtyFlag;
    
    /**
     * GitHub账户
     */
    public String social_github;

    @JsonIgnore
    public boolean social_githubDirtyFlag;
    
    /**
     * Google+账户
     */
    public String social_googleplus;

    @JsonIgnore
    public boolean social_googleplusDirtyFlag;
    
    /**
     * Instagram 账号
     */
    public String social_instagram;

    @JsonIgnore
    public boolean social_instagramDirtyFlag;
    
    /**
     * 领英账号
     */
    public String social_linkedin;

    @JsonIgnore
    public boolean social_linkedinDirtyFlag;
    
    /**
     * Twitter账号
     */
    public String social_twitter;

    @JsonIgnore
    public boolean social_twitterDirtyFlag;
    
    /**
     * Youtube账号
     */
    public String social_youtube;

    @JsonIgnore
    public boolean social_youtubeDirtyFlag;
    
    /**
     * 具体用户账号
     */
    public String specific_user_account;

    @JsonIgnore
    public boolean specific_user_accountDirtyFlag;
    
    /**
     * 税率计算的舍入方法
     */
    public String tax_calculation_rounding_method;

    @JsonIgnore
    public boolean tax_calculation_rounding_methodDirtyFlag;
    
    /**
     * 税率现金收付制日记账
     */
    public Integer tax_cash_basis_journal_id;

    @JsonIgnore
    public boolean tax_cash_basis_journal_idDirtyFlag;
    
    /**
     * 现金收付制
     */
    public String tax_exigibility;

    @JsonIgnore
    public boolean tax_exigibilityDirtyFlag;
    
    /**
     * EMail模板
     */
    public Integer template_id;

    @JsonIgnore
    public boolean template_idDirtyFlag;
    
    /**
     * EMail模板
     */
    public String template_id_text;

    @JsonIgnore
    public boolean template_id_textDirtyFlag;
    
    /**
     * 访问秘钥
     */
    public String unsplash_access_key;

    @JsonIgnore
    public boolean unsplash_access_keyDirtyFlag;
    
    /**
     * 默认访问权限
     */
    public String user_default_rights;

    @JsonIgnore
    public boolean user_default_rightsDirtyFlag;
    
    /**
     * 允许员工通过EMail记录费用
     */
    public String use_mailgateway;

    @JsonIgnore
    public boolean use_mailgatewayDirtyFlag;
    
    /**
     * 默认制造提前期
     */
    public String use_manufacturing_lead;

    @JsonIgnore
    public boolean use_manufacturing_leadDirtyFlag;
    
    /**
     * 安全交货时间
     */
    public String use_po_lead;

    @JsonIgnore
    public boolean use_po_leadDirtyFlag;
    
    /**
     * 无重新排程传播
     */
    public String use_propagation_minimum_delta;

    @JsonIgnore
    public boolean use_propagation_minimum_deltaDirtyFlag;
    
    /**
     * 默认报价有效期
     */
    public String use_quotation_validity_days;

    @JsonIgnore
    public boolean use_quotation_validity_daysDirtyFlag;
    
    /**
     * 默认条款和条件
     */
    public String use_sale_note;

    @JsonIgnore
    public boolean use_sale_noteDirtyFlag;
    
    /**
     * 销售的安全提前期
     */
    public String use_security_lead;

    @JsonIgnore
    public boolean use_security_leadDirtyFlag;
    
    /**
     * 网站公司
     */
    public Integer website_company_id;

    @JsonIgnore
    public boolean website_company_idDirtyFlag;
    
    /**
     * 国家/地区分组
     */
    public String website_country_group_ids;

    @JsonIgnore
    public boolean website_country_group_idsDirtyFlag;
    
    /**
     * 默认语言代码
     */
    public String website_default_lang_code;

    @JsonIgnore
    public boolean website_default_lang_codeDirtyFlag;
    
    /**
     * 默认语言
     */
    public Integer website_default_lang_id;

    @JsonIgnore
    public boolean website_default_lang_idDirtyFlag;
    
    /**
     * 网站域名
     */
    public String website_domain;

    @JsonIgnore
    public boolean website_domainDirtyFlag;
    
    /**
     * 联系表单上的技术数据
     */
    public String website_form_enable_metadata;

    @JsonIgnore
    public boolean website_form_enable_metadataDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站名称
     */
    public String website_name;

    @JsonIgnore
    public boolean website_nameDirtyFlag;
    
    /**
     * 谷歌文档密钥
     */
    public String website_slide_google_app_key;

    @JsonIgnore
    public boolean website_slide_google_app_keyDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [银行核销阈值]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public Timestamp getAccount_bank_reconciliation_start(){
        return this.account_bank_reconciliation_start ;
    }

    /**
     * 设置 [银行核销阈值]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public void setAccount_bank_reconciliation_start(Timestamp  account_bank_reconciliation_start){
        this.account_bank_reconciliation_start = account_bank_reconciliation_start ;
        this.account_bank_reconciliation_startDirtyFlag = true ;
    }

     /**
     * 获取 [银行核销阈值]脏标记
     */
    @JsonIgnore
    public boolean getAccount_bank_reconciliation_startDirtyFlag(){
        return this.account_bank_reconciliation_startDirtyFlag ;
    }   

    /**
     * 获取 [别名域]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [别名域]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [别名域]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [允许在登录页开启密码重置功能]
     */
    @JsonProperty("auth_signup_reset_password")
    public String getAuth_signup_reset_password(){
        return this.auth_signup_reset_password ;
    }

    /**
     * 设置 [允许在登录页开启密码重置功能]
     */
    @JsonProperty("auth_signup_reset_password")
    public void setAuth_signup_reset_password(String  auth_signup_reset_password){
        this.auth_signup_reset_password = auth_signup_reset_password ;
        this.auth_signup_reset_passwordDirtyFlag = true ;
    }

     /**
     * 获取 [允许在登录页开启密码重置功能]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_reset_passwordDirtyFlag(){
        return this.auth_signup_reset_passwordDirtyFlag ;
    }   

    /**
     * 获取 [用作通过注册创建的新用户的模版]
     */
    @JsonProperty("auth_signup_template_user_id")
    public Integer getAuth_signup_template_user_id(){
        return this.auth_signup_template_user_id ;
    }

    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    @JsonProperty("auth_signup_template_user_id")
    public void setAuth_signup_template_user_id(Integer  auth_signup_template_user_id){
        this.auth_signup_template_user_id = auth_signup_template_user_id ;
        this.auth_signup_template_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_template_user_idDirtyFlag(){
        return this.auth_signup_template_user_idDirtyFlag ;
    }   

    /**
     * 获取 [用作通过注册创建的新用户的模版]
     */
    @JsonProperty("auth_signup_template_user_id_text")
    public String getAuth_signup_template_user_id_text(){
        return this.auth_signup_template_user_id_text ;
    }

    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    @JsonProperty("auth_signup_template_user_id_text")
    public void setAuth_signup_template_user_id_text(String  auth_signup_template_user_id_text){
        this.auth_signup_template_user_id_text = auth_signup_template_user_id_text ;
        this.auth_signup_template_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_template_user_id_textDirtyFlag(){
        return this.auth_signup_template_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [顾客账号]
     */
    @JsonProperty("auth_signup_uninvited")
    public String getAuth_signup_uninvited(){
        return this.auth_signup_uninvited ;
    }

    /**
     * 设置 [顾客账号]
     */
    @JsonProperty("auth_signup_uninvited")
    public void setAuth_signup_uninvited(String  auth_signup_uninvited){
        this.auth_signup_uninvited = auth_signup_uninvited ;
        this.auth_signup_uninvitedDirtyFlag = true ;
    }

     /**
     * 获取 [顾客账号]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_uninvitedDirtyFlag(){
        return this.auth_signup_uninvitedDirtyFlag ;
    }   

    /**
     * 获取 [自动开票]
     */
    @JsonProperty("automatic_invoice")
    public String getAutomatic_invoice(){
        return this.automatic_invoice ;
    }

    /**
     * 设置 [自动开票]
     */
    @JsonProperty("automatic_invoice")
    public void setAutomatic_invoice(String  automatic_invoice){
        this.automatic_invoice = automatic_invoice ;
        this.automatic_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [自动开票]脏标记
     */
    @JsonIgnore
    public boolean getAutomatic_invoiceDirtyFlag(){
        return this.automatic_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [锁定]
     */
    @JsonProperty("auto_done_setting")
    public String getAuto_done_setting(){
        return this.auto_done_setting ;
    }

    /**
     * 设置 [锁定]
     */
    @JsonProperty("auto_done_setting")
    public void setAuto_done_setting(String  auto_done_setting){
        this.auto_done_setting = auto_done_setting ;
        this.auto_done_settingDirtyFlag = true ;
    }

     /**
     * 获取 [锁定]脏标记
     */
    @JsonIgnore
    public boolean getAuto_done_settingDirtyFlag(){
        return this.auto_done_settingDirtyFlag ;
    }   

    /**
     * 获取 [可用阈值]
     */
    @JsonProperty("available_threshold")
    public Double getAvailable_threshold(){
        return this.available_threshold ;
    }

    /**
     * 设置 [可用阈值]
     */
    @JsonProperty("available_threshold")
    public void setAvailable_threshold(Double  available_threshold){
        this.available_threshold = available_threshold ;
        this.available_thresholdDirtyFlag = true ;
    }

     /**
     * 获取 [可用阈值]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_thresholdDirtyFlag(){
        return this.available_thresholdDirtyFlag ;
    }   

    /**
     * 获取 [放弃时长]
     */
    @JsonProperty("cart_abandoned_delay")
    public Double getCart_abandoned_delay(){
        return this.cart_abandoned_delay ;
    }

    /**
     * 设置 [放弃时长]
     */
    @JsonProperty("cart_abandoned_delay")
    public void setCart_abandoned_delay(Double  cart_abandoned_delay){
        this.cart_abandoned_delay = cart_abandoned_delay ;
        this.cart_abandoned_delayDirtyFlag = true ;
    }

     /**
     * 获取 [放弃时长]脏标记
     */
    @JsonIgnore
    public boolean getCart_abandoned_delayDirtyFlag(){
        return this.cart_abandoned_delayDirtyFlag ;
    }   

    /**
     * 获取 [购物车恢复EMail]
     */
    @JsonProperty("cart_recovery_mail_template")
    public Integer getCart_recovery_mail_template(){
        return this.cart_recovery_mail_template ;
    }

    /**
     * 设置 [购物车恢复EMail]
     */
    @JsonProperty("cart_recovery_mail_template")
    public void setCart_recovery_mail_template(Integer  cart_recovery_mail_template){
        this.cart_recovery_mail_template = cart_recovery_mail_template ;
        this.cart_recovery_mail_templateDirtyFlag = true ;
    }

     /**
     * 获取 [购物车恢复EMail]脏标记
     */
    @JsonIgnore
    public boolean getCart_recovery_mail_templateDirtyFlag(){
        return this.cart_recovery_mail_templateDirtyFlag ;
    }   

    /**
     * 获取 [内容发布网络 (CDN)]
     */
    @JsonProperty("cdn_activated")
    public String getCdn_activated(){
        return this.cdn_activated ;
    }

    /**
     * 设置 [内容发布网络 (CDN)]
     */
    @JsonProperty("cdn_activated")
    public void setCdn_activated(String  cdn_activated){
        this.cdn_activated = cdn_activated ;
        this.cdn_activatedDirtyFlag = true ;
    }

     /**
     * 获取 [内容发布网络 (CDN)]脏标记
     */
    @JsonIgnore
    public boolean getCdn_activatedDirtyFlag(){
        return this.cdn_activatedDirtyFlag ;
    }   

    /**
     * 获取 [CDN筛选]
     */
    @JsonProperty("cdn_filters")
    public String getCdn_filters(){
        return this.cdn_filters ;
    }

    /**
     * 设置 [CDN筛选]
     */
    @JsonProperty("cdn_filters")
    public void setCdn_filters(String  cdn_filters){
        this.cdn_filters = cdn_filters ;
        this.cdn_filtersDirtyFlag = true ;
    }

     /**
     * 获取 [CDN筛选]脏标记
     */
    @JsonIgnore
    public boolean getCdn_filtersDirtyFlag(){
        return this.cdn_filtersDirtyFlag ;
    }   

    /**
     * 获取 [CDN基本网址]
     */
    @JsonProperty("cdn_url")
    public String getCdn_url(){
        return this.cdn_url ;
    }

    /**
     * 设置 [CDN基本网址]
     */
    @JsonProperty("cdn_url")
    public void setCdn_url(String  cdn_url){
        this.cdn_url = cdn_url ;
        this.cdn_urlDirtyFlag = true ;
    }

     /**
     * 获取 [CDN基本网址]脏标记
     */
    @JsonIgnore
    public boolean getCdn_urlDirtyFlag(){
        return this.cdn_urlDirtyFlag ;
    }   

    /**
     * 获取 [网站直播频道]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [网站直播频道]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站直播频道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [模板]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return this.chart_template_id ;
    }

    /**
     * 设置 [模板]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return this.chart_template_idDirtyFlag ;
    }   

    /**
     * 获取 [模板]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return this.chart_template_id_text ;
    }

    /**
     * 设置 [模板]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return this.chart_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [多公司间所有公司的公共用户]
     */
    @JsonProperty("company_share_partner")
    public String getCompany_share_partner(){
        return this.company_share_partner ;
    }

    /**
     * 设置 [多公司间所有公司的公共用户]
     */
    @JsonProperty("company_share_partner")
    public void setCompany_share_partner(String  company_share_partner){
        this.company_share_partner = company_share_partner ;
        this.company_share_partnerDirtyFlag = true ;
    }

     /**
     * 获取 [多公司间所有公司的公共用户]脏标记
     */
    @JsonIgnore
    public boolean getCompany_share_partnerDirtyFlag(){
        return this.company_share_partnerDirtyFlag ;
    }   

    /**
     * 获取 [分享产品 给所有公司]
     */
    @JsonProperty("company_share_product")
    public String getCompany_share_product(){
        return this.company_share_product ;
    }

    /**
     * 设置 [分享产品 给所有公司]
     */
    @JsonProperty("company_share_product")
    public void setCompany_share_product(String  company_share_product){
        this.company_share_product = company_share_product ;
        this.company_share_productDirtyFlag = true ;
    }

     /**
     * 获取 [分享产品 给所有公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_share_productDirtyFlag(){
        return this.company_share_productDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [默认线索别名]
     */
    @JsonProperty("crm_alias_prefix")
    public String getCrm_alias_prefix(){
        return this.crm_alias_prefix ;
    }

    /**
     * 设置 [默认线索别名]
     */
    @JsonProperty("crm_alias_prefix")
    public void setCrm_alias_prefix(String  crm_alias_prefix){
        this.crm_alias_prefix = crm_alias_prefix ;
        this.crm_alias_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [默认线索别名]脏标记
     */
    @JsonIgnore
    public boolean getCrm_alias_prefixDirtyFlag(){
        return this.crm_alias_prefixDirtyFlag ;
    }   

    /**
     * 获取 [默认销售团队]
     */
    @JsonProperty("crm_default_team_id")
    public Integer getCrm_default_team_id(){
        return this.crm_default_team_id ;
    }

    /**
     * 设置 [默认销售团队]
     */
    @JsonProperty("crm_default_team_id")
    public void setCrm_default_team_id(Integer  crm_default_team_id){
        this.crm_default_team_id = crm_default_team_id ;
        this.crm_default_team_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认销售团队]脏标记
     */
    @JsonIgnore
    public boolean getCrm_default_team_idDirtyFlag(){
        return this.crm_default_team_idDirtyFlag ;
    }   

    /**
     * 获取 [默认销售人员]
     */
    @JsonProperty("crm_default_user_id")
    public Integer getCrm_default_user_id(){
        return this.crm_default_user_id ;
    }

    /**
     * 设置 [默认销售人员]
     */
    @JsonProperty("crm_default_user_id")
    public void setCrm_default_user_id(Integer  crm_default_user_id){
        this.crm_default_user_id = crm_default_user_id ;
        this.crm_default_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认销售人员]脏标记
     */
    @JsonIgnore
    public boolean getCrm_default_user_idDirtyFlag(){
        return this.crm_default_user_idDirtyFlag ;
    }   

    /**
     * 获取 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id")
    public Integer getCurrency_exchange_journal_id(){
        return this.currency_exchange_journal_id ;
    }

    /**
     * 设置 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id")
    public void setCurrency_exchange_journal_id(Integer  currency_exchange_journal_id){
        this.currency_exchange_journal_id = currency_exchange_journal_id ;
        this.currency_exchange_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇兑损益]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_idDirtyFlag(){
        return this.currency_exchange_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [开票策略]
     */
    @JsonProperty("default_invoice_policy")
    public String getDefault_invoice_policy(){
        return this.default_invoice_policy ;
    }

    /**
     * 设置 [开票策略]
     */
    @JsonProperty("default_invoice_policy")
    public void setDefault_invoice_policy(String  default_invoice_policy){
        this.default_invoice_policy = default_invoice_policy ;
        this.default_invoice_policyDirtyFlag = true ;
    }

     /**
     * 获取 [开票策略]脏标记
     */
    @JsonIgnore
    public boolean getDefault_invoice_policyDirtyFlag(){
        return this.default_invoice_policyDirtyFlag ;
    }   

    /**
     * 获取 [拣货策略]
     */
    @JsonProperty("default_picking_policy")
    public String getDefault_picking_policy(){
        return this.default_picking_policy ;
    }

    /**
     * 设置 [拣货策略]
     */
    @JsonProperty("default_picking_policy")
    public void setDefault_picking_policy(String  default_picking_policy){
        this.default_picking_policy = default_picking_policy ;
        this.default_picking_policyDirtyFlag = true ;
    }

     /**
     * 获取 [拣货策略]脏标记
     */
    @JsonIgnore
    public boolean getDefault_picking_policyDirtyFlag(){
        return this.default_picking_policyDirtyFlag ;
    }   

    /**
     * 获取 [账单控制]
     */
    @JsonProperty("default_purchase_method")
    public String getDefault_purchase_method(){
        return this.default_purchase_method ;
    }

    /**
     * 设置 [账单控制]
     */
    @JsonProperty("default_purchase_method")
    public void setDefault_purchase_method(String  default_purchase_method){
        this.default_purchase_method = default_purchase_method ;
        this.default_purchase_methodDirtyFlag = true ;
    }

     /**
     * 获取 [账单控制]脏标记
     */
    @JsonIgnore
    public boolean getDefault_purchase_methodDirtyFlag(){
        return this.default_purchase_methodDirtyFlag ;
    }   

    /**
     * 获取 [默认模板]
     */
    @JsonProperty("default_sale_order_template_id")
    public Integer getDefault_sale_order_template_id(){
        return this.default_sale_order_template_id ;
    }

    /**
     * 设置 [默认模板]
     */
    @JsonProperty("default_sale_order_template_id")
    public void setDefault_sale_order_template_id(Integer  default_sale_order_template_id){
        this.default_sale_order_template_id = default_sale_order_template_id ;
        this.default_sale_order_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认模板]脏标记
     */
    @JsonIgnore
    public boolean getDefault_sale_order_template_idDirtyFlag(){
        return this.default_sale_order_template_idDirtyFlag ;
    }   

    /**
     * 获取 [默认模板]
     */
    @JsonProperty("default_sale_order_template_id_text")
    public String getDefault_sale_order_template_id_text(){
        return this.default_sale_order_template_id_text ;
    }

    /**
     * 设置 [默认模板]
     */
    @JsonProperty("default_sale_order_template_id_text")
    public void setDefault_sale_order_template_id_text(String  default_sale_order_template_id_text){
        this.default_sale_order_template_id_text = default_sale_order_template_id_text ;
        this.default_sale_order_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认模板]脏标记
     */
    @JsonIgnore
    public boolean getDefault_sale_order_template_id_textDirtyFlag(){
        return this.default_sale_order_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [押金产品]
     */
    @JsonProperty("deposit_default_product_id")
    public Integer getDeposit_default_product_id(){
        return this.deposit_default_product_id ;
    }

    /**
     * 设置 [押金产品]
     */
    @JsonProperty("deposit_default_product_id")
    public void setDeposit_default_product_id(Integer  deposit_default_product_id){
        this.deposit_default_product_id = deposit_default_product_id ;
        this.deposit_default_product_idDirtyFlag = true ;
    }

     /**
     * 获取 [押金产品]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_default_product_idDirtyFlag(){
        return this.deposit_default_product_idDirtyFlag ;
    }   

    /**
     * 获取 [押金产品]
     */
    @JsonProperty("deposit_default_product_id_text")
    public String getDeposit_default_product_id_text(){
        return this.deposit_default_product_id_text ;
    }

    /**
     * 设置 [押金产品]
     */
    @JsonProperty("deposit_default_product_id_text")
    public void setDeposit_default_product_id_text(String  deposit_default_product_id_text){
        this.deposit_default_product_id_text = deposit_default_product_id_text ;
        this.deposit_default_product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [押金产品]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_default_product_id_textDirtyFlag(){
        return this.deposit_default_product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [摘要邮件]
     */
    @JsonProperty("digest_emails")
    public String getDigest_emails(){
        return this.digest_emails ;
    }

    /**
     * 设置 [摘要邮件]
     */
    @JsonProperty("digest_emails")
    public void setDigest_emails(String  digest_emails){
        this.digest_emails = digest_emails ;
        this.digest_emailsDirtyFlag = true ;
    }

     /**
     * 获取 [摘要邮件]脏标记
     */
    @JsonIgnore
    public boolean getDigest_emailsDirtyFlag(){
        return this.digest_emailsDirtyFlag ;
    }   

    /**
     * 获取 [摘要邮件]
     */
    @JsonProperty("digest_id")
    public Integer getDigest_id(){
        return this.digest_id ;
    }

    /**
     * 设置 [摘要邮件]
     */
    @JsonProperty("digest_id")
    public void setDigest_id(Integer  digest_id){
        this.digest_id = digest_id ;
        this.digest_idDirtyFlag = true ;
    }

     /**
     * 获取 [摘要邮件]脏标记
     */
    @JsonIgnore
    public boolean getDigest_idDirtyFlag(){
        return this.digest_idDirtyFlag ;
    }   

    /**
     * 获取 [摘要邮件]
     */
    @JsonProperty("digest_id_text")
    public String getDigest_id_text(){
        return this.digest_id_text ;
    }

    /**
     * 设置 [摘要邮件]
     */
    @JsonProperty("digest_id_text")
    public void setDigest_id_text(String  digest_id_text){
        this.digest_id_text = digest_id_text ;
        this.digest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [摘要邮件]脏标记
     */
    @JsonIgnore
    public boolean getDigest_id_textDirtyFlag(){
        return this.digest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [默认的费用别名]
     */
    @JsonProperty("expense_alias_prefix")
    public String getExpense_alias_prefix(){
        return this.expense_alias_prefix ;
    }

    /**
     * 设置 [默认的费用别名]
     */
    @JsonProperty("expense_alias_prefix")
    public void setExpense_alias_prefix(String  expense_alias_prefix){
        this.expense_alias_prefix = expense_alias_prefix ;
        this.expense_alias_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [默认的费用别名]脏标记
     */
    @JsonIgnore
    public boolean getExpense_alias_prefixDirtyFlag(){
        return this.expense_alias_prefixDirtyFlag ;
    }   

    /**
     * 获取 [外部邮件服务器]
     */
    @JsonProperty("external_email_server_default")
    public String getExternal_email_server_default(){
        return this.external_email_server_default ;
    }

    /**
     * 设置 [外部邮件服务器]
     */
    @JsonProperty("external_email_server_default")
    public void setExternal_email_server_default(String  external_email_server_default){
        this.external_email_server_default = external_email_server_default ;
        this.external_email_server_defaultDirtyFlag = true ;
    }

     /**
     * 获取 [外部邮件服务器]脏标记
     */
    @JsonIgnore
    public boolean getExternal_email_server_defaultDirtyFlag(){
        return this.external_email_server_defaultDirtyFlag ;
    }   

    /**
     * 获取 [文档模板]
     */
    @JsonProperty("external_report_layout_id")
    public Integer getExternal_report_layout_id(){
        return this.external_report_layout_id ;
    }

    /**
     * 设置 [文档模板]
     */
    @JsonProperty("external_report_layout_id")
    public void setExternal_report_layout_id(Integer  external_report_layout_id){
        this.external_report_layout_id = external_report_layout_id ;
        this.external_report_layout_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档模板]脏标记
     */
    @JsonIgnore
    public boolean getExternal_report_layout_idDirtyFlag(){
        return this.external_report_layout_idDirtyFlag ;
    }   

    /**
     * 获取 [失败的邮件]
     */
    @JsonProperty("fail_counter")
    public Integer getFail_counter(){
        return this.fail_counter ;
    }

    /**
     * 设置 [失败的邮件]
     */
    @JsonProperty("fail_counter")
    public void setFail_counter(Integer  fail_counter){
        this.fail_counter = fail_counter ;
        this.fail_counterDirtyFlag = true ;
    }

     /**
     * 获取 [失败的邮件]脏标记
     */
    @JsonIgnore
    public boolean getFail_counterDirtyFlag(){
        return this.fail_counterDirtyFlag ;
    }   

    /**
     * 获取 [图标]
     */
    @JsonProperty("favicon")
    public byte[] getFavicon(){
        return this.favicon ;
    }

    /**
     * 设置 [图标]
     */
    @JsonProperty("favicon")
    public void setFavicon(byte[]  favicon){
        this.favicon = favicon ;
        this.faviconDirtyFlag = true ;
    }

     /**
     * 获取 [图标]脏标记
     */
    @JsonIgnore
    public boolean getFaviconDirtyFlag(){
        return this.faviconDirtyFlag ;
    }   

    /**
     * 获取 [手动分配EMail]
     */
    @JsonProperty("generate_lead_from_alias")
    public String getGenerate_lead_from_alias(){
        return this.generate_lead_from_alias ;
    }

    /**
     * 设置 [手动分配EMail]
     */
    @JsonProperty("generate_lead_from_alias")
    public void setGenerate_lead_from_alias(String  generate_lead_from_alias){
        this.generate_lead_from_alias = generate_lead_from_alias ;
        this.generate_lead_from_aliasDirtyFlag = true ;
    }

     /**
     * 获取 [手动分配EMail]脏标记
     */
    @JsonIgnore
    public boolean getGenerate_lead_from_aliasDirtyFlag(){
        return this.generate_lead_from_aliasDirtyFlag ;
    }   

    /**
     * 获取 [谷歌分析密钥]
     */
    @JsonProperty("google_analytics_key")
    public String getGoogle_analytics_key(){
        return this.google_analytics_key ;
    }

    /**
     * 设置 [谷歌分析密钥]
     */
    @JsonProperty("google_analytics_key")
    public void setGoogle_analytics_key(String  google_analytics_key){
        this.google_analytics_key = google_analytics_key ;
        this.google_analytics_keyDirtyFlag = true ;
    }

     /**
     * 获取 [谷歌分析密钥]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_analytics_keyDirtyFlag(){
        return this.google_analytics_keyDirtyFlag ;
    }   

    /**
     * 获取 [Google 客户 ID]
     */
    @JsonProperty("google_management_client_id")
    public String getGoogle_management_client_id(){
        return this.google_management_client_id ;
    }

    /**
     * 设置 [Google 客户 ID]
     */
    @JsonProperty("google_management_client_id")
    public void setGoogle_management_client_id(String  google_management_client_id){
        this.google_management_client_id = google_management_client_id ;
        this.google_management_client_idDirtyFlag = true ;
    }

     /**
     * 获取 [Google 客户 ID]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_management_client_idDirtyFlag(){
        return this.google_management_client_idDirtyFlag ;
    }   

    /**
     * 获取 [Google 客户端密钥]
     */
    @JsonProperty("google_management_client_secret")
    public String getGoogle_management_client_secret(){
        return this.google_management_client_secret ;
    }

    /**
     * 设置 [Google 客户端密钥]
     */
    @JsonProperty("google_management_client_secret")
    public void setGoogle_management_client_secret(String  google_management_client_secret){
        this.google_management_client_secret = google_management_client_secret ;
        this.google_management_client_secretDirtyFlag = true ;
    }

     /**
     * 获取 [Google 客户端密钥]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_management_client_secretDirtyFlag(){
        return this.google_management_client_secretDirtyFlag ;
    }   

    /**
     * 获取 [Google 地图 API 密钥]
     */
    @JsonProperty("google_maps_api_key")
    public String getGoogle_maps_api_key(){
        return this.google_maps_api_key ;
    }

    /**
     * 设置 [Google 地图 API 密钥]
     */
    @JsonProperty("google_maps_api_key")
    public void setGoogle_maps_api_key(String  google_maps_api_key){
        this.google_maps_api_key = google_maps_api_key ;
        this.google_maps_api_keyDirtyFlag = true ;
    }

     /**
     * 获取 [Google 地图 API 密钥]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_maps_api_keyDirtyFlag(){
        return this.google_maps_api_keyDirtyFlag ;
    }   

    /**
     * 获取 [分析会计]
     */
    @JsonProperty("group_analytic_accounting")
    public String getGroup_analytic_accounting(){
        return this.group_analytic_accounting ;
    }

    /**
     * 设置 [分析会计]
     */
    @JsonProperty("group_analytic_accounting")
    public void setGroup_analytic_accounting(String  group_analytic_accounting){
        this.group_analytic_accounting = group_analytic_accounting ;
        this.group_analytic_accountingDirtyFlag = true ;
    }

     /**
     * 获取 [分析会计]脏标记
     */
    @JsonIgnore
    public boolean getGroup_analytic_accountingDirtyFlag(){
        return this.group_analytic_accountingDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("group_analytic_tags")
    public String getGroup_analytic_tags(){
        return this.group_analytic_tags ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("group_analytic_tags")
    public void setGroup_analytic_tags(String  group_analytic_tags){
        this.group_analytic_tags = group_analytic_tags ;
        this.group_analytic_tagsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getGroup_analytic_tagsDirtyFlag(){
        return this.group_analytic_tagsDirtyFlag ;
    }   

    /**
     * 获取 [员工 PIN]
     */
    @JsonProperty("group_attendance_use_pin")
    public String getGroup_attendance_use_pin(){
        return this.group_attendance_use_pin ;
    }

    /**
     * 设置 [员工 PIN]
     */
    @JsonProperty("group_attendance_use_pin")
    public void setGroup_attendance_use_pin(String  group_attendance_use_pin){
        this.group_attendance_use_pin = group_attendance_use_pin ;
        this.group_attendance_use_pinDirtyFlag = true ;
    }

     /**
     * 获取 [员工 PIN]脏标记
     */
    @JsonIgnore
    public boolean getGroup_attendance_use_pinDirtyFlag(){
        return this.group_attendance_use_pinDirtyFlag ;
    }   

    /**
     * 获取 [现金舍入]
     */
    @JsonProperty("group_cash_rounding")
    public String getGroup_cash_rounding(){
        return this.group_cash_rounding ;
    }

    /**
     * 设置 [现金舍入]
     */
    @JsonProperty("group_cash_rounding")
    public void setGroup_cash_rounding(String  group_cash_rounding){
        this.group_cash_rounding = group_cash_rounding ;
        this.group_cash_roundingDirtyFlag = true ;
    }

     /**
     * 获取 [现金舍入]脏标记
     */
    @JsonIgnore
    public boolean getGroup_cash_roundingDirtyFlag(){
        return this.group_cash_roundingDirtyFlag ;
    }   

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("group_delivery_invoice_address")
    public String getGroup_delivery_invoice_address(){
        return this.group_delivery_invoice_address ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("group_delivery_invoice_address")
    public void setGroup_delivery_invoice_address(String  group_delivery_invoice_address){
        this.group_delivery_invoice_address = group_delivery_invoice_address ;
        this.group_delivery_invoice_addressDirtyFlag = true ;
    }

     /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getGroup_delivery_invoice_addressDirtyFlag(){
        return this.group_delivery_invoice_addressDirtyFlag ;
    }   

    /**
     * 获取 [折扣]
     */
    @JsonProperty("group_discount_per_so_line")
    public String getGroup_discount_per_so_line(){
        return this.group_discount_per_so_line ;
    }

    /**
     * 设置 [折扣]
     */
    @JsonProperty("group_discount_per_so_line")
    public void setGroup_discount_per_so_line(String  group_discount_per_so_line){
        this.group_discount_per_so_line = group_discount_per_so_line ;
        this.group_discount_per_so_lineDirtyFlag = true ;
    }

     /**
     * 获取 [折扣]脏标记
     */
    @JsonIgnore
    public boolean getGroup_discount_per_so_lineDirtyFlag(){
        return this.group_discount_per_so_lineDirtyFlag ;
    }   

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("group_display_incoterm")
    public String getGroup_display_incoterm(){
        return this.group_display_incoterm ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("group_display_incoterm")
    public void setGroup_display_incoterm(String  group_display_incoterm){
        this.group_display_incoterm = group_display_incoterm ;
        this.group_display_incotermDirtyFlag = true ;
    }

     /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getGroup_display_incotermDirtyFlag(){
        return this.group_display_incotermDirtyFlag ;
    }   

    /**
     * 获取 [财年]
     */
    @JsonProperty("group_fiscal_year")
    public String getGroup_fiscal_year(){
        return this.group_fiscal_year ;
    }

    /**
     * 设置 [财年]
     */
    @JsonProperty("group_fiscal_year")
    public void setGroup_fiscal_year(String  group_fiscal_year){
        this.group_fiscal_year = group_fiscal_year ;
        this.group_fiscal_yearDirtyFlag = true ;
    }

     /**
     * 获取 [财年]脏标记
     */
    @JsonIgnore
    public boolean getGroup_fiscal_yearDirtyFlag(){
        return this.group_fiscal_yearDirtyFlag ;
    }   

    /**
     * 获取 [显示批次 / 序列号]
     */
    @JsonProperty("group_lot_on_delivery_slip")
    public String getGroup_lot_on_delivery_slip(){
        return this.group_lot_on_delivery_slip ;
    }

    /**
     * 设置 [显示批次 / 序列号]
     */
    @JsonProperty("group_lot_on_delivery_slip")
    public void setGroup_lot_on_delivery_slip(String  group_lot_on_delivery_slip){
        this.group_lot_on_delivery_slip = group_lot_on_delivery_slip ;
        this.group_lot_on_delivery_slipDirtyFlag = true ;
    }

     /**
     * 获取 [显示批次 / 序列号]脏标记
     */
    @JsonIgnore
    public boolean getGroup_lot_on_delivery_slipDirtyFlag(){
        return this.group_lot_on_delivery_slipDirtyFlag ;
    }   

    /**
     * 获取 [供应商价格表]
     */
    @JsonProperty("group_manage_vendor_price")
    public String getGroup_manage_vendor_price(){
        return this.group_manage_vendor_price ;
    }

    /**
     * 设置 [供应商价格表]
     */
    @JsonProperty("group_manage_vendor_price")
    public void setGroup_manage_vendor_price(String  group_manage_vendor_price){
        this.group_manage_vendor_price = group_manage_vendor_price ;
        this.group_manage_vendor_priceDirtyFlag = true ;
    }

     /**
     * 获取 [供应商价格表]脏标记
     */
    @JsonIgnore
    public boolean getGroup_manage_vendor_priceDirtyFlag(){
        return this.group_manage_vendor_priceDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("group_mass_mailing_campaign")
    public String getGroup_mass_mailing_campaign(){
        return this.group_mass_mailing_campaign ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("group_mass_mailing_campaign")
    public void setGroup_mass_mailing_campaign(String  group_mass_mailing_campaign){
        this.group_mass_mailing_campaign = group_mass_mailing_campaign ;
        this.group_mass_mailing_campaignDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getGroup_mass_mailing_campaignDirtyFlag(){
        return this.group_mass_mailing_campaignDirtyFlag ;
    }   

    /**
     * 获取 [MRP 工单]
     */
    @JsonProperty("group_mrp_routings")
    public String getGroup_mrp_routings(){
        return this.group_mrp_routings ;
    }

    /**
     * 设置 [MRP 工单]
     */
    @JsonProperty("group_mrp_routings")
    public void setGroup_mrp_routings(String  group_mrp_routings){
        this.group_mrp_routings = group_mrp_routings ;
        this.group_mrp_routingsDirtyFlag = true ;
    }

     /**
     * 获取 [MRP 工单]脏标记
     */
    @JsonIgnore
    public boolean getGroup_mrp_routingsDirtyFlag(){
        return this.group_mrp_routingsDirtyFlag ;
    }   

    /**
     * 获取 [管理多公司]
     */
    @JsonProperty("group_multi_company")
    public String getGroup_multi_company(){
        return this.group_multi_company ;
    }

    /**
     * 设置 [管理多公司]
     */
    @JsonProperty("group_multi_company")
    public void setGroup_multi_company(String  group_multi_company){
        this.group_multi_company = group_multi_company ;
        this.group_multi_companyDirtyFlag = true ;
    }

     /**
     * 获取 [管理多公司]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_companyDirtyFlag(){
        return this.group_multi_companyDirtyFlag ;
    }   

    /**
     * 获取 [多币种]
     */
    @JsonProperty("group_multi_currency")
    public String getGroup_multi_currency(){
        return this.group_multi_currency ;
    }

    /**
     * 设置 [多币种]
     */
    @JsonProperty("group_multi_currency")
    public void setGroup_multi_currency(String  group_multi_currency){
        this.group_multi_currency = group_multi_currency ;
        this.group_multi_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [多币种]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_currencyDirtyFlag(){
        return this.group_multi_currencyDirtyFlag ;
    }   

    /**
     * 获取 [多网站]
     */
    @JsonProperty("group_multi_website")
    public String getGroup_multi_website(){
        return this.group_multi_website ;
    }

    /**
     * 设置 [多网站]
     */
    @JsonProperty("group_multi_website")
    public void setGroup_multi_website(String  group_multi_website){
        this.group_multi_website = group_multi_website ;
        this.group_multi_websiteDirtyFlag = true ;
    }

     /**
     * 获取 [多网站]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_websiteDirtyFlag(){
        return this.group_multi_websiteDirtyFlag ;
    }   

    /**
     * 获取 [给客户显示价目表]
     */
    @JsonProperty("group_pricelist_item")
    public String getGroup_pricelist_item(){
        return this.group_pricelist_item ;
    }

    /**
     * 设置 [给客户显示价目表]
     */
    @JsonProperty("group_pricelist_item")
    public void setGroup_pricelist_item(String  group_pricelist_item){
        this.group_pricelist_item = group_pricelist_item ;
        this.group_pricelist_itemDirtyFlag = true ;
    }

     /**
     * 获取 [给客户显示价目表]脏标记
     */
    @JsonIgnore
    public boolean getGroup_pricelist_itemDirtyFlag(){
        return this.group_pricelist_itemDirtyFlag ;
    }   

    /**
     * 获取 [使用供应商单据里的产品]
     */
    @JsonProperty("group_products_in_bills")
    public String getGroup_products_in_bills(){
        return this.group_products_in_bills ;
    }

    /**
     * 设置 [使用供应商单据里的产品]
     */
    @JsonProperty("group_products_in_bills")
    public void setGroup_products_in_bills(String  group_products_in_bills){
        this.group_products_in_bills = group_products_in_bills ;
        this.group_products_in_billsDirtyFlag = true ;
    }

     /**
     * 获取 [使用供应商单据里的产品]脏标记
     */
    @JsonIgnore
    public boolean getGroup_products_in_billsDirtyFlag(){
        return this.group_products_in_billsDirtyFlag ;
    }   

    /**
     * 获取 [显示产品的价目表]
     */
    @JsonProperty("group_product_pricelist")
    public String getGroup_product_pricelist(){
        return this.group_product_pricelist ;
    }

    /**
     * 设置 [显示产品的价目表]
     */
    @JsonProperty("group_product_pricelist")
    public void setGroup_product_pricelist(String  group_product_pricelist){
        this.group_product_pricelist = group_product_pricelist ;
        this.group_product_pricelistDirtyFlag = true ;
    }

     /**
     * 获取 [显示产品的价目表]脏标记
     */
    @JsonIgnore
    public boolean getGroup_product_pricelistDirtyFlag(){
        return this.group_product_pricelistDirtyFlag ;
    }   

    /**
     * 获取 [变体和选项]
     */
    @JsonProperty("group_product_variant")
    public String getGroup_product_variant(){
        return this.group_product_variant ;
    }

    /**
     * 设置 [变体和选项]
     */
    @JsonProperty("group_product_variant")
    public void setGroup_product_variant(String  group_product_variant){
        this.group_product_variant = group_product_variant ;
        this.group_product_variantDirtyFlag = true ;
    }

     /**
     * 获取 [变体和选项]脏标记
     */
    @JsonIgnore
    public boolean getGroup_product_variantDirtyFlag(){
        return this.group_product_variantDirtyFlag ;
    }   

    /**
     * 获取 [形式发票]
     */
    @JsonProperty("group_proforma_sales")
    public String getGroup_proforma_sales(){
        return this.group_proforma_sales ;
    }

    /**
     * 设置 [形式发票]
     */
    @JsonProperty("group_proforma_sales")
    public void setGroup_proforma_sales(String  group_proforma_sales){
        this.group_proforma_sales = group_proforma_sales ;
        this.group_proforma_salesDirtyFlag = true ;
    }

     /**
     * 获取 [形式发票]脏标记
     */
    @JsonIgnore
    public boolean getGroup_proforma_salesDirtyFlag(){
        return this.group_proforma_salesDirtyFlag ;
    }   

    /**
     * 获取 [使用项目评级]
     */
    @JsonProperty("group_project_rating")
    public String getGroup_project_rating(){
        return this.group_project_rating ;
    }

    /**
     * 设置 [使用项目评级]
     */
    @JsonProperty("group_project_rating")
    public void setGroup_project_rating(String  group_project_rating){
        this.group_project_rating = group_project_rating ;
        this.group_project_ratingDirtyFlag = true ;
    }

     /**
     * 获取 [使用项目评级]脏标记
     */
    @JsonIgnore
    public boolean getGroup_project_ratingDirtyFlag(){
        return this.group_project_ratingDirtyFlag ;
    }   

    /**
     * 获取 [订单特定路线]
     */
    @JsonProperty("group_route_so_lines")
    public String getGroup_route_so_lines(){
        return this.group_route_so_lines ;
    }

    /**
     * 设置 [订单特定路线]
     */
    @JsonProperty("group_route_so_lines")
    public void setGroup_route_so_lines(String  group_route_so_lines){
        this.group_route_so_lines = group_route_so_lines ;
        this.group_route_so_linesDirtyFlag = true ;
    }

     /**
     * 获取 [订单特定路线]脏标记
     */
    @JsonIgnore
    public boolean getGroup_route_so_linesDirtyFlag(){
        return this.group_route_so_linesDirtyFlag ;
    }   

    /**
     * 获取 [客户地址]
     */
    @JsonProperty("group_sale_delivery_address")
    public String getGroup_sale_delivery_address(){
        return this.group_sale_delivery_address ;
    }

    /**
     * 设置 [客户地址]
     */
    @JsonProperty("group_sale_delivery_address")
    public void setGroup_sale_delivery_address(String  group_sale_delivery_address){
        this.group_sale_delivery_address = group_sale_delivery_address ;
        this.group_sale_delivery_addressDirtyFlag = true ;
    }

     /**
     * 获取 [客户地址]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_delivery_addressDirtyFlag(){
        return this.group_sale_delivery_addressDirtyFlag ;
    }   

    /**
     * 获取 [交货日期]
     */
    @JsonProperty("group_sale_order_dates")
    public String getGroup_sale_order_dates(){
        return this.group_sale_order_dates ;
    }

    /**
     * 设置 [交货日期]
     */
    @JsonProperty("group_sale_order_dates")
    public void setGroup_sale_order_dates(String  group_sale_order_dates){
        this.group_sale_order_dates = group_sale_order_dates ;
        this.group_sale_order_datesDirtyFlag = true ;
    }

     /**
     * 获取 [交货日期]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_order_datesDirtyFlag(){
        return this.group_sale_order_datesDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("group_sale_order_template")
    public String getGroup_sale_order_template(){
        return this.group_sale_order_template ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("group_sale_order_template")
    public void setGroup_sale_order_template(String  group_sale_order_template){
        this.group_sale_order_template = group_sale_order_template ;
        this.group_sale_order_templateDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_order_templateDirtyFlag(){
        return this.group_sale_order_templateDirtyFlag ;
    }   

    /**
     * 获取 [为每个客户使用价格表来适配您的价格]
     */
    @JsonProperty("group_sale_pricelist")
    public String getGroup_sale_pricelist(){
        return this.group_sale_pricelist ;
    }

    /**
     * 设置 [为每个客户使用价格表来适配您的价格]
     */
    @JsonProperty("group_sale_pricelist")
    public void setGroup_sale_pricelist(String  group_sale_pricelist){
        this.group_sale_pricelist = group_sale_pricelist ;
        this.group_sale_pricelistDirtyFlag = true ;
    }

     /**
     * 获取 [为每个客户使用价格表来适配您的价格]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_pricelistDirtyFlag(){
        return this.group_sale_pricelistDirtyFlag ;
    }   

    /**
     * 获取 [明细行汇总含税(B2B).]
     */
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    public String getGroup_show_line_subtotals_tax_excluded(){
        return this.group_show_line_subtotals_tax_excluded ;
    }

    /**
     * 设置 [明细行汇总含税(B2B).]
     */
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    public void setGroup_show_line_subtotals_tax_excluded(String  group_show_line_subtotals_tax_excluded){
        this.group_show_line_subtotals_tax_excluded = group_show_line_subtotals_tax_excluded ;
        this.group_show_line_subtotals_tax_excludedDirtyFlag = true ;
    }

     /**
     * 获取 [明细行汇总含税(B2B).]脏标记
     */
    @JsonIgnore
    public boolean getGroup_show_line_subtotals_tax_excludedDirtyFlag(){
        return this.group_show_line_subtotals_tax_excludedDirtyFlag ;
    }   

    /**
     * 获取 [显示含税明细行在汇总表(B2B).]
     */
    @JsonProperty("group_show_line_subtotals_tax_included")
    public String getGroup_show_line_subtotals_tax_included(){
        return this.group_show_line_subtotals_tax_included ;
    }

    /**
     * 设置 [显示含税明细行在汇总表(B2B).]
     */
    @JsonProperty("group_show_line_subtotals_tax_included")
    public void setGroup_show_line_subtotals_tax_included(String  group_show_line_subtotals_tax_included){
        this.group_show_line_subtotals_tax_included = group_show_line_subtotals_tax_included ;
        this.group_show_line_subtotals_tax_includedDirtyFlag = true ;
    }

     /**
     * 获取 [显示含税明细行在汇总表(B2B).]脏标记
     */
    @JsonIgnore
    public boolean getGroup_show_line_subtotals_tax_includedDirtyFlag(){
        return this.group_show_line_subtotals_tax_includedDirtyFlag ;
    }   

    /**
     * 获取 [多步路由]
     */
    @JsonProperty("group_stock_adv_location")
    public String getGroup_stock_adv_location(){
        return this.group_stock_adv_location ;
    }

    /**
     * 设置 [多步路由]
     */
    @JsonProperty("group_stock_adv_location")
    public void setGroup_stock_adv_location(String  group_stock_adv_location){
        this.group_stock_adv_location = group_stock_adv_location ;
        this.group_stock_adv_locationDirtyFlag = true ;
    }

     /**
     * 获取 [多步路由]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_adv_locationDirtyFlag(){
        return this.group_stock_adv_locationDirtyFlag ;
    }   

    /**
     * 获取 [储存位置]
     */
    @JsonProperty("group_stock_multi_locations")
    public String getGroup_stock_multi_locations(){
        return this.group_stock_multi_locations ;
    }

    /**
     * 设置 [储存位置]
     */
    @JsonProperty("group_stock_multi_locations")
    public void setGroup_stock_multi_locations(String  group_stock_multi_locations){
        this.group_stock_multi_locations = group_stock_multi_locations ;
        this.group_stock_multi_locationsDirtyFlag = true ;
    }

     /**
     * 获取 [储存位置]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_multi_locationsDirtyFlag(){
        return this.group_stock_multi_locationsDirtyFlag ;
    }   

    /**
     * 获取 [多仓库]
     */
    @JsonProperty("group_stock_multi_warehouses")
    public String getGroup_stock_multi_warehouses(){
        return this.group_stock_multi_warehouses ;
    }

    /**
     * 设置 [多仓库]
     */
    @JsonProperty("group_stock_multi_warehouses")
    public void setGroup_stock_multi_warehouses(String  group_stock_multi_warehouses){
        this.group_stock_multi_warehouses = group_stock_multi_warehouses ;
        this.group_stock_multi_warehousesDirtyFlag = true ;
    }

     /**
     * 获取 [多仓库]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_multi_warehousesDirtyFlag(){
        return this.group_stock_multi_warehousesDirtyFlag ;
    }   

    /**
     * 获取 [产品包装]
     */
    @JsonProperty("group_stock_packaging")
    public String getGroup_stock_packaging(){
        return this.group_stock_packaging ;
    }

    /**
     * 设置 [产品包装]
     */
    @JsonProperty("group_stock_packaging")
    public void setGroup_stock_packaging(String  group_stock_packaging){
        this.group_stock_packaging = group_stock_packaging ;
        this.group_stock_packagingDirtyFlag = true ;
    }

     /**
     * 获取 [产品包装]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_packagingDirtyFlag(){
        return this.group_stock_packagingDirtyFlag ;
    }   

    /**
     * 获取 [批次和序列号]
     */
    @JsonProperty("group_stock_production_lot")
    public String getGroup_stock_production_lot(){
        return this.group_stock_production_lot ;
    }

    /**
     * 设置 [批次和序列号]
     */
    @JsonProperty("group_stock_production_lot")
    public void setGroup_stock_production_lot(String  group_stock_production_lot){
        this.group_stock_production_lot = group_stock_production_lot ;
        this.group_stock_production_lotDirtyFlag = true ;
    }

     /**
     * 获取 [批次和序列号]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_production_lotDirtyFlag(){
        return this.group_stock_production_lotDirtyFlag ;
    }   

    /**
     * 获取 [交货包裹]
     */
    @JsonProperty("group_stock_tracking_lot")
    public String getGroup_stock_tracking_lot(){
        return this.group_stock_tracking_lot ;
    }

    /**
     * 设置 [交货包裹]
     */
    @JsonProperty("group_stock_tracking_lot")
    public void setGroup_stock_tracking_lot(String  group_stock_tracking_lot){
        this.group_stock_tracking_lot = group_stock_tracking_lot ;
        this.group_stock_tracking_lotDirtyFlag = true ;
    }

     /**
     * 获取 [交货包裹]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_tracking_lotDirtyFlag(){
        return this.group_stock_tracking_lotDirtyFlag ;
    }   

    /**
     * 获取 [寄售]
     */
    @JsonProperty("group_stock_tracking_owner")
    public String getGroup_stock_tracking_owner(){
        return this.group_stock_tracking_owner ;
    }

    /**
     * 设置 [寄售]
     */
    @JsonProperty("group_stock_tracking_owner")
    public void setGroup_stock_tracking_owner(String  group_stock_tracking_owner){
        this.group_stock_tracking_owner = group_stock_tracking_owner ;
        this.group_stock_tracking_ownerDirtyFlag = true ;
    }

     /**
     * 获取 [寄售]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_tracking_ownerDirtyFlag(){
        return this.group_stock_tracking_ownerDirtyFlag ;
    }   

    /**
     * 获取 [子任务]
     */
    @JsonProperty("group_subtask_project")
    public String getGroup_subtask_project(){
        return this.group_subtask_project ;
    }

    /**
     * 设置 [子任务]
     */
    @JsonProperty("group_subtask_project")
    public void setGroup_subtask_project(String  group_subtask_project){
        this.group_subtask_project = group_subtask_project ;
        this.group_subtask_projectDirtyFlag = true ;
    }

     /**
     * 获取 [子任务]脏标记
     */
    @JsonIgnore
    public boolean getGroup_subtask_projectDirtyFlag(){
        return this.group_subtask_projectDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("group_uom")
    public String getGroup_uom(){
        return this.group_uom ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("group_uom")
    public void setGroup_uom(String  group_uom){
        this.group_uom = group_uom ;
        this.group_uomDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getGroup_uomDirtyFlag(){
        return this.group_uomDirtyFlag ;
    }   

    /**
     * 获取 [线索]
     */
    @JsonProperty("group_use_lead")
    public String getGroup_use_lead(){
        return this.group_use_lead ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("group_use_lead")
    public void setGroup_use_lead(String  group_use_lead){
        this.group_use_lead = group_use_lead ;
        this.group_use_leadDirtyFlag = true ;
    }

     /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getGroup_use_leadDirtyFlag(){
        return this.group_use_leadDirtyFlag ;
    }   

    /**
     * 获取 [发票警告]
     */
    @JsonProperty("group_warning_account")
    public String getGroup_warning_account(){
        return this.group_warning_account ;
    }

    /**
     * 设置 [发票警告]
     */
    @JsonProperty("group_warning_account")
    public void setGroup_warning_account(String  group_warning_account){
        this.group_warning_account = group_warning_account ;
        this.group_warning_accountDirtyFlag = true ;
    }

     /**
     * 获取 [发票警告]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_accountDirtyFlag(){
        return this.group_warning_accountDirtyFlag ;
    }   

    /**
     * 获取 [采购警告]
     */
    @JsonProperty("group_warning_purchase")
    public String getGroup_warning_purchase(){
        return this.group_warning_purchase ;
    }

    /**
     * 设置 [采购警告]
     */
    @JsonProperty("group_warning_purchase")
    public void setGroup_warning_purchase(String  group_warning_purchase){
        this.group_warning_purchase = group_warning_purchase ;
        this.group_warning_purchaseDirtyFlag = true ;
    }

     /**
     * 获取 [采购警告]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_purchaseDirtyFlag(){
        return this.group_warning_purchaseDirtyFlag ;
    }   

    /**
     * 获取 [销售订单警告]
     */
    @JsonProperty("group_warning_sale")
    public String getGroup_warning_sale(){
        return this.group_warning_sale ;
    }

    /**
     * 设置 [销售订单警告]
     */
    @JsonProperty("group_warning_sale")
    public void setGroup_warning_sale(String  group_warning_sale){
        this.group_warning_sale = group_warning_sale ;
        this.group_warning_saleDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单警告]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_saleDirtyFlag(){
        return this.group_warning_saleDirtyFlag ;
    }   

    /**
     * 获取 [库存警报]
     */
    @JsonProperty("group_warning_stock")
    public String getGroup_warning_stock(){
        return this.group_warning_stock ;
    }

    /**
     * 设置 [库存警报]
     */
    @JsonProperty("group_warning_stock")
    public void setGroup_warning_stock(String  group_warning_stock){
        this.group_warning_stock = group_warning_stock ;
        this.group_warning_stockDirtyFlag = true ;
    }

     /**
     * 获取 [库存警报]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_stockDirtyFlag(){
        return this.group_warning_stockDirtyFlag ;
    }   

    /**
     * 获取 [网站弹出窗口]
     */
    @JsonProperty("group_website_popup_on_exit")
    public String getGroup_website_popup_on_exit(){
        return this.group_website_popup_on_exit ;
    }

    /**
     * 设置 [网站弹出窗口]
     */
    @JsonProperty("group_website_popup_on_exit")
    public void setGroup_website_popup_on_exit(String  group_website_popup_on_exit){
        this.group_website_popup_on_exit = group_website_popup_on_exit ;
        this.group_website_popup_on_exitDirtyFlag = true ;
    }

     /**
     * 获取 [网站弹出窗口]脏标记
     */
    @JsonIgnore
    public boolean getGroup_website_popup_on_exitDirtyFlag(){
        return this.group_website_popup_on_exitDirtyFlag ;
    }   

    /**
     * 获取 [有会计分录]
     */
    @JsonProperty("has_accounting_entries")
    public String getHas_accounting_entries(){
        return this.has_accounting_entries ;
    }

    /**
     * 设置 [有会计分录]
     */
    @JsonProperty("has_accounting_entries")
    public void setHas_accounting_entries(String  has_accounting_entries){
        this.has_accounting_entries = has_accounting_entries ;
        this.has_accounting_entriesDirtyFlag = true ;
    }

     /**
     * 获取 [有会计分录]脏标记
     */
    @JsonIgnore
    public boolean getHas_accounting_entriesDirtyFlag(){
        return this.has_accounting_entriesDirtyFlag ;
    }   

    /**
     * 获取 [公司有科目表]
     */
    @JsonProperty("has_chart_of_accounts")
    public String getHas_chart_of_accounts(){
        return this.has_chart_of_accounts ;
    }

    /**
     * 设置 [公司有科目表]
     */
    @JsonProperty("has_chart_of_accounts")
    public void setHas_chart_of_accounts(String  has_chart_of_accounts){
        this.has_chart_of_accounts = has_chart_of_accounts ;
        this.has_chart_of_accountsDirtyFlag = true ;
    }

     /**
     * 获取 [公司有科目表]脏标记
     */
    @JsonIgnore
    public boolean getHas_chart_of_accountsDirtyFlag(){
        return this.has_chart_of_accountsDirtyFlag ;
    }   

    /**
     * 获取 [Google 分析]
     */
    @JsonProperty("has_google_analytics")
    public String getHas_google_analytics(){
        return this.has_google_analytics ;
    }

    /**
     * 设置 [Google 分析]
     */
    @JsonProperty("has_google_analytics")
    public void setHas_google_analytics(String  has_google_analytics){
        this.has_google_analytics = has_google_analytics ;
        this.has_google_analyticsDirtyFlag = true ;
    }

     /**
     * 获取 [Google 分析]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_analyticsDirtyFlag(){
        return this.has_google_analyticsDirtyFlag ;
    }   

    /**
     * 获取 [谷歌分析仪表板]
     */
    @JsonProperty("has_google_analytics_dashboard")
    public String getHas_google_analytics_dashboard(){
        return this.has_google_analytics_dashboard ;
    }

    /**
     * 设置 [谷歌分析仪表板]
     */
    @JsonProperty("has_google_analytics_dashboard")
    public void setHas_google_analytics_dashboard(String  has_google_analytics_dashboard){
        this.has_google_analytics_dashboard = has_google_analytics_dashboard ;
        this.has_google_analytics_dashboardDirtyFlag = true ;
    }

     /**
     * 获取 [谷歌分析仪表板]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_analytics_dashboardDirtyFlag(){
        return this.has_google_analytics_dashboardDirtyFlag ;
    }   

    /**
     * 获取 [Google 地图]
     */
    @JsonProperty("has_google_maps")
    public String getHas_google_maps(){
        return this.has_google_maps ;
    }

    /**
     * 设置 [Google 地图]
     */
    @JsonProperty("has_google_maps")
    public void setHas_google_maps(String  has_google_maps){
        this.has_google_maps = has_google_maps ;
        this.has_google_mapsDirtyFlag = true ;
    }

     /**
     * 获取 [Google 地图]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_mapsDirtyFlag(){
        return this.has_google_mapsDirtyFlag ;
    }   

    /**
     * 获取 [设置社交平台]
     */
    @JsonProperty("has_social_network")
    public String getHas_social_network(){
        return this.has_social_network ;
    }

    /**
     * 设置 [设置社交平台]
     */
    @JsonProperty("has_social_network")
    public void setHas_social_network(String  has_social_network){
        this.has_social_network = has_social_network ;
        this.has_social_networkDirtyFlag = true ;
    }

     /**
     * 获取 [设置社交平台]脏标记
     */
    @JsonIgnore
    public boolean getHas_social_networkDirtyFlag(){
        return this.has_social_networkDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [库存可用性]
     */
    @JsonProperty("inventory_availability")
    public String getInventory_availability(){
        return this.inventory_availability ;
    }

    /**
     * 设置 [库存可用性]
     */
    @JsonProperty("inventory_availability")
    public void setInventory_availability(String  inventory_availability){
        this.inventory_availability = inventory_availability ;
        this.inventory_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [库存可用性]脏标记
     */
    @JsonIgnore
    public boolean getInventory_availabilityDirtyFlag(){
        return this.inventory_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [发送EMail]
     */
    @JsonProperty("invoice_is_email")
    public String getInvoice_is_email(){
        return this.invoice_is_email ;
    }

    /**
     * 设置 [发送EMail]
     */
    @JsonProperty("invoice_is_email")
    public void setInvoice_is_email(String  invoice_is_email){
        this.invoice_is_email = invoice_is_email ;
        this.invoice_is_emailDirtyFlag = true ;
    }

     /**
     * 获取 [发送EMail]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_emailDirtyFlag(){
        return this.invoice_is_emailDirtyFlag ;
    }   

    /**
     * 获取 [打印]
     */
    @JsonProperty("invoice_is_print")
    public String getInvoice_is_print(){
        return this.invoice_is_print ;
    }

    /**
     * 设置 [打印]
     */
    @JsonProperty("invoice_is_print")
    public void setInvoice_is_print(String  invoice_is_print){
        this.invoice_is_print = invoice_is_print ;
        this.invoice_is_printDirtyFlag = true ;
    }

     /**
     * 获取 [打印]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_printDirtyFlag(){
        return this.invoice_is_printDirtyFlag ;
    }   

    /**
     * 获取 [透过邮递]
     */
    @JsonProperty("invoice_is_snailmail")
    public String getInvoice_is_snailmail(){
        return this.invoice_is_snailmail ;
    }

    /**
     * 设置 [透过邮递]
     */
    @JsonProperty("invoice_is_snailmail")
    public void setInvoice_is_snailmail(String  invoice_is_snailmail){
        this.invoice_is_snailmail = invoice_is_snailmail ;
        this.invoice_is_snailmailDirtyFlag = true ;
    }

     /**
     * 获取 [透过邮递]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_snailmailDirtyFlag(){
        return this.invoice_is_snailmailDirtyFlag ;
    }   

    /**
     * 获取 [交流]
     */
    @JsonProperty("invoice_reference_type")
    public String getInvoice_reference_type(){
        return this.invoice_reference_type ;
    }

    /**
     * 设置 [交流]
     */
    @JsonProperty("invoice_reference_type")
    public void setInvoice_reference_type(String  invoice_reference_type){
        this.invoice_reference_type = invoice_reference_type ;
        this.invoice_reference_typeDirtyFlag = true ;
    }

     /**
     * 获取 [交流]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_reference_typeDirtyFlag(){
        return this.invoice_reference_typeDirtyFlag ;
    }   

    /**
     * 获取 [销售模块是否已安装]
     */
    @JsonProperty("is_installed_sale")
    public String getIs_installed_sale(){
        return this.is_installed_sale ;
    }

    /**
     * 设置 [销售模块是否已安装]
     */
    @JsonProperty("is_installed_sale")
    public void setIs_installed_sale(String  is_installed_sale){
        this.is_installed_sale = is_installed_sale ;
        this.is_installed_saleDirtyFlag = true ;
    }

     /**
     * 获取 [销售模块是否已安装]脏标记
     */
    @JsonIgnore
    public boolean getIs_installed_saleDirtyFlag(){
        return this.is_installed_saleDirtyFlag ;
    }   

    /**
     * 获取 [语言数量]
     */
    @JsonProperty("language_count")
    public Integer getLanguage_count(){
        return this.language_count ;
    }

    /**
     * 设置 [语言数量]
     */
    @JsonProperty("language_count")
    public void setLanguage_count(Integer  language_count){
        this.language_count = language_count ;
        this.language_countDirtyFlag = true ;
    }

     /**
     * 获取 [语言数量]脏标记
     */
    @JsonIgnore
    public boolean getLanguage_countDirtyFlag(){
        return this.language_countDirtyFlag ;
    }   

    /**
     * 获取 [语言]
     */
    @JsonProperty("language_ids")
    public String getLanguage_ids(){
        return this.language_ids ;
    }

    /**
     * 设置 [语言]
     */
    @JsonProperty("language_ids")
    public void setLanguage_ids(String  language_ids){
        this.language_ids = language_ids ;
        this.language_idsDirtyFlag = true ;
    }

     /**
     * 获取 [语言]脏标记
     */
    @JsonIgnore
    public boolean getLanguage_idsDirtyFlag(){
        return this.language_idsDirtyFlag ;
    }   

    /**
     * 获取 [锁定确认订单]
     */
    @JsonProperty("lock_confirmed_po")
    public String getLock_confirmed_po(){
        return this.lock_confirmed_po ;
    }

    /**
     * 设置 [锁定确认订单]
     */
    @JsonProperty("lock_confirmed_po")
    public void setLock_confirmed_po(String  lock_confirmed_po){
        this.lock_confirmed_po = lock_confirmed_po ;
        this.lock_confirmed_poDirtyFlag = true ;
    }

     /**
     * 获取 [锁定确认订单]脏标记
     */
    @JsonIgnore
    public boolean getLock_confirmed_poDirtyFlag(){
        return this.lock_confirmed_poDirtyFlag ;
    }   

    /**
     * 获取 [制造提前期(日)]
     */
    @JsonProperty("manufacturing_lead")
    public Double getManufacturing_lead(){
        return this.manufacturing_lead ;
    }

    /**
     * 设置 [制造提前期(日)]
     */
    @JsonProperty("manufacturing_lead")
    public void setManufacturing_lead(Double  manufacturing_lead){
        this.manufacturing_lead = manufacturing_lead ;
        this.manufacturing_leadDirtyFlag = true ;
    }

     /**
     * 获取 [制造提前期(日)]脏标记
     */
    @JsonIgnore
    public boolean getManufacturing_leadDirtyFlag(){
        return this.manufacturing_leadDirtyFlag ;
    }   

    /**
     * 获取 [邮件服务器]
     */
    @JsonProperty("mass_mailing_mail_server_id")
    public Integer getMass_mailing_mail_server_id(){
        return this.mass_mailing_mail_server_id ;
    }

    /**
     * 设置 [邮件服务器]
     */
    @JsonProperty("mass_mailing_mail_server_id")
    public void setMass_mailing_mail_server_id(Integer  mass_mailing_mail_server_id){
        this.mass_mailing_mail_server_id = mass_mailing_mail_server_id ;
        this.mass_mailing_mail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件服务器]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_mail_server_idDirtyFlag(){
        return this.mass_mailing_mail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [指定邮件服务器]
     */
    @JsonProperty("mass_mailing_outgoing_mail_server")
    public String getMass_mailing_outgoing_mail_server(){
        return this.mass_mailing_outgoing_mail_server ;
    }

    /**
     * 设置 [指定邮件服务器]
     */
    @JsonProperty("mass_mailing_outgoing_mail_server")
    public void setMass_mailing_outgoing_mail_server(String  mass_mailing_outgoing_mail_server){
        this.mass_mailing_outgoing_mail_server = mass_mailing_outgoing_mail_server ;
        this.mass_mailing_outgoing_mail_serverDirtyFlag = true ;
    }

     /**
     * 获取 [指定邮件服务器]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_outgoing_mail_serverDirtyFlag(){
        return this.mass_mailing_outgoing_mail_serverDirtyFlag ;
    }   

    /**
     * 获取 [开票]
     */
    @JsonProperty("module_account")
    public String getModule_account(){
        return this.module_account ;
    }

    /**
     * 设置 [开票]
     */
    @JsonProperty("module_account")
    public void setModule_account(String  module_account){
        this.module_account = module_account ;
        this.module_accountDirtyFlag = true ;
    }

     /**
     * 获取 [开票]脏标记
     */
    @JsonIgnore
    public boolean getModule_accountDirtyFlag(){
        return this.module_accountDirtyFlag ;
    }   

    /**
     * 获取 [三方匹配:采购，收货和发票]
     */
    @JsonProperty("module_account_3way_match")
    public String getModule_account_3way_match(){
        return this.module_account_3way_match ;
    }

    /**
     * 设置 [三方匹配:采购，收货和发票]
     */
    @JsonProperty("module_account_3way_match")
    public void setModule_account_3way_match(String  module_account_3way_match){
        this.module_account_3way_match = module_account_3way_match ;
        this.module_account_3way_matchDirtyFlag = true ;
    }

     /**
     * 获取 [三方匹配:采购，收货和发票]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_3way_matchDirtyFlag(){
        return this.module_account_3way_matchDirtyFlag ;
    }   

    /**
     * 获取 [Accounting]
     */
    @JsonProperty("module_account_accountant")
    public String getModule_account_accountant(){
        return this.module_account_accountant ;
    }

    /**
     * 设置 [Accounting]
     */
    @JsonProperty("module_account_accountant")
    public void setModule_account_accountant(String  module_account_accountant){
        this.module_account_accountant = module_account_accountant ;
        this.module_account_accountantDirtyFlag = true ;
    }

     /**
     * 获取 [Accounting]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_accountantDirtyFlag(){
        return this.module_account_accountantDirtyFlag ;
    }   

    /**
     * 获取 [资产管理]
     */
    @JsonProperty("module_account_asset")
    public String getModule_account_asset(){
        return this.module_account_asset ;
    }

    /**
     * 设置 [资产管理]
     */
    @JsonProperty("module_account_asset")
    public void setModule_account_asset(String  module_account_asset){
        this.module_account_asset = module_account_asset ;
        this.module_account_assetDirtyFlag = true ;
    }

     /**
     * 获取 [资产管理]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_assetDirtyFlag(){
        return this.module_account_assetDirtyFlag ;
    }   

    /**
     * 获取 [用 CAMT.053 格式导入]
     */
    @JsonProperty("module_account_bank_statement_import_camt")
    public String getModule_account_bank_statement_import_camt(){
        return this.module_account_bank_statement_import_camt ;
    }

    /**
     * 设置 [用 CAMT.053 格式导入]
     */
    @JsonProperty("module_account_bank_statement_import_camt")
    public void setModule_account_bank_statement_import_camt(String  module_account_bank_statement_import_camt){
        this.module_account_bank_statement_import_camt = module_account_bank_statement_import_camt ;
        this.module_account_bank_statement_import_camtDirtyFlag = true ;
    }

     /**
     * 获取 [用 CAMT.053 格式导入]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_camtDirtyFlag(){
        return this.module_account_bank_statement_import_camtDirtyFlag ;
    }   

    /**
     * 获取 [以 CSV 格式导入]
     */
    @JsonProperty("module_account_bank_statement_import_csv")
    public String getModule_account_bank_statement_import_csv(){
        return this.module_account_bank_statement_import_csv ;
    }

    /**
     * 设置 [以 CSV 格式导入]
     */
    @JsonProperty("module_account_bank_statement_import_csv")
    public void setModule_account_bank_statement_import_csv(String  module_account_bank_statement_import_csv){
        this.module_account_bank_statement_import_csv = module_account_bank_statement_import_csv ;
        this.module_account_bank_statement_import_csvDirtyFlag = true ;
    }

     /**
     * 获取 [以 CSV 格式导入]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_csvDirtyFlag(){
        return this.module_account_bank_statement_import_csvDirtyFlag ;
    }   

    /**
     * 获取 [导入.ofx格式]
     */
    @JsonProperty("module_account_bank_statement_import_ofx")
    public String getModule_account_bank_statement_import_ofx(){
        return this.module_account_bank_statement_import_ofx ;
    }

    /**
     * 设置 [导入.ofx格式]
     */
    @JsonProperty("module_account_bank_statement_import_ofx")
    public void setModule_account_bank_statement_import_ofx(String  module_account_bank_statement_import_ofx){
        this.module_account_bank_statement_import_ofx = module_account_bank_statement_import_ofx ;
        this.module_account_bank_statement_import_ofxDirtyFlag = true ;
    }

     /**
     * 获取 [导入.ofx格式]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_ofxDirtyFlag(){
        return this.module_account_bank_statement_import_ofxDirtyFlag ;
    }   

    /**
     * 获取 [导入.qif 文件]
     */
    @JsonProperty("module_account_bank_statement_import_qif")
    public String getModule_account_bank_statement_import_qif(){
        return this.module_account_bank_statement_import_qif ;
    }

    /**
     * 设置 [导入.qif 文件]
     */
    @JsonProperty("module_account_bank_statement_import_qif")
    public void setModule_account_bank_statement_import_qif(String  module_account_bank_statement_import_qif){
        this.module_account_bank_statement_import_qif = module_account_bank_statement_import_qif ;
        this.module_account_bank_statement_import_qifDirtyFlag = true ;
    }

     /**
     * 获取 [导入.qif 文件]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_qifDirtyFlag(){
        return this.module_account_bank_statement_import_qifDirtyFlag ;
    }   

    /**
     * 获取 [使用批量付款]
     */
    @JsonProperty("module_account_batch_payment")
    public String getModule_account_batch_payment(){
        return this.module_account_batch_payment ;
    }

    /**
     * 设置 [使用批量付款]
     */
    @JsonProperty("module_account_batch_payment")
    public void setModule_account_batch_payment(String  module_account_batch_payment){
        this.module_account_batch_payment = module_account_batch_payment ;
        this.module_account_batch_paymentDirtyFlag = true ;
    }

     /**
     * 获取 [使用批量付款]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_batch_paymentDirtyFlag(){
        return this.module_account_batch_paymentDirtyFlag ;
    }   

    /**
     * 获取 [预算管理]
     */
    @JsonProperty("module_account_budget")
    public String getModule_account_budget(){
        return this.module_account_budget ;
    }

    /**
     * 设置 [预算管理]
     */
    @JsonProperty("module_account_budget")
    public void setModule_account_budget(String  module_account_budget){
        this.module_account_budget = module_account_budget ;
        this.module_account_budgetDirtyFlag = true ;
    }

     /**
     * 获取 [预算管理]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_budgetDirtyFlag(){
        return this.module_account_budgetDirtyFlag ;
    }   

    /**
     * 获取 [允许支票打印和存款]
     */
    @JsonProperty("module_account_check_printing")
    public String getModule_account_check_printing(){
        return this.module_account_check_printing ;
    }

    /**
     * 设置 [允许支票打印和存款]
     */
    @JsonProperty("module_account_check_printing")
    public void setModule_account_check_printing(String  module_account_check_printing){
        this.module_account_check_printing = module_account_check_printing ;
        this.module_account_check_printingDirtyFlag = true ;
    }

     /**
     * 获取 [允许支票打印和存款]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_check_printingDirtyFlag(){
        return this.module_account_check_printingDirtyFlag ;
    }   

    /**
     * 获取 [收入识别]
     */
    @JsonProperty("module_account_deferred_revenue")
    public String getModule_account_deferred_revenue(){
        return this.module_account_deferred_revenue ;
    }

    /**
     * 设置 [收入识别]
     */
    @JsonProperty("module_account_deferred_revenue")
    public void setModule_account_deferred_revenue(String  module_account_deferred_revenue){
        this.module_account_deferred_revenue = module_account_deferred_revenue ;
        this.module_account_deferred_revenueDirtyFlag = true ;
    }

     /**
     * 获取 [收入识别]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_deferred_revenueDirtyFlag(){
        return this.module_account_deferred_revenueDirtyFlag ;
    }   

    /**
     * 获取 [国际贸易统计组织]
     */
    @JsonProperty("module_account_intrastat")
    public String getModule_account_intrastat(){
        return this.module_account_intrastat ;
    }

    /**
     * 设置 [国际贸易统计组织]
     */
    @JsonProperty("module_account_intrastat")
    public void setModule_account_intrastat(String  module_account_intrastat){
        this.module_account_intrastat = module_account_intrastat ;
        this.module_account_intrastatDirtyFlag = true ;
    }

     /**
     * 获取 [国际贸易统计组织]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_intrastatDirtyFlag(){
        return this.module_account_intrastatDirtyFlag ;
    }   

    /**
     * 获取 [自动票据处理]
     */
    @JsonProperty("module_account_invoice_extract")
    public String getModule_account_invoice_extract(){
        return this.module_account_invoice_extract ;
    }

    /**
     * 设置 [自动票据处理]
     */
    @JsonProperty("module_account_invoice_extract")
    public void setModule_account_invoice_extract(String  module_account_invoice_extract){
        this.module_account_invoice_extract = module_account_invoice_extract ;
        this.module_account_invoice_extractDirtyFlag = true ;
    }

     /**
     * 获取 [自动票据处理]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_invoice_extractDirtyFlag(){
        return this.module_account_invoice_extractDirtyFlag ;
    }   

    /**
     * 获取 [发票在线付款]
     */
    @JsonProperty("module_account_payment")
    public String getModule_account_payment(){
        return this.module_account_payment ;
    }

    /**
     * 设置 [发票在线付款]
     */
    @JsonProperty("module_account_payment")
    public void setModule_account_payment(String  module_account_payment){
        this.module_account_payment = module_account_payment ;
        this.module_account_paymentDirtyFlag = true ;
    }

     /**
     * 获取 [发票在线付款]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_paymentDirtyFlag(){
        return this.module_account_paymentDirtyFlag ;
    }   

    /**
     * 获取 [Plaid 接口]
     */
    @JsonProperty("module_account_plaid")
    public String getModule_account_plaid(){
        return this.module_account_plaid ;
    }

    /**
     * 设置 [Plaid 接口]
     */
    @JsonProperty("module_account_plaid")
    public void setModule_account_plaid(String  module_account_plaid){
        this.module_account_plaid = module_account_plaid ;
        this.module_account_plaidDirtyFlag = true ;
    }

     /**
     * 获取 [Plaid 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_plaidDirtyFlag(){
        return this.module_account_plaidDirtyFlag ;
    }   

    /**
     * 获取 [动态报告]
     */
    @JsonProperty("module_account_reports")
    public String getModule_account_reports(){
        return this.module_account_reports ;
    }

    /**
     * 设置 [动态报告]
     */
    @JsonProperty("module_account_reports")
    public void setModule_account_reports(String  module_account_reports){
        this.module_account_reports = module_account_reports ;
        this.module_account_reportsDirtyFlag = true ;
    }

     /**
     * 获取 [动态报告]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_reportsDirtyFlag(){
        return this.module_account_reportsDirtyFlag ;
    }   

    /**
     * 获取 [催款等级]
     */
    @JsonProperty("module_account_reports_followup")
    public String getModule_account_reports_followup(){
        return this.module_account_reports_followup ;
    }

    /**
     * 设置 [催款等级]
     */
    @JsonProperty("module_account_reports_followup")
    public void setModule_account_reports_followup(String  module_account_reports_followup){
        this.module_account_reports_followup = module_account_reports_followup ;
        this.module_account_reports_followupDirtyFlag = true ;
    }

     /**
     * 获取 [催款等级]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_reports_followupDirtyFlag(){
        return this.module_account_reports_followupDirtyFlag ;
    }   

    /**
     * 获取 [SEPA贷记交易]
     */
    @JsonProperty("module_account_sepa")
    public String getModule_account_sepa(){
        return this.module_account_sepa ;
    }

    /**
     * 设置 [SEPA贷记交易]
     */
    @JsonProperty("module_account_sepa")
    public void setModule_account_sepa(String  module_account_sepa){
        this.module_account_sepa = module_account_sepa ;
        this.module_account_sepaDirtyFlag = true ;
    }

     /**
     * 获取 [SEPA贷记交易]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_sepaDirtyFlag(){
        return this.module_account_sepaDirtyFlag ;
    }   

    /**
     * 获取 [使用SEPA直接计入借方]
     */
    @JsonProperty("module_account_sepa_direct_debit")
    public String getModule_account_sepa_direct_debit(){
        return this.module_account_sepa_direct_debit ;
    }

    /**
     * 设置 [使用SEPA直接计入借方]
     */
    @JsonProperty("module_account_sepa_direct_debit")
    public void setModule_account_sepa_direct_debit(String  module_account_sepa_direct_debit){
        this.module_account_sepa_direct_debit = module_account_sepa_direct_debit ;
        this.module_account_sepa_direct_debitDirtyFlag = true ;
    }

     /**
     * 获取 [使用SEPA直接计入借方]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_sepa_direct_debitDirtyFlag(){
        return this.module_account_sepa_direct_debitDirtyFlag ;
    }   

    /**
     * 获取 [科目税]
     */
    @JsonProperty("module_account_taxcloud")
    public String getModule_account_taxcloud(){
        return this.module_account_taxcloud ;
    }

    /**
     * 设置 [科目税]
     */
    @JsonProperty("module_account_taxcloud")
    public void setModule_account_taxcloud(String  module_account_taxcloud){
        this.module_account_taxcloud = module_account_taxcloud ;
        this.module_account_taxcloudDirtyFlag = true ;
    }

     /**
     * 获取 [科目税]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_taxcloudDirtyFlag(){
        return this.module_account_taxcloudDirtyFlag ;
    }   

    /**
     * 获取 [银行接口－自动同步银行费用]
     */
    @JsonProperty("module_account_yodlee")
    public String getModule_account_yodlee(){
        return this.module_account_yodlee ;
    }

    /**
     * 设置 [银行接口－自动同步银行费用]
     */
    @JsonProperty("module_account_yodlee")
    public void setModule_account_yodlee(String  module_account_yodlee){
        this.module_account_yodlee = module_account_yodlee ;
        this.module_account_yodleeDirtyFlag = true ;
    }

     /**
     * 获取 [银行接口－自动同步银行费用]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_yodleeDirtyFlag(){
        return this.module_account_yodleeDirtyFlag ;
    }   

    /**
     * 获取 [LDAP认证]
     */
    @JsonProperty("module_auth_ldap")
    public String getModule_auth_ldap(){
        return this.module_auth_ldap ;
    }

    /**
     * 设置 [LDAP认证]
     */
    @JsonProperty("module_auth_ldap")
    public void setModule_auth_ldap(String  module_auth_ldap){
        this.module_auth_ldap = module_auth_ldap ;
        this.module_auth_ldapDirtyFlag = true ;
    }

     /**
     * 获取 [LDAP认证]脏标记
     */
    @JsonIgnore
    public boolean getModule_auth_ldapDirtyFlag(){
        return this.module_auth_ldapDirtyFlag ;
    }   

    /**
     * 获取 [使用外部验证提供者 (OAuth)]
     */
    @JsonProperty("module_auth_oauth")
    public String getModule_auth_oauth(){
        return this.module_auth_oauth ;
    }

    /**
     * 设置 [使用外部验证提供者 (OAuth)]
     */
    @JsonProperty("module_auth_oauth")
    public void setModule_auth_oauth(String  module_auth_oauth){
        this.module_auth_oauth = module_auth_oauth ;
        this.module_auth_oauthDirtyFlag = true ;
    }

     /**
     * 获取 [使用外部验证提供者 (OAuth)]脏标记
     */
    @JsonIgnore
    public boolean getModule_auth_oauthDirtyFlag(){
        return this.module_auth_oauthDirtyFlag ;
    }   

    /**
     * 获取 [用Gengo翻译您的网站]
     */
    @JsonProperty("module_base_gengo")
    public String getModule_base_gengo(){
        return this.module_base_gengo ;
    }

    /**
     * 设置 [用Gengo翻译您的网站]
     */
    @JsonProperty("module_base_gengo")
    public void setModule_base_gengo(String  module_base_gengo){
        this.module_base_gengo = module_base_gengo ;
        this.module_base_gengoDirtyFlag = true ;
    }

     /**
     * 获取 [用Gengo翻译您的网站]脏标记
     */
    @JsonIgnore
    public boolean getModule_base_gengoDirtyFlag(){
        return this.module_base_gengoDirtyFlag ;
    }   

    /**
     * 获取 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    @JsonProperty("module_base_import")
    public String getModule_base_import(){
        return this.module_base_import ;
    }

    /**
     * 设置 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    @JsonProperty("module_base_import")
    public void setModule_base_import(String  module_base_import){
        this.module_base_import = module_base_import ;
        this.module_base_importDirtyFlag = true ;
    }

     /**
     * 获取 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]脏标记
     */
    @JsonIgnore
    public boolean getModule_base_importDirtyFlag(){
        return this.module_base_importDirtyFlag ;
    }   

    /**
     * 获取 [号码格式]
     */
    @JsonProperty("module_crm_phone_validation")
    public String getModule_crm_phone_validation(){
        return this.module_crm_phone_validation ;
    }

    /**
     * 设置 [号码格式]
     */
    @JsonProperty("module_crm_phone_validation")
    public void setModule_crm_phone_validation(String  module_crm_phone_validation){
        this.module_crm_phone_validation = module_crm_phone_validation ;
        this.module_crm_phone_validationDirtyFlag = true ;
    }

     /**
     * 获取 [号码格式]脏标记
     */
    @JsonIgnore
    public boolean getModule_crm_phone_validationDirtyFlag(){
        return this.module_crm_phone_validationDirtyFlag ;
    }   

    /**
     * 获取 [从你的网站流量创建线索/商机]
     */
    @JsonProperty("module_crm_reveal")
    public String getModule_crm_reveal(){
        return this.module_crm_reveal ;
    }

    /**
     * 设置 [从你的网站流量创建线索/商机]
     */
    @JsonProperty("module_crm_reveal")
    public void setModule_crm_reveal(String  module_crm_reveal){
        this.module_crm_reveal = module_crm_reveal ;
        this.module_crm_revealDirtyFlag = true ;
    }

     /**
     * 获取 [从你的网站流量创建线索/商机]脏标记
     */
    @JsonIgnore
    public boolean getModule_crm_revealDirtyFlag(){
        return this.module_crm_revealDirtyFlag ;
    }   

    /**
     * 获取 [自动汇率]
     */
    @JsonProperty("module_currency_rate_live")
    public String getModule_currency_rate_live(){
        return this.module_currency_rate_live ;
    }

    /**
     * 设置 [自动汇率]
     */
    @JsonProperty("module_currency_rate_live")
    public void setModule_currency_rate_live(String  module_currency_rate_live){
        this.module_currency_rate_live = module_currency_rate_live ;
        this.module_currency_rate_liveDirtyFlag = true ;
    }

     /**
     * 获取 [自动汇率]脏标记
     */
    @JsonIgnore
    public boolean getModule_currency_rate_liveDirtyFlag(){
        return this.module_currency_rate_liveDirtyFlag ;
    }   

    /**
     * 获取 [运输成本]
     */
    @JsonProperty("module_delivery")
    public String getModule_delivery(){
        return this.module_delivery ;
    }

    /**
     * 设置 [运输成本]
     */
    @JsonProperty("module_delivery")
    public void setModule_delivery(String  module_delivery){
        this.module_delivery = module_delivery ;
        this.module_deliveryDirtyFlag = true ;
    }

     /**
     * 获取 [运输成本]脏标记
     */
    @JsonIgnore
    public boolean getModule_deliveryDirtyFlag(){
        return this.module_deliveryDirtyFlag ;
    }   

    /**
     * 获取 [bpost 接口]
     */
    @JsonProperty("module_delivery_bpost")
    public String getModule_delivery_bpost(){
        return this.module_delivery_bpost ;
    }

    /**
     * 设置 [bpost 接口]
     */
    @JsonProperty("module_delivery_bpost")
    public void setModule_delivery_bpost(String  module_delivery_bpost){
        this.module_delivery_bpost = module_delivery_bpost ;
        this.module_delivery_bpostDirtyFlag = true ;
    }

     /**
     * 获取 [bpost 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_bpostDirtyFlag(){
        return this.module_delivery_bpostDirtyFlag ;
    }   

    /**
     * 获取 [DHL 接口]
     */
    @JsonProperty("module_delivery_dhl")
    public String getModule_delivery_dhl(){
        return this.module_delivery_dhl ;
    }

    /**
     * 设置 [DHL 接口]
     */
    @JsonProperty("module_delivery_dhl")
    public void setModule_delivery_dhl(String  module_delivery_dhl){
        this.module_delivery_dhl = module_delivery_dhl ;
        this.module_delivery_dhlDirtyFlag = true ;
    }

     /**
     * 获取 [DHL 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_dhlDirtyFlag(){
        return this.module_delivery_dhlDirtyFlag ;
    }   

    /**
     * 获取 [Easypost 接口]
     */
    @JsonProperty("module_delivery_easypost")
    public String getModule_delivery_easypost(){
        return this.module_delivery_easypost ;
    }

    /**
     * 设置 [Easypost 接口]
     */
    @JsonProperty("module_delivery_easypost")
    public void setModule_delivery_easypost(String  module_delivery_easypost){
        this.module_delivery_easypost = module_delivery_easypost ;
        this.module_delivery_easypostDirtyFlag = true ;
    }

     /**
     * 获取 [Easypost 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_easypostDirtyFlag(){
        return this.module_delivery_easypostDirtyFlag ;
    }   

    /**
     * 获取 [FedEx 接口]
     */
    @JsonProperty("module_delivery_fedex")
    public String getModule_delivery_fedex(){
        return this.module_delivery_fedex ;
    }

    /**
     * 设置 [FedEx 接口]
     */
    @JsonProperty("module_delivery_fedex")
    public void setModule_delivery_fedex(String  module_delivery_fedex){
        this.module_delivery_fedex = module_delivery_fedex ;
        this.module_delivery_fedexDirtyFlag = true ;
    }

     /**
     * 获取 [FedEx 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_fedexDirtyFlag(){
        return this.module_delivery_fedexDirtyFlag ;
    }   

    /**
     * 获取 [UPS 接口]
     */
    @JsonProperty("module_delivery_ups")
    public String getModule_delivery_ups(){
        return this.module_delivery_ups ;
    }

    /**
     * 设置 [UPS 接口]
     */
    @JsonProperty("module_delivery_ups")
    public void setModule_delivery_ups(String  module_delivery_ups){
        this.module_delivery_ups = module_delivery_ups ;
        this.module_delivery_upsDirtyFlag = true ;
    }

     /**
     * 获取 [UPS 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_upsDirtyFlag(){
        return this.module_delivery_upsDirtyFlag ;
    }   

    /**
     * 获取 [USPS 接口]
     */
    @JsonProperty("module_delivery_usps")
    public String getModule_delivery_usps(){
        return this.module_delivery_usps ;
    }

    /**
     * 设置 [USPS 接口]
     */
    @JsonProperty("module_delivery_usps")
    public void setModule_delivery_usps(String  module_delivery_usps){
        this.module_delivery_usps = module_delivery_usps ;
        this.module_delivery_uspsDirtyFlag = true ;
    }

     /**
     * 获取 [USPS 接口]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_uspsDirtyFlag(){
        return this.module_delivery_uspsDirtyFlag ;
    }   

    /**
     * 获取 [条码]
     */
    @JsonProperty("module_event_barcode")
    public String getModule_event_barcode(){
        return this.module_event_barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("module_event_barcode")
    public void setModule_event_barcode(String  module_event_barcode){
        this.module_event_barcode = module_event_barcode ;
        this.module_event_barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getModule_event_barcodeDirtyFlag(){
        return this.module_event_barcodeDirtyFlag ;
    }   

    /**
     * 获取 [入场券]
     */
    @JsonProperty("module_event_sale")
    public String getModule_event_sale(){
        return this.module_event_sale ;
    }

    /**
     * 设置 [入场券]
     */
    @JsonProperty("module_event_sale")
    public void setModule_event_sale(String  module_event_sale){
        this.module_event_sale = module_event_sale ;
        this.module_event_saleDirtyFlag = true ;
    }

     /**
     * 获取 [入场券]脏标记
     */
    @JsonIgnore
    public boolean getModule_event_saleDirtyFlag(){
        return this.module_event_saleDirtyFlag ;
    }   

    /**
     * 获取 [允许用户同步Google日历]
     */
    @JsonProperty("module_google_calendar")
    public String getModule_google_calendar(){
        return this.module_google_calendar ;
    }

    /**
     * 设置 [允许用户同步Google日历]
     */
    @JsonProperty("module_google_calendar")
    public void setModule_google_calendar(String  module_google_calendar){
        this.module_google_calendar = module_google_calendar ;
        this.module_google_calendarDirtyFlag = true ;
    }

     /**
     * 获取 [允许用户同步Google日历]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_calendarDirtyFlag(){
        return this.module_google_calendarDirtyFlag ;
    }   

    /**
     * 获取 [附加Google文档到记录]
     */
    @JsonProperty("module_google_drive")
    public String getModule_google_drive(){
        return this.module_google_drive ;
    }

    /**
     * 设置 [附加Google文档到记录]
     */
    @JsonProperty("module_google_drive")
    public void setModule_google_drive(String  module_google_drive){
        this.module_google_drive = module_google_drive ;
        this.module_google_driveDirtyFlag = true ;
    }

     /**
     * 获取 [附加Google文档到记录]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_driveDirtyFlag(){
        return this.module_google_driveDirtyFlag ;
    }   

    /**
     * 获取 [Google 电子表格]
     */
    @JsonProperty("module_google_spreadsheet")
    public String getModule_google_spreadsheet(){
        return this.module_google_spreadsheet ;
    }

    /**
     * 设置 [Google 电子表格]
     */
    @JsonProperty("module_google_spreadsheet")
    public void setModule_google_spreadsheet(String  module_google_spreadsheet){
        this.module_google_spreadsheet = module_google_spreadsheet ;
        this.module_google_spreadsheetDirtyFlag = true ;
    }

     /**
     * 获取 [Google 电子表格]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_spreadsheetDirtyFlag(){
        return this.module_google_spreadsheetDirtyFlag ;
    }   

    /**
     * 获取 [显示组织架构图]
     */
    @JsonProperty("module_hr_org_chart")
    public String getModule_hr_org_chart(){
        return this.module_hr_org_chart ;
    }

    /**
     * 设置 [显示组织架构图]
     */
    @JsonProperty("module_hr_org_chart")
    public void setModule_hr_org_chart(String  module_hr_org_chart){
        this.module_hr_org_chart = module_hr_org_chart ;
        this.module_hr_org_chartDirtyFlag = true ;
    }

     /**
     * 获取 [显示组织架构图]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_org_chartDirtyFlag(){
        return this.module_hr_org_chartDirtyFlag ;
    }   

    /**
     * 获取 [面试表单]
     */
    @JsonProperty("module_hr_recruitment_survey")
    public String getModule_hr_recruitment_survey(){
        return this.module_hr_recruitment_survey ;
    }

    /**
     * 设置 [面试表单]
     */
    @JsonProperty("module_hr_recruitment_survey")
    public void setModule_hr_recruitment_survey(String  module_hr_recruitment_survey){
        this.module_hr_recruitment_survey = module_hr_recruitment_survey ;
        this.module_hr_recruitment_surveyDirtyFlag = true ;
    }

     /**
     * 获取 [面试表单]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_recruitment_surveyDirtyFlag(){
        return this.module_hr_recruitment_surveyDirtyFlag ;
    }   

    /**
     * 获取 [任务日志]
     */
    @JsonProperty("module_hr_timesheet")
    public String getModule_hr_timesheet(){
        return this.module_hr_timesheet ;
    }

    /**
     * 设置 [任务日志]
     */
    @JsonProperty("module_hr_timesheet")
    public void setModule_hr_timesheet(String  module_hr_timesheet){
        this.module_hr_timesheet = module_hr_timesheet ;
        this.module_hr_timesheetDirtyFlag = true ;
    }

     /**
     * 获取 [任务日志]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_timesheetDirtyFlag(){
        return this.module_hr_timesheetDirtyFlag ;
    }   

    /**
     * 获取 [管理公司间交易]
     */
    @JsonProperty("module_inter_company_rules")
    public String getModule_inter_company_rules(){
        return this.module_inter_company_rules ;
    }

    /**
     * 设置 [管理公司间交易]
     */
    @JsonProperty("module_inter_company_rules")
    public void setModule_inter_company_rules(String  module_inter_company_rules){
        this.module_inter_company_rules = module_inter_company_rules ;
        this.module_inter_company_rulesDirtyFlag = true ;
    }

     /**
     * 获取 [管理公司间交易]脏标记
     */
    @JsonIgnore
    public boolean getModule_inter_company_rulesDirtyFlag(){
        return this.module_inter_company_rulesDirtyFlag ;
    }   

    /**
     * 获取 [欧盟数字商品增值税]
     */
    @JsonProperty("module_l10n_eu_service")
    public String getModule_l10n_eu_service(){
        return this.module_l10n_eu_service ;
    }

    /**
     * 设置 [欧盟数字商品增值税]
     */
    @JsonProperty("module_l10n_eu_service")
    public void setModule_l10n_eu_service(String  module_l10n_eu_service){
        this.module_l10n_eu_service = module_l10n_eu_service ;
        this.module_l10n_eu_serviceDirtyFlag = true ;
    }

     /**
     * 获取 [欧盟数字商品增值税]脏标记
     */
    @JsonIgnore
    public boolean getModule_l10n_eu_serviceDirtyFlag(){
        return this.module_l10n_eu_serviceDirtyFlag ;
    }   

    /**
     * 获取 [副产品]
     */
    @JsonProperty("module_mrp_byproduct")
    public String getModule_mrp_byproduct(){
        return this.module_mrp_byproduct ;
    }

    /**
     * 设置 [副产品]
     */
    @JsonProperty("module_mrp_byproduct")
    public void setModule_mrp_byproduct(String  module_mrp_byproduct){
        this.module_mrp_byproduct = module_mrp_byproduct ;
        this.module_mrp_byproductDirtyFlag = true ;
    }

     /**
     * 获取 [副产品]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_byproductDirtyFlag(){
        return this.module_mrp_byproductDirtyFlag ;
    }   

    /**
     * 获取 [主生产排程]
     */
    @JsonProperty("module_mrp_mps")
    public String getModule_mrp_mps(){
        return this.module_mrp_mps ;
    }

    /**
     * 设置 [主生产排程]
     */
    @JsonProperty("module_mrp_mps")
    public void setModule_mrp_mps(String  module_mrp_mps){
        this.module_mrp_mps = module_mrp_mps ;
        this.module_mrp_mpsDirtyFlag = true ;
    }

     /**
     * 获取 [主生产排程]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_mpsDirtyFlag(){
        return this.module_mrp_mpsDirtyFlag ;
    }   

    /**
     * 获取 [产品生命周期管理 (PLM)]
     */
    @JsonProperty("module_mrp_plm")
    public String getModule_mrp_plm(){
        return this.module_mrp_plm ;
    }

    /**
     * 设置 [产品生命周期管理 (PLM)]
     */
    @JsonProperty("module_mrp_plm")
    public void setModule_mrp_plm(String  module_mrp_plm){
        this.module_mrp_plm = module_mrp_plm ;
        this.module_mrp_plmDirtyFlag = true ;
    }

     /**
     * 获取 [产品生命周期管理 (PLM)]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_plmDirtyFlag(){
        return this.module_mrp_plmDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("module_mrp_workorder")
    public String getModule_mrp_workorder(){
        return this.module_mrp_workorder ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("module_mrp_workorder")
    public void setModule_mrp_workorder(String  module_mrp_workorder){
        this.module_mrp_workorder = module_mrp_workorder ;
        this.module_mrp_workorderDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_workorderDirtyFlag(){
        return this.module_mrp_workorderDirtyFlag ;
    }   

    /**
     * 获取 [协作pad]
     */
    @JsonProperty("module_pad")
    public String getModule_pad(){
        return this.module_pad ;
    }

    /**
     * 设置 [协作pad]
     */
    @JsonProperty("module_pad")
    public void setModule_pad(String  module_pad){
        this.module_pad = module_pad ;
        this.module_padDirtyFlag = true ;
    }

     /**
     * 获取 [协作pad]脏标记
     */
    @JsonIgnore
    public boolean getModule_padDirtyFlag(){
        return this.module_padDirtyFlag ;
    }   

    /**
     * 获取 [自动填充公司数据]
     */
    @JsonProperty("module_partner_autocomplete")
    public String getModule_partner_autocomplete(){
        return this.module_partner_autocomplete ;
    }

    /**
     * 设置 [自动填充公司数据]
     */
    @JsonProperty("module_partner_autocomplete")
    public void setModule_partner_autocomplete(String  module_partner_autocomplete){
        this.module_partner_autocomplete = module_partner_autocomplete ;
        this.module_partner_autocompleteDirtyFlag = true ;
    }

     /**
     * 获取 [自动填充公司数据]脏标记
     */
    @JsonIgnore
    public boolean getModule_partner_autocompleteDirtyFlag(){
        return this.module_partner_autocompleteDirtyFlag ;
    }   

    /**
     * 获取 [集成卡支付]
     */
    @JsonProperty("module_pos_mercury")
    public String getModule_pos_mercury(){
        return this.module_pos_mercury ;
    }

    /**
     * 设置 [集成卡支付]
     */
    @JsonProperty("module_pos_mercury")
    public void setModule_pos_mercury(String  module_pos_mercury){
        this.module_pos_mercury = module_pos_mercury ;
        this.module_pos_mercuryDirtyFlag = true ;
    }

     /**
     * 获取 [集成卡支付]脏标记
     */
    @JsonIgnore
    public boolean getModule_pos_mercuryDirtyFlag(){
        return this.module_pos_mercuryDirtyFlag ;
    }   

    /**
     * 获取 [保留]
     */
    @JsonProperty("module_procurement_jit")
    public String getModule_procurement_jit(){
        return this.module_procurement_jit ;
    }

    /**
     * 设置 [保留]
     */
    @JsonProperty("module_procurement_jit")
    public void setModule_procurement_jit(String  module_procurement_jit){
        this.module_procurement_jit = module_procurement_jit ;
        this.module_procurement_jitDirtyFlag = true ;
    }

     /**
     * 获取 [保留]脏标记
     */
    @JsonIgnore
    public boolean getModule_procurement_jitDirtyFlag(){
        return this.module_procurement_jitDirtyFlag ;
    }   

    /**
     * 获取 [特定的EMail]
     */
    @JsonProperty("module_product_email_template")
    public String getModule_product_email_template(){
        return this.module_product_email_template ;
    }

    /**
     * 设置 [特定的EMail]
     */
    @JsonProperty("module_product_email_template")
    public void setModule_product_email_template(String  module_product_email_template){
        this.module_product_email_template = module_product_email_template ;
        this.module_product_email_templateDirtyFlag = true ;
    }

     /**
     * 获取 [特定的EMail]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_email_templateDirtyFlag(){
        return this.module_product_email_templateDirtyFlag ;
    }   

    /**
     * 获取 [到期日]
     */
    @JsonProperty("module_product_expiry")
    public String getModule_product_expiry(){
        return this.module_product_expiry ;
    }

    /**
     * 设置 [到期日]
     */
    @JsonProperty("module_product_expiry")
    public void setModule_product_expiry(String  module_product_expiry){
        this.module_product_expiry = module_product_expiry ;
        this.module_product_expiryDirtyFlag = true ;
    }

     /**
     * 获取 [到期日]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_expiryDirtyFlag(){
        return this.module_product_expiryDirtyFlag ;
    }   

    /**
     * 获取 [允许产品毛利]
     */
    @JsonProperty("module_product_margin")
    public String getModule_product_margin(){
        return this.module_product_margin ;
    }

    /**
     * 设置 [允许产品毛利]
     */
    @JsonProperty("module_product_margin")
    public void setModule_product_margin(String  module_product_margin){
        this.module_product_margin = module_product_margin ;
        this.module_product_marginDirtyFlag = true ;
    }

     /**
     * 获取 [允许产品毛利]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_marginDirtyFlag(){
        return this.module_product_marginDirtyFlag ;
    }   

    /**
     * 获取 [预测]
     */
    @JsonProperty("module_project_forecast")
    public String getModule_project_forecast(){
        return this.module_project_forecast ;
    }

    /**
     * 设置 [预测]
     */
    @JsonProperty("module_project_forecast")
    public void setModule_project_forecast(String  module_project_forecast){
        this.module_project_forecast = module_project_forecast ;
        this.module_project_forecastDirtyFlag = true ;
    }

     /**
     * 获取 [预测]脏标记
     */
    @JsonIgnore
    public boolean getModule_project_forecastDirtyFlag(){
        return this.module_project_forecastDirtyFlag ;
    }   

    /**
     * 获取 [采购招标]
     */
    @JsonProperty("module_purchase_requisition")
    public String getModule_purchase_requisition(){
        return this.module_purchase_requisition ;
    }

    /**
     * 设置 [采购招标]
     */
    @JsonProperty("module_purchase_requisition")
    public void setModule_purchase_requisition(String  module_purchase_requisition){
        this.module_purchase_requisition = module_purchase_requisition ;
        this.module_purchase_requisitionDirtyFlag = true ;
    }

     /**
     * 获取 [采购招标]脏标记
     */
    @JsonIgnore
    public boolean getModule_purchase_requisitionDirtyFlag(){
        return this.module_purchase_requisitionDirtyFlag ;
    }   

    /**
     * 获取 [质量]
     */
    @JsonProperty("module_quality_control")
    public String getModule_quality_control(){
        return this.module_quality_control ;
    }

    /**
     * 设置 [质量]
     */
    @JsonProperty("module_quality_control")
    public void setModule_quality_control(String  module_quality_control){
        this.module_quality_control = module_quality_control ;
        this.module_quality_controlDirtyFlag = true ;
    }

     /**
     * 获取 [质量]脏标记
     */
    @JsonIgnore
    public boolean getModule_quality_controlDirtyFlag(){
        return this.module_quality_controlDirtyFlag ;
    }   

    /**
     * 获取 [优惠券和促销]
     */
    @JsonProperty("module_sale_coupon")
    public String getModule_sale_coupon(){
        return this.module_sale_coupon ;
    }

    /**
     * 设置 [优惠券和促销]
     */
    @JsonProperty("module_sale_coupon")
    public void setModule_sale_coupon(String  module_sale_coupon){
        this.module_sale_coupon = module_sale_coupon ;
        this.module_sale_couponDirtyFlag = true ;
    }

     /**
     * 获取 [优惠券和促销]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_couponDirtyFlag(){
        return this.module_sale_couponDirtyFlag ;
    }   

    /**
     * 获取 [毛利]
     */
    @JsonProperty("module_sale_margin")
    public String getModule_sale_margin(){
        return this.module_sale_margin ;
    }

    /**
     * 设置 [毛利]
     */
    @JsonProperty("module_sale_margin")
    public void setModule_sale_margin(String  module_sale_margin){
        this.module_sale_margin = module_sale_margin ;
        this.module_sale_marginDirtyFlag = true ;
    }

     /**
     * 获取 [毛利]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_marginDirtyFlag(){
        return this.module_sale_marginDirtyFlag ;
    }   

    /**
     * 获取 [报价单生成器]
     */
    @JsonProperty("module_sale_quotation_builder")
    public String getModule_sale_quotation_builder(){
        return this.module_sale_quotation_builder ;
    }

    /**
     * 设置 [报价单生成器]
     */
    @JsonProperty("module_sale_quotation_builder")
    public void setModule_sale_quotation_builder(String  module_sale_quotation_builder){
        this.module_sale_quotation_builder = module_sale_quotation_builder ;
        this.module_sale_quotation_builderDirtyFlag = true ;
    }

     /**
     * 获取 [报价单生成器]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_quotation_builderDirtyFlag(){
        return this.module_sale_quotation_builderDirtyFlag ;
    }   

    /**
     * 获取 [条码扫描器]
     */
    @JsonProperty("module_stock_barcode")
    public String getModule_stock_barcode(){
        return this.module_stock_barcode ;
    }

    /**
     * 设置 [条码扫描器]
     */
    @JsonProperty("module_stock_barcode")
    public void setModule_stock_barcode(String  module_stock_barcode){
        this.module_stock_barcode = module_stock_barcode ;
        this.module_stock_barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [条码扫描器]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_barcodeDirtyFlag(){
        return this.module_stock_barcodeDirtyFlag ;
    }   

    /**
     * 获取 [代发货]
     */
    @JsonProperty("module_stock_dropshipping")
    public String getModule_stock_dropshipping(){
        return this.module_stock_dropshipping ;
    }

    /**
     * 设置 [代发货]
     */
    @JsonProperty("module_stock_dropshipping")
    public void setModule_stock_dropshipping(String  module_stock_dropshipping){
        this.module_stock_dropshipping = module_stock_dropshipping ;
        this.module_stock_dropshippingDirtyFlag = true ;
    }

     /**
     * 获取 [代发货]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_dropshippingDirtyFlag(){
        return this.module_stock_dropshippingDirtyFlag ;
    }   

    /**
     * 获取 [到岸成本]
     */
    @JsonProperty("module_stock_landed_costs")
    public String getModule_stock_landed_costs(){
        return this.module_stock_landed_costs ;
    }

    /**
     * 设置 [到岸成本]
     */
    @JsonProperty("module_stock_landed_costs")
    public void setModule_stock_landed_costs(String  module_stock_landed_costs){
        this.module_stock_landed_costs = module_stock_landed_costs ;
        this.module_stock_landed_costsDirtyFlag = true ;
    }

     /**
     * 获取 [到岸成本]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_landed_costsDirtyFlag(){
        return this.module_stock_landed_costsDirtyFlag ;
    }   

    /**
     * 获取 [批量拣货]
     */
    @JsonProperty("module_stock_picking_batch")
    public String getModule_stock_picking_batch(){
        return this.module_stock_picking_batch ;
    }

    /**
     * 设置 [批量拣货]
     */
    @JsonProperty("module_stock_picking_batch")
    public void setModule_stock_picking_batch(String  module_stock_picking_batch){
        this.module_stock_picking_batch = module_stock_picking_batch ;
        this.module_stock_picking_batchDirtyFlag = true ;
    }

     /**
     * 获取 [批量拣货]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_picking_batchDirtyFlag(){
        return this.module_stock_picking_batchDirtyFlag ;
    }   

    /**
     * 获取 [Asterisk (开源VoIP平台)]
     */
    @JsonProperty("module_voip")
    public String getModule_voip(){
        return this.module_voip ;
    }

    /**
     * 设置 [Asterisk (开源VoIP平台)]
     */
    @JsonProperty("module_voip")
    public void setModule_voip(String  module_voip){
        this.module_voip = module_voip ;
        this.module_voipDirtyFlag = true ;
    }

     /**
     * 获取 [Asterisk (开源VoIP平台)]脏标记
     */
    @JsonIgnore
    public boolean getModule_voipDirtyFlag(){
        return this.module_voipDirtyFlag ;
    }   

    /**
     * 获取 [调查登记]
     */
    @JsonProperty("module_website_event_questions")
    public String getModule_website_event_questions(){
        return this.module_website_event_questions ;
    }

    /**
     * 设置 [调查登记]
     */
    @JsonProperty("module_website_event_questions")
    public void setModule_website_event_questions(String  module_website_event_questions){
        this.module_website_event_questions = module_website_event_questions ;
        this.module_website_event_questionsDirtyFlag = true ;
    }

     /**
     * 获取 [调查登记]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_questionsDirtyFlag(){
        return this.module_website_event_questionsDirtyFlag ;
    }   

    /**
     * 获取 [在线票务]
     */
    @JsonProperty("module_website_event_sale")
    public String getModule_website_event_sale(){
        return this.module_website_event_sale ;
    }

    /**
     * 设置 [在线票务]
     */
    @JsonProperty("module_website_event_sale")
    public void setModule_website_event_sale(String  module_website_event_sale){
        this.module_website_event_sale = module_website_event_sale ;
        this.module_website_event_saleDirtyFlag = true ;
    }

     /**
     * 获取 [在线票务]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_saleDirtyFlag(){
        return this.module_website_event_saleDirtyFlag ;
    }   

    /**
     * 获取 [耿宗并计划]
     */
    @JsonProperty("module_website_event_track")
    public String getModule_website_event_track(){
        return this.module_website_event_track ;
    }

    /**
     * 设置 [耿宗并计划]
     */
    @JsonProperty("module_website_event_track")
    public void setModule_website_event_track(String  module_website_event_track){
        this.module_website_event_track = module_website_event_track ;
        this.module_website_event_trackDirtyFlag = true ;
    }

     /**
     * 获取 [耿宗并计划]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_trackDirtyFlag(){
        return this.module_website_event_trackDirtyFlag ;
    }   

    /**
     * 获取 [在线发布]
     */
    @JsonProperty("module_website_hr_recruitment")
    public String getModule_website_hr_recruitment(){
        return this.module_website_hr_recruitment ;
    }

    /**
     * 设置 [在线发布]
     */
    @JsonProperty("module_website_hr_recruitment")
    public void setModule_website_hr_recruitment(String  module_website_hr_recruitment){
        this.module_website_hr_recruitment = module_website_hr_recruitment ;
        this.module_website_hr_recruitmentDirtyFlag = true ;
    }

     /**
     * 获取 [在线发布]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_hr_recruitmentDirtyFlag(){
        return this.module_website_hr_recruitmentDirtyFlag ;
    }   

    /**
     * 获取 [链接跟踪器]
     */
    @JsonProperty("module_website_links")
    public String getModule_website_links(){
        return this.module_website_links ;
    }

    /**
     * 设置 [链接跟踪器]
     */
    @JsonProperty("module_website_links")
    public void setModule_website_links(String  module_website_links){
        this.module_website_links = module_website_links ;
        this.module_website_linksDirtyFlag = true ;
    }

     /**
     * 获取 [链接跟踪器]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_linksDirtyFlag(){
        return this.module_website_linksDirtyFlag ;
    }   

    /**
     * 获取 [产品比较工具]
     */
    @JsonProperty("module_website_sale_comparison")
    public String getModule_website_sale_comparison(){
        return this.module_website_sale_comparison ;
    }

    /**
     * 设置 [产品比较工具]
     */
    @JsonProperty("module_website_sale_comparison")
    public void setModule_website_sale_comparison(String  module_website_sale_comparison){
        this.module_website_sale_comparison = module_website_sale_comparison ;
        this.module_website_sale_comparisonDirtyFlag = true ;
    }

     /**
     * 获取 [产品比较工具]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_comparisonDirtyFlag(){
        return this.module_website_sale_comparisonDirtyFlag ;
    }   

    /**
     * 获取 [电商物流成本]
     */
    @JsonProperty("module_website_sale_delivery")
    public String getModule_website_sale_delivery(){
        return this.module_website_sale_delivery ;
    }

    /**
     * 设置 [电商物流成本]
     */
    @JsonProperty("module_website_sale_delivery")
    public void setModule_website_sale_delivery(String  module_website_sale_delivery){
        this.module_website_sale_delivery = module_website_sale_delivery ;
        this.module_website_sale_deliveryDirtyFlag = true ;
    }

     /**
     * 获取 [电商物流成本]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_deliveryDirtyFlag(){
        return this.module_website_sale_deliveryDirtyFlag ;
    }   

    /**
     * 获取 [数字内容]
     */
    @JsonProperty("module_website_sale_digital")
    public String getModule_website_sale_digital(){
        return this.module_website_sale_digital ;
    }

    /**
     * 设置 [数字内容]
     */
    @JsonProperty("module_website_sale_digital")
    public void setModule_website_sale_digital(String  module_website_sale_digital){
        this.module_website_sale_digital = module_website_sale_digital ;
        this.module_website_sale_digitalDirtyFlag = true ;
    }

     /**
     * 获取 [数字内容]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_digitalDirtyFlag(){
        return this.module_website_sale_digitalDirtyFlag ;
    }   

    /**
     * 获取 [库存]
     */
    @JsonProperty("module_website_sale_stock")
    public String getModule_website_sale_stock(){
        return this.module_website_sale_stock ;
    }

    /**
     * 设置 [库存]
     */
    @JsonProperty("module_website_sale_stock")
    public void setModule_website_sale_stock(String  module_website_sale_stock){
        this.module_website_sale_stock = module_website_sale_stock ;
        this.module_website_sale_stockDirtyFlag = true ;
    }

     /**
     * 获取 [库存]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_stockDirtyFlag(){
        return this.module_website_sale_stockDirtyFlag ;
    }   

    /**
     * 获取 [心愿单]
     */
    @JsonProperty("module_website_sale_wishlist")
    public String getModule_website_sale_wishlist(){
        return this.module_website_sale_wishlist ;
    }

    /**
     * 设置 [心愿单]
     */
    @JsonProperty("module_website_sale_wishlist")
    public void setModule_website_sale_wishlist(String  module_website_sale_wishlist){
        this.module_website_sale_wishlist = module_website_sale_wishlist ;
        this.module_website_sale_wishlistDirtyFlag = true ;
    }

     /**
     * 获取 [心愿单]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_wishlistDirtyFlag(){
        return this.module_website_sale_wishlistDirtyFlag ;
    }   

    /**
     * 获取 [A / B测试]
     */
    @JsonProperty("module_website_version")
    public String getModule_website_version(){
        return this.module_website_version ;
    }

    /**
     * 设置 [A / B测试]
     */
    @JsonProperty("module_website_version")
    public void setModule_website_version(String  module_website_version){
        this.module_website_version = module_website_version ;
        this.module_website_versionDirtyFlag = true ;
    }

     /**
     * 获取 [A / B测试]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_versionDirtyFlag(){
        return this.module_website_versionDirtyFlag ;
    }   

    /**
     * 获取 [Unsplash图像库]
     */
    @JsonProperty("module_web_unsplash")
    public String getModule_web_unsplash(){
        return this.module_web_unsplash ;
    }

    /**
     * 设置 [Unsplash图像库]
     */
    @JsonProperty("module_web_unsplash")
    public void setModule_web_unsplash(String  module_web_unsplash){
        this.module_web_unsplash = module_web_unsplash ;
        this.module_web_unsplashDirtyFlag = true ;
    }

     /**
     * 获取 [Unsplash图像库]脏标记
     */
    @JsonIgnore
    public boolean getModule_web_unsplashDirtyFlag(){
        return this.module_web_unsplashDirtyFlag ;
    }   

    /**
     * 获取 [每个产品的多种销售价格]
     */
    @JsonProperty("multi_sales_price")
    public String getMulti_sales_price(){
        return this.multi_sales_price ;
    }

    /**
     * 设置 [每个产品的多种销售价格]
     */
    @JsonProperty("multi_sales_price")
    public void setMulti_sales_price(String  multi_sales_price){
        this.multi_sales_price = multi_sales_price ;
        this.multi_sales_priceDirtyFlag = true ;
    }

     /**
     * 获取 [每个产品的多种销售价格]脏标记
     */
    @JsonIgnore
    public boolean getMulti_sales_priceDirtyFlag(){
        return this.multi_sales_priceDirtyFlag ;
    }   

    /**
     * 获取 [计价方法]
     */
    @JsonProperty("multi_sales_price_method")
    public String getMulti_sales_price_method(){
        return this.multi_sales_price_method ;
    }

    /**
     * 设置 [计价方法]
     */
    @JsonProperty("multi_sales_price_method")
    public void setMulti_sales_price_method(String  multi_sales_price_method){
        this.multi_sales_price_method = multi_sales_price_method ;
        this.multi_sales_price_methodDirtyFlag = true ;
    }

     /**
     * 获取 [计价方法]脏标记
     */
    @JsonIgnore
    public boolean getMulti_sales_price_methodDirtyFlag(){
        return this.multi_sales_price_methodDirtyFlag ;
    }   

    /**
     * 获取 [纸张格式]
     */
    @JsonProperty("paperformat_id")
    public Integer getPaperformat_id(){
        return this.paperformat_id ;
    }

    /**
     * 设置 [纸张格式]
     */
    @JsonProperty("paperformat_id")
    public void setPaperformat_id(Integer  paperformat_id){
        this.paperformat_id = paperformat_id ;
        this.paperformat_idDirtyFlag = true ;
    }

     /**
     * 获取 [纸张格式]脏标记
     */
    @JsonIgnore
    public boolean getPaperformat_idDirtyFlag(){
        return this.paperformat_idDirtyFlag ;
    }   

    /**
     * 获取 [信用不足]
     */
    @JsonProperty("partner_autocomplete_insufficient_credit")
    public String getPartner_autocomplete_insufficient_credit(){
        return this.partner_autocomplete_insufficient_credit ;
    }

    /**
     * 设置 [信用不足]
     */
    @JsonProperty("partner_autocomplete_insufficient_credit")
    public void setPartner_autocomplete_insufficient_credit(String  partner_autocomplete_insufficient_credit){
        this.partner_autocomplete_insufficient_credit = partner_autocomplete_insufficient_credit ;
        this.partner_autocomplete_insufficient_creditDirtyFlag = true ;
    }

     /**
     * 获取 [信用不足]脏标记
     */
    @JsonIgnore
    public boolean getPartner_autocomplete_insufficient_creditDirtyFlag(){
        return this.partner_autocomplete_insufficient_creditDirtyFlag ;
    }   

    /**
     * 获取 [在线支付]
     */
    @JsonProperty("portal_confirmation_pay")
    public String getPortal_confirmation_pay(){
        return this.portal_confirmation_pay ;
    }

    /**
     * 设置 [在线支付]
     */
    @JsonProperty("portal_confirmation_pay")
    public void setPortal_confirmation_pay(String  portal_confirmation_pay){
        this.portal_confirmation_pay = portal_confirmation_pay ;
        this.portal_confirmation_payDirtyFlag = true ;
    }

     /**
     * 获取 [在线支付]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_payDirtyFlag(){
        return this.portal_confirmation_payDirtyFlag ;
    }   

    /**
     * 获取 [在线签名]
     */
    @JsonProperty("portal_confirmation_sign")
    public String getPortal_confirmation_sign(){
        return this.portal_confirmation_sign ;
    }

    /**
     * 设置 [在线签名]
     */
    @JsonProperty("portal_confirmation_sign")
    public void setPortal_confirmation_sign(String  portal_confirmation_sign){
        this.portal_confirmation_sign = portal_confirmation_sign ;
        this.portal_confirmation_signDirtyFlag = true ;
    }

     /**
     * 获取 [在线签名]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_signDirtyFlag(){
        return this.portal_confirmation_signDirtyFlag ;
    }   

    /**
     * 获取 [POS价格表]
     */
    @JsonProperty("pos_pricelist_setting")
    public String getPos_pricelist_setting(){
        return this.pos_pricelist_setting ;
    }

    /**
     * 设置 [POS价格表]
     */
    @JsonProperty("pos_pricelist_setting")
    public void setPos_pricelist_setting(String  pos_pricelist_setting){
        this.pos_pricelist_setting = pos_pricelist_setting ;
        this.pos_pricelist_settingDirtyFlag = true ;
    }

     /**
     * 获取 [POS价格表]脏标记
     */
    @JsonIgnore
    public boolean getPos_pricelist_settingDirtyFlag(){
        return this.pos_pricelist_settingDirtyFlag ;
    }   

    /**
     * 获取 [产品复价]
     */
    @JsonProperty("pos_sales_price")
    public String getPos_sales_price(){
        return this.pos_sales_price ;
    }

    /**
     * 设置 [产品复价]
     */
    @JsonProperty("pos_sales_price")
    public void setPos_sales_price(String  pos_sales_price){
        this.pos_sales_price = pos_sales_price ;
        this.pos_sales_priceDirtyFlag = true ;
    }

     /**
     * 获取 [产品复价]脏标记
     */
    @JsonIgnore
    public boolean getPos_sales_priceDirtyFlag(){
        return this.pos_sales_priceDirtyFlag ;
    }   

    /**
     * 获取 [审批层级 *]
     */
    @JsonProperty("po_double_validation")
    public String getPo_double_validation(){
        return this.po_double_validation ;
    }

    /**
     * 设置 [审批层级 *]
     */
    @JsonProperty("po_double_validation")
    public void setPo_double_validation(String  po_double_validation){
        this.po_double_validation = po_double_validation ;
        this.po_double_validationDirtyFlag = true ;
    }

     /**
     * 获取 [审批层级 *]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validationDirtyFlag(){
        return this.po_double_validationDirtyFlag ;
    }   

    /**
     * 获取 [最小金额]
     */
    @JsonProperty("po_double_validation_amount")
    public Double getPo_double_validation_amount(){
        return this.po_double_validation_amount ;
    }

    /**
     * 设置 [最小金额]
     */
    @JsonProperty("po_double_validation_amount")
    public void setPo_double_validation_amount(Double  po_double_validation_amount){
        this.po_double_validation_amount = po_double_validation_amount ;
        this.po_double_validation_amountDirtyFlag = true ;
    }

     /**
     * 获取 [最小金额]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validation_amountDirtyFlag(){
        return this.po_double_validation_amountDirtyFlag ;
    }   

    /**
     * 获取 [采购提前时间]
     */
    @JsonProperty("po_lead")
    public Double getPo_lead(){
        return this.po_lead ;
    }

    /**
     * 设置 [采购提前时间]
     */
    @JsonProperty("po_lead")
    public void setPo_lead(Double  po_lead){
        this.po_lead = po_lead ;
        this.po_leadDirtyFlag = true ;
    }

     /**
     * 获取 [采购提前时间]脏标记
     */
    @JsonIgnore
    public boolean getPo_leadDirtyFlag(){
        return this.po_leadDirtyFlag ;
    }   

    /**
     * 获取 [采购订单修改 *]
     */
    @JsonProperty("po_lock")
    public String getPo_lock(){
        return this.po_lock ;
    }

    /**
     * 设置 [采购订单修改 *]
     */
    @JsonProperty("po_lock")
    public void setPo_lock(String  po_lock){
        this.po_lock = po_lock ;
        this.po_lockDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单修改 *]脏标记
     */
    @JsonIgnore
    public boolean getPo_lockDirtyFlag(){
        return this.po_lockDirtyFlag ;
    }   

    /**
     * 获取 [采购订单批准]
     */
    @JsonProperty("po_order_approval")
    public String getPo_order_approval(){
        return this.po_order_approval ;
    }

    /**
     * 设置 [采购订单批准]
     */
    @JsonProperty("po_order_approval")
    public void setPo_order_approval(String  po_order_approval){
        this.po_order_approval = po_order_approval ;
        this.po_order_approvalDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单批准]脏标记
     */
    @JsonIgnore
    public boolean getPo_order_approvalDirtyFlag(){
        return this.po_order_approvalDirtyFlag ;
    }   

    /**
     * 获取 [重量单位]
     */
    @JsonProperty("product_weight_in_lbs")
    public String getProduct_weight_in_lbs(){
        return this.product_weight_in_lbs ;
    }

    /**
     * 设置 [重量单位]
     */
    @JsonProperty("product_weight_in_lbs")
    public void setProduct_weight_in_lbs(String  product_weight_in_lbs){
        this.product_weight_in_lbs = product_weight_in_lbs ;
        this.product_weight_in_lbsDirtyFlag = true ;
    }

     /**
     * 获取 [重量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_weight_in_lbsDirtyFlag(){
        return this.product_weight_in_lbsDirtyFlag ;
    }   

    /**
     * 获取 [传播的最小差值]
     */
    @JsonProperty("propagation_minimum_delta")
    public Integer getPropagation_minimum_delta(){
        return this.propagation_minimum_delta ;
    }

    /**
     * 设置 [传播的最小差值]
     */
    @JsonProperty("propagation_minimum_delta")
    public void setPropagation_minimum_delta(Integer  propagation_minimum_delta){
        this.propagation_minimum_delta = propagation_minimum_delta ;
        this.propagation_minimum_deltaDirtyFlag = true ;
    }

     /**
     * 获取 [传播的最小差值]脏标记
     */
    @JsonIgnore
    public boolean getPropagation_minimum_deltaDirtyFlag(){
        return this.propagation_minimum_deltaDirtyFlag ;
    }   

    /**
     * 获取 [默认进项税]
     */
    @JsonProperty("purchase_tax_id")
    public Integer getPurchase_tax_id(){
        return this.purchase_tax_id ;
    }

    /**
     * 设置 [默认进项税]
     */
    @JsonProperty("purchase_tax_id")
    public void setPurchase_tax_id(Integer  purchase_tax_id){
        this.purchase_tax_id = purchase_tax_id ;
        this.purchase_tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认进项税]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_tax_idDirtyFlag(){
        return this.purchase_tax_idDirtyFlag ;
    }   

    /**
     * 获取 [显示SEPA QR码]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return this.qr_code ;
    }

    /**
     * 设置 [显示SEPA QR码]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

     /**
     * 获取 [显示SEPA QR码]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return this.qr_codeDirtyFlag ;
    }   

    /**
     * 获取 [默认报价有效期（日）]
     */
    @JsonProperty("quotation_validity_days")
    public Integer getQuotation_validity_days(){
        return this.quotation_validity_days ;
    }

    /**
     * 设置 [默认报价有效期（日）]
     */
    @JsonProperty("quotation_validity_days")
    public void setQuotation_validity_days(Integer  quotation_validity_days){
        this.quotation_validity_days = quotation_validity_days ;
        this.quotation_validity_daysDirtyFlag = true ;
    }

     /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_validity_daysDirtyFlag(){
        return this.quotation_validity_daysDirtyFlag ;
    }   

    /**
     * 获取 [自定义报表页脚]
     */
    @JsonProperty("report_footer")
    public String getReport_footer(){
        return this.report_footer ;
    }

    /**
     * 设置 [自定义报表页脚]
     */
    @JsonProperty("report_footer")
    public void setReport_footer(String  report_footer){
        this.report_footer = report_footer ;
        this.report_footerDirtyFlag = true ;
    }

     /**
     * 获取 [自定义报表页脚]脏标记
     */
    @JsonIgnore
    public boolean getReport_footerDirtyFlag(){
        return this.report_footerDirtyFlag ;
    }   

    /**
     * 获取 [公司的上班时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [公司的上班时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司的上班时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("salesperson_id")
    public Integer getSalesperson_id(){
        return this.salesperson_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("salesperson_id")
    public void setSalesperson_id(Integer  salesperson_id){
        this.salesperson_id = salesperson_id ;
        this.salesperson_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getSalesperson_idDirtyFlag(){
        return this.salesperson_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("salesteam_id")
    public Integer getSalesteam_id(){
        return this.salesteam_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("salesteam_id")
    public void setSalesteam_id(Integer  salesteam_id){
        this.salesteam_id = salesteam_id ;
        this.salesteam_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getSalesteam_idDirtyFlag(){
        return this.salesteam_idDirtyFlag ;
    }   

    /**
     * 获取 [送货管理]
     */
    @JsonProperty("sale_delivery_settings")
    public String getSale_delivery_settings(){
        return this.sale_delivery_settings ;
    }

    /**
     * 设置 [送货管理]
     */
    @JsonProperty("sale_delivery_settings")
    public void setSale_delivery_settings(String  sale_delivery_settings){
        this.sale_delivery_settings = sale_delivery_settings ;
        this.sale_delivery_settingsDirtyFlag = true ;
    }

     /**
     * 获取 [送货管理]脏标记
     */
    @JsonIgnore
    public boolean getSale_delivery_settingsDirtyFlag(){
        return this.sale_delivery_settingsDirtyFlag ;
    }   

    /**
     * 获取 [条款及条件]
     */
    @JsonProperty("sale_note")
    public String getSale_note(){
        return this.sale_note ;
    }

    /**
     * 设置 [条款及条件]
     */
    @JsonProperty("sale_note")
    public void setSale_note(String  sale_note){
        this.sale_note = sale_note ;
        this.sale_noteDirtyFlag = true ;
    }

     /**
     * 获取 [条款及条件]脏标记
     */
    @JsonIgnore
    public boolean getSale_noteDirtyFlag(){
        return this.sale_noteDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("sale_pricelist_setting")
    public String getSale_pricelist_setting(){
        return this.sale_pricelist_setting ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("sale_pricelist_setting")
    public void setSale_pricelist_setting(String  sale_pricelist_setting){
        this.sale_pricelist_setting = sale_pricelist_setting ;
        this.sale_pricelist_settingDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getSale_pricelist_settingDirtyFlag(){
        return this.sale_pricelist_settingDirtyFlag ;
    }   

    /**
     * 获取 [默认销售税]
     */
    @JsonProperty("sale_tax_id")
    public Integer getSale_tax_id(){
        return this.sale_tax_id ;
    }

    /**
     * 设置 [默认销售税]
     */
    @JsonProperty("sale_tax_id")
    public void setSale_tax_id(Integer  sale_tax_id){
        this.sale_tax_id = sale_tax_id ;
        this.sale_tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认销售税]脏标记
     */
    @JsonIgnore
    public boolean getSale_tax_idDirtyFlag(){
        return this.sale_tax_idDirtyFlag ;
    }   

    /**
     * 获取 [安全时间]
     */
    @JsonProperty("security_lead")
    public Double getSecurity_lead(){
        return this.security_lead ;
    }

    /**
     * 设置 [安全时间]
     */
    @JsonProperty("security_lead")
    public void setSecurity_lead(Double  security_lead){
        this.security_lead = security_lead ;
        this.security_leadDirtyFlag = true ;
    }

     /**
     * 获取 [安全时间]脏标记
     */
    @JsonIgnore
    public boolean getSecurity_leadDirtyFlag(){
        return this.security_leadDirtyFlag ;
    }   

    /**
     * 获取 [在取消订阅页面上显示黑名单按钮]
     */
    @JsonProperty("show_blacklist_buttons")
    public String getShow_blacklist_buttons(){
        return this.show_blacklist_buttons ;
    }

    /**
     * 设置 [在取消订阅页面上显示黑名单按钮]
     */
    @JsonProperty("show_blacklist_buttons")
    public void setShow_blacklist_buttons(String  show_blacklist_buttons){
        this.show_blacklist_buttons = show_blacklist_buttons ;
        this.show_blacklist_buttonsDirtyFlag = true ;
    }

     /**
     * 获取 [在取消订阅页面上显示黑名单按钮]脏标记
     */
    @JsonIgnore
    public boolean getShow_blacklist_buttonsDirtyFlag(){
        return this.show_blacklist_buttonsDirtyFlag ;
    }   

    /**
     * 获取 [显示效果]
     */
    @JsonProperty("show_effect")
    public String getShow_effect(){
        return this.show_effect ;
    }

    /**
     * 设置 [显示效果]
     */
    @JsonProperty("show_effect")
    public void setShow_effect(String  show_effect){
        this.show_effect = show_effect ;
        this.show_effectDirtyFlag = true ;
    }

     /**
     * 获取 [显示效果]脏标记
     */
    @JsonIgnore
    public boolean getShow_effectDirtyFlag(){
        return this.show_effectDirtyFlag ;
    }   

    /**
     * 获取 [税目汇总表]
     */
    @JsonProperty("show_line_subtotals_tax_selection")
    public String getShow_line_subtotals_tax_selection(){
        return this.show_line_subtotals_tax_selection ;
    }

    /**
     * 设置 [税目汇总表]
     */
    @JsonProperty("show_line_subtotals_tax_selection")
    public void setShow_line_subtotals_tax_selection(String  show_line_subtotals_tax_selection){
        this.show_line_subtotals_tax_selection = show_line_subtotals_tax_selection ;
        this.show_line_subtotals_tax_selectionDirtyFlag = true ;
    }

     /**
     * 获取 [税目汇总表]脏标记
     */
    @JsonIgnore
    public boolean getShow_line_subtotals_tax_selectionDirtyFlag(){
        return this.show_line_subtotals_tax_selectionDirtyFlag ;
    }   

    /**
     * 获取 [彩色打印]
     */
    @JsonProperty("snailmail_color")
    public String getSnailmail_color(){
        return this.snailmail_color ;
    }

    /**
     * 设置 [彩色打印]
     */
    @JsonProperty("snailmail_color")
    public void setSnailmail_color(String  snailmail_color){
        this.snailmail_color = snailmail_color ;
        this.snailmail_colorDirtyFlag = true ;
    }

     /**
     * 获取 [彩色打印]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_colorDirtyFlag(){
        return this.snailmail_colorDirtyFlag ;
    }   

    /**
     * 获取 [双面打印]
     */
    @JsonProperty("snailmail_duplex")
    public String getSnailmail_duplex(){
        return this.snailmail_duplex ;
    }

    /**
     * 设置 [双面打印]
     */
    @JsonProperty("snailmail_duplex")
    public void setSnailmail_duplex(String  snailmail_duplex){
        this.snailmail_duplex = snailmail_duplex ;
        this.snailmail_duplexDirtyFlag = true ;
    }

     /**
     * 获取 [双面打印]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_duplexDirtyFlag(){
        return this.snailmail_duplexDirtyFlag ;
    }   

    /**
     * 获取 [默认社交分享图片]
     */
    @JsonProperty("social_default_image")
    public byte[] getSocial_default_image(){
        return this.social_default_image ;
    }

    /**
     * 设置 [默认社交分享图片]
     */
    @JsonProperty("social_default_image")
    public void setSocial_default_image(byte[]  social_default_image){
        this.social_default_image = social_default_image ;
        this.social_default_imageDirtyFlag = true ;
    }

     /**
     * 获取 [默认社交分享图片]脏标记
     */
    @JsonIgnore
    public boolean getSocial_default_imageDirtyFlag(){
        return this.social_default_imageDirtyFlag ;
    }   

    /**
     * 获取 [脸书账号]
     */
    @JsonProperty("social_facebook")
    public String getSocial_facebook(){
        return this.social_facebook ;
    }

    /**
     * 设置 [脸书账号]
     */
    @JsonProperty("social_facebook")
    public void setSocial_facebook(String  social_facebook){
        this.social_facebook = social_facebook ;
        this.social_facebookDirtyFlag = true ;
    }

     /**
     * 获取 [脸书账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_facebookDirtyFlag(){
        return this.social_facebookDirtyFlag ;
    }   

    /**
     * 获取 [GitHub账户]
     */
    @JsonProperty("social_github")
    public String getSocial_github(){
        return this.social_github ;
    }

    /**
     * 设置 [GitHub账户]
     */
    @JsonProperty("social_github")
    public void setSocial_github(String  social_github){
        this.social_github = social_github ;
        this.social_githubDirtyFlag = true ;
    }

     /**
     * 获取 [GitHub账户]脏标记
     */
    @JsonIgnore
    public boolean getSocial_githubDirtyFlag(){
        return this.social_githubDirtyFlag ;
    }   

    /**
     * 获取 [Google+账户]
     */
    @JsonProperty("social_googleplus")
    public String getSocial_googleplus(){
        return this.social_googleplus ;
    }

    /**
     * 设置 [Google+账户]
     */
    @JsonProperty("social_googleplus")
    public void setSocial_googleplus(String  social_googleplus){
        this.social_googleplus = social_googleplus ;
        this.social_googleplusDirtyFlag = true ;
    }

     /**
     * 获取 [Google+账户]脏标记
     */
    @JsonIgnore
    public boolean getSocial_googleplusDirtyFlag(){
        return this.social_googleplusDirtyFlag ;
    }   

    /**
     * 获取 [Instagram 账号]
     */
    @JsonProperty("social_instagram")
    public String getSocial_instagram(){
        return this.social_instagram ;
    }

    /**
     * 设置 [Instagram 账号]
     */
    @JsonProperty("social_instagram")
    public void setSocial_instagram(String  social_instagram){
        this.social_instagram = social_instagram ;
        this.social_instagramDirtyFlag = true ;
    }

     /**
     * 获取 [Instagram 账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_instagramDirtyFlag(){
        return this.social_instagramDirtyFlag ;
    }   

    /**
     * 获取 [领英账号]
     */
    @JsonProperty("social_linkedin")
    public String getSocial_linkedin(){
        return this.social_linkedin ;
    }

    /**
     * 设置 [领英账号]
     */
    @JsonProperty("social_linkedin")
    public void setSocial_linkedin(String  social_linkedin){
        this.social_linkedin = social_linkedin ;
        this.social_linkedinDirtyFlag = true ;
    }

     /**
     * 获取 [领英账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_linkedinDirtyFlag(){
        return this.social_linkedinDirtyFlag ;
    }   

    /**
     * 获取 [Twitter账号]
     */
    @JsonProperty("social_twitter")
    public String getSocial_twitter(){
        return this.social_twitter ;
    }

    /**
     * 设置 [Twitter账号]
     */
    @JsonProperty("social_twitter")
    public void setSocial_twitter(String  social_twitter){
        this.social_twitter = social_twitter ;
        this.social_twitterDirtyFlag = true ;
    }

     /**
     * 获取 [Twitter账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_twitterDirtyFlag(){
        return this.social_twitterDirtyFlag ;
    }   

    /**
     * 获取 [Youtube账号]
     */
    @JsonProperty("social_youtube")
    public String getSocial_youtube(){
        return this.social_youtube ;
    }

    /**
     * 设置 [Youtube账号]
     */
    @JsonProperty("social_youtube")
    public void setSocial_youtube(String  social_youtube){
        this.social_youtube = social_youtube ;
        this.social_youtubeDirtyFlag = true ;
    }

     /**
     * 获取 [Youtube账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_youtubeDirtyFlag(){
        return this.social_youtubeDirtyFlag ;
    }   

    /**
     * 获取 [具体用户账号]
     */
    @JsonProperty("specific_user_account")
    public String getSpecific_user_account(){
        return this.specific_user_account ;
    }

    /**
     * 设置 [具体用户账号]
     */
    @JsonProperty("specific_user_account")
    public void setSpecific_user_account(String  specific_user_account){
        this.specific_user_account = specific_user_account ;
        this.specific_user_accountDirtyFlag = true ;
    }

     /**
     * 获取 [具体用户账号]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_user_accountDirtyFlag(){
        return this.specific_user_accountDirtyFlag ;
    }   

    /**
     * 获取 [税率计算的舍入方法]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public String getTax_calculation_rounding_method(){
        return this.tax_calculation_rounding_method ;
    }

    /**
     * 设置 [税率计算的舍入方法]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public void setTax_calculation_rounding_method(String  tax_calculation_rounding_method){
        this.tax_calculation_rounding_method = tax_calculation_rounding_method ;
        this.tax_calculation_rounding_methodDirtyFlag = true ;
    }

     /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    @JsonIgnore
    public boolean getTax_calculation_rounding_methodDirtyFlag(){
        return this.tax_calculation_rounding_methodDirtyFlag ;
    }   

    /**
     * 获取 [税率现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public Integer getTax_cash_basis_journal_id(){
        return this.tax_cash_basis_journal_id ;
    }

    /**
     * 设置 [税率现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public void setTax_cash_basis_journal_id(Integer  tax_cash_basis_journal_id){
        this.tax_cash_basis_journal_id = tax_cash_basis_journal_id ;
        this.tax_cash_basis_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率现金收付制日记账]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_idDirtyFlag(){
        return this.tax_cash_basis_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [现金收付制]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return this.tax_exigibility ;
    }

    /**
     * 设置 [现金收付制]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

     /**
     * 获取 [现金收付制]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return this.tax_exigibilityDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return this.template_id ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return this.template_idDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return this.template_id_text ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return this.template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [访问秘钥]
     */
    @JsonProperty("unsplash_access_key")
    public String getUnsplash_access_key(){
        return this.unsplash_access_key ;
    }

    /**
     * 设置 [访问秘钥]
     */
    @JsonProperty("unsplash_access_key")
    public void setUnsplash_access_key(String  unsplash_access_key){
        this.unsplash_access_key = unsplash_access_key ;
        this.unsplash_access_keyDirtyFlag = true ;
    }

     /**
     * 获取 [访问秘钥]脏标记
     */
    @JsonIgnore
    public boolean getUnsplash_access_keyDirtyFlag(){
        return this.unsplash_access_keyDirtyFlag ;
    }   

    /**
     * 获取 [默认访问权限]
     */
    @JsonProperty("user_default_rights")
    public String getUser_default_rights(){
        return this.user_default_rights ;
    }

    /**
     * 设置 [默认访问权限]
     */
    @JsonProperty("user_default_rights")
    public void setUser_default_rights(String  user_default_rights){
        this.user_default_rights = user_default_rights ;
        this.user_default_rightsDirtyFlag = true ;
    }

     /**
     * 获取 [默认访问权限]脏标记
     */
    @JsonIgnore
    public boolean getUser_default_rightsDirtyFlag(){
        return this.user_default_rightsDirtyFlag ;
    }   

    /**
     * 获取 [允许员工通过EMail记录费用]
     */
    @JsonProperty("use_mailgateway")
    public String getUse_mailgateway(){
        return this.use_mailgateway ;
    }

    /**
     * 设置 [允许员工通过EMail记录费用]
     */
    @JsonProperty("use_mailgateway")
    public void setUse_mailgateway(String  use_mailgateway){
        this.use_mailgateway = use_mailgateway ;
        this.use_mailgatewayDirtyFlag = true ;
    }

     /**
     * 获取 [允许员工通过EMail记录费用]脏标记
     */
    @JsonIgnore
    public boolean getUse_mailgatewayDirtyFlag(){
        return this.use_mailgatewayDirtyFlag ;
    }   

    /**
     * 获取 [默认制造提前期]
     */
    @JsonProperty("use_manufacturing_lead")
    public String getUse_manufacturing_lead(){
        return this.use_manufacturing_lead ;
    }

    /**
     * 设置 [默认制造提前期]
     */
    @JsonProperty("use_manufacturing_lead")
    public void setUse_manufacturing_lead(String  use_manufacturing_lead){
        this.use_manufacturing_lead = use_manufacturing_lead ;
        this.use_manufacturing_leadDirtyFlag = true ;
    }

     /**
     * 获取 [默认制造提前期]脏标记
     */
    @JsonIgnore
    public boolean getUse_manufacturing_leadDirtyFlag(){
        return this.use_manufacturing_leadDirtyFlag ;
    }   

    /**
     * 获取 [安全交货时间]
     */
    @JsonProperty("use_po_lead")
    public String getUse_po_lead(){
        return this.use_po_lead ;
    }

    /**
     * 设置 [安全交货时间]
     */
    @JsonProperty("use_po_lead")
    public void setUse_po_lead(String  use_po_lead){
        this.use_po_lead = use_po_lead ;
        this.use_po_leadDirtyFlag = true ;
    }

     /**
     * 获取 [安全交货时间]脏标记
     */
    @JsonIgnore
    public boolean getUse_po_leadDirtyFlag(){
        return this.use_po_leadDirtyFlag ;
    }   

    /**
     * 获取 [无重新排程传播]
     */
    @JsonProperty("use_propagation_minimum_delta")
    public String getUse_propagation_minimum_delta(){
        return this.use_propagation_minimum_delta ;
    }

    /**
     * 设置 [无重新排程传播]
     */
    @JsonProperty("use_propagation_minimum_delta")
    public void setUse_propagation_minimum_delta(String  use_propagation_minimum_delta){
        this.use_propagation_minimum_delta = use_propagation_minimum_delta ;
        this.use_propagation_minimum_deltaDirtyFlag = true ;
    }

     /**
     * 获取 [无重新排程传播]脏标记
     */
    @JsonIgnore
    public boolean getUse_propagation_minimum_deltaDirtyFlag(){
        return this.use_propagation_minimum_deltaDirtyFlag ;
    }   

    /**
     * 获取 [默认报价有效期]
     */
    @JsonProperty("use_quotation_validity_days")
    public String getUse_quotation_validity_days(){
        return this.use_quotation_validity_days ;
    }

    /**
     * 设置 [默认报价有效期]
     */
    @JsonProperty("use_quotation_validity_days")
    public void setUse_quotation_validity_days(String  use_quotation_validity_days){
        this.use_quotation_validity_days = use_quotation_validity_days ;
        this.use_quotation_validity_daysDirtyFlag = true ;
    }

     /**
     * 获取 [默认报价有效期]脏标记
     */
    @JsonIgnore
    public boolean getUse_quotation_validity_daysDirtyFlag(){
        return this.use_quotation_validity_daysDirtyFlag ;
    }   

    /**
     * 获取 [默认条款和条件]
     */
    @JsonProperty("use_sale_note")
    public String getUse_sale_note(){
        return this.use_sale_note ;
    }

    /**
     * 设置 [默认条款和条件]
     */
    @JsonProperty("use_sale_note")
    public void setUse_sale_note(String  use_sale_note){
        this.use_sale_note = use_sale_note ;
        this.use_sale_noteDirtyFlag = true ;
    }

     /**
     * 获取 [默认条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getUse_sale_noteDirtyFlag(){
        return this.use_sale_noteDirtyFlag ;
    }   

    /**
     * 获取 [销售的安全提前期]
     */
    @JsonProperty("use_security_lead")
    public String getUse_security_lead(){
        return this.use_security_lead ;
    }

    /**
     * 设置 [销售的安全提前期]
     */
    @JsonProperty("use_security_lead")
    public void setUse_security_lead(String  use_security_lead){
        this.use_security_lead = use_security_lead ;
        this.use_security_leadDirtyFlag = true ;
    }

     /**
     * 获取 [销售的安全提前期]脏标记
     */
    @JsonIgnore
    public boolean getUse_security_leadDirtyFlag(){
        return this.use_security_leadDirtyFlag ;
    }   

    /**
     * 获取 [网站公司]
     */
    @JsonProperty("website_company_id")
    public Integer getWebsite_company_id(){
        return this.website_company_id ;
    }

    /**
     * 设置 [网站公司]
     */
    @JsonProperty("website_company_id")
    public void setWebsite_company_id(Integer  website_company_id){
        this.website_company_id = website_company_id ;
        this.website_company_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站公司]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_company_idDirtyFlag(){
        return this.website_company_idDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区分组]
     */
    @JsonProperty("website_country_group_ids")
    public String getWebsite_country_group_ids(){
        return this.website_country_group_ids ;
    }

    /**
     * 设置 [国家/地区分组]
     */
    @JsonProperty("website_country_group_ids")
    public void setWebsite_country_group_ids(String  website_country_group_ids){
        this.website_country_group_ids = website_country_group_ids ;
        this.website_country_group_idsDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区分组]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_country_group_idsDirtyFlag(){
        return this.website_country_group_idsDirtyFlag ;
    }   

    /**
     * 获取 [默认语言代码]
     */
    @JsonProperty("website_default_lang_code")
    public String getWebsite_default_lang_code(){
        return this.website_default_lang_code ;
    }

    /**
     * 设置 [默认语言代码]
     */
    @JsonProperty("website_default_lang_code")
    public void setWebsite_default_lang_code(String  website_default_lang_code){
        this.website_default_lang_code = website_default_lang_code ;
        this.website_default_lang_codeDirtyFlag = true ;
    }

     /**
     * 获取 [默认语言代码]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_default_lang_codeDirtyFlag(){
        return this.website_default_lang_codeDirtyFlag ;
    }   

    /**
     * 获取 [默认语言]
     */
    @JsonProperty("website_default_lang_id")
    public Integer getWebsite_default_lang_id(){
        return this.website_default_lang_id ;
    }

    /**
     * 设置 [默认语言]
     */
    @JsonProperty("website_default_lang_id")
    public void setWebsite_default_lang_id(Integer  website_default_lang_id){
        this.website_default_lang_id = website_default_lang_id ;
        this.website_default_lang_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认语言]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_default_lang_idDirtyFlag(){
        return this.website_default_lang_idDirtyFlag ;
    }   

    /**
     * 获取 [网站域名]
     */
    @JsonProperty("website_domain")
    public String getWebsite_domain(){
        return this.website_domain ;
    }

    /**
     * 设置 [网站域名]
     */
    @JsonProperty("website_domain")
    public void setWebsite_domain(String  website_domain){
        this.website_domain = website_domain ;
        this.website_domainDirtyFlag = true ;
    }

     /**
     * 获取 [网站域名]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_domainDirtyFlag(){
        return this.website_domainDirtyFlag ;
    }   

    /**
     * 获取 [联系表单上的技术数据]
     */
    @JsonProperty("website_form_enable_metadata")
    public String getWebsite_form_enable_metadata(){
        return this.website_form_enable_metadata ;
    }

    /**
     * 设置 [联系表单上的技术数据]
     */
    @JsonProperty("website_form_enable_metadata")
    public void setWebsite_form_enable_metadata(String  website_form_enable_metadata){
        this.website_form_enable_metadata = website_form_enable_metadata ;
        this.website_form_enable_metadataDirtyFlag = true ;
    }

     /**
     * 获取 [联系表单上的技术数据]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_form_enable_metadataDirtyFlag(){
        return this.website_form_enable_metadataDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站名称]
     */
    @JsonProperty("website_name")
    public String getWebsite_name(){
        return this.website_name ;
    }

    /**
     * 设置 [网站名称]
     */
    @JsonProperty("website_name")
    public void setWebsite_name(String  website_name){
        this.website_name = website_name ;
        this.website_nameDirtyFlag = true ;
    }

     /**
     * 获取 [网站名称]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_nameDirtyFlag(){
        return this.website_nameDirtyFlag ;
    }   

    /**
     * 获取 [谷歌文档密钥]
     */
    @JsonProperty("website_slide_google_app_key")
    public String getWebsite_slide_google_app_key(){
        return this.website_slide_google_app_key ;
    }

    /**
     * 设置 [谷歌文档密钥]
     */
    @JsonProperty("website_slide_google_app_key")
    public void setWebsite_slide_google_app_key(String  website_slide_google_app_key){
        this.website_slide_google_app_key = website_slide_google_app_key ;
        this.website_slide_google_app_keyDirtyFlag = true ;
    }

     /**
     * 获取 [谷歌文档密钥]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_slide_google_app_keyDirtyFlag(){
        return this.website_slide_google_app_keyDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

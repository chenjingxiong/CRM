package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_currency;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_currencyClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_currencyImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_currencyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_currency] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_currencyClientServiceImpl implements Ires_currencyClientService {

    res_currencyFeignClient res_currencyFeignClient;

    @Autowired
    public res_currencyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_currencyFeignClient = nameBuilder.target(res_currencyFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_currencyFeignClient = nameBuilder.target(res_currencyFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_currency createModel() {
		return new res_currencyImpl();
	}


    public void get(Ires_currency res_currency){
        Ires_currency clientModel = res_currencyFeignClient.get(res_currency.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency.getClass(), false);
        copier.copy(clientModel, res_currency, null);
    }


    public void updateBatch(List<Ires_currency> res_currencies){
        if(res_currencies!=null){
            List<res_currencyImpl> list = new ArrayList<res_currencyImpl>();
            for(Ires_currency ires_currency :res_currencies){
                list.add((res_currencyImpl)ires_currency) ;
            }
            res_currencyFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ires_currency res_currency){
        res_currencyFeignClient.remove(res_currency.getId()) ;
    }


    public void update(Ires_currency res_currency){
        Ires_currency clientModel = res_currencyFeignClient.update(res_currency.getId(),(res_currencyImpl)res_currency) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency.getClass(), false);
        copier.copy(clientModel, res_currency, null);
    }


    public Page<Ires_currency> fetchDefault(SearchContext context){
        Page<res_currencyImpl> page = this.res_currencyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ires_currency res_currency){
        Ires_currency clientModel = res_currencyFeignClient.create((res_currencyImpl)res_currency) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency.getClass(), false);
        copier.copy(clientModel, res_currency, null);
    }


    public void removeBatch(List<Ires_currency> res_currencies){
        if(res_currencies!=null){
            List<res_currencyImpl> list = new ArrayList<res_currencyImpl>();
            for(Ires_currency ires_currency :res_currencies){
                list.add((res_currencyImpl)ires_currency) ;
            }
            res_currencyFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ires_currency> res_currencies){
        if(res_currencies!=null){
            List<res_currencyImpl> list = new ArrayList<res_currencyImpl>();
            for(Ires_currency ires_currency :res_currencies){
                list.add((res_currencyImpl)ires_currency) ;
            }
            res_currencyFeignClient.createBatch(list) ;
        }
    }


    public Page<Ires_currency> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_currency res_currency){
        Ires_currency clientModel = res_currencyFeignClient.getDraft(res_currency.getId(),(res_currencyImpl)res_currency) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_currency.getClass(), false);
        copier.copy(clientModel, res_currency, null);
    }



}


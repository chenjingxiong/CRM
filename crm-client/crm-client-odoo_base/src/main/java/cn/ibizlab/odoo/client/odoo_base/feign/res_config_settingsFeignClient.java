package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_config_settings;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_settingsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_config_settings] 服务对象接口
 */
public interface res_config_settingsFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_settings/createbatch")
    public res_config_settingsImpl createBatch(@RequestBody List<res_config_settingsImpl> res_config_settings);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_settings/updatebatch")
    public res_config_settingsImpl updateBatch(@RequestBody List<res_config_settingsImpl> res_config_settings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_settings/fetchdefault")
    public Page<res_config_settingsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_settings/removebatch")
    public res_config_settingsImpl removeBatch(@RequestBody List<res_config_settingsImpl> res_config_settings);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_settings/{id}")
    public res_config_settingsImpl update(@PathVariable("id") Integer id,@RequestBody res_config_settingsImpl res_config_settings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_settings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_settings")
    public res_config_settingsImpl create(@RequestBody res_config_settingsImpl res_config_settings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_settings/{id}")
    public res_config_settingsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_settings/select")
    public Page<res_config_settingsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_settings/{id}/getdraft")
    public res_config_settingsImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_config_settingsImpl res_config_settings);



}

package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_update;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_module_updateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_updateImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_module_updateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_module_update] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_module_updateClientServiceImpl implements Ibase_module_updateClientService {

    base_module_updateFeignClient base_module_updateFeignClient;

    @Autowired
    public base_module_updateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_updateFeignClient = nameBuilder.target(base_module_updateFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_updateFeignClient = nameBuilder.target(base_module_updateFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_module_update createModel() {
		return new base_module_updateImpl();
	}


    public void updateBatch(List<Ibase_module_update> base_module_updates){
        if(base_module_updates!=null){
            List<base_module_updateImpl> list = new ArrayList<base_module_updateImpl>();
            for(Ibase_module_update ibase_module_update :base_module_updates){
                list.add((base_module_updateImpl)ibase_module_update) ;
            }
            base_module_updateFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ibase_module_update base_module_update){
        Ibase_module_update clientModel = base_module_updateFeignClient.update(base_module_update.getId(),(base_module_updateImpl)base_module_update) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_update.getClass(), false);
        copier.copy(clientModel, base_module_update, null);
    }


    public void createBatch(List<Ibase_module_update> base_module_updates){
        if(base_module_updates!=null){
            List<base_module_updateImpl> list = new ArrayList<base_module_updateImpl>();
            for(Ibase_module_update ibase_module_update :base_module_updates){
                list.add((base_module_updateImpl)ibase_module_update) ;
            }
            base_module_updateFeignClient.createBatch(list) ;
        }
    }


    public void create(Ibase_module_update base_module_update){
        Ibase_module_update clientModel = base_module_updateFeignClient.create((base_module_updateImpl)base_module_update) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_update.getClass(), false);
        copier.copy(clientModel, base_module_update, null);
    }


    public void remove(Ibase_module_update base_module_update){
        base_module_updateFeignClient.remove(base_module_update.getId()) ;
    }


    public Page<Ibase_module_update> fetchDefault(SearchContext context){
        Page<base_module_updateImpl> page = this.base_module_updateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ibase_module_update base_module_update){
        Ibase_module_update clientModel = base_module_updateFeignClient.get(base_module_update.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_update.getClass(), false);
        copier.copy(clientModel, base_module_update, null);
    }


    public void removeBatch(List<Ibase_module_update> base_module_updates){
        if(base_module_updates!=null){
            List<base_module_updateImpl> list = new ArrayList<base_module_updateImpl>();
            for(Ibase_module_update ibase_module_update :base_module_updates){
                list.add((base_module_updateImpl)ibase_module_update) ;
            }
            base_module_updateFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibase_module_update> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_module_update base_module_update){
        Ibase_module_update clientModel = base_module_updateFeignClient.getDraft(base_module_update.getId(),(base_module_updateImpl)base_module_update) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_update.getClass(), false);
        copier.copy(clientModel, base_module_update, null);
    }



}


package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_countryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_countryImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_countryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_country] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_countryClientServiceImpl implements Ires_countryClientService {

    res_countryFeignClient res_countryFeignClient;

    @Autowired
    public res_countryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_countryFeignClient = nameBuilder.target(res_countryFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_countryFeignClient = nameBuilder.target(res_countryFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_country createModel() {
		return new res_countryImpl();
	}


    public void update(Ires_country res_country){
        Ires_country clientModel = res_countryFeignClient.update(res_country.getId(),(res_countryImpl)res_country) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country.getClass(), false);
        copier.copy(clientModel, res_country, null);
    }


    public void updateBatch(List<Ires_country> res_countries){
        if(res_countries!=null){
            List<res_countryImpl> list = new ArrayList<res_countryImpl>();
            for(Ires_country ires_country :res_countries){
                list.add((res_countryImpl)ires_country) ;
            }
            res_countryFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ires_country> res_countries){
        if(res_countries!=null){
            List<res_countryImpl> list = new ArrayList<res_countryImpl>();
            for(Ires_country ires_country :res_countries){
                list.add((res_countryImpl)ires_country) ;
            }
            res_countryFeignClient.createBatch(list) ;
        }
    }


    public void create(Ires_country res_country){
        Ires_country clientModel = res_countryFeignClient.create((res_countryImpl)res_country) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country.getClass(), false);
        copier.copy(clientModel, res_country, null);
    }


    public void removeBatch(List<Ires_country> res_countries){
        if(res_countries!=null){
            List<res_countryImpl> list = new ArrayList<res_countryImpl>();
            for(Ires_country ires_country :res_countries){
                list.add((res_countryImpl)ires_country) ;
            }
            res_countryFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ires_country> fetchDefault(SearchContext context){
        Page<res_countryImpl> page = this.res_countryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ires_country res_country){
        res_countryFeignClient.remove(res_country.getId()) ;
    }


    public void get(Ires_country res_country){
        Ires_country clientModel = res_countryFeignClient.get(res_country.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country.getClass(), false);
        copier.copy(clientModel, res_country, null);
    }


    public Page<Ires_country> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_country res_country){
        Ires_country clientModel = res_countryFeignClient.getDraft(res_country.getId(),(res_countryImpl)res_country) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_country.getClass(), false);
        copier.copy(clientModel, res_country, null);
    }



}


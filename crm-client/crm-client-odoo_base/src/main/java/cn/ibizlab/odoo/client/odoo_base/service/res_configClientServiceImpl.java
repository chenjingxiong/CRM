package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_configClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_configImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_configFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_config] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_configClientServiceImpl implements Ires_configClientService {

    res_configFeignClient res_configFeignClient;

    @Autowired
    public res_configClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_configFeignClient = nameBuilder.target(res_configFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_configFeignClient = nameBuilder.target(res_configFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_config createModel() {
		return new res_configImpl();
	}


    public void remove(Ires_config res_config){
        res_configFeignClient.remove(res_config.getId()) ;
    }


    public void create(Ires_config res_config){
        Ires_config clientModel = res_configFeignClient.create((res_configImpl)res_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config.getClass(), false);
        copier.copy(clientModel, res_config, null);
    }


    public void updateBatch(List<Ires_config> res_configs){
        if(res_configs!=null){
            List<res_configImpl> list = new ArrayList<res_configImpl>();
            for(Ires_config ires_config :res_configs){
                list.add((res_configImpl)ires_config) ;
            }
            res_configFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ires_config res_config){
        Ires_config clientModel = res_configFeignClient.get(res_config.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config.getClass(), false);
        copier.copy(clientModel, res_config, null);
    }


    public void update(Ires_config res_config){
        Ires_config clientModel = res_configFeignClient.update(res_config.getId(),(res_configImpl)res_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config.getClass(), false);
        copier.copy(clientModel, res_config, null);
    }


    public Page<Ires_config> fetchDefault(SearchContext context){
        Page<res_configImpl> page = this.res_configFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ires_config> res_configs){
        if(res_configs!=null){
            List<res_configImpl> list = new ArrayList<res_configImpl>();
            for(Ires_config ires_config :res_configs){
                list.add((res_configImpl)ires_config) ;
            }
            res_configFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_config> res_configs){
        if(res_configs!=null){
            List<res_configImpl> list = new ArrayList<res_configImpl>();
            for(Ires_config ires_config :res_configs){
                list.add((res_configImpl)ires_config) ;
            }
            res_configFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ires_config> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_config res_config){
        Ires_config clientModel = res_configFeignClient.getDraft(res_config.getId(),(res_configImpl)res_config) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_config.getClass(), false);
        copier.copy(clientModel, res_config, null);
    }



}


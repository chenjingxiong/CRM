package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_currency_rate;
import cn.ibizlab.odoo.client.odoo_base.model.res_currency_rateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_currency_rate] 服务对象接口
 */
public interface res_currency_rateFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currency_rates/{id}")
    public res_currency_rateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currency_rates/createbatch")
    public res_currency_rateImpl createBatch(@RequestBody List<res_currency_rateImpl> res_currency_rates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currency_rates")
    public res_currency_rateImpl create(@RequestBody res_currency_rateImpl res_currency_rate);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currency_rates/{id}")
    public res_currency_rateImpl update(@PathVariable("id") Integer id,@RequestBody res_currency_rateImpl res_currency_rate);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currency_rates/removebatch")
    public res_currency_rateImpl removeBatch(@RequestBody List<res_currency_rateImpl> res_currency_rates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currency_rates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currency_rates/updatebatch")
    public res_currency_rateImpl updateBatch(@RequestBody List<res_currency_rateImpl> res_currency_rates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currency_rates/fetchdefault")
    public Page<res_currency_rateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currency_rates/select")
    public Page<res_currency_rateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currency_rates/{id}/getdraft")
    public res_currency_rateImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_currency_rateImpl res_currency_rate);



}

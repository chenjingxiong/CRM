package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheetImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
public interface hr_expense_sheetFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheets")
    public hr_expense_sheetImpl create(@RequestBody hr_expense_sheetImpl hr_expense_sheet);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheets/createbatch")
    public hr_expense_sheetImpl createBatch(@RequestBody List<hr_expense_sheetImpl> hr_expense_sheets);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheets/updatebatch")
    public hr_expense_sheetImpl updateBatch(@RequestBody List<hr_expense_sheetImpl> hr_expense_sheets);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheets/{id}")
    public hr_expense_sheetImpl update(@PathVariable("id") Integer id,@RequestBody hr_expense_sheetImpl hr_expense_sheet);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheets/fetchdefault")
    public Page<hr_expense_sheetImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheets/{id}")
    public hr_expense_sheetImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheets/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheets/removebatch")
    public hr_expense_sheetImpl removeBatch(@RequestBody List<hr_expense_sheetImpl> hr_expense_sheets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheets/select")
    public Page<hr_expense_sheetImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheets/{id}/getdraft")
    public hr_expense_sheetImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_expense_sheetImpl hr_expense_sheet);



}

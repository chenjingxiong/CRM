package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_employee_category;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_employee_categoryClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_employee_categoryImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_employee_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_employee_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_employee_categoryClientServiceImpl implements Ihr_employee_categoryClientService {

    hr_employee_categoryFeignClient hr_employee_categoryFeignClient;

    @Autowired
    public hr_employee_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_employee_categoryFeignClient = nameBuilder.target(hr_employee_categoryFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_employee_categoryFeignClient = nameBuilder.target(hr_employee_categoryFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_employee_category createModel() {
		return new hr_employee_categoryImpl();
	}


    public Page<Ihr_employee_category> fetchDefault(SearchContext context){
        Page<hr_employee_categoryImpl> page = this.hr_employee_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ihr_employee_category> hr_employee_categories){
        if(hr_employee_categories!=null){
            List<hr_employee_categoryImpl> list = new ArrayList<hr_employee_categoryImpl>();
            for(Ihr_employee_category ihr_employee_category :hr_employee_categories){
                list.add((hr_employee_categoryImpl)ihr_employee_category) ;
            }
            hr_employee_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ihr_employee_category hr_employee_category){
        Ihr_employee_category clientModel = hr_employee_categoryFeignClient.get(hr_employee_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee_category.getClass(), false);
        copier.copy(clientModel, hr_employee_category, null);
    }


    public void createBatch(List<Ihr_employee_category> hr_employee_categories){
        if(hr_employee_categories!=null){
            List<hr_employee_categoryImpl> list = new ArrayList<hr_employee_categoryImpl>();
            for(Ihr_employee_category ihr_employee_category :hr_employee_categories){
                list.add((hr_employee_categoryImpl)ihr_employee_category) ;
            }
            hr_employee_categoryFeignClient.createBatch(list) ;
        }
    }


    public void update(Ihr_employee_category hr_employee_category){
        Ihr_employee_category clientModel = hr_employee_categoryFeignClient.update(hr_employee_category.getId(),(hr_employee_categoryImpl)hr_employee_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee_category.getClass(), false);
        copier.copy(clientModel, hr_employee_category, null);
    }


    public void removeBatch(List<Ihr_employee_category> hr_employee_categories){
        if(hr_employee_categories!=null){
            List<hr_employee_categoryImpl> list = new ArrayList<hr_employee_categoryImpl>();
            for(Ihr_employee_category ihr_employee_category :hr_employee_categories){
                list.add((hr_employee_categoryImpl)ihr_employee_category) ;
            }
            hr_employee_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ihr_employee_category hr_employee_category){
        Ihr_employee_category clientModel = hr_employee_categoryFeignClient.create((hr_employee_categoryImpl)hr_employee_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee_category.getClass(), false);
        copier.copy(clientModel, hr_employee_category, null);
    }


    public void remove(Ihr_employee_category hr_employee_category){
        hr_employee_categoryFeignClient.remove(hr_employee_category.getId()) ;
    }


    public Page<Ihr_employee_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_employee_category hr_employee_category){
        Ihr_employee_category clientModel = hr_employee_categoryFeignClient.getDraft(hr_employee_category.getId(),(hr_employee_categoryImpl)hr_employee_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_employee_category.getClass(), false);
        copier.copy(clientModel, hr_employee_category, null);
    }



}


package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant_category;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_applicant_categoryClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicant_categoryImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_applicant_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_applicant_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_applicant_categoryClientServiceImpl implements Ihr_applicant_categoryClientService {

    hr_applicant_categoryFeignClient hr_applicant_categoryFeignClient;

    @Autowired
    public hr_applicant_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_applicant_categoryFeignClient = nameBuilder.target(hr_applicant_categoryFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_applicant_categoryFeignClient = nameBuilder.target(hr_applicant_categoryFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_applicant_category createModel() {
		return new hr_applicant_categoryImpl();
	}


    public void updateBatch(List<Ihr_applicant_category> hr_applicant_categories){
        if(hr_applicant_categories!=null){
            List<hr_applicant_categoryImpl> list = new ArrayList<hr_applicant_categoryImpl>();
            for(Ihr_applicant_category ihr_applicant_category :hr_applicant_categories){
                list.add((hr_applicant_categoryImpl)ihr_applicant_category) ;
            }
            hr_applicant_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ihr_applicant_category hr_applicant_category){
        Ihr_applicant_category clientModel = hr_applicant_categoryFeignClient.get(hr_applicant_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant_category.getClass(), false);
        copier.copy(clientModel, hr_applicant_category, null);
    }


    public void create(Ihr_applicant_category hr_applicant_category){
        Ihr_applicant_category clientModel = hr_applicant_categoryFeignClient.create((hr_applicant_categoryImpl)hr_applicant_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant_category.getClass(), false);
        copier.copy(clientModel, hr_applicant_category, null);
    }


    public void removeBatch(List<Ihr_applicant_category> hr_applicant_categories){
        if(hr_applicant_categories!=null){
            List<hr_applicant_categoryImpl> list = new ArrayList<hr_applicant_categoryImpl>();
            for(Ihr_applicant_category ihr_applicant_category :hr_applicant_categories){
                list.add((hr_applicant_categoryImpl)ihr_applicant_category) ;
            }
            hr_applicant_categoryFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ihr_applicant_category hr_applicant_category){
        hr_applicant_categoryFeignClient.remove(hr_applicant_category.getId()) ;
    }


    public Page<Ihr_applicant_category> fetchDefault(SearchContext context){
        Page<hr_applicant_categoryImpl> page = this.hr_applicant_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ihr_applicant_category hr_applicant_category){
        Ihr_applicant_category clientModel = hr_applicant_categoryFeignClient.update(hr_applicant_category.getId(),(hr_applicant_categoryImpl)hr_applicant_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant_category.getClass(), false);
        copier.copy(clientModel, hr_applicant_category, null);
    }


    public void createBatch(List<Ihr_applicant_category> hr_applicant_categories){
        if(hr_applicant_categories!=null){
            List<hr_applicant_categoryImpl> list = new ArrayList<hr_applicant_categoryImpl>();
            for(Ihr_applicant_category ihr_applicant_category :hr_applicant_categories){
                list.add((hr_applicant_categoryImpl)ihr_applicant_category) ;
            }
            hr_applicant_categoryFeignClient.createBatch(list) ;
        }
    }


    public Page<Ihr_applicant_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_applicant_category hr_applicant_category){
        Ihr_applicant_category clientModel = hr_applicant_categoryFeignClient.getDraft(hr_applicant_category.getId(),(hr_applicant_categoryImpl)hr_applicant_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant_category.getClass(), false);
        copier.copy(clientModel, hr_applicant_category, null);
    }



}


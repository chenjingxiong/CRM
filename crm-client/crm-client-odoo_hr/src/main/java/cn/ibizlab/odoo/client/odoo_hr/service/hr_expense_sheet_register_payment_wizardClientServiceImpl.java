package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_expense_sheet_register_payment_wizardClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheet_register_payment_wizardImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_expense_sheet_register_payment_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_expense_sheet_register_payment_wizardClientServiceImpl implements Ihr_expense_sheet_register_payment_wizardClientService {

    hr_expense_sheet_register_payment_wizardFeignClient hr_expense_sheet_register_payment_wizardFeignClient;

    @Autowired
    public hr_expense_sheet_register_payment_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expense_sheet_register_payment_wizardFeignClient = nameBuilder.target(hr_expense_sheet_register_payment_wizardFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expense_sheet_register_payment_wizardFeignClient = nameBuilder.target(hr_expense_sheet_register_payment_wizardFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_expense_sheet_register_payment_wizard createModel() {
		return new hr_expense_sheet_register_payment_wizardImpl();
	}


    public void updateBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
        if(hr_expense_sheet_register_payment_wizards!=null){
            List<hr_expense_sheet_register_payment_wizardImpl> list = new ArrayList<hr_expense_sheet_register_payment_wizardImpl>();
            for(Ihr_expense_sheet_register_payment_wizard ihr_expense_sheet_register_payment_wizard :hr_expense_sheet_register_payment_wizards){
                list.add((hr_expense_sheet_register_payment_wizardImpl)ihr_expense_sheet_register_payment_wizard) ;
            }
            hr_expense_sheet_register_payment_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
        Ihr_expense_sheet_register_payment_wizard clientModel = hr_expense_sheet_register_payment_wizardFeignClient.update(hr_expense_sheet_register_payment_wizard.getId(),(hr_expense_sheet_register_payment_wizardImpl)hr_expense_sheet_register_payment_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet_register_payment_wizard.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet_register_payment_wizard, null);
    }


    public void removeBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
        if(hr_expense_sheet_register_payment_wizards!=null){
            List<hr_expense_sheet_register_payment_wizardImpl> list = new ArrayList<hr_expense_sheet_register_payment_wizardImpl>();
            for(Ihr_expense_sheet_register_payment_wizard ihr_expense_sheet_register_payment_wizard :hr_expense_sheet_register_payment_wizards){
                list.add((hr_expense_sheet_register_payment_wizardImpl)ihr_expense_sheet_register_payment_wizard) ;
            }
            hr_expense_sheet_register_payment_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
        Ihr_expense_sheet_register_payment_wizard clientModel = hr_expense_sheet_register_payment_wizardFeignClient.create((hr_expense_sheet_register_payment_wizardImpl)hr_expense_sheet_register_payment_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet_register_payment_wizard.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet_register_payment_wizard, null);
    }


    public Page<Ihr_expense_sheet_register_payment_wizard> fetchDefault(SearchContext context){
        Page<hr_expense_sheet_register_payment_wizardImpl> page = this.hr_expense_sheet_register_payment_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
        hr_expense_sheet_register_payment_wizardFeignClient.remove(hr_expense_sheet_register_payment_wizard.getId()) ;
    }


    public void createBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards){
        if(hr_expense_sheet_register_payment_wizards!=null){
            List<hr_expense_sheet_register_payment_wizardImpl> list = new ArrayList<hr_expense_sheet_register_payment_wizardImpl>();
            for(Ihr_expense_sheet_register_payment_wizard ihr_expense_sheet_register_payment_wizard :hr_expense_sheet_register_payment_wizards){
                list.add((hr_expense_sheet_register_payment_wizardImpl)ihr_expense_sheet_register_payment_wizard) ;
            }
            hr_expense_sheet_register_payment_wizardFeignClient.createBatch(list) ;
        }
    }


    public void get(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
        Ihr_expense_sheet_register_payment_wizard clientModel = hr_expense_sheet_register_payment_wizardFeignClient.get(hr_expense_sheet_register_payment_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet_register_payment_wizard.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet_register_payment_wizard, null);
    }


    public Page<Ihr_expense_sheet_register_payment_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard){
        Ihr_expense_sheet_register_payment_wizard clientModel = hr_expense_sheet_register_payment_wizardFeignClient.getDraft(hr_expense_sheet_register_payment_wizard.getId(),(hr_expense_sheet_register_payment_wizardImpl)hr_expense_sheet_register_payment_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet_register_payment_wizard.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet_register_payment_wizard, null);
    }



}


package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_contract;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_contractClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_contractImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_contractFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_contract] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_contractClientServiceImpl implements Ihr_contractClientService {

    hr_contractFeignClient hr_contractFeignClient;

    @Autowired
    public hr_contractClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_contractFeignClient = nameBuilder.target(hr_contractFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_contractFeignClient = nameBuilder.target(hr_contractFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_contract createModel() {
		return new hr_contractImpl();
	}


    public Page<Ihr_contract> fetchDefault(SearchContext context){
        Page<hr_contractImpl> page = this.hr_contractFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ihr_contract hr_contract){
        hr_contractFeignClient.remove(hr_contract.getId()) ;
    }


    public void get(Ihr_contract hr_contract){
        Ihr_contract clientModel = hr_contractFeignClient.get(hr_contract.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract.getClass(), false);
        copier.copy(clientModel, hr_contract, null);
    }


    public void createBatch(List<Ihr_contract> hr_contracts){
        if(hr_contracts!=null){
            List<hr_contractImpl> list = new ArrayList<hr_contractImpl>();
            for(Ihr_contract ihr_contract :hr_contracts){
                list.add((hr_contractImpl)ihr_contract) ;
            }
            hr_contractFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ihr_contract> hr_contracts){
        if(hr_contracts!=null){
            List<hr_contractImpl> list = new ArrayList<hr_contractImpl>();
            for(Ihr_contract ihr_contract :hr_contracts){
                list.add((hr_contractImpl)ihr_contract) ;
            }
            hr_contractFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ihr_contract hr_contract){
        Ihr_contract clientModel = hr_contractFeignClient.create((hr_contractImpl)hr_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract.getClass(), false);
        copier.copy(clientModel, hr_contract, null);
    }


    public void update(Ihr_contract hr_contract){
        Ihr_contract clientModel = hr_contractFeignClient.update(hr_contract.getId(),(hr_contractImpl)hr_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract.getClass(), false);
        copier.copy(clientModel, hr_contract, null);
    }


    public void removeBatch(List<Ihr_contract> hr_contracts){
        if(hr_contracts!=null){
            List<hr_contractImpl> list = new ArrayList<hr_contractImpl>();
            for(Ihr_contract ihr_contract :hr_contracts){
                list.add((hr_contractImpl)ihr_contract) ;
            }
            hr_contractFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ihr_contract> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_contract hr_contract){
        Ihr_contract clientModel = hr_contractFeignClient.getDraft(hr_contract.getId(),(hr_contractImpl)hr_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract.getClass(), false);
        copier.copy(clientModel, hr_contract, null);
    }



}


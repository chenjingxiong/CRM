package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_attendance;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_attendanceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_attendance] 服务对象接口
 */
public interface hr_attendanceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_attendances/{id}")
    public hr_attendanceImpl update(@PathVariable("id") Integer id,@RequestBody hr_attendanceImpl hr_attendance);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_attendances/createbatch")
    public hr_attendanceImpl createBatch(@RequestBody List<hr_attendanceImpl> hr_attendances);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_attendances")
    public hr_attendanceImpl create(@RequestBody hr_attendanceImpl hr_attendance);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_attendances/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_attendances/updatebatch")
    public hr_attendanceImpl updateBatch(@RequestBody List<hr_attendanceImpl> hr_attendances);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_attendances/removebatch")
    public hr_attendanceImpl removeBatch(@RequestBody List<hr_attendanceImpl> hr_attendances);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_attendances/{id}")
    public hr_attendanceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_attendances/fetchdefault")
    public Page<hr_attendanceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_attendances/select")
    public Page<hr_attendanceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_attendances/{id}/getdraft")
    public hr_attendanceImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_attendanceImpl hr_attendance);



}

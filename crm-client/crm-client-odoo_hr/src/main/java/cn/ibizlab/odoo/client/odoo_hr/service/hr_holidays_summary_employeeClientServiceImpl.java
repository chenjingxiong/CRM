package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_employee;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_holidays_summary_employeeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_employeeImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_holidays_summary_employeeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_holidays_summary_employeeClientServiceImpl implements Ihr_holidays_summary_employeeClientService {

    hr_holidays_summary_employeeFeignClient hr_holidays_summary_employeeFeignClient;

    @Autowired
    public hr_holidays_summary_employeeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_holidays_summary_employeeFeignClient = nameBuilder.target(hr_holidays_summary_employeeFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_holidays_summary_employeeFeignClient = nameBuilder.target(hr_holidays_summary_employeeFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_holidays_summary_employee createModel() {
		return new hr_holidays_summary_employeeImpl();
	}


    public void remove(Ihr_holidays_summary_employee hr_holidays_summary_employee){
        hr_holidays_summary_employeeFeignClient.remove(hr_holidays_summary_employee.getId()) ;
    }


    public Page<Ihr_holidays_summary_employee> fetchDefault(SearchContext context){
        Page<hr_holidays_summary_employeeImpl> page = this.hr_holidays_summary_employeeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
        if(hr_holidays_summary_employees!=null){
            List<hr_holidays_summary_employeeImpl> list = new ArrayList<hr_holidays_summary_employeeImpl>();
            for(Ihr_holidays_summary_employee ihr_holidays_summary_employee :hr_holidays_summary_employees){
                list.add((hr_holidays_summary_employeeImpl)ihr_holidays_summary_employee) ;
            }
            hr_holidays_summary_employeeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
        if(hr_holidays_summary_employees!=null){
            List<hr_holidays_summary_employeeImpl> list = new ArrayList<hr_holidays_summary_employeeImpl>();
            for(Ihr_holidays_summary_employee ihr_holidays_summary_employee :hr_holidays_summary_employees){
                list.add((hr_holidays_summary_employeeImpl)ihr_holidays_summary_employee) ;
            }
            hr_holidays_summary_employeeFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ihr_holidays_summary_employee> hr_holidays_summary_employees){
        if(hr_holidays_summary_employees!=null){
            List<hr_holidays_summary_employeeImpl> list = new ArrayList<hr_holidays_summary_employeeImpl>();
            for(Ihr_holidays_summary_employee ihr_holidays_summary_employee :hr_holidays_summary_employees){
                list.add((hr_holidays_summary_employeeImpl)ihr_holidays_summary_employee) ;
            }
            hr_holidays_summary_employeeFeignClient.createBatch(list) ;
        }
    }


    public void update(Ihr_holidays_summary_employee hr_holidays_summary_employee){
        Ihr_holidays_summary_employee clientModel = hr_holidays_summary_employeeFeignClient.update(hr_holidays_summary_employee.getId(),(hr_holidays_summary_employeeImpl)hr_holidays_summary_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_employee.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_employee, null);
    }


    public void get(Ihr_holidays_summary_employee hr_holidays_summary_employee){
        Ihr_holidays_summary_employee clientModel = hr_holidays_summary_employeeFeignClient.get(hr_holidays_summary_employee.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_employee.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_employee, null);
    }


    public void create(Ihr_holidays_summary_employee hr_holidays_summary_employee){
        Ihr_holidays_summary_employee clientModel = hr_holidays_summary_employeeFeignClient.create((hr_holidays_summary_employeeImpl)hr_holidays_summary_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_employee.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_employee, null);
    }


    public Page<Ihr_holidays_summary_employee> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_holidays_summary_employee hr_holidays_summary_employee){
        Ihr_holidays_summary_employee clientModel = hr_holidays_summary_employeeFeignClient.getDraft(hr_holidays_summary_employee.getId(),(hr_holidays_summary_employeeImpl)hr_holidays_summary_employee) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_employee.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_employee, null);
    }



}


package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_allocation;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_allocationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
public interface hr_leave_allocationFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_allocations/removebatch")
    public hr_leave_allocationImpl removeBatch(@RequestBody List<hr_leave_allocationImpl> hr_leave_allocations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_allocations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_allocations/{id}")
    public hr_leave_allocationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_allocations/fetchdefault")
    public Page<hr_leave_allocationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_allocations/updatebatch")
    public hr_leave_allocationImpl updateBatch(@RequestBody List<hr_leave_allocationImpl> hr_leave_allocations);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_allocations/{id}")
    public hr_leave_allocationImpl update(@PathVariable("id") Integer id,@RequestBody hr_leave_allocationImpl hr_leave_allocation);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_allocations")
    public hr_leave_allocationImpl create(@RequestBody hr_leave_allocationImpl hr_leave_allocation);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_allocations/createbatch")
    public hr_leave_allocationImpl createBatch(@RequestBody List<hr_leave_allocationImpl> hr_leave_allocations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_allocations/select")
    public Page<hr_leave_allocationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_allocations/{id}/getdraft")
    public hr_leave_allocationImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_leave_allocationImpl hr_leave_allocation);



}

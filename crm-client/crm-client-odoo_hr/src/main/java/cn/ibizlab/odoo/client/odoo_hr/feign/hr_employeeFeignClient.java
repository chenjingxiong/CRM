package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_employeeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_employee] 服务对象接口
 */
public interface hr_employeeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employees/fetchdefault")
    public Page<hr_employeeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employees/createbatch")
    public hr_employeeImpl createBatch(@RequestBody List<hr_employeeImpl> hr_employees);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employees/{id}")
    public hr_employeeImpl update(@PathVariable("id") Integer id,@RequestBody hr_employeeImpl hr_employee);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employees/updatebatch")
    public hr_employeeImpl updateBatch(@RequestBody List<hr_employeeImpl> hr_employees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employees/removebatch")
    public hr_employeeImpl removeBatch(@RequestBody List<hr_employeeImpl> hr_employees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employees/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employees")
    public hr_employeeImpl create(@RequestBody hr_employeeImpl hr_employee);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employees/{id}")
    public hr_employeeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employees/select")
    public Page<hr_employeeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employees/{id}/getdraft")
    public hr_employeeImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_employeeImpl hr_employee);



}

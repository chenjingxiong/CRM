package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_leave;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[hr_leave] 对象
 */
public class hr_leaveImpl implements Ihr_leave,Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 能批准
     */
    public String can_approve;

    @JsonIgnore
    public boolean can_approveDirtyFlag;
    
    /**
     * 能重置
     */
    public String can_reset;

    @JsonIgnore
    public boolean can_resetDirtyFlag;
    
    /**
     * 员工标签
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 员工标签
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_from;

    @JsonIgnore
    public boolean date_fromDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_to;

    @JsonIgnore
    public boolean date_toDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 要求的（天/小时）
     */
    public String duration_display;

    @JsonIgnore
    public boolean duration_displayDirtyFlag;
    
    /**
     * 员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * 首次审批
     */
    public Integer first_approver_id;

    @JsonIgnore
    public boolean first_approver_idDirtyFlag;
    
    /**
     * 首次审批
     */
    public String first_approver_id_text;

    @JsonIgnore
    public boolean first_approver_id_textDirtyFlag;
    
    /**
     * 休假类型
     */
    public Integer holiday_status_id;

    @JsonIgnore
    public boolean holiday_status_idDirtyFlag;
    
    /**
     * 休假类型
     */
    public String holiday_status_id_text;

    @JsonIgnore
    public boolean holiday_status_id_textDirtyFlag;
    
    /**
     * 分配模式
     */
    public String holiday_type;

    @JsonIgnore
    public boolean holiday_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 休假
     */
    public String leave_type_request_unit;

    @JsonIgnore
    public boolean leave_type_request_unitDirtyFlag;
    
    /**
     * 链接申请
     */
    public String linked_request_ids;

    @JsonIgnore
    public boolean linked_request_idsDirtyFlag;
    
    /**
     * 经理
     */
    public Integer manager_id;

    @JsonIgnore
    public boolean manager_idDirtyFlag;
    
    /**
     * 经理
     */
    public String manager_id_text;

    @JsonIgnore
    public boolean manager_id_textDirtyFlag;
    
    /**
     * 会议
     */
    public Integer meeting_id;

    @JsonIgnore
    public boolean meeting_idDirtyFlag;
    
    /**
     * 会议
     */
    public String meeting_id_text;

    @JsonIgnore
    public boolean meeting_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 信息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 公司
     */
    public Integer mode_company_id;

    @JsonIgnore
    public boolean mode_company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String mode_company_id_text;

    @JsonIgnore
    public boolean mode_company_id_textDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 理由
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 持续时间（天）
     */
    public Double number_of_days;

    @JsonIgnore
    public boolean number_of_daysDirtyFlag;
    
    /**
     * 持续时间（天）
     */
    public Double number_of_days_display;

    @JsonIgnore
    public boolean number_of_days_displayDirtyFlag;
    
    /**
     * 持续时间（小时）
     */
    public Double number_of_hours_display;

    @JsonIgnore
    public boolean number_of_hours_displayDirtyFlag;
    
    /**
     * 上级
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 反映在最近的工资单中
     */
    public String payslip_status;

    @JsonIgnore
    public boolean payslip_statusDirtyFlag;
    
    /**
     * HR 备注
     */
    public String report_note;

    @JsonIgnore
    public boolean report_noteDirtyFlag;
    
    /**
     * 请假开始日期
     */
    public Timestamp request_date_from;

    @JsonIgnore
    public boolean request_date_fromDirtyFlag;
    
    /**
     * 日期开始
     */
    public String request_date_from_period;

    @JsonIgnore
    public boolean request_date_from_periodDirtyFlag;
    
    /**
     * 请求结束日期
     */
    public Timestamp request_date_to;

    @JsonIgnore
    public boolean request_date_toDirtyFlag;
    
    /**
     * 时间从
     */
    public String request_hour_from;

    @JsonIgnore
    public boolean request_hour_fromDirtyFlag;
    
    /**
     * 时间到
     */
    public String request_hour_to;

    @JsonIgnore
    public boolean request_hour_toDirtyFlag;
    
    /**
     * 长达数天的定制时间
     */
    public String request_unit_custom;

    @JsonIgnore
    public boolean request_unit_customDirtyFlag;
    
    /**
     * 半天
     */
    public String request_unit_half;

    @JsonIgnore
    public boolean request_unit_halfDirtyFlag;
    
    /**
     * 自定义时间
     */
    public String request_unit_hours;

    @JsonIgnore
    public boolean request_unit_hoursDirtyFlag;
    
    /**
     * 第二次审批
     */
    public Integer second_approver_id;

    @JsonIgnore
    public boolean second_approver_idDirtyFlag;
    
    /**
     * 第二次审批
     */
    public String second_approver_id_text;

    @JsonIgnore
    public boolean second_approver_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 验证人
     */
    public String validation_type;

    @JsonIgnore
    public boolean validation_typeDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [能批准]
     */
    @JsonProperty("can_approve")
    public String getCan_approve(){
        return this.can_approve ;
    }

    /**
     * 设置 [能批准]
     */
    @JsonProperty("can_approve")
    public void setCan_approve(String  can_approve){
        this.can_approve = can_approve ;
        this.can_approveDirtyFlag = true ;
    }

     /**
     * 获取 [能批准]脏标记
     */
    @JsonIgnore
    public boolean getCan_approveDirtyFlag(){
        return this.can_approveDirtyFlag ;
    }   

    /**
     * 获取 [能重置]
     */
    @JsonProperty("can_reset")
    public String getCan_reset(){
        return this.can_reset ;
    }

    /**
     * 设置 [能重置]
     */
    @JsonProperty("can_reset")
    public void setCan_reset(String  can_reset){
        this.can_reset = can_reset ;
        this.can_resetDirtyFlag = true ;
    }

     /**
     * 获取 [能重置]脏标记
     */
    @JsonIgnore
    public boolean getCan_resetDirtyFlag(){
        return this.can_resetDirtyFlag ;
    }   

    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return this.date_from ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return this.date_fromDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return this.date_to ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return this.date_toDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [要求的（天/小时）]
     */
    @JsonProperty("duration_display")
    public String getDuration_display(){
        return this.duration_display ;
    }

    /**
     * 设置 [要求的（天/小时）]
     */
    @JsonProperty("duration_display")
    public void setDuration_display(String  duration_display){
        this.duration_display = duration_display ;
        this.duration_displayDirtyFlag = true ;
    }

     /**
     * 获取 [要求的（天/小时）]脏标记
     */
    @JsonIgnore
    public boolean getDuration_displayDirtyFlag(){
        return this.duration_displayDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [首次审批]
     */
    @JsonProperty("first_approver_id")
    public Integer getFirst_approver_id(){
        return this.first_approver_id ;
    }

    /**
     * 设置 [首次审批]
     */
    @JsonProperty("first_approver_id")
    public void setFirst_approver_id(Integer  first_approver_id){
        this.first_approver_id = first_approver_id ;
        this.first_approver_idDirtyFlag = true ;
    }

     /**
     * 获取 [首次审批]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_idDirtyFlag(){
        return this.first_approver_idDirtyFlag ;
    }   

    /**
     * 获取 [首次审批]
     */
    @JsonProperty("first_approver_id_text")
    public String getFirst_approver_id_text(){
        return this.first_approver_id_text ;
    }

    /**
     * 设置 [首次审批]
     */
    @JsonProperty("first_approver_id_text")
    public void setFirst_approver_id_text(String  first_approver_id_text){
        this.first_approver_id_text = first_approver_id_text ;
        this.first_approver_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [首次审批]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_id_textDirtyFlag(){
        return this.first_approver_id_textDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public Integer getHoliday_status_id(){
        return this.holiday_status_id ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public void setHoliday_status_id(Integer  holiday_status_id){
        this.holiday_status_id = holiday_status_id ;
        this.holiday_status_idDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_idDirtyFlag(){
        return this.holiday_status_idDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public String getHoliday_status_id_text(){
        return this.holiday_status_id_text ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public void setHoliday_status_id_text(String  holiday_status_id_text){
        this.holiday_status_id_text = holiday_status_id_text ;
        this.holiday_status_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_id_textDirtyFlag(){
        return this.holiday_status_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分配模式]
     */
    @JsonProperty("holiday_type")
    public String getHoliday_type(){
        return this.holiday_type ;
    }

    /**
     * 设置 [分配模式]
     */
    @JsonProperty("holiday_type")
    public void setHoliday_type(String  holiday_type){
        this.holiday_type = holiday_type ;
        this.holiday_typeDirtyFlag = true ;
    }

     /**
     * 获取 [分配模式]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_typeDirtyFlag(){
        return this.holiday_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [休假]
     */
    @JsonProperty("leave_type_request_unit")
    public String getLeave_type_request_unit(){
        return this.leave_type_request_unit ;
    }

    /**
     * 设置 [休假]
     */
    @JsonProperty("leave_type_request_unit")
    public void setLeave_type_request_unit(String  leave_type_request_unit){
        this.leave_type_request_unit = leave_type_request_unit ;
        this.leave_type_request_unitDirtyFlag = true ;
    }

     /**
     * 获取 [休假]脏标记
     */
    @JsonIgnore
    public boolean getLeave_type_request_unitDirtyFlag(){
        return this.leave_type_request_unitDirtyFlag ;
    }   

    /**
     * 获取 [链接申请]
     */
    @JsonProperty("linked_request_ids")
    public String getLinked_request_ids(){
        return this.linked_request_ids ;
    }

    /**
     * 设置 [链接申请]
     */
    @JsonProperty("linked_request_ids")
    public void setLinked_request_ids(String  linked_request_ids){
        this.linked_request_ids = linked_request_ids ;
        this.linked_request_idsDirtyFlag = true ;
    }

     /**
     * 获取 [链接申请]脏标记
     */
    @JsonIgnore
    public boolean getLinked_request_idsDirtyFlag(){
        return this.linked_request_idsDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return this.manager_id ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return this.manager_idDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return this.manager_id_text ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return this.manager_id_textDirtyFlag ;
    }   

    /**
     * 获取 [会议]
     */
    @JsonProperty("meeting_id")
    public Integer getMeeting_id(){
        return this.meeting_id ;
    }

    /**
     * 设置 [会议]
     */
    @JsonProperty("meeting_id")
    public void setMeeting_id(Integer  meeting_id){
        this.meeting_id = meeting_id ;
        this.meeting_idDirtyFlag = true ;
    }

     /**
     * 获取 [会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_idDirtyFlag(){
        return this.meeting_idDirtyFlag ;
    }   

    /**
     * 获取 [会议]
     */
    @JsonProperty("meeting_id_text")
    public String getMeeting_id_text(){
        return this.meeting_id_text ;
    }

    /**
     * 设置 [会议]
     */
    @JsonProperty("meeting_id_text")
    public void setMeeting_id_text(String  meeting_id_text){
        this.meeting_id_text = meeting_id_text ;
        this.meeting_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_id_textDirtyFlag(){
        return this.meeting_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [信息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [信息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("mode_company_id")
    public Integer getMode_company_id(){
        return this.mode_company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("mode_company_id")
    public void setMode_company_id(Integer  mode_company_id){
        this.mode_company_id = mode_company_id ;
        this.mode_company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_idDirtyFlag(){
        return this.mode_company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("mode_company_id_text")
    public String getMode_company_id_text(){
        return this.mode_company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("mode_company_id_text")
    public void setMode_company_id_text(String  mode_company_id_text){
        this.mode_company_id_text = mode_company_id_text ;
        this.mode_company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_id_textDirtyFlag(){
        return this.mode_company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [理由]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [理由]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [理由]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [持续时间（天）]
     */
    @JsonProperty("number_of_days")
    public Double getNumber_of_days(){
        return this.number_of_days ;
    }

    /**
     * 设置 [持续时间（天）]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Double  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间（天）]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return this.number_of_daysDirtyFlag ;
    }   

    /**
     * 获取 [持续时间（天）]
     */
    @JsonProperty("number_of_days_display")
    public Double getNumber_of_days_display(){
        return this.number_of_days_display ;
    }

    /**
     * 设置 [持续时间（天）]
     */
    @JsonProperty("number_of_days_display")
    public void setNumber_of_days_display(Double  number_of_days_display){
        this.number_of_days_display = number_of_days_display ;
        this.number_of_days_displayDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间（天）]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_days_displayDirtyFlag(){
        return this.number_of_days_displayDirtyFlag ;
    }   

    /**
     * 获取 [持续时间（小时）]
     */
    @JsonProperty("number_of_hours_display")
    public Double getNumber_of_hours_display(){
        return this.number_of_hours_display ;
    }

    /**
     * 设置 [持续时间（小时）]
     */
    @JsonProperty("number_of_hours_display")
    public void setNumber_of_hours_display(Double  number_of_hours_display){
        this.number_of_hours_display = number_of_hours_display ;
        this.number_of_hours_displayDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间（小时）]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_hours_displayDirtyFlag(){
        return this.number_of_hours_displayDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [反映在最近的工资单中]
     */
    @JsonProperty("payslip_status")
    public String getPayslip_status(){
        return this.payslip_status ;
    }

    /**
     * 设置 [反映在最近的工资单中]
     */
    @JsonProperty("payslip_status")
    public void setPayslip_status(String  payslip_status){
        this.payslip_status = payslip_status ;
        this.payslip_statusDirtyFlag = true ;
    }

     /**
     * 获取 [反映在最近的工资单中]脏标记
     */
    @JsonIgnore
    public boolean getPayslip_statusDirtyFlag(){
        return this.payslip_statusDirtyFlag ;
    }   

    /**
     * 获取 [HR 备注]
     */
    @JsonProperty("report_note")
    public String getReport_note(){
        return this.report_note ;
    }

    /**
     * 设置 [HR 备注]
     */
    @JsonProperty("report_note")
    public void setReport_note(String  report_note){
        this.report_note = report_note ;
        this.report_noteDirtyFlag = true ;
    }

     /**
     * 获取 [HR 备注]脏标记
     */
    @JsonIgnore
    public boolean getReport_noteDirtyFlag(){
        return this.report_noteDirtyFlag ;
    }   

    /**
     * 获取 [请假开始日期]
     */
    @JsonProperty("request_date_from")
    public Timestamp getRequest_date_from(){
        return this.request_date_from ;
    }

    /**
     * 设置 [请假开始日期]
     */
    @JsonProperty("request_date_from")
    public void setRequest_date_from(Timestamp  request_date_from){
        this.request_date_from = request_date_from ;
        this.request_date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [请假开始日期]脏标记
     */
    @JsonIgnore
    public boolean getRequest_date_fromDirtyFlag(){
        return this.request_date_fromDirtyFlag ;
    }   

    /**
     * 获取 [日期开始]
     */
    @JsonProperty("request_date_from_period")
    public String getRequest_date_from_period(){
        return this.request_date_from_period ;
    }

    /**
     * 设置 [日期开始]
     */
    @JsonProperty("request_date_from_period")
    public void setRequest_date_from_period(String  request_date_from_period){
        this.request_date_from_period = request_date_from_period ;
        this.request_date_from_periodDirtyFlag = true ;
    }

     /**
     * 获取 [日期开始]脏标记
     */
    @JsonIgnore
    public boolean getRequest_date_from_periodDirtyFlag(){
        return this.request_date_from_periodDirtyFlag ;
    }   

    /**
     * 获取 [请求结束日期]
     */
    @JsonProperty("request_date_to")
    public Timestamp getRequest_date_to(){
        return this.request_date_to ;
    }

    /**
     * 设置 [请求结束日期]
     */
    @JsonProperty("request_date_to")
    public void setRequest_date_to(Timestamp  request_date_to){
        this.request_date_to = request_date_to ;
        this.request_date_toDirtyFlag = true ;
    }

     /**
     * 获取 [请求结束日期]脏标记
     */
    @JsonIgnore
    public boolean getRequest_date_toDirtyFlag(){
        return this.request_date_toDirtyFlag ;
    }   

    /**
     * 获取 [时间从]
     */
    @JsonProperty("request_hour_from")
    public String getRequest_hour_from(){
        return this.request_hour_from ;
    }

    /**
     * 设置 [时间从]
     */
    @JsonProperty("request_hour_from")
    public void setRequest_hour_from(String  request_hour_from){
        this.request_hour_from = request_hour_from ;
        this.request_hour_fromDirtyFlag = true ;
    }

     /**
     * 获取 [时间从]脏标记
     */
    @JsonIgnore
    public boolean getRequest_hour_fromDirtyFlag(){
        return this.request_hour_fromDirtyFlag ;
    }   

    /**
     * 获取 [时间到]
     */
    @JsonProperty("request_hour_to")
    public String getRequest_hour_to(){
        return this.request_hour_to ;
    }

    /**
     * 设置 [时间到]
     */
    @JsonProperty("request_hour_to")
    public void setRequest_hour_to(String  request_hour_to){
        this.request_hour_to = request_hour_to ;
        this.request_hour_toDirtyFlag = true ;
    }

     /**
     * 获取 [时间到]脏标记
     */
    @JsonIgnore
    public boolean getRequest_hour_toDirtyFlag(){
        return this.request_hour_toDirtyFlag ;
    }   

    /**
     * 获取 [长达数天的定制时间]
     */
    @JsonProperty("request_unit_custom")
    public String getRequest_unit_custom(){
        return this.request_unit_custom ;
    }

    /**
     * 设置 [长达数天的定制时间]
     */
    @JsonProperty("request_unit_custom")
    public void setRequest_unit_custom(String  request_unit_custom){
        this.request_unit_custom = request_unit_custom ;
        this.request_unit_customDirtyFlag = true ;
    }

     /**
     * 获取 [长达数天的定制时间]脏标记
     */
    @JsonIgnore
    public boolean getRequest_unit_customDirtyFlag(){
        return this.request_unit_customDirtyFlag ;
    }   

    /**
     * 获取 [半天]
     */
    @JsonProperty("request_unit_half")
    public String getRequest_unit_half(){
        return this.request_unit_half ;
    }

    /**
     * 设置 [半天]
     */
    @JsonProperty("request_unit_half")
    public void setRequest_unit_half(String  request_unit_half){
        this.request_unit_half = request_unit_half ;
        this.request_unit_halfDirtyFlag = true ;
    }

     /**
     * 获取 [半天]脏标记
     */
    @JsonIgnore
    public boolean getRequest_unit_halfDirtyFlag(){
        return this.request_unit_halfDirtyFlag ;
    }   

    /**
     * 获取 [自定义时间]
     */
    @JsonProperty("request_unit_hours")
    public String getRequest_unit_hours(){
        return this.request_unit_hours ;
    }

    /**
     * 设置 [自定义时间]
     */
    @JsonProperty("request_unit_hours")
    public void setRequest_unit_hours(String  request_unit_hours){
        this.request_unit_hours = request_unit_hours ;
        this.request_unit_hoursDirtyFlag = true ;
    }

     /**
     * 获取 [自定义时间]脏标记
     */
    @JsonIgnore
    public boolean getRequest_unit_hoursDirtyFlag(){
        return this.request_unit_hoursDirtyFlag ;
    }   

    /**
     * 获取 [第二次审批]
     */
    @JsonProperty("second_approver_id")
    public Integer getSecond_approver_id(){
        return this.second_approver_id ;
    }

    /**
     * 设置 [第二次审批]
     */
    @JsonProperty("second_approver_id")
    public void setSecond_approver_id(Integer  second_approver_id){
        this.second_approver_id = second_approver_id ;
        this.second_approver_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二次审批]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_idDirtyFlag(){
        return this.second_approver_idDirtyFlag ;
    }   

    /**
     * 获取 [第二次审批]
     */
    @JsonProperty("second_approver_id_text")
    public String getSecond_approver_id_text(){
        return this.second_approver_id_text ;
    }

    /**
     * 设置 [第二次审批]
     */
    @JsonProperty("second_approver_id_text")
    public void setSecond_approver_id_text(String  second_approver_id_text){
        this.second_approver_id_text = second_approver_id_text ;
        this.second_approver_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二次审批]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_id_textDirtyFlag(){
        return this.second_approver_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [验证人]
     */
    @JsonProperty("validation_type")
    public String getValidation_type(){
        return this.validation_type ;
    }

    /**
     * 设置 [验证人]
     */
    @JsonProperty("validation_type")
    public void setValidation_type(String  validation_type){
        this.validation_type = validation_type ;
        this.validation_typeDirtyFlag = true ;
    }

     /**
     * 获取 [验证人]脏标记
     */
    @JsonIgnore
    public boolean getValidation_typeDirtyFlag(){
        return this.validation_typeDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

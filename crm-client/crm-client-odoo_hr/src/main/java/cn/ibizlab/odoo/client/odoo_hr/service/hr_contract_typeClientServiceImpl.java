package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_contract_type;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_contract_typeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_contract_typeImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_contract_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_contract_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_contract_typeClientServiceImpl implements Ihr_contract_typeClientService {

    hr_contract_typeFeignClient hr_contract_typeFeignClient;

    @Autowired
    public hr_contract_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_contract_typeFeignClient = nameBuilder.target(hr_contract_typeFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_contract_typeFeignClient = nameBuilder.target(hr_contract_typeFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_contract_type createModel() {
		return new hr_contract_typeImpl();
	}


    public void removeBatch(List<Ihr_contract_type> hr_contract_types){
        if(hr_contract_types!=null){
            List<hr_contract_typeImpl> list = new ArrayList<hr_contract_typeImpl>();
            for(Ihr_contract_type ihr_contract_type :hr_contract_types){
                list.add((hr_contract_typeImpl)ihr_contract_type) ;
            }
            hr_contract_typeFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ihr_contract_type hr_contract_type){
        hr_contract_typeFeignClient.remove(hr_contract_type.getId()) ;
    }


    public void createBatch(List<Ihr_contract_type> hr_contract_types){
        if(hr_contract_types!=null){
            List<hr_contract_typeImpl> list = new ArrayList<hr_contract_typeImpl>();
            for(Ihr_contract_type ihr_contract_type :hr_contract_types){
                list.add((hr_contract_typeImpl)ihr_contract_type) ;
            }
            hr_contract_typeFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ihr_contract_type> hr_contract_types){
        if(hr_contract_types!=null){
            List<hr_contract_typeImpl> list = new ArrayList<hr_contract_typeImpl>();
            for(Ihr_contract_type ihr_contract_type :hr_contract_types){
                list.add((hr_contract_typeImpl)ihr_contract_type) ;
            }
            hr_contract_typeFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ihr_contract_type hr_contract_type){
        Ihr_contract_type clientModel = hr_contract_typeFeignClient.update(hr_contract_type.getId(),(hr_contract_typeImpl)hr_contract_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract_type.getClass(), false);
        copier.copy(clientModel, hr_contract_type, null);
    }


    public void create(Ihr_contract_type hr_contract_type){
        Ihr_contract_type clientModel = hr_contract_typeFeignClient.create((hr_contract_typeImpl)hr_contract_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract_type.getClass(), false);
        copier.copy(clientModel, hr_contract_type, null);
    }


    public Page<Ihr_contract_type> fetchDefault(SearchContext context){
        Page<hr_contract_typeImpl> page = this.hr_contract_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ihr_contract_type hr_contract_type){
        Ihr_contract_type clientModel = hr_contract_typeFeignClient.get(hr_contract_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract_type.getClass(), false);
        copier.copy(clientModel, hr_contract_type, null);
    }


    public Page<Ihr_contract_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_contract_type hr_contract_type){
        Ihr_contract_type clientModel = hr_contract_typeFeignClient.getDraft(hr_contract_type.getId(),(hr_contract_typeImpl)hr_contract_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_contract_type.getClass(), false);
        copier.copy(clientModel, hr_contract_type, null);
    }



}


package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_applicantClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicantImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_applicantFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_applicant] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_applicantClientServiceImpl implements Ihr_applicantClientService {

    hr_applicantFeignClient hr_applicantFeignClient;

    @Autowired
    public hr_applicantClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_applicantFeignClient = nameBuilder.target(hr_applicantFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_applicantFeignClient = nameBuilder.target(hr_applicantFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_applicant createModel() {
		return new hr_applicantImpl();
	}


    public void get(Ihr_applicant hr_applicant){
        Ihr_applicant clientModel = hr_applicantFeignClient.get(hr_applicant.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant.getClass(), false);
        copier.copy(clientModel, hr_applicant, null);
    }


    public Page<Ihr_applicant> fetchDefault(SearchContext context){
        Page<hr_applicantImpl> page = this.hr_applicantFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ihr_applicant> hr_applicants){
        if(hr_applicants!=null){
            List<hr_applicantImpl> list = new ArrayList<hr_applicantImpl>();
            for(Ihr_applicant ihr_applicant :hr_applicants){
                list.add((hr_applicantImpl)ihr_applicant) ;
            }
            hr_applicantFeignClient.createBatch(list) ;
        }
    }


    public void create(Ihr_applicant hr_applicant){
        Ihr_applicant clientModel = hr_applicantFeignClient.create((hr_applicantImpl)hr_applicant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant.getClass(), false);
        copier.copy(clientModel, hr_applicant, null);
    }


    public void remove(Ihr_applicant hr_applicant){
        hr_applicantFeignClient.remove(hr_applicant.getId()) ;
    }


    public void updateBatch(List<Ihr_applicant> hr_applicants){
        if(hr_applicants!=null){
            List<hr_applicantImpl> list = new ArrayList<hr_applicantImpl>();
            for(Ihr_applicant ihr_applicant :hr_applicants){
                list.add((hr_applicantImpl)ihr_applicant) ;
            }
            hr_applicantFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ihr_applicant hr_applicant){
        Ihr_applicant clientModel = hr_applicantFeignClient.update(hr_applicant.getId(),(hr_applicantImpl)hr_applicant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant.getClass(), false);
        copier.copy(clientModel, hr_applicant, null);
    }


    public void removeBatch(List<Ihr_applicant> hr_applicants){
        if(hr_applicants!=null){
            List<hr_applicantImpl> list = new ArrayList<hr_applicantImpl>();
            for(Ihr_applicant ihr_applicant :hr_applicants){
                list.add((hr_applicantImpl)ihr_applicant) ;
            }
            hr_applicantFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ihr_applicant> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_applicant hr_applicant){
        Ihr_applicant clientModel = hr_applicantFeignClient.getDraft(hr_applicant.getId(),(hr_applicantImpl)hr_applicant) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_applicant.getClass(), false);
        copier.copy(clientModel, hr_applicant, null);
    }



}


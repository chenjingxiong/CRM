package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant_category;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicant_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_applicant_category] 服务对象接口
 */
public interface hr_applicant_categoryFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicant_categories/updatebatch")
    public hr_applicant_categoryImpl updateBatch(@RequestBody List<hr_applicant_categoryImpl> hr_applicant_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicant_categories/{id}")
    public hr_applicant_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicant_categories")
    public hr_applicant_categoryImpl create(@RequestBody hr_applicant_categoryImpl hr_applicant_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicant_categories/removebatch")
    public hr_applicant_categoryImpl removeBatch(@RequestBody List<hr_applicant_categoryImpl> hr_applicant_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicant_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicant_categories/fetchdefault")
    public Page<hr_applicant_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicant_categories/{id}")
    public hr_applicant_categoryImpl update(@PathVariable("id") Integer id,@RequestBody hr_applicant_categoryImpl hr_applicant_category);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicant_categories/createbatch")
    public hr_applicant_categoryImpl createBatch(@RequestBody List<hr_applicant_categoryImpl> hr_applicant_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicant_categories/select")
    public Page<hr_applicant_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicant_categories/{id}/getdraft")
    public hr_applicant_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_applicant_categoryImpl hr_applicant_category);



}

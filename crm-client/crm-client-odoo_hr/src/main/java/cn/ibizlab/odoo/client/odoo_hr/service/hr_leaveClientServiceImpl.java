package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_leaveClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leaveImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_leaveFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_leave] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_leaveClientServiceImpl implements Ihr_leaveClientService {

    hr_leaveFeignClient hr_leaveFeignClient;

    @Autowired
    public hr_leaveClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leaveFeignClient = nameBuilder.target(hr_leaveFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leaveFeignClient = nameBuilder.target(hr_leaveFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_leave createModel() {
		return new hr_leaveImpl();
	}


    public Page<Ihr_leave> fetchDefault(SearchContext context){
        Page<hr_leaveImpl> page = this.hr_leaveFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ihr_leave> hr_leaves){
        if(hr_leaves!=null){
            List<hr_leaveImpl> list = new ArrayList<hr_leaveImpl>();
            for(Ihr_leave ihr_leave :hr_leaves){
                list.add((hr_leaveImpl)ihr_leave) ;
            }
            hr_leaveFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ihr_leave hr_leave){
        hr_leaveFeignClient.remove(hr_leave.getId()) ;
    }


    public void removeBatch(List<Ihr_leave> hr_leaves){
        if(hr_leaves!=null){
            List<hr_leaveImpl> list = new ArrayList<hr_leaveImpl>();
            for(Ihr_leave ihr_leave :hr_leaves){
                list.add((hr_leaveImpl)ihr_leave) ;
            }
            hr_leaveFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ihr_leave> hr_leaves){
        if(hr_leaves!=null){
            List<hr_leaveImpl> list = new ArrayList<hr_leaveImpl>();
            for(Ihr_leave ihr_leave :hr_leaves){
                list.add((hr_leaveImpl)ihr_leave) ;
            }
            hr_leaveFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ihr_leave hr_leave){
        Ihr_leave clientModel = hr_leaveFeignClient.create((hr_leaveImpl)hr_leave) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave.getClass(), false);
        copier.copy(clientModel, hr_leave, null);
    }


    public void get(Ihr_leave hr_leave){
        Ihr_leave clientModel = hr_leaveFeignClient.get(hr_leave.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave.getClass(), false);
        copier.copy(clientModel, hr_leave, null);
    }


    public void update(Ihr_leave hr_leave){
        Ihr_leave clientModel = hr_leaveFeignClient.update(hr_leave.getId(),(hr_leaveImpl)hr_leave) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave.getClass(), false);
        copier.copy(clientModel, hr_leave, null);
    }


    public Page<Ihr_leave> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_leave hr_leave){
        Ihr_leave clientModel = hr_leaveFeignClient.getDraft(hr_leave.getId(),(hr_leaveImpl)hr_leave) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave.getClass(), false);
        copier.copy(clientModel, hr_leave, null);
    }



}


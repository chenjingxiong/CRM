package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_dept;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_holidays_summary_deptClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_deptImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_holidays_summary_deptFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_holidays_summary_deptClientServiceImpl implements Ihr_holidays_summary_deptClientService {

    hr_holidays_summary_deptFeignClient hr_holidays_summary_deptFeignClient;

    @Autowired
    public hr_holidays_summary_deptClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_holidays_summary_deptFeignClient = nameBuilder.target(hr_holidays_summary_deptFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_holidays_summary_deptFeignClient = nameBuilder.target(hr_holidays_summary_deptFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_holidays_summary_dept createModel() {
		return new hr_holidays_summary_deptImpl();
	}


    public void remove(Ihr_holidays_summary_dept hr_holidays_summary_dept){
        hr_holidays_summary_deptFeignClient.remove(hr_holidays_summary_dept.getId()) ;
    }


    public Page<Ihr_holidays_summary_dept> fetchDefault(SearchContext context){
        Page<hr_holidays_summary_deptImpl> page = this.hr_holidays_summary_deptFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ihr_holidays_summary_dept hr_holidays_summary_dept){
        Ihr_holidays_summary_dept clientModel = hr_holidays_summary_deptFeignClient.get(hr_holidays_summary_dept.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_dept.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_dept, null);
    }


    public void createBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
        if(hr_holidays_summary_depts!=null){
            List<hr_holidays_summary_deptImpl> list = new ArrayList<hr_holidays_summary_deptImpl>();
            for(Ihr_holidays_summary_dept ihr_holidays_summary_dept :hr_holidays_summary_depts){
                list.add((hr_holidays_summary_deptImpl)ihr_holidays_summary_dept) ;
            }
            hr_holidays_summary_deptFeignClient.createBatch(list) ;
        }
    }


    public void create(Ihr_holidays_summary_dept hr_holidays_summary_dept){
        Ihr_holidays_summary_dept clientModel = hr_holidays_summary_deptFeignClient.create((hr_holidays_summary_deptImpl)hr_holidays_summary_dept) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_dept.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_dept, null);
    }


    public void removeBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
        if(hr_holidays_summary_depts!=null){
            List<hr_holidays_summary_deptImpl> list = new ArrayList<hr_holidays_summary_deptImpl>();
            for(Ihr_holidays_summary_dept ihr_holidays_summary_dept :hr_holidays_summary_depts){
                list.add((hr_holidays_summary_deptImpl)ihr_holidays_summary_dept) ;
            }
            hr_holidays_summary_deptFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ihr_holidays_summary_dept hr_holidays_summary_dept){
        Ihr_holidays_summary_dept clientModel = hr_holidays_summary_deptFeignClient.update(hr_holidays_summary_dept.getId(),(hr_holidays_summary_deptImpl)hr_holidays_summary_dept) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_dept.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_dept, null);
    }


    public void updateBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
        if(hr_holidays_summary_depts!=null){
            List<hr_holidays_summary_deptImpl> list = new ArrayList<hr_holidays_summary_deptImpl>();
            for(Ihr_holidays_summary_dept ihr_holidays_summary_dept :hr_holidays_summary_depts){
                list.add((hr_holidays_summary_deptImpl)ihr_holidays_summary_dept) ;
            }
            hr_holidays_summary_deptFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ihr_holidays_summary_dept> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_holidays_summary_dept hr_holidays_summary_dept){
        Ihr_holidays_summary_dept clientModel = hr_holidays_summary_deptFeignClient.getDraft(hr_holidays_summary_dept.getId(),(hr_holidays_summary_deptImpl)hr_holidays_summary_dept) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_holidays_summary_dept.getClass(), false);
        copier.copy(clientModel, hr_holidays_summary_dept, null);
    }



}


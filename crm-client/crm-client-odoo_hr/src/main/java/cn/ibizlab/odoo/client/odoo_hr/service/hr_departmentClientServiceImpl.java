package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_department;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_departmentClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_departmentImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_departmentFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_department] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_departmentClientServiceImpl implements Ihr_departmentClientService {

    hr_departmentFeignClient hr_departmentFeignClient;

    @Autowired
    public hr_departmentClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_departmentFeignClient = nameBuilder.target(hr_departmentFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_departmentFeignClient = nameBuilder.target(hr_departmentFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_department createModel() {
		return new hr_departmentImpl();
	}


    public void remove(Ihr_department hr_department){
        hr_departmentFeignClient.remove(hr_department.getId()) ;
    }


    public void get(Ihr_department hr_department){
        Ihr_department clientModel = hr_departmentFeignClient.get(hr_department.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_department.getClass(), false);
        copier.copy(clientModel, hr_department, null);
    }


    public void update(Ihr_department hr_department){
        Ihr_department clientModel = hr_departmentFeignClient.update(hr_department.getId(),(hr_departmentImpl)hr_department) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_department.getClass(), false);
        copier.copy(clientModel, hr_department, null);
    }


    public void removeBatch(List<Ihr_department> hr_departments){
        if(hr_departments!=null){
            List<hr_departmentImpl> list = new ArrayList<hr_departmentImpl>();
            for(Ihr_department ihr_department :hr_departments){
                list.add((hr_departmentImpl)ihr_department) ;
            }
            hr_departmentFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ihr_department> hr_departments){
        if(hr_departments!=null){
            List<hr_departmentImpl> list = new ArrayList<hr_departmentImpl>();
            for(Ihr_department ihr_department :hr_departments){
                list.add((hr_departmentImpl)ihr_department) ;
            }
            hr_departmentFeignClient.createBatch(list) ;
        }
    }


    public void create(Ihr_department hr_department){
        Ihr_department clientModel = hr_departmentFeignClient.create((hr_departmentImpl)hr_department) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_department.getClass(), false);
        copier.copy(clientModel, hr_department, null);
    }


    public Page<Ihr_department> fetchDefault(SearchContext context){
        Page<hr_departmentImpl> page = this.hr_departmentFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ihr_department> hr_departments){
        if(hr_departments!=null){
            List<hr_departmentImpl> list = new ArrayList<hr_departmentImpl>();
            for(Ihr_department ihr_department :hr_departments){
                list.add((hr_departmentImpl)ihr_department) ;
            }
            hr_departmentFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ihr_department> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_department hr_department){
        Ihr_department clientModel = hr_departmentFeignClient.getDraft(hr_department.getId(),(hr_departmentImpl)hr_department) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_department.getClass(), false);
        copier.copy(clientModel, hr_department, null);
    }



}


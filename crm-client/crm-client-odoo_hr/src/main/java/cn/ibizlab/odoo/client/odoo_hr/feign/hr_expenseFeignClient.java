package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_expense;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expenseImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_expense] 服务对象接口
 */
public interface hr_expenseFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expenses/removebatch")
    public hr_expenseImpl removeBatch(@RequestBody List<hr_expenseImpl> hr_expenses);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expenses")
    public hr_expenseImpl create(@RequestBody hr_expenseImpl hr_expense);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expenses/{id}")
    public hr_expenseImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expenses/fetchdefault")
    public Page<hr_expenseImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expenses/createbatch")
    public hr_expenseImpl createBatch(@RequestBody List<hr_expenseImpl> hr_expenses);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expenses/updatebatch")
    public hr_expenseImpl updateBatch(@RequestBody List<hr_expenseImpl> hr_expenses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expenses/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expenses/{id}")
    public hr_expenseImpl update(@PathVariable("id") Integer id,@RequestBody hr_expenseImpl hr_expense);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expenses/select")
    public Page<hr_expenseImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expenses/{id}/getdraft")
    public hr_expenseImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_expenseImpl hr_expense);



}

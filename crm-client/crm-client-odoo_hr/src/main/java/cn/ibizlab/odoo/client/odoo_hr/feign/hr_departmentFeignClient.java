package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_department;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_departmentImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_department] 服务对象接口
 */
public interface hr_departmentFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_departments/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_departments/{id}")
    public hr_departmentImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_departments/{id}")
    public hr_departmentImpl update(@PathVariable("id") Integer id,@RequestBody hr_departmentImpl hr_department);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_departments/removebatch")
    public hr_departmentImpl removeBatch(@RequestBody List<hr_departmentImpl> hr_departments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_departments/createbatch")
    public hr_departmentImpl createBatch(@RequestBody List<hr_departmentImpl> hr_departments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_departments")
    public hr_departmentImpl create(@RequestBody hr_departmentImpl hr_department);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_departments/fetchdefault")
    public Page<hr_departmentImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_departments/updatebatch")
    public hr_departmentImpl updateBatch(@RequestBody List<hr_departmentImpl> hr_departments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_departments/select")
    public Page<hr_departmentImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_departments/{id}/getdraft")
    public hr_departmentImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_departmentImpl hr_department);



}

package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_employee;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_employeeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
public interface hr_holidays_summary_employeeFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_employees/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_employees/fetchdefault")
    public Page<hr_holidays_summary_employeeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_employees/updatebatch")
    public hr_holidays_summary_employeeImpl updateBatch(@RequestBody List<hr_holidays_summary_employeeImpl> hr_holidays_summary_employees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_employees/removebatch")
    public hr_holidays_summary_employeeImpl removeBatch(@RequestBody List<hr_holidays_summary_employeeImpl> hr_holidays_summary_employees);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_employees/createbatch")
    public hr_holidays_summary_employeeImpl createBatch(@RequestBody List<hr_holidays_summary_employeeImpl> hr_holidays_summary_employees);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_employees/{id}")
    public hr_holidays_summary_employeeImpl update(@PathVariable("id") Integer id,@RequestBody hr_holidays_summary_employeeImpl hr_holidays_summary_employee);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_employees/{id}")
    public hr_holidays_summary_employeeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_employees")
    public hr_holidays_summary_employeeImpl create(@RequestBody hr_holidays_summary_employeeImpl hr_holidays_summary_employee);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_employees/select")
    public Page<hr_holidays_summary_employeeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_employees/{id}/getdraft")
    public hr_holidays_summary_employeeImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_holidays_summary_employeeImpl hr_holidays_summary_employee);



}

package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_expenseClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expenseImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_expenseFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_expense] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_expenseClientServiceImpl implements Ihr_expenseClientService {

    hr_expenseFeignClient hr_expenseFeignClient;

    @Autowired
    public hr_expenseClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expenseFeignClient = nameBuilder.target(hr_expenseFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expenseFeignClient = nameBuilder.target(hr_expenseFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_expense createModel() {
		return new hr_expenseImpl();
	}


    public void removeBatch(List<Ihr_expense> hr_expenses){
        if(hr_expenses!=null){
            List<hr_expenseImpl> list = new ArrayList<hr_expenseImpl>();
            for(Ihr_expense ihr_expense :hr_expenses){
                list.add((hr_expenseImpl)ihr_expense) ;
            }
            hr_expenseFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ihr_expense hr_expense){
        Ihr_expense clientModel = hr_expenseFeignClient.create((hr_expenseImpl)hr_expense) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense.getClass(), false);
        copier.copy(clientModel, hr_expense, null);
    }


    public void get(Ihr_expense hr_expense){
        Ihr_expense clientModel = hr_expenseFeignClient.get(hr_expense.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense.getClass(), false);
        copier.copy(clientModel, hr_expense, null);
    }


    public Page<Ihr_expense> fetchDefault(SearchContext context){
        Page<hr_expenseImpl> page = this.hr_expenseFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ihr_expense> hr_expenses){
        if(hr_expenses!=null){
            List<hr_expenseImpl> list = new ArrayList<hr_expenseImpl>();
            for(Ihr_expense ihr_expense :hr_expenses){
                list.add((hr_expenseImpl)ihr_expense) ;
            }
            hr_expenseFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ihr_expense> hr_expenses){
        if(hr_expenses!=null){
            List<hr_expenseImpl> list = new ArrayList<hr_expenseImpl>();
            for(Ihr_expense ihr_expense :hr_expenses){
                list.add((hr_expenseImpl)ihr_expense) ;
            }
            hr_expenseFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ihr_expense hr_expense){
        hr_expenseFeignClient.remove(hr_expense.getId()) ;
    }


    public void update(Ihr_expense hr_expense){
        Ihr_expense clientModel = hr_expenseFeignClient.update(hr_expense.getId(),(hr_expenseImpl)hr_expense) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense.getClass(), false);
        copier.copy(clientModel, hr_expense, null);
    }


    public Page<Ihr_expense> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_expense hr_expense){
        Ihr_expense clientModel = hr_expenseFeignClient.getDraft(hr_expense.getId(),(hr_expenseImpl)hr_expense) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense.getClass(), false);
        copier.copy(clientModel, hr_expense, null);
    }



}


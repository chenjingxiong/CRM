package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_reportClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_reportImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_leave_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_leave_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_leave_reportClientServiceImpl implements Ihr_leave_reportClientService {

    hr_leave_reportFeignClient hr_leave_reportFeignClient;

    @Autowired
    public hr_leave_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_reportFeignClient = nameBuilder.target(hr_leave_reportFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_leave_reportFeignClient = nameBuilder.target(hr_leave_reportFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_leave_report createModel() {
		return new hr_leave_reportImpl();
	}


    public void remove(Ihr_leave_report hr_leave_report){
        hr_leave_reportFeignClient.remove(hr_leave_report.getId()) ;
    }


    public void update(Ihr_leave_report hr_leave_report){
        Ihr_leave_report clientModel = hr_leave_reportFeignClient.update(hr_leave_report.getId(),(hr_leave_reportImpl)hr_leave_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_report.getClass(), false);
        copier.copy(clientModel, hr_leave_report, null);
    }


    public void removeBatch(List<Ihr_leave_report> hr_leave_reports){
        if(hr_leave_reports!=null){
            List<hr_leave_reportImpl> list = new ArrayList<hr_leave_reportImpl>();
            for(Ihr_leave_report ihr_leave_report :hr_leave_reports){
                list.add((hr_leave_reportImpl)ihr_leave_report) ;
            }
            hr_leave_reportFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ihr_leave_report> hr_leave_reports){
        if(hr_leave_reports!=null){
            List<hr_leave_reportImpl> list = new ArrayList<hr_leave_reportImpl>();
            for(Ihr_leave_report ihr_leave_report :hr_leave_reports){
                list.add((hr_leave_reportImpl)ihr_leave_report) ;
            }
            hr_leave_reportFeignClient.createBatch(list) ;
        }
    }


    public Page<Ihr_leave_report> fetchDefault(SearchContext context){
        Page<hr_leave_reportImpl> page = this.hr_leave_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ihr_leave_report hr_leave_report){
        Ihr_leave_report clientModel = hr_leave_reportFeignClient.create((hr_leave_reportImpl)hr_leave_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_report.getClass(), false);
        copier.copy(clientModel, hr_leave_report, null);
    }


    public void get(Ihr_leave_report hr_leave_report){
        Ihr_leave_report clientModel = hr_leave_reportFeignClient.get(hr_leave_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_report.getClass(), false);
        copier.copy(clientModel, hr_leave_report, null);
    }


    public void updateBatch(List<Ihr_leave_report> hr_leave_reports){
        if(hr_leave_reports!=null){
            List<hr_leave_reportImpl> list = new ArrayList<hr_leave_reportImpl>();
            for(Ihr_leave_report ihr_leave_report :hr_leave_reports){
                list.add((hr_leave_reportImpl)ihr_leave_report) ;
            }
            hr_leave_reportFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ihr_leave_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_leave_report hr_leave_report){
        Ihr_leave_report clientModel = hr_leave_reportFeignClient.getDraft(hr_leave_report.getId(),(hr_leave_reportImpl)hr_leave_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_leave_report.getClass(), false);
        copier.copy(clientModel, hr_leave_report, null);
    }



}


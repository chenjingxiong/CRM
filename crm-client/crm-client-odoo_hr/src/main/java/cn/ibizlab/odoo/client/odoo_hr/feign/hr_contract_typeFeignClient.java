package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_contract_type;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_contract_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_contract_type] 服务对象接口
 */
public interface hr_contract_typeFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contract_types/removebatch")
    public hr_contract_typeImpl removeBatch(@RequestBody List<hr_contract_typeImpl> hr_contract_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contract_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contract_types/createbatch")
    public hr_contract_typeImpl createBatch(@RequestBody List<hr_contract_typeImpl> hr_contract_types);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contract_types/updatebatch")
    public hr_contract_typeImpl updateBatch(@RequestBody List<hr_contract_typeImpl> hr_contract_types);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contract_types/{id}")
    public hr_contract_typeImpl update(@PathVariable("id") Integer id,@RequestBody hr_contract_typeImpl hr_contract_type);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contract_types")
    public hr_contract_typeImpl create(@RequestBody hr_contract_typeImpl hr_contract_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contract_types/fetchdefault")
    public Page<hr_contract_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contract_types/{id}")
    public hr_contract_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contract_types/select")
    public Page<hr_contract_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contract_types/{id}/getdraft")
    public hr_contract_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_contract_typeImpl hr_contract_type);



}

package cn.ibizlab.odoo.client.odoo_board.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iboard_board;
import cn.ibizlab.odoo.client.odoo_board.config.odoo_boardClientProperties;
import cn.ibizlab.odoo.core.client.service.Iboard_boardClientService;
import cn.ibizlab.odoo.client.odoo_board.model.board_boardImpl;
import cn.ibizlab.odoo.client.odoo_board.feign.board_boardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[board_board] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class board_boardClientServiceImpl implements Iboard_boardClientService {

    board_boardFeignClient board_boardFeignClient;

    @Autowired
    public board_boardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_boardClientProperties odoo_boardClientProperties) {
        if (odoo_boardClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.board_boardFeignClient = nameBuilder.target(board_boardFeignClient.class,"http://"+odoo_boardClientProperties.getServiceId()+"/") ;
		}else if (odoo_boardClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.board_boardFeignClient = nameBuilder.target(board_boardFeignClient.class,odoo_boardClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iboard_board createModel() {
		return new board_boardImpl();
	}


    public void removeBatch(List<Iboard_board> board_boards){
        if(board_boards!=null){
            List<board_boardImpl> list = new ArrayList<board_boardImpl>();
            for(Iboard_board iboard_board :board_boards){
                list.add((board_boardImpl)iboard_board) ;
            }
            board_boardFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iboard_board board_board){
        board_boardFeignClient.remove(board_board.getId()) ;
    }


    public void create(Iboard_board board_board){
        Iboard_board clientModel = board_boardFeignClient.create((board_boardImpl)board_board) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), board_board.getClass(), false);
        copier.copy(clientModel, board_board, null);
    }


    public void update(Iboard_board board_board){
        Iboard_board clientModel = board_boardFeignClient.update(board_board.getId(),(board_boardImpl)board_board) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), board_board.getClass(), false);
        copier.copy(clientModel, board_board, null);
    }


    public Page<Iboard_board> fetchDefault(SearchContext context){
        Page<board_boardImpl> page = this.board_boardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iboard_board> board_boards){
        if(board_boards!=null){
            List<board_boardImpl> list = new ArrayList<board_boardImpl>();
            for(Iboard_board iboard_board :board_boards){
                list.add((board_boardImpl)iboard_board) ;
            }
            board_boardFeignClient.createBatch(list) ;
        }
    }


    public void get(Iboard_board board_board){
        Iboard_board clientModel = board_boardFeignClient.get(board_board.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), board_board.getClass(), false);
        copier.copy(clientModel, board_board, null);
    }


    public void updateBatch(List<Iboard_board> board_boards){
        if(board_boards!=null){
            List<board_boardImpl> list = new ArrayList<board_boardImpl>();
            for(Iboard_board iboard_board :board_boards){
                list.add((board_boardImpl)iboard_board) ;
            }
            board_boardFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iboard_board> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iboard_board board_board){
        Iboard_board clientModel = board_boardFeignClient.getDraft(board_board.getId(),(board_boardImpl)board_board) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), board_board.getClass(), false);
        copier.copy(clientModel, board_board, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mrp.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mrp_routing_workcenter] 对象
 */
public class mrp_routing_workcenterImpl implements Imrp_routing_workcenter,Serializable{

    /**
     * 下一作业
     */
    public String batch;

    @JsonIgnore
    public boolean batchDirtyFlag;
    
    /**
     * 待处理的数量
     */
    public Double batch_size;

    @JsonIgnore
    public boolean batch_sizeDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 操作
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 说明
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 父级工艺路线
     */
    public Integer routing_id;

    @JsonIgnore
    public boolean routing_idDirtyFlag;
    
    /**
     * 父级工艺路线
     */
    public String routing_id_text;

    @JsonIgnore
    public boolean routing_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 时长
     */
    public Double time_cycle;

    @JsonIgnore
    public boolean time_cycleDirtyFlag;
    
    /**
     * 人工时长
     */
    public Double time_cycle_manual;

    @JsonIgnore
    public boolean time_cycle_manualDirtyFlag;
    
    /**
     * 时长计算
     */
    public String time_mode;

    @JsonIgnore
    public boolean time_modeDirtyFlag;
    
    /**
     * 基于
     */
    public Integer time_mode_batch;

    @JsonIgnore
    public boolean time_mode_batchDirtyFlag;
    
    /**
     * 工作中心
     */
    public Integer workcenter_id;

    @JsonIgnore
    public boolean workcenter_idDirtyFlag;
    
    /**
     * 工作中心
     */
    public String workcenter_id_text;

    @JsonIgnore
    public boolean workcenter_id_textDirtyFlag;
    
    /**
     * # 工单
     */
    public Integer workorder_count;

    @JsonIgnore
    public boolean workorder_countDirtyFlag;
    
    /**
     * 工单
     */
    public String workorder_ids;

    @JsonIgnore
    public boolean workorder_idsDirtyFlag;
    
    /**
     * 工作记录表
     */
    public byte[] worksheet;

    @JsonIgnore
    public boolean worksheetDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一作业]
     */
    @JsonProperty("batch")
    public String getBatch(){
        return this.batch ;
    }

    /**
     * 设置 [下一作业]
     */
    @JsonProperty("batch")
    public void setBatch(String  batch){
        this.batch = batch ;
        this.batchDirtyFlag = true ;
    }

     /**
     * 获取 [下一作业]脏标记
     */
    @JsonIgnore
    public boolean getBatchDirtyFlag(){
        return this.batchDirtyFlag ;
    }   

    /**
     * 获取 [待处理的数量]
     */
    @JsonProperty("batch_size")
    public Double getBatch_size(){
        return this.batch_size ;
    }

    /**
     * 设置 [待处理的数量]
     */
    @JsonProperty("batch_size")
    public void setBatch_size(Double  batch_size){
        this.batch_size = batch_size ;
        this.batch_sizeDirtyFlag = true ;
    }

     /**
     * 获取 [待处理的数量]脏标记
     */
    @JsonIgnore
    public boolean getBatch_sizeDirtyFlag(){
        return this.batch_sizeDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [操作]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [操作]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [操作]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [父级工艺路线]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return this.routing_id ;
    }

    /**
     * 设置 [父级工艺路线]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级工艺路线]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return this.routing_idDirtyFlag ;
    }   

    /**
     * 获取 [父级工艺路线]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return this.routing_id_text ;
    }

    /**
     * 设置 [父级工艺路线]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [父级工艺路线]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return this.routing_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [时长]
     */
    @JsonProperty("time_cycle")
    public Double getTime_cycle(){
        return this.time_cycle ;
    }

    /**
     * 设置 [时长]
     */
    @JsonProperty("time_cycle")
    public void setTime_cycle(Double  time_cycle){
        this.time_cycle = time_cycle ;
        this.time_cycleDirtyFlag = true ;
    }

     /**
     * 获取 [时长]脏标记
     */
    @JsonIgnore
    public boolean getTime_cycleDirtyFlag(){
        return this.time_cycleDirtyFlag ;
    }   

    /**
     * 获取 [人工时长]
     */
    @JsonProperty("time_cycle_manual")
    public Double getTime_cycle_manual(){
        return this.time_cycle_manual ;
    }

    /**
     * 设置 [人工时长]
     */
    @JsonProperty("time_cycle_manual")
    public void setTime_cycle_manual(Double  time_cycle_manual){
        this.time_cycle_manual = time_cycle_manual ;
        this.time_cycle_manualDirtyFlag = true ;
    }

     /**
     * 获取 [人工时长]脏标记
     */
    @JsonIgnore
    public boolean getTime_cycle_manualDirtyFlag(){
        return this.time_cycle_manualDirtyFlag ;
    }   

    /**
     * 获取 [时长计算]
     */
    @JsonProperty("time_mode")
    public String getTime_mode(){
        return this.time_mode ;
    }

    /**
     * 设置 [时长计算]
     */
    @JsonProperty("time_mode")
    public void setTime_mode(String  time_mode){
        this.time_mode = time_mode ;
        this.time_modeDirtyFlag = true ;
    }

     /**
     * 获取 [时长计算]脏标记
     */
    @JsonIgnore
    public boolean getTime_modeDirtyFlag(){
        return this.time_modeDirtyFlag ;
    }   

    /**
     * 获取 [基于]
     */
    @JsonProperty("time_mode_batch")
    public Integer getTime_mode_batch(){
        return this.time_mode_batch ;
    }

    /**
     * 设置 [基于]
     */
    @JsonProperty("time_mode_batch")
    public void setTime_mode_batch(Integer  time_mode_batch){
        this.time_mode_batch = time_mode_batch ;
        this.time_mode_batchDirtyFlag = true ;
    }

     /**
     * 获取 [基于]脏标记
     */
    @JsonIgnore
    public boolean getTime_mode_batchDirtyFlag(){
        return this.time_mode_batchDirtyFlag ;
    }   

    /**
     * 获取 [工作中心]
     */
    @JsonProperty("workcenter_id")
    public Integer getWorkcenter_id(){
        return this.workcenter_id ;
    }

    /**
     * 设置 [工作中心]
     */
    @JsonProperty("workcenter_id")
    public void setWorkcenter_id(Integer  workcenter_id){
        this.workcenter_id = workcenter_id ;
        this.workcenter_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_idDirtyFlag(){
        return this.workcenter_idDirtyFlag ;
    }   

    /**
     * 获取 [工作中心]
     */
    @JsonProperty("workcenter_id_text")
    public String getWorkcenter_id_text(){
        return this.workcenter_id_text ;
    }

    /**
     * 设置 [工作中心]
     */
    @JsonProperty("workcenter_id_text")
    public void setWorkcenter_id_text(String  workcenter_id_text){
        this.workcenter_id_text = workcenter_id_text ;
        this.workcenter_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_id_textDirtyFlag(){
        return this.workcenter_id_textDirtyFlag ;
    }   

    /**
     * 获取 [# 工单]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return this.workorder_count ;
    }

    /**
     * 设置 [# 工单]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return this.workorder_countDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_ids")
    public String getWorkorder_ids(){
        return this.workorder_ids ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_ids")
    public void setWorkorder_ids(String  workorder_ids){
        this.workorder_ids = workorder_ids ;
        this.workorder_idsDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idsDirtyFlag(){
        return this.workorder_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作记录表]
     */
    @JsonProperty("worksheet")
    public byte[] getWorksheet(){
        return this.worksheet ;
    }

    /**
     * 设置 [工作记录表]
     */
    @JsonProperty("worksheet")
    public void setWorksheet(byte[]  worksheet){
        this.worksheet = worksheet ;
        this.worksheetDirtyFlag = true ;
    }

     /**
     * 获取 [工作记录表]脏标记
     */
    @JsonIgnore
    public boolean getWorksheetDirtyFlag(){
        return this.worksheetDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

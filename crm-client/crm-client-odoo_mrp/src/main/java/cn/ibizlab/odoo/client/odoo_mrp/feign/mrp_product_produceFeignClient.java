package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_product_produce] 服务对象接口
 */
public interface mrp_product_produceFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produces/createbatch")
    public mrp_product_produceImpl createBatch(@RequestBody List<mrp_product_produceImpl> mrp_product_produces);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produces/fetchdefault")
    public Page<mrp_product_produceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produces/removebatch")
    public mrp_product_produceImpl removeBatch(@RequestBody List<mrp_product_produceImpl> mrp_product_produces);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produces/updatebatch")
    public mrp_product_produceImpl updateBatch(@RequestBody List<mrp_product_produceImpl> mrp_product_produces);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produces/{id}")
    public mrp_product_produceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produces/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produces")
    public mrp_product_produceImpl create(@RequestBody mrp_product_produceImpl mrp_product_produce);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produces/{id}")
    public mrp_product_produceImpl update(@PathVariable("id") Integer id,@RequestBody mrp_product_produceImpl mrp_product_produce);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produces/select")
    public Page<mrp_product_produceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produces/{id}/getdraft")
    public mrp_product_produceImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_product_produceImpl mrp_product_produce);



}

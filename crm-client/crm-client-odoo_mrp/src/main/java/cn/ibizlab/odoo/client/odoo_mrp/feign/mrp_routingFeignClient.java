package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_routing;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_routing] 服务对象接口
 */
public interface mrp_routingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routings/{id}")
    public mrp_routingImpl update(@PathVariable("id") Integer id,@RequestBody mrp_routingImpl mrp_routing);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routings/{id}")
    public mrp_routingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routings")
    public mrp_routingImpl create(@RequestBody mrp_routingImpl mrp_routing);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routings/fetchdefault")
    public Page<mrp_routingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routings/createbatch")
    public mrp_routingImpl createBatch(@RequestBody List<mrp_routingImpl> mrp_routings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routings/removebatch")
    public mrp_routingImpl removeBatch(@RequestBody List<mrp_routingImpl> mrp_routings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routings/updatebatch")
    public mrp_routingImpl updateBatch(@RequestBody List<mrp_routingImpl> mrp_routings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routings/select")
    public Page<mrp_routingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routings/{id}/getdraft")
    public mrp_routingImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_routingImpl mrp_routing);



}

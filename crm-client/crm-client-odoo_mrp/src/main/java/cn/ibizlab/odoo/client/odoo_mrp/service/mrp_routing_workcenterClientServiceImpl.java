package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_routing_workcenterClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routing_workcenterImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_routing_workcenterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_routing_workcenterClientServiceImpl implements Imrp_routing_workcenterClientService {

    mrp_routing_workcenterFeignClient mrp_routing_workcenterFeignClient;

    @Autowired
    public mrp_routing_workcenterClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_routing_workcenterFeignClient = nameBuilder.target(mrp_routing_workcenterFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_routing_workcenterFeignClient = nameBuilder.target(mrp_routing_workcenterFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_routing_workcenter createModel() {
		return new mrp_routing_workcenterImpl();
	}


    public void createBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
        if(mrp_routing_workcenters!=null){
            List<mrp_routing_workcenterImpl> list = new ArrayList<mrp_routing_workcenterImpl>();
            for(Imrp_routing_workcenter imrp_routing_workcenter :mrp_routing_workcenters){
                list.add((mrp_routing_workcenterImpl)imrp_routing_workcenter) ;
            }
            mrp_routing_workcenterFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imrp_routing_workcenter mrp_routing_workcenter){
        mrp_routing_workcenterFeignClient.remove(mrp_routing_workcenter.getId()) ;
    }


    public void get(Imrp_routing_workcenter mrp_routing_workcenter){
        Imrp_routing_workcenter clientModel = mrp_routing_workcenterFeignClient.get(mrp_routing_workcenter.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_routing_workcenter, null);
    }


    public void updateBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
        if(mrp_routing_workcenters!=null){
            List<mrp_routing_workcenterImpl> list = new ArrayList<mrp_routing_workcenterImpl>();
            for(Imrp_routing_workcenter imrp_routing_workcenter :mrp_routing_workcenters){
                list.add((mrp_routing_workcenterImpl)imrp_routing_workcenter) ;
            }
            mrp_routing_workcenterFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_routing_workcenter> mrp_routing_workcenters){
        if(mrp_routing_workcenters!=null){
            List<mrp_routing_workcenterImpl> list = new ArrayList<mrp_routing_workcenterImpl>();
            for(Imrp_routing_workcenter imrp_routing_workcenter :mrp_routing_workcenters){
                list.add((mrp_routing_workcenterImpl)imrp_routing_workcenter) ;
            }
            mrp_routing_workcenterFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imrp_routing_workcenter> fetchDefault(SearchContext context){
        Page<mrp_routing_workcenterImpl> page = this.mrp_routing_workcenterFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imrp_routing_workcenter mrp_routing_workcenter){
        Imrp_routing_workcenter clientModel = mrp_routing_workcenterFeignClient.update(mrp_routing_workcenter.getId(),(mrp_routing_workcenterImpl)mrp_routing_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_routing_workcenter, null);
    }


    public void create(Imrp_routing_workcenter mrp_routing_workcenter){
        Imrp_routing_workcenter clientModel = mrp_routing_workcenterFeignClient.create((mrp_routing_workcenterImpl)mrp_routing_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_routing_workcenter, null);
    }


    public Page<Imrp_routing_workcenter> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_routing_workcenter mrp_routing_workcenter){
        Imrp_routing_workcenter clientModel = mrp_routing_workcenterFeignClient.getDraft(mrp_routing_workcenter.getId(),(mrp_routing_workcenterImpl)mrp_routing_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_routing_workcenter, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_routing_workcenter;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routing_workcenterImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
public interface mrp_routing_workcenterFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routing_workcenters/createbatch")
    public mrp_routing_workcenterImpl createBatch(@RequestBody List<mrp_routing_workcenterImpl> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routing_workcenters/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routing_workcenters/{id}")
    public mrp_routing_workcenterImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routing_workcenters/updatebatch")
    public mrp_routing_workcenterImpl updateBatch(@RequestBody List<mrp_routing_workcenterImpl> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routing_workcenters/removebatch")
    public mrp_routing_workcenterImpl removeBatch(@RequestBody List<mrp_routing_workcenterImpl> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routing_workcenters/fetchdefault")
    public Page<mrp_routing_workcenterImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routing_workcenters/{id}")
    public mrp_routing_workcenterImpl update(@PathVariable("id") Integer id,@RequestBody mrp_routing_workcenterImpl mrp_routing_workcenter);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routing_workcenters")
    public mrp_routing_workcenterImpl create(@RequestBody mrp_routing_workcenterImpl mrp_routing_workcenter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routing_workcenters/select")
    public Page<mrp_routing_workcenterImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routing_workcenters/{id}/getdraft")
    public mrp_routing_workcenterImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_routing_workcenterImpl mrp_routing_workcenter);



}

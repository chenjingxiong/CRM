package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenterClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenterImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_workcenterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_workcenterClientServiceImpl implements Imrp_workcenterClientService {

    mrp_workcenterFeignClient mrp_workcenterFeignClient;

    @Autowired
    public mrp_workcenterClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenterFeignClient = nameBuilder.target(mrp_workcenterFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenterFeignClient = nameBuilder.target(mrp_workcenterFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_workcenter createModel() {
		return new mrp_workcenterImpl();
	}


    public void updateBatch(List<Imrp_workcenter> mrp_workcenters){
        if(mrp_workcenters!=null){
            List<mrp_workcenterImpl> list = new ArrayList<mrp_workcenterImpl>();
            for(Imrp_workcenter imrp_workcenter :mrp_workcenters){
                list.add((mrp_workcenterImpl)imrp_workcenter) ;
            }
            mrp_workcenterFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imrp_workcenter> fetchDefault(SearchContext context){
        Page<mrp_workcenterImpl> page = this.mrp_workcenterFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imrp_workcenter> mrp_workcenters){
        if(mrp_workcenters!=null){
            List<mrp_workcenterImpl> list = new ArrayList<mrp_workcenterImpl>();
            for(Imrp_workcenter imrp_workcenter :mrp_workcenters){
                list.add((mrp_workcenterImpl)imrp_workcenter) ;
            }
            mrp_workcenterFeignClient.createBatch(list) ;
        }
    }


    public void create(Imrp_workcenter mrp_workcenter){
        Imrp_workcenter clientModel = mrp_workcenterFeignClient.create((mrp_workcenterImpl)mrp_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_workcenter, null);
    }


    public void removeBatch(List<Imrp_workcenter> mrp_workcenters){
        if(mrp_workcenters!=null){
            List<mrp_workcenterImpl> list = new ArrayList<mrp_workcenterImpl>();
            for(Imrp_workcenter imrp_workcenter :mrp_workcenters){
                list.add((mrp_workcenterImpl)imrp_workcenter) ;
            }
            mrp_workcenterFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imrp_workcenter mrp_workcenter){
        Imrp_workcenter clientModel = mrp_workcenterFeignClient.get(mrp_workcenter.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_workcenter, null);
    }


    public void remove(Imrp_workcenter mrp_workcenter){
        mrp_workcenterFeignClient.remove(mrp_workcenter.getId()) ;
    }


    public void update(Imrp_workcenter mrp_workcenter){
        Imrp_workcenter clientModel = mrp_workcenterFeignClient.update(mrp_workcenter.getId(),(mrp_workcenterImpl)mrp_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_workcenter, null);
    }


    public Page<Imrp_workcenter> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_workcenter mrp_workcenter){
        Imrp_workcenter clientModel = mrp_workcenterFeignClient.getDraft(mrp_workcenter.getId(),(mrp_workcenterImpl)mrp_workcenter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter.getClass(), false);
        copier.copy(clientModel, mrp_workcenter, null);
    }



}


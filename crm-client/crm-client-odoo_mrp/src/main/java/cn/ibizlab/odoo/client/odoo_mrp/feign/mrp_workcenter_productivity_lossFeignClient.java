package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_lossImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
public interface mrp_workcenter_productivity_lossFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{id}")
    public mrp_workcenter_productivity_lossImpl update(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivity_lossImpl mrp_workcenter_productivity_loss);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_losses")
    public mrp_workcenter_productivity_lossImpl create(@RequestBody mrp_workcenter_productivity_lossImpl mrp_workcenter_productivity_loss);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_losses/removebatch")
    public mrp_workcenter_productivity_lossImpl removeBatch(@RequestBody List<mrp_workcenter_productivity_lossImpl> mrp_workcenter_productivity_losses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_losses/fetchdefault")
    public Page<mrp_workcenter_productivity_lossImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_losses/createbatch")
    public mrp_workcenter_productivity_lossImpl createBatch(@RequestBody List<mrp_workcenter_productivity_lossImpl> mrp_workcenter_productivity_losses);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_losses/updatebatch")
    public mrp_workcenter_productivity_lossImpl updateBatch(@RequestBody List<mrp_workcenter_productivity_lossImpl> mrp_workcenter_productivity_losses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{id}")
    public mrp_workcenter_productivity_lossImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_losses/select")
    public Page<mrp_workcenter_productivity_lossImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{id}/getdraft")
    public mrp_workcenter_productivity_lossImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivity_lossImpl mrp_workcenter_productivity_loss);



}

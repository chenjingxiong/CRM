package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_document;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_documentImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_document] 服务对象接口
 */
public interface mrp_documentFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_documents/fetchdefault")
    public Page<mrp_documentImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_documents/removebatch")
    public mrp_documentImpl removeBatch(@RequestBody List<mrp_documentImpl> mrp_documents);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_documents")
    public mrp_documentImpl create(@RequestBody mrp_documentImpl mrp_document);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_documents/updatebatch")
    public mrp_documentImpl updateBatch(@RequestBody List<mrp_documentImpl> mrp_documents);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_documents/createbatch")
    public mrp_documentImpl createBatch(@RequestBody List<mrp_documentImpl> mrp_documents);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_documents/{id}")
    public mrp_documentImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_documents/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_documents/{id}")
    public mrp_documentImpl update(@PathVariable("id") Integer id,@RequestBody mrp_documentImpl mrp_document);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_documents/select")
    public Page<mrp_documentImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_documents/{id}/getdraft")
    public mrp_documentImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_documentImpl mrp_document);



}

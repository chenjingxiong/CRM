package cn.ibizlab.odoo.client.odoo_mrp.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imrp_workorder;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mrp_workorder] 对象
 */
public class mrp_workorderImpl implements Imrp_workorder,Serializable{

    /**
     * 操作凭证行
     */
    public String active_move_line_ids;

    @JsonIgnore
    public boolean active_move_line_idsDirtyFlag;
    
    /**
     * 容量
     */
    public Double capacity;

    @JsonIgnore
    public boolean capacityDirtyFlag;
    
    /**
     * 颜色
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 实际结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_finished;

    @JsonIgnore
    public boolean date_finishedDirtyFlag;
    
    /**
     * 安排的完工日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned_finished;

    @JsonIgnore
    public boolean date_planned_finishedDirtyFlag;
    
    /**
     * 安排的开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned_start;

    @JsonIgnore
    public boolean date_planned_startDirtyFlag;
    
    /**
     * 实际开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 实际时长
     */
    public Double duration;

    @JsonIgnore
    public boolean durationDirtyFlag;
    
    /**
     * 预计时长
     */
    public Double duration_expected;

    @JsonIgnore
    public boolean duration_expectedDirtyFlag;
    
    /**
     * 时长偏差(%)
     */
    public Integer duration_percent;

    @JsonIgnore
    public boolean duration_percentDirtyFlag;
    
    /**
     * 每单位时长
     */
    public Double duration_unit;

    @JsonIgnore
    public boolean duration_unitDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public Integer final_lot_id;

    @JsonIgnore
    public boolean final_lot_idDirtyFlag;
    
    /**
     * 批次/序列号码
     */
    public String final_lot_id_text;

    @JsonIgnore
    public boolean final_lot_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Is the first WO to produce
     */
    public String is_first_wo;

    @JsonIgnore
    public boolean is_first_woDirtyFlag;
    
    /**
     * 已生产
     */
    public String is_produced;

    @JsonIgnore
    public boolean is_producedDirtyFlag;
    
    /**
     * 当前用户正在工作吗？
     */
    public String is_user_working;

    @JsonIgnore
    public boolean is_user_workingDirtyFlag;
    
    /**
     * 上一个在此工单工作的用户
     */
    public String last_working_user_id;

    @JsonIgnore
    public boolean last_working_user_idDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 待追踪的产品
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 移动
     */
    public String move_raw_ids;

    @JsonIgnore
    public boolean move_raw_idsDirtyFlag;
    
    /**
     * 工单
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 下一工单
     */
    public Integer next_work_order_id;

    @JsonIgnore
    public boolean next_work_order_idDirtyFlag;
    
    /**
     * 下一工单
     */
    public String next_work_order_id_text;

    @JsonIgnore
    public boolean next_work_order_id_textDirtyFlag;
    
    /**
     * 操作
     */
    public Integer operation_id;

    @JsonIgnore
    public boolean operation_idDirtyFlag;
    
    /**
     * 操作
     */
    public String operation_id_text;

    @JsonIgnore
    public boolean operation_id_textDirtyFlag;
    
    /**
     * 材料可用性
     */
    public String production_availability;

    @JsonIgnore
    public boolean production_availabilityDirtyFlag;
    
    /**
     * 生产日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp production_date;

    @JsonIgnore
    public boolean production_dateDirtyFlag;
    
    /**
     * 制造订单
     */
    public Integer production_id;

    @JsonIgnore
    public boolean production_idDirtyFlag;
    
    /**
     * 制造订单
     */
    public String production_id_text;

    @JsonIgnore
    public boolean production_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String production_state;

    @JsonIgnore
    public boolean production_stateDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 追踪
     */
    public String product_tracking;

    @JsonIgnore
    public boolean product_trackingDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 数量
     */
    public Double qty_produced;

    @JsonIgnore
    public boolean qty_producedDirtyFlag;
    
    /**
     * 当前的已生产数量
     */
    public Double qty_producing;

    @JsonIgnore
    public boolean qty_producingDirtyFlag;
    
    /**
     * 原始生产数量
     */
    public Double qty_production;

    @JsonIgnore
    public boolean qty_productionDirtyFlag;
    
    /**
     * 将被生产的数量
     */
    public Double qty_remaining;

    @JsonIgnore
    public boolean qty_remainingDirtyFlag;
    
    /**
     * 报废转移
     */
    public Integer scrap_count;

    @JsonIgnore
    public boolean scrap_countDirtyFlag;
    
    /**
     * 报废
     */
    public String scrap_ids;

    @JsonIgnore
    public boolean scrap_idsDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 时间
     */
    public String time_ids;

    @JsonIgnore
    public boolean time_idsDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 工作中心
     */
    public Integer workcenter_id;

    @JsonIgnore
    public boolean workcenter_idDirtyFlag;
    
    /**
     * 工作中心
     */
    public String workcenter_id_text;

    @JsonIgnore
    public boolean workcenter_id_textDirtyFlag;
    
    /**
     * 工作中心状态
     */
    public String working_state;

    @JsonIgnore
    public boolean working_stateDirtyFlag;
    
    /**
     * 在此工单工作的用户
     */
    public String working_user_ids;

    @JsonIgnore
    public boolean working_user_idsDirtyFlag;
    
    /**
     * 工作记录表
     */
    public byte[] worksheet;

    @JsonIgnore
    public boolean worksheetDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [操作凭证行]
     */
    @JsonProperty("active_move_line_ids")
    public String getActive_move_line_ids(){
        return this.active_move_line_ids ;
    }

    /**
     * 设置 [操作凭证行]
     */
    @JsonProperty("active_move_line_ids")
    public void setActive_move_line_ids(String  active_move_line_ids){
        this.active_move_line_ids = active_move_line_ids ;
        this.active_move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [操作凭证行]脏标记
     */
    @JsonIgnore
    public boolean getActive_move_line_idsDirtyFlag(){
        return this.active_move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [容量]
     */
    @JsonProperty("capacity")
    public Double getCapacity(){
        return this.capacity ;
    }

    /**
     * 设置 [容量]
     */
    @JsonProperty("capacity")
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.capacityDirtyFlag = true ;
    }

     /**
     * 获取 [容量]脏标记
     */
    @JsonIgnore
    public boolean getCapacityDirtyFlag(){
        return this.capacityDirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [实际结束日期]
     */
    @JsonProperty("date_finished")
    public Timestamp getDate_finished(){
        return this.date_finished ;
    }

    /**
     * 设置 [实际结束日期]
     */
    @JsonProperty("date_finished")
    public void setDate_finished(Timestamp  date_finished){
        this.date_finished = date_finished ;
        this.date_finishedDirtyFlag = true ;
    }

     /**
     * 获取 [实际结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_finishedDirtyFlag(){
        return this.date_finishedDirtyFlag ;
    }   

    /**
     * 获取 [安排的完工日期]
     */
    @JsonProperty("date_planned_finished")
    public Timestamp getDate_planned_finished(){
        return this.date_planned_finished ;
    }

    /**
     * 设置 [安排的完工日期]
     */
    @JsonProperty("date_planned_finished")
    public void setDate_planned_finished(Timestamp  date_planned_finished){
        this.date_planned_finished = date_planned_finished ;
        this.date_planned_finishedDirtyFlag = true ;
    }

     /**
     * 获取 [安排的完工日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_finishedDirtyFlag(){
        return this.date_planned_finishedDirtyFlag ;
    }   

    /**
     * 获取 [安排的开始日期]
     */
    @JsonProperty("date_planned_start")
    public Timestamp getDate_planned_start(){
        return this.date_planned_start ;
    }

    /**
     * 设置 [安排的开始日期]
     */
    @JsonProperty("date_planned_start")
    public void setDate_planned_start(Timestamp  date_planned_start){
        this.date_planned_start = date_planned_start ;
        this.date_planned_startDirtyFlag = true ;
    }

     /**
     * 获取 [安排的开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_startDirtyFlag(){
        return this.date_planned_startDirtyFlag ;
    }   

    /**
     * 获取 [实际开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [实际开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [实际开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [实际时长]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [实际时长]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

     /**
     * 获取 [实际时长]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }   

    /**
     * 获取 [预计时长]
     */
    @JsonProperty("duration_expected")
    public Double getDuration_expected(){
        return this.duration_expected ;
    }

    /**
     * 设置 [预计时长]
     */
    @JsonProperty("duration_expected")
    public void setDuration_expected(Double  duration_expected){
        this.duration_expected = duration_expected ;
        this.duration_expectedDirtyFlag = true ;
    }

     /**
     * 获取 [预计时长]脏标记
     */
    @JsonIgnore
    public boolean getDuration_expectedDirtyFlag(){
        return this.duration_expectedDirtyFlag ;
    }   

    /**
     * 获取 [时长偏差(%)]
     */
    @JsonProperty("duration_percent")
    public Integer getDuration_percent(){
        return this.duration_percent ;
    }

    /**
     * 设置 [时长偏差(%)]
     */
    @JsonProperty("duration_percent")
    public void setDuration_percent(Integer  duration_percent){
        this.duration_percent = duration_percent ;
        this.duration_percentDirtyFlag = true ;
    }

     /**
     * 获取 [时长偏差(%)]脏标记
     */
    @JsonIgnore
    public boolean getDuration_percentDirtyFlag(){
        return this.duration_percentDirtyFlag ;
    }   

    /**
     * 获取 [每单位时长]
     */
    @JsonProperty("duration_unit")
    public Double getDuration_unit(){
        return this.duration_unit ;
    }

    /**
     * 设置 [每单位时长]
     */
    @JsonProperty("duration_unit")
    public void setDuration_unit(Double  duration_unit){
        this.duration_unit = duration_unit ;
        this.duration_unitDirtyFlag = true ;
    }

     /**
     * 获取 [每单位时长]脏标记
     */
    @JsonIgnore
    public boolean getDuration_unitDirtyFlag(){
        return this.duration_unitDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("final_lot_id")
    public Integer getFinal_lot_id(){
        return this.final_lot_id ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("final_lot_id")
    public void setFinal_lot_id(Integer  final_lot_id){
        this.final_lot_id = final_lot_id ;
        this.final_lot_idDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getFinal_lot_idDirtyFlag(){
        return this.final_lot_idDirtyFlag ;
    }   

    /**
     * 获取 [批次/序列号码]
     */
    @JsonProperty("final_lot_id_text")
    public String getFinal_lot_id_text(){
        return this.final_lot_id_text ;
    }

    /**
     * 设置 [批次/序列号码]
     */
    @JsonProperty("final_lot_id_text")
    public void setFinal_lot_id_text(String  final_lot_id_text){
        this.final_lot_id_text = final_lot_id_text ;
        this.final_lot_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [批次/序列号码]脏标记
     */
    @JsonIgnore
    public boolean getFinal_lot_id_textDirtyFlag(){
        return this.final_lot_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Is the first WO to produce]
     */
    @JsonProperty("is_first_wo")
    public String getIs_first_wo(){
        return this.is_first_wo ;
    }

    /**
     * 设置 [Is the first WO to produce]
     */
    @JsonProperty("is_first_wo")
    public void setIs_first_wo(String  is_first_wo){
        this.is_first_wo = is_first_wo ;
        this.is_first_woDirtyFlag = true ;
    }

     /**
     * 获取 [Is the first WO to produce]脏标记
     */
    @JsonIgnore
    public boolean getIs_first_woDirtyFlag(){
        return this.is_first_woDirtyFlag ;
    }   

    /**
     * 获取 [已生产]
     */
    @JsonProperty("is_produced")
    public String getIs_produced(){
        return this.is_produced ;
    }

    /**
     * 设置 [已生产]
     */
    @JsonProperty("is_produced")
    public void setIs_produced(String  is_produced){
        this.is_produced = is_produced ;
        this.is_producedDirtyFlag = true ;
    }

     /**
     * 获取 [已生产]脏标记
     */
    @JsonIgnore
    public boolean getIs_producedDirtyFlag(){
        return this.is_producedDirtyFlag ;
    }   

    /**
     * 获取 [当前用户正在工作吗？]
     */
    @JsonProperty("is_user_working")
    public String getIs_user_working(){
        return this.is_user_working ;
    }

    /**
     * 设置 [当前用户正在工作吗？]
     */
    @JsonProperty("is_user_working")
    public void setIs_user_working(String  is_user_working){
        this.is_user_working = is_user_working ;
        this.is_user_workingDirtyFlag = true ;
    }

     /**
     * 获取 [当前用户正在工作吗？]脏标记
     */
    @JsonIgnore
    public boolean getIs_user_workingDirtyFlag(){
        return this.is_user_workingDirtyFlag ;
    }   

    /**
     * 获取 [上一个在此工单工作的用户]
     */
    @JsonProperty("last_working_user_id")
    public String getLast_working_user_id(){
        return this.last_working_user_id ;
    }

    /**
     * 设置 [上一个在此工单工作的用户]
     */
    @JsonProperty("last_working_user_id")
    public void setLast_working_user_id(String  last_working_user_id){
        this.last_working_user_id = last_working_user_id ;
        this.last_working_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [上一个在此工单工作的用户]脏标记
     */
    @JsonIgnore
    public boolean getLast_working_user_idDirtyFlag(){
        return this.last_working_user_idDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [待追踪的产品]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [待追踪的产品]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [待追踪的产品]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [移动]
     */
    @JsonProperty("move_raw_ids")
    public String getMove_raw_ids(){
        return this.move_raw_ids ;
    }

    /**
     * 设置 [移动]
     */
    @JsonProperty("move_raw_ids")
    public void setMove_raw_ids(String  move_raw_ids){
        this.move_raw_ids = move_raw_ids ;
        this.move_raw_idsDirtyFlag = true ;
    }

     /**
     * 获取 [移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_raw_idsDirtyFlag(){
        return this.move_raw_idsDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [下一工单]
     */
    @JsonProperty("next_work_order_id")
    public Integer getNext_work_order_id(){
        return this.next_work_order_id ;
    }

    /**
     * 设置 [下一工单]
     */
    @JsonProperty("next_work_order_id")
    public void setNext_work_order_id(Integer  next_work_order_id){
        this.next_work_order_id = next_work_order_id ;
        this.next_work_order_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一工单]脏标记
     */
    @JsonIgnore
    public boolean getNext_work_order_idDirtyFlag(){
        return this.next_work_order_idDirtyFlag ;
    }   

    /**
     * 获取 [下一工单]
     */
    @JsonProperty("next_work_order_id_text")
    public String getNext_work_order_id_text(){
        return this.next_work_order_id_text ;
    }

    /**
     * 设置 [下一工单]
     */
    @JsonProperty("next_work_order_id_text")
    public void setNext_work_order_id_text(String  next_work_order_id_text){
        this.next_work_order_id_text = next_work_order_id_text ;
        this.next_work_order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [下一工单]脏标记
     */
    @JsonIgnore
    public boolean getNext_work_order_id_textDirtyFlag(){
        return this.next_work_order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [操作]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return this.operation_id ;
    }

    /**
     * 设置 [操作]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

     /**
     * 获取 [操作]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return this.operation_idDirtyFlag ;
    }   

    /**
     * 获取 [操作]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return this.operation_id_text ;
    }

    /**
     * 设置 [操作]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [操作]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return this.operation_id_textDirtyFlag ;
    }   

    /**
     * 获取 [材料可用性]
     */
    @JsonProperty("production_availability")
    public String getProduction_availability(){
        return this.production_availability ;
    }

    /**
     * 设置 [材料可用性]
     */
    @JsonProperty("production_availability")
    public void setProduction_availability(String  production_availability){
        this.production_availability = production_availability ;
        this.production_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [材料可用性]脏标记
     */
    @JsonIgnore
    public boolean getProduction_availabilityDirtyFlag(){
        return this.production_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [生产日期]
     */
    @JsonProperty("production_date")
    public Timestamp getProduction_date(){
        return this.production_date ;
    }

    /**
     * 设置 [生产日期]
     */
    @JsonProperty("production_date")
    public void setProduction_date(Timestamp  production_date){
        this.production_date = production_date ;
        this.production_dateDirtyFlag = true ;
    }

     /**
     * 获取 [生产日期]脏标记
     */
    @JsonIgnore
    public boolean getProduction_dateDirtyFlag(){
        return this.production_dateDirtyFlag ;
    }   

    /**
     * 获取 [制造订单]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return this.production_id ;
    }

    /**
     * 设置 [制造订单]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

     /**
     * 获取 [制造订单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return this.production_idDirtyFlag ;
    }   

    /**
     * 获取 [制造订单]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return this.production_id_text ;
    }

    /**
     * 设置 [制造订单]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [制造订单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return this.production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("production_state")
    public String getProduction_state(){
        return this.production_state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("production_state")
    public void setProduction_state(String  production_state){
        this.production_state = production_state ;
        this.production_stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getProduction_stateDirtyFlag(){
        return this.production_stateDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("product_tracking")
    public String getProduct_tracking(){
        return this.product_tracking ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("product_tracking")
    public void setProduct_tracking(String  product_tracking){
        this.product_tracking = product_tracking ;
        this.product_trackingDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getProduct_trackingDirtyFlag(){
        return this.product_trackingDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("qty_produced")
    public Double getQty_produced(){
        return this.qty_produced ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("qty_produced")
    public void setQty_produced(Double  qty_produced){
        this.qty_produced = qty_produced ;
        this.qty_producedDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_producedDirtyFlag(){
        return this.qty_producedDirtyFlag ;
    }   

    /**
     * 获取 [当前的已生产数量]
     */
    @JsonProperty("qty_producing")
    public Double getQty_producing(){
        return this.qty_producing ;
    }

    /**
     * 设置 [当前的已生产数量]
     */
    @JsonProperty("qty_producing")
    public void setQty_producing(Double  qty_producing){
        this.qty_producing = qty_producing ;
        this.qty_producingDirtyFlag = true ;
    }

     /**
     * 获取 [当前的已生产数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_producingDirtyFlag(){
        return this.qty_producingDirtyFlag ;
    }   

    /**
     * 获取 [原始生产数量]
     */
    @JsonProperty("qty_production")
    public Double getQty_production(){
        return this.qty_production ;
    }

    /**
     * 设置 [原始生产数量]
     */
    @JsonProperty("qty_production")
    public void setQty_production(Double  qty_production){
        this.qty_production = qty_production ;
        this.qty_productionDirtyFlag = true ;
    }

     /**
     * 获取 [原始生产数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_productionDirtyFlag(){
        return this.qty_productionDirtyFlag ;
    }   

    /**
     * 获取 [将被生产的数量]
     */
    @JsonProperty("qty_remaining")
    public Double getQty_remaining(){
        return this.qty_remaining ;
    }

    /**
     * 设置 [将被生产的数量]
     */
    @JsonProperty("qty_remaining")
    public void setQty_remaining(Double  qty_remaining){
        this.qty_remaining = qty_remaining ;
        this.qty_remainingDirtyFlag = true ;
    }

     /**
     * 获取 [将被生产的数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_remainingDirtyFlag(){
        return this.qty_remainingDirtyFlag ;
    }   

    /**
     * 获取 [报废转移]
     */
    @JsonProperty("scrap_count")
    public Integer getScrap_count(){
        return this.scrap_count ;
    }

    /**
     * 设置 [报废转移]
     */
    @JsonProperty("scrap_count")
    public void setScrap_count(Integer  scrap_count){
        this.scrap_count = scrap_count ;
        this.scrap_countDirtyFlag = true ;
    }

     /**
     * 获取 [报废转移]脏标记
     */
    @JsonIgnore
    public boolean getScrap_countDirtyFlag(){
        return this.scrap_countDirtyFlag ;
    }   

    /**
     * 获取 [报废]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return this.scrap_ids ;
    }

    /**
     * 设置 [报废]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

     /**
     * 获取 [报废]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return this.scrap_idsDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [时间]
     */
    @JsonProperty("time_ids")
    public String getTime_ids(){
        return this.time_ids ;
    }

    /**
     * 设置 [时间]
     */
    @JsonProperty("time_ids")
    public void setTime_ids(String  time_ids){
        this.time_ids = time_ids ;
        this.time_idsDirtyFlag = true ;
    }

     /**
     * 获取 [时间]脏标记
     */
    @JsonIgnore
    public boolean getTime_idsDirtyFlag(){
        return this.time_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作中心]
     */
    @JsonProperty("workcenter_id")
    public Integer getWorkcenter_id(){
        return this.workcenter_id ;
    }

    /**
     * 设置 [工作中心]
     */
    @JsonProperty("workcenter_id")
    public void setWorkcenter_id(Integer  workcenter_id){
        this.workcenter_id = workcenter_id ;
        this.workcenter_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_idDirtyFlag(){
        return this.workcenter_idDirtyFlag ;
    }   

    /**
     * 获取 [工作中心]
     */
    @JsonProperty("workcenter_id_text")
    public String getWorkcenter_id_text(){
        return this.workcenter_id_text ;
    }

    /**
     * 设置 [工作中心]
     */
    @JsonProperty("workcenter_id_text")
    public void setWorkcenter_id_text(String  workcenter_id_text){
        this.workcenter_id_text = workcenter_id_text ;
        this.workcenter_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_id_textDirtyFlag(){
        return this.workcenter_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作中心状态]
     */
    @JsonProperty("working_state")
    public String getWorking_state(){
        return this.working_state ;
    }

    /**
     * 设置 [工作中心状态]
     */
    @JsonProperty("working_state")
    public void setWorking_state(String  working_state){
        this.working_state = working_state ;
        this.working_stateDirtyFlag = true ;
    }

     /**
     * 获取 [工作中心状态]脏标记
     */
    @JsonIgnore
    public boolean getWorking_stateDirtyFlag(){
        return this.working_stateDirtyFlag ;
    }   

    /**
     * 获取 [在此工单工作的用户]
     */
    @JsonProperty("working_user_ids")
    public String getWorking_user_ids(){
        return this.working_user_ids ;
    }

    /**
     * 设置 [在此工单工作的用户]
     */
    @JsonProperty("working_user_ids")
    public void setWorking_user_ids(String  working_user_ids){
        this.working_user_ids = working_user_ids ;
        this.working_user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [在此工单工作的用户]脏标记
     */
    @JsonIgnore
    public boolean getWorking_user_idsDirtyFlag(){
        return this.working_user_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作记录表]
     */
    @JsonProperty("worksheet")
    public byte[] getWorksheet(){
        return this.worksheet ;
    }

    /**
     * 设置 [工作记录表]
     */
    @JsonProperty("worksheet")
    public void setWorksheet(byte[]  worksheet){
        this.worksheet = worksheet ;
        this.worksheetDirtyFlag = true ;
    }

     /**
     * 获取 [工作记录表]脏标记
     */
    @JsonIgnore
    public boolean getWorksheetDirtyFlag(){
        return this.worksheetDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

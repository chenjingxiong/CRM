package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_product_produceClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produceImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_product_produceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_product_produce] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_product_produceClientServiceImpl implements Imrp_product_produceClientService {

    mrp_product_produceFeignClient mrp_product_produceFeignClient;

    @Autowired
    public mrp_product_produceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_product_produceFeignClient = nameBuilder.target(mrp_product_produceFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_product_produceFeignClient = nameBuilder.target(mrp_product_produceFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_product_produce createModel() {
		return new mrp_product_produceImpl();
	}


    public void createBatch(List<Imrp_product_produce> mrp_product_produces){
        if(mrp_product_produces!=null){
            List<mrp_product_produceImpl> list = new ArrayList<mrp_product_produceImpl>();
            for(Imrp_product_produce imrp_product_produce :mrp_product_produces){
                list.add((mrp_product_produceImpl)imrp_product_produce) ;
            }
            mrp_product_produceFeignClient.createBatch(list) ;
        }
    }


    public Page<Imrp_product_produce> fetchDefault(SearchContext context){
        Page<mrp_product_produceImpl> page = this.mrp_product_produceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imrp_product_produce> mrp_product_produces){
        if(mrp_product_produces!=null){
            List<mrp_product_produceImpl> list = new ArrayList<mrp_product_produceImpl>();
            for(Imrp_product_produce imrp_product_produce :mrp_product_produces){
                list.add((mrp_product_produceImpl)imrp_product_produce) ;
            }
            mrp_product_produceFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imrp_product_produce> mrp_product_produces){
        if(mrp_product_produces!=null){
            List<mrp_product_produceImpl> list = new ArrayList<mrp_product_produceImpl>();
            for(Imrp_product_produce imrp_product_produce :mrp_product_produces){
                list.add((mrp_product_produceImpl)imrp_product_produce) ;
            }
            mrp_product_produceFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imrp_product_produce mrp_product_produce){
        Imrp_product_produce clientModel = mrp_product_produceFeignClient.get(mrp_product_produce.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce.getClass(), false);
        copier.copy(clientModel, mrp_product_produce, null);
    }


    public void remove(Imrp_product_produce mrp_product_produce){
        mrp_product_produceFeignClient.remove(mrp_product_produce.getId()) ;
    }


    public void create(Imrp_product_produce mrp_product_produce){
        Imrp_product_produce clientModel = mrp_product_produceFeignClient.create((mrp_product_produceImpl)mrp_product_produce) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce.getClass(), false);
        copier.copy(clientModel, mrp_product_produce, null);
    }


    public void update(Imrp_product_produce mrp_product_produce){
        Imrp_product_produce clientModel = mrp_product_produceFeignClient.update(mrp_product_produce.getId(),(mrp_product_produceImpl)mrp_product_produce) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce.getClass(), false);
        copier.copy(clientModel, mrp_product_produce, null);
    }


    public Page<Imrp_product_produce> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_product_produce mrp_product_produce){
        Imrp_product_produce clientModel = mrp_product_produceFeignClient.getDraft(mrp_product_produce.getId(),(mrp_product_produceImpl)mrp_product_produce) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce.getClass(), false);
        copier.copy(clientModel, mrp_product_produce, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_routing;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_routingClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routingImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_routingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_routing] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_routingClientServiceImpl implements Imrp_routingClientService {

    mrp_routingFeignClient mrp_routingFeignClient;

    @Autowired
    public mrp_routingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_routingFeignClient = nameBuilder.target(mrp_routingFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_routingFeignClient = nameBuilder.target(mrp_routingFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_routing createModel() {
		return new mrp_routingImpl();
	}


    public void update(Imrp_routing mrp_routing){
        Imrp_routing clientModel = mrp_routingFeignClient.update(mrp_routing.getId(),(mrp_routingImpl)mrp_routing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing.getClass(), false);
        copier.copy(clientModel, mrp_routing, null);
    }


    public void get(Imrp_routing mrp_routing){
        Imrp_routing clientModel = mrp_routingFeignClient.get(mrp_routing.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing.getClass(), false);
        copier.copy(clientModel, mrp_routing, null);
    }


    public void create(Imrp_routing mrp_routing){
        Imrp_routing clientModel = mrp_routingFeignClient.create((mrp_routingImpl)mrp_routing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing.getClass(), false);
        copier.copy(clientModel, mrp_routing, null);
    }


    public Page<Imrp_routing> fetchDefault(SearchContext context){
        Page<mrp_routingImpl> page = this.mrp_routingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imrp_routing> mrp_routings){
        if(mrp_routings!=null){
            List<mrp_routingImpl> list = new ArrayList<mrp_routingImpl>();
            for(Imrp_routing imrp_routing :mrp_routings){
                list.add((mrp_routingImpl)imrp_routing) ;
            }
            mrp_routingFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_routing> mrp_routings){
        if(mrp_routings!=null){
            List<mrp_routingImpl> list = new ArrayList<mrp_routingImpl>();
            for(Imrp_routing imrp_routing :mrp_routings){
                list.add((mrp_routingImpl)imrp_routing) ;
            }
            mrp_routingFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imrp_routing mrp_routing){
        mrp_routingFeignClient.remove(mrp_routing.getId()) ;
    }


    public void updateBatch(List<Imrp_routing> mrp_routings){
        if(mrp_routings!=null){
            List<mrp_routingImpl> list = new ArrayList<mrp_routingImpl>();
            for(Imrp_routing imrp_routing :mrp_routings){
                list.add((mrp_routingImpl)imrp_routing) ;
            }
            mrp_routingFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imrp_routing> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_routing mrp_routing){
        Imrp_routing clientModel = mrp_routingFeignClient.getDraft(mrp_routing.getId(),(mrp_routingImpl)mrp_routing) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_routing.getClass(), false);
        copier.copy(clientModel, mrp_routing, null);
    }



}


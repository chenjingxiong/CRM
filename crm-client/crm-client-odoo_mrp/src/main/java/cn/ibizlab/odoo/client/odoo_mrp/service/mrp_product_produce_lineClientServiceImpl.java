package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce_line;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_product_produce_lineClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produce_lineImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_product_produce_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_product_produce_lineClientServiceImpl implements Imrp_product_produce_lineClientService {

    mrp_product_produce_lineFeignClient mrp_product_produce_lineFeignClient;

    @Autowired
    public mrp_product_produce_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_product_produce_lineFeignClient = nameBuilder.target(mrp_product_produce_lineFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_product_produce_lineFeignClient = nameBuilder.target(mrp_product_produce_lineFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_product_produce_line createModel() {
		return new mrp_product_produce_lineImpl();
	}


    public void createBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
        if(mrp_product_produce_lines!=null){
            List<mrp_product_produce_lineImpl> list = new ArrayList<mrp_product_produce_lineImpl>();
            for(Imrp_product_produce_line imrp_product_produce_line :mrp_product_produce_lines){
                list.add((mrp_product_produce_lineImpl)imrp_product_produce_line) ;
            }
            mrp_product_produce_lineFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
        if(mrp_product_produce_lines!=null){
            List<mrp_product_produce_lineImpl> list = new ArrayList<mrp_product_produce_lineImpl>();
            for(Imrp_product_produce_line imrp_product_produce_line :mrp_product_produce_lines){
                list.add((mrp_product_produce_lineImpl)imrp_product_produce_line) ;
            }
            mrp_product_produce_lineFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imrp_product_produce_line mrp_product_produce_line){
        Imrp_product_produce_line clientModel = mrp_product_produce_lineFeignClient.update(mrp_product_produce_line.getId(),(mrp_product_produce_lineImpl)mrp_product_produce_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce_line.getClass(), false);
        copier.copy(clientModel, mrp_product_produce_line, null);
    }


    public void remove(Imrp_product_produce_line mrp_product_produce_line){
        mrp_product_produce_lineFeignClient.remove(mrp_product_produce_line.getId()) ;
    }


    public Page<Imrp_product_produce_line> fetchDefault(SearchContext context){
        Page<mrp_product_produce_lineImpl> page = this.mrp_product_produce_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imrp_product_produce_line mrp_product_produce_line){
        Imrp_product_produce_line clientModel = mrp_product_produce_lineFeignClient.get(mrp_product_produce_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce_line.getClass(), false);
        copier.copy(clientModel, mrp_product_produce_line, null);
    }


    public void updateBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
        if(mrp_product_produce_lines!=null){
            List<mrp_product_produce_lineImpl> list = new ArrayList<mrp_product_produce_lineImpl>();
            for(Imrp_product_produce_line imrp_product_produce_line :mrp_product_produce_lines){
                list.add((mrp_product_produce_lineImpl)imrp_product_produce_line) ;
            }
            mrp_product_produce_lineFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imrp_product_produce_line mrp_product_produce_line){
        Imrp_product_produce_line clientModel = mrp_product_produce_lineFeignClient.create((mrp_product_produce_lineImpl)mrp_product_produce_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce_line.getClass(), false);
        copier.copy(clientModel, mrp_product_produce_line, null);
    }


    public Page<Imrp_product_produce_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_product_produce_line mrp_product_produce_line){
        Imrp_product_produce_line clientModel = mrp_product_produce_lineFeignClient.getDraft(mrp_product_produce_line.getId(),(mrp_product_produce_lineImpl)mrp_product_produce_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_product_produce_line.getClass(), false);
        copier.copy(clientModel, mrp_product_produce_line, null);
    }



}


package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce_line;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produce_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
public interface mrp_product_produce_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produce_lines/createbatch")
    public mrp_product_produce_lineImpl createBatch(@RequestBody List<mrp_product_produce_lineImpl> mrp_product_produce_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produce_lines/removebatch")
    public mrp_product_produce_lineImpl removeBatch(@RequestBody List<mrp_product_produce_lineImpl> mrp_product_produce_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produce_lines/{id}")
    public mrp_product_produce_lineImpl update(@PathVariable("id") Integer id,@RequestBody mrp_product_produce_lineImpl mrp_product_produce_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produce_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produce_lines/fetchdefault")
    public Page<mrp_product_produce_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produce_lines/{id}")
    public mrp_product_produce_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produce_lines/updatebatch")
    public mrp_product_produce_lineImpl updateBatch(@RequestBody List<mrp_product_produce_lineImpl> mrp_product_produce_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produce_lines")
    public mrp_product_produce_lineImpl create(@RequestBody mrp_product_produce_lineImpl mrp_product_produce_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produce_lines/select")
    public Page<mrp_product_produce_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produce_lines/{id}/getdraft")
    public mrp_product_produce_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_product_produce_lineImpl mrp_product_produce_line);



}

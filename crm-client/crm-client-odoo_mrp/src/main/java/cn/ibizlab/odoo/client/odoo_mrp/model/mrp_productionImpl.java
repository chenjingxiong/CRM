package cn.ibizlab.odoo.client.odoo_mrp.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imrp_production;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mrp_production] 对象
 */
public class mrp_productionImpl implements Imrp_production,Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 材料可用性
     */
    public String availability;

    @JsonIgnore
    public boolean availabilityDirtyFlag;
    
    /**
     * 物料清单
     */
    public Integer bom_id;

    @JsonIgnore
    public boolean bom_idDirtyFlag;
    
    /**
     * 检查生产的数量
     */
    public String check_to_done;

    @JsonIgnore
    public boolean check_to_doneDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 消费量少于计划数量
     */
    public String consumed_less_than_planned;

    @JsonIgnore
    public boolean consumed_less_than_plannedDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_finished;

    @JsonIgnore
    public boolean date_finishedDirtyFlag;
    
    /**
     * 截止日期结束
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned_finished;

    @JsonIgnore
    public boolean date_planned_finishedDirtyFlag;
    
    /**
     * 截止日期开始
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned_start;

    @JsonIgnore
    public boolean date_planned_startDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 出库单
     */
    public Integer delivery_count;

    @JsonIgnore
    public boolean delivery_countDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 完工产品
     */
    public String finished_move_line_ids;

    @JsonIgnore
    public boolean finished_move_line_idsDirtyFlag;
    
    /**
     * 有移动
     */
    public String has_moves;

    @JsonIgnore
    public boolean has_movesDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 成品位置
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 成品位置
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 原料位置
     */
    public Integer location_src_id;

    @JsonIgnore
    public boolean location_src_idDirtyFlag;
    
    /**
     * 原料位置
     */
    public String location_src_id_text;

    @JsonIgnore
    public boolean location_src_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 生产货物的库存移动
     */
    public String move_dest_ids;

    @JsonIgnore
    public boolean move_dest_idsDirtyFlag;
    
    /**
     * 产成品
     */
    public String move_finished_ids;

    @JsonIgnore
    public boolean move_finished_idsDirtyFlag;
    
    /**
     * 原材料
     */
    public String move_raw_ids;

    @JsonIgnore
    public boolean move_raw_idsDirtyFlag;
    
    /**
     * 参考
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 来源
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 与此制造订单相关的拣货
     */
    public String picking_ids;

    @JsonIgnore
    public boolean picking_idsDirtyFlag;
    
    /**
     * 作业类型
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 作业类型
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 允许发布库存
     */
    public String post_visible;

    @JsonIgnore
    public boolean post_visibleDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 补货组
     */
    public Integer procurement_group_id;

    @JsonIgnore
    public boolean procurement_group_idDirtyFlag;
    
    /**
     * 生产位置
     */
    public Integer production_location_id;

    @JsonIgnore
    public boolean production_location_idDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 待生产数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 数量总计
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 传播取消以及拆分
     */
    public String propagate;

    @JsonIgnore
    public boolean propagateDirtyFlag;
    
    /**
     * 已生产数量
     */
    public Double qty_produced;

    @JsonIgnore
    public boolean qty_producedDirtyFlag;
    
    /**
     * 工艺
     */
    public Integer routing_id;

    @JsonIgnore
    public boolean routing_idDirtyFlag;
    
    /**
     * 工艺
     */
    public String routing_id_text;

    @JsonIgnore
    public boolean routing_id_textDirtyFlag;
    
    /**
     * 报废转移
     */
    public Integer scrap_count;

    @JsonIgnore
    public boolean scrap_countDirtyFlag;
    
    /**
     * 报废
     */
    public String scrap_ids;

    @JsonIgnore
    public boolean scrap_idsDirtyFlag;
    
    /**
     * 显示最后一批
     */
    public String show_final_lots;

    @JsonIgnore
    public boolean show_final_lotsDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 允许取消预留库存
     */
    public String unreserve_visible;

    @JsonIgnore
    public boolean unreserve_visibleDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * # 工单
     */
    public Integer workorder_count;

    @JsonIgnore
    public boolean workorder_countDirtyFlag;
    
    /**
     * # 完工工单
     */
    public Integer workorder_done_count;

    @JsonIgnore
    public boolean workorder_done_countDirtyFlag;
    
    /**
     * 工单
     */
    public String workorder_ids;

    @JsonIgnore
    public boolean workorder_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [材料可用性]
     */
    @JsonProperty("availability")
    public String getAvailability(){
        return this.availability ;
    }

    /**
     * 设置 [材料可用性]
     */
    @JsonProperty("availability")
    public void setAvailability(String  availability){
        this.availability = availability ;
        this.availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [材料可用性]脏标记
     */
    @JsonIgnore
    public boolean getAvailabilityDirtyFlag(){
        return this.availabilityDirtyFlag ;
    }   

    /**
     * 获取 [物料清单]
     */
    @JsonProperty("bom_id")
    public Integer getBom_id(){
        return this.bom_id ;
    }

    /**
     * 设置 [物料清单]
     */
    @JsonProperty("bom_id")
    public void setBom_id(Integer  bom_id){
        this.bom_id = bom_id ;
        this.bom_idDirtyFlag = true ;
    }

     /**
     * 获取 [物料清单]脏标记
     */
    @JsonIgnore
    public boolean getBom_idDirtyFlag(){
        return this.bom_idDirtyFlag ;
    }   

    /**
     * 获取 [检查生产的数量]
     */
    @JsonProperty("check_to_done")
    public String getCheck_to_done(){
        return this.check_to_done ;
    }

    /**
     * 设置 [检查生产的数量]
     */
    @JsonProperty("check_to_done")
    public void setCheck_to_done(String  check_to_done){
        this.check_to_done = check_to_done ;
        this.check_to_doneDirtyFlag = true ;
    }

     /**
     * 获取 [检查生产的数量]脏标记
     */
    @JsonIgnore
    public boolean getCheck_to_doneDirtyFlag(){
        return this.check_to_doneDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [消费量少于计划数量]
     */
    @JsonProperty("consumed_less_than_planned")
    public String getConsumed_less_than_planned(){
        return this.consumed_less_than_planned ;
    }

    /**
     * 设置 [消费量少于计划数量]
     */
    @JsonProperty("consumed_less_than_planned")
    public void setConsumed_less_than_planned(String  consumed_less_than_planned){
        this.consumed_less_than_planned = consumed_less_than_planned ;
        this.consumed_less_than_plannedDirtyFlag = true ;
    }

     /**
     * 获取 [消费量少于计划数量]脏标记
     */
    @JsonIgnore
    public boolean getConsumed_less_than_plannedDirtyFlag(){
        return this.consumed_less_than_plannedDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_finished")
    public Timestamp getDate_finished(){
        return this.date_finished ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_finished")
    public void setDate_finished(Timestamp  date_finished){
        this.date_finished = date_finished ;
        this.date_finishedDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_finishedDirtyFlag(){
        return this.date_finishedDirtyFlag ;
    }   

    /**
     * 获取 [截止日期结束]
     */
    @JsonProperty("date_planned_finished")
    public Timestamp getDate_planned_finished(){
        return this.date_planned_finished ;
    }

    /**
     * 设置 [截止日期结束]
     */
    @JsonProperty("date_planned_finished")
    public void setDate_planned_finished(Timestamp  date_planned_finished){
        this.date_planned_finished = date_planned_finished ;
        this.date_planned_finishedDirtyFlag = true ;
    }

     /**
     * 获取 [截止日期结束]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_finishedDirtyFlag(){
        return this.date_planned_finishedDirtyFlag ;
    }   

    /**
     * 获取 [截止日期开始]
     */
    @JsonProperty("date_planned_start")
    public Timestamp getDate_planned_start(){
        return this.date_planned_start ;
    }

    /**
     * 设置 [截止日期开始]
     */
    @JsonProperty("date_planned_start")
    public void setDate_planned_start(Timestamp  date_planned_start){
        this.date_planned_start = date_planned_start ;
        this.date_planned_startDirtyFlag = true ;
    }

     /**
     * 获取 [截止日期开始]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_startDirtyFlag(){
        return this.date_planned_startDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [出库单]
     */
    @JsonProperty("delivery_count")
    public Integer getDelivery_count(){
        return this.delivery_count ;
    }

    /**
     * 设置 [出库单]
     */
    @JsonProperty("delivery_count")
    public void setDelivery_count(Integer  delivery_count){
        this.delivery_count = delivery_count ;
        this.delivery_countDirtyFlag = true ;
    }

     /**
     * 获取 [出库单]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_countDirtyFlag(){
        return this.delivery_countDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [完工产品]
     */
    @JsonProperty("finished_move_line_ids")
    public String getFinished_move_line_ids(){
        return this.finished_move_line_ids ;
    }

    /**
     * 设置 [完工产品]
     */
    @JsonProperty("finished_move_line_ids")
    public void setFinished_move_line_ids(String  finished_move_line_ids){
        this.finished_move_line_ids = finished_move_line_ids ;
        this.finished_move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [完工产品]脏标记
     */
    @JsonIgnore
    public boolean getFinished_move_line_idsDirtyFlag(){
        return this.finished_move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [有移动]
     */
    @JsonProperty("has_moves")
    public String getHas_moves(){
        return this.has_moves ;
    }

    /**
     * 设置 [有移动]
     */
    @JsonProperty("has_moves")
    public void setHas_moves(String  has_moves){
        this.has_moves = has_moves ;
        this.has_movesDirtyFlag = true ;
    }

     /**
     * 获取 [有移动]脏标记
     */
    @JsonIgnore
    public boolean getHas_movesDirtyFlag(){
        return this.has_movesDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [成品位置]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [成品位置]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [成品位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [成品位置]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [成品位置]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [成品位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [原料位置]
     */
    @JsonProperty("location_src_id")
    public Integer getLocation_src_id(){
        return this.location_src_id ;
    }

    /**
     * 设置 [原料位置]
     */
    @JsonProperty("location_src_id")
    public void setLocation_src_id(Integer  location_src_id){
        this.location_src_id = location_src_id ;
        this.location_src_idDirtyFlag = true ;
    }

     /**
     * 获取 [原料位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_idDirtyFlag(){
        return this.location_src_idDirtyFlag ;
    }   

    /**
     * 获取 [原料位置]
     */
    @JsonProperty("location_src_id_text")
    public String getLocation_src_id_text(){
        return this.location_src_id_text ;
    }

    /**
     * 设置 [原料位置]
     */
    @JsonProperty("location_src_id_text")
    public void setLocation_src_id_text(String  location_src_id_text){
        this.location_src_id_text = location_src_id_text ;
        this.location_src_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [原料位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_id_textDirtyFlag(){
        return this.location_src_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [生产货物的库存移动]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return this.move_dest_ids ;
    }

    /**
     * 设置 [生产货物的库存移动]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

     /**
     * 获取 [生产货物的库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return this.move_dest_idsDirtyFlag ;
    }   

    /**
     * 获取 [产成品]
     */
    @JsonProperty("move_finished_ids")
    public String getMove_finished_ids(){
        return this.move_finished_ids ;
    }

    /**
     * 设置 [产成品]
     */
    @JsonProperty("move_finished_ids")
    public void setMove_finished_ids(String  move_finished_ids){
        this.move_finished_ids = move_finished_ids ;
        this.move_finished_idsDirtyFlag = true ;
    }

     /**
     * 获取 [产成品]脏标记
     */
    @JsonIgnore
    public boolean getMove_finished_idsDirtyFlag(){
        return this.move_finished_idsDirtyFlag ;
    }   

    /**
     * 获取 [原材料]
     */
    @JsonProperty("move_raw_ids")
    public String getMove_raw_ids(){
        return this.move_raw_ids ;
    }

    /**
     * 设置 [原材料]
     */
    @JsonProperty("move_raw_ids")
    public void setMove_raw_ids(String  move_raw_ids){
        this.move_raw_ids = move_raw_ids ;
        this.move_raw_idsDirtyFlag = true ;
    }

     /**
     * 获取 [原材料]脏标记
     */
    @JsonIgnore
    public boolean getMove_raw_idsDirtyFlag(){
        return this.move_raw_idsDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [与此制造订单相关的拣货]
     */
    @JsonProperty("picking_ids")
    public String getPicking_ids(){
        return this.picking_ids ;
    }

    /**
     * 设置 [与此制造订单相关的拣货]
     */
    @JsonProperty("picking_ids")
    public void setPicking_ids(String  picking_ids){
        this.picking_ids = picking_ids ;
        this.picking_idsDirtyFlag = true ;
    }

     /**
     * 获取 [与此制造订单相关的拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idsDirtyFlag(){
        return this.picking_idsDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [允许发布库存]
     */
    @JsonProperty("post_visible")
    public String getPost_visible(){
        return this.post_visible ;
    }

    /**
     * 设置 [允许发布库存]
     */
    @JsonProperty("post_visible")
    public void setPost_visible(String  post_visible){
        this.post_visible = post_visible ;
        this.post_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [允许发布库存]脏标记
     */
    @JsonIgnore
    public boolean getPost_visibleDirtyFlag(){
        return this.post_visibleDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return this.procurement_group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return this.procurement_group_idDirtyFlag ;
    }   

    /**
     * 获取 [生产位置]
     */
    @JsonProperty("production_location_id")
    public Integer getProduction_location_id(){
        return this.production_location_id ;
    }

    /**
     * 设置 [生产位置]
     */
    @JsonProperty("production_location_id")
    public void setProduction_location_id(Integer  production_location_id){
        this.production_location_id = production_location_id ;
        this.production_location_idDirtyFlag = true ;
    }

     /**
     * 获取 [生产位置]脏标记
     */
    @JsonIgnore
    public boolean getProduction_location_idDirtyFlag(){
        return this.production_location_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [待生产数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [待生产数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [待生产数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量总计]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [数量总计]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量总计]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return this.propagate ;
    }

    /**
     * 设置 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

     /**
     * 获取 [传播取消以及拆分]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return this.propagateDirtyFlag ;
    }   

    /**
     * 获取 [已生产数量]
     */
    @JsonProperty("qty_produced")
    public Double getQty_produced(){
        return this.qty_produced ;
    }

    /**
     * 设置 [已生产数量]
     */
    @JsonProperty("qty_produced")
    public void setQty_produced(Double  qty_produced){
        this.qty_produced = qty_produced ;
        this.qty_producedDirtyFlag = true ;
    }

     /**
     * 获取 [已生产数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_producedDirtyFlag(){
        return this.qty_producedDirtyFlag ;
    }   

    /**
     * 获取 [工艺]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return this.routing_id ;
    }

    /**
     * 设置 [工艺]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

     /**
     * 获取 [工艺]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return this.routing_idDirtyFlag ;
    }   

    /**
     * 获取 [工艺]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return this.routing_id_text ;
    }

    /**
     * 设置 [工艺]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工艺]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return this.routing_id_textDirtyFlag ;
    }   

    /**
     * 获取 [报废转移]
     */
    @JsonProperty("scrap_count")
    public Integer getScrap_count(){
        return this.scrap_count ;
    }

    /**
     * 设置 [报废转移]
     */
    @JsonProperty("scrap_count")
    public void setScrap_count(Integer  scrap_count){
        this.scrap_count = scrap_count ;
        this.scrap_countDirtyFlag = true ;
    }

     /**
     * 获取 [报废转移]脏标记
     */
    @JsonIgnore
    public boolean getScrap_countDirtyFlag(){
        return this.scrap_countDirtyFlag ;
    }   

    /**
     * 获取 [报废]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return this.scrap_ids ;
    }

    /**
     * 设置 [报废]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

     /**
     * 获取 [报废]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return this.scrap_idsDirtyFlag ;
    }   

    /**
     * 获取 [显示最后一批]
     */
    @JsonProperty("show_final_lots")
    public String getShow_final_lots(){
        return this.show_final_lots ;
    }

    /**
     * 设置 [显示最后一批]
     */
    @JsonProperty("show_final_lots")
    public void setShow_final_lots(String  show_final_lots){
        this.show_final_lots = show_final_lots ;
        this.show_final_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [显示最后一批]脏标记
     */
    @JsonIgnore
    public boolean getShow_final_lotsDirtyFlag(){
        return this.show_final_lotsDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [允许取消预留库存]
     */
    @JsonProperty("unreserve_visible")
    public String getUnreserve_visible(){
        return this.unreserve_visible ;
    }

    /**
     * 设置 [允许取消预留库存]
     */
    @JsonProperty("unreserve_visible")
    public void setUnreserve_visible(String  unreserve_visible){
        this.unreserve_visible = unreserve_visible ;
        this.unreserve_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [允许取消预留库存]脏标记
     */
    @JsonIgnore
    public boolean getUnreserve_visibleDirtyFlag(){
        return this.unreserve_visibleDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [# 工单]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return this.workorder_count ;
    }

    /**
     * 设置 [# 工单]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return this.workorder_countDirtyFlag ;
    }   

    /**
     * 获取 [# 完工工单]
     */
    @JsonProperty("workorder_done_count")
    public Integer getWorkorder_done_count(){
        return this.workorder_done_count ;
    }

    /**
     * 设置 [# 完工工单]
     */
    @JsonProperty("workorder_done_count")
    public void setWorkorder_done_count(Integer  workorder_done_count){
        this.workorder_done_count = workorder_done_count ;
        this.workorder_done_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 完工工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_done_countDirtyFlag(){
        return this.workorder_done_countDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("workorder_ids")
    public String getWorkorder_ids(){
        return this.workorder_ids ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("workorder_ids")
    public void setWorkorder_ids(String  workorder_ids){
        this.workorder_ids = workorder_ids ;
        this.workorder_idsDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idsDirtyFlag(){
        return this.workorder_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

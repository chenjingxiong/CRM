package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bom_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_bom_line] 服务对象接口
 */
public interface mrp_bom_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_bom_lines/updatebatch")
    public mrp_bom_lineImpl updateBatch(@RequestBody List<mrp_bom_lineImpl> mrp_bom_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_bom_lines/fetchdefault")
    public Page<mrp_bom_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_bom_lines/createbatch")
    public mrp_bom_lineImpl createBatch(@RequestBody List<mrp_bom_lineImpl> mrp_bom_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_bom_lines/removebatch")
    public mrp_bom_lineImpl removeBatch(@RequestBody List<mrp_bom_lineImpl> mrp_bom_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_bom_lines/{id}")
    public mrp_bom_lineImpl update(@PathVariable("id") Integer id,@RequestBody mrp_bom_lineImpl mrp_bom_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_bom_lines/{id}")
    public mrp_bom_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_bom_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_bom_lines")
    public mrp_bom_lineImpl create(@RequestBody mrp_bom_lineImpl mrp_bom_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_bom_lines/select")
    public Page<mrp_bom_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_bom_lines/{id}/getdraft")
    public mrp_bom_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_bom_lineImpl mrp_bom_line);



}

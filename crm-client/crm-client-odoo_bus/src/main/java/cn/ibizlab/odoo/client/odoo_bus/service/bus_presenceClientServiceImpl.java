package cn.ibizlab.odoo.client.odoo_bus.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibus_presence;
import cn.ibizlab.odoo.client.odoo_bus.config.odoo_busClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibus_presenceClientService;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_presenceImpl;
import cn.ibizlab.odoo.client.odoo_bus.feign.bus_presenceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[bus_presence] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class bus_presenceClientServiceImpl implements Ibus_presenceClientService {

    bus_presenceFeignClient bus_presenceFeignClient;

    @Autowired
    public bus_presenceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_busClientProperties odoo_busClientProperties) {
        if (odoo_busClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.bus_presenceFeignClient = nameBuilder.target(bus_presenceFeignClient.class,"http://"+odoo_busClientProperties.getServiceId()+"/") ;
		}else if (odoo_busClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.bus_presenceFeignClient = nameBuilder.target(bus_presenceFeignClient.class,odoo_busClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibus_presence createModel() {
		return new bus_presenceImpl();
	}


    public void updateBatch(List<Ibus_presence> bus_presences){
        if(bus_presences!=null){
            List<bus_presenceImpl> list = new ArrayList<bus_presenceImpl>();
            for(Ibus_presence ibus_presence :bus_presences){
                list.add((bus_presenceImpl)ibus_presence) ;
            }
            bus_presenceFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ibus_presence bus_presence){
        Ibus_presence clientModel = bus_presenceFeignClient.get(bus_presence.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_presence.getClass(), false);
        copier.copy(clientModel, bus_presence, null);
    }


    public Page<Ibus_presence> fetchDefault(SearchContext context){
        Page<bus_presenceImpl> page = this.bus_presenceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ibus_presence bus_presence){
        Ibus_presence clientModel = bus_presenceFeignClient.create((bus_presenceImpl)bus_presence) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_presence.getClass(), false);
        copier.copy(clientModel, bus_presence, null);
    }


    public void createBatch(List<Ibus_presence> bus_presences){
        if(bus_presences!=null){
            List<bus_presenceImpl> list = new ArrayList<bus_presenceImpl>();
            for(Ibus_presence ibus_presence :bus_presences){
                list.add((bus_presenceImpl)ibus_presence) ;
            }
            bus_presenceFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibus_presence> bus_presences){
        if(bus_presences!=null){
            List<bus_presenceImpl> list = new ArrayList<bus_presenceImpl>();
            for(Ibus_presence ibus_presence :bus_presences){
                list.add((bus_presenceImpl)ibus_presence) ;
            }
            bus_presenceFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ibus_presence bus_presence){
        Ibus_presence clientModel = bus_presenceFeignClient.update(bus_presence.getId(),(bus_presenceImpl)bus_presence) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_presence.getClass(), false);
        copier.copy(clientModel, bus_presence, null);
    }


    public void remove(Ibus_presence bus_presence){
        bus_presenceFeignClient.remove(bus_presence.getId()) ;
    }


    public Page<Ibus_presence> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibus_presence bus_presence){
        Ibus_presence clientModel = bus_presenceFeignClient.getDraft(bus_presence.getId(),(bus_presenceImpl)bus_presence) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_presence.getClass(), false);
        copier.copy(clientModel, bus_presence, null);
    }



}


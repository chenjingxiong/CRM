package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax_template;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_fiscal_position_tax_template] 对象
 */
public class account_fiscal_position_tax_templateImpl implements Iaccount_fiscal_position_tax_template,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer position_id;

    @JsonIgnore
    public boolean position_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String position_id_text;

    @JsonIgnore
    public boolean position_id_textDirtyFlag;
    
    /**
     * 替代税
     */
    public Integer tax_dest_id;

    @JsonIgnore
    public boolean tax_dest_idDirtyFlag;
    
    /**
     * 替代税
     */
    public String tax_dest_id_text;

    @JsonIgnore
    public boolean tax_dest_id_textDirtyFlag;
    
    /**
     * 税源
     */
    public Integer tax_src_id;

    @JsonIgnore
    public boolean tax_src_idDirtyFlag;
    
    /**
     * 税源
     */
    public String tax_src_id_text;

    @JsonIgnore
    public boolean tax_src_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("position_id")
    public Integer getPosition_id(){
        return this.position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("position_id")
    public void setPosition_id(Integer  position_id){
        this.position_id = position_id ;
        this.position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getPosition_idDirtyFlag(){
        return this.position_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("position_id_text")
    public String getPosition_id_text(){
        return this.position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("position_id_text")
    public void setPosition_id_text(String  position_id_text){
        this.position_id_text = position_id_text ;
        this.position_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getPosition_id_textDirtyFlag(){
        return this.position_id_textDirtyFlag ;
    }   

    /**
     * 获取 [替代税]
     */
    @JsonProperty("tax_dest_id")
    public Integer getTax_dest_id(){
        return this.tax_dest_id ;
    }

    /**
     * 设置 [替代税]
     */
    @JsonProperty("tax_dest_id")
    public void setTax_dest_id(Integer  tax_dest_id){
        this.tax_dest_id = tax_dest_id ;
        this.tax_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [替代税]脏标记
     */
    @JsonIgnore
    public boolean getTax_dest_idDirtyFlag(){
        return this.tax_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [替代税]
     */
    @JsonProperty("tax_dest_id_text")
    public String getTax_dest_id_text(){
        return this.tax_dest_id_text ;
    }

    /**
     * 设置 [替代税]
     */
    @JsonProperty("tax_dest_id_text")
    public void setTax_dest_id_text(String  tax_dest_id_text){
        this.tax_dest_id_text = tax_dest_id_text ;
        this.tax_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [替代税]脏标记
     */
    @JsonIgnore
    public boolean getTax_dest_id_textDirtyFlag(){
        return this.tax_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税源]
     */
    @JsonProperty("tax_src_id")
    public Integer getTax_src_id(){
        return this.tax_src_id ;
    }

    /**
     * 设置 [税源]
     */
    @JsonProperty("tax_src_id")
    public void setTax_src_id(Integer  tax_src_id){
        this.tax_src_id = tax_src_id ;
        this.tax_src_idDirtyFlag = true ;
    }

     /**
     * 获取 [税源]脏标记
     */
    @JsonIgnore
    public boolean getTax_src_idDirtyFlag(){
        return this.tax_src_idDirtyFlag ;
    }   

    /**
     * 获取 [税源]
     */
    @JsonProperty("tax_src_id_text")
    public String getTax_src_id_text(){
        return this.tax_src_id_text ;
    }

    /**
     * 设置 [税源]
     */
    @JsonProperty("tax_src_id_text")
    public void setTax_src_id_text(String  tax_src_id_text){
        this.tax_src_id_text = tax_src_id_text ;
        this.tax_src_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税源]脏标记
     */
    @JsonIgnore
    public boolean getTax_src_id_textDirtyFlag(){
        return this.tax_src_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

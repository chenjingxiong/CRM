package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_tag;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_account_tag] 服务对象接口
 */
public interface account_account_tagFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_tags/removebatch")
    public account_account_tagImpl removeBatch(@RequestBody List<account_account_tagImpl> account_account_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_tags")
    public account_account_tagImpl create(@RequestBody account_account_tagImpl account_account_tag);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_tags/updatebatch")
    public account_account_tagImpl updateBatch(@RequestBody List<account_account_tagImpl> account_account_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_tags/createbatch")
    public account_account_tagImpl createBatch(@RequestBody List<account_account_tagImpl> account_account_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_tags/fetchdefault")
    public Page<account_account_tagImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_tags/{id}")
    public account_account_tagImpl update(@PathVariable("id") Integer id,@RequestBody account_account_tagImpl account_account_tag);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_tags/{id}")
    public account_account_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_tags/select")
    public Page<account_account_tagImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_tags/{id}/getdraft")
    public account_account_tagImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_account_tagImpl account_account_tag);



}

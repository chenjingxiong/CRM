package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account_template;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_account_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_position_account_template] 服务对象接口
 */
public interface account_fiscal_position_account_templateFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_account_templates/createbatch")
    public account_fiscal_position_account_templateImpl createBatch(@RequestBody List<account_fiscal_position_account_templateImpl> account_fiscal_position_account_templates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_account_templates")
    public account_fiscal_position_account_templateImpl create(@RequestBody account_fiscal_position_account_templateImpl account_fiscal_position_account_template);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_account_templates/removebatch")
    public account_fiscal_position_account_templateImpl removeBatch(@RequestBody List<account_fiscal_position_account_templateImpl> account_fiscal_position_account_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_account_templates/fetchdefault")
    public Page<account_fiscal_position_account_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_account_templates/{id}")
    public account_fiscal_position_account_templateImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_account_templateImpl account_fiscal_position_account_template);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_account_templates/updatebatch")
    public account_fiscal_position_account_templateImpl updateBatch(@RequestBody List<account_fiscal_position_account_templateImpl> account_fiscal_position_account_templates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_account_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_account_templates/{id}")
    public account_fiscal_position_account_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_account_templates/select")
    public Page<account_fiscal_position_account_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_account_templates/{id}/getdraft")
    public account_fiscal_position_account_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_account_templateImpl account_fiscal_position_account_template);



}

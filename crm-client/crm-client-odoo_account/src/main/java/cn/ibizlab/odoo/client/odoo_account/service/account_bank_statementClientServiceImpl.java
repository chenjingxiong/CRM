package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statementClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statementImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statementFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statementClientServiceImpl implements Iaccount_bank_statementClientService {

    account_bank_statementFeignClient account_bank_statementFeignClient;

    @Autowired
    public account_bank_statementClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statementFeignClient = nameBuilder.target(account_bank_statementFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statementFeignClient = nameBuilder.target(account_bank_statementFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement createModel() {
		return new account_bank_statementImpl();
	}


    public void remove(Iaccount_bank_statement account_bank_statement){
        account_bank_statementFeignClient.remove(account_bank_statement.getId()) ;
    }


    public Page<Iaccount_bank_statement> fetchDefault(SearchContext context){
        Page<account_bank_statementImpl> page = this.account_bank_statementFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iaccount_bank_statement> account_bank_statements){
        if(account_bank_statements!=null){
            List<account_bank_statementImpl> list = new ArrayList<account_bank_statementImpl>();
            for(Iaccount_bank_statement iaccount_bank_statement :account_bank_statements){
                list.add((account_bank_statementImpl)iaccount_bank_statement) ;
            }
            account_bank_statementFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_bank_statement account_bank_statement){
        Iaccount_bank_statement clientModel = account_bank_statementFeignClient.get(account_bank_statement.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement.getClass(), false);
        copier.copy(clientModel, account_bank_statement, null);
    }


    public void removeBatch(List<Iaccount_bank_statement> account_bank_statements){
        if(account_bank_statements!=null){
            List<account_bank_statementImpl> list = new ArrayList<account_bank_statementImpl>();
            for(Iaccount_bank_statement iaccount_bank_statement :account_bank_statements){
                list.add((account_bank_statementImpl)iaccount_bank_statement) ;
            }
            account_bank_statementFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_bank_statement account_bank_statement){
        Iaccount_bank_statement clientModel = account_bank_statementFeignClient.create((account_bank_statementImpl)account_bank_statement) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement.getClass(), false);
        copier.copy(clientModel, account_bank_statement, null);
    }


    public void update(Iaccount_bank_statement account_bank_statement){
        Iaccount_bank_statement clientModel = account_bank_statementFeignClient.update(account_bank_statement.getId(),(account_bank_statementImpl)account_bank_statement) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement.getClass(), false);
        copier.copy(clientModel, account_bank_statement, null);
    }


    public void updateBatch(List<Iaccount_bank_statement> account_bank_statements){
        if(account_bank_statements!=null){
            List<account_bank_statementImpl> list = new ArrayList<account_bank_statementImpl>();
            for(Iaccount_bank_statement iaccount_bank_statement :account_bank_statements){
                list.add((account_bank_statementImpl)iaccount_bank_statement) ;
            }
            account_bank_statementFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_bank_statement> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement account_bank_statement){
        Iaccount_bank_statement clientModel = account_bank_statementFeignClient.getDraft(account_bank_statement.getId(),(account_bank_statementImpl)account_bank_statement) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement.getClass(), false);
        copier.copy(clientModel, account_bank_statement, null);
    }



}


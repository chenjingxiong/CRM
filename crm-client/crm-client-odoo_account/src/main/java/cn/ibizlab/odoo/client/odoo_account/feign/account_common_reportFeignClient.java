package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_common_report;
import cn.ibizlab.odoo.client.odoo_account.model.account_common_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_common_report] 服务对象接口
 */
public interface account_common_reportFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_common_reports/{id}")
    public account_common_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_common_reports/updatebatch")
    public account_common_reportImpl updateBatch(@RequestBody List<account_common_reportImpl> account_common_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_common_reports")
    public account_common_reportImpl create(@RequestBody account_common_reportImpl account_common_report);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_common_reports/createbatch")
    public account_common_reportImpl createBatch(@RequestBody List<account_common_reportImpl> account_common_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_common_reports/removebatch")
    public account_common_reportImpl removeBatch(@RequestBody List<account_common_reportImpl> account_common_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_common_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_common_reports/fetchdefault")
    public Page<account_common_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_common_reports/{id}")
    public account_common_reportImpl update(@PathVariable("id") Integer id,@RequestBody account_common_reportImpl account_common_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_common_reports/select")
    public Page<account_common_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_common_reports/{id}/getdraft")
    public account_common_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_common_reportImpl account_common_report);



}

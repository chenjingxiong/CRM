package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_type;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_account_type] 服务对象接口
 */
public interface account_account_typeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_types/createbatch")
    public account_account_typeImpl createBatch(@RequestBody List<account_account_typeImpl> account_account_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_types/removebatch")
    public account_account_typeImpl removeBatch(@RequestBody List<account_account_typeImpl> account_account_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_types")
    public account_account_typeImpl create(@RequestBody account_account_typeImpl account_account_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_types/fetchdefault")
    public Page<account_account_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_types/updatebatch")
    public account_account_typeImpl updateBatch(@RequestBody List<account_account_typeImpl> account_account_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_types/{id}")
    public account_account_typeImpl update(@PathVariable("id") Integer id,@RequestBody account_account_typeImpl account_account_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_types/{id}")
    public account_account_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_types/select")
    public Page<account_account_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_types/{id}/getdraft")
    public account_account_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_account_typeImpl account_account_type);



}

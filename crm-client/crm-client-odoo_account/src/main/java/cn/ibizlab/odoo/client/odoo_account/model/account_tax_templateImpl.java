package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_template;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_tax_template] 对象
 */
public class account_tax_templateImpl implements Iaccount_tax_template,Serializable{

    /**
     * 税率科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 税率科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 税率计算
     */
    public String amount_type;

    @JsonIgnore
    public boolean amount_typeDirtyFlag;
    
    /**
     * 分析成本
     */
    public String analytic;

    @JsonIgnore
    public boolean analyticDirtyFlag;
    
    /**
     * 税应收科目
     */
    public Integer cash_basis_account_id;

    @JsonIgnore
    public boolean cash_basis_account_idDirtyFlag;
    
    /**
     * 税应收科目
     */
    public String cash_basis_account_id_text;

    @JsonIgnore
    public boolean cash_basis_account_id_textDirtyFlag;
    
    /**
     * 基本税应收科目
     */
    public Integer cash_basis_base_account_id;

    @JsonIgnore
    public boolean cash_basis_base_account_idDirtyFlag;
    
    /**
     * 基本税应收科目
     */
    public String cash_basis_base_account_id_text;

    @JsonIgnore
    public boolean cash_basis_base_account_id_textDirtyFlag;
    
    /**
     * 表模板
     */
    public Integer chart_template_id;

    @JsonIgnore
    public boolean chart_template_idDirtyFlag;
    
    /**
     * 表模板
     */
    public String chart_template_id_text;

    @JsonIgnore
    public boolean chart_template_id_textDirtyFlag;
    
    /**
     * 子级税
     */
    public String children_tax_ids;

    @JsonIgnore
    public boolean children_tax_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示在发票上
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 影响后续税收
     */
    public String include_base_amount;

    @JsonIgnore
    public boolean include_base_amountDirtyFlag;
    
    /**
     * 税率名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 包含在价格中
     */
    public String price_include;

    @JsonIgnore
    public boolean price_includeDirtyFlag;
    
    /**
     * 退款的税金科目
     */
    public Integer refund_account_id;

    @JsonIgnore
    public boolean refund_account_idDirtyFlag;
    
    /**
     * 退款的税金科目
     */
    public String refund_account_id_text;

    @JsonIgnore
    public boolean refund_account_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 科目标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 应有税金
     */
    public String tax_exigibility;

    @JsonIgnore
    public boolean tax_exigibilityDirtyFlag;
    
    /**
     * 税组
     */
    public Integer tax_group_id;

    @JsonIgnore
    public boolean tax_group_idDirtyFlag;
    
    /**
     * 税组
     */
    public String tax_group_id_text;

    @JsonIgnore
    public boolean tax_group_id_textDirtyFlag;
    
    /**
     * 税范围
     */
    public String type_tax_use;

    @JsonIgnore
    public boolean type_tax_useDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [税率计算]
     */
    @JsonProperty("amount_type")
    public String getAmount_type(){
        return this.amount_type ;
    }

    /**
     * 设置 [税率计算]
     */
    @JsonProperty("amount_type")
    public void setAmount_type(String  amount_type){
        this.amount_type = amount_type ;
        this.amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [税率计算]脏标记
     */
    @JsonIgnore
    public boolean getAmount_typeDirtyFlag(){
        return this.amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [分析成本]
     */
    @JsonProperty("analytic")
    public String getAnalytic(){
        return this.analytic ;
    }

    /**
     * 设置 [分析成本]
     */
    @JsonProperty("analytic")
    public void setAnalytic(String  analytic){
        this.analytic = analytic ;
        this.analyticDirtyFlag = true ;
    }

     /**
     * 获取 [分析成本]脏标记
     */
    @JsonIgnore
    public boolean getAnalyticDirtyFlag(){
        return this.analyticDirtyFlag ;
    }   

    /**
     * 获取 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id")
    public Integer getCash_basis_account_id(){
        return this.cash_basis_account_id ;
    }

    /**
     * 设置 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id")
    public void setCash_basis_account_id(Integer  cash_basis_account_id){
        this.cash_basis_account_id = cash_basis_account_id ;
        this.cash_basis_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_idDirtyFlag(){
        return this.cash_basis_account_idDirtyFlag ;
    }   

    /**
     * 获取 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id_text")
    public String getCash_basis_account_id_text(){
        return this.cash_basis_account_id_text ;
    }

    /**
     * 设置 [税应收科目]
     */
    @JsonProperty("cash_basis_account_id_text")
    public void setCash_basis_account_id_text(String  cash_basis_account_id_text){
        this.cash_basis_account_id_text = cash_basis_account_id_text ;
        this.cash_basis_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_id_textDirtyFlag(){
        return this.cash_basis_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id")
    public Integer getCash_basis_base_account_id(){
        return this.cash_basis_base_account_id ;
    }

    /**
     * 设置 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id")
    public void setCash_basis_base_account_id(Integer  cash_basis_base_account_id){
        this.cash_basis_base_account_id = cash_basis_base_account_id ;
        this.cash_basis_base_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [基本税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_idDirtyFlag(){
        return this.cash_basis_base_account_idDirtyFlag ;
    }   

    /**
     * 获取 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public String getCash_basis_base_account_id_text(){
        return this.cash_basis_base_account_id_text ;
    }

    /**
     * 设置 [基本税应收科目]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public void setCash_basis_base_account_id_text(String  cash_basis_base_account_id_text){
        this.cash_basis_base_account_id_text = cash_basis_base_account_id_text ;
        this.cash_basis_base_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [基本税应收科目]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_id_textDirtyFlag(){
        return this.cash_basis_base_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return this.chart_template_id ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return this.chart_template_idDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return this.chart_template_id_text ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return this.chart_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [子级税]
     */
    @JsonProperty("children_tax_ids")
    public String getChildren_tax_ids(){
        return this.children_tax_ids ;
    }

    /**
     * 设置 [子级税]
     */
    @JsonProperty("children_tax_ids")
    public void setChildren_tax_ids(String  children_tax_ids){
        this.children_tax_ids = children_tax_ids ;
        this.children_tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [子级税]脏标记
     */
    @JsonIgnore
    public boolean getChildren_tax_idsDirtyFlag(){
        return this.children_tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示在发票上]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [显示在发票上]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [显示在发票上]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [影响后续税收]
     */
    @JsonProperty("include_base_amount")
    public String getInclude_base_amount(){
        return this.include_base_amount ;
    }

    /**
     * 设置 [影响后续税收]
     */
    @JsonProperty("include_base_amount")
    public void setInclude_base_amount(String  include_base_amount){
        this.include_base_amount = include_base_amount ;
        this.include_base_amountDirtyFlag = true ;
    }

     /**
     * 获取 [影响后续税收]脏标记
     */
    @JsonIgnore
    public boolean getInclude_base_amountDirtyFlag(){
        return this.include_base_amountDirtyFlag ;
    }   

    /**
     * 获取 [税率名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [税率名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [税率名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [包含在价格中]
     */
    @JsonProperty("price_include")
    public String getPrice_include(){
        return this.price_include ;
    }

    /**
     * 设置 [包含在价格中]
     */
    @JsonProperty("price_include")
    public void setPrice_include(String  price_include){
        this.price_include = price_include ;
        this.price_includeDirtyFlag = true ;
    }

     /**
     * 获取 [包含在价格中]脏标记
     */
    @JsonIgnore
    public boolean getPrice_includeDirtyFlag(){
        return this.price_includeDirtyFlag ;
    }   

    /**
     * 获取 [退款的税金科目]
     */
    @JsonProperty("refund_account_id")
    public Integer getRefund_account_id(){
        return this.refund_account_id ;
    }

    /**
     * 设置 [退款的税金科目]
     */
    @JsonProperty("refund_account_id")
    public void setRefund_account_id(Integer  refund_account_id){
        this.refund_account_id = refund_account_id ;
        this.refund_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [退款的税金科目]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_idDirtyFlag(){
        return this.refund_account_idDirtyFlag ;
    }   

    /**
     * 获取 [退款的税金科目]
     */
    @JsonProperty("refund_account_id_text")
    public String getRefund_account_id_text(){
        return this.refund_account_id_text ;
    }

    /**
     * 设置 [退款的税金科目]
     */
    @JsonProperty("refund_account_id_text")
    public void setRefund_account_id_text(String  refund_account_id_text){
        this.refund_account_id_text = refund_account_id_text ;
        this.refund_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [退款的税金科目]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_id_textDirtyFlag(){
        return this.refund_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [科目标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [科目标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [科目标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [应有税金]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return this.tax_exigibility ;
    }

    /**
     * 设置 [应有税金]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

     /**
     * 获取 [应有税金]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return this.tax_exigibilityDirtyFlag ;
    }   

    /**
     * 获取 [税组]
     */
    @JsonProperty("tax_group_id")
    public Integer getTax_group_id(){
        return this.tax_group_id ;
    }

    /**
     * 设置 [税组]
     */
    @JsonProperty("tax_group_id")
    public void setTax_group_id(Integer  tax_group_id){
        this.tax_group_id = tax_group_id ;
        this.tax_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [税组]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_idDirtyFlag(){
        return this.tax_group_idDirtyFlag ;
    }   

    /**
     * 获取 [税组]
     */
    @JsonProperty("tax_group_id_text")
    public String getTax_group_id_text(){
        return this.tax_group_id_text ;
    }

    /**
     * 设置 [税组]
     */
    @JsonProperty("tax_group_id_text")
    public void setTax_group_id_text(String  tax_group_id_text){
        this.tax_group_id_text = tax_group_id_text ;
        this.tax_group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税组]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_id_textDirtyFlag(){
        return this.tax_group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税范围]
     */
    @JsonProperty("type_tax_use")
    public String getType_tax_use(){
        return this.type_tax_use ;
    }

    /**
     * 设置 [税范围]
     */
    @JsonProperty("type_tax_use")
    public void setType_tax_use(String  type_tax_use){
        this.type_tax_use = type_tax_use ;
        this.type_tax_useDirtyFlag = true ;
    }

     /**
     * 获取 [税范围]脏标记
     */
    @JsonIgnore
    public boolean getType_tax_useDirtyFlag(){
        return this.type_tax_useDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

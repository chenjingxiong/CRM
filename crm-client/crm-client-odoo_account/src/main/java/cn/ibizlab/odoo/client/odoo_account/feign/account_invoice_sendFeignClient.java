package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_send;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_sendImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_send] 服务对象接口
 */
public interface account_invoice_sendFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_sends/fetchdefault")
    public Page<account_invoice_sendImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_sends")
    public account_invoice_sendImpl create(@RequestBody account_invoice_sendImpl account_invoice_send);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_sends/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_sends/createbatch")
    public account_invoice_sendImpl createBatch(@RequestBody List<account_invoice_sendImpl> account_invoice_sends);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_sends/removebatch")
    public account_invoice_sendImpl removeBatch(@RequestBody List<account_invoice_sendImpl> account_invoice_sends);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_sends/{id}")
    public account_invoice_sendImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_sends/updatebatch")
    public account_invoice_sendImpl updateBatch(@RequestBody List<account_invoice_sendImpl> account_invoice_sends);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_sends/{id}")
    public account_invoice_sendImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_sendImpl account_invoice_send);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_sends/select")
    public Page<account_invoice_sendImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_sends/{id}/getdraft")
    public account_invoice_sendImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_invoice_sendImpl account_invoice_send);



}

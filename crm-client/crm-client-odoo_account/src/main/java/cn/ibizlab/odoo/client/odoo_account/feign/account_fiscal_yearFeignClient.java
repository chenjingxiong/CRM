package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_year;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_yearImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_year] 服务对象接口
 */
public interface account_fiscal_yearFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_years/removebatch")
    public account_fiscal_yearImpl removeBatch(@RequestBody List<account_fiscal_yearImpl> account_fiscal_years);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_years/fetchdefault")
    public Page<account_fiscal_yearImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_years/updatebatch")
    public account_fiscal_yearImpl updateBatch(@RequestBody List<account_fiscal_yearImpl> account_fiscal_years);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_years/{id}")
    public account_fiscal_yearImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_years/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_years/createbatch")
    public account_fiscal_yearImpl createBatch(@RequestBody List<account_fiscal_yearImpl> account_fiscal_years);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_years/{id}")
    public account_fiscal_yearImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_yearImpl account_fiscal_year);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_years")
    public account_fiscal_yearImpl create(@RequestBody account_fiscal_yearImpl account_fiscal_year);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_years/select")
    public Page<account_fiscal_yearImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_years/{id}/getdraft")
    public account_fiscal_yearImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_yearImpl account_fiscal_year);



}

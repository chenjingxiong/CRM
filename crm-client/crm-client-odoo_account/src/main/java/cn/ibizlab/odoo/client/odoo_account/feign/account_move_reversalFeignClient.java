package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_reversal;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_reversalImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_move_reversal] 服务对象接口
 */
public interface account_move_reversalFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_reversals/createbatch")
    public account_move_reversalImpl createBatch(@RequestBody List<account_move_reversalImpl> account_move_reversals);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_reversals/{id}")
    public account_move_reversalImpl update(@PathVariable("id") Integer id,@RequestBody account_move_reversalImpl account_move_reversal);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_reversals/removebatch")
    public account_move_reversalImpl removeBatch(@RequestBody List<account_move_reversalImpl> account_move_reversals);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_reversals")
    public account_move_reversalImpl create(@RequestBody account_move_reversalImpl account_move_reversal);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_reversals/updatebatch")
    public account_move_reversalImpl updateBatch(@RequestBody List<account_move_reversalImpl> account_move_reversals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_reversals/{id}")
    public account_move_reversalImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_reversals/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_reversals/fetchdefault")
    public Page<account_move_reversalImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_reversals/select")
    public Page<account_move_reversalImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_reversals/{id}/getdraft")
    public account_move_reversalImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_move_reversalImpl account_move_reversal);



}

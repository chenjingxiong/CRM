package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_importImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement_import] 服务对象接口
 */
public interface account_bank_statement_importFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_imports/{id}")
    public account_bank_statement_importImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statement_importImpl account_bank_statement_import);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_imports/{id}")
    public account_bank_statement_importImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_imports")
    public account_bank_statement_importImpl create(@RequestBody account_bank_statement_importImpl account_bank_statement_import);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_imports/createbatch")
    public account_bank_statement_importImpl createBatch(@RequestBody List<account_bank_statement_importImpl> account_bank_statement_imports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_imports/updatebatch")
    public account_bank_statement_importImpl updateBatch(@RequestBody List<account_bank_statement_importImpl> account_bank_statement_imports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_imports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_imports/removebatch")
    public account_bank_statement_importImpl removeBatch(@RequestBody List<account_bank_statement_importImpl> account_bank_statement_imports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_imports/fetchdefault")
    public Page<account_bank_statement_importImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_imports/select")
    public Page<account_bank_statement_importImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_imports/{id}/getdraft")
    public account_bank_statement_importImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_bank_statement_importImpl account_bank_statement_import);



}

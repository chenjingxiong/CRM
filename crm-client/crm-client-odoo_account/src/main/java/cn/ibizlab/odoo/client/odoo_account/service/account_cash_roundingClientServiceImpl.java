package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_cash_rounding;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_cash_roundingClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_cash_roundingImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_cash_roundingFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_cash_rounding] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_cash_roundingClientServiceImpl implements Iaccount_cash_roundingClientService {

    account_cash_roundingFeignClient account_cash_roundingFeignClient;

    @Autowired
    public account_cash_roundingClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_cash_roundingFeignClient = nameBuilder.target(account_cash_roundingFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_cash_roundingFeignClient = nameBuilder.target(account_cash_roundingFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_cash_rounding createModel() {
		return new account_cash_roundingImpl();
	}


    public void get(Iaccount_cash_rounding account_cash_rounding){
        Iaccount_cash_rounding clientModel = account_cash_roundingFeignClient.get(account_cash_rounding.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_cash_rounding.getClass(), false);
        copier.copy(clientModel, account_cash_rounding, null);
    }


    public void remove(Iaccount_cash_rounding account_cash_rounding){
        account_cash_roundingFeignClient.remove(account_cash_rounding.getId()) ;
    }


    public void createBatch(List<Iaccount_cash_rounding> account_cash_roundings){
        if(account_cash_roundings!=null){
            List<account_cash_roundingImpl> list = new ArrayList<account_cash_roundingImpl>();
            for(Iaccount_cash_rounding iaccount_cash_rounding :account_cash_roundings){
                list.add((account_cash_roundingImpl)iaccount_cash_rounding) ;
            }
            account_cash_roundingFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_cash_rounding> account_cash_roundings){
        if(account_cash_roundings!=null){
            List<account_cash_roundingImpl> list = new ArrayList<account_cash_roundingImpl>();
            for(Iaccount_cash_rounding iaccount_cash_rounding :account_cash_roundings){
                list.add((account_cash_roundingImpl)iaccount_cash_rounding) ;
            }
            account_cash_roundingFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_cash_rounding account_cash_rounding){
        Iaccount_cash_rounding clientModel = account_cash_roundingFeignClient.create((account_cash_roundingImpl)account_cash_rounding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_cash_rounding.getClass(), false);
        copier.copy(clientModel, account_cash_rounding, null);
    }


    public Page<Iaccount_cash_rounding> fetchDefault(SearchContext context){
        Page<account_cash_roundingImpl> page = this.account_cash_roundingFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_cash_rounding account_cash_rounding){
        Iaccount_cash_rounding clientModel = account_cash_roundingFeignClient.update(account_cash_rounding.getId(),(account_cash_roundingImpl)account_cash_rounding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_cash_rounding.getClass(), false);
        copier.copy(clientModel, account_cash_rounding, null);
    }


    public void updateBatch(List<Iaccount_cash_rounding> account_cash_roundings){
        if(account_cash_roundings!=null){
            List<account_cash_roundingImpl> list = new ArrayList<account_cash_roundingImpl>();
            for(Iaccount_cash_rounding iaccount_cash_rounding :account_cash_roundings){
                list.add((account_cash_roundingImpl)iaccount_cash_rounding) ;
            }
            account_cash_roundingFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_cash_rounding> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_cash_rounding account_cash_rounding){
        Iaccount_cash_rounding clientModel = account_cash_roundingFeignClient.getDraft(account_cash_rounding.getId(),(account_cash_roundingImpl)account_cash_rounding) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_cash_rounding.getClass(), false);
        copier.copy(clientModel, account_cash_rounding, null);
    }



}


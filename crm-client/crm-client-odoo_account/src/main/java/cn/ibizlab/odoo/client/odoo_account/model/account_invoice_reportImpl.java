package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_invoice_report] 对象
 */
public class account_invoice_reportImpl implements Iaccount_invoice_report,Serializable{

    /**
     * 分析账户
     */
    public Integer account_analytic_id;

    @JsonIgnore
    public boolean account_analytic_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String account_analytic_id_text;

    @JsonIgnore
    public boolean account_analytic_id_textDirtyFlag;
    
    /**
     * 收／付款账户
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 收／付款账户
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 收入/费用科目
     */
    public Integer account_line_id;

    @JsonIgnore
    public boolean account_line_idDirtyFlag;
    
    /**
     * 收入/费用科目
     */
    public String account_line_id_text;

    @JsonIgnore
    public boolean account_line_id_textDirtyFlag;
    
    /**
     * 总计
     */
    public Double amount_total;

    @JsonIgnore
    public boolean amount_totalDirtyFlag;
    
    /**
     * 产品类别
     */
    public Integer categ_id;

    @JsonIgnore
    public boolean categ_idDirtyFlag;
    
    /**
     * 产品类别
     */
    public String categ_id_text;

    @JsonIgnore
    public boolean categ_id_textDirtyFlag;
    
    /**
     * 业务伙伴公司
     */
    public Integer commercial_partner_id;

    @JsonIgnore
    public boolean commercial_partner_idDirtyFlag;
    
    /**
     * 业务伙伴公司
     */
    public String commercial_partner_id_text;

    @JsonIgnore
    public boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 业务伙伴的国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 业务伙伴的国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 汇率
     */
    public Double currency_rate;

    @JsonIgnore
    public boolean currency_rateDirtyFlag;
    
    /**
     * 开票日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 到期日期
     */
    public Timestamp date_due;

    @JsonIgnore
    public boolean date_dueDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer fiscal_position_id;

    @JsonIgnore
    public boolean fiscal_position_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String fiscal_position_id_text;

    @JsonIgnore
    public boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public Integer invoice_id;

    @JsonIgnore
    public boolean invoice_idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_id_text;

    @JsonIgnore
    public boolean invoice_id_textDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 行数
     */
    public Integer nbr;

    @JsonIgnore
    public boolean nbrDirtyFlag;
    
    /**
     * 发票 #
     */
    public String number;

    @JsonIgnore
    public boolean numberDirtyFlag;
    
    /**
     * 银行账户
     */
    public Integer partner_bank_id;

    @JsonIgnore
    public boolean partner_bank_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 付款条款
     */
    public Integer payment_term_id;

    @JsonIgnore
    public boolean payment_term_idDirtyFlag;
    
    /**
     * 付款条款
     */
    public String payment_term_id_text;

    @JsonIgnore
    public boolean payment_term_id_textDirtyFlag;
    
    /**
     * 平均价格
     */
    public Double price_average;

    @JsonIgnore
    public boolean price_averageDirtyFlag;
    
    /**
     * 不含税总计
     */
    public Double price_total;

    @JsonIgnore
    public boolean price_totalDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 到期金额
     */
    public Double residual;

    @JsonIgnore
    public boolean residualDirtyFlag;
    
    /**
     * 发票状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 参考计量单位
     */
    public String uom_name;

    @JsonIgnore
    public boolean uom_nameDirtyFlag;
    
    /**
     * 本币平均价格
     */
    public Double user_currency_price_average;

    @JsonIgnore
    public boolean user_currency_price_averageDirtyFlag;
    
    /**
     * 当前货币不含税总计
     */
    public Double user_currency_price_total;

    @JsonIgnore
    public boolean user_currency_price_totalDirtyFlag;
    
    /**
     * 余额总计
     */
    public Double user_currency_residual;

    @JsonIgnore
    public boolean user_currency_residualDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return this.account_analytic_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return this.account_analytic_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return this.account_analytic_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return this.account_analytic_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收／付款账户]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [收／付款账户]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [收／付款账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [收／付款账户]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [收／付款账户]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收／付款账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收入/费用科目]
     */
    @JsonProperty("account_line_id")
    public Integer getAccount_line_id(){
        return this.account_line_id ;
    }

    /**
     * 设置 [收入/费用科目]
     */
    @JsonProperty("account_line_id")
    public void setAccount_line_id(Integer  account_line_id){
        this.account_line_id = account_line_id ;
        this.account_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [收入/费用科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_line_idDirtyFlag(){
        return this.account_line_idDirtyFlag ;
    }   

    /**
     * 获取 [收入/费用科目]
     */
    @JsonProperty("account_line_id_text")
    public String getAccount_line_id_text(){
        return this.account_line_id_text ;
    }

    /**
     * 设置 [收入/费用科目]
     */
    @JsonProperty("account_line_id_text")
    public void setAccount_line_id_text(String  account_line_id_text){
        this.account_line_id_text = account_line_id_text ;
        this.account_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收入/费用科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_line_id_textDirtyFlag(){
        return this.account_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [产品类别]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [产品类别]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品类别]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }   

    /**
     * 获取 [产品类别]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return this.categ_id_text ;
    }

    /**
     * 设置 [产品类别]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品类别]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return this.categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴公司]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [业务伙伴公司]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴公司]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴公司]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [业务伙伴公司]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴公司]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴的国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [业务伙伴的国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴的国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴的国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [业务伙伴的国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴的国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [汇率]
     */
    @JsonProperty("currency_rate")
    public Double getCurrency_rate(){
        return this.currency_rate ;
    }

    /**
     * 设置 [汇率]
     */
    @JsonProperty("currency_rate")
    public void setCurrency_rate(Double  currency_rate){
        this.currency_rate = currency_rate ;
        this.currency_rateDirtyFlag = true ;
    }

     /**
     * 获取 [汇率]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_rateDirtyFlag(){
        return this.currency_rateDirtyFlag ;
    }   

    /**
     * 获取 [开票日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [开票日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [开票日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [到期日期]
     */
    @JsonProperty("date_due")
    public Timestamp getDate_due(){
        return this.date_due ;
    }

    /**
     * 设置 [到期日期]
     */
    @JsonProperty("date_due")
    public void setDate_due(Timestamp  date_due){
        this.date_due = date_due ;
        this.date_dueDirtyFlag = true ;
    }

     /**
     * 获取 [到期日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_dueDirtyFlag(){
        return this.date_dueDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return this.invoice_id ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return this.invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return this.invoice_id_text ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return this.invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [行数]
     */
    @JsonProperty("nbr")
    public Integer getNbr(){
        return this.nbr ;
    }

    /**
     * 设置 [行数]
     */
    @JsonProperty("nbr")
    public void setNbr(Integer  nbr){
        this.nbr = nbr ;
        this.nbrDirtyFlag = true ;
    }

     /**
     * 获取 [行数]脏标记
     */
    @JsonIgnore
    public boolean getNbrDirtyFlag(){
        return this.nbrDirtyFlag ;
    }   

    /**
     * 获取 [发票 #]
     */
    @JsonProperty("number")
    public String getNumber(){
        return this.number ;
    }

    /**
     * 设置 [发票 #]
     */
    @JsonProperty("number")
    public void setNumber(String  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

     /**
     * 获取 [发票 #]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return this.numberDirtyFlag ;
    }   

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public Integer getPartner_bank_id(){
        return this.partner_bank_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public void setPartner_bank_id(Integer  partner_bank_id){
        this.partner_bank_id = partner_bank_id ;
        this.partner_bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_idDirtyFlag(){
        return this.partner_bank_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return this.payment_term_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return this.payment_term_idDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return this.payment_term_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return this.payment_term_id_textDirtyFlag ;
    }   

    /**
     * 获取 [平均价格]
     */
    @JsonProperty("price_average")
    public Double getPrice_average(){
        return this.price_average ;
    }

    /**
     * 设置 [平均价格]
     */
    @JsonProperty("price_average")
    public void setPrice_average(Double  price_average){
        this.price_average = price_average ;
        this.price_averageDirtyFlag = true ;
    }

     /**
     * 获取 [平均价格]脏标记
     */
    @JsonIgnore
    public boolean getPrice_averageDirtyFlag(){
        return this.price_averageDirtyFlag ;
    }   

    /**
     * 获取 [不含税总计]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [不含税总计]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [不含税总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [到期金额]
     */
    @JsonProperty("residual")
    public Double getResidual(){
        return this.residual ;
    }

    /**
     * 设置 [到期金额]
     */
    @JsonProperty("residual")
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.residualDirtyFlag = true ;
    }

     /**
     * 获取 [到期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidualDirtyFlag(){
        return this.residualDirtyFlag ;
    }   

    /**
     * 获取 [发票状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [发票状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [发票状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [参考计量单位]
     */
    @JsonProperty("uom_name")
    public String getUom_name(){
        return this.uom_name ;
    }

    /**
     * 设置 [参考计量单位]
     */
    @JsonProperty("uom_name")
    public void setUom_name(String  uom_name){
        this.uom_name = uom_name ;
        this.uom_nameDirtyFlag = true ;
    }

     /**
     * 获取 [参考计量单位]脏标记
     */
    @JsonIgnore
    public boolean getUom_nameDirtyFlag(){
        return this.uom_nameDirtyFlag ;
    }   

    /**
     * 获取 [本币平均价格]
     */
    @JsonProperty("user_currency_price_average")
    public Double getUser_currency_price_average(){
        return this.user_currency_price_average ;
    }

    /**
     * 设置 [本币平均价格]
     */
    @JsonProperty("user_currency_price_average")
    public void setUser_currency_price_average(Double  user_currency_price_average){
        this.user_currency_price_average = user_currency_price_average ;
        this.user_currency_price_averageDirtyFlag = true ;
    }

     /**
     * 获取 [本币平均价格]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_price_averageDirtyFlag(){
        return this.user_currency_price_averageDirtyFlag ;
    }   

    /**
     * 获取 [当前货币不含税总计]
     */
    @JsonProperty("user_currency_price_total")
    public Double getUser_currency_price_total(){
        return this.user_currency_price_total ;
    }

    /**
     * 设置 [当前货币不含税总计]
     */
    @JsonProperty("user_currency_price_total")
    public void setUser_currency_price_total(Double  user_currency_price_total){
        this.user_currency_price_total = user_currency_price_total ;
        this.user_currency_price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [当前货币不含税总计]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_price_totalDirtyFlag(){
        return this.user_currency_price_totalDirtyFlag ;
    }   

    /**
     * 获取 [余额总计]
     */
    @JsonProperty("user_currency_residual")
    public Double getUser_currency_residual(){
        return this.user_currency_residual ;
    }

    /**
     * 设置 [余额总计]
     */
    @JsonProperty("user_currency_residual")
    public void setUser_currency_residual(Double  user_currency_residual){
        this.user_currency_residual = user_currency_residual ;
        this.user_currency_residualDirtyFlag = true ;
    }

     /**
     * 获取 [余额总计]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_residualDirtyFlag(){
        return this.user_currency_residualDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

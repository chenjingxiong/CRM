package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_taxImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
public interface account_fiscal_position_taxFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_taxes/removebatch")
    public account_fiscal_position_taxImpl removeBatch(@RequestBody List<account_fiscal_position_taxImpl> account_fiscal_position_taxes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_taxes/updatebatch")
    public account_fiscal_position_taxImpl updateBatch(@RequestBody List<account_fiscal_position_taxImpl> account_fiscal_position_taxes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_taxes/{id}")
    public account_fiscal_position_taxImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_taxes/{id}")
    public account_fiscal_position_taxImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_taxImpl account_fiscal_position_tax);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_taxes/createbatch")
    public account_fiscal_position_taxImpl createBatch(@RequestBody List<account_fiscal_position_taxImpl> account_fiscal_position_taxes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_taxes")
    public account_fiscal_position_taxImpl create(@RequestBody account_fiscal_position_taxImpl account_fiscal_position_tax);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_taxes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_taxes/fetchdefault")
    public Page<account_fiscal_position_taxImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_taxes/select")
    public Page<account_fiscal_position_taxImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_taxes/{id}/getdraft")
    public account_fiscal_position_taxImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_taxImpl account_fiscal_position_tax);



}

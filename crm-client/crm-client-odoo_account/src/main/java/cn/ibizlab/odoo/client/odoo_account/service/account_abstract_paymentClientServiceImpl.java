package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_abstract_payment;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_abstract_paymentClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_abstract_paymentImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_abstract_paymentFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_abstract_payment] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_abstract_paymentClientServiceImpl implements Iaccount_abstract_paymentClientService {

    account_abstract_paymentFeignClient account_abstract_paymentFeignClient;

    @Autowired
    public account_abstract_paymentClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_abstract_paymentFeignClient = nameBuilder.target(account_abstract_paymentFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_abstract_paymentFeignClient = nameBuilder.target(account_abstract_paymentFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_abstract_payment createModel() {
		return new account_abstract_paymentImpl();
	}


    public Page<Iaccount_abstract_payment> fetchDefault(SearchContext context){
        Page<account_abstract_paymentImpl> page = this.account_abstract_paymentFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_abstract_payment account_abstract_payment){
        Iaccount_abstract_payment clientModel = account_abstract_paymentFeignClient.create((account_abstract_paymentImpl)account_abstract_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_abstract_payment.getClass(), false);
        copier.copy(clientModel, account_abstract_payment, null);
    }


    public void removeBatch(List<Iaccount_abstract_payment> account_abstract_payments){
        if(account_abstract_payments!=null){
            List<account_abstract_paymentImpl> list = new ArrayList<account_abstract_paymentImpl>();
            for(Iaccount_abstract_payment iaccount_abstract_payment :account_abstract_payments){
                list.add((account_abstract_paymentImpl)iaccount_abstract_payment) ;
            }
            account_abstract_paymentFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iaccount_abstract_payment account_abstract_payment){
        Iaccount_abstract_payment clientModel = account_abstract_paymentFeignClient.get(account_abstract_payment.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_abstract_payment.getClass(), false);
        copier.copy(clientModel, account_abstract_payment, null);
    }


    public void remove(Iaccount_abstract_payment account_abstract_payment){
        account_abstract_paymentFeignClient.remove(account_abstract_payment.getId()) ;
    }


    public void updateBatch(List<Iaccount_abstract_payment> account_abstract_payments){
        if(account_abstract_payments!=null){
            List<account_abstract_paymentImpl> list = new ArrayList<account_abstract_paymentImpl>();
            for(Iaccount_abstract_payment iaccount_abstract_payment :account_abstract_payments){
                list.add((account_abstract_paymentImpl)iaccount_abstract_payment) ;
            }
            account_abstract_paymentFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_abstract_payment account_abstract_payment){
        Iaccount_abstract_payment clientModel = account_abstract_paymentFeignClient.update(account_abstract_payment.getId(),(account_abstract_paymentImpl)account_abstract_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_abstract_payment.getClass(), false);
        copier.copy(clientModel, account_abstract_payment, null);
    }


    public void createBatch(List<Iaccount_abstract_payment> account_abstract_payments){
        if(account_abstract_payments!=null){
            List<account_abstract_paymentImpl> list = new ArrayList<account_abstract_paymentImpl>();
            for(Iaccount_abstract_payment iaccount_abstract_payment :account_abstract_payments){
                list.add((account_abstract_paymentImpl)iaccount_abstract_payment) ;
            }
            account_abstract_paymentFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_abstract_payment> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_abstract_payment account_abstract_payment){
        Iaccount_abstract_payment clientModel = account_abstract_paymentFeignClient.getDraft(account_abstract_payment.getId(),(account_abstract_paymentImpl)account_abstract_payment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_abstract_payment.getClass(), false);
        copier.copy(clientModel, account_abstract_payment, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_register_payments;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_register_paymentsClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_register_paymentsImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_register_paymentsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_register_payments] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_register_paymentsClientServiceImpl implements Iaccount_register_paymentsClientService {

    account_register_paymentsFeignClient account_register_paymentsFeignClient;

    @Autowired
    public account_register_paymentsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_register_paymentsFeignClient = nameBuilder.target(account_register_paymentsFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_register_paymentsFeignClient = nameBuilder.target(account_register_paymentsFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_register_payments createModel() {
		return new account_register_paymentsImpl();
	}


    public void create(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.create((account_register_paymentsImpl)account_register_payments) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }


    public void createBatch(List<Iaccount_register_payments> account_register_payments){
        if(account_register_payments!=null){
            List<account_register_paymentsImpl> list = new ArrayList<account_register_paymentsImpl>();
            for(Iaccount_register_payments iaccount_register_payments :account_register_payments){
                list.add((account_register_paymentsImpl)iaccount_register_payments) ;
            }
            account_register_paymentsFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_register_payments> account_register_payments){
        if(account_register_payments!=null){
            List<account_register_paymentsImpl> list = new ArrayList<account_register_paymentsImpl>();
            for(Iaccount_register_payments iaccount_register_payments :account_register_payments){
                list.add((account_register_paymentsImpl)iaccount_register_payments) ;
            }
            account_register_paymentsFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_register_payments> fetchDefault(SearchContext context){
        Page<account_register_paymentsImpl> page = this.account_register_paymentsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_register_payments> account_register_payments){
        if(account_register_payments!=null){
            List<account_register_paymentsImpl> list = new ArrayList<account_register_paymentsImpl>();
            for(Iaccount_register_payments iaccount_register_payments :account_register_payments){
                list.add((account_register_paymentsImpl)iaccount_register_payments) ;
            }
            account_register_paymentsFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_register_payments account_register_payments){
        account_register_paymentsFeignClient.remove(account_register_payments.getId()) ;
    }


    public void update(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.update(account_register_payments.getId(),(account_register_paymentsImpl)account_register_payments) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }


    public void get(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.get(account_register_payments.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }


    public Page<Iaccount_register_payments> select(SearchContext context){
        return null ;
    }


    public void checkKey(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.checkKey(account_register_payments.getId(),(account_register_paymentsImpl)account_register_payments) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }


    public void save(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.save(account_register_payments.getId(),(account_register_paymentsImpl)account_register_payments) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }


    public void getDraft(Iaccount_register_payments account_register_payments){
        Iaccount_register_payments clientModel = account_register_paymentsFeignClient.getDraft(account_register_payments.getId(),(account_register_paymentsImpl)account_register_payments) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_register_payments.getClass(), false);
        copier.copy(clientModel, account_register_payments, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_setup_bank_manual_config;
import cn.ibizlab.odoo.client.odoo_account.model.account_setup_bank_manual_configImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
public interface account_setup_bank_manual_configFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_setup_bank_manual_configs/{id}")
    public account_setup_bank_manual_configImpl update(@PathVariable("id") Integer id,@RequestBody account_setup_bank_manual_configImpl account_setup_bank_manual_config);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_setup_bank_manual_configs")
    public account_setup_bank_manual_configImpl create(@RequestBody account_setup_bank_manual_configImpl account_setup_bank_manual_config);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_setup_bank_manual_configs/{id}")
    public account_setup_bank_manual_configImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_setup_bank_manual_configs/updatebatch")
    public account_setup_bank_manual_configImpl updateBatch(@RequestBody List<account_setup_bank_manual_configImpl> account_setup_bank_manual_configs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_setup_bank_manual_configs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_setup_bank_manual_configs/removebatch")
    public account_setup_bank_manual_configImpl removeBatch(@RequestBody List<account_setup_bank_manual_configImpl> account_setup_bank_manual_configs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_setup_bank_manual_configs/createbatch")
    public account_setup_bank_manual_configImpl createBatch(@RequestBody List<account_setup_bank_manual_configImpl> account_setup_bank_manual_configs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_setup_bank_manual_configs/fetchdefault")
    public Page<account_setup_bank_manual_configImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_setup_bank_manual_configs/select")
    public Page<account_setup_bank_manual_configImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_setup_bank_manual_configs/{id}/getdraft")
    public account_setup_bank_manual_configImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_setup_bank_manual_configImpl account_setup_bank_manual_config);



}

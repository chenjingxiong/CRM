package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_method;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_methodClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_methodImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_payment_methodFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_payment_method] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_payment_methodClientServiceImpl implements Iaccount_payment_methodClientService {

    account_payment_methodFeignClient account_payment_methodFeignClient;

    @Autowired
    public account_payment_methodClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_methodFeignClient = nameBuilder.target(account_payment_methodFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_methodFeignClient = nameBuilder.target(account_payment_methodFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_payment_method createModel() {
		return new account_payment_methodImpl();
	}


    public void get(Iaccount_payment_method account_payment_method){
        Iaccount_payment_method clientModel = account_payment_methodFeignClient.get(account_payment_method.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_method.getClass(), false);
        copier.copy(clientModel, account_payment_method, null);
    }


    public void updateBatch(List<Iaccount_payment_method> account_payment_methods){
        if(account_payment_methods!=null){
            List<account_payment_methodImpl> list = new ArrayList<account_payment_methodImpl>();
            for(Iaccount_payment_method iaccount_payment_method :account_payment_methods){
                list.add((account_payment_methodImpl)iaccount_payment_method) ;
            }
            account_payment_methodFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_payment_method account_payment_method){
        account_payment_methodFeignClient.remove(account_payment_method.getId()) ;
    }


    public void create(Iaccount_payment_method account_payment_method){
        Iaccount_payment_method clientModel = account_payment_methodFeignClient.create((account_payment_methodImpl)account_payment_method) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_method.getClass(), false);
        copier.copy(clientModel, account_payment_method, null);
    }


    public void removeBatch(List<Iaccount_payment_method> account_payment_methods){
        if(account_payment_methods!=null){
            List<account_payment_methodImpl> list = new ArrayList<account_payment_methodImpl>();
            for(Iaccount_payment_method iaccount_payment_method :account_payment_methods){
                list.add((account_payment_methodImpl)iaccount_payment_method) ;
            }
            account_payment_methodFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_payment_method> fetchDefault(SearchContext context){
        Page<account_payment_methodImpl> page = this.account_payment_methodFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_payment_method account_payment_method){
        Iaccount_payment_method clientModel = account_payment_methodFeignClient.update(account_payment_method.getId(),(account_payment_methodImpl)account_payment_method) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_method.getClass(), false);
        copier.copy(clientModel, account_payment_method, null);
    }


    public void createBatch(List<Iaccount_payment_method> account_payment_methods){
        if(account_payment_methods!=null){
            List<account_payment_methodImpl> list = new ArrayList<account_payment_methodImpl>();
            for(Iaccount_payment_method iaccount_payment_method :account_payment_methods){
                list.add((account_payment_methodImpl)iaccount_payment_method) ;
            }
            account_payment_methodFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_payment_method> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_payment_method account_payment_method){
        Iaccount_payment_method clientModel = account_payment_methodFeignClient.getDraft(account_payment_method.getId(),(account_payment_methodImpl)account_payment_method) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_method.getClass(), false);
        copier.copy(clientModel, account_payment_method, null);
    }



}


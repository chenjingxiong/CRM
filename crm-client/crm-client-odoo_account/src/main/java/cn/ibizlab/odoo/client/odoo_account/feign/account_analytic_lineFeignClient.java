package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_analytic_line] 服务对象接口
 */
public interface account_analytic_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_lines/{id}")
    public account_analytic_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_lines")
    public account_analytic_lineImpl create(@RequestBody account_analytic_lineImpl account_analytic_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_lines/removebatch")
    public account_analytic_lineImpl removeBatch(@RequestBody List<account_analytic_lineImpl> account_analytic_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_lines/createbatch")
    public account_analytic_lineImpl createBatch(@RequestBody List<account_analytic_lineImpl> account_analytic_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_lines/updatebatch")
    public account_analytic_lineImpl updateBatch(@RequestBody List<account_analytic_lineImpl> account_analytic_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_lines/{id}")
    public account_analytic_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_analytic_lineImpl account_analytic_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_lines/fetchdefault")
    public Page<account_analytic_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_lines/select")
    public Page<account_analytic_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_lines/{id}/getdraft")
    public account_analytic_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_analytic_lineImpl account_analytic_line);



}

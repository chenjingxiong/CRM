package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_accountFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_account] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_accountClientServiceImpl implements Iaccount_accountClientService {

    account_accountFeignClient account_accountFeignClient;

    @Autowired
    public account_accountClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_accountFeignClient = nameBuilder.target(account_accountFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_accountFeignClient = nameBuilder.target(account_accountFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_account createModel() {
		return new account_accountImpl();
	}


    public void remove(Iaccount_account account_account){
        account_accountFeignClient.remove(account_account.getId()) ;
    }


    public void createBatch(List<Iaccount_account> account_accounts){
        if(account_accounts!=null){
            List<account_accountImpl> list = new ArrayList<account_accountImpl>();
            for(Iaccount_account iaccount_account :account_accounts){
                list.add((account_accountImpl)iaccount_account) ;
            }
            account_accountFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_account account_account){
        Iaccount_account clientModel = account_accountFeignClient.get(account_account.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account.getClass(), false);
        copier.copy(clientModel, account_account, null);
    }


    public Page<Iaccount_account> fetchDefault(SearchContext context){
        Page<account_accountImpl> page = this.account_accountFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_account account_account){
        Iaccount_account clientModel = account_accountFeignClient.update(account_account.getId(),(account_accountImpl)account_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account.getClass(), false);
        copier.copy(clientModel, account_account, null);
    }


    public void updateBatch(List<Iaccount_account> account_accounts){
        if(account_accounts!=null){
            List<account_accountImpl> list = new ArrayList<account_accountImpl>();
            for(Iaccount_account iaccount_account :account_accounts){
                list.add((account_accountImpl)iaccount_account) ;
            }
            account_accountFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_account account_account){
        Iaccount_account clientModel = account_accountFeignClient.create((account_accountImpl)account_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account.getClass(), false);
        copier.copy(clientModel, account_account, null);
    }


    public void removeBatch(List<Iaccount_account> account_accounts){
        if(account_accounts!=null){
            List<account_accountImpl> list = new ArrayList<account_accountImpl>();
            for(Iaccount_account iaccount_account :account_accounts){
                list.add((account_accountImpl)iaccount_account) ;
            }
            account_accountFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_account> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_account account_account){
        Iaccount_account clientModel = account_accountFeignClient.getDraft(account_account.getId(),(account_accountImpl)account_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account.getClass(), false);
        copier.copy(clientModel, account_account, null);
    }



}


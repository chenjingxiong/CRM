package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model_template;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconcile_model_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_model_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_reconcile_model_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_reconcile_model_templateClientServiceImpl implements Iaccount_reconcile_model_templateClientService {

    account_reconcile_model_templateFeignClient account_reconcile_model_templateFeignClient;

    @Autowired
    public account_reconcile_model_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconcile_model_templateFeignClient = nameBuilder.target(account_reconcile_model_templateFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconcile_model_templateFeignClient = nameBuilder.target(account_reconcile_model_templateFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_reconcile_model_template createModel() {
		return new account_reconcile_model_templateImpl();
	}


    public void remove(Iaccount_reconcile_model_template account_reconcile_model_template){
        account_reconcile_model_templateFeignClient.remove(account_reconcile_model_template.getId()) ;
    }


    public Page<Iaccount_reconcile_model_template> fetchDefault(SearchContext context){
        Page<account_reconcile_model_templateImpl> page = this.account_reconcile_model_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_reconcile_model_template account_reconcile_model_template){
        Iaccount_reconcile_model_template clientModel = account_reconcile_model_templateFeignClient.create((account_reconcile_model_templateImpl)account_reconcile_model_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model_template.getClass(), false);
        copier.copy(clientModel, account_reconcile_model_template, null);
    }


    public void update(Iaccount_reconcile_model_template account_reconcile_model_template){
        Iaccount_reconcile_model_template clientModel = account_reconcile_model_templateFeignClient.update(account_reconcile_model_template.getId(),(account_reconcile_model_templateImpl)account_reconcile_model_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model_template.getClass(), false);
        copier.copy(clientModel, account_reconcile_model_template, null);
    }


    public void updateBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
        if(account_reconcile_model_templates!=null){
            List<account_reconcile_model_templateImpl> list = new ArrayList<account_reconcile_model_templateImpl>();
            for(Iaccount_reconcile_model_template iaccount_reconcile_model_template :account_reconcile_model_templates){
                list.add((account_reconcile_model_templateImpl)iaccount_reconcile_model_template) ;
            }
            account_reconcile_model_templateFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_reconcile_model_template account_reconcile_model_template){
        Iaccount_reconcile_model_template clientModel = account_reconcile_model_templateFeignClient.get(account_reconcile_model_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model_template.getClass(), false);
        copier.copy(clientModel, account_reconcile_model_template, null);
    }


    public void removeBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
        if(account_reconcile_model_templates!=null){
            List<account_reconcile_model_templateImpl> list = new ArrayList<account_reconcile_model_templateImpl>();
            for(Iaccount_reconcile_model_template iaccount_reconcile_model_template :account_reconcile_model_templates){
                list.add((account_reconcile_model_templateImpl)iaccount_reconcile_model_template) ;
            }
            account_reconcile_model_templateFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates){
        if(account_reconcile_model_templates!=null){
            List<account_reconcile_model_templateImpl> list = new ArrayList<account_reconcile_model_templateImpl>();
            for(Iaccount_reconcile_model_template iaccount_reconcile_model_template :account_reconcile_model_templates){
                list.add((account_reconcile_model_templateImpl)iaccount_reconcile_model_template) ;
            }
            account_reconcile_model_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_reconcile_model_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_reconcile_model_template account_reconcile_model_template){
        Iaccount_reconcile_model_template clientModel = account_reconcile_model_templateFeignClient.getDraft(account_reconcile_model_template.getId(),(account_reconcile_model_templateImpl)account_reconcile_model_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model_template.getClass(), false);
        copier.copy(clientModel, account_reconcile_model_template, null);
    }



}


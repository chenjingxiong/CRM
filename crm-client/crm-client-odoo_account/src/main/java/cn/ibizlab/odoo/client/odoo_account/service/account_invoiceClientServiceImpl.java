package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoiceClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoiceImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoiceClientServiceImpl implements Iaccount_invoiceClientService {

    account_invoiceFeignClient account_invoiceFeignClient;

    @Autowired
    public account_invoiceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoiceFeignClient = nameBuilder.target(account_invoiceFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoiceFeignClient = nameBuilder.target(account_invoiceFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice createModel() {
		return new account_invoiceImpl();
	}


    public void update(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.update(account_invoice.getId(),(account_invoiceImpl)account_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }


    public Page<Iaccount_invoice> fetchDefault(SearchContext context){
        Page<account_invoiceImpl> page = this.account_invoiceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iaccount_invoice> account_invoices){
        if(account_invoices!=null){
            List<account_invoiceImpl> list = new ArrayList<account_invoiceImpl>();
            for(Iaccount_invoice iaccount_invoice :account_invoices){
                list.add((account_invoiceImpl)iaccount_invoice) ;
            }
            account_invoiceFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_invoice> account_invoices){
        if(account_invoices!=null){
            List<account_invoiceImpl> list = new ArrayList<account_invoiceImpl>();
            for(Iaccount_invoice iaccount_invoice :account_invoices){
                list.add((account_invoiceImpl)iaccount_invoice) ;
            }
            account_invoiceFeignClient.createBatch(list) ;
        }
    }


    public void create(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.create((account_invoiceImpl)account_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }


    public void updateBatch(List<Iaccount_invoice> account_invoices){
        if(account_invoices!=null){
            List<account_invoiceImpl> list = new ArrayList<account_invoiceImpl>();
            for(Iaccount_invoice iaccount_invoice :account_invoices){
                list.add((account_invoiceImpl)iaccount_invoice) ;
            }
            account_invoiceFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_invoice account_invoice){
        account_invoiceFeignClient.remove(account_invoice.getId()) ;
    }


    public void get(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.get(account_invoice.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }


    public Page<Iaccount_invoice> select(SearchContext context){
        return null ;
    }


    public void checkKey(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.checkKey(account_invoice.getId(),(account_invoiceImpl)account_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }


    public void getDraft(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.getDraft(account_invoice.getId(),(account_invoiceImpl)account_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }


    public void save(Iaccount_invoice account_invoice){
        Iaccount_invoice clientModel = account_invoiceFeignClient.save(account_invoice.getId(),(account_invoiceImpl)account_invoice) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice.getClass(), false);
        copier.copy(clientModel, account_invoice, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statementImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement] 服务对象接口
 */
public interface account_bank_statementFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statements/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statements/fetchdefault")
    public Page<account_bank_statementImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statements/createbatch")
    public account_bank_statementImpl createBatch(@RequestBody List<account_bank_statementImpl> account_bank_statements);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statements/{id}")
    public account_bank_statementImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statements/removebatch")
    public account_bank_statementImpl removeBatch(@RequestBody List<account_bank_statementImpl> account_bank_statements);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statements")
    public account_bank_statementImpl create(@RequestBody account_bank_statementImpl account_bank_statement);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statements/{id}")
    public account_bank_statementImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statementImpl account_bank_statement);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statements/updatebatch")
    public account_bank_statementImpl updateBatch(@RequestBody List<account_bank_statementImpl> account_bank_statements);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statements/select")
    public Page<account_bank_statementImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statements/{id}/getdraft")
    public account_bank_statementImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_bank_statementImpl account_bank_statement);



}

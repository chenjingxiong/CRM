package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_group;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_groupImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_analytic_group] 服务对象接口
 */
public interface account_analytic_groupFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_groups/{id}")
    public account_analytic_groupImpl update(@PathVariable("id") Integer id,@RequestBody account_analytic_groupImpl account_analytic_group);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_groups/fetchdefault")
    public Page<account_analytic_groupImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_groups/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_groups/createbatch")
    public account_analytic_groupImpl createBatch(@RequestBody List<account_analytic_groupImpl> account_analytic_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_groups")
    public account_analytic_groupImpl create(@RequestBody account_analytic_groupImpl account_analytic_group);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_groups/{id}")
    public account_analytic_groupImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_groups/updatebatch")
    public account_analytic_groupImpl updateBatch(@RequestBody List<account_analytic_groupImpl> account_analytic_groups);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_groups/removebatch")
    public account_analytic_groupImpl removeBatch(@RequestBody List<account_analytic_groupImpl> account_analytic_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_groups/select")
    public Page<account_analytic_groupImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_groups/{id}/getdraft")
    public account_analytic_groupImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_analytic_groupImpl account_analytic_group);



}

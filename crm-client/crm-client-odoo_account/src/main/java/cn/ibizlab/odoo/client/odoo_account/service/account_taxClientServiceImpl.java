package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_taxFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_tax] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_taxClientServiceImpl implements Iaccount_taxClientService {

    account_taxFeignClient account_taxFeignClient;

    @Autowired
    public account_taxClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_taxFeignClient = nameBuilder.target(account_taxFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_taxFeignClient = nameBuilder.target(account_taxFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_tax createModel() {
		return new account_taxImpl();
	}


    public void get(Iaccount_tax account_tax){
        Iaccount_tax clientModel = account_taxFeignClient.get(account_tax.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax.getClass(), false);
        copier.copy(clientModel, account_tax, null);
    }


    public void create(Iaccount_tax account_tax){
        Iaccount_tax clientModel = account_taxFeignClient.create((account_taxImpl)account_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax.getClass(), false);
        copier.copy(clientModel, account_tax, null);
    }


    public void createBatch(List<Iaccount_tax> account_taxes){
        if(account_taxes!=null){
            List<account_taxImpl> list = new ArrayList<account_taxImpl>();
            for(Iaccount_tax iaccount_tax :account_taxes){
                list.add((account_taxImpl)iaccount_tax) ;
            }
            account_taxFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_tax account_tax){
        Iaccount_tax clientModel = account_taxFeignClient.update(account_tax.getId(),(account_taxImpl)account_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax.getClass(), false);
        copier.copy(clientModel, account_tax, null);
    }


    public void updateBatch(List<Iaccount_tax> account_taxes){
        if(account_taxes!=null){
            List<account_taxImpl> list = new ArrayList<account_taxImpl>();
            for(Iaccount_tax iaccount_tax :account_taxes){
                list.add((account_taxImpl)iaccount_tax) ;
            }
            account_taxFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_tax> fetchDefault(SearchContext context){
        Page<account_taxImpl> page = this.account_taxFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iaccount_tax> account_taxes){
        if(account_taxes!=null){
            List<account_taxImpl> list = new ArrayList<account_taxImpl>();
            for(Iaccount_tax iaccount_tax :account_taxes){
                list.add((account_taxImpl)iaccount_tax) ;
            }
            account_taxFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iaccount_tax account_tax){
        account_taxFeignClient.remove(account_tax.getId()) ;
    }


    public Page<Iaccount_tax> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_tax account_tax){
        Iaccount_tax clientModel = account_taxFeignClient.getDraft(account_tax.getId(),(account_taxImpl)account_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax.getClass(), false);
        copier.copy(clientModel, account_tax, null);
    }



}


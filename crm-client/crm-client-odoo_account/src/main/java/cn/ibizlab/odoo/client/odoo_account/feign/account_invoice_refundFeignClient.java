package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_refund;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_refundImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_refund] 服务对象接口
 */
public interface account_invoice_refundFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_refunds/updatebatch")
    public account_invoice_refundImpl updateBatch(@RequestBody List<account_invoice_refundImpl> account_invoice_refunds);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_refunds/{id}")
    public account_invoice_refundImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_refunds/removebatch")
    public account_invoice_refundImpl removeBatch(@RequestBody List<account_invoice_refundImpl> account_invoice_refunds);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_refunds/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_refunds/fetchdefault")
    public Page<account_invoice_refundImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_refunds/{id}")
    public account_invoice_refundImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_refundImpl account_invoice_refund);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_refunds")
    public account_invoice_refundImpl create(@RequestBody account_invoice_refundImpl account_invoice_refund);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_refunds/createbatch")
    public account_invoice_refundImpl createBatch(@RequestBody List<account_invoice_refundImpl> account_invoice_refunds);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_refunds/select")
    public Page<account_invoice_refundImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_refunds/{id}/getdraft")
    public account_invoice_refundImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_invoice_refundImpl account_invoice_refund);



}

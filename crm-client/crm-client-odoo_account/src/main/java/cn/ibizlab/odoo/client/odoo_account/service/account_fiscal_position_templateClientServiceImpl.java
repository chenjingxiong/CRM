package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_template;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_fiscal_position_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_fiscal_position_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_fiscal_position_templateClientServiceImpl implements Iaccount_fiscal_position_templateClientService {

    account_fiscal_position_templateFeignClient account_fiscal_position_templateFeignClient;

    @Autowired
    public account_fiscal_position_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_templateFeignClient = nameBuilder.target(account_fiscal_position_templateFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_templateFeignClient = nameBuilder.target(account_fiscal_position_templateFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_fiscal_position_template createModel() {
		return new account_fiscal_position_templateImpl();
	}


    public void remove(Iaccount_fiscal_position_template account_fiscal_position_template){
        account_fiscal_position_templateFeignClient.remove(account_fiscal_position_template.getId()) ;
    }


    public void createBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
        if(account_fiscal_position_templates!=null){
            List<account_fiscal_position_templateImpl> list = new ArrayList<account_fiscal_position_templateImpl>();
            for(Iaccount_fiscal_position_template iaccount_fiscal_position_template :account_fiscal_position_templates){
                list.add((account_fiscal_position_templateImpl)iaccount_fiscal_position_template) ;
            }
            account_fiscal_position_templateFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_fiscal_position_template account_fiscal_position_template){
        Iaccount_fiscal_position_template clientModel = account_fiscal_position_templateFeignClient.update(account_fiscal_position_template.getId(),(account_fiscal_position_templateImpl)account_fiscal_position_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_template.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_template, null);
    }


    public void removeBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
        if(account_fiscal_position_templates!=null){
            List<account_fiscal_position_templateImpl> list = new ArrayList<account_fiscal_position_templateImpl>();
            for(Iaccount_fiscal_position_template iaccount_fiscal_position_template :account_fiscal_position_templates){
                list.add((account_fiscal_position_templateImpl)iaccount_fiscal_position_template) ;
            }
            account_fiscal_position_templateFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iaccount_fiscal_position_template account_fiscal_position_template){
        Iaccount_fiscal_position_template clientModel = account_fiscal_position_templateFeignClient.get(account_fiscal_position_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_template.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_template, null);
    }


    public Page<Iaccount_fiscal_position_template> fetchDefault(SearchContext context){
        Page<account_fiscal_position_templateImpl> page = this.account_fiscal_position_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_fiscal_position_template account_fiscal_position_template){
        Iaccount_fiscal_position_template clientModel = account_fiscal_position_templateFeignClient.create((account_fiscal_position_templateImpl)account_fiscal_position_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_template.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_template, null);
    }


    public void updateBatch(List<Iaccount_fiscal_position_template> account_fiscal_position_templates){
        if(account_fiscal_position_templates!=null){
            List<account_fiscal_position_templateImpl> list = new ArrayList<account_fiscal_position_templateImpl>();
            for(Iaccount_fiscal_position_template iaccount_fiscal_position_template :account_fiscal_position_templates){
                list.add((account_fiscal_position_templateImpl)iaccount_fiscal_position_template) ;
            }
            account_fiscal_position_templateFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_fiscal_position_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_fiscal_position_template account_fiscal_position_template){
        Iaccount_fiscal_position_template clientModel = account_fiscal_position_templateFeignClient.getDraft(account_fiscal_position_template.getId(),(account_fiscal_position_templateImpl)account_fiscal_position_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_template.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_template, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
public interface account_bank_statement_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_lines/updatebatch")
    public account_bank_statement_lineImpl updateBatch(@RequestBody List<account_bank_statement_lineImpl> account_bank_statement_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_lines/fetchdefault")
    public Page<account_bank_statement_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_lines/createbatch")
    public account_bank_statement_lineImpl createBatch(@RequestBody List<account_bank_statement_lineImpl> account_bank_statement_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_lines/{id}")
    public account_bank_statement_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_lines/removebatch")
    public account_bank_statement_lineImpl removeBatch(@RequestBody List<account_bank_statement_lineImpl> account_bank_statement_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_lines/{id}")
    public account_bank_statement_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statement_lineImpl account_bank_statement_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_lines")
    public account_bank_statement_lineImpl create(@RequestBody account_bank_statement_lineImpl account_bank_statement_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_lines/select")
    public Page<account_bank_statement_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_lines/{id}/getdraft")
    public account_bank_statement_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_bank_statement_lineImpl account_bank_statement_line);



}

package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_group;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_analytic_groupFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_analytic_group] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_analytic_groupClientServiceImpl implements Iaccount_analytic_groupClientService {

    account_analytic_groupFeignClient account_analytic_groupFeignClient;

    @Autowired
    public account_analytic_groupClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_groupFeignClient = nameBuilder.target(account_analytic_groupFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_groupFeignClient = nameBuilder.target(account_analytic_groupFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_analytic_group createModel() {
		return new account_analytic_groupImpl();
	}


    public void update(Iaccount_analytic_group account_analytic_group){
        Iaccount_analytic_group clientModel = account_analytic_groupFeignClient.update(account_analytic_group.getId(),(account_analytic_groupImpl)account_analytic_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_group.getClass(), false);
        copier.copy(clientModel, account_analytic_group, null);
    }


    public Page<Iaccount_analytic_group> fetchDefault(SearchContext context){
        Page<account_analytic_groupImpl> page = this.account_analytic_groupFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iaccount_analytic_group account_analytic_group){
        account_analytic_groupFeignClient.remove(account_analytic_group.getId()) ;
    }


    public void createBatch(List<Iaccount_analytic_group> account_analytic_groups){
        if(account_analytic_groups!=null){
            List<account_analytic_groupImpl> list = new ArrayList<account_analytic_groupImpl>();
            for(Iaccount_analytic_group iaccount_analytic_group :account_analytic_groups){
                list.add((account_analytic_groupImpl)iaccount_analytic_group) ;
            }
            account_analytic_groupFeignClient.createBatch(list) ;
        }
    }


    public void create(Iaccount_analytic_group account_analytic_group){
        Iaccount_analytic_group clientModel = account_analytic_groupFeignClient.create((account_analytic_groupImpl)account_analytic_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_group.getClass(), false);
        copier.copy(clientModel, account_analytic_group, null);
    }


    public void get(Iaccount_analytic_group account_analytic_group){
        Iaccount_analytic_group clientModel = account_analytic_groupFeignClient.get(account_analytic_group.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_group.getClass(), false);
        copier.copy(clientModel, account_analytic_group, null);
    }


    public void updateBatch(List<Iaccount_analytic_group> account_analytic_groups){
        if(account_analytic_groups!=null){
            List<account_analytic_groupImpl> list = new ArrayList<account_analytic_groupImpl>();
            for(Iaccount_analytic_group iaccount_analytic_group :account_analytic_groups){
                list.add((account_analytic_groupImpl)iaccount_analytic_group) ;
            }
            account_analytic_groupFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_analytic_group> account_analytic_groups){
        if(account_analytic_groups!=null){
            List<account_analytic_groupImpl> list = new ArrayList<account_analytic_groupImpl>();
            for(Iaccount_analytic_group iaccount_analytic_group :account_analytic_groups){
                list.add((account_analytic_groupImpl)iaccount_analytic_group) ;
            }
            account_analytic_groupFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_analytic_group> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_analytic_group account_analytic_group){
        Iaccount_analytic_group clientModel = account_analytic_groupFeignClient.getDraft(account_analytic_group.getId(),(account_analytic_groupImpl)account_analytic_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_group.getClass(), false);
        copier.copy(clientModel, account_analytic_group, null);
    }



}


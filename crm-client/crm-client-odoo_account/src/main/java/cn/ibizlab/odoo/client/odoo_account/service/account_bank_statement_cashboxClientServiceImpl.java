package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_cashbox;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_cashboxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_cashboxImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statement_cashboxFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statement_cashboxClientServiceImpl implements Iaccount_bank_statement_cashboxClientService {

    account_bank_statement_cashboxFeignClient account_bank_statement_cashboxFeignClient;

    @Autowired
    public account_bank_statement_cashboxClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_cashboxFeignClient = nameBuilder.target(account_bank_statement_cashboxFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_cashboxFeignClient = nameBuilder.target(account_bank_statement_cashboxFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement_cashbox createModel() {
		return new account_bank_statement_cashboxImpl();
	}


    public void update(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
        Iaccount_bank_statement_cashbox clientModel = account_bank_statement_cashboxFeignClient.update(account_bank_statement_cashbox.getId(),(account_bank_statement_cashboxImpl)account_bank_statement_cashbox) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_cashbox.getClass(), false);
        copier.copy(clientModel, account_bank_statement_cashbox, null);
    }


    public void get(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
        Iaccount_bank_statement_cashbox clientModel = account_bank_statement_cashboxFeignClient.get(account_bank_statement_cashbox.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_cashbox.getClass(), false);
        copier.copy(clientModel, account_bank_statement_cashbox, null);
    }


    public void removeBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
        if(account_bank_statement_cashboxes!=null){
            List<account_bank_statement_cashboxImpl> list = new ArrayList<account_bank_statement_cashboxImpl>();
            for(Iaccount_bank_statement_cashbox iaccount_bank_statement_cashbox :account_bank_statement_cashboxes){
                list.add((account_bank_statement_cashboxImpl)iaccount_bank_statement_cashbox) ;
            }
            account_bank_statement_cashboxFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_bank_statement_cashbox> fetchDefault(SearchContext context){
        Page<account_bank_statement_cashboxImpl> page = this.account_bank_statement_cashboxFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
        if(account_bank_statement_cashboxes!=null){
            List<account_bank_statement_cashboxImpl> list = new ArrayList<account_bank_statement_cashboxImpl>();
            for(Iaccount_bank_statement_cashbox iaccount_bank_statement_cashbox :account_bank_statement_cashboxes){
                list.add((account_bank_statement_cashboxImpl)iaccount_bank_statement_cashbox) ;
            }
            account_bank_statement_cashboxFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
        account_bank_statement_cashboxFeignClient.remove(account_bank_statement_cashbox.getId()) ;
    }


    public void updateBatch(List<Iaccount_bank_statement_cashbox> account_bank_statement_cashboxes){
        if(account_bank_statement_cashboxes!=null){
            List<account_bank_statement_cashboxImpl> list = new ArrayList<account_bank_statement_cashboxImpl>();
            for(Iaccount_bank_statement_cashbox iaccount_bank_statement_cashbox :account_bank_statement_cashboxes){
                list.add((account_bank_statement_cashboxImpl)iaccount_bank_statement_cashbox) ;
            }
            account_bank_statement_cashboxFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
        Iaccount_bank_statement_cashbox clientModel = account_bank_statement_cashboxFeignClient.create((account_bank_statement_cashboxImpl)account_bank_statement_cashbox) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_cashbox.getClass(), false);
        copier.copy(clientModel, account_bank_statement_cashbox, null);
    }


    public Page<Iaccount_bank_statement_cashbox> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement_cashbox account_bank_statement_cashbox){
        Iaccount_bank_statement_cashbox clientModel = account_bank_statement_cashboxFeignClient.getDraft(account_bank_statement_cashbox.getId(),(account_bank_statement_cashboxImpl)account_bank_statement_cashbox) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_cashbox.getClass(), false);
        copier.copy(clientModel, account_bank_statement_cashbox, null);
    }



}


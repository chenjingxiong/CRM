package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_template;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_account_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_account_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_account_templateClientServiceImpl implements Iaccount_account_templateClientService {

    account_account_templateFeignClient account_account_templateFeignClient;

    @Autowired
    public account_account_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_templateFeignClient = nameBuilder.target(account_account_templateFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_templateFeignClient = nameBuilder.target(account_account_templateFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_account_template createModel() {
		return new account_account_templateImpl();
	}


    public void update(Iaccount_account_template account_account_template){
        Iaccount_account_template clientModel = account_account_templateFeignClient.update(account_account_template.getId(),(account_account_templateImpl)account_account_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_template.getClass(), false);
        copier.copy(clientModel, account_account_template, null);
    }


    public void get(Iaccount_account_template account_account_template){
        Iaccount_account_template clientModel = account_account_templateFeignClient.get(account_account_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_template.getClass(), false);
        copier.copy(clientModel, account_account_template, null);
    }


    public void removeBatch(List<Iaccount_account_template> account_account_templates){
        if(account_account_templates!=null){
            List<account_account_templateImpl> list = new ArrayList<account_account_templateImpl>();
            for(Iaccount_account_template iaccount_account_template :account_account_templates){
                list.add((account_account_templateImpl)iaccount_account_template) ;
            }
            account_account_templateFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_account_template> account_account_templates){
        if(account_account_templates!=null){
            List<account_account_templateImpl> list = new ArrayList<account_account_templateImpl>();
            for(Iaccount_account_template iaccount_account_template :account_account_templates){
                list.add((account_account_templateImpl)iaccount_account_template) ;
            }
            account_account_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_account_template> fetchDefault(SearchContext context){
        Page<account_account_templateImpl> page = this.account_account_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iaccount_account_template account_account_template){
        account_account_templateFeignClient.remove(account_account_template.getId()) ;
    }


    public void create(Iaccount_account_template account_account_template){
        Iaccount_account_template clientModel = account_account_templateFeignClient.create((account_account_templateImpl)account_account_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_template.getClass(), false);
        copier.copy(clientModel, account_account_template, null);
    }


    public void updateBatch(List<Iaccount_account_template> account_account_templates){
        if(account_account_templates!=null){
            List<account_account_templateImpl> list = new ArrayList<account_account_templateImpl>();
            for(Iaccount_account_template iaccount_account_template :account_account_templates){
                list.add((account_account_templateImpl)iaccount_account_template) ;
            }
            account_account_templateFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_account_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_account_template account_account_template){
        Iaccount_account_template clientModel = account_account_templateFeignClient.getDraft(account_account_template.getId(),(account_account_templateImpl)account_account_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_template.getClass(), false);
        copier.copy(clientModel, account_account_template, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_common_journal_report;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_common_journal_reportClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_common_journal_reportImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_common_journal_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_common_journal_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_common_journal_reportClientServiceImpl implements Iaccount_common_journal_reportClientService {

    account_common_journal_reportFeignClient account_common_journal_reportFeignClient;

    @Autowired
    public account_common_journal_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_common_journal_reportFeignClient = nameBuilder.target(account_common_journal_reportFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_common_journal_reportFeignClient = nameBuilder.target(account_common_journal_reportFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_common_journal_report createModel() {
		return new account_common_journal_reportImpl();
	}


    public void createBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
        if(account_common_journal_reports!=null){
            List<account_common_journal_reportImpl> list = new ArrayList<account_common_journal_reportImpl>();
            for(Iaccount_common_journal_report iaccount_common_journal_report :account_common_journal_reports){
                list.add((account_common_journal_reportImpl)iaccount_common_journal_report) ;
            }
            account_common_journal_reportFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iaccount_common_journal_report account_common_journal_report){
        account_common_journal_reportFeignClient.remove(account_common_journal_report.getId()) ;
    }


    public void get(Iaccount_common_journal_report account_common_journal_report){
        Iaccount_common_journal_report clientModel = account_common_journal_reportFeignClient.get(account_common_journal_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_common_journal_report.getClass(), false);
        copier.copy(clientModel, account_common_journal_report, null);
    }


    public void removeBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
        if(account_common_journal_reports!=null){
            List<account_common_journal_reportImpl> list = new ArrayList<account_common_journal_reportImpl>();
            for(Iaccount_common_journal_report iaccount_common_journal_report :account_common_journal_reports){
                list.add((account_common_journal_reportImpl)iaccount_common_journal_report) ;
            }
            account_common_journal_reportFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_common_journal_report account_common_journal_report){
        Iaccount_common_journal_report clientModel = account_common_journal_reportFeignClient.create((account_common_journal_reportImpl)account_common_journal_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_common_journal_report.getClass(), false);
        copier.copy(clientModel, account_common_journal_report, null);
    }


    public Page<Iaccount_common_journal_report> fetchDefault(SearchContext context){
        Page<account_common_journal_reportImpl> page = this.account_common_journal_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
        if(account_common_journal_reports!=null){
            List<account_common_journal_reportImpl> list = new ArrayList<account_common_journal_reportImpl>();
            for(Iaccount_common_journal_report iaccount_common_journal_report :account_common_journal_reports){
                list.add((account_common_journal_reportImpl)iaccount_common_journal_report) ;
            }
            account_common_journal_reportFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_common_journal_report account_common_journal_report){
        Iaccount_common_journal_report clientModel = account_common_journal_reportFeignClient.update(account_common_journal_report.getId(),(account_common_journal_reportImpl)account_common_journal_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_common_journal_report.getClass(), false);
        copier.copy(clientModel, account_common_journal_report, null);
    }


    public Page<Iaccount_common_journal_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_common_journal_report account_common_journal_report){
        Iaccount_common_journal_report clientModel = account_common_journal_reportFeignClient.getDraft(account_common_journal_report.getId(),(account_common_journal_reportImpl)account_common_journal_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_common_journal_report.getClass(), false);
        copier.copy(clientModel, account_common_journal_report, null);
    }



}


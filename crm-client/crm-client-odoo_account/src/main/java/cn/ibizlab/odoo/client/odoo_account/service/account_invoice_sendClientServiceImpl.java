package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_send;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_sendClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_sendImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_sendFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_send] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_sendClientServiceImpl implements Iaccount_invoice_sendClientService {

    account_invoice_sendFeignClient account_invoice_sendFeignClient;

    @Autowired
    public account_invoice_sendClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_sendFeignClient = nameBuilder.target(account_invoice_sendFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_sendFeignClient = nameBuilder.target(account_invoice_sendFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_send createModel() {
		return new account_invoice_sendImpl();
	}


    public Page<Iaccount_invoice_send> fetchDefault(SearchContext context){
        Page<account_invoice_sendImpl> page = this.account_invoice_sendFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_invoice_send account_invoice_send){
        Iaccount_invoice_send clientModel = account_invoice_sendFeignClient.create((account_invoice_sendImpl)account_invoice_send) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_send.getClass(), false);
        copier.copy(clientModel, account_invoice_send, null);
    }


    public void remove(Iaccount_invoice_send account_invoice_send){
        account_invoice_sendFeignClient.remove(account_invoice_send.getId()) ;
    }


    public void createBatch(List<Iaccount_invoice_send> account_invoice_sends){
        if(account_invoice_sends!=null){
            List<account_invoice_sendImpl> list = new ArrayList<account_invoice_sendImpl>();
            for(Iaccount_invoice_send iaccount_invoice_send :account_invoice_sends){
                list.add((account_invoice_sendImpl)iaccount_invoice_send) ;
            }
            account_invoice_sendFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_invoice_send> account_invoice_sends){
        if(account_invoice_sends!=null){
            List<account_invoice_sendImpl> list = new ArrayList<account_invoice_sendImpl>();
            for(Iaccount_invoice_send iaccount_invoice_send :account_invoice_sends){
                list.add((account_invoice_sendImpl)iaccount_invoice_send) ;
            }
            account_invoice_sendFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iaccount_invoice_send account_invoice_send){
        Iaccount_invoice_send clientModel = account_invoice_sendFeignClient.get(account_invoice_send.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_send.getClass(), false);
        copier.copy(clientModel, account_invoice_send, null);
    }


    public void updateBatch(List<Iaccount_invoice_send> account_invoice_sends){
        if(account_invoice_sends!=null){
            List<account_invoice_sendImpl> list = new ArrayList<account_invoice_sendImpl>();
            for(Iaccount_invoice_send iaccount_invoice_send :account_invoice_sends){
                list.add((account_invoice_sendImpl)iaccount_invoice_send) ;
            }
            account_invoice_sendFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_invoice_send account_invoice_send){
        Iaccount_invoice_send clientModel = account_invoice_sendFeignClient.update(account_invoice_send.getId(),(account_invoice_sendImpl)account_invoice_send) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_send.getClass(), false);
        copier.copy(clientModel, account_invoice_send, null);
    }


    public Page<Iaccount_invoice_send> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_send account_invoice_send){
        Iaccount_invoice_send clientModel = account_invoice_sendFeignClient.getDraft(account_invoice_send.getId(),(account_invoice_sendImpl)account_invoice_send) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_send.getClass(), false);
        copier.copy(clientModel, account_invoice_send, null);
    }



}


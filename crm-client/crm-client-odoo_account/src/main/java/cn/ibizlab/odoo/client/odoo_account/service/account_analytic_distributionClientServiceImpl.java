package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_distribution;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_distributionClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_distributionImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_analytic_distributionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_analytic_distributionClientServiceImpl implements Iaccount_analytic_distributionClientService {

    account_analytic_distributionFeignClient account_analytic_distributionFeignClient;

    @Autowired
    public account_analytic_distributionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_distributionFeignClient = nameBuilder.target(account_analytic_distributionFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_distributionFeignClient = nameBuilder.target(account_analytic_distributionFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_analytic_distribution createModel() {
		return new account_analytic_distributionImpl();
	}


    public void updateBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
        if(account_analytic_distributions!=null){
            List<account_analytic_distributionImpl> list = new ArrayList<account_analytic_distributionImpl>();
            for(Iaccount_analytic_distribution iaccount_analytic_distribution :account_analytic_distributions){
                list.add((account_analytic_distributionImpl)iaccount_analytic_distribution) ;
            }
            account_analytic_distributionFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
        if(account_analytic_distributions!=null){
            List<account_analytic_distributionImpl> list = new ArrayList<account_analytic_distributionImpl>();
            for(Iaccount_analytic_distribution iaccount_analytic_distribution :account_analytic_distributions){
                list.add((account_analytic_distributionImpl)iaccount_analytic_distribution) ;
            }
            account_analytic_distributionFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_analytic_distribution> fetchDefault(SearchContext context){
        Page<account_analytic_distributionImpl> page = this.account_analytic_distributionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_analytic_distribution account_analytic_distribution){
        Iaccount_analytic_distribution clientModel = account_analytic_distributionFeignClient.create((account_analytic_distributionImpl)account_analytic_distribution) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_distribution.getClass(), false);
        copier.copy(clientModel, account_analytic_distribution, null);
    }


    public void update(Iaccount_analytic_distribution account_analytic_distribution){
        Iaccount_analytic_distribution clientModel = account_analytic_distributionFeignClient.update(account_analytic_distribution.getId(),(account_analytic_distributionImpl)account_analytic_distribution) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_distribution.getClass(), false);
        copier.copy(clientModel, account_analytic_distribution, null);
    }


    public void removeBatch(List<Iaccount_analytic_distribution> account_analytic_distributions){
        if(account_analytic_distributions!=null){
            List<account_analytic_distributionImpl> list = new ArrayList<account_analytic_distributionImpl>();
            for(Iaccount_analytic_distribution iaccount_analytic_distribution :account_analytic_distributions){
                list.add((account_analytic_distributionImpl)iaccount_analytic_distribution) ;
            }
            account_analytic_distributionFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iaccount_analytic_distribution account_analytic_distribution){
        Iaccount_analytic_distribution clientModel = account_analytic_distributionFeignClient.get(account_analytic_distribution.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_distribution.getClass(), false);
        copier.copy(clientModel, account_analytic_distribution, null);
    }


    public void remove(Iaccount_analytic_distribution account_analytic_distribution){
        account_analytic_distributionFeignClient.remove(account_analytic_distribution.getId()) ;
    }


    public Page<Iaccount_analytic_distribution> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_analytic_distribution account_analytic_distribution){
        Iaccount_analytic_distribution clientModel = account_analytic_distributionFeignClient.getDraft(account_analytic_distribution.getId(),(account_analytic_distributionImpl)account_analytic_distribution) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_distribution.getClass(), false);
        copier.copy(clientModel, account_analytic_distribution, null);
    }



}


package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_line;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_move_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_move_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_move_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_move_lineClientServiceImpl implements Iaccount_move_lineClientService {

    account_move_lineFeignClient account_move_lineFeignClient;

    @Autowired
    public account_move_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_move_lineFeignClient = nameBuilder.target(account_move_lineFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_move_lineFeignClient = nameBuilder.target(account_move_lineFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_move_line createModel() {
		return new account_move_lineImpl();
	}


    public void update(Iaccount_move_line account_move_line){
        Iaccount_move_line clientModel = account_move_lineFeignClient.update(account_move_line.getId(),(account_move_lineImpl)account_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_line.getClass(), false);
        copier.copy(clientModel, account_move_line, null);
    }


    public void createBatch(List<Iaccount_move_line> account_move_lines){
        if(account_move_lines!=null){
            List<account_move_lineImpl> list = new ArrayList<account_move_lineImpl>();
            for(Iaccount_move_line iaccount_move_line :account_move_lines){
                list.add((account_move_lineImpl)iaccount_move_line) ;
            }
            account_move_lineFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_move_line> fetchDefault(SearchContext context){
        Page<account_move_lineImpl> page = this.account_move_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_move_line account_move_line){
        Iaccount_move_line clientModel = account_move_lineFeignClient.create((account_move_lineImpl)account_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_line.getClass(), false);
        copier.copy(clientModel, account_move_line, null);
    }


    public void remove(Iaccount_move_line account_move_line){
        account_move_lineFeignClient.remove(account_move_line.getId()) ;
    }


    public void get(Iaccount_move_line account_move_line){
        Iaccount_move_line clientModel = account_move_lineFeignClient.get(account_move_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_line.getClass(), false);
        copier.copy(clientModel, account_move_line, null);
    }


    public void updateBatch(List<Iaccount_move_line> account_move_lines){
        if(account_move_lines!=null){
            List<account_move_lineImpl> list = new ArrayList<account_move_lineImpl>();
            for(Iaccount_move_line iaccount_move_line :account_move_lines){
                list.add((account_move_lineImpl)iaccount_move_line) ;
            }
            account_move_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_move_line> account_move_lines){
        if(account_move_lines!=null){
            List<account_move_lineImpl> list = new ArrayList<account_move_lineImpl>();
            for(Iaccount_move_line iaccount_move_line :account_move_lines){
                list.add((account_move_lineImpl)iaccount_move_line) ;
            }
            account_move_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_move_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_move_line account_move_line){
        Iaccount_move_line clientModel = account_move_lineFeignClient.getDraft(account_move_line.getId(),(account_move_lineImpl)account_move_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move_line.getClass(), false);
        copier.copy(clientModel, account_move_line, null);
    }



}


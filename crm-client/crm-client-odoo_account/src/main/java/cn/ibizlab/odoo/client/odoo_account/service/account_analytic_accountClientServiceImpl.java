package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_analytic_accountFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_analytic_account] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_analytic_accountClientServiceImpl implements Iaccount_analytic_accountClientService {

    account_analytic_accountFeignClient account_analytic_accountFeignClient;

    @Autowired
    public account_analytic_accountClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_accountFeignClient = nameBuilder.target(account_analytic_accountFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_analytic_accountFeignClient = nameBuilder.target(account_analytic_accountFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_analytic_account createModel() {
		return new account_analytic_accountImpl();
	}


    public void updateBatch(List<Iaccount_analytic_account> account_analytic_accounts){
        if(account_analytic_accounts!=null){
            List<account_analytic_accountImpl> list = new ArrayList<account_analytic_accountImpl>();
            for(Iaccount_analytic_account iaccount_analytic_account :account_analytic_accounts){
                list.add((account_analytic_accountImpl)iaccount_analytic_account) ;
            }
            account_analytic_accountFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iaccount_analytic_account account_analytic_account){
        Iaccount_analytic_account clientModel = account_analytic_accountFeignClient.create((account_analytic_accountImpl)account_analytic_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_account.getClass(), false);
        copier.copy(clientModel, account_analytic_account, null);
    }


    public void remove(Iaccount_analytic_account account_analytic_account){
        account_analytic_accountFeignClient.remove(account_analytic_account.getId()) ;
    }


    public void removeBatch(List<Iaccount_analytic_account> account_analytic_accounts){
        if(account_analytic_accounts!=null){
            List<account_analytic_accountImpl> list = new ArrayList<account_analytic_accountImpl>();
            for(Iaccount_analytic_account iaccount_analytic_account :account_analytic_accounts){
                list.add((account_analytic_accountImpl)iaccount_analytic_account) ;
            }
            account_analytic_accountFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_analytic_account> account_analytic_accounts){
        if(account_analytic_accounts!=null){
            List<account_analytic_accountImpl> list = new ArrayList<account_analytic_accountImpl>();
            for(Iaccount_analytic_account iaccount_analytic_account :account_analytic_accounts){
                list.add((account_analytic_accountImpl)iaccount_analytic_account) ;
            }
            account_analytic_accountFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_analytic_account> fetchDefault(SearchContext context){
        Page<account_analytic_accountImpl> page = this.account_analytic_accountFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iaccount_analytic_account account_analytic_account){
        Iaccount_analytic_account clientModel = account_analytic_accountFeignClient.get(account_analytic_account.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_account.getClass(), false);
        copier.copy(clientModel, account_analytic_account, null);
    }


    public void update(Iaccount_analytic_account account_analytic_account){
        Iaccount_analytic_account clientModel = account_analytic_accountFeignClient.update(account_analytic_account.getId(),(account_analytic_accountImpl)account_analytic_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_account.getClass(), false);
        copier.copy(clientModel, account_analytic_account, null);
    }


    public Page<Iaccount_analytic_account> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_analytic_account account_analytic_account){
        Iaccount_analytic_account clientModel = account_analytic_accountFeignClient.getDraft(account_analytic_account.getId(),(account_analytic_accountImpl)account_analytic_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_analytic_account.getClass(), false);
        copier.copy(clientModel, account_analytic_account, null);
    }



}


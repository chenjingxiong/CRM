package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_cashbox_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_cashbox_line] 服务对象接口
 */
public interface account_cashbox_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cashbox_lines/{id}")
    public account_cashbox_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_cashbox_lineImpl account_cashbox_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cashbox_lines/createbatch")
    public account_cashbox_lineImpl createBatch(@RequestBody List<account_cashbox_lineImpl> account_cashbox_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cashbox_lines/fetchdefault")
    public Page<account_cashbox_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cashbox_lines/{id}")
    public account_cashbox_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cashbox_lines")
    public account_cashbox_lineImpl create(@RequestBody account_cashbox_lineImpl account_cashbox_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cashbox_lines/updatebatch")
    public account_cashbox_lineImpl updateBatch(@RequestBody List<account_cashbox_lineImpl> account_cashbox_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cashbox_lines/removebatch")
    public account_cashbox_lineImpl removeBatch(@RequestBody List<account_cashbox_lineImpl> account_cashbox_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cashbox_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cashbox_lines/select")
    public Page<account_cashbox_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cashbox_lines/{id}/getdraft")
    public account_cashbox_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_cashbox_lineImpl account_cashbox_line);



}

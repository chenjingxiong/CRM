package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_closebalance;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_closebalanceClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_closebalanceImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_bank_statement_closebalanceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_bank_statement_closebalanceClientServiceImpl implements Iaccount_bank_statement_closebalanceClientService {

    account_bank_statement_closebalanceFeignClient account_bank_statement_closebalanceFeignClient;

    @Autowired
    public account_bank_statement_closebalanceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_closebalanceFeignClient = nameBuilder.target(account_bank_statement_closebalanceFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_bank_statement_closebalanceFeignClient = nameBuilder.target(account_bank_statement_closebalanceFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_bank_statement_closebalance createModel() {
		return new account_bank_statement_closebalanceImpl();
	}


    public Page<Iaccount_bank_statement_closebalance> fetchDefault(SearchContext context){
        Page<account_bank_statement_closebalanceImpl> page = this.account_bank_statement_closebalanceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
        Iaccount_bank_statement_closebalance clientModel = account_bank_statement_closebalanceFeignClient.update(account_bank_statement_closebalance.getId(),(account_bank_statement_closebalanceImpl)account_bank_statement_closebalance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_closebalance.getClass(), false);
        copier.copy(clientModel, account_bank_statement_closebalance, null);
    }


    public void remove(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
        account_bank_statement_closebalanceFeignClient.remove(account_bank_statement_closebalance.getId()) ;
    }


    public void get(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
        Iaccount_bank_statement_closebalance clientModel = account_bank_statement_closebalanceFeignClient.get(account_bank_statement_closebalance.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_closebalance.getClass(), false);
        copier.copy(clientModel, account_bank_statement_closebalance, null);
    }


    public void create(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
        Iaccount_bank_statement_closebalance clientModel = account_bank_statement_closebalanceFeignClient.create((account_bank_statement_closebalanceImpl)account_bank_statement_closebalance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_closebalance.getClass(), false);
        copier.copy(clientModel, account_bank_statement_closebalance, null);
    }


    public void createBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
        if(account_bank_statement_closebalances!=null){
            List<account_bank_statement_closebalanceImpl> list = new ArrayList<account_bank_statement_closebalanceImpl>();
            for(Iaccount_bank_statement_closebalance iaccount_bank_statement_closebalance :account_bank_statement_closebalances){
                list.add((account_bank_statement_closebalanceImpl)iaccount_bank_statement_closebalance) ;
            }
            account_bank_statement_closebalanceFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
        if(account_bank_statement_closebalances!=null){
            List<account_bank_statement_closebalanceImpl> list = new ArrayList<account_bank_statement_closebalanceImpl>();
            for(Iaccount_bank_statement_closebalance iaccount_bank_statement_closebalance :account_bank_statement_closebalances){
                list.add((account_bank_statement_closebalanceImpl)iaccount_bank_statement_closebalance) ;
            }
            account_bank_statement_closebalanceFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
        if(account_bank_statement_closebalances!=null){
            List<account_bank_statement_closebalanceImpl> list = new ArrayList<account_bank_statement_closebalanceImpl>();
            for(Iaccount_bank_statement_closebalance iaccount_bank_statement_closebalance :account_bank_statement_closebalances){
                list.add((account_bank_statement_closebalanceImpl)iaccount_bank_statement_closebalance) ;
            }
            account_bank_statement_closebalanceFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_bank_statement_closebalance> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
        Iaccount_bank_statement_closebalance clientModel = account_bank_statement_closebalanceFeignClient.getDraft(account_bank_statement_closebalance.getId(),(account_bank_statement_closebalanceImpl)account_bank_statement_closebalance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_bank_statement_closebalance.getClass(), false);
        copier.copy(clientModel, account_bank_statement_closebalance, null);
    }



}


package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_product_category;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_product_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_product_category] 服务对象接口
 */
public interface lunch_product_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_product_categories/fetchdefault")
    public Page<lunch_product_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_product_categories")
    public lunch_product_categoryImpl create(@RequestBody lunch_product_categoryImpl lunch_product_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_product_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_product_categories/updatebatch")
    public lunch_product_categoryImpl updateBatch(@RequestBody List<lunch_product_categoryImpl> lunch_product_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_product_categories/{id}")
    public lunch_product_categoryImpl update(@PathVariable("id") Integer id,@RequestBody lunch_product_categoryImpl lunch_product_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_product_categories/{id}")
    public lunch_product_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_product_categories/createbatch")
    public lunch_product_categoryImpl createBatch(@RequestBody List<lunch_product_categoryImpl> lunch_product_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_product_categories/removebatch")
    public lunch_product_categoryImpl removeBatch(@RequestBody List<lunch_product_categoryImpl> lunch_product_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_product_categories/select")
    public Page<lunch_product_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_product_categories/{id}/getdraft")
    public lunch_product_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_product_categoryImpl lunch_product_category);



}

package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_order_line] 服务对象接口
 */
public interface lunch_order_lineFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_lines/{id}")
    public lunch_order_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_lines/createbatch")
    public lunch_order_lineImpl createBatch(@RequestBody List<lunch_order_lineImpl> lunch_order_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_lines/removebatch")
    public lunch_order_lineImpl removeBatch(@RequestBody List<lunch_order_lineImpl> lunch_order_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_lines/{id}")
    public lunch_order_lineImpl update(@PathVariable("id") Integer id,@RequestBody lunch_order_lineImpl lunch_order_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_lines/fetchdefault")
    public Page<lunch_order_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_lines")
    public lunch_order_lineImpl create(@RequestBody lunch_order_lineImpl lunch_order_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_lines/updatebatch")
    public lunch_order_lineImpl updateBatch(@RequestBody List<lunch_order_lineImpl> lunch_order_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_lines/select")
    public Page<lunch_order_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_lines/{id}/getdraft")
    public lunch_order_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_order_lineImpl lunch_order_line);



}

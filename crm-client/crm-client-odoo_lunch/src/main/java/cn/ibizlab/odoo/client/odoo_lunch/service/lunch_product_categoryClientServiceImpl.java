package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_product_category;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_product_categoryClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_product_categoryImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_product_categoryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_product_category] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_product_categoryClientServiceImpl implements Ilunch_product_categoryClientService {

    lunch_product_categoryFeignClient lunch_product_categoryFeignClient;

    @Autowired
    public lunch_product_categoryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_product_categoryFeignClient = nameBuilder.target(lunch_product_categoryFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_product_categoryFeignClient = nameBuilder.target(lunch_product_categoryFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_product_category createModel() {
		return new lunch_product_categoryImpl();
	}


    public Page<Ilunch_product_category> fetchDefault(SearchContext context){
        Page<lunch_product_categoryImpl> page = this.lunch_product_categoryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ilunch_product_category lunch_product_category){
        Ilunch_product_category clientModel = lunch_product_categoryFeignClient.create((lunch_product_categoryImpl)lunch_product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product_category.getClass(), false);
        copier.copy(clientModel, lunch_product_category, null);
    }


    public void remove(Ilunch_product_category lunch_product_category){
        lunch_product_categoryFeignClient.remove(lunch_product_category.getId()) ;
    }


    public void updateBatch(List<Ilunch_product_category> lunch_product_categories){
        if(lunch_product_categories!=null){
            List<lunch_product_categoryImpl> list = new ArrayList<lunch_product_categoryImpl>();
            for(Ilunch_product_category ilunch_product_category :lunch_product_categories){
                list.add((lunch_product_categoryImpl)ilunch_product_category) ;
            }
            lunch_product_categoryFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ilunch_product_category lunch_product_category){
        Ilunch_product_category clientModel = lunch_product_categoryFeignClient.update(lunch_product_category.getId(),(lunch_product_categoryImpl)lunch_product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product_category.getClass(), false);
        copier.copy(clientModel, lunch_product_category, null);
    }


    public void get(Ilunch_product_category lunch_product_category){
        Ilunch_product_category clientModel = lunch_product_categoryFeignClient.get(lunch_product_category.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product_category.getClass(), false);
        copier.copy(clientModel, lunch_product_category, null);
    }


    public void createBatch(List<Ilunch_product_category> lunch_product_categories){
        if(lunch_product_categories!=null){
            List<lunch_product_categoryImpl> list = new ArrayList<lunch_product_categoryImpl>();
            for(Ilunch_product_category ilunch_product_category :lunch_product_categories){
                list.add((lunch_product_categoryImpl)ilunch_product_category) ;
            }
            lunch_product_categoryFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ilunch_product_category> lunch_product_categories){
        if(lunch_product_categories!=null){
            List<lunch_product_categoryImpl> list = new ArrayList<lunch_product_categoryImpl>();
            for(Ilunch_product_category ilunch_product_category :lunch_product_categories){
                list.add((lunch_product_categoryImpl)ilunch_product_category) ;
            }
            lunch_product_categoryFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ilunch_product_category> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_product_category lunch_product_category){
        Ilunch_product_category clientModel = lunch_product_categoryFeignClient.getDraft(lunch_product_category.getId(),(lunch_product_categoryImpl)lunch_product_category) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product_category.getClass(), false);
        copier.copy(clientModel, lunch_product_category, null);
    }



}


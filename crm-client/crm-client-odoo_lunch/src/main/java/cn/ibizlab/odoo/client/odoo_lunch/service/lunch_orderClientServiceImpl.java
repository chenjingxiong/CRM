package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_orderClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_orderImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_orderClientServiceImpl implements Ilunch_orderClientService {

    lunch_orderFeignClient lunch_orderFeignClient;

    @Autowired
    public lunch_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_orderFeignClient = nameBuilder.target(lunch_orderFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_orderFeignClient = nameBuilder.target(lunch_orderFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_order createModel() {
		return new lunch_orderImpl();
	}


    public void removeBatch(List<Ilunch_order> lunch_orders){
        if(lunch_orders!=null){
            List<lunch_orderImpl> list = new ArrayList<lunch_orderImpl>();
            for(Ilunch_order ilunch_order :lunch_orders){
                list.add((lunch_orderImpl)ilunch_order) ;
            }
            lunch_orderFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ilunch_order> lunch_orders){
        if(lunch_orders!=null){
            List<lunch_orderImpl> list = new ArrayList<lunch_orderImpl>();
            for(Ilunch_order ilunch_order :lunch_orders){
                list.add((lunch_orderImpl)ilunch_order) ;
            }
            lunch_orderFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ilunch_order lunch_order){
        lunch_orderFeignClient.remove(lunch_order.getId()) ;
    }


    public Page<Ilunch_order> fetchDefault(SearchContext context){
        Page<lunch_orderImpl> page = this.lunch_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ilunch_order lunch_order){
        Ilunch_order clientModel = lunch_orderFeignClient.create((lunch_orderImpl)lunch_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order.getClass(), false);
        copier.copy(clientModel, lunch_order, null);
    }


    public void updateBatch(List<Ilunch_order> lunch_orders){
        if(lunch_orders!=null){
            List<lunch_orderImpl> list = new ArrayList<lunch_orderImpl>();
            for(Ilunch_order ilunch_order :lunch_orders){
                list.add((lunch_orderImpl)ilunch_order) ;
            }
            lunch_orderFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ilunch_order lunch_order){
        Ilunch_order clientModel = lunch_orderFeignClient.get(lunch_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order.getClass(), false);
        copier.copy(clientModel, lunch_order, null);
    }


    public void update(Ilunch_order lunch_order){
        Ilunch_order clientModel = lunch_orderFeignClient.update(lunch_order.getId(),(lunch_orderImpl)lunch_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order.getClass(), false);
        copier.copy(clientModel, lunch_order, null);
    }


    public Page<Ilunch_order> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_order lunch_order){
        Ilunch_order clientModel = lunch_orderFeignClient.getDraft(lunch_order.getId(),(lunch_orderImpl)lunch_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order.getClass(), false);
        copier.copy(clientModel, lunch_order, null);
    }



}


package cn.ibizlab.odoo.client.odoo_lunch.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ilunch_alert;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_alertImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[lunch_alert] 服务对象接口
 */
public interface lunch_alertFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_alerts/removebatch")
    public lunch_alertImpl removeBatch(@RequestBody List<lunch_alertImpl> lunch_alerts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_alerts/createbatch")
    public lunch_alertImpl createBatch(@RequestBody List<lunch_alertImpl> lunch_alerts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_alerts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_alerts/{id}")
    public lunch_alertImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_alerts/fetchdefault")
    public Page<lunch_alertImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_alerts/updatebatch")
    public lunch_alertImpl updateBatch(@RequestBody List<lunch_alertImpl> lunch_alerts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_alerts/{id}")
    public lunch_alertImpl update(@PathVariable("id") Integer id,@RequestBody lunch_alertImpl lunch_alert);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_alerts")
    public lunch_alertImpl create(@RequestBody lunch_alertImpl lunch_alert);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_alerts/select")
    public Page<lunch_alertImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_alerts/{id}/getdraft")
    public lunch_alertImpl getDraft(@PathVariable("id") Integer id,@RequestBody lunch_alertImpl lunch_alert);



}

package cn.ibizlab.odoo.client.odoo_lunch.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ilunch_alert;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[lunch_alert] 对象
 */
public class lunch_alertImpl implements Ilunch_alert,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 重新提起
     */
    public String alert_type;

    @JsonIgnore
    public boolean alert_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示
     */
    public String display;

    @JsonIgnore
    public boolean displayDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 和
     */
    public Double end_hour;

    @JsonIgnore
    public boolean end_hourDirtyFlag;
    
    /**
     * 周五
     */
    public String friday;

    @JsonIgnore
    public boolean fridayDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 消息
     */
    public String message;

    @JsonIgnore
    public boolean messageDirtyFlag;
    
    /**
     * 周一
     */
    public String monday;

    @JsonIgnore
    public boolean mondayDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 周六
     */
    public String saturday;

    @JsonIgnore
    public boolean saturdayDirtyFlag;
    
    /**
     * 日
     */
    public Timestamp specific_day;

    @JsonIgnore
    public boolean specific_dayDirtyFlag;
    
    /**
     * 介于
     */
    public Double start_hour;

    @JsonIgnore
    public boolean start_hourDirtyFlag;
    
    /**
     * 周日
     */
    public String sunday;

    @JsonIgnore
    public boolean sundayDirtyFlag;
    
    /**
     * 周四
     */
    public String thursday;

    @JsonIgnore
    public boolean thursdayDirtyFlag;
    
    /**
     * 周二
     */
    public String tuesday;

    @JsonIgnore
    public boolean tuesdayDirtyFlag;
    
    /**
     * 周三
     */
    public String wednesday;

    @JsonIgnore
    public boolean wednesdayDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [重新提起]
     */
    @JsonProperty("alert_type")
    public String getAlert_type(){
        return this.alert_type ;
    }

    /**
     * 设置 [重新提起]
     */
    @JsonProperty("alert_type")
    public void setAlert_type(String  alert_type){
        this.alert_type = alert_type ;
        this.alert_typeDirtyFlag = true ;
    }

     /**
     * 获取 [重新提起]脏标记
     */
    @JsonIgnore
    public boolean getAlert_typeDirtyFlag(){
        return this.alert_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示]
     */
    @JsonProperty("display")
    public String getDisplay(){
        return this.display ;
    }

    /**
     * 设置 [显示]
     */
    @JsonProperty("display")
    public void setDisplay(String  display){
        this.display = display ;
        this.displayDirtyFlag = true ;
    }

     /**
     * 获取 [显示]脏标记
     */
    @JsonIgnore
    public boolean getDisplayDirtyFlag(){
        return this.displayDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [和]
     */
    @JsonProperty("end_hour")
    public Double getEnd_hour(){
        return this.end_hour ;
    }

    /**
     * 设置 [和]
     */
    @JsonProperty("end_hour")
    public void setEnd_hour(Double  end_hour){
        this.end_hour = end_hour ;
        this.end_hourDirtyFlag = true ;
    }

     /**
     * 获取 [和]脏标记
     */
    @JsonIgnore
    public boolean getEnd_hourDirtyFlag(){
        return this.end_hourDirtyFlag ;
    }   

    /**
     * 获取 [周五]
     */
    @JsonProperty("friday")
    public String getFriday(){
        return this.friday ;
    }

    /**
     * 设置 [周五]
     */
    @JsonProperty("friday")
    public void setFriday(String  friday){
        this.friday = friday ;
        this.fridayDirtyFlag = true ;
    }

     /**
     * 获取 [周五]脏标记
     */
    @JsonIgnore
    public boolean getFridayDirtyFlag(){
        return this.fridayDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message")
    public String getMessage(){
        return this.message ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message")
    public void setMessage(String  message){
        this.message = message ;
        this.messageDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessageDirtyFlag(){
        return this.messageDirtyFlag ;
    }   

    /**
     * 获取 [周一]
     */
    @JsonProperty("monday")
    public String getMonday(){
        return this.monday ;
    }

    /**
     * 设置 [周一]
     */
    @JsonProperty("monday")
    public void setMonday(String  monday){
        this.monday = monday ;
        this.mondayDirtyFlag = true ;
    }

     /**
     * 获取 [周一]脏标记
     */
    @JsonIgnore
    public boolean getMondayDirtyFlag(){
        return this.mondayDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [周六]
     */
    @JsonProperty("saturday")
    public String getSaturday(){
        return this.saturday ;
    }

    /**
     * 设置 [周六]
     */
    @JsonProperty("saturday")
    public void setSaturday(String  saturday){
        this.saturday = saturday ;
        this.saturdayDirtyFlag = true ;
    }

     /**
     * 获取 [周六]脏标记
     */
    @JsonIgnore
    public boolean getSaturdayDirtyFlag(){
        return this.saturdayDirtyFlag ;
    }   

    /**
     * 获取 [日]
     */
    @JsonProperty("specific_day")
    public Timestamp getSpecific_day(){
        return this.specific_day ;
    }

    /**
     * 设置 [日]
     */
    @JsonProperty("specific_day")
    public void setSpecific_day(Timestamp  specific_day){
        this.specific_day = specific_day ;
        this.specific_dayDirtyFlag = true ;
    }

     /**
     * 获取 [日]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_dayDirtyFlag(){
        return this.specific_dayDirtyFlag ;
    }   

    /**
     * 获取 [介于]
     */
    @JsonProperty("start_hour")
    public Double getStart_hour(){
        return this.start_hour ;
    }

    /**
     * 设置 [介于]
     */
    @JsonProperty("start_hour")
    public void setStart_hour(Double  start_hour){
        this.start_hour = start_hour ;
        this.start_hourDirtyFlag = true ;
    }

     /**
     * 获取 [介于]脏标记
     */
    @JsonIgnore
    public boolean getStart_hourDirtyFlag(){
        return this.start_hourDirtyFlag ;
    }   

    /**
     * 获取 [周日]
     */
    @JsonProperty("sunday")
    public String getSunday(){
        return this.sunday ;
    }

    /**
     * 设置 [周日]
     */
    @JsonProperty("sunday")
    public void setSunday(String  sunday){
        this.sunday = sunday ;
        this.sundayDirtyFlag = true ;
    }

     /**
     * 获取 [周日]脏标记
     */
    @JsonIgnore
    public boolean getSundayDirtyFlag(){
        return this.sundayDirtyFlag ;
    }   

    /**
     * 获取 [周四]
     */
    @JsonProperty("thursday")
    public String getThursday(){
        return this.thursday ;
    }

    /**
     * 设置 [周四]
     */
    @JsonProperty("thursday")
    public void setThursday(String  thursday){
        this.thursday = thursday ;
        this.thursdayDirtyFlag = true ;
    }

     /**
     * 获取 [周四]脏标记
     */
    @JsonIgnore
    public boolean getThursdayDirtyFlag(){
        return this.thursdayDirtyFlag ;
    }   

    /**
     * 获取 [周二]
     */
    @JsonProperty("tuesday")
    public String getTuesday(){
        return this.tuesday ;
    }

    /**
     * 设置 [周二]
     */
    @JsonProperty("tuesday")
    public void setTuesday(String  tuesday){
        this.tuesday = tuesday ;
        this.tuesdayDirtyFlag = true ;
    }

     /**
     * 获取 [周二]脏标记
     */
    @JsonIgnore
    public boolean getTuesdayDirtyFlag(){
        return this.tuesdayDirtyFlag ;
    }   

    /**
     * 获取 [周三]
     */
    @JsonProperty("wednesday")
    public String getWednesday(){
        return this.wednesday ;
    }

    /**
     * 设置 [周三]
     */
    @JsonProperty("wednesday")
    public void setWednesday(String  wednesday){
        this.wednesday = wednesday ;
        this.wednesdayDirtyFlag = true ;
    }

     /**
     * 获取 [周三]脏标记
     */
    @JsonIgnore
    public boolean getWednesdayDirtyFlag(){
        return this.wednesdayDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}

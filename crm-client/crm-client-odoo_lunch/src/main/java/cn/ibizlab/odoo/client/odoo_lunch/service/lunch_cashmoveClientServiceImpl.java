package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_cashmove;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_cashmoveClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_cashmoveImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_cashmoveFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_cashmoveClientServiceImpl implements Ilunch_cashmoveClientService {

    lunch_cashmoveFeignClient lunch_cashmoveFeignClient;

    @Autowired
    public lunch_cashmoveClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_cashmoveFeignClient = nameBuilder.target(lunch_cashmoveFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_cashmoveFeignClient = nameBuilder.target(lunch_cashmoveFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_cashmove createModel() {
		return new lunch_cashmoveImpl();
	}


    public void createBatch(List<Ilunch_cashmove> lunch_cashmoves){
        if(lunch_cashmoves!=null){
            List<lunch_cashmoveImpl> list = new ArrayList<lunch_cashmoveImpl>();
            for(Ilunch_cashmove ilunch_cashmove :lunch_cashmoves){
                list.add((lunch_cashmoveImpl)ilunch_cashmove) ;
            }
            lunch_cashmoveFeignClient.createBatch(list) ;
        }
    }


    public Page<Ilunch_cashmove> fetchDefault(SearchContext context){
        Page<lunch_cashmoveImpl> page = this.lunch_cashmoveFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ilunch_cashmove> lunch_cashmoves){
        if(lunch_cashmoves!=null){
            List<lunch_cashmoveImpl> list = new ArrayList<lunch_cashmoveImpl>();
            for(Ilunch_cashmove ilunch_cashmove :lunch_cashmoves){
                list.add((lunch_cashmoveImpl)ilunch_cashmove) ;
            }
            lunch_cashmoveFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ilunch_cashmove> lunch_cashmoves){
        if(lunch_cashmoves!=null){
            List<lunch_cashmoveImpl> list = new ArrayList<lunch_cashmoveImpl>();
            for(Ilunch_cashmove ilunch_cashmove :lunch_cashmoves){
                list.add((lunch_cashmoveImpl)ilunch_cashmove) ;
            }
            lunch_cashmoveFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ilunch_cashmove lunch_cashmove){
        lunch_cashmoveFeignClient.remove(lunch_cashmove.getId()) ;
    }


    public void create(Ilunch_cashmove lunch_cashmove){
        Ilunch_cashmove clientModel = lunch_cashmoveFeignClient.create((lunch_cashmoveImpl)lunch_cashmove) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_cashmove.getClass(), false);
        copier.copy(clientModel, lunch_cashmove, null);
    }


    public void update(Ilunch_cashmove lunch_cashmove){
        Ilunch_cashmove clientModel = lunch_cashmoveFeignClient.update(lunch_cashmove.getId(),(lunch_cashmoveImpl)lunch_cashmove) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_cashmove.getClass(), false);
        copier.copy(clientModel, lunch_cashmove, null);
    }


    public void get(Ilunch_cashmove lunch_cashmove){
        Ilunch_cashmove clientModel = lunch_cashmoveFeignClient.get(lunch_cashmove.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_cashmove.getClass(), false);
        copier.copy(clientModel, lunch_cashmove, null);
    }


    public Page<Ilunch_cashmove> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_cashmove lunch_cashmove){
        Ilunch_cashmove clientModel = lunch_cashmoveFeignClient.getDraft(lunch_cashmove.getId(),(lunch_cashmoveImpl)lunch_cashmove) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_cashmove.getClass(), false);
        copier.copy(clientModel, lunch_cashmove, null);
    }



}


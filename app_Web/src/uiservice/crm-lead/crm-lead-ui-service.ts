import Crm_leadUIServiceBase from './crm-lead-ui-service-base';

/**
 * 线索/商机UI服务对象
 *
 * @export
 * @class Crm_leadUIService
 */
export default class Crm_leadUIService extends Crm_leadUIServiceBase {

    /**
     * Creates an instance of  Crm_leadUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_leadUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
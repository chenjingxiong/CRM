import Account_paymentUIServiceBase from './account-payment-ui-service-base';

/**
 * 付款UI服务对象
 *
 * @export
 * @class Account_paymentUIService
 */
export default class Account_paymentUIService extends Account_paymentUIServiceBase {

    /**
     * Creates an instance of  Account_paymentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_paymentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
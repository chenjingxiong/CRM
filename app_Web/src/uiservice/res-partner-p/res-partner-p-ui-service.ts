import Res_partner_pUIServiceBase from './res-partner-p-ui-service-base';

/**
 * 客户UI服务对象
 *
 * @export
 * @class Res_partner_pUIService
 */
export default class Res_partner_pUIService extends Res_partner_pUIServiceBase {

    /**
     * Creates an instance of  Res_partner_pUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_pUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
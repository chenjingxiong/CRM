import Account_register_paymentsUIServiceBase from './account-register-payments-ui-service-base';

/**
 * 登记付款UI服务对象
 *
 * @export
 * @class Account_register_paymentsUIService
 */
export default class Account_register_paymentsUIService extends Account_register_paymentsUIServiceBase {

    /**
     * Creates an instance of  Account_register_paymentsUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_register_paymentsUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
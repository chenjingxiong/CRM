import Sale_orderUIServiceBase from './sale-order-ui-service-base';

/**
 * 销售订单UI服务对象
 *
 * @export
 * @class Sale_orderUIService
 */
export default class Sale_orderUIService extends Sale_orderUIServiceBase {

    /**
     * Creates an instance of  Sale_orderUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_orderUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
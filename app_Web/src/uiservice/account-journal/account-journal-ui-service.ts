import Account_journalUIServiceBase from './account-journal-ui-service-base';

/**
 * 日记账UI服务对象
 *
 * @export
 * @class Account_journalUIService
 */
export default class Account_journalUIService extends Account_journalUIServiceBase {

    /**
     * Creates an instance of  Account_journalUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_journalUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
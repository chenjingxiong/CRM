/**
 * BillDetailForm 部件模型
 *
 * @export
 * @class BillDetailFormModel
 */
export default class BillDetailFormModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof BillDetailFormModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'product_id_text',
        prop: 'product_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price_unit',
        prop: 'price_unit',
        dataType: 'FLOAT',
      },
      {
        name: 'quantity',
        prop: 'quantity',
        dataType: 'FLOAT',
      },
      {
        name: 'uom_id_text',
        prop: 'uom_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price_total',
        prop: 'price_total',
        dataType: 'DECIMAL',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_id',
        prop: 'product_id',
        dataType: 'PICKUP',
      },
      {
        name: 'uom_id',
        prop: 'uom_id',
        dataType: 'PICKUP',
      },
      {
        name: 'account_invoice_line',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: '__last_update',
      },
      {
        name: 'property_stock_valuation_account_id',
      },
      {
        name: 'child_id',
      },
      {
        name: 'product_count',
      },
      {
        name: 'route_ids',
      },
      {
        name: 'name',
      },
      {
        name: 'display_name',
      },
      {
        name: 'property_valuation',
      },
      {
        name: 'parent_path',
      },
      {
        name: 'product_category',
        prop: 'id',
      },
      {
        name: 'complete_name',
      },
      {
        name: 'property_account_creditor_price_difference_categ',
      },
      {
        name: 'property_stock_account_output_categ_id',
      },
      {
        name: 'create_date',
      },
      {
        name: 'total_route_ids',
      },
      {
        name: 'property_cost_method',
      },
      {
        name: 'property_account_income_categ_id',
      },
      {
        name: 'property_account_expense_categ_id',
      },
      {
        name: 'property_stock_journal',
      },
      {
        name: 'write_date',
      },
      {
        name: 'property_stock_account_input_categ_id',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'parent_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'removal_strategy_id_text',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'removal_strategy_id',
      },
      {
        name: 'write_uid',
      },
    ]
  }


}
import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_partner_pService from '@/service/res-partner-p/res-partner-p-service';
import PMainTabExpViewtabexppanelModel from './pmain-tab-exp-viewtabexppanel-tabexppanel-model';


/**
 * PMainTabExpViewtabexppanel 部件服务对象
 *
 * @export
 * @class PMainTabExpViewtabexppanelService
 */
export default class PMainTabExpViewtabexppanelService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {Res_partner_pService}
     * @memberof PMainTabExpViewtabexppanelService
     */
    public appEntityService: Res_partner_pService = new Res_partner_pService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof PMainTabExpViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of PMainTabExpViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof PMainTabExpViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new PMainTabExpViewtabexppanelModel();
    }

    
}
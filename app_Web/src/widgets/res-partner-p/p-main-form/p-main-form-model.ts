/**
 * P_Main 部件模型
 *
 * @export
 * @class P_MainModel
 */
export default class P_MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof P_MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'display_name',
        prop: 'display_name',
        dataType: 'TEXT',
      },
      {
        name: 'parent_name',
        prop: 'parent_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'res_partner_p',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
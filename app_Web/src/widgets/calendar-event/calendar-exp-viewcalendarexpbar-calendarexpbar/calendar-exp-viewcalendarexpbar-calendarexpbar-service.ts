import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Calendar_eventService from '@/service/calendar-event/calendar-event-service';
import CalendarExpViewcalendarexpbarModel from './calendar-exp-viewcalendarexpbar-calendarexpbar-model';


/**
 * CalendarExpViewcalendarexpbar 部件服务对象
 *
 * @export
 * @class CalendarExpViewcalendarexpbarService
 */
export default class CalendarExpViewcalendarexpbarService extends ControlService {

    /**
     * 活动服务对象
     *
     * @type {Calendar_eventService}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public appEntityService: Calendar_eventService = new Calendar_eventService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof CalendarExpViewcalendarexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of CalendarExpViewcalendarexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof CalendarExpViewcalendarexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new CalendarExpViewcalendarexpbarModel();
    }

}
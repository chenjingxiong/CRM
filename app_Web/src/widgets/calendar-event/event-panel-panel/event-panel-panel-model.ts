/**
 * EventPanel 部件模型
 *
 * @export
 * @class EventPanelModel
 */
export default class EventPanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof EventPanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'location',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'description',
      },
      {
        name: 'recurrent_id_date',
      },
      {
        name: 'sa',
      },
      {
        name: 'display_name',
      },
      {
        name: 'recurrency',
      },
      {
        name: 'count',
      },
      {
        name: 'fr',
      },
      {
        name: 'rrule',
      },
      {
        name: 'month_by',
      },
      {
        name: 'week_list',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'stop',
      },
      {
        name: 'res_id',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'recurrent_id',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'end_type',
      },
      {
        name: 'attendee_ids',
      },
      {
        name: 'attendee_status',
      },
      {
        name: 'active',
      },
      {
        name: 'rrule_type',
      },
      {
        name: 'interval',
      },
      {
        name: 'privacy',
      },
      {
        name: 'duration',
      },
      {
        name: 'start_datetime',
      },
      {
        name: 'is_attendee',
      },
      {
        name: 'start_date',
      },
      {
        name: 'mo',
      },
      {
        name: 'state',
      },
      {
        name: '__last_update',
      },
      {
        name: 'we',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'display_time',
      },
      {
        name: 'display_start',
      },
      {
        name: 'stop_date',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'final_date',
      },
      {
        name: 'res_model_id',
      },
      {
        name: 'is_highlighted',
      },
      {
        name: 'calendar_event',
        prop: 'id',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'write_date',
      },
      {
        name: 'stop_datetime',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'res_model',
      },
      {
        name: 'tu',
      },
      {
        name: 'create_date',
      },
      {
        name: 'show_as',
      },
      {
        name: 'categ_ids',
      },
      {
        name: 'alarm_ids',
      },
      {
        name: 'th',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'byday',
      },
      {
        name: 'day',
      },
      {
        name: 'start',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'partner_ids',
      },
      {
        name: 'allday',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'su',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'name',
      },
      {
        name: 'opportunity_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'applicant_id_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'applicant_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'user_id',
      },
      {
        name: 'opportunity_id',
      },
    ]
  }


}
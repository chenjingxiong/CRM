import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ToDoOrders 部件服务对象
 *
 * @export
 * @class ToDoOrdersService
 */
export default class ToDoOrdersService extends ControlService {
}
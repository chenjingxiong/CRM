import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DashboardView9dashboard_container4 部件服务对象
 *
 * @export
 * @class DashboardView9dashboard_container4Service
 */
export default class DashboardView9dashboard_container4Service extends ControlService {
}
import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_partnerService from '@/service/res-partner/res-partner-service';
import PieModel from './pie-chart-model';


/**
 * Pie 部件服务对象
 *
 * @export
 * @class PieService
 */
export default class PieService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {Res_partnerService}
     * @memberof PieService
     */
    public appEntityService: Res_partnerService = new Res_partnerService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof PieService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of PieService.
     * 
     * @param {*} [opts={}]
     * @memberof PieService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new PieModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PieService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleSeries(response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
    /**
     * 生成图表数据
     *
     * @param {*} response
     * @memberof PieService
     */
    public handleSeries(response: any) {
        let chartOption:any = {};
        let catalogFields: any = ["city",];
        let valueFields: any = [[ "company_id", "公司" ],];
        // 数据按分类属性分组处理
        let xFields:any = [];
        let yFields:any = [];
        valueFields.forEach((field: any,index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                let num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
            }else{
                xFields.push(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        let series: any = [];
        valueFields.forEach((field: any,index: number) => {
            let yData: any = [];
             xFields.forEach((item:any, num: number) => {
                yData.push({value: yFields[index][num], name: item});
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
              name:field[1],
              type:"pie",
              data:yData,
              top:"40px",
              left: (100/valueFields.length)*index+"%",
              right: (100/valueFields.length)*(valueFields.length-index-1)+"%",
              animationType: 'scale',
              animationEasing: 'elasticOut',
              animationDelay: function (idx: any) {
                  return Math.random() * 200;
              }
            });
        });
        chartOption.series = series;
        response.data = chartOption;
    }
}
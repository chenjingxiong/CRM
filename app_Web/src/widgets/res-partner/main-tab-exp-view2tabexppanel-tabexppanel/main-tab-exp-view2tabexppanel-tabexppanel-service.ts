import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_partnerService from '@/service/res-partner/res-partner-service';
import MainTabExpView2tabexppanelModel from './main-tab-exp-view2tabexppanel-tabexppanel-model';


/**
 * MainTabExpView2tabexppanel 部件服务对象
 *
 * @export
 * @class MainTabExpView2tabexppanelService
 */
export default class MainTabExpView2tabexppanelService extends ControlService {

    /**
     * 客户服务对象
     *
     * @type {Res_partnerService}
     * @memberof MainTabExpView2tabexppanelService
     */
    public appEntityService: Res_partnerService = new Res_partnerService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MainTabExpView2tabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MainTabExpView2tabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MainTabExpView2tabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MainTabExpView2tabexppanelModel();
    }

    
}
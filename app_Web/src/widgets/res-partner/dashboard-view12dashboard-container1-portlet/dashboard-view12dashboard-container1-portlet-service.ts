import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DashboardView12dashboard_container1 部件服务对象
 *
 * @export
 * @class DashboardView12dashboard_container1Service
 */
export default class DashboardView12dashboard_container1Service extends ControlService {
}
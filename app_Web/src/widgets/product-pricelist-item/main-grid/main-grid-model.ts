/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_tmpl_id',
          prop: 'product_tmpl_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'base_pricelist_id',
          prop: 'base_pricelist_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
        },
        {
          name: 'pricelist_id',
          prop: 'pricelist_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'product_id_text',
          prop: 'product_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'write_date',
          prop: 'write_date',
          dataType: 'DATETIME',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'TEXT',
        },
        {
          name: 'write_uid_text',
          prop: 'write_uid_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'categ_id',
          prop: 'categ_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_pricelist_item',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
/**
 * CurWeek 部件模型
 *
 * @export
 * @class CurWeekModel
 */
export default class CurWeekModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof CurWeekListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'planned_revenue',
				prop: 'planned_revenue',
				dataType: 'DECIMAL',
			},
			{
				name: 'name',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}
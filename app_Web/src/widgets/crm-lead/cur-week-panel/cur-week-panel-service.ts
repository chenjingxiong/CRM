import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CurWeek 部件服务对象
 *
 * @export
 * @class CurWeekService
 */
export default class CurWeekService extends ControlService {
}
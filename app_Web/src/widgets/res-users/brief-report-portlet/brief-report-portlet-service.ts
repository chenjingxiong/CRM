import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * BriefReport 部件服务对象
 *
 * @export
 * @class BriefReportService
 */
export default class BriefReportService extends ControlService {
}
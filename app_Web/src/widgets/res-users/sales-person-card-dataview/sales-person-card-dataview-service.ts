import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_usersService from '@/service/res-users/res-users-service';
import SalesPersonCardModel from './sales-person-card-dataview-model';


/**
 * SalesPersonCard 部件服务对象
 *
 * @export
 * @class SalesPersonCardService
 */
export default class SalesPersonCardService extends ControlService {

    /**
     * 用户服务对象
     *
     * @type {Res_usersService}
     * @memberof SalesPersonCardService
     */
    public appEntityService: Res_usersService = new Res_usersService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof SalesPersonCardService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of SalesPersonCardService.
     * 
     * @param {*} [opts={}]
     * @memberof SalesPersonCardService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new SalesPersonCardModel();
    }




    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesPersonCardService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof SalesPersonCardService
     */
    @Errorlog
    public delete(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.remove(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }



}
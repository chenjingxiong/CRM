import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TeamUserList 部件服务对象
 *
 * @export
 * @class TeamUserListService
 */
export default class TeamUserListService extends ControlService {
}
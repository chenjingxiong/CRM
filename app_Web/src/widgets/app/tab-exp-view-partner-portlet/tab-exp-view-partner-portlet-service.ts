import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * TabExpView_Partner 部件服务对象
 *
 * @export
 * @class TabExpView_PartnerService
 */
export default class TabExpView_PartnerService extends ControlService {
}
import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * OrderBar 部件服务对象
 *
 * @export
 * @class OrderBarService
 */
export default class OrderBarService extends ControlService {
}
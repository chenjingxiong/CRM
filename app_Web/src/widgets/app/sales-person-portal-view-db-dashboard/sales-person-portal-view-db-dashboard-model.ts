/**
 * SalesPersonPortalView_db 部件模型
 *
 * @export
 * @class SalesPersonPortalView_dbModel
 */
export default class SalesPersonPortalView_dbModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof SalesPersonPortalView_dbModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}
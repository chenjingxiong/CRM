import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MainView9 部件服务对象
 *
 * @export
 * @class MainView9Service
 */
export default class MainView9Service extends ControlService {
}
import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ActivityCalendar 部件服务对象
 *
 * @export
 * @class ActivityCalendarService
 */
export default class ActivityCalendarService extends ControlService {
}
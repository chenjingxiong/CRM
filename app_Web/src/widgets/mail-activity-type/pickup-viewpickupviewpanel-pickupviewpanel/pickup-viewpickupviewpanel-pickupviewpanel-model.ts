/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'res_model_change',
      },
      {
        name: 'summary',
      },
      {
        name: 'force_next',
      },
      {
        name: 'sequence',
      },
      {
        name: 'mail_activity_type',
        prop: 'id',
      },
      {
        name: 'category',
      },
      {
        name: 'icon',
      },
      {
        name: 'delay_count',
      },
      {
        name: 'previous_type_ids',
      },
      {
        name: 'display_name',
      },
      {
        name: 'active',
      },
      {
        name: 'delay_from',
      },
      {
        name: 'write_date',
      },
      {
        name: 'mail_template_ids',
      },
      {
        name: 'name',
      },
      {
        name: 'create_date',
      },
      {
        name: 'res_model_id',
      },
      {
        name: '__last_update',
      },
      {
        name: 'decoration_type',
      },
      {
        name: 'next_type_ids',
      },
      {
        name: 'delay_unit',
      },
      {
        name: 'initial_res_model_id',
      },
      {
        name: 'default_next_type_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'default_next_type_id',
      },
    ]
  }


}
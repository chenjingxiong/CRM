/**
 * ToDoOrders 部件模型
 *
 * @export
 * @class ToDoOrdersModel
 */
export default class ToDoOrdersModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ToDoOrdersListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'client_order_ref',
				prop: 'client_order_ref',
				dataType: 'TEXT',
			},
			{
				name: 'amount_total',
				prop: 'amount_total',
				dataType: 'DECIMAL',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}
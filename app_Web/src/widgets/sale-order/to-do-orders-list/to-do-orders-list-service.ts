import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Sale_orderService from '@/service/sale-order/sale-order-service';
import ToDoOrdersModel from './to-do-orders-list-model';


/**
 * ToDoOrders 部件服务对象
 *
 * @export
 * @class ToDoOrdersService
 */
export default class ToDoOrdersService extends ControlService {

    /**
     * 销售订单服务对象
     *
     * @type {Sale_orderService}
     * @memberof ToDoOrdersService
     */
    public appEntityService: Sale_orderService = new Sale_orderService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof ToDoOrdersService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of ToDoOrdersService.
     * 
     * @param {*} [opts={}]
     * @memberof ToDoOrdersService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new ToDoOrdersModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ToDoOrdersService
     */
    @Errorlog
    public search(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ToDoOrdersService
     */
    @Errorlog
    public delete(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.remove(Context,Data , isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

}
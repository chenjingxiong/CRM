/**
 * OrderBar 部件模型
 *
 * @export
 * @class OrderBarModel
 */
export default class OrderBarModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof OrderBarPortlet_OrderBar_chartMode
	 */
	public getDataItems(): any[] {
		return [
      {
        name:'query',
        prop:'query'
      },
		]
	}

}
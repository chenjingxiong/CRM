import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Sale_orderService from '@/service/sale-order/sale-order-service';
import OrderBarModel from './order-bar-chart-model';


/**
 * OrderBar 部件服务对象
 *
 * @export
 * @class OrderBarService
 */
export default class OrderBarService extends ControlService {

    /**
     * 销售订单服务对象
     *
     * @type {Sale_orderService}
     * @memberof OrderBarService
     */
    public appEntityService: Sale_orderService = new Sale_orderService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof OrderBarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of OrderBarService.
     * 
     * @param {*} [opts={}]
     * @memberof OrderBarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new OrderBarModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof OrderBarService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleSeries(response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
    /**
     * 生成图表数据
     *
     * @param {*} response
     * @memberof OrderBarService
     */
    public handleSeries(response: any) {
        let chartOption:any = {};
        let catalogFields: any = ["user_id_text",];
        let valueFields: any = [[ "amount_total", "总计" ],];
        // 数据按分类属性分组处理
        let xFields:any = [];
        let yFields:any = [];
        valueFields.forEach((field: any,index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                let num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
            }else{
                xFields.push(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        chartOption.xAxis = { data: xFields };
        let series: any = [];
        valueFields.forEach((field: any,index: number) => {
            let yData: any = [];
             xFields.forEach((item:any, num: number) => {
                yData.push(yFields[index][num]);
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
              name:field[1],
              type:"bar",
              data:yData,
            });
        });
        chartOption.series = series;
        response.data = chartOption;
    }
}
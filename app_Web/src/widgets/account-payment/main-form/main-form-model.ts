/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'partner_id_text',
        prop: 'partner_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'DECIMAL',
      },
      {
        name: 'payment_date',
        prop: 'payment_date',
        dataType: 'DATE',
      },
      {
        name: 'communication',
        prop: 'communication',
        dataType: 'TEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'partner_id',
        prop: 'partner_id',
        dataType: 'PICKUP',
      },
      {
        name: 'account_payment',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
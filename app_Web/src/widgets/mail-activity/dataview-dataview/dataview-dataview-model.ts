/**
 * DataView 部件模型
 *
 * @export
 * @class DataViewModel
 */
export default class DataViewModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof DataViewDataViewMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfmajortext',
				prop: 'display_name',
				dataType: 'TEXT',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},

			{
				name: 'mail_activity',
				prop: 'id',
				dataType: 'FONTKEY',
			},



      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      {
        name:'srfparentdata',
        prop:'srfparentdata'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'display_name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'activity_type_id_text',
        prop: 'activity_type_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_deadline',
        prop: 'date_deadline',
        dataType: 'DATE',
      },
      {
        name: 'summary',
        prop: 'summary',
        dataType: 'TEXT',
      },
      {
        name: 'user_id_text',
        prop: 'user_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'note',
        prop: 'note',
        dataType: 'HTMLTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'mail_activity',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'summary',
          prop: 'summary',
          dataType: 'TEXT',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_user_id',
          prop: 'create_user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'display_name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'previous_activity_type_id',
          prop: 'previous_activity_type_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'activity_type_id_text',
          prop: 'activity_type_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'note_id',
          prop: 'note_id',
          dataType: 'PICKUP',
        },
        {
          name: 'activity_type_id',
          prop: 'activity_type_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'calendar_event_id',
          prop: 'calendar_event_id',
          dataType: 'PICKUP',
        },
        {
          name: 'recommended_activity_type_id',
          prop: 'recommended_activity_type_id',
          dataType: 'PICKUP',
        },
        {
          name: 'date_deadline',
          prop: 'date_deadline',
          dataType: 'DATE',
        },
        {
          name: 'mail_activity',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
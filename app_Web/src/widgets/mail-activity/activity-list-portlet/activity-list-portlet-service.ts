import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * ActivityList 部件服务对象
 *
 * @export
 * @class ActivityListService
 */
export default class ActivityListService extends ControlService {
}

export default {
  fields: {
    image: '图像',
    type: '地址类型',
    color: '颜色索引',
    payment_token_ids: '付款令牌',
    invoice_ids: '发票',
    meeting_count: '#会议',
    supplier_invoice_count: '＃供应商账单',
    company_name: '公司名称',
    website_published: '在当前网站显示',
    last_time_entries_checked: '最近的发票和付款匹配时间',
    message_unread: '未读消息',
    trust: '对此债务人的信任度',
    ibizfunction: '工作岗位',
    total_invoiced: '已开票总计',
    pos_order_count: '销售点订单计数',
    contact_address: '完整地址',
    invoice_warn: '发票',
    bank_ids: '银行',
    signup_expiration: '注册到期',
    purchase_order_count: '采购订单数',
    has_unreconciled_entries: '有未核销的分录',
    category_id: '标签',
    website_description: '网站业务伙伴的详细说明',
    message_main_attachment_id: '附件',
    meeting_ids: '会议',
    employee: '员工',
    display_name: '显示名称',
    child_ids: '联系人',
    website_meta_description: '网站元说明',
    is_blacklisted: '黑名单',
    property_product_pricelist: '价格表',
    activity_date_deadline: '下一活动截止日期',
    activity_type_id: '下一活动类型',
    signup_token: '注册令牌 Token',
    ref_company_ids: '公司是指业务伙伴',
    is_company: '公司',
    phone: '电话',
    create_date: '创建时间',
    tz: '时区',
    event_count: '活动',
    message_has_error: '消息递送错误',
    calendar_last_notif_ack: '最后的提醒已经标志为已读',
    message_channel_ids: '关注者(渠道)',
    signup_type: '注册令牌（Token）类型',
    email_formatted: '格式化的邮件',
    website_message_ids: '网站消息',
    partner_share: '共享合作伙伴',
    street2: '街道 2',
    debit: '应付总计',
    payment_token_count: '付款令牌计数',
    ref: '内部参考',
    partner_gid: '公司数据库ID',
    signup_valid: '注册令牌（ Token  ）是有效的',
    website_meta_og_img: '网站opengraph图像',
    image_small: '小尺寸图像',
    bank_account_count: '银行',
    street: '街道',
    sale_warn: '销售警告',
    message_bounce: '退回',
    message_needaction_counter: '操作次数',
    message_follower_ids: '关注者',
    opportunity_count: '商机',
    date: '日期',
    __last_update: '最后修改日',
    message_partner_ids: '关注者(业务伙伴)',
    self: '自己',
    im_status: 'IM的状态',
    customer: '客户',
    write_date: '最后更新时间',
    message_has_error_counter: '错误个数',
    invoice_warn_msg: '发票消息',
    message_needaction: '前置操作',
    picking_warn: '库存拣货',
    contract_ids: '客户合同',
    currency_id: '币种',
    website: '网站',
    mobile: '手机',
    message_attachment_count: '附件数量',
    city: '城市',
    property_payment_term_id: '客户付款条款',
    user_ids: '用户',
    website_meta_keywords: '网站meta关键词',
    channel_ids: '渠道',
    purchase_warn: '采购订单',
    journal_item_count: '日记账项目',
    supplier: '供应商',
    property_stock_supplier: '供应商位置',
    property_account_payable_id: '应付账款',
    website_short_description: '网站业务伙伴简介',
    sale_warn_msg: '销售订单消息',
    credit: '应收总计',
    activity_state: '活动状态',
    activity_ids: '活动',
    message_is_follower: '关注者',
    name: '名称',
    vat: '税号',
    property_supplier_payment_term_id: '供应商付款条款',
    property_stock_customer: '客户位置',
    comment: '便签',
    task_ids: '任务',
    message_unread_counter: '未读消息计数器',
    email: 'EMail',
    purchase_warn_msg: '采购订单消息',
    website_meta_title: '网站meta标题',
    zip: '邮政编码',
    tz_offset: '时区偏移',
    company_type: '公司类别',
    activity_summary: '下一个活动摘要',
    task_count: '# 任务',
    credit_limit: '信用额度',
    property_account_receivable_id: '应收账款',
    property_purchase_currency_id: '供应商货币',
    picking_warn_msg: '库存拣货单消息',
    id: 'ID',
    signup_url: '注册网址',
    lang: '语言',
    message_ids: '消息',
    property_account_position_id: '税科目调整',
    website_id: '登记网站',
    active: '有效',
    barcode: '条码',
    is_published: '已发布',
    activity_user_id: '责任用户',
    sale_order_count: '销售订单个数',
    image_medium: '中等尺寸图像',
    additional_info: '附加信息',
    opportunity_ids: '商机',
    contracts_count: '合同统计',
    debit_limit: '应付限额',
    website_url: '网站网址',
    sale_order_ids: '销售订单',
    last_website_so_id: '最近的在线销售订单',
    is_seo_optimized: 'SEO优化',
    commercial_company_name: '公司名称实体',
    write_uid_text: '最后更新者',
    title_text: '称谓',
    company_id_text: '公司',
    country_id_text: '国家/地区',
    state_id_text: '省/ 州',
    commercial_partner_id_text: '商业实体',
    parent_name: '上级名称',
    user_id_text: '销售员',
    create_uid_text: '创建人',
    industry_id_text: '工业',
    team_id_text: '销售团队',
    team_id: '销售团队',
    state_id: '省/ 州',
    user_id: '销售员',
    create_uid: '创建人',
    parent_id: '关联公司',
    title: '称谓',
    write_uid: '最后更新者',
    commercial_partner_id: '商业实体',
    industry_id: '工业',
    company_id: '公司',
    country_id: '国家/地区',
  },
	views: {
		treeexpview: {
			caption: "全部客户",
      title: '全部客户',
		},
		treeview: {
			caption: "客户",
      title: '客户',
		},
		dashboardview12: {
			caption: "客户",
      title: '客户',
		},
		dashboardview9: {
			caption: "客户",
      title: '客户',
		},
		pickupview: {
			caption: "客户",
      title: '客户',
		},
		dashboardview11: {
			caption: "客户",
      title: '客户',
		},
		mainview9: {
			caption: "客户信息",
      title: '客户信息',
		},
		contractseditview: {
			caption: "客户",
      title: '客户',
		},
		pickupgridview: {
			caption: "客户",
      title: '客户',
		},
		maintabexpview2: {
			caption: "客户信息",
      title: '客户信息',
		},
		mainview9_editmode: {
			caption: "客户编辑",
      title: '客户编辑',
		},
		salespersontreeexpview: {
			caption: "团队客户",
      title: '团队客户',
		},
		dashboardview10: {
			caption: "客户",
      title: '客户',
		},
		maintabexpview: {
			caption: "客户",
      title: '客户',
		},
		gridview: {
			caption: "客户",
      title: '客户',
		},
		gridview9: {
			caption: "客户联系人",
      title: '客户联系人',
		},
		dataviewexpview: {
			caption: "客户",
      title: '客户',
		},
		contactsgridview: {
			caption: "客户",
      title: '客户',
		},
		customercardview: {
			caption: "客户",
      title: '客户',
		},
		editview: {
			caption: "客户",
      title: '客户',
		},
		contactscardview: {
			caption: "联系人",
      title: '联系人',
		},
	},
	main2_form: {
		details: {
			grouppanel5: "分组面板", 
			grouppanel6: "分组面板", 
			grouppanel1: "客户", 
			grouppanel2: "地址", 
			grouppanel3: "联系方式", 
			grouppanel4: "分组面板", 
			formpage1: "客户信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			image: "", 
			name: "", 
			is_company: "", 
			city: "", 
			zip: "", 
			street: "", 
			street2: "", 
			phone: "", 
			mobile: "", 
			email: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			grouppanel4: "分组面板", 
			grouppanel6: "分组面板", 
			grouppanel1: "基本信息", 
			grouppanel2: "地址", 
			grouppanel3: "联系方式", 
			formpage1: "客户信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			image: "", 
			name: "", 
			is_company: "", 
			city: "地址", 
			zip: "", 
			street: "", 
			street2: "", 
			phone: "电话", 
			mobile: "手机", 
			email: "EMail", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	contractmain_form: {
		details: {
			group1: "联系人基本信息", 
			grouppanel1: "地址", 
			grouppanel2: "联系人信息", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "名称", 
			is_company: "公司", 
			company_id_text: "公司", 
			country_id_text: "国家/地区", 
			state_id_text: "省/ 州", 
			city: "城市", 
			zip: "邮政编码", 
			street: "街道", 
			street2: "街道 2", 
			ibizfunction: "工作岗位", 
			email: "EMail", 
			phone: "电话", 
			mobile: "手机", 
			id: "ID", 
			state_id: "省/ 州", 
			company_id: "公司", 
			country_id: "国家/地区", 
		},
		uiactions: {
		},
	},
	p_main_grid: {
		columns: {
			name: "名称",
			contact_address: "完整地址",
			zip: "邮政编码",
			phone: "电话",
			mobile: "手机",
			email: "EMail",
			write_uid_text: "最后更新者",
			write_date: "最后更新时间",
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			name: "名称",
			phone: "电话",
			mobile: "手机",
			email: "EMail",
			zip: "邮政编码",
			contact_address: "完整地址",
			uagridcolumn1: "操作",
		},
		uiactions: {
			edit: "Edit",
			copy: "Copy",
			remove: "Remove",
		},
	},
	main2_grid: {
		columns: {
			name: "姓名",
			mobile: "手机",
			email: "EMail",
			parent_name: "客户",
			write_uid: "最后更新者",
			write_date: "最后更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	mainview9toolbar_toolbar: {
		deuiaction2: {
			caption: "编辑",
			tip: "编辑",
		},
	},
	mainview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem2: {
			caption: "关闭",
			tip: "关闭",
		},
	},
	dataviewexpviewdataviewexpbar_toolbar_toolbar: {
		deuiaction1: {
			caption: "新建",
			tip: "新建",
		},
	},
	contactscardviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
	},
	customercardviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	contactsgridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem23: {
			caption: "导入",
			tip: "导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	contractseditviewtoolbar_toolbar: {
	},
	gridview9toolbar_toolbar: {
	},
	teamusrtree_treeview: {
		nodes: {
			root: '默认根节点',
		},
		uiactions: {
		},
	},
	salespersontree_treeview: {
		nodes: {
			root: '默认根节点',
		},
		uiactions: {
		},
	},
};
export default {
  fields: {
    display_name: '显示名称',
    mail_template_ids: '邮件模板',
    automated: '自动活动',
    res_name: '文档名称',
    state: '状态',
    id: 'ID',
    create_date: '创建时间',
    write_date: '最后更新时间',
    date_deadline: '到期时间',
    __last_update: '最后修改日',
    summary: '摘要',
    has_recommended_activities: '下一活动可用',
    res_id: '相关文档编号',
    note: '备注',
    feedback: '反馈',
    res_model_id: '文档模型',
    res_model: '相关的文档模型',
    user_id_text: '分派给',
    activity_category: '类别',
    activity_type_id_text: '活动',
    previous_activity_type_id_text: '前一活动类型',
    note_id_text: '相关便签',
    create_user_id_text: '建立者',
    icon: '图标',
    create_uid_text: '创建人',
    activity_decoration: '排版类型',
    write_uid_text: '最后更新者',
    force_next: '自动安排下一个活动',
    recommended_activity_type_id_text: '推荐的活动类型',
    calendar_event_id_text: '日历会议',
    recommended_activity_type_id: '推荐的活动类型',
    activity_type_id: '活动',
    create_user_id: '建立者',
    create_uid: '创建人',
    user_id: '分派给',
    note_id: '相关便签',
    previous_activity_type_id: '前一活动类型',
    calendar_event_id: '日历会议',
    write_uid: '最后更新者',
  },
	views: {
		listview: {
			caption: '活动',
      title: '活动',
		},
		calendarexpview: {
			caption: '活动',
      title: '活动',
		},
		calendarview: {
			caption: '活动',
      title: '活动',
		},
		mainview9: {
			caption: '活动',
      title: '活动',
		},
		editview: {
			caption: '活动',
      title: '活动',
		},
		aclistview: {
			caption: '活动',
      title: '活动',
		},
		gridview9: {
			caption: '商机活动安排',
      title: '商机活动安排',
		},
		mainview9_editmode: {
			caption: '活动',
      title: '活动',
		},
	},
	main2_form: {
		details: {
			group1: '基本信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '显示名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			activity_type_id_text: '活动', 
			date_deadline: '到期时间', 
			summary: '摘要', 
			user_id_text: '分派给', 
			note: '备注', 
			id: 'ID', 
			activity_type_id: '活动', 
			user_id: '分派给', 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: '基本信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '显示名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			activity_type_id_text: '活动', 
			date_deadline: '到期时间', 
			summary: '摘要', 
			user_id_text: '分派给', 
			note: '备注', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			summary: '摘要',
			activity_type_id_text: '活动',
			date_deadline: '到期时间',
			user_id_text: '分派给',
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
		},
		uiactions: {
		},
	},
	mainview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem2: {
			caption: '关闭',
			tip: '关闭',
		},
	},
	mainview9toolbar_toolbar: {
		deuiaction2: {
			caption: '编辑',
			tip: '编辑',
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: '保存',
			tip: '保存',
		},
		tbitem4: {
			caption: '保存并新建',
			tip: '保存并新建',
		},
		tbitem5: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem6: {
			caption: '-',
			tip: '',
		},
		tbitem7: {
			caption: '删除并关闭',
			tip: '删除并关闭',
		},
		tbitem8: {
			caption: '-',
			tip: '',
		},
		tbitem12: {
			caption: '新建',
			tip: '新建',
		},
		tbitem13: {
			caption: '-',
			tip: '',
		},
		tbitem14: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem16: {
			caption: '-',
			tip: '',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: '第一个记录',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: '上一个记录',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: '下一个记录',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: '最后一个记录',
		},
		tbitem21: {
			caption: '-',
			tip: '',
		},
		tbitem22: {
			caption: '帮助',
			tip: '帮助',
		},
	},
	aclistviewtoolbar_toolbar: {
	},
	gridview9toolbar_toolbar: {
	},
};
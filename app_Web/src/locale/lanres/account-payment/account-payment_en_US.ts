
export default {
  fields: {
    invoice_ids: '发票',
    message_needaction: '需要采取行动',
    display_name: '显示名称',
    message_needaction_counter: '操作编号',
    payment_type: '付款类型',
    message_channel_ids: '关注者(渠道)',
    has_invoices: '有发票',
    payment_date: '付款日期',
    multi: '多',
    message_attachment_count: '附件数量',
    name: '名称',
    move_reconciled: '凭证已核销',
    destination_account_id: '目标账户',
    message_has_error: '消息递送错误',
    message_main_attachment_id: '附件',
    message_unread: '未读消息',
    payment_difference: '付款差异',
    message_follower_ids: '关注者',
    message_has_error_counter: '需要一个动作消息的编码',
    payment_difference_handling: '付款差异处理',
    reconciled_invoice_ids: '已核销的发票',
    payment_reference: '付款参考',
    writeoff_label: '日记账项目标签',
    move_line_ids: '分录行',
    website_message_ids: '网站信息',
    write_date: '最后更新时间',
    amount: '付款金额',
    show_partner_bank_account: '显示合作伙伴银行账户',
    create_date: '创建时间',
    partner_type: '业务伙伴类型',
    message_ids: '消息',
    __last_update: '最后修改日',
    communication: '备忘',
    state: '状态',
    id: 'ID',
    message_partner_ids: '关注者(业务伙伴)',
    message_is_follower: '是关注者',
    hide_payment_method: '隐藏付款方式',
    move_name: '日记账分录名称',
    message_unread_counter: '未读消息计数器',
    destination_journal_id_text: '转账到',
    payment_method_code: '代码',
    write_uid_text: '最后更新人',
    journal_id_text: '付款日记账',
    create_uid_text: '创建人',
    writeoff_account_id_text: '差异科目',
    currency_id_text: '币种',
    payment_method_id_text: '付款方法类型',
    payment_token_id_text: '保存的付款令牌',
    company_id: '公司',
    partner_id_text: '业务伙伴',
    write_uid: '最后更新人',
    payment_token_id: '保存的付款令牌',
    payment_transaction_id: '付款交易',
    writeoff_account_id: '差异科目',
    partner_bank_account_id: '收款银行账号',
    create_uid: '创建人',
    payment_method_id: '付款方法类型',
    destination_journal_id: '转账到',
    partner_id: '业务伙伴',
    journal_id: '付款日记账',
    currency_id: '币种',
  },
	views: {
		gridview: {
			caption: "付款",
      title: '付款',
		},
		editview: {
			caption: "付款",
      title: '付款',
		},
	},
	main_form: {
		details: {
			group1: "付款基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			partner_id_text: "业务伙伴", 
			amount: "付款金额", 
			payment_date: "付款日期", 
			communication: "备忘", 
			id: "ID", 
			partner_id: "业务伙伴", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			partner_id_text: "客户",
			amount: "付款金额",
			payment_date: "付款日期",
			communication: "备忘",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem23: {
			caption: "导入",
			tip: "导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};
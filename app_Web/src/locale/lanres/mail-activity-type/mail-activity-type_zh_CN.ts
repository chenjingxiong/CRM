export default {
  fields: {
    res_model_change: '模型已更改',
    summary: '摘要',
    force_next: '自动安排下一个活动',
    sequence: '序号',
    id: 'ID',
    category: '类别',
    icon: '图标',
    delay_count: '之后',
    previous_type_ids: '预先活动',
    display_name: '显示名称',
    active: '有效',
    delay_from: '延迟类型',
    write_date: '最后更新时间',
    mail_template_ids: '邮件模板',
    name: '名称',
    create_date: '创建时间',
    res_model_id: '模型',
    __last_update: '最后修改日',
    decoration_type: '排版类型',
    next_type_ids: '推荐的下一活动',
    delay_unit: '延迟单位',
    initial_res_model_id: '初始模型',
    default_next_type_id_text: '设置默认下一个活动',
    write_uid_text: '最后更新者',
    create_uid_text: '创建人',
    create_uid: '创建人',
    write_uid: '最后更新者',
    default_next_type_id: '设置默认下一个活动',
  },
	views: {
		pickupview: {
			caption: '活动类型',
      title: '活动类型',
		},
		pickupgridview: {
			caption: '活动类型',
      title: '活动类型',
		},
	},
	main_grid: {
		columns: {
			name: '名称',
			write_uid_text: '最后更新者',
			write_date: '最后更新时间',
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
		},
		uiactions: {
		},
	},
};
import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import { globalRoutes, indexRoutes} from '@/router'

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/appindexview/:appindexview?',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = 'appindexview';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
                }
                const url: string = '/appdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, router);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {  
                caption: 'app.views.appindexview.caption',
                viewType: 'APPINDEX',
                parameters: [
                    { pathName: 'appindexview', parameterName: 'appindexview' },
                ],
                requireAuth: true,
            },
            component: () => import('@pages/ungroup/app-index-view/app-index-view.vue'),
            children: [
                {
                    path: 'product_categories/:product_category?/editview/:editview?',
                    meta: {
                        caption: 'entities.product_category.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-edit-view/product-category-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/customercardview/:customercardview?',
                    meta: {
                        caption: 'entities.res_partner.views.customercardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'customercardview', parameterName: 'customercardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-customer-card-view/res-partner-customer-card-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.crm_team.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-grid-view/crm-team-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/dashboardview11/:dashboardview11?',
                    meta: {
                        caption: 'entities.res_partner.views.dashboardview11.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'dashboardview11', parameterName: 'dashboardview11' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-dashboard-view11/res-partner-dashboard-view11.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sale_order.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-grid-view/sale-order-grid-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sale_order.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-grid-view/sale-order-grid-view.vue'),
                },
                {
                    path: 'product_pricelists/:product_pricelist?/editview/:editview?',
                    meta: {
                        caption: 'entities.product_pricelist.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-edit-view/product-pricelist-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/editview/:editview?',
                    meta: {
                        caption: 'entities.res_partner.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-edit-view/res-partner-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/dashboardview9/:dashboardview9?',
                    meta: {
                        caption: 'entities.res_partner.views.dashboardview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'dashboardview9', parameterName: 'dashboardview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-dashboard-view9/res-partner-dashboard-view9.vue'),
                },
                {
                    path: 'appportalview/:appportalview?',
                    meta: {
                        caption: 'app.views.appportalview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'appportalview', parameterName: 'appportalview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/app-portal-view/app-portal-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/gridexpview/:gridexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.gridexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'gridexpview', parameterName: 'gridexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-grid-exp-view/crm-lead-grid-exp-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/gridexpview/:gridexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.gridexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'gridexpview', parameterName: 'gridexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-grid-exp-view/crm-lead-grid-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pmaintabexpview/:pmaintabexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.pmaintabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pmaintabexpview', parameterName: 'pmaintabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pmain-tab-exp-view/res-partner-pmain-tab-exp-view.vue'),
                },
                {
                    path: 'res_partner_ps/:res_partner_p?/pmaintabexpview/:pmaintabexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.pmaintabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pmaintabexpview', parameterName: 'pmaintabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pmain-tab-exp-view/res-partner-pmain-tab-exp-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/dashboardview9/:dashboardview9?',
                    meta: {
                        caption: 'entities.crm_team.views.dashboardview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'dashboardview9', parameterName: 'dashboardview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-dashboard-view9/crm-team-dashboard-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/editview9_editmode/:editview9_editmode?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview9_editmode.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view9-edit-mode/crm-lead-edit-view9-edit-mode.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/editview9_editmode/:editview9_editmode?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview9_editmode.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view9-edit-mode/crm-lead-edit-view9-edit-mode.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/salespersoncardview/:salespersoncardview?',
                    meta: {
                        caption: 'entities.res_users.views.salespersoncardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'salespersoncardview', parameterName: 'salespersoncardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-sales-person-card-view/res-users-sales-person-card-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/salespersoncardview/:salespersoncardview?',
                    meta: {
                        caption: 'entities.res_users.views.salespersoncardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'salespersoncardview', parameterName: 'salespersoncardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-sales-person-card-view/res-users-sales-person-card-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-view/res-partner-pickup-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/dashboardview11/:dashboardview11?',
                    meta: {
                        caption: 'entities.crm_team.views.dashboardview11.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'dashboardview11', parameterName: 'dashboardview11' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-dashboard-view11/crm-team-dashboard-view11.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.res_users.views.treeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-tree-exp-view/res-users-tree-exp-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.res_users.views.treeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-tree-exp-view/res-users-tree-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.crm_lead.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-grid-view/crm-lead-grid-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.crm_lead.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-grid-view/crm-lead-grid-view.vue'),
                },
                {
                    path: 'product_pricelists/:product_pricelist?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.product_pricelist.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-grid-view/product-pricelist-grid-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.res_users.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-grid-view/res-users-grid-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.res_users.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-grid-view/res-users-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/usr2editview/:usr2editview?',
                    meta: {
                        caption: 'entities.sale_order.views.usr2editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'usr2editview', parameterName: 'usr2editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-usr2-edit-view/sale-order-usr2-edit-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/usr2editview/:usr2editview?',
                    meta: {
                        caption: 'entities.sale_order.views.usr2editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'usr2editview', parameterName: 'usr2editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-usr2-edit-view/sale-order-usr2-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/salespersontreeexpview/:salespersontreeexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.salespersontreeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-sales-person-tree-exp-view/res-partner-sales-person-tree-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
                },
                {
                    path: 'sale_order_lines/:sale_order_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/dataviewexpview2/:dataviewexpview2?',
                    meta: {
                        caption: 'entities.crm_team.views.dataviewexpview2.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'dataviewexpview2', parameterName: 'dataviewexpview2' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-data-view-exp-view2/crm-team-data-view-exp-view2.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/editview9/:editview9?',
                    meta: {
                        caption: 'entities.crm_team.views.editview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'editview9', parameterName: 'editview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-edit-view9/crm-team-edit-view9.vue'),
                },
                {
                    path: 'teamleaderportalview/:teamleaderportalview?',
                    meta: {
                        caption: 'app.views.teamleaderportalview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'teamleaderportalview', parameterName: 'teamleaderportalview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/team-leader-portal-view/team-leader-portal-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/tabexpview/:tabexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.tabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'tabexpview', parameterName: 'tabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-tab-exp-view/crm-lead-tab-exp-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/tabexpview/:tabexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.tabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'tabexpview', parameterName: 'tabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-tab-exp-view/crm-lead-tab-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/editview/:editview?',
                    meta: {
                        caption: 'entities.account_invoice.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-edit-view/account-invoice-edit-view.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/editview/:editview?',
                    meta: {
                        caption: 'entities.account_invoice.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-edit-view/account-invoice-edit-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/editview/:editview?',
                    meta: {
                        caption: 'entities.crm_team.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-edit-view/crm-team-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/editview/:editview?',
                    meta: {
                        caption: 'entities.sale_order.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-edit-view/sale-order-edit-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/editview/:editview?',
                    meta: {
                        caption: 'entities.sale_order.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-edit-view/sale-order-edit-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.product_category.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-grid-view/product-category-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/contactscardview/:contactscardview?',
                    meta: {
                        caption: 'entities.res_partner.views.contactscardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'contactscardview', parameterName: 'contactscardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-contacts-card-view/res-partner-contacts-card-view.vue'),
                },
                {
                    path: 'product_pricelist_items/:product_pricelist_item?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.product_pricelist_item.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_pricelist_items', parameterName: 'product_pricelist_item' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-item-grid-view/product-pricelist-item-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pgridview/:pgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pgridview', parameterName: 'pgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pgrid-view/res-partner-pgrid-view.vue'),
                },
                {
                    path: 'res_partner_ps/:res_partner_p?/pgridview/:pgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pgridview', parameterName: 'pgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pgrid-view/res-partner-pgrid-view.vue'),
                },
                {
                    path: 'account_invoice_lines/:account_invoice_line?/usr2gridview/:usr2gridview?',
                    meta: {
                        caption: 'entities.account_invoice_line.views.usr2gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoice_lines', parameterName: 'account_invoice_line' },
                            { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-line-usr2-grid-view/account-invoice-line-usr2-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mainview9_editmode/:mainview9_editmode?',
                    meta: {
                        caption: 'entities.res_partner.views.mainview9_editmode.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mainview9_editmode', parameterName: 'mainview9_editmode' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-main-view9-edit-mode/res-partner-main-view9-edit-mode.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/editview9/:editview9?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview9', parameterName: 'editview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view9/crm-lead-edit-view9.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/editview9/:editview9?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview9', parameterName: 'editview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view9/crm-lead-edit-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/dataviewexpview/:dataviewexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.dataviewexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'dataviewexpview', parameterName: 'dataviewexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-data-view-exp-view/res-partner-data-view-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/allleadtreeexpview/:allleadtreeexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.allleadtreeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'allleadtreeexpview', parameterName: 'allleadtreeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-all-lead-tree-exp-view/crm-lead-all-lead-tree-exp-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/allleadtreeexpview/:allleadtreeexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.allleadtreeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'allleadtreeexpview', parameterName: 'allleadtreeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-all-lead-tree-exp-view/crm-lead-all-lead-tree-exp-view.vue'),
                },
                {
                    path: 'salespersonportalview/:salespersonportalview?',
                    meta: {
                        caption: 'app.views.salespersonportalview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'salespersonportalview', parameterName: 'salespersonportalview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/sales-person-portal-view/sales-person-portal-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_payments/:account_payment?/editview/:editview?',
                    meta: {
                        caption: 'entities.account_payment.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-edit-view/account-payment-edit-view.vue'),
                },
                {
                    path: 'account_payments/:account_payment?/editview/:editview?',
                    meta: {
                        caption: 'entities.account_payment.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-edit-view/account-payment-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/editview/:editview?',
                    meta: {
                        caption: 'entities.product_product.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-edit-view/product-product-edit-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/gridview2/:gridview2?',
                    meta: {
                        caption: 'entities.res_users.views.gridview2.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'gridview2', parameterName: 'gridview2' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-grid-view2/res-users-grid-view2.vue'),
                },
                {
                    path: 'res_users/:res_users?/gridview2/:gridview2?',
                    meta: {
                        caption: 'entities.res_users.views.gridview2.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'gridview2', parameterName: 'gridview2' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-grid-view2/res-users-grid-view2.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.crm_team.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-pickup-grid-view/crm-team-pickup-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.account_invoice.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-grid-view/account-invoice-grid-view.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.account_invoice.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-grid-view/account-invoice-grid-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.product_category.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-pickup-view/product-category-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
                },
                {
                    path: 'sale_order_lines/:sale_order_line?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.sale_order_line.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-grid-view/res-partner-pickup-grid-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-view/product-product-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/maintabexpview/:maintabexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.maintabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'maintabexpview', parameterName: 'maintabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-main-tab-exp-view/res-partner-main-tab-exp-view.vue'),
                },
                {
                    path: 'calendar_events/:calendar_event?/mainview9/:mainview9?',
                    meta: {
                        caption: 'entities.calendar_event.views.mainview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'calendar_events', parameterName: 'calendar_event' },
                            { pathName: 'mainview9', parameterName: 'mainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-calendar/calendar-event-main-view9/calendar-event-main-view9.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/dataviewexpview/:dataviewexpview?',
                    meta: {
                        caption: 'entities.crm_team.views.dataviewexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'dataviewexpview', parameterName: 'dataviewexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-data-view-exp-view/crm-team-data-view-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/usr2editview/:usr2editview?',
                    meta: {
                        caption: 'entities.account_invoice.views.usr2editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'usr2editview', parameterName: 'usr2editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-usr2-edit-view/account-invoice-usr2-edit-view.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/usr2editview/:usr2editview?',
                    meta: {
                        caption: 'entities.account_invoice.views.usr2editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'usr2editview', parameterName: 'usr2editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-usr2-edit-view/account-invoice-usr2-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.res_partner.views.treeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-tree-exp-view/res-partner-tree-exp-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/editview/:editview?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view/crm-lead-edit-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/editview/:editview?',
                    meta: {
                        caption: 'entities.crm_lead.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-edit-view/crm-lead-edit-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/tabexpview/:tabexpview?',
                    meta: {
                        caption: 'entities.crm_team.views.tabexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'tabexpview', parameterName: 'tabexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-tab-exp-view/crm-team-tab-exp-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/miniinfoview/:miniinfoview?',
                    meta: {
                        caption: 'entities.res_users.views.miniinfoview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'miniinfoview', parameterName: 'miniinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-mini-info-view/res-users-mini-info-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/miniinfoview/:miniinfoview?',
                    meta: {
                        caption: 'entities.res_users.views.miniinfoview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'miniinfoview', parameterName: 'miniinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-mini-info-view/res-users-mini-info-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pmainview9/:pmainview9?',
                    meta: {
                        caption: 'entities.res_partner.views.pmainview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pmainview9', parameterName: 'pmainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pmain-view9/res-partner-pmain-view9.vue'),
                },
                {
                    path: 'res_partner_ps/:res_partner_p?/pmainview9/:pmainview9?',
                    meta: {
                        caption: 'entities.res_partner.views.pmainview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                            { pathName: 'pmainview9', parameterName: 'pmainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pmain-view9/res-partner-pmain-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/usr2gridview/:usr2gridview?',
                    meta: {
                        caption: 'entities.account_invoice.views.usr2gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-usr2-grid-view/account-invoice-usr2-grid-view.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/usr2gridview/:usr2gridview?',
                    meta: {
                        caption: 'entities.account_invoice.views.usr2gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-usr2-grid-view/account-invoice-usr2-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_payments/:account_payment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.account_payment.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-grid-view/account-payment-grid-view.vue'),
                },
                {
                    path: 'account_payments/:account_payment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.account_payment.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-grid-view/account-payment-grid-view.vue'),
                },
                {
                    path: 'product_categories/:product_category?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.product_category.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-pickup-grid-view/product-category-pickup-grid-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.product_product.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-grid-view/product-product-grid-view.vue'),
                },
                {
                    path: 'calendar_events/:calendar_event?/calendarexpview/:calendarexpview?',
                    meta: {
                        caption: 'entities.calendar_event.views.calendarexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'calendar_events', parameterName: 'calendar_event' },
                            { pathName: 'calendarexpview', parameterName: 'calendarexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-calendar/calendar-event-calendar-exp-view/calendar-event-calendar-exp-view.vue'),
                },
                {
                    path: 'leaderportalview/:leaderportalview?',
                    meta: {
                        caption: 'app.views.leaderportalview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'leaderportalview', parameterName: 'leaderportalview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/leader-portal-view/leader-portal-view.vue'),
                },
                {
                    path: 'product_pricelist_items/:product_pricelist_item?/editview/:editview?',
                    meta: {
                        caption: 'entities.product_pricelist_item.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_pricelist_items', parameterName: 'product_pricelist_item' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-item-edit-view/product-pricelist-item-edit-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/editview9_editmode/:editview9_editmode?',
                    meta: {
                        caption: 'entities.crm_team.views.editview9_editmode.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-edit-view9-edit-mode/crm-team-edit-view9-edit-mode.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/dashboardview10/:dashboardview10?',
                    meta: {
                        caption: 'entities.res_partner.views.dashboardview10.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'dashboardview10', parameterName: 'dashboardview10' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-dashboard-view10/res-partner-dashboard-view10.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.crm_team.views.pickupview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-team-pickup-view/crm-team-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/leadcardview/:leadcardview?',
                    meta: {
                        caption: 'entities.crm_lead.views.leadcardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'leadcardview', parameterName: 'leadcardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-lead-card-view/crm-lead-lead-card-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/leadcardview/:leadcardview?',
                    meta: {
                        caption: 'entities.crm_lead.views.leadcardview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'leadcardview', parameterName: 'leadcardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-lead-card-view/crm-lead-lead-card-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mainview9/:mainview9?',
                    meta: {
                        caption: 'entities.res_partner.views.mainview9.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mainview9', parameterName: 'mainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-main-view9/res-partner-main-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/salespersontreeexpview/:salespersontreeexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.salespersontreeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-sales-person-tree-exp-view/crm-lead-sales-person-tree-exp-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/salespersontreeexpview/:salespersontreeexpview?',
                    meta: {
                        caption: 'entities.crm_lead.views.salespersontreeexpview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-sales-person-tree-exp-view/crm-lead-sales-person-tree-exp-view.vue'),
                },
                {
                    path: 'crm_teams/:crm_team?/res_users/:res_users?/editview/:editview?',
                    meta: {
                        caption: 'entities.res_users.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'crm_teams', parameterName: 'crm_team' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-edit-view/res-users-edit-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/editview/:editview?',
                    meta: {
                        caption: 'entities.res_users.views.editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-edit-view/res-users-edit-view.vue'),
                },
                {
                    path: 'account_invoice_lines/:account_invoice_line?/usr2editview/:usr2editview?',
                    meta: {
                        caption: 'entities.account_invoice_line.views.usr2editview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'account_invoice_lines', parameterName: 'account_invoice_line' },
                            { pathName: 'usr2editview', parameterName: 'usr2editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-line-usr2-edit-view/account-invoice-line-usr2-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/usr2gridview/:usr2gridview?',
                    meta: {
                        caption: 'entities.sale_order.views.usr2gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-usr2-grid-view/sale-order-usr2-grid-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/usr2gridview/:usr2gridview?',
                    meta: {
                        caption: 'entities.sale_order.views.usr2gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-usr2-grid-view/sale-order-usr2-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.res_partner.views.gridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-grid-view/res-partner-grid-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.product_product.views.pickupgridview.caption',
                        parameters: [
                            { pathName: 'appindexview', parameterName: 'appindexview' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-pickup-grid-view/product-product-pickup-grid-view.vue'),
                },
            {
                path: 'product_pricelistredirectview/:product_pricelistredirectview?',
                meta: {
                    caption: 'entities.product_pricelist.views.redirectview.caption',
                    parameters: [
                        { pathName: 'product_pricelistredirectview', parameterName: 'product_pricelistredirectview' },
                    ],
                    requireAuth: true,
                },
                component: () => import('@pages/odoo-product/product-pricelist-redirect-view/product-pricelist-redirect-view.vue'),
            },
            {
                path: 'product_pricelist_itemredirectview/:product_pricelist_itemredirectview?',
                meta: {
                    caption: 'entities.product_pricelist_item.views.redirectview.caption',
                    parameters: [
                        { pathName: 'product_pricelist_itemredirectview', parameterName: 'product_pricelist_itemredirectview' },
                    ],
                    requireAuth: true,
                },
                component: () => import('@pages/odoo-product/product-pricelist-item-redirect-view/product-pricelist-item-redirect-view.vue'),
            },
            ...indexRoutes,
            ],
        },
    {
        path: '/salespersonportalview/:salespersonportalview?',
        meta: {
            caption: 'app.views.salespersonportalview.caption',
            parameters: [
                { pathName: 'salespersonportalview', parameterName: 'salespersonportalview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/ungroup/sales-person-portal-view/sales-person-portal-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/gridview/:gridview?',
        meta: {
            caption: 'entities.res_users.views.gridview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-grid-view/res-users-grid-view.vue'),
    },
    {
        path: '/res_users/:res_users?/gridview/:gridview?',
        meta: {
            caption: 'entities.res_users.views.gridview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-grid-view/res-users-grid-view.vue'),
    },
    {
        path: '/calendar_events/:calendar_event?/mainview9_editmode/:mainview9_editmode?',
        meta: {
            caption: 'entities.calendar_event.views.mainview9_editmode.caption',
            parameters: [
                { pathName: 'calendar_events', parameterName: 'calendar_event' },
                { pathName: 'mainview9_editmode', parameterName: 'mainview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-calendar/calendar-event-main-view9-edit-mode/calendar-event-main-view9-edit-mode.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/usr2gridview/:usr2gridview?',
        meta: {
            caption: 'entities.sale_order.views.usr2gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-usr2-grid-view/sale-order-usr2-grid-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/usr2gridview/:usr2gridview?',
        meta: {
            caption: 'entities.sale_order.views.usr2gridview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-usr2-grid-view/sale-order-usr2-grid-view.vue'),
    },
    {
        path: '/product_categories/:product_category?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.product_category.views.pickupview.caption',
            parameters: [
                { pathName: 'product_categories', parameterName: 'product_category' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-category-pickup-view/product-category-pickup-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/allleadtreeexpview/:allleadtreeexpview?',
        meta: {
            caption: 'entities.crm_lead.views.allleadtreeexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'allleadtreeexpview', parameterName: 'allleadtreeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-all-lead-tree-exp-view/crm-lead-all-lead-tree-exp-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/allleadtreeexpview/:allleadtreeexpview?',
        meta: {
            caption: 'entities.crm_lead.views.allleadtreeexpview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'allleadtreeexpview', parameterName: 'allleadtreeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-all-lead-tree-exp-view/crm-lead-all-lead-tree-exp-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/dataviewexpview/:dataviewexpview?',
        meta: {
            caption: 'entities.crm_team.views.dataviewexpview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'dataviewexpview', parameterName: 'dataviewexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-data-view-exp-view/crm-team-data-view-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/treeexpview/:treeexpview?',
        meta: {
            caption: 'entities.res_partner.views.treeexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-tree-exp-view/res-partner-tree-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/leadcardview/:leadcardview?',
        meta: {
            caption: 'entities.crm_lead.views.leadcardview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'leadcardview', parameterName: 'leadcardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-lead-card-view/crm-lead-lead-card-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/leadcardview/:leadcardview?',
        meta: {
            caption: 'entities.crm_lead.views.leadcardview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'leadcardview', parameterName: 'leadcardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-lead-card-view/crm-lead-lead-card-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/editview9_editmode/:editview9_editmode?',
        meta: {
            caption: 'entities.crm_team.views.editview9_editmode.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-edit-view9-edit-mode/crm-team-edit-view9-edit-mode.vue'),
    },
    {
        path: '/teamleaderportalview/:teamleaderportalview?',
        meta: {
            caption: 'app.views.teamleaderportalview.caption',
            parameters: [
                { pathName: 'teamleaderportalview', parameterName: 'teamleaderportalview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/ungroup/team-leader-portal-view/team-leader-portal-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pmainview9/:pmainview9?',
        meta: {
            caption: 'entities.res_partner.views.pmainview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pmainview9', parameterName: 'pmainview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pmain-view9/res-partner-pmain-view9.vue'),
    },
    {
        path: '/res_partner_ps/:res_partner_p?/pmainview9/:pmainview9?',
        meta: {
            caption: 'entities.res_partner.views.pmainview9.caption',
            parameters: [
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pmainview9', parameterName: 'pmainview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pmain-view9/res-partner-pmain-view9.vue'),
    },
    {
        path: '/product_pricelists/:product_pricelist?/editview/:editview?',
        meta: {
            caption: 'entities.product_pricelist.views.editview.caption',
            parameters: [
                { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-pricelist-edit-view/product-pricelist-edit-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_payments/:account_payment?/gridview/:gridview?',
        meta: {
            caption: 'entities.account_payment.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_payments', parameterName: 'account_payment' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-payment-grid-view/account-payment-grid-view.vue'),
    },
    {
        path: '/account_payments/:account_payment?/gridview/:gridview?',
        meta: {
            caption: 'entities.account_payment.views.gridview.caption',
            parameters: [
                { pathName: 'account_payments', parameterName: 'account_payment' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-payment-grid-view/account-payment-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_invoices/:account_invoice?/usr2editview/:usr2editview?',
        meta: {
            caption: 'entities.account_invoice.views.usr2editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'usr2editview', parameterName: 'usr2editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-usr2-edit-view/account-invoice-usr2-edit-view.vue'),
    },
    {
        path: '/account_invoices/:account_invoice?/usr2editview/:usr2editview?',
        meta: {
            caption: 'entities.account_invoice.views.usr2editview.caption',
            parameters: [
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'usr2editview', parameterName: 'usr2editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-usr2-edit-view/account-invoice-usr2-edit-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/tabexpview/:tabexpview?',
        meta: {
            caption: 'entities.crm_team.views.tabexpview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'tabexpview', parameterName: 'tabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-tab-exp-view/crm-team-tab-exp-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.crm_team.views.pickupgridview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-pickup-grid-view/crm-team-pickup-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/gridview/:gridview?',
        meta: {
            caption: 'entities.sale_order_line.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/gridview/:gridview?',
        meta: {
            caption: 'entities.sale_order_line.views.gridview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
    },
    {
        path: '/sale_order_lines/:sale_order_line?/gridview/:gridview?',
        meta: {
            caption: 'entities.sale_order_line.views.gridview.caption',
            parameters: [
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-grid-view/sale-order-line-grid-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.res_users.views.pickupview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
    },
    {
        path: '/res_users/:res_users?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.res_users.views.pickupview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/dashboardview9/:dashboardview9?',
        meta: {
            caption: 'entities.res_partner.views.dashboardview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'dashboardview9', parameterName: 'dashboardview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-dashboard-view9/res-partner-dashboard-view9.vue'),
    },
    {
        path: '/account_invoice_lines/:account_invoice_line?/usr2gridview/:usr2gridview?',
        meta: {
            caption: 'entities.account_invoice_line.views.usr2gridview.caption',
            parameters: [
                { pathName: 'account_invoice_lines', parameterName: 'account_invoice_line' },
                { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-line-usr2-grid-view/account-invoice-line-usr2-grid-view.vue'),
    },
    {
        path: '/mail_activity_types/:mail_activity_type?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.mail_activity_type.views.pickupview.caption',
            parameters: [
                { pathName: 'mail_activity_types', parameterName: 'mail_activity_type' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-mail/mail-activity-type-pickup-view/mail-activity-type-pickup-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/gridview/:gridview?',
        meta: {
            caption: 'entities.crm_lead.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-grid-view/crm-lead-grid-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/gridview/:gridview?',
        meta: {
            caption: 'entities.crm_lead.views.gridview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-grid-view/crm-lead-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.res_partner.views.pickupview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pickup-view/res-partner-pickup-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pgridview/:pgridview?',
        meta: {
            caption: 'entities.res_partner.views.pgridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pgridview', parameterName: 'pgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pgrid-view/res-partner-pgrid-view.vue'),
    },
    {
        path: '/res_partner_ps/:res_partner_p?/pgridview/:pgridview?',
        meta: {
            caption: 'entities.res_partner.views.pgridview.caption',
            parameters: [
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pgridview', parameterName: 'pgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pgrid-view/res-partner-pgrid-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.res_users.views.pickupgridview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
    },
    {
        path: '/res_users/:res_users?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.res_users.views.pickupgridview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
    },
    {
        path: '/product_products/:product_product?/gridview/:gridview?',
        meta: {
            caption: 'entities.product_product.views.gridview.caption',
            parameters: [
                { pathName: 'product_products', parameterName: 'product_product' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-product-grid-view/product-product-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/usr2editview/:usr2editview?',
        meta: {
            caption: 'entities.sale_order.views.usr2editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'usr2editview', parameterName: 'usr2editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-usr2-edit-view/sale-order-usr2-edit-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/usr2editview/:usr2editview?',
        meta: {
            caption: 'entities.sale_order.views.usr2editview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'usr2editview', parameterName: 'usr2editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-usr2-edit-view/sale-order-usr2-edit-view.vue'),
    },
    {
        path: '/product_products/:product_product?/editview/:editview?',
        meta: {
            caption: 'entities.product_product.views.editview.caption',
            parameters: [
                { pathName: 'product_products', parameterName: 'product_product' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-product-edit-view/product-product-edit-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_payments/:account_payment?/editview/:editview?',
        meta: {
            caption: 'entities.account_payment.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_payments', parameterName: 'account_payment' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-payment-edit-view/account-payment-edit-view.vue'),
    },
    {
        path: '/account_payments/:account_payment?/editview/:editview?',
        meta: {
            caption: 'entities.account_payment.views.editview.caption',
            parameters: [
                { pathName: 'account_payments', parameterName: 'account_payment' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-payment-edit-view/account-payment-edit-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_invoices/:account_invoice?/gridview/:gridview?',
        meta: {
            caption: 'entities.account_invoice.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-grid-view/account-invoice-grid-view.vue'),
    },
    {
        path: '/account_invoices/:account_invoice?/gridview/:gridview?',
        meta: {
            caption: 'entities.account_invoice.views.gridview.caption',
            parameters: [
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-grid-view/account-invoice-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_invoices/:account_invoice?/usr2gridview/:usr2gridview?',
        meta: {
            caption: 'entities.account_invoice.views.usr2gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-usr2-grid-view/account-invoice-usr2-grid-view.vue'),
    },
    {
        path: '/account_invoices/:account_invoice?/usr2gridview/:usr2gridview?',
        meta: {
            caption: 'entities.account_invoice.views.usr2gridview.caption',
            parameters: [
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'usr2gridview', parameterName: 'usr2gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-usr2-grid-view/account-invoice-usr2-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/dashboardview11/:dashboardview11?',
        meta: {
            caption: 'entities.res_partner.views.dashboardview11.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'dashboardview11', parameterName: 'dashboardview11' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-dashboard-view11/res-partner-dashboard-view11.vue'),
    },
    {
        path: '/mail_activities/:mail_activity?/listview/:listview?',
        meta: {
            caption: 'entities.mail_activity.views.listview.caption',
            parameters: [
                { pathName: 'mail_activities', parameterName: 'mail_activity' },
                { pathName: 'listview', parameterName: 'listview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-mail/mail-activity-list-view/mail-activity-list-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/mainview9/:mainview9?',
        meta: {
            caption: 'entities.res_partner.views.mainview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'mainview9', parameterName: 'mainview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-main-view9/res-partner-main-view9.vue'),
    },
    {
        path: '/product_categories/:product_category?/gridview/:gridview?',
        meta: {
            caption: 'entities.product_category.views.gridview.caption',
            parameters: [
                { pathName: 'product_categories', parameterName: 'product_category' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-category-grid-view/product-category-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/salespersontreeexpview/:salespersontreeexpview?',
        meta: {
            caption: 'entities.crm_lead.views.salespersontreeexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-sales-person-tree-exp-view/crm-lead-sales-person-tree-exp-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/salespersontreeexpview/:salespersontreeexpview?',
        meta: {
            caption: 'entities.crm_lead.views.salespersontreeexpview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-sales-person-tree-exp-view/crm-lead-sales-person-tree-exp-view.vue'),
    },
    {
        path: '/calendar_events/:calendar_event?/calendarexpview/:calendarexpview?',
        meta: {
            caption: 'entities.calendar_event.views.calendarexpview.caption',
            parameters: [
                { pathName: 'calendar_events', parameterName: 'calendar_event' },
                { pathName: 'calendarexpview', parameterName: 'calendarexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-calendar/calendar-event-calendar-exp-view/calendar-event-calendar-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/tabexpview/:tabexpview?',
        meta: {
            caption: 'entities.crm_lead.views.tabexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'tabexpview', parameterName: 'tabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-tab-exp-view/crm-lead-tab-exp-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/tabexpview/:tabexpview?',
        meta: {
            caption: 'entities.crm_lead.views.tabexpview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'tabexpview', parameterName: 'tabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-tab-exp-view/crm-lead-tab-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/listview9/:listview9?',
        meta: {
            caption: 'entities.sale_order.views.listview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'listview9', parameterName: 'listview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-list-view9/sale-order-list-view9.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/listview9/:listview9?',
        meta: {
            caption: 'entities.sale_order.views.listview9.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'listview9', parameterName: 'listview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-list-view9/sale-order-list-view9.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/editview9_editmode/:editview9_editmode?',
        meta: {
            caption: 'entities.crm_lead.views.editview9_editmode.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view9-edit-mode/crm-lead-edit-view9-edit-mode.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/editview9_editmode/:editview9_editmode?',
        meta: {
            caption: 'entities.crm_lead.views.editview9_editmode.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview9_editmode', parameterName: 'editview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view9-edit-mode/crm-lead-edit-view9-edit-mode.vue'),
    },
    {
        path: '/res_partners/:res_partner?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.res_partner.views.pickupgridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pickup-grid-view/res-partner-pickup-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/mainview9_editmode/:mainview9_editmode?',
        meta: {
            caption: 'entities.res_partner.views.mainview9_editmode.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'mainview9_editmode', parameterName: 'mainview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-main-view9-edit-mode/res-partner-main-view9-edit-mode.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/editview/:editview?',
        meta: {
            caption: 'entities.res_users.views.editview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-edit-view/res-users-edit-view.vue'),
    },
    {
        path: '/res_users/:res_users?/editview/:editview?',
        meta: {
            caption: 'entities.res_users.views.editview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-edit-view/res-users-edit-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/dashboardview9/:dashboardview9?',
        meta: {
            caption: 'entities.crm_team.views.dashboardview9.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'dashboardview9', parameterName: 'dashboardview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-dashboard-view9/crm-team-dashboard-view9.vue'),
    },
    {
        path: '/mail_activities/:mail_activity?/calendarview/:calendarview?',
        meta: {
            caption: 'entities.mail_activity.views.calendarview.caption',
            parameters: [
                { pathName: 'mail_activities', parameterName: 'mail_activity' },
                { pathName: 'calendarview', parameterName: 'calendarview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-mail/mail-activity-calendar-view/mail-activity-calendar-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/salespersontreeexpview/:salespersontreeexpview?',
        meta: {
            caption: 'entities.res_partner.views.salespersontreeexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'salespersontreeexpview', parameterName: 'salespersontreeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-sales-person-tree-exp-view/res-partner-sales-person-tree-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/editview/:editview?',
        meta: {
            caption: 'entities.sale_order.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-edit-view/sale-order-edit-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/editview/:editview?',
        meta: {
            caption: 'entities.sale_order.views.editview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-edit-view/sale-order-edit-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/editview9/:editview9?',
        meta: {
            caption: 'entities.crm_team.views.editview9.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'editview9', parameterName: 'editview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-edit-view9/crm-team-edit-view9.vue'),
    },
    {
        path: '/product_pricelist_items/:product_pricelist_item?/gridview/:gridview?',
        meta: {
            caption: 'entities.product_pricelist_item.views.gridview.caption',
            parameters: [
                { pathName: 'product_pricelist_items', parameterName: 'product_pricelist_item' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-pricelist-item-grid-view/product-pricelist-item-grid-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/treeexpview/:treeexpview?',
        meta: {
            caption: 'entities.res_users.views.treeexpview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-tree-exp-view/res-users-tree-exp-view.vue'),
    },
    {
        path: '/res_users/:res_users?/treeexpview/:treeexpview?',
        meta: {
            caption: 'entities.res_users.views.treeexpview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'treeexpview', parameterName: 'treeexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-tree-exp-view/res-users-tree-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/dashboardview10/:dashboardview10?',
        meta: {
            caption: 'entities.res_partner.views.dashboardview10.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'dashboardview10', parameterName: 'dashboardview10' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-dashboard-view10/res-partner-dashboard-view10.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/editview/:editview?',
        meta: {
            caption: 'entities.crm_lead.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view/crm-lead-edit-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/editview/:editview?',
        meta: {
            caption: 'entities.crm_lead.views.editview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view/crm-lead-edit-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/listview9/:listview9?',
        meta: {
            caption: 'entities.crm_lead.views.listview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'listview9', parameterName: 'listview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-list-view9/crm-lead-list-view9.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/listview9/:listview9?',
        meta: {
            caption: 'entities.crm_lead.views.listview9.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'listview9', parameterName: 'listview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-list-view9/crm-lead-list-view9.vue'),
    },
    {
        path: '/appportalview/:appportalview?',
        meta: {
            caption: 'app.views.appportalview.caption',
            parameters: [
                { pathName: 'appportalview', parameterName: 'appportalview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/ungroup/app-portal-view/app-portal-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/account_invoices/:account_invoice?/editview/:editview?',
        meta: {
            caption: 'entities.account_invoice.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-edit-view/account-invoice-edit-view.vue'),
    },
    {
        path: '/account_invoices/:account_invoice?/editview/:editview?',
        meta: {
            caption: 'entities.account_invoice.views.editview.caption',
            parameters: [
                { pathName: 'account_invoices', parameterName: 'account_invoice' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-edit-view/account-invoice-edit-view.vue'),
    },
    {
        path: '/product_products/:product_product?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.product_product.views.pickupview.caption',
            parameters: [
                { pathName: 'product_products', parameterName: 'product_product' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-product-pickup-view/product-product-pickup-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/salespersoncardview/:salespersoncardview?',
        meta: {
            caption: 'entities.res_users.views.salespersoncardview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'salespersoncardview', parameterName: 'salespersoncardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-sales-person-card-view/res-users-sales-person-card-view.vue'),
    },
    {
        path: '/res_users/:res_users?/salespersoncardview/:salespersoncardview?',
        meta: {
            caption: 'entities.res_users.views.salespersoncardview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'salespersoncardview', parameterName: 'salespersoncardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-sales-person-card-view/res-users-sales-person-card-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/dashboardview11/:dashboardview11?',
        meta: {
            caption: 'entities.crm_team.views.dashboardview11.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'dashboardview11', parameterName: 'dashboardview11' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-dashboard-view11/crm-team-dashboard-view11.vue'),
    },
    {
        path: '/res_partners/:res_partner?/res_partner_ps/:res_partner_p?/pmaintabexpview/:pmaintabexpview?',
        meta: {
            caption: 'entities.res_partner.views.pmaintabexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pmaintabexpview', parameterName: 'pmaintabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pmain-tab-exp-view/res-partner-pmain-tab-exp-view.vue'),
    },
    {
        path: '/res_partner_ps/:res_partner_p?/pmaintabexpview/:pmaintabexpview?',
        meta: {
            caption: 'entities.res_partner.views.pmaintabexpview.caption',
            parameters: [
                { pathName: 'res_partner_ps', parameterName: 'res_partner_p' },
                { pathName: 'pmaintabexpview', parameterName: 'pmaintabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-pmain-tab-exp-view/res-partner-pmain-tab-exp-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/pickupview/:pickupview?',
        meta: {
            caption: 'entities.crm_team.views.pickupview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'pickupview', parameterName: 'pickupview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-pickup-view/crm-team-pickup-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/maintabexpview/:maintabexpview?',
        meta: {
            caption: 'entities.res_partner.views.maintabexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'maintabexpview', parameterName: 'maintabexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-main-tab-exp-view/res-partner-main-tab-exp-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/gridview/:gridview?',
        meta: {
            caption: 'entities.res_partner.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-grid-view/res-partner-grid-view.vue'),
    },
    {
        path: '/account_invoice_lines/:account_invoice_line?/usr2editview/:usr2editview?',
        meta: {
            caption: 'entities.account_invoice_line.views.usr2editview.caption',
            parameters: [
                { pathName: 'account_invoice_lines', parameterName: 'account_invoice_line' },
                { pathName: 'usr2editview', parameterName: 'usr2editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-account/account-invoice-line-usr2-edit-view/account-invoice-line-usr2-edit-view.vue'),
    },
    {
        path: '/product_pricelists/:product_pricelist?/gridview/:gridview?',
        meta: {
            caption: 'entities.product_pricelist.views.gridview.caption',
            parameters: [
                { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-pricelist-grid-view/product-pricelist-grid-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/gridview/:gridview?',
        meta: {
            caption: 'entities.crm_team.views.gridview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-grid-view/crm-team-grid-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/miniinfoview/:miniinfoview?',
        meta: {
            caption: 'entities.res_users.views.miniinfoview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'miniinfoview', parameterName: 'miniinfoview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-mini-info-view/res-users-mini-info-view.vue'),
    },
    {
        path: '/res_users/:res_users?/miniinfoview/:miniinfoview?',
        meta: {
            caption: 'entities.res_users.views.miniinfoview.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'miniinfoview', parameterName: 'miniinfoview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-mini-info-view/res-users-mini-info-view.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/editview/:editview?',
        meta: {
            caption: 'entities.crm_team.views.editview.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-edit-view/crm-team-edit-view.vue'),
    },
    {
        path: '/product_categories/:product_category?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.product_category.views.pickupgridview.caption',
            parameters: [
                { pathName: 'product_categories', parameterName: 'product_category' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-category-pickup-grid-view/product-category-pickup-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/dataviewexpview/:dataviewexpview?',
        meta: {
            caption: 'entities.res_partner.views.dataviewexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'dataviewexpview', parameterName: 'dataviewexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-data-view-exp-view/res-partner-data-view-exp-view.vue'),
    },
    {
        path: '/leaderportalview/:leaderportalview?',
        meta: {
            caption: 'app.views.leaderportalview.caption',
            parameters: [
                { pathName: 'leaderportalview', parameterName: 'leaderportalview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/ungroup/leader-portal-view/leader-portal-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/gridview/:gridview?',
        meta: {
            caption: 'entities.sale_order.views.gridview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-grid-view/sale-order-grid-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/gridview/:gridview?',
        meta: {
            caption: 'entities.sale_order.views.gridview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'gridview', parameterName: 'gridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-grid-view/sale-order-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/editview9/:editview9?',
        meta: {
            caption: 'entities.crm_lead.views.editview9.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview9', parameterName: 'editview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view9/crm-lead-edit-view9.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/editview9/:editview9?',
        meta: {
            caption: 'entities.crm_lead.views.editview9.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'editview9', parameterName: 'editview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-edit-view9/crm-lead-edit-view9.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/res_users/:res_users?/gridview2/:gridview2?',
        meta: {
            caption: 'entities.res_users.views.gridview2.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'gridview2', parameterName: 'gridview2' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-grid-view2/res-users-grid-view2.vue'),
    },
    {
        path: '/res_users/:res_users?/gridview2/:gridview2?',
        meta: {
            caption: 'entities.res_users.views.gridview2.caption',
            parameters: [
                { pathName: 'res_users', parameterName: 'res_users' },
                { pathName: 'gridview2', parameterName: 'gridview2' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-users-grid-view2/res-users-grid-view2.vue'),
    },
    {
        path: '/calendar_events/:calendar_event?/mainview9/:mainview9?',
        meta: {
            caption: 'entities.calendar_event.views.mainview9.caption',
            parameters: [
                { pathName: 'calendar_events', parameterName: 'calendar_event' },
                { pathName: 'mainview9', parameterName: 'mainview9' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-calendar/calendar-event-main-view9/calendar-event-main-view9.vue'),
    },
    {
        path: '/res_partners/:res_partner?/customercardview/:customercardview?',
        meta: {
            caption: 'entities.res_partner.views.customercardview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'customercardview', parameterName: 'customercardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-customer-card-view/res-partner-customer-card-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/crm_leads/:crm_lead?/gridexpview/:gridexpview?',
        meta: {
            caption: 'entities.crm_lead.views.gridexpview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'gridexpview', parameterName: 'gridexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-grid-exp-view/crm-lead-grid-exp-view.vue'),
    },
    {
        path: '/crm_leads/:crm_lead?/gridexpview/:gridexpview?',
        meta: {
            caption: 'entities.crm_lead.views.gridexpview.caption',
            parameters: [
                { pathName: 'crm_leads', parameterName: 'crm_lead' },
                { pathName: 'gridexpview', parameterName: 'gridexpview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-lead-grid-exp-view/crm-lead-grid-exp-view.vue'),
    },
    {
        path: '/product_products/:product_product?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.product_product.views.pickupgridview.caption',
            parameters: [
                { pathName: 'product_products', parameterName: 'product_product' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-product-pickup-grid-view/product-product-pickup-grid-view.vue'),
    },
    {
        path: '/product_categories/:product_category?/editview/:editview?',
        meta: {
            caption: 'entities.product_category.views.editview.caption',
            parameters: [
                { pathName: 'product_categories', parameterName: 'product_category' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-category-edit-view/product-category-edit-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/editview/:editview?',
        meta: {
            caption: 'entities.sale_order_line.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
    },
    {
        path: '/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/editview/:editview?',
        meta: {
            caption: 'entities.sale_order_line.views.editview.caption',
            parameters: [
                { pathName: 'sale_orders', parameterName: 'sale_order' },
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
    },
    {
        path: '/sale_order_lines/:sale_order_line?/editview/:editview?',
        meta: {
            caption: 'entities.sale_order_line.views.editview.caption',
            parameters: [
                { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-sale/sale-order-line-edit-view/sale-order-line-edit-view.vue'),
    },
    {
        path: '/mail_activity_types/:mail_activity_type?/pickupgridview/:pickupgridview?',
        meta: {
            caption: 'entities.mail_activity_type.views.pickupgridview.caption',
            parameters: [
                { pathName: 'mail_activity_types', parameterName: 'mail_activity_type' },
                { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-mail/mail-activity-type-pickup-grid-view/mail-activity-type-pickup-grid-view.vue'),
    },
    {
        path: '/res_partners/:res_partner?/editview/:editview?',
        meta: {
            caption: 'entities.res_partner.views.editview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-edit-view/res-partner-edit-view.vue'),
    },
    {
        path: '/mail_activities/:mail_activity?/mainview9_editmode/:mainview9_editmode?',
        meta: {
            caption: 'entities.mail_activity.views.mainview9_editmode.caption',
            parameters: [
                { pathName: 'mail_activities', parameterName: 'mail_activity' },
                { pathName: 'mainview9_editmode', parameterName: 'mainview9_editmode' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-mail/mail-activity-main-view9-edit-mode/mail-activity-main-view9-edit-mode.vue'),
    },
    {
        path: '/crm_teams/:crm_team?/dataviewexpview2/:dataviewexpview2?',
        meta: {
            caption: 'entities.crm_team.views.dataviewexpview2.caption',
            parameters: [
                { pathName: 'crm_teams', parameterName: 'crm_team' },
                { pathName: 'dataviewexpview2', parameterName: 'dataviewexpview2' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-crm/crm-team-data-view-exp-view2/crm-team-data-view-exp-view2.vue'),
    },
    {
        path: '/res_partners/:res_partner?/contactscardview/:contactscardview?',
        meta: {
            caption: 'entities.res_partner.views.contactscardview.caption',
            parameters: [
                { pathName: 'res_partners', parameterName: 'res_partner' },
                { pathName: 'contactscardview', parameterName: 'contactscardview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-base/res-partner-contacts-card-view/res-partner-contacts-card-view.vue'),
    },
    {
        path: '/product_pricelist_items/:product_pricelist_item?/editview/:editview?',
        meta: {
            caption: 'entities.product_pricelist_item.views.editview.caption',
            parameters: [
                { pathName: 'product_pricelist_items', parameterName: 'product_pricelist_item' },
                { pathName: 'editview', parameterName: 'editview' },
            ],
            requireAuth: true,
        },
        component: () => import('@pages/odoo-product/product-pricelist-item-edit-view/product-pricelist-item-edit-view.vue'),
    },
        ...globalRoutes,
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                router.app.$store.commit('resetRootStateData');
                next();
            },
            component: () => import('@components/login/login.vue'),
        },
        {
            path: '/404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: 'appindexview'
        },
    ],
});

export default router;

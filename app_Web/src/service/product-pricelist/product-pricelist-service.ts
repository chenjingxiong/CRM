import { Http,Util } from '@/utils';
import Product_pricelistServiceBase from './product-pricelist-service-base';


/**
 * 价格表服务对象
 *
 * @export
 * @class Product_pricelistService
 * @extends {Product_pricelistServiceBase}
 */
export default class Product_pricelistService extends Product_pricelistServiceBase {

    /**
     * Creates an instance of  Product_pricelistService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_pricelistService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
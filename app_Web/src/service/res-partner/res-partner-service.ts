import { Http,Util } from '@/utils';
import Res_partnerServiceBase from './res-partner-service-base';


/**
 * 客户服务对象
 *
 * @export
 * @class Res_partnerService
 * @extends {Res_partnerServiceBase}
 */
export default class Res_partnerService extends Res_partnerServiceBase {

    /**
     * Creates an instance of  Res_partnerService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partnerService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
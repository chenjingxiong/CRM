import { Http,Util } from '@/utils';
import Product_price_listServiceBase from './product-price-list-service-base';


/**
 * 基于价格列表版本的单位产品价格服务对象
 *
 * @export
 * @class Product_price_listService
 * @extends {Product_price_listServiceBase}
 */
export default class Product_price_listService extends Product_price_listServiceBase {

    /**
     * Creates an instance of  Product_price_listService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_price_listService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
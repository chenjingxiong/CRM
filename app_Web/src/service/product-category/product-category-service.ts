import { Http,Util } from '@/utils';
import Product_categoryServiceBase from './product-category-service-base';


/**
 * 产品种类服务对象
 *
 * @export
 * @class Product_categoryService
 * @extends {Product_categoryServiceBase}
 */
export default class Product_categoryService extends Product_categoryServiceBase {

    /**
     * Creates an instance of  Product_categoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_categoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 用户服务对象基类
 *
 * @export
 * @class Res_usersServiceBase
 * @extends {EntityServie}
 */
export default class Res_usersServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_usersServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_usersServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_usersServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_users';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_users';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/select`,data,isloading);
        }
            return Http.getInstance().post(`/res_users/${context.res_users}/select`,data,isloading);
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().put(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_users/${context.res_users}`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/save`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_users/${context.res_users}/save`,data,isloading);
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/res_users`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_users`,data,isloading);
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/res_users/${context.res_users}/checkkey`,data,isloading);
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().get(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/res_users/${context.res_users}`,isloading);
            return res;

    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().put(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/res_users/${context.res_users}/updatebatch`,data,isloading);
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/res_users/${context.res_users}/createbatch`,data,isloading);
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().delete(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/res_users/${context.res_users}/removebatch`,isloading);

    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && context.res_users){
            return Http.getInstance().delete(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`,isloading);
        }
            return Http.getInstance().delete(`/res_users/${context.res_users}`,isloading);

    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && true){
            return Http.getInstance().get(`/crm_teams/${context.crm_team}/res_users/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/res_users/getdraft`,isloading);
        res.data.res_users = data.res_users;
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_usersServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.crm_team && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/crm_teams/${context.crm_team}/res_users/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_users/fetchdefault`,tempData,isloading);
    }
}
/**
 * 实体数据服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister {

    /**
     * 所有实体数据服务Map
     *
     * @protected
     * @type {*}
     * @memberof EntityServiceRegister
     */
    protected allEntityService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体数据服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof EntityServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of EntityServiceRegister.
     * @memberof EntityServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof EntityServiceRegister
     */
    protected init(): void {
                this.allEntityService.set('product_price_list', () => import('@/service/product-price-list/product-price-list-service'));
        this.allEntityService.set('account_journal', () => import('@/service/account-journal/account-journal-service'));
        this.allEntityService.set('product_pricelist', () => import('@/service/product-pricelist/product-pricelist-service'));
        this.allEntityService.set('crm_lead', () => import('@/service/crm-lead/crm-lead-service'));
        this.allEntityService.set('mail_activity', () => import('@/service/mail-activity/mail-activity-service'));
        this.allEntityService.set('res_company', () => import('@/service/res-company/res-company-service'));
        this.allEntityService.set('sale_order_line', () => import('@/service/sale-order-line/sale-order-line-service'));
        this.allEntityService.set('account_payment', () => import('@/service/account-payment/account-payment-service'));
        this.allEntityService.set('product_product', () => import('@/service/product-product/product-product-service'));
        this.allEntityService.set('calendar_event', () => import('@/service/calendar-event/calendar-event-service'));
        this.allEntityService.set('crm_team', () => import('@/service/crm-team/crm-team-service'));
        this.allEntityService.set('product_category', () => import('@/service/product-category/product-category-service'));
        this.allEntityService.set('res_partner_p', () => import('@/service/res-partner-p/res-partner-p-service'));
        this.allEntityService.set('product_pricelist_item', () => import('@/service/product-pricelist-item/product-pricelist-item-service'));
        this.allEntityService.set('account_invoice', () => import('@/service/account-invoice/account-invoice-service'));
        this.allEntityService.set('mail_message', () => import('@/service/mail-message/mail-message-service'));
        this.allEntityService.set('res_users', () => import('@/service/res-users/res-users-service'));
        this.allEntityService.set('sale_order', () => import('@/service/sale-order/sale-order-service'));
        this.allEntityService.set('mail_activity_type', () => import('@/service/mail-activity-type/mail-activity-type-service'));
        this.allEntityService.set('res_partner', () => import('@/service/res-partner/res-partner-service'));
        this.allEntityService.set('account_register_payments', () => import('@/service/account-register-payments/account-register-payments-service'));
        this.allEntityService.set('dynachart', () => import('@/service/dyna-chart/dyna-chart-service'));
        this.allEntityService.set('dynadashboard', () => import('@/service/dyna-dashboard/dyna-dashboard-service'));
        this.allEntityService.set('account_invoice_line', () => import('@/service/account-invoice-line/account-invoice-line-service'));
    }

    /**
     * 加载实体数据服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allEntityService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体数据服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const entityServiceRegister: EntityServiceRegister = new EntityServiceRegister();
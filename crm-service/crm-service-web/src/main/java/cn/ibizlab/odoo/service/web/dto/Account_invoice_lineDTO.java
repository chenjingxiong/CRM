package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_invoice_line.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_invoice_lineDTO]
 */
public class Account_invoice_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_TYPE]
     *
     */
    @Account_invoice_lineDisplay_typeDefault(info = "默认规则")
    private String display_type;

    @JsonIgnore
    private boolean display_typeDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_invoice_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @Account_invoice_linePrice_subtotalDefault(info = "默认规则")
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_invoice_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Account_invoice_lineOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_invoice_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SALE_LINE_IDS]
     *
     */
    @Account_invoice_lineSale_line_idsDefault(info = "默认规则")
    private String sale_line_ids;

    @JsonIgnore
    private boolean sale_line_idsDirtyFlag;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @Account_invoice_lineDiscountDefault(info = "默认规则")
    private Double discount;

    @JsonIgnore
    private boolean discountDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Account_invoice_linePrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_invoice_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [QUANTITY]
     *
     */
    @Account_invoice_lineQuantityDefault(info = "默认规则")
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @Account_invoice_linePrice_taxDefault(info = "默认规则")
    private Double price_tax;

    @JsonIgnore
    private boolean price_taxDirtyFlag;

    /**
     * 属性 [PRICE_SUBTOTAL_SIGNED]
     *
     */
    @Account_invoice_linePrice_subtotal_signedDefault(info = "默认规则")
    private Double price_subtotal_signed;

    @JsonIgnore
    private boolean price_subtotal_signedDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_invoice_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Account_invoice_linePrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_invoice_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [INVOICE_LINE_TAX_IDS]
     *
     */
    @Account_invoice_lineInvoice_line_tax_idsDefault(info = "默认规则")
    private String invoice_line_tax_ids;

    @JsonIgnore
    private boolean invoice_line_tax_idsDirtyFlag;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Account_invoice_lineAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [IS_ROUNDING_LINE]
     *
     */
    @Account_invoice_lineIs_rounding_lineDefault(info = "默认规则")
    private String is_rounding_line;

    @JsonIgnore
    private boolean is_rounding_lineDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_invoice_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_invoice_lineCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @Account_invoice_lineAccount_analytic_id_textDefault(info = "默认规则")
    private String account_analytic_id_text;

    @JsonIgnore
    private boolean account_analytic_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Account_invoice_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @Account_invoice_lineInvoice_id_textDefault(info = "默认规则")
    private String invoice_id_text;

    @JsonIgnore
    private boolean invoice_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_invoice_linePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_invoice_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [INVOICE_TYPE]
     *
     */
    @Account_invoice_lineInvoice_typeDefault(info = "默认规则")
    private String invoice_type;

    @JsonIgnore
    private boolean invoice_typeDirtyFlag;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @Account_invoice_lineProduct_imageDefault(info = "默认规则")
    private byte[] product_image;

    @JsonIgnore
    private boolean product_imageDirtyFlag;

    /**
     * 属性 [UOM_ID_TEXT]
     *
     */
    @Account_invoice_lineUom_id_textDefault(info = "默认规则")
    private String uom_id_text;

    @JsonIgnore
    private boolean uom_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Account_invoice_lineCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @Account_invoice_linePurchase_idDefault(info = "默认规则")
    private Integer purchase_id;

    @JsonIgnore
    private boolean purchase_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_invoice_lineAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_ID_TEXT]
     *
     */
    @Account_invoice_linePurchase_line_id_textDefault(info = "默认规则")
    private String purchase_line_id_text;

    @JsonIgnore
    private boolean purchase_line_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_invoice_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_invoice_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @Account_invoice_lineInvoice_idDefault(info = "默认规则")
    private Integer invoice_id;

    @JsonIgnore
    private boolean invoice_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_invoice_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Account_invoice_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_invoice_lineAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_ID]
     *
     */
    @Account_invoice_linePurchase_line_idDefault(info = "默认规则")
    private Integer purchase_line_id;

    @JsonIgnore
    private boolean purchase_line_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @Account_invoice_lineAccount_analytic_idDefault(info = "默认规则")
    private Integer account_analytic_id;

    @JsonIgnore
    private boolean account_analytic_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_invoice_linePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_invoice_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_invoice_lineCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [UOM_ID]
     *
     */
    @Account_invoice_lineUom_idDefault(info = "默认规则")
    private Integer uom_id;

    @JsonIgnore
    private boolean uom_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_invoice_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;


    /**
     * 获取 [DISPLAY_TYPE]
     */
    @JsonProperty("display_type")
    public String getDisplay_type(){
        return display_type ;
    }

    /**
     * 设置 [DISPLAY_TYPE]
     */
    @JsonProperty("display_type")
    public void setDisplay_type(String  display_type){
        this.display_type = display_type ;
        this.display_typeDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_typeDirtyFlag(){
        return display_typeDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return price_subtotal ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_IDS]
     */
    @JsonProperty("sale_line_ids")
    public String getSale_line_ids(){
        return sale_line_ids ;
    }

    /**
     * 设置 [SALE_LINE_IDS]
     */
    @JsonProperty("sale_line_ids")
    public void setSale_line_ids(String  sale_line_ids){
        this.sale_line_ids = sale_line_ids ;
        this.sale_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idsDirtyFlag(){
        return sale_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DISCOUNT]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return discount ;
    }

    /**
     * 设置 [DISCOUNT]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

    /**
     * 获取 [DISCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return discountDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [QUANTITY]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return quantity ;
    }

    /**
     * 设置 [QUANTITY]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return quantityDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return price_tax ;
    }

    /**
     * 设置 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TAX]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return price_taxDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL_SIGNED]
     */
    @JsonProperty("price_subtotal_signed")
    public Double getPrice_subtotal_signed(){
        return price_subtotal_signed ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL_SIGNED]
     */
    @JsonProperty("price_subtotal_signed")
    public void setPrice_subtotal_signed(Double  price_subtotal_signed){
        this.price_subtotal_signed = price_subtotal_signed ;
        this.price_subtotal_signedDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotal_signedDirtyFlag(){
        return price_subtotal_signedDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINE_TAX_IDS]
     */
    @JsonProperty("invoice_line_tax_ids")
    public String getInvoice_line_tax_ids(){
        return invoice_line_tax_ids ;
    }

    /**
     * 设置 [INVOICE_LINE_TAX_IDS]
     */
    @JsonProperty("invoice_line_tax_ids")
    public void setInvoice_line_tax_ids(String  invoice_line_tax_ids){
        this.invoice_line_tax_ids = invoice_line_tax_ids ;
        this.invoice_line_tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINE_TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_tax_idsDirtyFlag(){
        return invoice_line_tax_idsDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_ROUNDING_LINE]
     */
    @JsonProperty("is_rounding_line")
    public String getIs_rounding_line(){
        return is_rounding_line ;
    }

    /**
     * 设置 [IS_ROUNDING_LINE]
     */
    @JsonProperty("is_rounding_line")
    public void setIs_rounding_line(String  is_rounding_line){
        this.is_rounding_line = is_rounding_line ;
        this.is_rounding_lineDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ROUNDING_LINE]脏标记
     */
    @JsonIgnore
    public boolean getIs_rounding_lineDirtyFlag(){
        return is_rounding_lineDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return account_analytic_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return account_analytic_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return invoice_id_text ;
    }

    /**
     * 设置 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_TYPE]
     */
    @JsonProperty("invoice_type")
    public String getInvoice_type(){
        return invoice_type ;
    }

    /**
     * 设置 [INVOICE_TYPE]
     */
    @JsonProperty("invoice_type")
    public void setInvoice_type(String  invoice_type){
        this.invoice_type = invoice_type ;
        this.invoice_typeDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_typeDirtyFlag(){
        return invoice_typeDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return product_image ;
    }

    /**
     * 设置 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return product_imageDirtyFlag ;
    }

    /**
     * 获取 [UOM_ID_TEXT]
     */
    @JsonProperty("uom_id_text")
    public String getUom_id_text(){
        return uom_id_text ;
    }

    /**
     * 设置 [UOM_ID_TEXT]
     */
    @JsonProperty("uom_id_text")
    public void setUom_id_text(String  uom_id_text){
        this.uom_id_text = uom_id_text ;
        this.uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUom_id_textDirtyFlag(){
        return uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return purchase_id ;
    }

    /**
     * 设置 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return purchase_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("purchase_line_id_text")
    public String getPurchase_line_id_text(){
        return purchase_line_id_text ;
    }

    /**
     * 设置 [PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("purchase_line_id_text")
    public void setPurchase_line_id_text(String  purchase_line_id_text){
        this.purchase_line_id_text = purchase_line_id_text ;
        this.purchase_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_id_textDirtyFlag(){
        return purchase_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return invoice_id ;
    }

    /**
     * 设置 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return invoice_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID]
     */
    @JsonProperty("purchase_line_id")
    public Integer getPurchase_line_id(){
        return purchase_line_id ;
    }

    /**
     * 设置 [PURCHASE_LINE_ID]
     */
    @JsonProperty("purchase_line_id")
    public void setPurchase_line_id(Integer  purchase_line_id){
        this.purchase_line_id = purchase_line_id ;
        this.purchase_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idDirtyFlag(){
        return purchase_line_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return account_analytic_id ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return account_analytic_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public Integer getUom_id(){
        return uom_id ;
    }

    /**
     * 设置 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public void setUom_id(Integer  uom_id){
        this.uom_id = uom_id ;
        this.uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getUom_idDirtyFlag(){
        return uom_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }



    public Account_invoice_line toDO() {
        Account_invoice_line srfdomain = new Account_invoice_line();
        if(getDisplay_typeDirtyFlag())
            srfdomain.setDisplay_type(display_type);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPrice_subtotalDirtyFlag())
            srfdomain.setPrice_subtotal(price_subtotal);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSale_line_idsDirtyFlag())
            srfdomain.setSale_line_ids(sale_line_ids);
        if(getDiscountDirtyFlag())
            srfdomain.setDiscount(discount);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getQuantityDirtyFlag())
            srfdomain.setQuantity(quantity);
        if(getPrice_taxDirtyFlag())
            srfdomain.setPrice_tax(price_tax);
        if(getPrice_subtotal_signedDirtyFlag())
            srfdomain.setPrice_subtotal_signed(price_subtotal_signed);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getInvoice_line_tax_idsDirtyFlag())
            srfdomain.setInvoice_line_tax_ids(invoice_line_tax_ids);
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getIs_rounding_lineDirtyFlag())
            srfdomain.setIs_rounding_line(is_rounding_line);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getAccount_analytic_id_textDirtyFlag())
            srfdomain.setAccount_analytic_id_text(account_analytic_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getInvoice_id_textDirtyFlag())
            srfdomain.setInvoice_id_text(invoice_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getInvoice_typeDirtyFlag())
            srfdomain.setInvoice_type(invoice_type);
        if(getProduct_imageDirtyFlag())
            srfdomain.setProduct_image(product_image);
        if(getUom_id_textDirtyFlag())
            srfdomain.setUom_id_text(uom_id_text);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getPurchase_idDirtyFlag())
            srfdomain.setPurchase_id(purchase_id);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getPurchase_line_id_textDirtyFlag())
            srfdomain.setPurchase_line_id_text(purchase_line_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getInvoice_idDirtyFlag())
            srfdomain.setInvoice_id(invoice_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getPurchase_line_idDirtyFlag())
            srfdomain.setPurchase_line_id(purchase_line_id);
        if(getAccount_analytic_idDirtyFlag())
            srfdomain.setAccount_analytic_id(account_analytic_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getUom_idDirtyFlag())
            srfdomain.setUom_id(uom_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);

        return srfdomain;
    }

    public void fromDO(Account_invoice_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_typeDirtyFlag())
            this.setDisplay_type(srfdomain.getDisplay_type());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPrice_subtotalDirtyFlag())
            this.setPrice_subtotal(srfdomain.getPrice_subtotal());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSale_line_idsDirtyFlag())
            this.setSale_line_ids(srfdomain.getSale_line_ids());
        if(srfdomain.getDiscountDirtyFlag())
            this.setDiscount(srfdomain.getDiscount());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getQuantityDirtyFlag())
            this.setQuantity(srfdomain.getQuantity());
        if(srfdomain.getPrice_taxDirtyFlag())
            this.setPrice_tax(srfdomain.getPrice_tax());
        if(srfdomain.getPrice_subtotal_signedDirtyFlag())
            this.setPrice_subtotal_signed(srfdomain.getPrice_subtotal_signed());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getInvoice_line_tax_idsDirtyFlag())
            this.setInvoice_line_tax_ids(srfdomain.getInvoice_line_tax_ids());
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getIs_rounding_lineDirtyFlag())
            this.setIs_rounding_line(srfdomain.getIs_rounding_line());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getAccount_analytic_id_textDirtyFlag())
            this.setAccount_analytic_id_text(srfdomain.getAccount_analytic_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getInvoice_id_textDirtyFlag())
            this.setInvoice_id_text(srfdomain.getInvoice_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getInvoice_typeDirtyFlag())
            this.setInvoice_type(srfdomain.getInvoice_type());
        if(srfdomain.getProduct_imageDirtyFlag())
            this.setProduct_image(srfdomain.getProduct_image());
        if(srfdomain.getUom_id_textDirtyFlag())
            this.setUom_id_text(srfdomain.getUom_id_text());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getPurchase_idDirtyFlag())
            this.setPurchase_id(srfdomain.getPurchase_id());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getPurchase_line_id_textDirtyFlag())
            this.setPurchase_line_id_text(srfdomain.getPurchase_line_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getInvoice_idDirtyFlag())
            this.setInvoice_id(srfdomain.getInvoice_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getPurchase_line_idDirtyFlag())
            this.setPurchase_line_id(srfdomain.getPurchase_line_id());
        if(srfdomain.getAccount_analytic_idDirtyFlag())
            this.setAccount_analytic_id(srfdomain.getAccount_analytic_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getUom_idDirtyFlag())
            this.setUom_id(srfdomain.getUom_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());

    }

    public List<Account_invoice_lineDTO> fromDOPage(List<Account_invoice_line> poPage)   {
        if(poPage == null)
            return null;
        List<Account_invoice_lineDTO> dtos=new ArrayList<Account_invoice_lineDTO>();
        for(Account_invoice_line domain : poPage) {
            Account_invoice_lineDTO dto = new Account_invoice_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


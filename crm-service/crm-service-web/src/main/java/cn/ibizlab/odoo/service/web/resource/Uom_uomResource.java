package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Uom_uomDTO;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_uomService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Uom_uom" })
@RestController
@RequestMapping("")
public class Uom_uomResource {

    @Autowired
    private IUom_uomService uom_uomService;

    public IUom_uomService getUom_uomService() {
        return this.uom_uomService;
    }

    @ApiOperation(value = "删除数据", tags = {"Uom_uom" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/uom_uoms/{uom_uom_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("uom_uom_id") Integer uom_uom_id) {
        Uom_uomDTO uom_uomdto = new Uom_uomDTO();
		Uom_uom domain = new Uom_uom();
		uom_uomdto.setId(uom_uom_id);
		domain.setId(uom_uom_id);
        Boolean rst = uom_uomService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Uom_uom" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/uom_uoms/{uom_uom_id}/getdraft")

    public ResponseEntity<Uom_uomDTO> getDraft(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uomDTO uom_uomdto) {
        Uom_uom uom_uom = uom_uomdto.toDO();
    	uom_uom = uom_uomService.getDraft(uom_uom) ;
    	uom_uomdto.fromDO(uom_uom);
        return ResponseEntity.status(HttpStatus.OK).body(uom_uomdto);
    }

    @ApiOperation(value = "更新数据", tags = {"Uom_uom" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/uom_uoms/{uom_uom_id}")

    public ResponseEntity<Uom_uomDTO> update(@PathVariable("uom_uom_id") Integer uom_uom_id, @RequestBody Uom_uomDTO uom_uomdto) {
		Uom_uom domain = uom_uomdto.toDO();
        domain.setId(uom_uom_id);
		uom_uomService.update(domain);
		Uom_uomDTO dto = new Uom_uomDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Uom_uom" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/uom_uoms/createBatch")
    public ResponseEntity<Boolean> createBatchUom_uom(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Uom_uom" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/uom_uoms/{uom_uom_id}")
    public ResponseEntity<Uom_uomDTO> get(@PathVariable("uom_uom_id") Integer uom_uom_id) {
        Uom_uomDTO dto = new Uom_uomDTO();
        Uom_uom domain = uom_uomService.get(uom_uom_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Uom_uom" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/uom_uoms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Uom_uom" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/uom_uoms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Uom_uomDTO> uom_uomdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Uom_uom" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/uom_uoms")

    public ResponseEntity<Uom_uomDTO> create(@RequestBody Uom_uomDTO uom_uomdto) {
        Uom_uomDTO dto = new Uom_uomDTO();
        Uom_uom domain = uom_uomdto.toDO();
		uom_uomService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Uom_uom" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/uom_uoms/fetchdefault")
	public ResponseEntity<Page<Uom_uomDTO>> fetchDefault(Uom_uomSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Uom_uomDTO> list = new ArrayList<Uom_uomDTO>();
        
        Page<Uom_uom> domains = uom_uomService.searchDefault(context) ;
        for(Uom_uom uom_uom : domains.getContent()){
            Uom_uomDTO dto = new Uom_uomDTO();
            dto.fromDO(uom_uom);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

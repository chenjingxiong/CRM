package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_users.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_usersDTO]
 */
public class Res_usersDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_MODERATOR]
     *
     */
    @Res_usersIs_moderatorDefault(info = "默认规则")
    private String is_moderator;

    @JsonIgnore
    private boolean is_moderatorDirtyFlag;

    /**
     * 属性 [RESOURCE_IDS]
     *
     */
    @Res_usersResource_idsDefault(info = "默认规则")
    private String resource_ids;

    @JsonIgnore
    private boolean resource_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_usersCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Res_usersCategory_idDefault(info = "默认规则")
    private String category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Res_usersResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [LOG_IDS]
     *
     */
    @Res_usersLog_idsDefault(info = "默认规则")
    private String log_ids;

    @JsonIgnore
    private boolean log_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Res_usersMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [POS_SECURITY_PIN]
     *
     */
    @Res_usersPos_security_pinDefault(info = "默认规则")
    private String pos_security_pin;

    @JsonIgnore
    private boolean pos_security_pinDirtyFlag;

    /**
     * 属性 [BADGE_IDS]
     *
     */
    @Res_usersBadge_idsDefault(info = "默认规则")
    private String badge_ids;

    @JsonIgnore
    private boolean badge_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_usersWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_usersIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [COMPANY_IDS]
     *
     */
    @Res_usersCompany_idsDefault(info = "默认规则")
    private String company_ids;

    @JsonIgnore
    private boolean company_idsDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Res_usersChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [TZ_OFFSET]
     *
     */
    @Res_usersTz_offsetDefault(info = "默认规则")
    private String tz_offset;

    @JsonIgnore
    private boolean tz_offsetDirtyFlag;

    /**
     * 属性 [TARGET_SALES_DONE]
     *
     */
    @Res_usersTarget_sales_doneDefault(info = "默认规则")
    private Integer target_sales_done;

    @JsonIgnore
    private boolean target_sales_doneDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Res_usersMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [NOTIFICATION_TYPE]
     *
     */
    @Res_usersNotification_typeDefault(info = "默认规则")
    private String notification_type;

    @JsonIgnore
    private boolean notification_typeDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Res_usersActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [IM_STATUS]
     *
     */
    @Res_usersIm_statusDefault(info = "默认规则")
    private String im_status;

    @JsonIgnore
    private boolean im_statusDirtyFlag;

    /**
     * 属性 [KARMA]
     *
     */
    @Res_usersKarmaDefault(info = "默认规则")
    private Integer karma;

    @JsonIgnore
    private boolean karmaDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Res_usersWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Res_usersActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [GOLD_BADGE]
     *
     */
    @Res_usersGold_badgeDefault(info = "默认规则")
    private Integer gold_badge;

    @JsonIgnore
    private boolean gold_badgeDirtyFlag;

    /**
     * 属性 [EMPLOYEE_IDS]
     *
     */
    @Res_usersEmployee_idsDefault(info = "默认规则")
    private String employee_ids;

    @JsonIgnore
    private boolean employee_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Res_usersStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [MODERATION_CHANNEL_IDS]
     *
     */
    @Res_usersModeration_channel_idsDefault(info = "默认规则")
    private String moderation_channel_ids;

    @JsonIgnore
    private boolean moderation_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Res_usersMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [SILVER_BADGE]
     *
     */
    @Res_usersSilver_badgeDefault(info = "默认规则")
    private Integer silver_badge;

    @JsonIgnore
    private boolean silver_badgeDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_IDS]
     *
     */
    @Res_usersPayment_token_idsDefault(info = "默认规则")
    private String payment_token_ids;

    @JsonIgnore
    private boolean payment_token_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_usersDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [COMPANIES_COUNT]
     *
     */
    @Res_usersCompanies_countDefault(info = "默认规则")
    private Integer companies_count;

    @JsonIgnore
    private boolean companies_countDirtyFlag;

    /**
     * 属性 [TARGET_SALES_INVOICED]
     *
     */
    @Res_usersTarget_sales_invoicedDefault(info = "默认规则")
    private Integer target_sales_invoiced;

    @JsonIgnore
    private boolean target_sales_invoicedDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Res_usersMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [LOGIN_DATE]
     *
     */
    @Res_usersLogin_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp login_date;

    @JsonIgnore
    private boolean login_dateDirtyFlag;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @Res_usersChannel_idsDefault(info = "默认规则")
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;

    /**
     * 属性 [GROUPS_ID]
     *
     */
    @Res_usersGroups_idDefault(info = "默认规则")
    private String groups_id;

    @JsonIgnore
    private boolean groups_idDirtyFlag;

    /**
     * 属性 [SHARE]
     *
     */
    @Res_usersShareDefault(info = "默认规则")
    private String share;

    @JsonIgnore
    private boolean shareDirtyFlag;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @Res_usersBank_idsDefault(info = "默认规则")
    private String bank_ids;

    @JsonIgnore
    private boolean bank_idsDirtyFlag;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @Res_usersSale_order_idsDefault(info = "默认规则")
    private String sale_order_ids;

    @JsonIgnore
    private boolean sale_order_idsDirtyFlag;

    /**
     * 属性 [NEW_PASSWORD]
     *
     */
    @Res_usersNew_passwordDefault(info = "默认规则")
    private String new_password;

    @JsonIgnore
    private boolean new_passwordDirtyFlag;

    /**
     * 属性 [ODOOBOT_STATE]
     *
     */
    @Res_usersOdoobot_stateDefault(info = "默认规则")
    private String odoobot_state;

    @JsonIgnore
    private boolean odoobot_stateDirtyFlag;

    /**
     * 属性 [REF_COMPANY_IDS]
     *
     */
    @Res_usersRef_company_idsDefault(info = "默认规则")
    private String ref_company_ids;

    @JsonIgnore
    private boolean ref_company_idsDirtyFlag;

    /**
     * 属性 [PASSWORD]
     *
     */
    @Res_usersPasswordDefault(info = "默认规则")
    private String password;

    @JsonIgnore
    private boolean passwordDirtyFlag;

    /**
     * 属性 [BRONZE_BADGE]
     *
     */
    @Res_usersBronze_badgeDefault(info = "默认规则")
    private Integer bronze_badge;

    @JsonIgnore
    private boolean bronze_badgeDirtyFlag;

    /**
     * 属性 [MEETING_IDS]
     *
     */
    @Res_usersMeeting_idsDefault(info = "默认规则")
    private String meeting_ids;

    @JsonIgnore
    private boolean meeting_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_users__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [FORUM_WAITING_POSTS_COUNT]
     *
     */
    @Res_usersForum_waiting_posts_countDefault(info = "默认规则")
    private Integer forum_waiting_posts_count;

    @JsonIgnore
    private boolean forum_waiting_posts_countDirtyFlag;

    /**
     * 属性 [GOAL_IDS]
     *
     */
    @Res_usersGoal_idsDefault(info = "默认规则")
    private String goal_ids;

    @JsonIgnore
    private boolean goal_idsDirtyFlag;

    /**
     * 属性 [TARGET_SALES_WON]
     *
     */
    @Res_usersTarget_sales_wonDefault(info = "默认规则")
    private Integer target_sales_won;

    @JsonIgnore
    private boolean target_sales_wonDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Res_usersWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [ACTION_ID]
     *
     */
    @Res_usersAction_idDefault(info = "默认规则")
    private Integer action_id;

    @JsonIgnore
    private boolean action_idDirtyFlag;

    /**
     * 属性 [LOGIN]
     *
     */
    @Res_usersLoginDefault(info = "默认规则")
    private String login;

    @JsonIgnore
    private boolean loginDirtyFlag;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @Res_usersContract_idsDefault(info = "默认规则")
    private String contract_ids;

    @JsonIgnore
    private boolean contract_idsDirtyFlag;

    /**
     * 属性 [MODERATION_COUNTER]
     *
     */
    @Res_usersModeration_counterDefault(info = "默认规则")
    private Integer moderation_counter;

    @JsonIgnore
    private boolean moderation_counterDirtyFlag;

    /**
     * 属性 [SIGNATURE]
     *
     */
    @Res_usersSignatureDefault(info = "默认规则")
    private String signature;

    @JsonIgnore
    private boolean signatureDirtyFlag;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Res_usersUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_IDS]
     *
     */
    @Res_usersOpportunity_idsDefault(info = "默认规则")
    private String opportunity_ids;

    @JsonIgnore
    private boolean opportunity_idsDirtyFlag;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @Res_usersTask_idsDefault(info = "默认规则")
    private String task_ids;

    @JsonIgnore
    private boolean task_idsDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Res_usersInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [REF]
     *
     */
    @Res_usersRefDefault(info = "默认规则")
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Res_usersMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [LAST_WEBSITE_SO_ID]
     *
     */
    @Res_usersLast_website_so_idDefault(info = "默认规则")
    private Integer last_website_so_id;

    @JsonIgnore
    private boolean last_website_so_idDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Res_usersDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Res_usersMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER]
     *
     */
    @Res_usersProperty_stock_customerDefault(info = "默认规则")
    private Integer property_stock_customer;

    @JsonIgnore
    private boolean property_stock_customerDirtyFlag;

    /**
     * 属性 [LAST_TIME_ENTRIES_CHECKED]
     *
     */
    @Res_usersLast_time_entries_checkedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_time_entries_checked;

    @JsonIgnore
    private boolean last_time_entries_checkedDirtyFlag;

    /**
     * 属性 [LANG]
     *
     */
    @Res_usersLangDefault(info = "默认规则")
    private String lang;

    @JsonIgnore
    private boolean langDirtyFlag;

    /**
     * 属性 [SALE_WARN]
     *
     */
    @Res_usersSale_warnDefault(info = "默认规则")
    private String sale_warn;

    @JsonIgnore
    private boolean sale_warnDirtyFlag;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @Res_usersMeeting_countDefault(info = "默认规则")
    private Integer meeting_count;

    @JsonIgnore
    private boolean meeting_countDirtyFlag;

    /**
     * 属性 [STREET]
     *
     */
    @Res_usersStreetDefault(info = "默认规则")
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;

    /**
     * 属性 [INVOICE_WARN]
     *
     */
    @Res_usersInvoice_warnDefault(info = "默认规则")
    private String invoice_warn;

    @JsonIgnore
    private boolean invoice_warnDirtyFlag;

    /**
     * 属性 [SIGNUP_TOKEN]
     *
     */
    @Res_usersSignup_tokenDefault(info = "默认规则")
    private String signup_token;

    @JsonIgnore
    private boolean signup_tokenDirtyFlag;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @Res_usersTask_countDefault(info = "默认规则")
    private Integer task_count;

    @JsonIgnore
    private boolean task_countDirtyFlag;

    /**
     * 属性 [SIGNUP_VALID]
     *
     */
    @Res_usersSignup_validDefault(info = "默认规则")
    private String signup_valid;

    @JsonIgnore
    private boolean signup_validDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Res_usersMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [SIGNUP_TYPE]
     *
     */
    @Res_usersSignup_typeDefault(info = "默认规则")
    private String signup_type;

    @JsonIgnore
    private boolean signup_typeDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @Res_usersProperty_account_receivable_idDefault(info = "默认规则")
    private Integer property_account_receivable_id;

    @JsonIgnore
    private boolean property_account_receivable_idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Res_usersWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [EVENT_COUNT]
     *
     */
    @Res_usersEvent_countDefault(info = "默认规则")
    private Integer event_count;

    @JsonIgnore
    private boolean event_countDirtyFlag;

    /**
     * 属性 [JOURNAL_ITEM_COUNT]
     *
     */
    @Res_usersJournal_item_countDefault(info = "默认规则")
    private Integer journal_item_count;

    @JsonIgnore
    private boolean journal_item_countDirtyFlag;

    /**
     * 属性 [PARENT_NAME]
     *
     */
    @Res_usersParent_nameDefault(info = "默认规则")
    private String parent_name;

    @JsonIgnore
    private boolean parent_nameDirtyFlag;

    /**
     * 属性 [SALE_TEAM_ID_TEXT]
     *
     */
    @Res_usersSale_team_id_textDefault(info = "默认规则")
    private String sale_team_id_text;

    @JsonIgnore
    private boolean sale_team_id_textDirtyFlag;

    /**
     * 属性 [CREDIT]
     *
     */
    @Res_usersCreditDefault(info = "默认规则")
    private Double credit;

    @JsonIgnore
    private boolean creditDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_COUNT]
     *
     */
    @Res_usersOpportunity_countDefault(info = "默认规则")
    private Integer opportunity_count;

    @JsonIgnore
    private boolean opportunity_countDirtyFlag;

    /**
     * 属性 [SIGNUP_URL]
     *
     */
    @Res_usersSignup_urlDefault(info = "默认规则")
    private String signup_url;

    @JsonIgnore
    private boolean signup_urlDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @Res_usersProperty_account_payable_idDefault(info = "默认规则")
    private Integer property_account_payable_id;

    @JsonIgnore
    private boolean property_account_payable_idDirtyFlag;

    /**
     * 属性 [STATE_ID]
     *
     */
    @Res_usersState_idDefault(info = "默认规则")
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Res_usersMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [SALE_WARN_MSG]
     *
     */
    @Res_usersSale_warn_msgDefault(info = "默认规则")
    private String sale_warn_msg;

    @JsonIgnore
    private boolean sale_warn_msgDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Res_usersWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [TOTAL_INVOICED]
     *
     */
    @Res_usersTotal_invoicedDefault(info = "默认规则")
    private Double total_invoiced;

    @JsonIgnore
    private boolean total_invoicedDirtyFlag;

    /**
     * 属性 [DEBIT]
     *
     */
    @Res_usersDebitDefault(info = "默认规则")
    private Double debit;

    @JsonIgnore
    private boolean debitDirtyFlag;

    /**
     * 属性 [POS_ORDER_COUNT]
     *
     */
    @Res_usersPos_order_countDefault(info = "默认规则")
    private Integer pos_order_count;

    @JsonIgnore
    private boolean pos_order_countDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_usersWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TITLE]
     *
     */
    @Res_usersTitleDefault(info = "默认规则")
    private Integer title;

    @JsonIgnore
    private boolean titleDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Res_usersMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [SUPPLIER_INVOICE_COUNT]
     *
     */
    @Res_usersSupplier_invoice_countDefault(info = "默认规则")
    private Integer supplier_invoice_count;

    @JsonIgnore
    private boolean supplier_invoice_countDirtyFlag;

    /**
     * 属性 [CITY]
     *
     */
    @Res_usersCityDefault(info = "默认规则")
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;

    /**
     * 属性 [PICKING_WARN]
     *
     */
    @Res_usersPicking_warnDefault(info = "默认规则")
    private String picking_warn;

    @JsonIgnore
    private boolean picking_warnDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Res_usersMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Res_usersActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ADDITIONAL_INFO]
     *
     */
    @Res_usersAdditional_infoDefault(info = "默认规则")
    private String additional_info;

    @JsonIgnore
    private boolean additional_infoDirtyFlag;

    /**
     * 属性 [IBIZFUNCTION]
     *
     */
    @Res_usersIbizfunctionDefault(info = "默认规则")
    private String ibizfunction;

    @JsonIgnore
    private boolean ibizfunctionDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Res_usersWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [CALENDAR_LAST_NOTIF_ACK]
     *
     */
    @Res_usersCalendar_last_notif_ackDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp calendar_last_notif_ack;

    @JsonIgnore
    private boolean calendar_last_notif_ackDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Res_usersMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Res_usersActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [EMPLOYEE]
     *
     */
    @Res_usersEmployeeDefault(info = "默认规则")
    private String employee;

    @JsonIgnore
    private boolean employeeDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Res_usersActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Res_usersBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @Res_usersPartner_gidDefault(info = "默认规则")
    private Integer partner_gid;

    @JsonIgnore
    private boolean partner_gidDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Res_usersTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [VAT]
     *
     */
    @Res_usersVatDefault(info = "默认规则")
    private String vat;

    @JsonIgnore
    private boolean vatDirtyFlag;

    /**
     * 属性 [PURCHASE_WARN_MSG]
     *
     */
    @Res_usersPurchase_warn_msgDefault(info = "默认规则")
    private String purchase_warn_msg;

    @JsonIgnore
    private boolean purchase_warn_msgDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Res_usersCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [SUPPLIER]
     *
     */
    @Res_usersSupplierDefault(info = "默认规则")
    private String supplier;

    @JsonIgnore
    private boolean supplierDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Res_usersWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Res_usersParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [PURCHASE_WARN]
     *
     */
    @Res_usersPurchase_warnDefault(info = "默认规则")
    private String purchase_warn;

    @JsonIgnore
    private boolean purchase_warnDirtyFlag;

    /**
     * 属性 [ACTIVE_PARTNER]
     *
     */
    @Res_usersActive_partnerDefault(info = "默认规则")
    private String active_partner;

    @JsonIgnore
    private boolean active_partnerDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_usersCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [INDUSTRY_ID]
     *
     */
    @Res_usersIndustry_idDefault(info = "默认规则")
    private Integer industry_id;

    @JsonIgnore
    private boolean industry_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER]
     *
     */
    @Res_usersProperty_stock_supplierDefault(info = "默认规则")
    private Integer property_stock_supplier;

    @JsonIgnore
    private boolean property_stock_supplierDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_COUNT]
     *
     */
    @Res_usersPayment_token_countDefault(info = "默认规则")
    private Integer payment_token_count;

    @JsonIgnore
    private boolean payment_token_countDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Res_usersMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Res_usersUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [CUSTOMER]
     *
     */
    @Res_usersCustomerDefault(info = "默认规则")
    private String customer;

    @JsonIgnore
    private boolean customerDirtyFlag;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_ID]
     *
     */
    @Res_usersProperty_payment_term_idDefault(info = "默认规则")
    private Integer property_payment_term_id;

    @JsonIgnore
    private boolean property_payment_term_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Res_usersActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @Res_usersContracts_countDefault(info = "默认规则")
    private Integer contracts_count;

    @JsonIgnore
    private boolean contracts_countDirtyFlag;

    /**
     * 属性 [SELF]
     *
     */
    @Res_usersSelfDefault(info = "默认规则")
    private Integer self;

    @JsonIgnore
    private boolean selfDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Res_usersWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Res_usersImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Res_usersEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Res_usersImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Res_usersActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [DEBIT_LIMIT]
     *
     */
    @Res_usersDebit_limitDefault(info = "默认规则")
    private Double debit_limit;

    @JsonIgnore
    private boolean debit_limitDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Res_usersCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [CREDIT_LIMIT]
     *
     */
    @Res_usersCredit_limitDefault(info = "默认规则")
    private Double credit_limit;

    @JsonIgnore
    private boolean credit_limitDirtyFlag;

    /**
     * 属性 [COMMERCIAL_COMPANY_NAME]
     *
     */
    @Res_usersCommercial_company_nameDefault(info = "默认规则")
    private String commercial_company_name;

    @JsonIgnore
    private boolean commercial_company_nameDirtyFlag;

    /**
     * 属性 [INVOICE_WARN_MSG]
     *
     */
    @Res_usersInvoice_warn_msgDefault(info = "默认规则")
    private String invoice_warn_msg;

    @JsonIgnore
    private boolean invoice_warn_msgDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Res_usersIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [TRUST]
     *
     */
    @Res_usersTrustDefault(info = "默认规则")
    private String trust;

    @JsonIgnore
    private boolean trustDirtyFlag;

    /**
     * 属性 [MOBILE]
     *
     */
    @Res_usersMobileDefault(info = "默认规则")
    private String mobile;

    @JsonIgnore
    private boolean mobileDirtyFlag;

    /**
     * 属性 [EMAIL_FORMATTED]
     *
     */
    @Res_usersEmail_formattedDefault(info = "默认规则")
    private String email_formatted;

    @JsonIgnore
    private boolean email_formattedDirtyFlag;

    /**
     * 属性 [IS_COMPANY]
     *
     */
    @Res_usersIs_companyDefault(info = "默认规则")
    private String is_company;

    @JsonIgnore
    private boolean is_companyDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_usersCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Res_usersTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @Res_usersIs_blacklistedDefault(info = "默认规则")
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_COUNT]
     *
     */
    @Res_usersBank_account_countDefault(info = "默认规则")
    private Integer bank_account_count;

    @JsonIgnore
    private boolean bank_account_countDirtyFlag;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST]
     *
     */
    @Res_usersProperty_product_pricelistDefault(info = "默认规则")
    private Integer property_product_pricelist;

    @JsonIgnore
    private boolean property_product_pricelistDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_usersNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PICKING_WARN_MSG]
     *
     */
    @Res_usersPicking_warn_msgDefault(info = "默认规则")
    private String picking_warn_msg;

    @JsonIgnore
    private boolean picking_warn_msgDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Res_usersMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WEBSITE]
     *
     */
    @Res_usersWebsiteDefault(info = "默认规则")
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Res_usersPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [STREET2]
     *
     */
    @Res_usersStreet2Default(info = "默认规则")
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;

    /**
     * 属性 [HAS_UNRECONCILED_ENTRIES]
     *
     */
    @Res_usersHas_unreconciled_entriesDefault(info = "默认规则")
    private String has_unreconciled_entries;

    @JsonIgnore
    private boolean has_unreconciled_entriesDirtyFlag;

    /**
     * 属性 [SIGNUP_EXPIRATION]
     *
     */
    @Res_usersSignup_expirationDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp signup_expiration;

    @JsonIgnore
    private boolean signup_expirationDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Res_usersTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [CONTACT_ADDRESS]
     *
     */
    @Res_usersContact_addressDefault(info = "默认规则")
    private String contact_address;

    @JsonIgnore
    private boolean contact_addressDirtyFlag;

    /**
     * 属性 [WEBSITE_SHORT_DESCRIPTION]
     *
     */
    @Res_usersWebsite_short_descriptionDefault(info = "默认规则")
    private String website_short_description;

    @JsonIgnore
    private boolean website_short_descriptionDirtyFlag;

    /**
     * 属性 [PARTNER_SHARE]
     *
     */
    @Res_usersPartner_shareDefault(info = "默认规则")
    private String partner_share;

    @JsonIgnore
    private boolean partner_shareDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Res_usersCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Res_usersColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [ZIP]
     *
     */
    @Res_usersZipDefault(info = "默认规则")
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;

    /**
     * 属性 [SALE_ORDER_COUNT]
     *
     */
    @Res_usersSale_order_countDefault(info = "默认规则")
    private Integer sale_order_count;

    @JsonIgnore
    private boolean sale_order_countDirtyFlag;

    /**
     * 属性 [COMPANY_TYPE]
     *
     */
    @Res_usersCompany_typeDefault(info = "默认规则")
    private String company_type;

    @JsonIgnore
    private boolean company_typeDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_ID]
     *
     */
    @Res_usersProperty_account_position_idDefault(info = "默认规则")
    private Integer property_account_position_id;

    @JsonIgnore
    private boolean property_account_position_idDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Res_usersIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @Res_usersMessage_bounceDefault(info = "默认规则")
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Res_usersWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Res_usersImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_ID]
     *
     */
    @Res_usersProperty_purchase_currency_idDefault(info = "默认规则")
    private Integer property_purchase_currency_id;

    @JsonIgnore
    private boolean property_purchase_currency_idDirtyFlag;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @Res_usersPurchase_order_countDefault(info = "默认规则")
    private Integer purchase_order_count;

    @JsonIgnore
    private boolean purchase_order_countDirtyFlag;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @Res_usersWebsite_descriptionDefault(info = "默认规则")
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     *
     */
    @Res_usersProperty_supplier_payment_term_idDefault(info = "默认规则")
    private Integer property_supplier_payment_term_id;

    @JsonIgnore
    private boolean property_supplier_payment_term_idDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Res_usersCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @Res_usersAlias_contactDefault(info = "默认规则")
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @Res_usersCompany_nameDefault(info = "默认规则")
    private String company_name;

    @JsonIgnore
    private boolean company_nameDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Res_usersCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Res_usersPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Res_usersAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;

    /**
     * 属性 [SALE_TEAM_ID]
     *
     */
    @Res_usersSale_team_idDefault(info = "默认规则")
    private Integer sale_team_id;

    @JsonIgnore
    private boolean sale_team_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_usersWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_usersCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [IS_MODERATOR]
     */
    @JsonProperty("is_moderator")
    public String getIs_moderator(){
        return is_moderator ;
    }

    /**
     * 设置 [IS_MODERATOR]
     */
    @JsonProperty("is_moderator")
    public void setIs_moderator(String  is_moderator){
        this.is_moderator = is_moderator ;
        this.is_moderatorDirtyFlag = true ;
    }

    /**
     * 获取 [IS_MODERATOR]脏标记
     */
    @JsonIgnore
    public boolean getIs_moderatorDirtyFlag(){
        return is_moderatorDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_IDS]
     */
    @JsonProperty("resource_ids")
    public String getResource_ids(){
        return resource_ids ;
    }

    /**
     * 设置 [RESOURCE_IDS]
     */
    @JsonProperty("resource_ids")
    public void setResource_ids(String  resource_ids){
        this.resource_ids = resource_ids ;
        this.resource_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getResource_idsDirtyFlag(){
        return resource_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public String getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(String  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [LOG_IDS]
     */
    @JsonProperty("log_ids")
    public String getLog_ids(){
        return log_ids ;
    }

    /**
     * 设置 [LOG_IDS]
     */
    @JsonProperty("log_ids")
    public void setLog_ids(String  log_ids){
        this.log_ids = log_ids ;
        this.log_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LOG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLog_idsDirtyFlag(){
        return log_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [POS_SECURITY_PIN]
     */
    @JsonProperty("pos_security_pin")
    public String getPos_security_pin(){
        return pos_security_pin ;
    }

    /**
     * 设置 [POS_SECURITY_PIN]
     */
    @JsonProperty("pos_security_pin")
    public void setPos_security_pin(String  pos_security_pin){
        this.pos_security_pin = pos_security_pin ;
        this.pos_security_pinDirtyFlag = true ;
    }

    /**
     * 获取 [POS_SECURITY_PIN]脏标记
     */
    @JsonIgnore
    public boolean getPos_security_pinDirtyFlag(){
        return pos_security_pinDirtyFlag ;
    }

    /**
     * 获取 [BADGE_IDS]
     */
    @JsonProperty("badge_ids")
    public String getBadge_ids(){
        return badge_ids ;
    }

    /**
     * 设置 [BADGE_IDS]
     */
    @JsonProperty("badge_ids")
    public void setBadge_ids(String  badge_ids){
        this.badge_ids = badge_ids ;
        this.badge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idsDirtyFlag(){
        return badge_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_IDS]
     */
    @JsonProperty("company_ids")
    public String getCompany_ids(){
        return company_ids ;
    }

    /**
     * 设置 [COMPANY_IDS]
     */
    @JsonProperty("company_ids")
    public void setCompany_ids(String  company_ids){
        this.company_ids = company_ids ;
        this.company_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idsDirtyFlag(){
        return company_idsDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [TZ_OFFSET]
     */
    @JsonProperty("tz_offset")
    public String getTz_offset(){
        return tz_offset ;
    }

    /**
     * 设置 [TZ_OFFSET]
     */
    @JsonProperty("tz_offset")
    public void setTz_offset(String  tz_offset){
        this.tz_offset = tz_offset ;
        this.tz_offsetDirtyFlag = true ;
    }

    /**
     * 获取 [TZ_OFFSET]脏标记
     */
    @JsonIgnore
    public boolean getTz_offsetDirtyFlag(){
        return tz_offsetDirtyFlag ;
    }

    /**
     * 获取 [TARGET_SALES_DONE]
     */
    @JsonProperty("target_sales_done")
    public Integer getTarget_sales_done(){
        return target_sales_done ;
    }

    /**
     * 设置 [TARGET_SALES_DONE]
     */
    @JsonProperty("target_sales_done")
    public void setTarget_sales_done(Integer  target_sales_done){
        this.target_sales_done = target_sales_done ;
        this.target_sales_doneDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_SALES_DONE]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_doneDirtyFlag(){
        return target_sales_doneDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [NOTIFICATION_TYPE]
     */
    @JsonProperty("notification_type")
    public String getNotification_type(){
        return notification_type ;
    }

    /**
     * 设置 [NOTIFICATION_TYPE]
     */
    @JsonProperty("notification_type")
    public void setNotification_type(String  notification_type){
        this.notification_type = notification_type ;
        this.notification_typeDirtyFlag = true ;
    }

    /**
     * 获取 [NOTIFICATION_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getNotification_typeDirtyFlag(){
        return notification_typeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [IM_STATUS]
     */
    @JsonProperty("im_status")
    public String getIm_status(){
        return im_status ;
    }

    /**
     * 设置 [IM_STATUS]
     */
    @JsonProperty("im_status")
    public void setIm_status(String  im_status){
        this.im_status = im_status ;
        this.im_statusDirtyFlag = true ;
    }

    /**
     * 获取 [IM_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getIm_statusDirtyFlag(){
        return im_statusDirtyFlag ;
    }

    /**
     * 获取 [KARMA]
     */
    @JsonProperty("karma")
    public Integer getKarma(){
        return karma ;
    }

    /**
     * 设置 [KARMA]
     */
    @JsonProperty("karma")
    public void setKarma(Integer  karma){
        this.karma = karma ;
        this.karmaDirtyFlag = true ;
    }

    /**
     * 获取 [KARMA]脏标记
     */
    @JsonIgnore
    public boolean getKarmaDirtyFlag(){
        return karmaDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [GOLD_BADGE]
     */
    @JsonProperty("gold_badge")
    public Integer getGold_badge(){
        return gold_badge ;
    }

    /**
     * 设置 [GOLD_BADGE]
     */
    @JsonProperty("gold_badge")
    public void setGold_badge(Integer  gold_badge){
        this.gold_badge = gold_badge ;
        this.gold_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [GOLD_BADGE]脏标记
     */
    @JsonIgnore
    public boolean getGold_badgeDirtyFlag(){
        return gold_badgeDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_IDS]
     */
    @JsonProperty("employee_ids")
    public String getEmployee_ids(){
        return employee_ids ;
    }

    /**
     * 设置 [EMPLOYEE_IDS]
     */
    @JsonProperty("employee_ids")
    public void setEmployee_ids(String  employee_ids){
        this.employee_ids = employee_ids ;
        this.employee_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idsDirtyFlag(){
        return employee_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_CHANNEL_IDS]
     */
    @JsonProperty("moderation_channel_ids")
    public String getModeration_channel_ids(){
        return moderation_channel_ids ;
    }

    /**
     * 设置 [MODERATION_CHANNEL_IDS]
     */
    @JsonProperty("moderation_channel_ids")
    public void setModeration_channel_ids(String  moderation_channel_ids){
        this.moderation_channel_ids = moderation_channel_ids ;
        this.moderation_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModeration_channel_idsDirtyFlag(){
        return moderation_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [SILVER_BADGE]
     */
    @JsonProperty("silver_badge")
    public Integer getSilver_badge(){
        return silver_badge ;
    }

    /**
     * 设置 [SILVER_BADGE]
     */
    @JsonProperty("silver_badge")
    public void setSilver_badge(Integer  silver_badge){
        this.silver_badge = silver_badge ;
        this.silver_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [SILVER_BADGE]脏标记
     */
    @JsonIgnore
    public boolean getSilver_badgeDirtyFlag(){
        return silver_badgeDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_IDS]
     */
    @JsonProperty("payment_token_ids")
    public String getPayment_token_ids(){
        return payment_token_ids ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_IDS]
     */
    @JsonProperty("payment_token_ids")
    public void setPayment_token_ids(String  payment_token_ids){
        this.payment_token_ids = payment_token_ids ;
        this.payment_token_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idsDirtyFlag(){
        return payment_token_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [COMPANIES_COUNT]
     */
    @JsonProperty("companies_count")
    public Integer getCompanies_count(){
        return companies_count ;
    }

    /**
     * 设置 [COMPANIES_COUNT]
     */
    @JsonProperty("companies_count")
    public void setCompanies_count(Integer  companies_count){
        this.companies_count = companies_count ;
        this.companies_countDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANIES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getCompanies_countDirtyFlag(){
        return companies_countDirtyFlag ;
    }

    /**
     * 获取 [TARGET_SALES_INVOICED]
     */
    @JsonProperty("target_sales_invoiced")
    public Integer getTarget_sales_invoiced(){
        return target_sales_invoiced ;
    }

    /**
     * 设置 [TARGET_SALES_INVOICED]
     */
    @JsonProperty("target_sales_invoiced")
    public void setTarget_sales_invoiced(Integer  target_sales_invoiced){
        this.target_sales_invoiced = target_sales_invoiced ;
        this.target_sales_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_SALES_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_invoicedDirtyFlag(){
        return target_sales_invoicedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [LOGIN_DATE]
     */
    @JsonProperty("login_date")
    public Timestamp getLogin_date(){
        return login_date ;
    }

    /**
     * 设置 [LOGIN_DATE]
     */
    @JsonProperty("login_date")
    public void setLogin_date(Timestamp  login_date){
        this.login_date = login_date ;
        this.login_dateDirtyFlag = true ;
    }

    /**
     * 获取 [LOGIN_DATE]脏标记
     */
    @JsonIgnore
    public boolean getLogin_dateDirtyFlag(){
        return login_dateDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return channel_ids ;
    }

    /**
     * 设置 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return channel_idsDirtyFlag ;
    }

    /**
     * 获取 [GROUPS_ID]
     */
    @JsonProperty("groups_id")
    public String getGroups_id(){
        return groups_id ;
    }

    /**
     * 设置 [GROUPS_ID]
     */
    @JsonProperty("groups_id")
    public void setGroups_id(String  groups_id){
        this.groups_id = groups_id ;
        this.groups_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUPS_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroups_idDirtyFlag(){
        return groups_idDirtyFlag ;
    }

    /**
     * 获取 [SHARE]
     */
    @JsonProperty("share")
    public String getShare(){
        return share ;
    }

    /**
     * 设置 [SHARE]
     */
    @JsonProperty("share")
    public void setShare(String  share){
        this.share = share ;
        this.shareDirtyFlag = true ;
    }

    /**
     * 获取 [SHARE]脏标记
     */
    @JsonIgnore
    public boolean getShareDirtyFlag(){
        return shareDirtyFlag ;
    }

    /**
     * 获取 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return bank_ids ;
    }

    /**
     * 设置 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return bank_idsDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return sale_order_ids ;
    }

    /**
     * 设置 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return sale_order_idsDirtyFlag ;
    }

    /**
     * 获取 [NEW_PASSWORD]
     */
    @JsonProperty("new_password")
    public String getNew_password(){
        return new_password ;
    }

    /**
     * 设置 [NEW_PASSWORD]
     */
    @JsonProperty("new_password")
    public void setNew_password(String  new_password){
        this.new_password = new_password ;
        this.new_passwordDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_PASSWORD]脏标记
     */
    @JsonIgnore
    public boolean getNew_passwordDirtyFlag(){
        return new_passwordDirtyFlag ;
    }

    /**
     * 获取 [ODOOBOT_STATE]
     */
    @JsonProperty("odoobot_state")
    public String getOdoobot_state(){
        return odoobot_state ;
    }

    /**
     * 设置 [ODOOBOT_STATE]
     */
    @JsonProperty("odoobot_state")
    public void setOdoobot_state(String  odoobot_state){
        this.odoobot_state = odoobot_state ;
        this.odoobot_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ODOOBOT_STATE]脏标记
     */
    @JsonIgnore
    public boolean getOdoobot_stateDirtyFlag(){
        return odoobot_stateDirtyFlag ;
    }

    /**
     * 获取 [REF_COMPANY_IDS]
     */
    @JsonProperty("ref_company_ids")
    public String getRef_company_ids(){
        return ref_company_ids ;
    }

    /**
     * 设置 [REF_COMPANY_IDS]
     */
    @JsonProperty("ref_company_ids")
    public void setRef_company_ids(String  ref_company_ids){
        this.ref_company_ids = ref_company_ids ;
        this.ref_company_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REF_COMPANY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRef_company_idsDirtyFlag(){
        return ref_company_idsDirtyFlag ;
    }

    /**
     * 获取 [PASSWORD]
     */
    @JsonProperty("password")
    public String getPassword(){
        return password ;
    }

    /**
     * 设置 [PASSWORD]
     */
    @JsonProperty("password")
    public void setPassword(String  password){
        this.password = password ;
        this.passwordDirtyFlag = true ;
    }

    /**
     * 获取 [PASSWORD]脏标记
     */
    @JsonIgnore
    public boolean getPasswordDirtyFlag(){
        return passwordDirtyFlag ;
    }

    /**
     * 获取 [BRONZE_BADGE]
     */
    @JsonProperty("bronze_badge")
    public Integer getBronze_badge(){
        return bronze_badge ;
    }

    /**
     * 设置 [BRONZE_BADGE]
     */
    @JsonProperty("bronze_badge")
    public void setBronze_badge(Integer  bronze_badge){
        this.bronze_badge = bronze_badge ;
        this.bronze_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [BRONZE_BADGE]脏标记
     */
    @JsonIgnore
    public boolean getBronze_badgeDirtyFlag(){
        return bronze_badgeDirtyFlag ;
    }

    /**
     * 获取 [MEETING_IDS]
     */
    @JsonProperty("meeting_ids")
    public String getMeeting_ids(){
        return meeting_ids ;
    }

    /**
     * 设置 [MEETING_IDS]
     */
    @JsonProperty("meeting_ids")
    public void setMeeting_ids(String  meeting_ids){
        this.meeting_ids = meeting_ids ;
        this.meeting_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MEETING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_idsDirtyFlag(){
        return meeting_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [FORUM_WAITING_POSTS_COUNT]
     */
    @JsonProperty("forum_waiting_posts_count")
    public Integer getForum_waiting_posts_count(){
        return forum_waiting_posts_count ;
    }

    /**
     * 设置 [FORUM_WAITING_POSTS_COUNT]
     */
    @JsonProperty("forum_waiting_posts_count")
    public void setForum_waiting_posts_count(Integer  forum_waiting_posts_count){
        this.forum_waiting_posts_count = forum_waiting_posts_count ;
        this.forum_waiting_posts_countDirtyFlag = true ;
    }

    /**
     * 获取 [FORUM_WAITING_POSTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getForum_waiting_posts_countDirtyFlag(){
        return forum_waiting_posts_countDirtyFlag ;
    }

    /**
     * 获取 [GOAL_IDS]
     */
    @JsonProperty("goal_ids")
    public String getGoal_ids(){
        return goal_ids ;
    }

    /**
     * 设置 [GOAL_IDS]
     */
    @JsonProperty("goal_ids")
    public void setGoal_ids(String  goal_ids){
        this.goal_ids = goal_ids ;
        this.goal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GOAL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGoal_idsDirtyFlag(){
        return goal_idsDirtyFlag ;
    }

    /**
     * 获取 [TARGET_SALES_WON]
     */
    @JsonProperty("target_sales_won")
    public Integer getTarget_sales_won(){
        return target_sales_won ;
    }

    /**
     * 设置 [TARGET_SALES_WON]
     */
    @JsonProperty("target_sales_won")
    public void setTarget_sales_won(Integer  target_sales_won){
        this.target_sales_won = target_sales_won ;
        this.target_sales_wonDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_SALES_WON]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_wonDirtyFlag(){
        return target_sales_wonDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTION_ID]
     */
    @JsonProperty("action_id")
    public Integer getAction_id(){
        return action_id ;
    }

    /**
     * 设置 [ACTION_ID]
     */
    @JsonProperty("action_id")
    public void setAction_id(Integer  action_id){
        this.action_id = action_id ;
        this.action_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getAction_idDirtyFlag(){
        return action_idDirtyFlag ;
    }

    /**
     * 获取 [LOGIN]
     */
    @JsonProperty("login")
    public String getLogin(){
        return login ;
    }

    /**
     * 设置 [LOGIN]
     */
    @JsonProperty("login")
    public void setLogin(String  login){
        this.login = login ;
        this.loginDirtyFlag = true ;
    }

    /**
     * 获取 [LOGIN]脏标记
     */
    @JsonIgnore
    public boolean getLoginDirtyFlag(){
        return loginDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return contract_ids ;
    }

    /**
     * 设置 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return contract_idsDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_COUNTER]
     */
    @JsonProperty("moderation_counter")
    public Integer getModeration_counter(){
        return moderation_counter ;
    }

    /**
     * 设置 [MODERATION_COUNTER]
     */
    @JsonProperty("moderation_counter")
    public void setModeration_counter(Integer  moderation_counter){
        this.moderation_counter = moderation_counter ;
        this.moderation_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getModeration_counterDirtyFlag(){
        return moderation_counterDirtyFlag ;
    }

    /**
     * 获取 [SIGNATURE]
     */
    @JsonProperty("signature")
    public String getSignature(){
        return signature ;
    }

    /**
     * 设置 [SIGNATURE]
     */
    @JsonProperty("signature")
    public void setSignature(String  signature){
        this.signature = signature ;
        this.signatureDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNATURE]脏标记
     */
    @JsonIgnore
    public boolean getSignatureDirtyFlag(){
        return signatureDirtyFlag ;
    }

    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_IDS]
     */
    @JsonProperty("opportunity_ids")
    public String getOpportunity_ids(){
        return opportunity_ids ;
    }

    /**
     * 设置 [OPPORTUNITY_IDS]
     */
    @JsonProperty("opportunity_ids")
    public void setOpportunity_ids(String  opportunity_ids){
        this.opportunity_ids = opportunity_ids ;
        this.opportunity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idsDirtyFlag(){
        return opportunity_idsDirtyFlag ;
    }

    /**
     * 获取 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return task_ids ;
    }

    /**
     * 设置 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return task_idsDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [REF]
     */
    @JsonProperty("ref")
    public String getRef(){
        return ref ;
    }

    /**
     * 设置 [REF]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [REF]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return refDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [LAST_WEBSITE_SO_ID]
     */
    @JsonProperty("last_website_so_id")
    public Integer getLast_website_so_id(){
        return last_website_so_id ;
    }

    /**
     * 设置 [LAST_WEBSITE_SO_ID]
     */
    @JsonProperty("last_website_so_id")
    public void setLast_website_so_id(Integer  last_website_so_id){
        this.last_website_so_id = last_website_so_id ;
        this.last_website_so_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_WEBSITE_SO_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_website_so_idDirtyFlag(){
        return last_website_so_idDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_CUSTOMER]
     */
    @JsonProperty("property_stock_customer")
    public Integer getProperty_stock_customer(){
        return property_stock_customer ;
    }

    /**
     * 设置 [PROPERTY_STOCK_CUSTOMER]
     */
    @JsonProperty("property_stock_customer")
    public void setProperty_stock_customer(Integer  property_stock_customer){
        this.property_stock_customer = property_stock_customer ;
        this.property_stock_customerDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_CUSTOMER]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_customerDirtyFlag(){
        return property_stock_customerDirtyFlag ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return last_time_entries_checked ;
    }

    /**
     * 设置 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return last_time_entries_checkedDirtyFlag ;
    }

    /**
     * 获取 [LANG]
     */
    @JsonProperty("lang")
    public String getLang(){
        return lang ;
    }

    /**
     * 设置 [LANG]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

    /**
     * 获取 [LANG]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return langDirtyFlag ;
    }

    /**
     * 获取 [SALE_WARN]
     */
    @JsonProperty("sale_warn")
    public String getSale_warn(){
        return sale_warn ;
    }

    /**
     * 设置 [SALE_WARN]
     */
    @JsonProperty("sale_warn")
    public void setSale_warn(String  sale_warn){
        this.sale_warn = sale_warn ;
        this.sale_warnDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getSale_warnDirtyFlag(){
        return sale_warnDirtyFlag ;
    }

    /**
     * 获取 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return meeting_count ;
    }

    /**
     * 设置 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

    /**
     * 获取 [MEETING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return meeting_countDirtyFlag ;
    }

    /**
     * 获取 [STREET]
     */
    @JsonProperty("street")
    public String getStreet(){
        return street ;
    }

    /**
     * 设置 [STREET]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [STREET]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return streetDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_WARN]
     */
    @JsonProperty("invoice_warn")
    public String getInvoice_warn(){
        return invoice_warn ;
    }

    /**
     * 设置 [INVOICE_WARN]
     */
    @JsonProperty("invoice_warn")
    public void setInvoice_warn(String  invoice_warn){
        this.invoice_warn = invoice_warn ;
        this.invoice_warnDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warnDirtyFlag(){
        return invoice_warnDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_TOKEN]
     */
    @JsonProperty("signup_token")
    public String getSignup_token(){
        return signup_token ;
    }

    /**
     * 设置 [SIGNUP_TOKEN]
     */
    @JsonProperty("signup_token")
    public void setSignup_token(String  signup_token){
        this.signup_token = signup_token ;
        this.signup_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getSignup_tokenDirtyFlag(){
        return signup_tokenDirtyFlag ;
    }

    /**
     * 获取 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return task_count ;
    }

    /**
     * 设置 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return task_countDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_VALID]
     */
    @JsonProperty("signup_valid")
    public String getSignup_valid(){
        return signup_valid ;
    }

    /**
     * 设置 [SIGNUP_VALID]
     */
    @JsonProperty("signup_valid")
    public void setSignup_valid(String  signup_valid){
        this.signup_valid = signup_valid ;
        this.signup_validDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_VALID]脏标记
     */
    @JsonIgnore
    public boolean getSignup_validDirtyFlag(){
        return signup_validDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_TYPE]
     */
    @JsonProperty("signup_type")
    public String getSignup_type(){
        return signup_type ;
    }

    /**
     * 设置 [SIGNUP_TYPE]
     */
    @JsonProperty("signup_type")
    public void setSignup_type(String  signup_type){
        this.signup_type = signup_type ;
        this.signup_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getSignup_typeDirtyFlag(){
        return signup_typeDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return property_account_receivable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return property_account_receivable_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [EVENT_COUNT]
     */
    @JsonProperty("event_count")
    public Integer getEvent_count(){
        return event_count ;
    }

    /**
     * 设置 [EVENT_COUNT]
     */
    @JsonProperty("event_count")
    public void setEvent_count(Integer  event_count){
        this.event_count = event_count ;
        this.event_countDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_countDirtyFlag(){
        return event_countDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ITEM_COUNT]
     */
    @JsonProperty("journal_item_count")
    public Integer getJournal_item_count(){
        return journal_item_count ;
    }

    /**
     * 设置 [JOURNAL_ITEM_COUNT]
     */
    @JsonProperty("journal_item_count")
    public void setJournal_item_count(Integer  journal_item_count){
        this.journal_item_count = journal_item_count ;
        this.journal_item_countDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ITEM_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_item_countDirtyFlag(){
        return journal_item_countDirtyFlag ;
    }

    /**
     * 获取 [PARENT_NAME]
     */
    @JsonProperty("parent_name")
    public String getParent_name(){
        return parent_name ;
    }

    /**
     * 设置 [PARENT_NAME]
     */
    @JsonProperty("parent_name")
    public void setParent_name(String  parent_name){
        this.parent_name = parent_name ;
        this.parent_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getParent_nameDirtyFlag(){
        return parent_nameDirtyFlag ;
    }

    /**
     * 获取 [SALE_TEAM_ID_TEXT]
     */
    @JsonProperty("sale_team_id_text")
    public String getSale_team_id_text(){
        return sale_team_id_text ;
    }

    /**
     * 设置 [SALE_TEAM_ID_TEXT]
     */
    @JsonProperty("sale_team_id_text")
    public void setSale_team_id_text(String  sale_team_id_text){
        this.sale_team_id_text = sale_team_id_text ;
        this.sale_team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_team_id_textDirtyFlag(){
        return sale_team_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREDIT]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return credit ;
    }

    /**
     * 设置 [CREDIT]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return creditDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_COUNT]
     */
    @JsonProperty("opportunity_count")
    public Integer getOpportunity_count(){
        return opportunity_count ;
    }

    /**
     * 设置 [OPPORTUNITY_COUNT]
     */
    @JsonProperty("opportunity_count")
    public void setOpportunity_count(Integer  opportunity_count){
        this.opportunity_count = opportunity_count ;
        this.opportunity_countDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_countDirtyFlag(){
        return opportunity_countDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_URL]
     */
    @JsonProperty("signup_url")
    public String getSignup_url(){
        return signup_url ;
    }

    /**
     * 设置 [SIGNUP_URL]
     */
    @JsonProperty("signup_url")
    public void setSignup_url(String  signup_url){
        this.signup_url = signup_url ;
        this.signup_urlDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_URL]脏标记
     */
    @JsonIgnore
    public boolean getSignup_urlDirtyFlag(){
        return signup_urlDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return property_account_payable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return property_account_payable_idDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return state_id ;
    }

    /**
     * 设置 [STATE_ID]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return state_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [SALE_WARN_MSG]
     */
    @JsonProperty("sale_warn_msg")
    public String getSale_warn_msg(){
        return sale_warn_msg ;
    }

    /**
     * 设置 [SALE_WARN_MSG]
     */
    @JsonProperty("sale_warn_msg")
    public void setSale_warn_msg(String  sale_warn_msg){
        this.sale_warn_msg = sale_warn_msg ;
        this.sale_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getSale_warn_msgDirtyFlag(){
        return sale_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_INVOICED]
     */
    @JsonProperty("total_invoiced")
    public Double getTotal_invoiced(){
        return total_invoiced ;
    }

    /**
     * 设置 [TOTAL_INVOICED]
     */
    @JsonProperty("total_invoiced")
    public void setTotal_invoiced(Double  total_invoiced){
        this.total_invoiced = total_invoiced ;
        this.total_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getTotal_invoicedDirtyFlag(){
        return total_invoicedDirtyFlag ;
    }

    /**
     * 获取 [DEBIT]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return debit ;
    }

    /**
     * 设置 [DEBIT]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return debitDirtyFlag ;
    }

    /**
     * 获取 [POS_ORDER_COUNT]
     */
    @JsonProperty("pos_order_count")
    public Integer getPos_order_count(){
        return pos_order_count ;
    }

    /**
     * 设置 [POS_ORDER_COUNT]
     */
    @JsonProperty("pos_order_count")
    public void setPos_order_count(Integer  pos_order_count){
        this.pos_order_count = pos_order_count ;
        this.pos_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [POS_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_countDirtyFlag(){
        return pos_order_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TITLE]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return title ;
    }

    /**
     * 设置 [TITLE]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return titleDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER_INVOICE_COUNT]
     */
    @JsonProperty("supplier_invoice_count")
    public Integer getSupplier_invoice_count(){
        return supplier_invoice_count ;
    }

    /**
     * 设置 [SUPPLIER_INVOICE_COUNT]
     */
    @JsonProperty("supplier_invoice_count")
    public void setSupplier_invoice_count(Integer  supplier_invoice_count){
        this.supplier_invoice_count = supplier_invoice_count ;
        this.supplier_invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER_INVOICE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_invoice_countDirtyFlag(){
        return supplier_invoice_countDirtyFlag ;
    }

    /**
     * 获取 [CITY]
     */
    @JsonProperty("city")
    public String getCity(){
        return city ;
    }

    /**
     * 设置 [CITY]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [CITY]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return cityDirtyFlag ;
    }

    /**
     * 获取 [PICKING_WARN]
     */
    @JsonProperty("picking_warn")
    public String getPicking_warn(){
        return picking_warn ;
    }

    /**
     * 设置 [PICKING_WARN]
     */
    @JsonProperty("picking_warn")
    public void setPicking_warn(String  picking_warn){
        this.picking_warn = picking_warn ;
        this.picking_warnDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_WARN]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warnDirtyFlag(){
        return picking_warnDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ADDITIONAL_INFO]
     */
    @JsonProperty("additional_info")
    public String getAdditional_info(){
        return additional_info ;
    }

    /**
     * 设置 [ADDITIONAL_INFO]
     */
    @JsonProperty("additional_info")
    public void setAdditional_info(String  additional_info){
        this.additional_info = additional_info ;
        this.additional_infoDirtyFlag = true ;
    }

    /**
     * 获取 [ADDITIONAL_INFO]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_infoDirtyFlag(){
        return additional_infoDirtyFlag ;
    }

    /**
     * 获取 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return ibizfunction ;
    }

    /**
     * 设置 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZFUNCTION]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return ibizfunctionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_LAST_NOTIF_ACK]
     */
    @JsonProperty("calendar_last_notif_ack")
    public Timestamp getCalendar_last_notif_ack(){
        return calendar_last_notif_ack ;
    }

    /**
     * 设置 [CALENDAR_LAST_NOTIF_ACK]
     */
    @JsonProperty("calendar_last_notif_ack")
    public void setCalendar_last_notif_ack(Timestamp  calendar_last_notif_ack){
        this.calendar_last_notif_ack = calendar_last_notif_ack ;
        this.calendar_last_notif_ackDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_LAST_NOTIF_ACK]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_last_notif_ackDirtyFlag(){
        return calendar_last_notif_ackDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE]
     */
    @JsonProperty("employee")
    public String getEmployee(){
        return employee ;
    }

    /**
     * 设置 [EMPLOYEE]
     */
    @JsonProperty("employee")
    public void setEmployee(String  employee){
        this.employee = employee ;
        this.employeeDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getEmployeeDirtyFlag(){
        return employeeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return partner_gid ;
    }

    /**
     * 设置 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_GID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return partner_gidDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [VAT]
     */
    @JsonProperty("vat")
    public String getVat(){
        return vat ;
    }

    /**
     * 设置 [VAT]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

    /**
     * 获取 [VAT]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return vatDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_WARN_MSG]
     */
    @JsonProperty("purchase_warn_msg")
    public String getPurchase_warn_msg(){
        return purchase_warn_msg ;
    }

    /**
     * 设置 [PURCHASE_WARN_MSG]
     */
    @JsonProperty("purchase_warn_msg")
    public void setPurchase_warn_msg(String  purchase_warn_msg){
        this.purchase_warn_msg = purchase_warn_msg ;
        this.purchase_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warn_msgDirtyFlag(){
        return purchase_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER]
     */
    @JsonProperty("supplier")
    public String getSupplier(){
        return supplier ;
    }

    /**
     * 设置 [SUPPLIER]
     */
    @JsonProperty("supplier")
    public void setSupplier(String  supplier){
        this.supplier = supplier ;
        this.supplierDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER]脏标记
     */
    @JsonIgnore
    public boolean getSupplierDirtyFlag(){
        return supplierDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_WARN]
     */
    @JsonProperty("purchase_warn")
    public String getPurchase_warn(){
        return purchase_warn ;
    }

    /**
     * 设置 [PURCHASE_WARN]
     */
    @JsonProperty("purchase_warn")
    public void setPurchase_warn(String  purchase_warn){
        this.purchase_warn = purchase_warn ;
        this.purchase_warnDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warnDirtyFlag(){
        return purchase_warnDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE_PARTNER]
     */
    @JsonProperty("active_partner")
    public String getActive_partner(){
        return active_partner ;
    }

    /**
     * 设置 [ACTIVE_PARTNER]
     */
    @JsonProperty("active_partner")
    public void setActive_partner(String  active_partner){
        this.active_partner = active_partner ;
        this.active_partnerDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE_PARTNER]脏标记
     */
    @JsonIgnore
    public boolean getActive_partnerDirtyFlag(){
        return active_partnerDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [INDUSTRY_ID]
     */
    @JsonProperty("industry_id")
    public Integer getIndustry_id(){
        return industry_id ;
    }

    /**
     * 设置 [INDUSTRY_ID]
     */
    @JsonProperty("industry_id")
    public void setIndustry_id(Integer  industry_id){
        this.industry_id = industry_id ;
        this.industry_idDirtyFlag = true ;
    }

    /**
     * 获取 [INDUSTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_idDirtyFlag(){
        return industry_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_SUPPLIER]
     */
    @JsonProperty("property_stock_supplier")
    public Integer getProperty_stock_supplier(){
        return property_stock_supplier ;
    }

    /**
     * 设置 [PROPERTY_STOCK_SUPPLIER]
     */
    @JsonProperty("property_stock_supplier")
    public void setProperty_stock_supplier(Integer  property_stock_supplier){
        this.property_stock_supplier = property_stock_supplier ;
        this.property_stock_supplierDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_SUPPLIER]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_supplierDirtyFlag(){
        return property_stock_supplierDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_COUNT]
     */
    @JsonProperty("payment_token_count")
    public Integer getPayment_token_count(){
        return payment_token_count ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_COUNT]
     */
    @JsonProperty("payment_token_count")
    public void setPayment_token_count(Integer  payment_token_count){
        this.payment_token_count = payment_token_count ;
        this.payment_token_countDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_countDirtyFlag(){
        return payment_token_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [CUSTOMER]
     */
    @JsonProperty("customer")
    public String getCustomer(){
        return customer ;
    }

    /**
     * 设置 [CUSTOMER]
     */
    @JsonProperty("customer")
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.customerDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOMER]脏标记
     */
    @JsonIgnore
    public boolean getCustomerDirtyFlag(){
        return customerDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_payment_term_id")
    public Integer getProperty_payment_term_id(){
        return property_payment_term_id ;
    }

    /**
     * 设置 [PROPERTY_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_payment_term_id")
    public void setProperty_payment_term_id(Integer  property_payment_term_id){
        this.property_payment_term_id = property_payment_term_id ;
        this.property_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_payment_term_idDirtyFlag(){
        return property_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return contracts_count ;
    }

    /**
     * 设置 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return contracts_countDirtyFlag ;
    }

    /**
     * 获取 [SELF]
     */
    @JsonProperty("self")
    public Integer getSelf(){
        return self ;
    }

    /**
     * 设置 [SELF]
     */
    @JsonProperty("self")
    public void setSelf(Integer  self){
        this.self = self ;
        this.selfDirtyFlag = true ;
    }

    /**
     * 获取 [SELF]脏标记
     */
    @JsonIgnore
    public boolean getSelfDirtyFlag(){
        return selfDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [DEBIT_LIMIT]
     */
    @JsonProperty("debit_limit")
    public Double getDebit_limit(){
        return debit_limit ;
    }

    /**
     * 设置 [DEBIT_LIMIT]
     */
    @JsonProperty("debit_limit")
    public void setDebit_limit(Double  debit_limit){
        this.debit_limit = debit_limit ;
        this.debit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getDebit_limitDirtyFlag(){
        return debit_limitDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [CREDIT_LIMIT]
     */
    @JsonProperty("credit_limit")
    public Double getCredit_limit(){
        return credit_limit ;
    }

    /**
     * 设置 [CREDIT_LIMIT]
     */
    @JsonProperty("credit_limit")
    public void setCredit_limit(Double  credit_limit){
        this.credit_limit = credit_limit ;
        this.credit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getCredit_limitDirtyFlag(){
        return credit_limitDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_COMPANY_NAME]
     */
    @JsonProperty("commercial_company_name")
    public String getCommercial_company_name(){
        return commercial_company_name ;
    }

    /**
     * 设置 [COMMERCIAL_COMPANY_NAME]
     */
    @JsonProperty("commercial_company_name")
    public void setCommercial_company_name(String  commercial_company_name){
        this.commercial_company_name = commercial_company_name ;
        this.commercial_company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_COMPANY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_company_nameDirtyFlag(){
        return commercial_company_nameDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_WARN_MSG]
     */
    @JsonProperty("invoice_warn_msg")
    public String getInvoice_warn_msg(){
        return invoice_warn_msg ;
    }

    /**
     * 设置 [INVOICE_WARN_MSG]
     */
    @JsonProperty("invoice_warn_msg")
    public void setInvoice_warn_msg(String  invoice_warn_msg){
        this.invoice_warn_msg = invoice_warn_msg ;
        this.invoice_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warn_msgDirtyFlag(){
        return invoice_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [TRUST]
     */
    @JsonProperty("trust")
    public String getTrust(){
        return trust ;
    }

    /**
     * 设置 [TRUST]
     */
    @JsonProperty("trust")
    public void setTrust(String  trust){
        this.trust = trust ;
        this.trustDirtyFlag = true ;
    }

    /**
     * 获取 [TRUST]脏标记
     */
    @JsonIgnore
    public boolean getTrustDirtyFlag(){
        return trustDirtyFlag ;
    }

    /**
     * 获取 [MOBILE]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return mobile ;
    }

    /**
     * 设置 [MOBILE]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

    /**
     * 获取 [MOBILE]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return mobileDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FORMATTED]
     */
    @JsonProperty("email_formatted")
    public String getEmail_formatted(){
        return email_formatted ;
    }

    /**
     * 设置 [EMAIL_FORMATTED]
     */
    @JsonProperty("email_formatted")
    public void setEmail_formatted(String  email_formatted){
        this.email_formatted = email_formatted ;
        this.email_formattedDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FORMATTED]脏标记
     */
    @JsonIgnore
    public boolean getEmail_formattedDirtyFlag(){
        return email_formattedDirtyFlag ;
    }

    /**
     * 获取 [IS_COMPANY]
     */
    @JsonProperty("is_company")
    public String getIs_company(){
        return is_company ;
    }

    /**
     * 设置 [IS_COMPANY]
     */
    @JsonProperty("is_company")
    public void setIs_company(String  is_company){
        this.is_company = is_company ;
        this.is_companyDirtyFlag = true ;
    }

    /**
     * 获取 [IS_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getIs_companyDirtyFlag(){
        return is_companyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return is_blacklisted ;
    }

    /**
     * 设置 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_COUNT]
     */
    @JsonProperty("bank_account_count")
    public Integer getBank_account_count(){
        return bank_account_count ;
    }

    /**
     * 设置 [BANK_ACCOUNT_COUNT]
     */
    @JsonProperty("bank_account_count")
    public void setBank_account_count(Integer  bank_account_count){
        this.bank_account_count = bank_account_count ;
        this.bank_account_countDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_countDirtyFlag(){
        return bank_account_countDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PRODUCT_PRICELIST]
     */
    @JsonProperty("property_product_pricelist")
    public Integer getProperty_product_pricelist(){
        return property_product_pricelist ;
    }

    /**
     * 设置 [PROPERTY_PRODUCT_PRICELIST]
     */
    @JsonProperty("property_product_pricelist")
    public void setProperty_product_pricelist(Integer  property_product_pricelist){
        this.property_product_pricelist = property_product_pricelist ;
        this.property_product_pricelistDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PRODUCT_PRICELIST]脏标记
     */
    @JsonIgnore
    public boolean getProperty_product_pricelistDirtyFlag(){
        return property_product_pricelistDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PICKING_WARN_MSG]
     */
    @JsonProperty("picking_warn_msg")
    public String getPicking_warn_msg(){
        return picking_warn_msg ;
    }

    /**
     * 设置 [PICKING_WARN_MSG]
     */
    @JsonProperty("picking_warn_msg")
    public void setPicking_warn_msg(String  picking_warn_msg){
        this.picking_warn_msg = picking_warn_msg ;
        this.picking_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warn_msgDirtyFlag(){
        return picking_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return website ;
    }

    /**
     * 设置 [WEBSITE]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return websiteDirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [STREET2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return street2 ;
    }

    /**
     * 设置 [STREET2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [STREET2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return street2DirtyFlag ;
    }

    /**
     * 获取 [HAS_UNRECONCILED_ENTRIES]
     */
    @JsonProperty("has_unreconciled_entries")
    public String getHas_unreconciled_entries(){
        return has_unreconciled_entries ;
    }

    /**
     * 设置 [HAS_UNRECONCILED_ENTRIES]
     */
    @JsonProperty("has_unreconciled_entries")
    public void setHas_unreconciled_entries(String  has_unreconciled_entries){
        this.has_unreconciled_entries = has_unreconciled_entries ;
        this.has_unreconciled_entriesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_UNRECONCILED_ENTRIES]脏标记
     */
    @JsonIgnore
    public boolean getHas_unreconciled_entriesDirtyFlag(){
        return has_unreconciled_entriesDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_EXPIRATION]
     */
    @JsonProperty("signup_expiration")
    public Timestamp getSignup_expiration(){
        return signup_expiration ;
    }

    /**
     * 设置 [SIGNUP_EXPIRATION]
     */
    @JsonProperty("signup_expiration")
    public void setSignup_expiration(Timestamp  signup_expiration){
        this.signup_expiration = signup_expiration ;
        this.signup_expirationDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_EXPIRATION]脏标记
     */
    @JsonIgnore
    public boolean getSignup_expirationDirtyFlag(){
        return signup_expirationDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_ADDRESS]
     */
    @JsonProperty("contact_address")
    public String getContact_address(){
        return contact_address ;
    }

    /**
     * 设置 [CONTACT_ADDRESS]
     */
    @JsonProperty("contact_address")
    public void setContact_address(String  contact_address){
        this.contact_address = contact_address ;
        this.contact_addressDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_ADDRESS]脏标记
     */
    @JsonIgnore
    public boolean getContact_addressDirtyFlag(){
        return contact_addressDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SHORT_DESCRIPTION]
     */
    @JsonProperty("website_short_description")
    public String getWebsite_short_description(){
        return website_short_description ;
    }

    /**
     * 设置 [WEBSITE_SHORT_DESCRIPTION]
     */
    @JsonProperty("website_short_description")
    public void setWebsite_short_description(String  website_short_description){
        this.website_short_description = website_short_description ;
        this.website_short_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SHORT_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_short_descriptionDirtyFlag(){
        return website_short_descriptionDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHARE]
     */
    @JsonProperty("partner_share")
    public String getPartner_share(){
        return partner_share ;
    }

    /**
     * 设置 [PARTNER_SHARE]
     */
    @JsonProperty("partner_share")
    public void setPartner_share(String  partner_share){
        this.partner_share = partner_share ;
        this.partner_shareDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHARE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shareDirtyFlag(){
        return partner_shareDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [ZIP]
     */
    @JsonProperty("zip")
    public String getZip(){
        return zip ;
    }

    /**
     * 设置 [ZIP]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return zipDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_COUNT]
     */
    @JsonProperty("sale_order_count")
    public Integer getSale_order_count(){
        return sale_order_count ;
    }

    /**
     * 设置 [SALE_ORDER_COUNT]
     */
    @JsonProperty("sale_order_count")
    public void setSale_order_count(Integer  sale_order_count){
        this.sale_order_count = sale_order_count ;
        this.sale_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_countDirtyFlag(){
        return sale_order_countDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_TYPE]
     */
    @JsonProperty("company_type")
    public String getCompany_type(){
        return company_type ;
    }

    /**
     * 设置 [COMPANY_TYPE]
     */
    @JsonProperty("company_type")
    public void setCompany_type(String  company_type){
        this.company_type = company_type ;
        this.company_typeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getCompany_typeDirtyFlag(){
        return company_typeDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_POSITION_ID]
     */
    @JsonProperty("property_account_position_id")
    public Integer getProperty_account_position_id(){
        return property_account_position_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_POSITION_ID]
     */
    @JsonProperty("property_account_position_id")
    public void setProperty_account_position_id(Integer  property_account_position_id){
        this.property_account_position_id = property_account_position_id ;
        this.property_account_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_position_idDirtyFlag(){
        return property_account_position_idDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return message_bounce ;
    }

    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return message_bounceDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PURCHASE_CURRENCY_ID]
     */
    @JsonProperty("property_purchase_currency_id")
    public Integer getProperty_purchase_currency_id(){
        return property_purchase_currency_id ;
    }

    /**
     * 设置 [PROPERTY_PURCHASE_CURRENCY_ID]
     */
    @JsonProperty("property_purchase_currency_id")
    public void setProperty_purchase_currency_id(Integer  property_purchase_currency_id){
        this.property_purchase_currency_id = property_purchase_currency_id ;
        this.property_purchase_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PURCHASE_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_purchase_currency_idDirtyFlag(){
        return property_purchase_currency_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return purchase_order_count ;
    }

    /**
     * 设置 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return purchase_order_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return website_description ;
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public Integer getProperty_supplier_payment_term_id(){
        return property_supplier_payment_term_id ;
    }

    /**
     * 设置 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public void setProperty_supplier_payment_term_id(Integer  property_supplier_payment_term_id){
        this.property_supplier_payment_term_id = property_supplier_payment_term_id ;
        this.property_supplier_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_supplier_payment_term_idDirtyFlag(){
        return property_supplier_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return alias_contact ;
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return alias_contactDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_NAME]
     */
    @JsonProperty("company_name")
    public String getCompany_name(){
        return company_name ;
    }

    /**
     * 设置 [COMPANY_NAME]
     */
    @JsonProperty("company_name")
    public void setCompany_name(String  company_name){
        this.company_name = company_name ;
        this.company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getCompany_nameDirtyFlag(){
        return company_nameDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_TEAM_ID]
     */
    @JsonProperty("sale_team_id")
    public Integer getSale_team_id(){
        return sale_team_id ;
    }

    /**
     * 设置 [SALE_TEAM_ID]
     */
    @JsonProperty("sale_team_id")
    public void setSale_team_id(Integer  sale_team_id){
        this.sale_team_id = sale_team_id ;
        this.sale_team_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_team_idDirtyFlag(){
        return sale_team_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Res_users toDO() {
        Res_users srfdomain = new Res_users();
        if(getIs_moderatorDirtyFlag())
            srfdomain.setIs_moderator(is_moderator);
        if(getResource_idsDirtyFlag())
            srfdomain.setResource_ids(resource_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getLog_idsDirtyFlag())
            srfdomain.setLog_ids(log_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getPos_security_pinDirtyFlag())
            srfdomain.setPos_security_pin(pos_security_pin);
        if(getBadge_idsDirtyFlag())
            srfdomain.setBadge_ids(badge_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCompany_idsDirtyFlag())
            srfdomain.setCompany_ids(company_ids);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getTz_offsetDirtyFlag())
            srfdomain.setTz_offset(tz_offset);
        if(getTarget_sales_doneDirtyFlag())
            srfdomain.setTarget_sales_done(target_sales_done);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getNotification_typeDirtyFlag())
            srfdomain.setNotification_type(notification_type);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getIm_statusDirtyFlag())
            srfdomain.setIm_status(im_status);
        if(getKarmaDirtyFlag())
            srfdomain.setKarma(karma);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getGold_badgeDirtyFlag())
            srfdomain.setGold_badge(gold_badge);
        if(getEmployee_idsDirtyFlag())
            srfdomain.setEmployee_ids(employee_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getModeration_channel_idsDirtyFlag())
            srfdomain.setModeration_channel_ids(moderation_channel_ids);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getSilver_badgeDirtyFlag())
            srfdomain.setSilver_badge(silver_badge);
        if(getPayment_token_idsDirtyFlag())
            srfdomain.setPayment_token_ids(payment_token_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCompanies_countDirtyFlag())
            srfdomain.setCompanies_count(companies_count);
        if(getTarget_sales_invoicedDirtyFlag())
            srfdomain.setTarget_sales_invoiced(target_sales_invoiced);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getLogin_dateDirtyFlag())
            srfdomain.setLogin_date(login_date);
        if(getChannel_idsDirtyFlag())
            srfdomain.setChannel_ids(channel_ids);
        if(getGroups_idDirtyFlag())
            srfdomain.setGroups_id(groups_id);
        if(getShareDirtyFlag())
            srfdomain.setShare(share);
        if(getBank_idsDirtyFlag())
            srfdomain.setBank_ids(bank_ids);
        if(getSale_order_idsDirtyFlag())
            srfdomain.setSale_order_ids(sale_order_ids);
        if(getNew_passwordDirtyFlag())
            srfdomain.setNew_password(new_password);
        if(getOdoobot_stateDirtyFlag())
            srfdomain.setOdoobot_state(odoobot_state);
        if(getRef_company_idsDirtyFlag())
            srfdomain.setRef_company_ids(ref_company_ids);
        if(getPasswordDirtyFlag())
            srfdomain.setPassword(password);
        if(getBronze_badgeDirtyFlag())
            srfdomain.setBronze_badge(bronze_badge);
        if(getMeeting_idsDirtyFlag())
            srfdomain.setMeeting_ids(meeting_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getForum_waiting_posts_countDirtyFlag())
            srfdomain.setForum_waiting_posts_count(forum_waiting_posts_count);
        if(getGoal_idsDirtyFlag())
            srfdomain.setGoal_ids(goal_ids);
        if(getTarget_sales_wonDirtyFlag())
            srfdomain.setTarget_sales_won(target_sales_won);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getAction_idDirtyFlag())
            srfdomain.setAction_id(action_id);
        if(getLoginDirtyFlag())
            srfdomain.setLogin(login);
        if(getContract_idsDirtyFlag())
            srfdomain.setContract_ids(contract_ids);
        if(getModeration_counterDirtyFlag())
            srfdomain.setModeration_counter(moderation_counter);
        if(getSignatureDirtyFlag())
            srfdomain.setSignature(signature);
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getOpportunity_idsDirtyFlag())
            srfdomain.setOpportunity_ids(opportunity_ids);
        if(getTask_idsDirtyFlag())
            srfdomain.setTask_ids(task_ids);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getRefDirtyFlag())
            srfdomain.setRef(ref);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getLast_website_so_idDirtyFlag())
            srfdomain.setLast_website_so_id(last_website_so_id);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getProperty_stock_customerDirtyFlag())
            srfdomain.setProperty_stock_customer(property_stock_customer);
        if(getLast_time_entries_checkedDirtyFlag())
            srfdomain.setLast_time_entries_checked(last_time_entries_checked);
        if(getLangDirtyFlag())
            srfdomain.setLang(lang);
        if(getSale_warnDirtyFlag())
            srfdomain.setSale_warn(sale_warn);
        if(getMeeting_countDirtyFlag())
            srfdomain.setMeeting_count(meeting_count);
        if(getStreetDirtyFlag())
            srfdomain.setStreet(street);
        if(getInvoice_warnDirtyFlag())
            srfdomain.setInvoice_warn(invoice_warn);
        if(getSignup_tokenDirtyFlag())
            srfdomain.setSignup_token(signup_token);
        if(getTask_countDirtyFlag())
            srfdomain.setTask_count(task_count);
        if(getSignup_validDirtyFlag())
            srfdomain.setSignup_valid(signup_valid);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getSignup_typeDirtyFlag())
            srfdomain.setSignup_type(signup_type);
        if(getProperty_account_receivable_idDirtyFlag())
            srfdomain.setProperty_account_receivable_id(property_account_receivable_id);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getEvent_countDirtyFlag())
            srfdomain.setEvent_count(event_count);
        if(getJournal_item_countDirtyFlag())
            srfdomain.setJournal_item_count(journal_item_count);
        if(getParent_nameDirtyFlag())
            srfdomain.setParent_name(parent_name);
        if(getSale_team_id_textDirtyFlag())
            srfdomain.setSale_team_id_text(sale_team_id_text);
        if(getCreditDirtyFlag())
            srfdomain.setCredit(credit);
        if(getOpportunity_countDirtyFlag())
            srfdomain.setOpportunity_count(opportunity_count);
        if(getSignup_urlDirtyFlag())
            srfdomain.setSignup_url(signup_url);
        if(getProperty_account_payable_idDirtyFlag())
            srfdomain.setProperty_account_payable_id(property_account_payable_id);
        if(getState_idDirtyFlag())
            srfdomain.setState_id(state_id);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getSale_warn_msgDirtyFlag())
            srfdomain.setSale_warn_msg(sale_warn_msg);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getTotal_invoicedDirtyFlag())
            srfdomain.setTotal_invoiced(total_invoiced);
        if(getDebitDirtyFlag())
            srfdomain.setDebit(debit);
        if(getPos_order_countDirtyFlag())
            srfdomain.setPos_order_count(pos_order_count);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTitleDirtyFlag())
            srfdomain.setTitle(title);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getSupplier_invoice_countDirtyFlag())
            srfdomain.setSupplier_invoice_count(supplier_invoice_count);
        if(getCityDirtyFlag())
            srfdomain.setCity(city);
        if(getPicking_warnDirtyFlag())
            srfdomain.setPicking_warn(picking_warn);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getAdditional_infoDirtyFlag())
            srfdomain.setAdditional_info(additional_info);
        if(getIbizfunctionDirtyFlag())
            srfdomain.setIbizfunction(ibizfunction);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getCalendar_last_notif_ackDirtyFlag())
            srfdomain.setCalendar_last_notif_ack(calendar_last_notif_ack);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getEmployeeDirtyFlag())
            srfdomain.setEmployee(employee);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getPartner_gidDirtyFlag())
            srfdomain.setPartner_gid(partner_gid);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getVatDirtyFlag())
            srfdomain.setVat(vat);
        if(getPurchase_warn_msgDirtyFlag())
            srfdomain.setPurchase_warn_msg(purchase_warn_msg);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(getSupplierDirtyFlag())
            srfdomain.setSupplier(supplier);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getPurchase_warnDirtyFlag())
            srfdomain.setPurchase_warn(purchase_warn);
        if(getActive_partnerDirtyFlag())
            srfdomain.setActive_partner(active_partner);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getIndustry_idDirtyFlag())
            srfdomain.setIndustry_id(industry_id);
        if(getProperty_stock_supplierDirtyFlag())
            srfdomain.setProperty_stock_supplier(property_stock_supplier);
        if(getPayment_token_countDirtyFlag())
            srfdomain.setPayment_token_count(payment_token_count);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCustomerDirtyFlag())
            srfdomain.setCustomer(customer);
        if(getProperty_payment_term_idDirtyFlag())
            srfdomain.setProperty_payment_term_id(property_payment_term_id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getContracts_countDirtyFlag())
            srfdomain.setContracts_count(contracts_count);
        if(getSelfDirtyFlag())
            srfdomain.setSelf(self);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getDebit_limitDirtyFlag())
            srfdomain.setDebit_limit(debit_limit);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getCredit_limitDirtyFlag())
            srfdomain.setCredit_limit(credit_limit);
        if(getCommercial_company_nameDirtyFlag())
            srfdomain.setCommercial_company_name(commercial_company_name);
        if(getInvoice_warn_msgDirtyFlag())
            srfdomain.setInvoice_warn_msg(invoice_warn_msg);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getTrustDirtyFlag())
            srfdomain.setTrust(trust);
        if(getMobileDirtyFlag())
            srfdomain.setMobile(mobile);
        if(getEmail_formattedDirtyFlag())
            srfdomain.setEmail_formatted(email_formatted);
        if(getIs_companyDirtyFlag())
            srfdomain.setIs_company(is_company);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getIs_blacklistedDirtyFlag())
            srfdomain.setIs_blacklisted(is_blacklisted);
        if(getBank_account_countDirtyFlag())
            srfdomain.setBank_account_count(bank_account_count);
        if(getProperty_product_pricelistDirtyFlag())
            srfdomain.setProperty_product_pricelist(property_product_pricelist);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPicking_warn_msgDirtyFlag())
            srfdomain.setPicking_warn_msg(picking_warn_msg);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWebsiteDirtyFlag())
            srfdomain.setWebsite(website);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getStreet2DirtyFlag())
            srfdomain.setStreet2(street2);
        if(getHas_unreconciled_entriesDirtyFlag())
            srfdomain.setHas_unreconciled_entries(has_unreconciled_entries);
        if(getSignup_expirationDirtyFlag())
            srfdomain.setSignup_expiration(signup_expiration);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getContact_addressDirtyFlag())
            srfdomain.setContact_address(contact_address);
        if(getWebsite_short_descriptionDirtyFlag())
            srfdomain.setWebsite_short_description(website_short_description);
        if(getPartner_shareDirtyFlag())
            srfdomain.setPartner_share(partner_share);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getZipDirtyFlag())
            srfdomain.setZip(zip);
        if(getSale_order_countDirtyFlag())
            srfdomain.setSale_order_count(sale_order_count);
        if(getCompany_typeDirtyFlag())
            srfdomain.setCompany_type(company_type);
        if(getProperty_account_position_idDirtyFlag())
            srfdomain.setProperty_account_position_id(property_account_position_id);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getMessage_bounceDirtyFlag())
            srfdomain.setMessage_bounce(message_bounce);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getProperty_purchase_currency_idDirtyFlag())
            srfdomain.setProperty_purchase_currency_id(property_purchase_currency_id);
        if(getPurchase_order_countDirtyFlag())
            srfdomain.setPurchase_order_count(purchase_order_count);
        if(getWebsite_descriptionDirtyFlag())
            srfdomain.setWebsite_description(website_description);
        if(getProperty_supplier_payment_term_idDirtyFlag())
            srfdomain.setProperty_supplier_payment_term_id(property_supplier_payment_term_id);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getAlias_contactDirtyFlag())
            srfdomain.setAlias_contact(alias_contact);
        if(getCompany_nameDirtyFlag())
            srfdomain.setCompany_name(company_name);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);
        if(getSale_team_idDirtyFlag())
            srfdomain.setSale_team_id(sale_team_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Res_users srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIs_moderatorDirtyFlag())
            this.setIs_moderator(srfdomain.getIs_moderator());
        if(srfdomain.getResource_idsDirtyFlag())
            this.setResource_ids(srfdomain.getResource_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getLog_idsDirtyFlag())
            this.setLog_ids(srfdomain.getLog_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getPos_security_pinDirtyFlag())
            this.setPos_security_pin(srfdomain.getPos_security_pin());
        if(srfdomain.getBadge_idsDirtyFlag())
            this.setBadge_ids(srfdomain.getBadge_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCompany_idsDirtyFlag())
            this.setCompany_ids(srfdomain.getCompany_ids());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getTz_offsetDirtyFlag())
            this.setTz_offset(srfdomain.getTz_offset());
        if(srfdomain.getTarget_sales_doneDirtyFlag())
            this.setTarget_sales_done(srfdomain.getTarget_sales_done());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getNotification_typeDirtyFlag())
            this.setNotification_type(srfdomain.getNotification_type());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getIm_statusDirtyFlag())
            this.setIm_status(srfdomain.getIm_status());
        if(srfdomain.getKarmaDirtyFlag())
            this.setKarma(srfdomain.getKarma());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getGold_badgeDirtyFlag())
            this.setGold_badge(srfdomain.getGold_badge());
        if(srfdomain.getEmployee_idsDirtyFlag())
            this.setEmployee_ids(srfdomain.getEmployee_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getModeration_channel_idsDirtyFlag())
            this.setModeration_channel_ids(srfdomain.getModeration_channel_ids());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getSilver_badgeDirtyFlag())
            this.setSilver_badge(srfdomain.getSilver_badge());
        if(srfdomain.getPayment_token_idsDirtyFlag())
            this.setPayment_token_ids(srfdomain.getPayment_token_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCompanies_countDirtyFlag())
            this.setCompanies_count(srfdomain.getCompanies_count());
        if(srfdomain.getTarget_sales_invoicedDirtyFlag())
            this.setTarget_sales_invoiced(srfdomain.getTarget_sales_invoiced());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getLogin_dateDirtyFlag())
            this.setLogin_date(srfdomain.getLogin_date());
        if(srfdomain.getChannel_idsDirtyFlag())
            this.setChannel_ids(srfdomain.getChannel_ids());
        if(srfdomain.getGroups_idDirtyFlag())
            this.setGroups_id(srfdomain.getGroups_id());
        if(srfdomain.getShareDirtyFlag())
            this.setShare(srfdomain.getShare());
        if(srfdomain.getBank_idsDirtyFlag())
            this.setBank_ids(srfdomain.getBank_ids());
        if(srfdomain.getSale_order_idsDirtyFlag())
            this.setSale_order_ids(srfdomain.getSale_order_ids());
        if(srfdomain.getNew_passwordDirtyFlag())
            this.setNew_password(srfdomain.getNew_password());
        if(srfdomain.getOdoobot_stateDirtyFlag())
            this.setOdoobot_state(srfdomain.getOdoobot_state());
        if(srfdomain.getRef_company_idsDirtyFlag())
            this.setRef_company_ids(srfdomain.getRef_company_ids());
        if(srfdomain.getPasswordDirtyFlag())
            this.setPassword(srfdomain.getPassword());
        if(srfdomain.getBronze_badgeDirtyFlag())
            this.setBronze_badge(srfdomain.getBronze_badge());
        if(srfdomain.getMeeting_idsDirtyFlag())
            this.setMeeting_ids(srfdomain.getMeeting_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getForum_waiting_posts_countDirtyFlag())
            this.setForum_waiting_posts_count(srfdomain.getForum_waiting_posts_count());
        if(srfdomain.getGoal_idsDirtyFlag())
            this.setGoal_ids(srfdomain.getGoal_ids());
        if(srfdomain.getTarget_sales_wonDirtyFlag())
            this.setTarget_sales_won(srfdomain.getTarget_sales_won());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getAction_idDirtyFlag())
            this.setAction_id(srfdomain.getAction_id());
        if(srfdomain.getLoginDirtyFlag())
            this.setLogin(srfdomain.getLogin());
        if(srfdomain.getContract_idsDirtyFlag())
            this.setContract_ids(srfdomain.getContract_ids());
        if(srfdomain.getModeration_counterDirtyFlag())
            this.setModeration_counter(srfdomain.getModeration_counter());
        if(srfdomain.getSignatureDirtyFlag())
            this.setSignature(srfdomain.getSignature());
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getOpportunity_idsDirtyFlag())
            this.setOpportunity_ids(srfdomain.getOpportunity_ids());
        if(srfdomain.getTask_idsDirtyFlag())
            this.setTask_ids(srfdomain.getTask_ids());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getRefDirtyFlag())
            this.setRef(srfdomain.getRef());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getLast_website_so_idDirtyFlag())
            this.setLast_website_so_id(srfdomain.getLast_website_so_id());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getProperty_stock_customerDirtyFlag())
            this.setProperty_stock_customer(srfdomain.getProperty_stock_customer());
        if(srfdomain.getLast_time_entries_checkedDirtyFlag())
            this.setLast_time_entries_checked(srfdomain.getLast_time_entries_checked());
        if(srfdomain.getLangDirtyFlag())
            this.setLang(srfdomain.getLang());
        if(srfdomain.getSale_warnDirtyFlag())
            this.setSale_warn(srfdomain.getSale_warn());
        if(srfdomain.getMeeting_countDirtyFlag())
            this.setMeeting_count(srfdomain.getMeeting_count());
        if(srfdomain.getStreetDirtyFlag())
            this.setStreet(srfdomain.getStreet());
        if(srfdomain.getInvoice_warnDirtyFlag())
            this.setInvoice_warn(srfdomain.getInvoice_warn());
        if(srfdomain.getSignup_tokenDirtyFlag())
            this.setSignup_token(srfdomain.getSignup_token());
        if(srfdomain.getTask_countDirtyFlag())
            this.setTask_count(srfdomain.getTask_count());
        if(srfdomain.getSignup_validDirtyFlag())
            this.setSignup_valid(srfdomain.getSignup_valid());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getSignup_typeDirtyFlag())
            this.setSignup_type(srfdomain.getSignup_type());
        if(srfdomain.getProperty_account_receivable_idDirtyFlag())
            this.setProperty_account_receivable_id(srfdomain.getProperty_account_receivable_id());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getEvent_countDirtyFlag())
            this.setEvent_count(srfdomain.getEvent_count());
        if(srfdomain.getJournal_item_countDirtyFlag())
            this.setJournal_item_count(srfdomain.getJournal_item_count());
        if(srfdomain.getParent_nameDirtyFlag())
            this.setParent_name(srfdomain.getParent_name());
        if(srfdomain.getSale_team_id_textDirtyFlag())
            this.setSale_team_id_text(srfdomain.getSale_team_id_text());
        if(srfdomain.getCreditDirtyFlag())
            this.setCredit(srfdomain.getCredit());
        if(srfdomain.getOpportunity_countDirtyFlag())
            this.setOpportunity_count(srfdomain.getOpportunity_count());
        if(srfdomain.getSignup_urlDirtyFlag())
            this.setSignup_url(srfdomain.getSignup_url());
        if(srfdomain.getProperty_account_payable_idDirtyFlag())
            this.setProperty_account_payable_id(srfdomain.getProperty_account_payable_id());
        if(srfdomain.getState_idDirtyFlag())
            this.setState_id(srfdomain.getState_id());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getSale_warn_msgDirtyFlag())
            this.setSale_warn_msg(srfdomain.getSale_warn_msg());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getTotal_invoicedDirtyFlag())
            this.setTotal_invoiced(srfdomain.getTotal_invoiced());
        if(srfdomain.getDebitDirtyFlag())
            this.setDebit(srfdomain.getDebit());
        if(srfdomain.getPos_order_countDirtyFlag())
            this.setPos_order_count(srfdomain.getPos_order_count());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTitleDirtyFlag())
            this.setTitle(srfdomain.getTitle());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getSupplier_invoice_countDirtyFlag())
            this.setSupplier_invoice_count(srfdomain.getSupplier_invoice_count());
        if(srfdomain.getCityDirtyFlag())
            this.setCity(srfdomain.getCity());
        if(srfdomain.getPicking_warnDirtyFlag())
            this.setPicking_warn(srfdomain.getPicking_warn());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getAdditional_infoDirtyFlag())
            this.setAdditional_info(srfdomain.getAdditional_info());
        if(srfdomain.getIbizfunctionDirtyFlag())
            this.setIbizfunction(srfdomain.getIbizfunction());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getCalendar_last_notif_ackDirtyFlag())
            this.setCalendar_last_notif_ack(srfdomain.getCalendar_last_notif_ack());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getEmployeeDirtyFlag())
            this.setEmployee(srfdomain.getEmployee());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getPartner_gidDirtyFlag())
            this.setPartner_gid(srfdomain.getPartner_gid());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getVatDirtyFlag())
            this.setVat(srfdomain.getVat());
        if(srfdomain.getPurchase_warn_msgDirtyFlag())
            this.setPurchase_warn_msg(srfdomain.getPurchase_warn_msg());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.getSupplierDirtyFlag())
            this.setSupplier(srfdomain.getSupplier());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getPurchase_warnDirtyFlag())
            this.setPurchase_warn(srfdomain.getPurchase_warn());
        if(srfdomain.getActive_partnerDirtyFlag())
            this.setActive_partner(srfdomain.getActive_partner());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getIndustry_idDirtyFlag())
            this.setIndustry_id(srfdomain.getIndustry_id());
        if(srfdomain.getProperty_stock_supplierDirtyFlag())
            this.setProperty_stock_supplier(srfdomain.getProperty_stock_supplier());
        if(srfdomain.getPayment_token_countDirtyFlag())
            this.setPayment_token_count(srfdomain.getPayment_token_count());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCustomerDirtyFlag())
            this.setCustomer(srfdomain.getCustomer());
        if(srfdomain.getProperty_payment_term_idDirtyFlag())
            this.setProperty_payment_term_id(srfdomain.getProperty_payment_term_id());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getContracts_countDirtyFlag())
            this.setContracts_count(srfdomain.getContracts_count());
        if(srfdomain.getSelfDirtyFlag())
            this.setSelf(srfdomain.getSelf());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getDebit_limitDirtyFlag())
            this.setDebit_limit(srfdomain.getDebit_limit());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getCredit_limitDirtyFlag())
            this.setCredit_limit(srfdomain.getCredit_limit());
        if(srfdomain.getCommercial_company_nameDirtyFlag())
            this.setCommercial_company_name(srfdomain.getCommercial_company_name());
        if(srfdomain.getInvoice_warn_msgDirtyFlag())
            this.setInvoice_warn_msg(srfdomain.getInvoice_warn_msg());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getTrustDirtyFlag())
            this.setTrust(srfdomain.getTrust());
        if(srfdomain.getMobileDirtyFlag())
            this.setMobile(srfdomain.getMobile());
        if(srfdomain.getEmail_formattedDirtyFlag())
            this.setEmail_formatted(srfdomain.getEmail_formatted());
        if(srfdomain.getIs_companyDirtyFlag())
            this.setIs_company(srfdomain.getIs_company());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getIs_blacklistedDirtyFlag())
            this.setIs_blacklisted(srfdomain.getIs_blacklisted());
        if(srfdomain.getBank_account_countDirtyFlag())
            this.setBank_account_count(srfdomain.getBank_account_count());
        if(srfdomain.getProperty_product_pricelistDirtyFlag())
            this.setProperty_product_pricelist(srfdomain.getProperty_product_pricelist());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPicking_warn_msgDirtyFlag())
            this.setPicking_warn_msg(srfdomain.getPicking_warn_msg());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWebsiteDirtyFlag())
            this.setWebsite(srfdomain.getWebsite());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getStreet2DirtyFlag())
            this.setStreet2(srfdomain.getStreet2());
        if(srfdomain.getHas_unreconciled_entriesDirtyFlag())
            this.setHas_unreconciled_entries(srfdomain.getHas_unreconciled_entries());
        if(srfdomain.getSignup_expirationDirtyFlag())
            this.setSignup_expiration(srfdomain.getSignup_expiration());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getContact_addressDirtyFlag())
            this.setContact_address(srfdomain.getContact_address());
        if(srfdomain.getWebsite_short_descriptionDirtyFlag())
            this.setWebsite_short_description(srfdomain.getWebsite_short_description());
        if(srfdomain.getPartner_shareDirtyFlag())
            this.setPartner_share(srfdomain.getPartner_share());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getZipDirtyFlag())
            this.setZip(srfdomain.getZip());
        if(srfdomain.getSale_order_countDirtyFlag())
            this.setSale_order_count(srfdomain.getSale_order_count());
        if(srfdomain.getCompany_typeDirtyFlag())
            this.setCompany_type(srfdomain.getCompany_type());
        if(srfdomain.getProperty_account_position_idDirtyFlag())
            this.setProperty_account_position_id(srfdomain.getProperty_account_position_id());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getMessage_bounceDirtyFlag())
            this.setMessage_bounce(srfdomain.getMessage_bounce());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getProperty_purchase_currency_idDirtyFlag())
            this.setProperty_purchase_currency_id(srfdomain.getProperty_purchase_currency_id());
        if(srfdomain.getPurchase_order_countDirtyFlag())
            this.setPurchase_order_count(srfdomain.getPurchase_order_count());
        if(srfdomain.getWebsite_descriptionDirtyFlag())
            this.setWebsite_description(srfdomain.getWebsite_description());
        if(srfdomain.getProperty_supplier_payment_term_idDirtyFlag())
            this.setProperty_supplier_payment_term_id(srfdomain.getProperty_supplier_payment_term_id());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getAlias_contactDirtyFlag())
            this.setAlias_contact(srfdomain.getAlias_contact());
        if(srfdomain.getCompany_nameDirtyFlag())
            this.setCompany_name(srfdomain.getCompany_name());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());
        if(srfdomain.getSale_team_idDirtyFlag())
            this.setSale_team_id(srfdomain.getSale_team_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Res_usersDTO> fromDOPage(List<Res_users> poPage)   {
        if(poPage == null)
            return null;
        List<Res_usersDTO> dtos=new ArrayList<Res_usersDTO>();
        for(Res_users domain : poPage) {
            Res_usersDTO dto = new Res_usersDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_partner_pDTO]
 */
public class Res_partner_pDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IMAGE]
     *
     */
    @Res_partnerImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Res_partnerTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Res_partnerColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_IDS]
     *
     */
    @Res_partnerPayment_token_idsDefault(info = "默认规则")
    private String payment_token_ids;

    @JsonIgnore
    private boolean payment_token_idsDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Res_partnerInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @Res_partnerMeeting_countDefault(info = "默认规则")
    private Integer meeting_count;

    @JsonIgnore
    private boolean meeting_countDirtyFlag;

    /**
     * 属性 [SUPPLIER_INVOICE_COUNT]
     *
     */
    @Res_partnerSupplier_invoice_countDefault(info = "默认规则")
    private Integer supplier_invoice_count;

    @JsonIgnore
    private boolean supplier_invoice_countDirtyFlag;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @Res_partnerCompany_nameDefault(info = "默认规则")
    private String company_name;

    @JsonIgnore
    private boolean company_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Res_partnerWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [LAST_TIME_ENTRIES_CHECKED]
     *
     */
    @Res_partnerLast_time_entries_checkedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_time_entries_checked;

    @JsonIgnore
    private boolean last_time_entries_checkedDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Res_partnerMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [TRUST]
     *
     */
    @Res_partnerTrustDefault(info = "默认规则")
    private String trust;

    @JsonIgnore
    private boolean trustDirtyFlag;

    /**
     * 属性 [IBIZFUNCTION]
     *
     */
    @Res_partnerIbizfunctionDefault(info = "默认规则")
    private String ibizfunction;

    @JsonIgnore
    private boolean ibizfunctionDirtyFlag;

    /**
     * 属性 [TOTAL_INVOICED]
     *
     */
    @Res_partnerTotal_invoicedDefault(info = "默认规则")
    private Double total_invoiced;

    @JsonIgnore
    private boolean total_invoicedDirtyFlag;

    /**
     * 属性 [POS_ORDER_COUNT]
     *
     */
    @Res_partnerPos_order_countDefault(info = "默认规则")
    private Integer pos_order_count;

    @JsonIgnore
    private boolean pos_order_countDirtyFlag;

    /**
     * 属性 [CONTACT_ADDRESS]
     *
     */
    @Res_partnerContact_addressDefault(info = "默认规则")
    private String contact_address;

    @JsonIgnore
    private boolean contact_addressDirtyFlag;

    /**
     * 属性 [INVOICE_WARN]
     *
     */
    @Res_partnerInvoice_warnDefault(info = "默认规则")
    private String invoice_warn;

    @JsonIgnore
    private boolean invoice_warnDirtyFlag;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @Res_partnerBank_idsDefault(info = "默认规则")
    private String bank_ids;

    @JsonIgnore
    private boolean bank_idsDirtyFlag;

    /**
     * 属性 [SIGNUP_EXPIRATION]
     *
     */
    @Res_partnerSignup_expirationDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp signup_expiration;

    @JsonIgnore
    private boolean signup_expirationDirtyFlag;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @Res_partnerPurchase_order_countDefault(info = "默认规则")
    private Integer purchase_order_count;

    @JsonIgnore
    private boolean purchase_order_countDirtyFlag;

    /**
     * 属性 [HAS_UNRECONCILED_ENTRIES]
     *
     */
    @Res_partnerHas_unreconciled_entriesDefault(info = "默认规则")
    private String has_unreconciled_entries;

    @JsonIgnore
    private boolean has_unreconciled_entriesDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Res_partnerCategory_idDefault(info = "默认规则")
    private String category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @Res_partnerWebsite_descriptionDefault(info = "默认规则")
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Res_partnerMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MEETING_IDS]
     *
     */
    @Res_partnerMeeting_idsDefault(info = "默认规则")
    private String meeting_ids;

    @JsonIgnore
    private boolean meeting_idsDirtyFlag;

    /**
     * 属性 [EMPLOYEE]
     *
     */
    @Res_partnerEmployeeDefault(info = "默认规则")
    private String employee;

    @JsonIgnore
    private boolean employeeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_partnerDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Res_partnerChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Res_partnerWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @Res_partnerIs_blacklistedDefault(info = "默认规则")
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST]
     *
     */
    @Res_partnerProperty_product_pricelistDefault(info = "默认规则")
    private Integer property_product_pricelist;

    @JsonIgnore
    private boolean property_product_pricelistDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Res_partnerActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Res_partnerActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [SIGNUP_TOKEN]
     *
     */
    @Res_partnerSignup_tokenDefault(info = "默认规则")
    private String signup_token;

    @JsonIgnore
    private boolean signup_tokenDirtyFlag;

    /**
     * 属性 [REF_COMPANY_IDS]
     *
     */
    @Res_partnerRef_company_idsDefault(info = "默认规则")
    private String ref_company_ids;

    @JsonIgnore
    private boolean ref_company_idsDirtyFlag;

    /**
     * 属性 [IS_COMPANY]
     *
     */
    @Res_partnerIs_companyDefault(info = "默认规则")
    private String is_company;

    @JsonIgnore
    private boolean is_companyDirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Res_partnerPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_partnerCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Res_partnerTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [EVENT_COUNT]
     *
     */
    @Res_partnerEvent_countDefault(info = "默认规则")
    private Integer event_count;

    @JsonIgnore
    private boolean event_countDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Res_partnerMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [CALENDAR_LAST_NOTIF_ACK]
     *
     */
    @Res_partnerCalendar_last_notif_ackDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp calendar_last_notif_ack;

    @JsonIgnore
    private boolean calendar_last_notif_ackDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Res_partnerMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [SIGNUP_TYPE]
     *
     */
    @Res_partnerSignup_typeDefault(info = "默认规则")
    private String signup_type;

    @JsonIgnore
    private boolean signup_typeDirtyFlag;

    /**
     * 属性 [EMAIL_FORMATTED]
     *
     */
    @Res_partnerEmail_formattedDefault(info = "默认规则")
    private String email_formatted;

    @JsonIgnore
    private boolean email_formattedDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Res_partnerWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [PARTNER_SHARE]
     *
     */
    @Res_partnerPartner_shareDefault(info = "默认规则")
    private String partner_share;

    @JsonIgnore
    private boolean partner_shareDirtyFlag;

    /**
     * 属性 [STREET2]
     *
     */
    @Res_partnerStreet2Default(info = "默认规则")
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;

    /**
     * 属性 [DEBIT]
     *
     */
    @Res_partnerDebitDefault(info = "默认规则")
    private Double debit;

    @JsonIgnore
    private boolean debitDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_COUNT]
     *
     */
    @Res_partnerPayment_token_countDefault(info = "默认规则")
    private Integer payment_token_count;

    @JsonIgnore
    private boolean payment_token_countDirtyFlag;

    /**
     * 属性 [REF]
     *
     */
    @Res_partnerRefDefault(info = "默认规则")
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @Res_partnerPartner_gidDefault(info = "默认规则")
    private Integer partner_gid;

    @JsonIgnore
    private boolean partner_gidDirtyFlag;

    /**
     * 属性 [SIGNUP_VALID]
     *
     */
    @Res_partnerSignup_validDefault(info = "默认规则")
    private String signup_valid;

    @JsonIgnore
    private boolean signup_validDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Res_partnerWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Res_partnerImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_COUNT]
     *
     */
    @Res_partnerBank_account_countDefault(info = "默认规则")
    private Integer bank_account_count;

    @JsonIgnore
    private boolean bank_account_countDirtyFlag;

    /**
     * 属性 [STREET]
     *
     */
    @Res_partnerStreetDefault(info = "默认规则")
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;

    /**
     * 属性 [SALE_WARN]
     *
     */
    @Res_partnerSale_warnDefault(info = "默认规则")
    private String sale_warn;

    @JsonIgnore
    private boolean sale_warnDirtyFlag;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @Res_partnerMessage_bounceDefault(info = "默认规则")
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Res_partnerMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Res_partnerMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_COUNT]
     *
     */
    @Res_partnerOpportunity_countDefault(info = "默认规则")
    private Integer opportunity_count;

    @JsonIgnore
    private boolean opportunity_countDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Res_partnerDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_partner__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Res_partnerMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [SELF]
     *
     */
    @Res_partnerSelfDefault(info = "默认规则")
    private Integer self;

    @JsonIgnore
    private boolean selfDirtyFlag;

    /**
     * 属性 [IM_STATUS]
     *
     */
    @Res_partnerIm_statusDefault(info = "默认规则")
    private String im_status;

    @JsonIgnore
    private boolean im_statusDirtyFlag;

    /**
     * 属性 [CUSTOMER]
     *
     */
    @Res_partnerCustomerDefault(info = "默认规则")
    private String customer;

    @JsonIgnore
    private boolean customerDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_partnerWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Res_partnerMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [INVOICE_WARN_MSG]
     *
     */
    @Res_partnerInvoice_warn_msgDefault(info = "默认规则")
    private String invoice_warn_msg;

    @JsonIgnore
    private boolean invoice_warn_msgDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Res_partnerMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [PICKING_WARN]
     *
     */
    @Res_partnerPicking_warnDefault(info = "默认规则")
    private String picking_warn;

    @JsonIgnore
    private boolean picking_warnDirtyFlag;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @Res_partnerContract_idsDefault(info = "默认规则")
    private String contract_ids;

    @JsonIgnore
    private boolean contract_idsDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_partnerCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WEBSITE]
     *
     */
    @Res_partnerWebsiteDefault(info = "默认规则")
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;

    /**
     * 属性 [MOBILE]
     *
     */
    @Res_partnerMobileDefault(info = "默认规则")
    private String mobile;

    @JsonIgnore
    private boolean mobileDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Res_partnerMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [CITY]
     *
     */
    @Res_partnerCityDefault(info = "默认规则")
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_ID]
     *
     */
    @Res_partnerProperty_payment_term_idDefault(info = "默认规则")
    private Integer property_payment_term_id;

    @JsonIgnore
    private boolean property_payment_term_idDirtyFlag;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Res_partnerUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Res_partnerWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @Res_partnerChannel_idsDefault(info = "默认规则")
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;

    /**
     * 属性 [PURCHASE_WARN]
     *
     */
    @Res_partnerPurchase_warnDefault(info = "默认规则")
    private String purchase_warn;

    @JsonIgnore
    private boolean purchase_warnDirtyFlag;

    /**
     * 属性 [JOURNAL_ITEM_COUNT]
     *
     */
    @Res_partnerJournal_item_countDefault(info = "默认规则")
    private Integer journal_item_count;

    @JsonIgnore
    private boolean journal_item_countDirtyFlag;

    /**
     * 属性 [SUPPLIER]
     *
     */
    @Res_partnerSupplierDefault(info = "默认规则")
    private String supplier;

    @JsonIgnore
    private boolean supplierDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER]
     *
     */
    @Res_partnerProperty_stock_supplierDefault(info = "默认规则")
    private Integer property_stock_supplier;

    @JsonIgnore
    private boolean property_stock_supplierDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @Res_partnerProperty_account_payable_idDefault(info = "默认规则")
    private Integer property_account_payable_id;

    @JsonIgnore
    private boolean property_account_payable_idDirtyFlag;

    /**
     * 属性 [WEBSITE_SHORT_DESCRIPTION]
     *
     */
    @Res_partnerWebsite_short_descriptionDefault(info = "默认规则")
    private String website_short_description;

    @JsonIgnore
    private boolean website_short_descriptionDirtyFlag;

    /**
     * 属性 [SALE_WARN_MSG]
     *
     */
    @Res_partnerSale_warn_msgDefault(info = "默认规则")
    private String sale_warn_msg;

    @JsonIgnore
    private boolean sale_warn_msgDirtyFlag;

    /**
     * 属性 [CREDIT]
     *
     */
    @Res_partnerCreditDefault(info = "默认规则")
    private Double credit;

    @JsonIgnore
    private boolean creditDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Res_partnerActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Res_partnerActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Res_partnerMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_partnerNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [VAT]
     *
     */
    @Res_partnerVatDefault(info = "默认规则")
    private String vat;

    @JsonIgnore
    private boolean vatDirtyFlag;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     *
     */
    @Res_partnerProperty_supplier_payment_term_idDefault(info = "默认规则")
    private Integer property_supplier_payment_term_id;

    @JsonIgnore
    private boolean property_supplier_payment_term_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER]
     *
     */
    @Res_partnerProperty_stock_customerDefault(info = "默认规则")
    private Integer property_stock_customer;

    @JsonIgnore
    private boolean property_stock_customerDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Res_partnerCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @Res_partnerTask_idsDefault(info = "默认规则")
    private String task_ids;

    @JsonIgnore
    private boolean task_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Res_partnerMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Res_partnerEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [PURCHASE_WARN_MSG]
     *
     */
    @Res_partnerPurchase_warn_msgDefault(info = "默认规则")
    private String purchase_warn_msg;

    @JsonIgnore
    private boolean purchase_warn_msgDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Res_partnerWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [ZIP]
     *
     */
    @Res_partnerZipDefault(info = "默认规则")
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;

    /**
     * 属性 [TZ_OFFSET]
     *
     */
    @Res_partnerTz_offsetDefault(info = "默认规则")
    private String tz_offset;

    @JsonIgnore
    private boolean tz_offsetDirtyFlag;

    /**
     * 属性 [COMPANY_TYPE]
     *
     */
    @Res_partnerCompany_typeDefault(info = "默认规则")
    private String company_type;

    @JsonIgnore
    private boolean company_typeDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Res_partnerActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @Res_partnerTask_countDefault(info = "默认规则")
    private Integer task_count;

    @JsonIgnore
    private boolean task_countDirtyFlag;

    /**
     * 属性 [CREDIT_LIMIT]
     *
     */
    @Res_partnerCredit_limitDefault(info = "默认规则")
    private Double credit_limit;

    @JsonIgnore
    private boolean credit_limitDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @Res_partnerProperty_account_receivable_idDefault(info = "默认规则")
    private Integer property_account_receivable_id;

    @JsonIgnore
    private boolean property_account_receivable_idDirtyFlag;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_ID]
     *
     */
    @Res_partnerProperty_purchase_currency_idDefault(info = "默认规则")
    private Integer property_purchase_currency_id;

    @JsonIgnore
    private boolean property_purchase_currency_idDirtyFlag;

    /**
     * 属性 [PICKING_WARN_MSG]
     *
     */
    @Res_partnerPicking_warn_msgDefault(info = "默认规则")
    private String picking_warn_msg;

    @JsonIgnore
    private boolean picking_warn_msgDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_partnerIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [SIGNUP_URL]
     *
     */
    @Res_partnerSignup_urlDefault(info = "默认规则")
    private String signup_url;

    @JsonIgnore
    private boolean signup_urlDirtyFlag;

    /**
     * 属性 [LANG]
     *
     */
    @Res_partnerLangDefault(info = "默认规则")
    private String lang;

    @JsonIgnore
    private boolean langDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Res_partnerMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_ID]
     *
     */
    @Res_partnerProperty_account_position_idDefault(info = "默认规则")
    private Integer property_account_position_id;

    @JsonIgnore
    private boolean property_account_position_idDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Res_partnerWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Res_partnerActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Res_partnerBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Res_partnerIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Res_partnerActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_COUNT]
     *
     */
    @Res_partnerSale_order_countDefault(info = "默认规则")
    private Integer sale_order_count;

    @JsonIgnore
    private boolean sale_order_countDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Res_partnerImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [ADDITIONAL_INFO]
     *
     */
    @Res_partnerAdditional_infoDefault(info = "默认规则")
    private String additional_info;

    @JsonIgnore
    private boolean additional_infoDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_IDS]
     *
     */
    @Res_partnerOpportunity_idsDefault(info = "默认规则")
    private String opportunity_ids;

    @JsonIgnore
    private boolean opportunity_idsDirtyFlag;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @Res_partnerContracts_countDefault(info = "默认规则")
    private Integer contracts_count;

    @JsonIgnore
    private boolean contracts_countDirtyFlag;

    /**
     * 属性 [DEBIT_LIMIT]
     *
     */
    @Res_partnerDebit_limitDefault(info = "默认规则")
    private Double debit_limit;

    @JsonIgnore
    private boolean debit_limitDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Res_partnerWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @Res_partnerSale_order_idsDefault(info = "默认规则")
    private String sale_order_ids;

    @JsonIgnore
    private boolean sale_order_idsDirtyFlag;

    /**
     * 属性 [LAST_WEBSITE_SO_ID]
     *
     */
    @Res_partnerLast_website_so_idDefault(info = "默认规则")
    private Integer last_website_so_id;

    @JsonIgnore
    private boolean last_website_so_idDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Res_partnerIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [COMMERCIAL_COMPANY_NAME]
     *
     */
    @Res_partnerCommercial_company_nameDefault(info = "默认规则")
    private String commercial_company_name;

    @JsonIgnore
    private boolean commercial_company_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_partnerWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TITLE_TEXT]
     *
     */
    @Res_partnerTitle_textDefault(info = "默认规则")
    private String title_text;

    @JsonIgnore
    private boolean title_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Res_partnerCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Res_partnerCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @Res_partnerState_id_textDefault(info = "默认规则")
    private String state_id_text;

    @JsonIgnore
    private boolean state_id_textDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @Res_partnerCommercial_partner_id_textDefault(info = "默认规则")
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;

    /**
     * 属性 [PARENT_NAME]
     *
     */
    @Res_partnerParent_nameDefault(info = "默认规则")
    private String parent_name;

    @JsonIgnore
    private boolean parent_nameDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Res_partnerUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_partnerCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [INDUSTRY_ID_TEXT]
     *
     */
    @Res_partnerIndustry_id_textDefault(info = "默认规则")
    private String industry_id_text;

    @JsonIgnore
    private boolean industry_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Res_partnerTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Res_partnerTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [STATE_ID]
     *
     */
    @Res_partnerState_idDefault(info = "默认规则")
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Res_partnerUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_partnerCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Res_partnerParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [TITLE]
     *
     */
    @Res_partnerTitleDefault(info = "默认规则")
    private Integer title;

    @JsonIgnore
    private boolean titleDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_partnerWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Res_partnerCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [INDUSTRY_ID]
     *
     */
    @Res_partnerIndustry_idDefault(info = "默认规则")
    private Integer industry_id;

    @JsonIgnore
    private boolean industry_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Res_partnerCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Res_partnerCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;


    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_IDS]
     */
    @JsonProperty("payment_token_ids")
    public String getPayment_token_ids(){
        return payment_token_ids ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_IDS]
     */
    @JsonProperty("payment_token_ids")
    public void setPayment_token_ids(String  payment_token_ids){
        this.payment_token_ids = payment_token_ids ;
        this.payment_token_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idsDirtyFlag(){
        return payment_token_idsDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return meeting_count ;
    }

    /**
     * 设置 [MEETING_COUNT]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

    /**
     * 获取 [MEETING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return meeting_countDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER_INVOICE_COUNT]
     */
    @JsonProperty("supplier_invoice_count")
    public Integer getSupplier_invoice_count(){
        return supplier_invoice_count ;
    }

    /**
     * 设置 [SUPPLIER_INVOICE_COUNT]
     */
    @JsonProperty("supplier_invoice_count")
    public void setSupplier_invoice_count(Integer  supplier_invoice_count){
        this.supplier_invoice_count = supplier_invoice_count ;
        this.supplier_invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER_INVOICE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_invoice_countDirtyFlag(){
        return supplier_invoice_countDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_NAME]
     */
    @JsonProperty("company_name")
    public String getCompany_name(){
        return company_name ;
    }

    /**
     * 设置 [COMPANY_NAME]
     */
    @JsonProperty("company_name")
    public void setCompany_name(String  company_name){
        this.company_name = company_name ;
        this.company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getCompany_nameDirtyFlag(){
        return company_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return last_time_entries_checked ;
    }

    /**
     * 设置 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return last_time_entries_checkedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [TRUST]
     */
    @JsonProperty("trust")
    public String getTrust(){
        return trust ;
    }

    /**
     * 设置 [TRUST]
     */
    @JsonProperty("trust")
    public void setTrust(String  trust){
        this.trust = trust ;
        this.trustDirtyFlag = true ;
    }

    /**
     * 获取 [TRUST]脏标记
     */
    @JsonIgnore
    public boolean getTrustDirtyFlag(){
        return trustDirtyFlag ;
    }

    /**
     * 获取 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return ibizfunction ;
    }

    /**
     * 设置 [IBIZFUNCTION]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZFUNCTION]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return ibizfunctionDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_INVOICED]
     */
    @JsonProperty("total_invoiced")
    public Double getTotal_invoiced(){
        return total_invoiced ;
    }

    /**
     * 设置 [TOTAL_INVOICED]
     */
    @JsonProperty("total_invoiced")
    public void setTotal_invoiced(Double  total_invoiced){
        this.total_invoiced = total_invoiced ;
        this.total_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getTotal_invoicedDirtyFlag(){
        return total_invoicedDirtyFlag ;
    }

    /**
     * 获取 [POS_ORDER_COUNT]
     */
    @JsonProperty("pos_order_count")
    public Integer getPos_order_count(){
        return pos_order_count ;
    }

    /**
     * 设置 [POS_ORDER_COUNT]
     */
    @JsonProperty("pos_order_count")
    public void setPos_order_count(Integer  pos_order_count){
        this.pos_order_count = pos_order_count ;
        this.pos_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [POS_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_countDirtyFlag(){
        return pos_order_countDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_ADDRESS]
     */
    @JsonProperty("contact_address")
    public String getContact_address(){
        return contact_address ;
    }

    /**
     * 设置 [CONTACT_ADDRESS]
     */
    @JsonProperty("contact_address")
    public void setContact_address(String  contact_address){
        this.contact_address = contact_address ;
        this.contact_addressDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_ADDRESS]脏标记
     */
    @JsonIgnore
    public boolean getContact_addressDirtyFlag(){
        return contact_addressDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_WARN]
     */
    @JsonProperty("invoice_warn")
    public String getInvoice_warn(){
        return invoice_warn ;
    }

    /**
     * 设置 [INVOICE_WARN]
     */
    @JsonProperty("invoice_warn")
    public void setInvoice_warn(String  invoice_warn){
        this.invoice_warn = invoice_warn ;
        this.invoice_warnDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warnDirtyFlag(){
        return invoice_warnDirtyFlag ;
    }

    /**
     * 获取 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return bank_ids ;
    }

    /**
     * 设置 [BANK_IDS]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return bank_idsDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_EXPIRATION]
     */
    @JsonProperty("signup_expiration")
    public Timestamp getSignup_expiration(){
        return signup_expiration ;
    }

    /**
     * 设置 [SIGNUP_EXPIRATION]
     */
    @JsonProperty("signup_expiration")
    public void setSignup_expiration(Timestamp  signup_expiration){
        this.signup_expiration = signup_expiration ;
        this.signup_expirationDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_EXPIRATION]脏标记
     */
    @JsonIgnore
    public boolean getSignup_expirationDirtyFlag(){
        return signup_expirationDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return purchase_order_count ;
    }

    /**
     * 设置 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return purchase_order_countDirtyFlag ;
    }

    /**
     * 获取 [HAS_UNRECONCILED_ENTRIES]
     */
    @JsonProperty("has_unreconciled_entries")
    public String getHas_unreconciled_entries(){
        return has_unreconciled_entries ;
    }

    /**
     * 设置 [HAS_UNRECONCILED_ENTRIES]
     */
    @JsonProperty("has_unreconciled_entries")
    public void setHas_unreconciled_entries(String  has_unreconciled_entries){
        this.has_unreconciled_entries = has_unreconciled_entries ;
        this.has_unreconciled_entriesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_UNRECONCILED_ENTRIES]脏标记
     */
    @JsonIgnore
    public boolean getHas_unreconciled_entriesDirtyFlag(){
        return has_unreconciled_entriesDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public String getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(String  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return website_description ;
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MEETING_IDS]
     */
    @JsonProperty("meeting_ids")
    public String getMeeting_ids(){
        return meeting_ids ;
    }

    /**
     * 设置 [MEETING_IDS]
     */
    @JsonProperty("meeting_ids")
    public void setMeeting_ids(String  meeting_ids){
        this.meeting_ids = meeting_ids ;
        this.meeting_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MEETING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_idsDirtyFlag(){
        return meeting_idsDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE]
     */
    @JsonProperty("employee")
    public String getEmployee(){
        return employee ;
    }

    /**
     * 设置 [EMPLOYEE]
     */
    @JsonProperty("employee")
    public void setEmployee(String  employee){
        this.employee = employee ;
        this.employeeDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getEmployeeDirtyFlag(){
        return employeeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return is_blacklisted ;
    }

    /**
     * 设置 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PRODUCT_PRICELIST]
     */
    @JsonProperty("property_product_pricelist")
    public Integer getProperty_product_pricelist(){
        return property_product_pricelist ;
    }

    /**
     * 设置 [PROPERTY_PRODUCT_PRICELIST]
     */
    @JsonProperty("property_product_pricelist")
    public void setProperty_product_pricelist(Integer  property_product_pricelist){
        this.property_product_pricelist = property_product_pricelist ;
        this.property_product_pricelistDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PRODUCT_PRICELIST]脏标记
     */
    @JsonIgnore
    public boolean getProperty_product_pricelistDirtyFlag(){
        return property_product_pricelistDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_TOKEN]
     */
    @JsonProperty("signup_token")
    public String getSignup_token(){
        return signup_token ;
    }

    /**
     * 设置 [SIGNUP_TOKEN]
     */
    @JsonProperty("signup_token")
    public void setSignup_token(String  signup_token){
        this.signup_token = signup_token ;
        this.signup_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getSignup_tokenDirtyFlag(){
        return signup_tokenDirtyFlag ;
    }

    /**
     * 获取 [REF_COMPANY_IDS]
     */
    @JsonProperty("ref_company_ids")
    public String getRef_company_ids(){
        return ref_company_ids ;
    }

    /**
     * 设置 [REF_COMPANY_IDS]
     */
    @JsonProperty("ref_company_ids")
    public void setRef_company_ids(String  ref_company_ids){
        this.ref_company_ids = ref_company_ids ;
        this.ref_company_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REF_COMPANY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRef_company_idsDirtyFlag(){
        return ref_company_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_COMPANY]
     */
    @JsonProperty("is_company")
    public String getIs_company(){
        return is_company ;
    }

    /**
     * 设置 [IS_COMPANY]
     */
    @JsonProperty("is_company")
    public void setIs_company(String  is_company){
        this.is_company = is_company ;
        this.is_companyDirtyFlag = true ;
    }

    /**
     * 获取 [IS_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getIs_companyDirtyFlag(){
        return is_companyDirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [EVENT_COUNT]
     */
    @JsonProperty("event_count")
    public Integer getEvent_count(){
        return event_count ;
    }

    /**
     * 设置 [EVENT_COUNT]
     */
    @JsonProperty("event_count")
    public void setEvent_count(Integer  event_count){
        this.event_count = event_count ;
        this.event_countDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_countDirtyFlag(){
        return event_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_LAST_NOTIF_ACK]
     */
    @JsonProperty("calendar_last_notif_ack")
    public Timestamp getCalendar_last_notif_ack(){
        return calendar_last_notif_ack ;
    }

    /**
     * 设置 [CALENDAR_LAST_NOTIF_ACK]
     */
    @JsonProperty("calendar_last_notif_ack")
    public void setCalendar_last_notif_ack(Timestamp  calendar_last_notif_ack){
        this.calendar_last_notif_ack = calendar_last_notif_ack ;
        this.calendar_last_notif_ackDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_LAST_NOTIF_ACK]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_last_notif_ackDirtyFlag(){
        return calendar_last_notif_ackDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_TYPE]
     */
    @JsonProperty("signup_type")
    public String getSignup_type(){
        return signup_type ;
    }

    /**
     * 设置 [SIGNUP_TYPE]
     */
    @JsonProperty("signup_type")
    public void setSignup_type(String  signup_type){
        this.signup_type = signup_type ;
        this.signup_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getSignup_typeDirtyFlag(){
        return signup_typeDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FORMATTED]
     */
    @JsonProperty("email_formatted")
    public String getEmail_formatted(){
        return email_formatted ;
    }

    /**
     * 设置 [EMAIL_FORMATTED]
     */
    @JsonProperty("email_formatted")
    public void setEmail_formatted(String  email_formatted){
        this.email_formatted = email_formatted ;
        this.email_formattedDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FORMATTED]脏标记
     */
    @JsonIgnore
    public boolean getEmail_formattedDirtyFlag(){
        return email_formattedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHARE]
     */
    @JsonProperty("partner_share")
    public String getPartner_share(){
        return partner_share ;
    }

    /**
     * 设置 [PARTNER_SHARE]
     */
    @JsonProperty("partner_share")
    public void setPartner_share(String  partner_share){
        this.partner_share = partner_share ;
        this.partner_shareDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHARE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shareDirtyFlag(){
        return partner_shareDirtyFlag ;
    }

    /**
     * 获取 [STREET2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return street2 ;
    }

    /**
     * 设置 [STREET2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [STREET2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return street2DirtyFlag ;
    }

    /**
     * 获取 [DEBIT]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return debit ;
    }

    /**
     * 设置 [DEBIT]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return debitDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_COUNT]
     */
    @JsonProperty("payment_token_count")
    public Integer getPayment_token_count(){
        return payment_token_count ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_COUNT]
     */
    @JsonProperty("payment_token_count")
    public void setPayment_token_count(Integer  payment_token_count){
        this.payment_token_count = payment_token_count ;
        this.payment_token_countDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_countDirtyFlag(){
        return payment_token_countDirtyFlag ;
    }

    /**
     * 获取 [REF]
     */
    @JsonProperty("ref")
    public String getRef(){
        return ref ;
    }

    /**
     * 设置 [REF]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [REF]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return refDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return partner_gid ;
    }

    /**
     * 设置 [PARTNER_GID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_GID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return partner_gidDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_VALID]
     */
    @JsonProperty("signup_valid")
    public String getSignup_valid(){
        return signup_valid ;
    }

    /**
     * 设置 [SIGNUP_VALID]
     */
    @JsonProperty("signup_valid")
    public void setSignup_valid(String  signup_valid){
        this.signup_valid = signup_valid ;
        this.signup_validDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_VALID]脏标记
     */
    @JsonIgnore
    public boolean getSignup_validDirtyFlag(){
        return signup_validDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_COUNT]
     */
    @JsonProperty("bank_account_count")
    public Integer getBank_account_count(){
        return bank_account_count ;
    }

    /**
     * 设置 [BANK_ACCOUNT_COUNT]
     */
    @JsonProperty("bank_account_count")
    public void setBank_account_count(Integer  bank_account_count){
        this.bank_account_count = bank_account_count ;
        this.bank_account_countDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_countDirtyFlag(){
        return bank_account_countDirtyFlag ;
    }

    /**
     * 获取 [STREET]
     */
    @JsonProperty("street")
    public String getStreet(){
        return street ;
    }

    /**
     * 设置 [STREET]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [STREET]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return streetDirtyFlag ;
    }

    /**
     * 获取 [SALE_WARN]
     */
    @JsonProperty("sale_warn")
    public String getSale_warn(){
        return sale_warn ;
    }

    /**
     * 设置 [SALE_WARN]
     */
    @JsonProperty("sale_warn")
    public void setSale_warn(String  sale_warn){
        this.sale_warn = sale_warn ;
        this.sale_warnDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getSale_warnDirtyFlag(){
        return sale_warnDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return message_bounce ;
    }

    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return message_bounceDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_COUNT]
     */
    @JsonProperty("opportunity_count")
    public Integer getOpportunity_count(){
        return opportunity_count ;
    }

    /**
     * 设置 [OPPORTUNITY_COUNT]
     */
    @JsonProperty("opportunity_count")
    public void setOpportunity_count(Integer  opportunity_count){
        this.opportunity_count = opportunity_count ;
        this.opportunity_countDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_countDirtyFlag(){
        return opportunity_countDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [SELF]
     */
    @JsonProperty("self")
    public Integer getSelf(){
        return self ;
    }

    /**
     * 设置 [SELF]
     */
    @JsonProperty("self")
    public void setSelf(Integer  self){
        this.self = self ;
        this.selfDirtyFlag = true ;
    }

    /**
     * 获取 [SELF]脏标记
     */
    @JsonIgnore
    public boolean getSelfDirtyFlag(){
        return selfDirtyFlag ;
    }

    /**
     * 获取 [IM_STATUS]
     */
    @JsonProperty("im_status")
    public String getIm_status(){
        return im_status ;
    }

    /**
     * 设置 [IM_STATUS]
     */
    @JsonProperty("im_status")
    public void setIm_status(String  im_status){
        this.im_status = im_status ;
        this.im_statusDirtyFlag = true ;
    }

    /**
     * 获取 [IM_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getIm_statusDirtyFlag(){
        return im_statusDirtyFlag ;
    }

    /**
     * 获取 [CUSTOMER]
     */
    @JsonProperty("customer")
    public String getCustomer(){
        return customer ;
    }

    /**
     * 设置 [CUSTOMER]
     */
    @JsonProperty("customer")
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.customerDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOMER]脏标记
     */
    @JsonIgnore
    public boolean getCustomerDirtyFlag(){
        return customerDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_WARN_MSG]
     */
    @JsonProperty("invoice_warn_msg")
    public String getInvoice_warn_msg(){
        return invoice_warn_msg ;
    }

    /**
     * 设置 [INVOICE_WARN_MSG]
     */
    @JsonProperty("invoice_warn_msg")
    public void setInvoice_warn_msg(String  invoice_warn_msg){
        this.invoice_warn_msg = invoice_warn_msg ;
        this.invoice_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warn_msgDirtyFlag(){
        return invoice_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [PICKING_WARN]
     */
    @JsonProperty("picking_warn")
    public String getPicking_warn(){
        return picking_warn ;
    }

    /**
     * 设置 [PICKING_WARN]
     */
    @JsonProperty("picking_warn")
    public void setPicking_warn(String  picking_warn){
        this.picking_warn = picking_warn ;
        this.picking_warnDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_WARN]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warnDirtyFlag(){
        return picking_warnDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return contract_ids ;
    }

    /**
     * 设置 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return contract_idsDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return website ;
    }

    /**
     * 设置 [WEBSITE]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return websiteDirtyFlag ;
    }

    /**
     * 获取 [MOBILE]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return mobile ;
    }

    /**
     * 设置 [MOBILE]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

    /**
     * 获取 [MOBILE]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return mobileDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [CITY]
     */
    @JsonProperty("city")
    public String getCity(){
        return city ;
    }

    /**
     * 设置 [CITY]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [CITY]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return cityDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_payment_term_id")
    public Integer getProperty_payment_term_id(){
        return property_payment_term_id ;
    }

    /**
     * 设置 [PROPERTY_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_payment_term_id")
    public void setProperty_payment_term_id(Integer  property_payment_term_id){
        this.property_payment_term_id = property_payment_term_id ;
        this.property_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_payment_term_idDirtyFlag(){
        return property_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return channel_ids ;
    }

    /**
     * 设置 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return channel_idsDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_WARN]
     */
    @JsonProperty("purchase_warn")
    public String getPurchase_warn(){
        return purchase_warn ;
    }

    /**
     * 设置 [PURCHASE_WARN]
     */
    @JsonProperty("purchase_warn")
    public void setPurchase_warn(String  purchase_warn){
        this.purchase_warn = purchase_warn ;
        this.purchase_warnDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warnDirtyFlag(){
        return purchase_warnDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ITEM_COUNT]
     */
    @JsonProperty("journal_item_count")
    public Integer getJournal_item_count(){
        return journal_item_count ;
    }

    /**
     * 设置 [JOURNAL_ITEM_COUNT]
     */
    @JsonProperty("journal_item_count")
    public void setJournal_item_count(Integer  journal_item_count){
        this.journal_item_count = journal_item_count ;
        this.journal_item_countDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ITEM_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_item_countDirtyFlag(){
        return journal_item_countDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER]
     */
    @JsonProperty("supplier")
    public String getSupplier(){
        return supplier ;
    }

    /**
     * 设置 [SUPPLIER]
     */
    @JsonProperty("supplier")
    public void setSupplier(String  supplier){
        this.supplier = supplier ;
        this.supplierDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER]脏标记
     */
    @JsonIgnore
    public boolean getSupplierDirtyFlag(){
        return supplierDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_SUPPLIER]
     */
    @JsonProperty("property_stock_supplier")
    public Integer getProperty_stock_supplier(){
        return property_stock_supplier ;
    }

    /**
     * 设置 [PROPERTY_STOCK_SUPPLIER]
     */
    @JsonProperty("property_stock_supplier")
    public void setProperty_stock_supplier(Integer  property_stock_supplier){
        this.property_stock_supplier = property_stock_supplier ;
        this.property_stock_supplierDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_SUPPLIER]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_supplierDirtyFlag(){
        return property_stock_supplierDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return property_account_payable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return property_account_payable_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SHORT_DESCRIPTION]
     */
    @JsonProperty("website_short_description")
    public String getWebsite_short_description(){
        return website_short_description ;
    }

    /**
     * 设置 [WEBSITE_SHORT_DESCRIPTION]
     */
    @JsonProperty("website_short_description")
    public void setWebsite_short_description(String  website_short_description){
        this.website_short_description = website_short_description ;
        this.website_short_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SHORT_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_short_descriptionDirtyFlag(){
        return website_short_descriptionDirtyFlag ;
    }

    /**
     * 获取 [SALE_WARN_MSG]
     */
    @JsonProperty("sale_warn_msg")
    public String getSale_warn_msg(){
        return sale_warn_msg ;
    }

    /**
     * 设置 [SALE_WARN_MSG]
     */
    @JsonProperty("sale_warn_msg")
    public void setSale_warn_msg(String  sale_warn_msg){
        this.sale_warn_msg = sale_warn_msg ;
        this.sale_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getSale_warn_msgDirtyFlag(){
        return sale_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [CREDIT]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return credit ;
    }

    /**
     * 设置 [CREDIT]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return creditDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [VAT]
     */
    @JsonProperty("vat")
    public String getVat(){
        return vat ;
    }

    /**
     * 设置 [VAT]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

    /**
     * 获取 [VAT]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return vatDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public Integer getProperty_supplier_payment_term_id(){
        return property_supplier_payment_term_id ;
    }

    /**
     * 设置 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public void setProperty_supplier_payment_term_id(Integer  property_supplier_payment_term_id){
        this.property_supplier_payment_term_id = property_supplier_payment_term_id ;
        this.property_supplier_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_supplier_payment_term_idDirtyFlag(){
        return property_supplier_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_CUSTOMER]
     */
    @JsonProperty("property_stock_customer")
    public Integer getProperty_stock_customer(){
        return property_stock_customer ;
    }

    /**
     * 设置 [PROPERTY_STOCK_CUSTOMER]
     */
    @JsonProperty("property_stock_customer")
    public void setProperty_stock_customer(Integer  property_stock_customer){
        this.property_stock_customer = property_stock_customer ;
        this.property_stock_customerDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_CUSTOMER]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_customerDirtyFlag(){
        return property_stock_customerDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return task_ids ;
    }

    /**
     * 设置 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return task_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_WARN_MSG]
     */
    @JsonProperty("purchase_warn_msg")
    public String getPurchase_warn_msg(){
        return purchase_warn_msg ;
    }

    /**
     * 设置 [PURCHASE_WARN_MSG]
     */
    @JsonProperty("purchase_warn_msg")
    public void setPurchase_warn_msg(String  purchase_warn_msg){
        this.purchase_warn_msg = purchase_warn_msg ;
        this.purchase_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warn_msgDirtyFlag(){
        return purchase_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [ZIP]
     */
    @JsonProperty("zip")
    public String getZip(){
        return zip ;
    }

    /**
     * 设置 [ZIP]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return zipDirtyFlag ;
    }

    /**
     * 获取 [TZ_OFFSET]
     */
    @JsonProperty("tz_offset")
    public String getTz_offset(){
        return tz_offset ;
    }

    /**
     * 设置 [TZ_OFFSET]
     */
    @JsonProperty("tz_offset")
    public void setTz_offset(String  tz_offset){
        this.tz_offset = tz_offset ;
        this.tz_offsetDirtyFlag = true ;
    }

    /**
     * 获取 [TZ_OFFSET]脏标记
     */
    @JsonIgnore
    public boolean getTz_offsetDirtyFlag(){
        return tz_offsetDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_TYPE]
     */
    @JsonProperty("company_type")
    public String getCompany_type(){
        return company_type ;
    }

    /**
     * 设置 [COMPANY_TYPE]
     */
    @JsonProperty("company_type")
    public void setCompany_type(String  company_type){
        this.company_type = company_type ;
        this.company_typeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getCompany_typeDirtyFlag(){
        return company_typeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return task_count ;
    }

    /**
     * 设置 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return task_countDirtyFlag ;
    }

    /**
     * 获取 [CREDIT_LIMIT]
     */
    @JsonProperty("credit_limit")
    public Double getCredit_limit(){
        return credit_limit ;
    }

    /**
     * 设置 [CREDIT_LIMIT]
     */
    @JsonProperty("credit_limit")
    public void setCredit_limit(Double  credit_limit){
        this.credit_limit = credit_limit ;
        this.credit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getCredit_limitDirtyFlag(){
        return credit_limitDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return property_account_receivable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return property_account_receivable_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_PURCHASE_CURRENCY_ID]
     */
    @JsonProperty("property_purchase_currency_id")
    public Integer getProperty_purchase_currency_id(){
        return property_purchase_currency_id ;
    }

    /**
     * 设置 [PROPERTY_PURCHASE_CURRENCY_ID]
     */
    @JsonProperty("property_purchase_currency_id")
    public void setProperty_purchase_currency_id(Integer  property_purchase_currency_id){
        this.property_purchase_currency_id = property_purchase_currency_id ;
        this.property_purchase_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_PURCHASE_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_purchase_currency_idDirtyFlag(){
        return property_purchase_currency_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_WARN_MSG]
     */
    @JsonProperty("picking_warn_msg")
    public String getPicking_warn_msg(){
        return picking_warn_msg ;
    }

    /**
     * 设置 [PICKING_WARN_MSG]
     */
    @JsonProperty("picking_warn_msg")
    public void setPicking_warn_msg(String  picking_warn_msg){
        this.picking_warn_msg = picking_warn_msg ;
        this.picking_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warn_msgDirtyFlag(){
        return picking_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [SIGNUP_URL]
     */
    @JsonProperty("signup_url")
    public String getSignup_url(){
        return signup_url ;
    }

    /**
     * 设置 [SIGNUP_URL]
     */
    @JsonProperty("signup_url")
    public void setSignup_url(String  signup_url){
        this.signup_url = signup_url ;
        this.signup_urlDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNUP_URL]脏标记
     */
    @JsonIgnore
    public boolean getSignup_urlDirtyFlag(){
        return signup_urlDirtyFlag ;
    }

    /**
     * 获取 [LANG]
     */
    @JsonProperty("lang")
    public String getLang(){
        return lang ;
    }

    /**
     * 设置 [LANG]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

    /**
     * 获取 [LANG]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return langDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_POSITION_ID]
     */
    @JsonProperty("property_account_position_id")
    public Integer getProperty_account_position_id(){
        return property_account_position_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_POSITION_ID]
     */
    @JsonProperty("property_account_position_id")
    public void setProperty_account_position_id(Integer  property_account_position_id){
        this.property_account_position_id = property_account_position_id ;
        this.property_account_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_position_idDirtyFlag(){
        return property_account_position_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_COUNT]
     */
    @JsonProperty("sale_order_count")
    public Integer getSale_order_count(){
        return sale_order_count ;
    }

    /**
     * 设置 [SALE_ORDER_COUNT]
     */
    @JsonProperty("sale_order_count")
    public void setSale_order_count(Integer  sale_order_count){
        this.sale_order_count = sale_order_count ;
        this.sale_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_countDirtyFlag(){
        return sale_order_countDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [ADDITIONAL_INFO]
     */
    @JsonProperty("additional_info")
    public String getAdditional_info(){
        return additional_info ;
    }

    /**
     * 设置 [ADDITIONAL_INFO]
     */
    @JsonProperty("additional_info")
    public void setAdditional_info(String  additional_info){
        this.additional_info = additional_info ;
        this.additional_infoDirtyFlag = true ;
    }

    /**
     * 获取 [ADDITIONAL_INFO]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_infoDirtyFlag(){
        return additional_infoDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_IDS]
     */
    @JsonProperty("opportunity_ids")
    public String getOpportunity_ids(){
        return opportunity_ids ;
    }

    /**
     * 设置 [OPPORTUNITY_IDS]
     */
    @JsonProperty("opportunity_ids")
    public void setOpportunity_ids(String  opportunity_ids){
        this.opportunity_ids = opportunity_ids ;
        this.opportunity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idsDirtyFlag(){
        return opportunity_idsDirtyFlag ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return contracts_count ;
    }

    /**
     * 设置 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return contracts_countDirtyFlag ;
    }

    /**
     * 获取 [DEBIT_LIMIT]
     */
    @JsonProperty("debit_limit")
    public Double getDebit_limit(){
        return debit_limit ;
    }

    /**
     * 设置 [DEBIT_LIMIT]
     */
    @JsonProperty("debit_limit")
    public void setDebit_limit(Double  debit_limit){
        this.debit_limit = debit_limit ;
        this.debit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getDebit_limitDirtyFlag(){
        return debit_limitDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return sale_order_ids ;
    }

    /**
     * 设置 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return sale_order_idsDirtyFlag ;
    }

    /**
     * 获取 [LAST_WEBSITE_SO_ID]
     */
    @JsonProperty("last_website_so_id")
    public Integer getLast_website_so_id(){
        return last_website_so_id ;
    }

    /**
     * 设置 [LAST_WEBSITE_SO_ID]
     */
    @JsonProperty("last_website_so_id")
    public void setLast_website_so_id(Integer  last_website_so_id){
        this.last_website_so_id = last_website_so_id ;
        this.last_website_so_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_WEBSITE_SO_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_website_so_idDirtyFlag(){
        return last_website_so_idDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_COMPANY_NAME]
     */
    @JsonProperty("commercial_company_name")
    public String getCommercial_company_name(){
        return commercial_company_name ;
    }

    /**
     * 设置 [COMMERCIAL_COMPANY_NAME]
     */
    @JsonProperty("commercial_company_name")
    public void setCommercial_company_name(String  commercial_company_name){
        this.commercial_company_name = commercial_company_name ;
        this.commercial_company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_COMPANY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_company_nameDirtyFlag(){
        return commercial_company_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TITLE_TEXT]
     */
    @JsonProperty("title_text")
    public String getTitle_text(){
        return title_text ;
    }

    /**
     * 设置 [TITLE_TEXT]
     */
    @JsonProperty("title_text")
    public void setTitle_text(String  title_text){
        this.title_text = title_text ;
        this.title_textDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTitle_textDirtyFlag(){
        return title_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return state_id_text ;
    }

    /**
     * 设置 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return state_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return commercial_partner_id_text ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_NAME]
     */
    @JsonProperty("parent_name")
    public String getParent_name(){
        return parent_name ;
    }

    /**
     * 设置 [PARENT_NAME]
     */
    @JsonProperty("parent_name")
    public void setParent_name(String  parent_name){
        this.parent_name = parent_name ;
        this.parent_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getParent_nameDirtyFlag(){
        return parent_nameDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [INDUSTRY_ID_TEXT]
     */
    @JsonProperty("industry_id_text")
    public String getIndustry_id_text(){
        return industry_id_text ;
    }

    /**
     * 设置 [INDUSTRY_ID_TEXT]
     */
    @JsonProperty("industry_id_text")
    public void setIndustry_id_text(String  industry_id_text){
        this.industry_id_text = industry_id_text ;
        this.industry_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INDUSTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_id_textDirtyFlag(){
        return industry_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return state_id ;
    }

    /**
     * 设置 [STATE_ID]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return state_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [TITLE]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return title ;
    }

    /**
     * 设置 [TITLE]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return titleDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [INDUSTRY_ID]
     */
    @JsonProperty("industry_id")
    public Integer getIndustry_id(){
        return industry_id ;
    }

    /**
     * 设置 [INDUSTRY_ID]
     */
    @JsonProperty("industry_id")
    public void setIndustry_id(Integer  industry_id){
        this.industry_id = industry_id ;
        this.industry_idDirtyFlag = true ;
    }

    /**
     * 获取 [INDUSTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_idDirtyFlag(){
        return industry_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }



    public Res_partner toDO() {
        Res_partner srfdomain = new Res_partner();
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getPayment_token_idsDirtyFlag())
            srfdomain.setPayment_token_ids(payment_token_ids);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getMeeting_countDirtyFlag())
            srfdomain.setMeeting_count(meeting_count);
        if(getSupplier_invoice_countDirtyFlag())
            srfdomain.setSupplier_invoice_count(supplier_invoice_count);
        if(getCompany_nameDirtyFlag())
            srfdomain.setCompany_name(company_name);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getLast_time_entries_checkedDirtyFlag())
            srfdomain.setLast_time_entries_checked(last_time_entries_checked);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getTrustDirtyFlag())
            srfdomain.setTrust(trust);
        if(getIbizfunctionDirtyFlag())
            srfdomain.setIbizfunction(ibizfunction);
        if(getTotal_invoicedDirtyFlag())
            srfdomain.setTotal_invoiced(total_invoiced);
        if(getPos_order_countDirtyFlag())
            srfdomain.setPos_order_count(pos_order_count);
        if(getContact_addressDirtyFlag())
            srfdomain.setContact_address(contact_address);
        if(getInvoice_warnDirtyFlag())
            srfdomain.setInvoice_warn(invoice_warn);
        if(getBank_idsDirtyFlag())
            srfdomain.setBank_ids(bank_ids);
        if(getSignup_expirationDirtyFlag())
            srfdomain.setSignup_expiration(signup_expiration);
        if(getPurchase_order_countDirtyFlag())
            srfdomain.setPurchase_order_count(purchase_order_count);
        if(getHas_unreconciled_entriesDirtyFlag())
            srfdomain.setHas_unreconciled_entries(has_unreconciled_entries);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getWebsite_descriptionDirtyFlag())
            srfdomain.setWebsite_description(website_description);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMeeting_idsDirtyFlag())
            srfdomain.setMeeting_ids(meeting_ids);
        if(getEmployeeDirtyFlag())
            srfdomain.setEmployee(employee);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getIs_blacklistedDirtyFlag())
            srfdomain.setIs_blacklisted(is_blacklisted);
        if(getProperty_product_pricelistDirtyFlag())
            srfdomain.setProperty_product_pricelist(property_product_pricelist);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getSignup_tokenDirtyFlag())
            srfdomain.setSignup_token(signup_token);
        if(getRef_company_idsDirtyFlag())
            srfdomain.setRef_company_ids(ref_company_ids);
        if(getIs_companyDirtyFlag())
            srfdomain.setIs_company(is_company);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getEvent_countDirtyFlag())
            srfdomain.setEvent_count(event_count);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getCalendar_last_notif_ackDirtyFlag())
            srfdomain.setCalendar_last_notif_ack(calendar_last_notif_ack);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getSignup_typeDirtyFlag())
            srfdomain.setSignup_type(signup_type);
        if(getEmail_formattedDirtyFlag())
            srfdomain.setEmail_formatted(email_formatted);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getPartner_shareDirtyFlag())
            srfdomain.setPartner_share(partner_share);
        if(getStreet2DirtyFlag())
            srfdomain.setStreet2(street2);
        if(getDebitDirtyFlag())
            srfdomain.setDebit(debit);
        if(getPayment_token_countDirtyFlag())
            srfdomain.setPayment_token_count(payment_token_count);
        if(getRefDirtyFlag())
            srfdomain.setRef(ref);
        if(getPartner_gidDirtyFlag())
            srfdomain.setPartner_gid(partner_gid);
        if(getSignup_validDirtyFlag())
            srfdomain.setSignup_valid(signup_valid);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getBank_account_countDirtyFlag())
            srfdomain.setBank_account_count(bank_account_count);
        if(getStreetDirtyFlag())
            srfdomain.setStreet(street);
        if(getSale_warnDirtyFlag())
            srfdomain.setSale_warn(sale_warn);
        if(getMessage_bounceDirtyFlag())
            srfdomain.setMessage_bounce(message_bounce);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getOpportunity_countDirtyFlag())
            srfdomain.setOpportunity_count(opportunity_count);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getSelfDirtyFlag())
            srfdomain.setSelf(self);
        if(getIm_statusDirtyFlag())
            srfdomain.setIm_status(im_status);
        if(getCustomerDirtyFlag())
            srfdomain.setCustomer(customer);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getInvoice_warn_msgDirtyFlag())
            srfdomain.setInvoice_warn_msg(invoice_warn_msg);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getPicking_warnDirtyFlag())
            srfdomain.setPicking_warn(picking_warn);
        if(getContract_idsDirtyFlag())
            srfdomain.setContract_ids(contract_ids);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWebsiteDirtyFlag())
            srfdomain.setWebsite(website);
        if(getMobileDirtyFlag())
            srfdomain.setMobile(mobile);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getCityDirtyFlag())
            srfdomain.setCity(city);
        if(getProperty_payment_term_idDirtyFlag())
            srfdomain.setProperty_payment_term_id(property_payment_term_id);
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getChannel_idsDirtyFlag())
            srfdomain.setChannel_ids(channel_ids);
        if(getPurchase_warnDirtyFlag())
            srfdomain.setPurchase_warn(purchase_warn);
        if(getJournal_item_countDirtyFlag())
            srfdomain.setJournal_item_count(journal_item_count);
        if(getSupplierDirtyFlag())
            srfdomain.setSupplier(supplier);
        if(getProperty_stock_supplierDirtyFlag())
            srfdomain.setProperty_stock_supplier(property_stock_supplier);
        if(getProperty_account_payable_idDirtyFlag())
            srfdomain.setProperty_account_payable_id(property_account_payable_id);
        if(getWebsite_short_descriptionDirtyFlag())
            srfdomain.setWebsite_short_description(website_short_description);
        if(getSale_warn_msgDirtyFlag())
            srfdomain.setSale_warn_msg(sale_warn_msg);
        if(getCreditDirtyFlag())
            srfdomain.setCredit(credit);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getVatDirtyFlag())
            srfdomain.setVat(vat);
        if(getProperty_supplier_payment_term_idDirtyFlag())
            srfdomain.setProperty_supplier_payment_term_id(property_supplier_payment_term_id);
        if(getProperty_stock_customerDirtyFlag())
            srfdomain.setProperty_stock_customer(property_stock_customer);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(getTask_idsDirtyFlag())
            srfdomain.setTask_ids(task_ids);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getPurchase_warn_msgDirtyFlag())
            srfdomain.setPurchase_warn_msg(purchase_warn_msg);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getZipDirtyFlag())
            srfdomain.setZip(zip);
        if(getTz_offsetDirtyFlag())
            srfdomain.setTz_offset(tz_offset);
        if(getCompany_typeDirtyFlag())
            srfdomain.setCompany_type(company_type);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getTask_countDirtyFlag())
            srfdomain.setTask_count(task_count);
        if(getCredit_limitDirtyFlag())
            srfdomain.setCredit_limit(credit_limit);
        if(getProperty_account_receivable_idDirtyFlag())
            srfdomain.setProperty_account_receivable_id(property_account_receivable_id);
        if(getProperty_purchase_currency_idDirtyFlag())
            srfdomain.setProperty_purchase_currency_id(property_purchase_currency_id);
        if(getPicking_warn_msgDirtyFlag())
            srfdomain.setPicking_warn_msg(picking_warn_msg);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getSignup_urlDirtyFlag())
            srfdomain.setSignup_url(signup_url);
        if(getLangDirtyFlag())
            srfdomain.setLang(lang);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getProperty_account_position_idDirtyFlag())
            srfdomain.setProperty_account_position_id(property_account_position_id);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getSale_order_countDirtyFlag())
            srfdomain.setSale_order_count(sale_order_count);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getAdditional_infoDirtyFlag())
            srfdomain.setAdditional_info(additional_info);
        if(getOpportunity_idsDirtyFlag())
            srfdomain.setOpportunity_ids(opportunity_ids);
        if(getContracts_countDirtyFlag())
            srfdomain.setContracts_count(contracts_count);
        if(getDebit_limitDirtyFlag())
            srfdomain.setDebit_limit(debit_limit);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getSale_order_idsDirtyFlag())
            srfdomain.setSale_order_ids(sale_order_ids);
        if(getLast_website_so_idDirtyFlag())
            srfdomain.setLast_website_so_id(last_website_so_id);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getCommercial_company_nameDirtyFlag())
            srfdomain.setCommercial_company_name(commercial_company_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTitle_textDirtyFlag())
            srfdomain.setTitle_text(title_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getState_id_textDirtyFlag())
            srfdomain.setState_id_text(state_id_text);
        if(getCommercial_partner_id_textDirtyFlag())
            srfdomain.setCommercial_partner_id_text(commercial_partner_id_text);
        if(getParent_nameDirtyFlag())
            srfdomain.setParent_name(parent_name);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getIndustry_id_textDirtyFlag())
            srfdomain.setIndustry_id_text(industry_id_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getState_idDirtyFlag())
            srfdomain.setState_id(state_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getTitleDirtyFlag())
            srfdomain.setTitle(title);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getIndustry_idDirtyFlag())
            srfdomain.setIndustry_id(industry_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);

        return srfdomain;
    }

    public void fromDO(Res_partner srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getPayment_token_idsDirtyFlag())
            this.setPayment_token_ids(srfdomain.getPayment_token_ids());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getMeeting_countDirtyFlag())
            this.setMeeting_count(srfdomain.getMeeting_count());
        if(srfdomain.getSupplier_invoice_countDirtyFlag())
            this.setSupplier_invoice_count(srfdomain.getSupplier_invoice_count());
        if(srfdomain.getCompany_nameDirtyFlag())
            this.setCompany_name(srfdomain.getCompany_name());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getLast_time_entries_checkedDirtyFlag())
            this.setLast_time_entries_checked(srfdomain.getLast_time_entries_checked());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getTrustDirtyFlag())
            this.setTrust(srfdomain.getTrust());
        if(srfdomain.getIbizfunctionDirtyFlag())
            this.setIbizfunction(srfdomain.getIbizfunction());
        if(srfdomain.getTotal_invoicedDirtyFlag())
            this.setTotal_invoiced(srfdomain.getTotal_invoiced());
        if(srfdomain.getPos_order_countDirtyFlag())
            this.setPos_order_count(srfdomain.getPos_order_count());
        if(srfdomain.getContact_addressDirtyFlag())
            this.setContact_address(srfdomain.getContact_address());
        if(srfdomain.getInvoice_warnDirtyFlag())
            this.setInvoice_warn(srfdomain.getInvoice_warn());
        if(srfdomain.getBank_idsDirtyFlag())
            this.setBank_ids(srfdomain.getBank_ids());
        if(srfdomain.getSignup_expirationDirtyFlag())
            this.setSignup_expiration(srfdomain.getSignup_expiration());
        if(srfdomain.getPurchase_order_countDirtyFlag())
            this.setPurchase_order_count(srfdomain.getPurchase_order_count());
        if(srfdomain.getHas_unreconciled_entriesDirtyFlag())
            this.setHas_unreconciled_entries(srfdomain.getHas_unreconciled_entries());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getWebsite_descriptionDirtyFlag())
            this.setWebsite_description(srfdomain.getWebsite_description());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMeeting_idsDirtyFlag())
            this.setMeeting_ids(srfdomain.getMeeting_ids());
        if(srfdomain.getEmployeeDirtyFlag())
            this.setEmployee(srfdomain.getEmployee());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getIs_blacklistedDirtyFlag())
            this.setIs_blacklisted(srfdomain.getIs_blacklisted());
        if(srfdomain.getProperty_product_pricelistDirtyFlag())
            this.setProperty_product_pricelist(srfdomain.getProperty_product_pricelist());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getSignup_tokenDirtyFlag())
            this.setSignup_token(srfdomain.getSignup_token());
        if(srfdomain.getRef_company_idsDirtyFlag())
            this.setRef_company_ids(srfdomain.getRef_company_ids());
        if(srfdomain.getIs_companyDirtyFlag())
            this.setIs_company(srfdomain.getIs_company());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getEvent_countDirtyFlag())
            this.setEvent_count(srfdomain.getEvent_count());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getCalendar_last_notif_ackDirtyFlag())
            this.setCalendar_last_notif_ack(srfdomain.getCalendar_last_notif_ack());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getSignup_typeDirtyFlag())
            this.setSignup_type(srfdomain.getSignup_type());
        if(srfdomain.getEmail_formattedDirtyFlag())
            this.setEmail_formatted(srfdomain.getEmail_formatted());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getPartner_shareDirtyFlag())
            this.setPartner_share(srfdomain.getPartner_share());
        if(srfdomain.getStreet2DirtyFlag())
            this.setStreet2(srfdomain.getStreet2());
        if(srfdomain.getDebitDirtyFlag())
            this.setDebit(srfdomain.getDebit());
        if(srfdomain.getPayment_token_countDirtyFlag())
            this.setPayment_token_count(srfdomain.getPayment_token_count());
        if(srfdomain.getRefDirtyFlag())
            this.setRef(srfdomain.getRef());
        if(srfdomain.getPartner_gidDirtyFlag())
            this.setPartner_gid(srfdomain.getPartner_gid());
        if(srfdomain.getSignup_validDirtyFlag())
            this.setSignup_valid(srfdomain.getSignup_valid());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getBank_account_countDirtyFlag())
            this.setBank_account_count(srfdomain.getBank_account_count());
        if(srfdomain.getStreetDirtyFlag())
            this.setStreet(srfdomain.getStreet());
        if(srfdomain.getSale_warnDirtyFlag())
            this.setSale_warn(srfdomain.getSale_warn());
        if(srfdomain.getMessage_bounceDirtyFlag())
            this.setMessage_bounce(srfdomain.getMessage_bounce());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getOpportunity_countDirtyFlag())
            this.setOpportunity_count(srfdomain.getOpportunity_count());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getSelfDirtyFlag())
            this.setSelf(srfdomain.getSelf());
        if(srfdomain.getIm_statusDirtyFlag())
            this.setIm_status(srfdomain.getIm_status());
        if(srfdomain.getCustomerDirtyFlag())
            this.setCustomer(srfdomain.getCustomer());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getInvoice_warn_msgDirtyFlag())
            this.setInvoice_warn_msg(srfdomain.getInvoice_warn_msg());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getPicking_warnDirtyFlag())
            this.setPicking_warn(srfdomain.getPicking_warn());
        if(srfdomain.getContract_idsDirtyFlag())
            this.setContract_ids(srfdomain.getContract_ids());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWebsiteDirtyFlag())
            this.setWebsite(srfdomain.getWebsite());
        if(srfdomain.getMobileDirtyFlag())
            this.setMobile(srfdomain.getMobile());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getCityDirtyFlag())
            this.setCity(srfdomain.getCity());
        if(srfdomain.getProperty_payment_term_idDirtyFlag())
            this.setProperty_payment_term_id(srfdomain.getProperty_payment_term_id());
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getChannel_idsDirtyFlag())
            this.setChannel_ids(srfdomain.getChannel_ids());
        if(srfdomain.getPurchase_warnDirtyFlag())
            this.setPurchase_warn(srfdomain.getPurchase_warn());
        if(srfdomain.getJournal_item_countDirtyFlag())
            this.setJournal_item_count(srfdomain.getJournal_item_count());
        if(srfdomain.getSupplierDirtyFlag())
            this.setSupplier(srfdomain.getSupplier());
        if(srfdomain.getProperty_stock_supplierDirtyFlag())
            this.setProperty_stock_supplier(srfdomain.getProperty_stock_supplier());
        if(srfdomain.getProperty_account_payable_idDirtyFlag())
            this.setProperty_account_payable_id(srfdomain.getProperty_account_payable_id());
        if(srfdomain.getWebsite_short_descriptionDirtyFlag())
            this.setWebsite_short_description(srfdomain.getWebsite_short_description());
        if(srfdomain.getSale_warn_msgDirtyFlag())
            this.setSale_warn_msg(srfdomain.getSale_warn_msg());
        if(srfdomain.getCreditDirtyFlag())
            this.setCredit(srfdomain.getCredit());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getVatDirtyFlag())
            this.setVat(srfdomain.getVat());
        if(srfdomain.getProperty_supplier_payment_term_idDirtyFlag())
            this.setProperty_supplier_payment_term_id(srfdomain.getProperty_supplier_payment_term_id());
        if(srfdomain.getProperty_stock_customerDirtyFlag())
            this.setProperty_stock_customer(srfdomain.getProperty_stock_customer());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.getTask_idsDirtyFlag())
            this.setTask_ids(srfdomain.getTask_ids());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getPurchase_warn_msgDirtyFlag())
            this.setPurchase_warn_msg(srfdomain.getPurchase_warn_msg());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getZipDirtyFlag())
            this.setZip(srfdomain.getZip());
        if(srfdomain.getTz_offsetDirtyFlag())
            this.setTz_offset(srfdomain.getTz_offset());
        if(srfdomain.getCompany_typeDirtyFlag())
            this.setCompany_type(srfdomain.getCompany_type());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getTask_countDirtyFlag())
            this.setTask_count(srfdomain.getTask_count());
        if(srfdomain.getCredit_limitDirtyFlag())
            this.setCredit_limit(srfdomain.getCredit_limit());
        if(srfdomain.getProperty_account_receivable_idDirtyFlag())
            this.setProperty_account_receivable_id(srfdomain.getProperty_account_receivable_id());
        if(srfdomain.getProperty_purchase_currency_idDirtyFlag())
            this.setProperty_purchase_currency_id(srfdomain.getProperty_purchase_currency_id());
        if(srfdomain.getPicking_warn_msgDirtyFlag())
            this.setPicking_warn_msg(srfdomain.getPicking_warn_msg());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getSignup_urlDirtyFlag())
            this.setSignup_url(srfdomain.getSignup_url());
        if(srfdomain.getLangDirtyFlag())
            this.setLang(srfdomain.getLang());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getProperty_account_position_idDirtyFlag())
            this.setProperty_account_position_id(srfdomain.getProperty_account_position_id());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getSale_order_countDirtyFlag())
            this.setSale_order_count(srfdomain.getSale_order_count());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getAdditional_infoDirtyFlag())
            this.setAdditional_info(srfdomain.getAdditional_info());
        if(srfdomain.getOpportunity_idsDirtyFlag())
            this.setOpportunity_ids(srfdomain.getOpportunity_ids());
        if(srfdomain.getContracts_countDirtyFlag())
            this.setContracts_count(srfdomain.getContracts_count());
        if(srfdomain.getDebit_limitDirtyFlag())
            this.setDebit_limit(srfdomain.getDebit_limit());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getSale_order_idsDirtyFlag())
            this.setSale_order_ids(srfdomain.getSale_order_ids());
        if(srfdomain.getLast_website_so_idDirtyFlag())
            this.setLast_website_so_id(srfdomain.getLast_website_so_id());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getCommercial_company_nameDirtyFlag())
            this.setCommercial_company_name(srfdomain.getCommercial_company_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTitle_textDirtyFlag())
            this.setTitle_text(srfdomain.getTitle_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getState_id_textDirtyFlag())
            this.setState_id_text(srfdomain.getState_id_text());
        if(srfdomain.getCommercial_partner_id_textDirtyFlag())
            this.setCommercial_partner_id_text(srfdomain.getCommercial_partner_id_text());
        if(srfdomain.getParent_nameDirtyFlag())
            this.setParent_name(srfdomain.getParent_name());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getIndustry_id_textDirtyFlag())
            this.setIndustry_id_text(srfdomain.getIndustry_id_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getState_idDirtyFlag())
            this.setState_id(srfdomain.getState_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getTitleDirtyFlag())
            this.setTitle(srfdomain.getTitle());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getIndustry_idDirtyFlag())
            this.setIndustry_id(srfdomain.getIndustry_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());

    }

    public List<Res_partnerDTO> fromDOPage(List<Res_partner> poPage)   {
        if(poPage == null)
            return null;
        List<Res_partnerDTO> dtos=new ArrayList<Res_partnerDTO>();
        for(Res_partner domain : poPage) {
            Res_partnerDTO dto = new Res_partnerDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


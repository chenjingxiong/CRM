package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Calendar_eventDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_eventService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_event" })
@RestController
@RequestMapping("")
public class Calendar_eventResource {

    @Autowired
    private ICalendar_eventService calendar_eventService;

    public ICalendar_eventService getCalendar_eventService() {
        return this.calendar_eventService;
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_event" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/calendar_events/{calendar_event_id}")

    public ResponseEntity<Calendar_eventDTO> update(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_eventDTO calendar_eventdto) {
		Calendar_event domain = calendar_eventdto.toDO();
        domain.setId(calendar_event_id);
		calendar_eventService.update(domain);
		Calendar_eventDTO dto = new Calendar_eventDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_event" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/calendar_events/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_event" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_event(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_event" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/calendar_events/{calendar_event_id}")
    public ResponseEntity<Calendar_eventDTO> get(@PathVariable("calendar_event_id") Integer calendar_event_id) {
        Calendar_eventDTO dto = new Calendar_eventDTO();
        Calendar_event domain = calendar_eventService.get(calendar_event_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_event" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/calendar_events/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_event" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/calendar_events/{calendar_event_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_id") Integer calendar_event_id) {
        Calendar_eventDTO calendar_eventdto = new Calendar_eventDTO();
		Calendar_event domain = new Calendar_event();
		calendar_eventdto.setId(calendar_event_id);
		domain.setId(calendar_event_id);
        Boolean rst = calendar_eventService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Calendar_event" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/calendar_events/{calendar_event_id}/getdraft")

    public ResponseEntity<Calendar_eventDTO> getDraft(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_eventDTO calendar_eventdto) {
        Calendar_event calendar_event = calendar_eventdto.toDO();
    	calendar_event = calendar_eventService.getDraft(calendar_event) ;
    	calendar_eventdto.fromDO(calendar_event);
        return ResponseEntity.status(HttpStatus.OK).body(calendar_eventdto);
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_event" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events")

    public ResponseEntity<Calendar_eventDTO> create(@RequestBody Calendar_eventDTO calendar_eventdto) {
        Calendar_eventDTO dto = new Calendar_eventDTO();
        Calendar_event domain = calendar_eventdto.toDO();
		calendar_eventService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Calendar_event" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/{calendar_event_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_eventDTO calendar_eventdto) {
        Calendar_event calendar_event = calendar_eventdto.toDO();
    	Boolean b = calendar_eventService.save(calendar_event) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "CheckKey", tags = {"Calendar_event" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/calendar_events/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_eventDTO calendar_eventdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_event" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/calendar_events/fetchdefault")
	public ResponseEntity<Page<Calendar_eventDTO>> fetchDefault(Calendar_eventSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_eventDTO> list = new ArrayList<Calendar_eventDTO>();
        
        Page<Calendar_event> domains = calendar_eventService.searchDefault(context) ;
        for(Calendar_event calendar_event : domains.getContent()){
            Calendar_eventDTO dto = new Calendar_eventDTO();
            dto.fromDO(calendar_event);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_team.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Crm_teamDTO]
 */
public class Crm_teamDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OPPORTUNITIES_AMOUNT]
     *
     */
    @Crm_teamOpportunities_amountDefault(info = "默认规则")
    private Integer opportunities_amount;

    @JsonIgnore
    private boolean opportunities_amountDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Crm_teamIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Crm_teamCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [USE_OPPORTUNITIES]
     *
     */
    @Crm_teamUse_opportunitiesDefault(info = "默认规则")
    private String use_opportunities;

    @JsonIgnore
    private boolean use_opportunitiesDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Crm_teamDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [QUOTATIONS_COUNT]
     *
     */
    @Crm_teamQuotations_countDefault(info = "默认规则")
    private Integer quotations_count;

    @JsonIgnore
    private boolean quotations_countDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_TYPE]
     *
     */
    @Crm_teamDashboard_graph_typeDefault(info = "默认规则")
    private String dashboard_graph_type;

    @JsonIgnore
    private boolean dashboard_graph_typeDirtyFlag;

    /**
     * 属性 [IS_FAVORITE]
     *
     */
    @Crm_teamIs_favoriteDefault(info = "默认规则")
    private String is_favorite;

    @JsonIgnore
    private boolean is_favoriteDirtyFlag;

    /**
     * 属性 [OPPORTUNITIES_COUNT]
     *
     */
    @Crm_teamOpportunities_countDefault(info = "默认规则")
    private Integer opportunities_count;

    @JsonIgnore
    private boolean opportunities_countDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Crm_teamColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [USE_QUOTATIONS]
     *
     */
    @Crm_teamUse_quotationsDefault(info = "默认规则")
    private String use_quotations;

    @JsonIgnore
    private boolean use_quotationsDirtyFlag;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @Crm_teamReply_toDefault(info = "默认规则")
    private String reply_to;

    @JsonIgnore
    private boolean reply_toDirtyFlag;

    /**
     * 属性 [FAVORITE_USER_IDS]
     *
     */
    @Crm_teamFavorite_user_idsDefault(info = "默认规则")
    private String favorite_user_ids;

    @JsonIgnore
    private boolean favorite_user_idsDirtyFlag;

    /**
     * 属性 [TEAM_TYPE]
     *
     */
    @Crm_teamTeam_typeDefault(info = "默认规则")
    private String team_type;

    @JsonIgnore
    private boolean team_typeDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Crm_teamMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Crm_teamMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_MODEL]
     *
     */
    @Crm_teamDashboard_graph_modelDefault(info = "默认规则")
    private String dashboard_graph_model;

    @JsonIgnore
    private boolean dashboard_graph_modelDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Crm_teamMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_PERIOD_PIPELINE]
     *
     */
    @Crm_teamDashboard_graph_period_pipelineDefault(info = "默认规则")
    private String dashboard_graph_period_pipeline;

    @JsonIgnore
    private boolean dashboard_graph_period_pipelineDirtyFlag;

    /**
     * 属性 [ABANDONED_CARTS_COUNT]
     *
     */
    @Crm_teamAbandoned_carts_countDefault(info = "默认规则")
    private Integer abandoned_carts_count;

    @JsonIgnore
    private boolean abandoned_carts_countDirtyFlag;

    /**
     * 属性 [POS_CONFIG_IDS]
     *
     */
    @Crm_teamPos_config_idsDefault(info = "默认规则")
    private String pos_config_ids;

    @JsonIgnore
    private boolean pos_config_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Crm_teamMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [USE_LEADS]
     *
     */
    @Crm_teamUse_leadsDefault(info = "默认规则")
    private String use_leads;

    @JsonIgnore
    private boolean use_leadsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Crm_teamMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [WEBSITE_IDS]
     *
     */
    @Crm_teamWebsite_idsDefault(info = "默认规则")
    private String website_ids;

    @JsonIgnore
    private boolean website_idsDirtyFlag;

    /**
     * 属性 [QUOTATIONS_AMOUNT]
     *
     */
    @Crm_teamQuotations_amountDefault(info = "默认规则")
    private Integer quotations_amount;

    @JsonIgnore
    private boolean quotations_amountDirtyFlag;

    /**
     * 属性 [INVOICED]
     *
     */
    @Crm_teamInvoicedDefault(info = "默认规则")
    private Integer invoiced;

    @JsonIgnore
    private boolean invoicedDirtyFlag;

    /**
     * 属性 [ABANDONED_CARTS_AMOUNT]
     *
     */
    @Crm_teamAbandoned_carts_amountDefault(info = "默认规则")
    private Integer abandoned_carts_amount;

    @JsonIgnore
    private boolean abandoned_carts_amountDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Crm_teamActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Crm_teamMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Crm_team__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [SALES_TO_INVOICE_COUNT]
     *
     */
    @Crm_teamSales_to_invoice_countDefault(info = "默认规则")
    private Integer sales_to_invoice_count;

    @JsonIgnore
    private boolean sales_to_invoice_countDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Crm_teamMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [UNASSIGNED_LEADS_COUNT]
     *
     */
    @Crm_teamUnassigned_leads_countDefault(info = "默认规则")
    private Integer unassigned_leads_count;

    @JsonIgnore
    private boolean unassigned_leads_countDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Crm_teamMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP_POS]
     *
     */
    @Crm_teamDashboard_graph_group_posDefault(info = "默认规则")
    private String dashboard_graph_group_pos;

    @JsonIgnore
    private boolean dashboard_graph_group_posDirtyFlag;

    /**
     * 属性 [MEMBER_IDS]
     *
     */
    @Crm_teamMember_idsDefault(info = "默认规则")
    private String member_ids;

    @JsonIgnore
    private boolean member_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Crm_teamWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [INVOICED_TARGET]
     *
     */
    @Crm_teamInvoiced_targetDefault(info = "默认规则")
    private Integer invoiced_target;

    @JsonIgnore
    private boolean invoiced_targetDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP]
     *
     */
    @Crm_teamDashboard_graph_groupDefault(info = "默认规则")
    private String dashboard_graph_group;

    @JsonIgnore
    private boolean dashboard_graph_groupDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Crm_teamMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_DATA]
     *
     */
    @Crm_teamDashboard_graph_dataDefault(info = "默认规则")
    private String dashboard_graph_data;

    @JsonIgnore
    private boolean dashboard_graph_dataDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_PERIOD]
     *
     */
    @Crm_teamDashboard_graph_periodDefault(info = "默认规则")
    private String dashboard_graph_period;

    @JsonIgnore
    private boolean dashboard_graph_periodDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Crm_teamWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Crm_teamMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Crm_teamMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [POS_SESSIONS_OPEN_COUNT]
     *
     */
    @Crm_teamPos_sessions_open_countDefault(info = "默认规则")
    private Integer pos_sessions_open_count;

    @JsonIgnore
    private boolean pos_sessions_open_countDirtyFlag;

    /**
     * 属性 [USE_INVOICES]
     *
     */
    @Crm_teamUse_invoicesDefault(info = "默认规则")
    private String use_invoices;

    @JsonIgnore
    private boolean use_invoicesDirtyFlag;

    /**
     * 属性 [DASHBOARD_BUTTON_NAME]
     *
     */
    @Crm_teamDashboard_button_nameDefault(info = "默认规则")
    private String dashboard_button_name;

    @JsonIgnore
    private boolean dashboard_button_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Crm_teamMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [DASHBOARD_GRAPH_GROUP_PIPELINE]
     *
     */
    @Crm_teamDashboard_graph_group_pipelineDefault(info = "默认规则")
    private String dashboard_graph_group_pipeline;

    @JsonIgnore
    private boolean dashboard_graph_group_pipelineDirtyFlag;

    /**
     * 属性 [POS_ORDER_AMOUNT_TOTAL]
     *
     */
    @Crm_teamPos_order_amount_totalDefault(info = "默认规则")
    private Double pos_order_amount_total;

    @JsonIgnore
    private boolean pos_order_amount_totalDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Crm_teamMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Crm_teamNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Crm_teamAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Crm_teamCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @Crm_teamAlias_model_idDefault(info = "默认规则")
    private Integer alias_model_id;

    @JsonIgnore
    private boolean alias_model_idDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @Crm_teamAlias_parent_model_idDefault(info = "默认规则")
    private Integer alias_parent_model_id;

    @JsonIgnore
    private boolean alias_parent_model_idDirtyFlag;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @Crm_teamAlias_contactDefault(info = "默认规则")
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @Crm_teamAlias_user_idDefault(info = "默认规则")
    private Integer alias_user_id;

    @JsonIgnore
    private boolean alias_user_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Crm_teamCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @Crm_teamAlias_defaultsDefault(info = "默认规则")
    private String alias_defaults;

    @JsonIgnore
    private boolean alias_defaultsDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @Crm_teamAlias_parent_thread_idDefault(info = "默认规则")
    private Integer alias_parent_thread_id;

    @JsonIgnore
    private boolean alias_parent_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @Crm_teamAlias_force_thread_idDefault(info = "默认规则")
    private Integer alias_force_thread_id;

    @JsonIgnore
    private boolean alias_force_thread_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Crm_teamCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Crm_teamUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @Crm_teamAlias_nameDefault(info = "默认规则")
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Crm_teamWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Crm_teamWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Crm_teamAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Crm_teamCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Crm_teamCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Crm_teamUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [OPPORTUNITIES_AMOUNT]
     */
    @JsonProperty("opportunities_amount")
    public Integer getOpportunities_amount(){
        return opportunities_amount ;
    }

    /**
     * 设置 [OPPORTUNITIES_AMOUNT]
     */
    @JsonProperty("opportunities_amount")
    public void setOpportunities_amount(Integer  opportunities_amount){
        this.opportunities_amount = opportunities_amount ;
        this.opportunities_amountDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITIES_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunities_amountDirtyFlag(){
        return opportunities_amountDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [USE_OPPORTUNITIES]
     */
    @JsonProperty("use_opportunities")
    public String getUse_opportunities(){
        return use_opportunities ;
    }

    /**
     * 设置 [USE_OPPORTUNITIES]
     */
    @JsonProperty("use_opportunities")
    public void setUse_opportunities(String  use_opportunities){
        this.use_opportunities = use_opportunities ;
        this.use_opportunitiesDirtyFlag = true ;
    }

    /**
     * 获取 [USE_OPPORTUNITIES]脏标记
     */
    @JsonIgnore
    public boolean getUse_opportunitiesDirtyFlag(){
        return use_opportunitiesDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [QUOTATIONS_COUNT]
     */
    @JsonProperty("quotations_count")
    public Integer getQuotations_count(){
        return quotations_count ;
    }

    /**
     * 设置 [QUOTATIONS_COUNT]
     */
    @JsonProperty("quotations_count")
    public void setQuotations_count(Integer  quotations_count){
        this.quotations_count = quotations_count ;
        this.quotations_countDirtyFlag = true ;
    }

    /**
     * 获取 [QUOTATIONS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getQuotations_countDirtyFlag(){
        return quotations_countDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_TYPE]
     */
    @JsonProperty("dashboard_graph_type")
    public String getDashboard_graph_type(){
        return dashboard_graph_type ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_TYPE]
     */
    @JsonProperty("dashboard_graph_type")
    public void setDashboard_graph_type(String  dashboard_graph_type){
        this.dashboard_graph_type = dashboard_graph_type ;
        this.dashboard_graph_typeDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_typeDirtyFlag(){
        return dashboard_graph_typeDirtyFlag ;
    }

    /**
     * 获取 [IS_FAVORITE]
     */
    @JsonProperty("is_favorite")
    public String getIs_favorite(){
        return is_favorite ;
    }

    /**
     * 设置 [IS_FAVORITE]
     */
    @JsonProperty("is_favorite")
    public void setIs_favorite(String  is_favorite){
        this.is_favorite = is_favorite ;
        this.is_favoriteDirtyFlag = true ;
    }

    /**
     * 获取 [IS_FAVORITE]脏标记
     */
    @JsonIgnore
    public boolean getIs_favoriteDirtyFlag(){
        return is_favoriteDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITIES_COUNT]
     */
    @JsonProperty("opportunities_count")
    public Integer getOpportunities_count(){
        return opportunities_count ;
    }

    /**
     * 设置 [OPPORTUNITIES_COUNT]
     */
    @JsonProperty("opportunities_count")
    public void setOpportunities_count(Integer  opportunities_count){
        this.opportunities_count = opportunities_count ;
        this.opportunities_countDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITIES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunities_countDirtyFlag(){
        return opportunities_countDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [USE_QUOTATIONS]
     */
    @JsonProperty("use_quotations")
    public String getUse_quotations(){
        return use_quotations ;
    }

    /**
     * 设置 [USE_QUOTATIONS]
     */
    @JsonProperty("use_quotations")
    public void setUse_quotations(String  use_quotations){
        this.use_quotations = use_quotations ;
        this.use_quotationsDirtyFlag = true ;
    }

    /**
     * 获取 [USE_QUOTATIONS]脏标记
     */
    @JsonIgnore
    public boolean getUse_quotationsDirtyFlag(){
        return use_quotationsDirtyFlag ;
    }

    /**
     * 获取 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return reply_to ;
    }

    /**
     * 设置 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

    /**
     * 获取 [REPLY_TO]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return reply_toDirtyFlag ;
    }

    /**
     * 获取 [FAVORITE_USER_IDS]
     */
    @JsonProperty("favorite_user_ids")
    public String getFavorite_user_ids(){
        return favorite_user_ids ;
    }

    /**
     * 设置 [FAVORITE_USER_IDS]
     */
    @JsonProperty("favorite_user_ids")
    public void setFavorite_user_ids(String  favorite_user_ids){
        this.favorite_user_ids = favorite_user_ids ;
        this.favorite_user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [FAVORITE_USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getFavorite_user_idsDirtyFlag(){
        return favorite_user_idsDirtyFlag ;
    }

    /**
     * 获取 [TEAM_TYPE]
     */
    @JsonProperty("team_type")
    public String getTeam_type(){
        return team_type ;
    }

    /**
     * 设置 [TEAM_TYPE]
     */
    @JsonProperty("team_type")
    public void setTeam_type(String  team_type){
        this.team_type = team_type ;
        this.team_typeDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTeam_typeDirtyFlag(){
        return team_typeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_MODEL]
     */
    @JsonProperty("dashboard_graph_model")
    public String getDashboard_graph_model(){
        return dashboard_graph_model ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_MODEL]
     */
    @JsonProperty("dashboard_graph_model")
    public void setDashboard_graph_model(String  dashboard_graph_model){
        this.dashboard_graph_model = dashboard_graph_model ;
        this.dashboard_graph_modelDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_modelDirtyFlag(){
        return dashboard_graph_modelDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_PERIOD_PIPELINE]
     */
    @JsonProperty("dashboard_graph_period_pipeline")
    public String getDashboard_graph_period_pipeline(){
        return dashboard_graph_period_pipeline ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_PERIOD_PIPELINE]
     */
    @JsonProperty("dashboard_graph_period_pipeline")
    public void setDashboard_graph_period_pipeline(String  dashboard_graph_period_pipeline){
        this.dashboard_graph_period_pipeline = dashboard_graph_period_pipeline ;
        this.dashboard_graph_period_pipelineDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_PERIOD_PIPELINE]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_period_pipelineDirtyFlag(){
        return dashboard_graph_period_pipelineDirtyFlag ;
    }

    /**
     * 获取 [ABANDONED_CARTS_COUNT]
     */
    @JsonProperty("abandoned_carts_count")
    public Integer getAbandoned_carts_count(){
        return abandoned_carts_count ;
    }

    /**
     * 设置 [ABANDONED_CARTS_COUNT]
     */
    @JsonProperty("abandoned_carts_count")
    public void setAbandoned_carts_count(Integer  abandoned_carts_count){
        this.abandoned_carts_count = abandoned_carts_count ;
        this.abandoned_carts_countDirtyFlag = true ;
    }

    /**
     * 获取 [ABANDONED_CARTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getAbandoned_carts_countDirtyFlag(){
        return abandoned_carts_countDirtyFlag ;
    }

    /**
     * 获取 [POS_CONFIG_IDS]
     */
    @JsonProperty("pos_config_ids")
    public String getPos_config_ids(){
        return pos_config_ids ;
    }

    /**
     * 设置 [POS_CONFIG_IDS]
     */
    @JsonProperty("pos_config_ids")
    public void setPos_config_ids(String  pos_config_ids){
        this.pos_config_ids = pos_config_ids ;
        this.pos_config_idsDirtyFlag = true ;
    }

    /**
     * 获取 [POS_CONFIG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPos_config_idsDirtyFlag(){
        return pos_config_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [USE_LEADS]
     */
    @JsonProperty("use_leads")
    public String getUse_leads(){
        return use_leads ;
    }

    /**
     * 设置 [USE_LEADS]
     */
    @JsonProperty("use_leads")
    public void setUse_leads(String  use_leads){
        this.use_leads = use_leads ;
        this.use_leadsDirtyFlag = true ;
    }

    /**
     * 获取 [USE_LEADS]脏标记
     */
    @JsonIgnore
    public boolean getUse_leadsDirtyFlag(){
        return use_leadsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_IDS]
     */
    @JsonProperty("website_ids")
    public String getWebsite_ids(){
        return website_ids ;
    }

    /**
     * 设置 [WEBSITE_IDS]
     */
    @JsonProperty("website_ids")
    public void setWebsite_ids(String  website_ids){
        this.website_ids = website_ids ;
        this.website_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idsDirtyFlag(){
        return website_idsDirtyFlag ;
    }

    /**
     * 获取 [QUOTATIONS_AMOUNT]
     */
    @JsonProperty("quotations_amount")
    public Integer getQuotations_amount(){
        return quotations_amount ;
    }

    /**
     * 设置 [QUOTATIONS_AMOUNT]
     */
    @JsonProperty("quotations_amount")
    public void setQuotations_amount(Integer  quotations_amount){
        this.quotations_amount = quotations_amount ;
        this.quotations_amountDirtyFlag = true ;
    }

    /**
     * 获取 [QUOTATIONS_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getQuotations_amountDirtyFlag(){
        return quotations_amountDirtyFlag ;
    }

    /**
     * 获取 [INVOICED]
     */
    @JsonProperty("invoiced")
    public Integer getInvoiced(){
        return invoiced ;
    }

    /**
     * 设置 [INVOICED]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(Integer  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return invoicedDirtyFlag ;
    }

    /**
     * 获取 [ABANDONED_CARTS_AMOUNT]
     */
    @JsonProperty("abandoned_carts_amount")
    public Integer getAbandoned_carts_amount(){
        return abandoned_carts_amount ;
    }

    /**
     * 设置 [ABANDONED_CARTS_AMOUNT]
     */
    @JsonProperty("abandoned_carts_amount")
    public void setAbandoned_carts_amount(Integer  abandoned_carts_amount){
        this.abandoned_carts_amount = abandoned_carts_amount ;
        this.abandoned_carts_amountDirtyFlag = true ;
    }

    /**
     * 获取 [ABANDONED_CARTS_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAbandoned_carts_amountDirtyFlag(){
        return abandoned_carts_amountDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [SALES_TO_INVOICE_COUNT]
     */
    @JsonProperty("sales_to_invoice_count")
    public Integer getSales_to_invoice_count(){
        return sales_to_invoice_count ;
    }

    /**
     * 设置 [SALES_TO_INVOICE_COUNT]
     */
    @JsonProperty("sales_to_invoice_count")
    public void setSales_to_invoice_count(Integer  sales_to_invoice_count){
        this.sales_to_invoice_count = sales_to_invoice_count ;
        this.sales_to_invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [SALES_TO_INVOICE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSales_to_invoice_countDirtyFlag(){
        return sales_to_invoice_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [UNASSIGNED_LEADS_COUNT]
     */
    @JsonProperty("unassigned_leads_count")
    public Integer getUnassigned_leads_count(){
        return unassigned_leads_count ;
    }

    /**
     * 设置 [UNASSIGNED_LEADS_COUNT]
     */
    @JsonProperty("unassigned_leads_count")
    public void setUnassigned_leads_count(Integer  unassigned_leads_count){
        this.unassigned_leads_count = unassigned_leads_count ;
        this.unassigned_leads_countDirtyFlag = true ;
    }

    /**
     * 获取 [UNASSIGNED_LEADS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getUnassigned_leads_countDirtyFlag(){
        return unassigned_leads_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP_POS]
     */
    @JsonProperty("dashboard_graph_group_pos")
    public String getDashboard_graph_group_pos(){
        return dashboard_graph_group_pos ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP_POS]
     */
    @JsonProperty("dashboard_graph_group_pos")
    public void setDashboard_graph_group_pos(String  dashboard_graph_group_pos){
        this.dashboard_graph_group_pos = dashboard_graph_group_pos ;
        this.dashboard_graph_group_posDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP_POS]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_group_posDirtyFlag(){
        return dashboard_graph_group_posDirtyFlag ;
    }

    /**
     * 获取 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public String getMember_ids(){
        return member_ids ;
    }

    /**
     * 设置 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public void setMember_ids(String  member_ids){
        this.member_ids = member_ids ;
        this.member_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MEMBER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMember_idsDirtyFlag(){
        return member_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [INVOICED_TARGET]
     */
    @JsonProperty("invoiced_target")
    public Integer getInvoiced_target(){
        return invoiced_target ;
    }

    /**
     * 设置 [INVOICED_TARGET]
     */
    @JsonProperty("invoiced_target")
    public void setInvoiced_target(Integer  invoiced_target){
        this.invoiced_target = invoiced_target ;
        this.invoiced_targetDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICED_TARGET]脏标记
     */
    @JsonIgnore
    public boolean getInvoiced_targetDirtyFlag(){
        return invoiced_targetDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP]
     */
    @JsonProperty("dashboard_graph_group")
    public String getDashboard_graph_group(){
        return dashboard_graph_group ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP]
     */
    @JsonProperty("dashboard_graph_group")
    public void setDashboard_graph_group(String  dashboard_graph_group){
        this.dashboard_graph_group = dashboard_graph_group ;
        this.dashboard_graph_groupDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_groupDirtyFlag(){
        return dashboard_graph_groupDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_DATA]
     */
    @JsonProperty("dashboard_graph_data")
    public String getDashboard_graph_data(){
        return dashboard_graph_data ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_DATA]
     */
    @JsonProperty("dashboard_graph_data")
    public void setDashboard_graph_data(String  dashboard_graph_data){
        this.dashboard_graph_data = dashboard_graph_data ;
        this.dashboard_graph_dataDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_DATA]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_dataDirtyFlag(){
        return dashboard_graph_dataDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_PERIOD]
     */
    @JsonProperty("dashboard_graph_period")
    public String getDashboard_graph_period(){
        return dashboard_graph_period ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_PERIOD]
     */
    @JsonProperty("dashboard_graph_period")
    public void setDashboard_graph_period(String  dashboard_graph_period){
        this.dashboard_graph_period = dashboard_graph_period ;
        this.dashboard_graph_periodDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_PERIOD]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_periodDirtyFlag(){
        return dashboard_graph_periodDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [POS_SESSIONS_OPEN_COUNT]
     */
    @JsonProperty("pos_sessions_open_count")
    public Integer getPos_sessions_open_count(){
        return pos_sessions_open_count ;
    }

    /**
     * 设置 [POS_SESSIONS_OPEN_COUNT]
     */
    @JsonProperty("pos_sessions_open_count")
    public void setPos_sessions_open_count(Integer  pos_sessions_open_count){
        this.pos_sessions_open_count = pos_sessions_open_count ;
        this.pos_sessions_open_countDirtyFlag = true ;
    }

    /**
     * 获取 [POS_SESSIONS_OPEN_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPos_sessions_open_countDirtyFlag(){
        return pos_sessions_open_countDirtyFlag ;
    }

    /**
     * 获取 [USE_INVOICES]
     */
    @JsonProperty("use_invoices")
    public String getUse_invoices(){
        return use_invoices ;
    }

    /**
     * 设置 [USE_INVOICES]
     */
    @JsonProperty("use_invoices")
    public void setUse_invoices(String  use_invoices){
        this.use_invoices = use_invoices ;
        this.use_invoicesDirtyFlag = true ;
    }

    /**
     * 获取 [USE_INVOICES]脏标记
     */
    @JsonIgnore
    public boolean getUse_invoicesDirtyFlag(){
        return use_invoicesDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_BUTTON_NAME]
     */
    @JsonProperty("dashboard_button_name")
    public String getDashboard_button_name(){
        return dashboard_button_name ;
    }

    /**
     * 设置 [DASHBOARD_BUTTON_NAME]
     */
    @JsonProperty("dashboard_button_name")
    public void setDashboard_button_name(String  dashboard_button_name){
        this.dashboard_button_name = dashboard_button_name ;
        this.dashboard_button_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_BUTTON_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_button_nameDirtyFlag(){
        return dashboard_button_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP_PIPELINE]
     */
    @JsonProperty("dashboard_graph_group_pipeline")
    public String getDashboard_graph_group_pipeline(){
        return dashboard_graph_group_pipeline ;
    }

    /**
     * 设置 [DASHBOARD_GRAPH_GROUP_PIPELINE]
     */
    @JsonProperty("dashboard_graph_group_pipeline")
    public void setDashboard_graph_group_pipeline(String  dashboard_graph_group_pipeline){
        this.dashboard_graph_group_pipeline = dashboard_graph_group_pipeline ;
        this.dashboard_graph_group_pipelineDirtyFlag = true ;
    }

    /**
     * 获取 [DASHBOARD_GRAPH_GROUP_PIPELINE]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_group_pipelineDirtyFlag(){
        return dashboard_graph_group_pipelineDirtyFlag ;
    }

    /**
     * 获取 [POS_ORDER_AMOUNT_TOTAL]
     */
    @JsonProperty("pos_order_amount_total")
    public Double getPos_order_amount_total(){
        return pos_order_amount_total ;
    }

    /**
     * 设置 [POS_ORDER_AMOUNT_TOTAL]
     */
    @JsonProperty("pos_order_amount_total")
    public void setPos_order_amount_total(Double  pos_order_amount_total){
        this.pos_order_amount_total = pos_order_amount_total ;
        this.pos_order_amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [POS_ORDER_AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_amount_totalDirtyFlag(){
        return pos_order_amount_totalDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return alias_model_id ;
    }

    /**
     * 设置 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return alias_model_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return alias_parent_model_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return alias_parent_model_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return alias_contact ;
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return alias_contactDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return alias_user_id ;
    }

    /**
     * 设置 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return alias_user_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return alias_defaults ;
    }

    /**
     * 设置 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return alias_defaultsDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return alias_parent_thread_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return alias_parent_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return alias_force_thread_id ;
    }

    /**
     * 设置 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return alias_force_thread_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return alias_name ;
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return alias_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Crm_team toDO() {
        Crm_team srfdomain = new Crm_team();
        if(getOpportunities_amountDirtyFlag())
            srfdomain.setOpportunities_amount(opportunities_amount);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getUse_opportunitiesDirtyFlag())
            srfdomain.setUse_opportunities(use_opportunities);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getQuotations_countDirtyFlag())
            srfdomain.setQuotations_count(quotations_count);
        if(getDashboard_graph_typeDirtyFlag())
            srfdomain.setDashboard_graph_type(dashboard_graph_type);
        if(getIs_favoriteDirtyFlag())
            srfdomain.setIs_favorite(is_favorite);
        if(getOpportunities_countDirtyFlag())
            srfdomain.setOpportunities_count(opportunities_count);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getUse_quotationsDirtyFlag())
            srfdomain.setUse_quotations(use_quotations);
        if(getReply_toDirtyFlag())
            srfdomain.setReply_to(reply_to);
        if(getFavorite_user_idsDirtyFlag())
            srfdomain.setFavorite_user_ids(favorite_user_ids);
        if(getTeam_typeDirtyFlag())
            srfdomain.setTeam_type(team_type);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getDashboard_graph_modelDirtyFlag())
            srfdomain.setDashboard_graph_model(dashboard_graph_model);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getDashboard_graph_period_pipelineDirtyFlag())
            srfdomain.setDashboard_graph_period_pipeline(dashboard_graph_period_pipeline);
        if(getAbandoned_carts_countDirtyFlag())
            srfdomain.setAbandoned_carts_count(abandoned_carts_count);
        if(getPos_config_idsDirtyFlag())
            srfdomain.setPos_config_ids(pos_config_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getUse_leadsDirtyFlag())
            srfdomain.setUse_leads(use_leads);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getWebsite_idsDirtyFlag())
            srfdomain.setWebsite_ids(website_ids);
        if(getQuotations_amountDirtyFlag())
            srfdomain.setQuotations_amount(quotations_amount);
        if(getInvoicedDirtyFlag())
            srfdomain.setInvoiced(invoiced);
        if(getAbandoned_carts_amountDirtyFlag())
            srfdomain.setAbandoned_carts_amount(abandoned_carts_amount);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getSales_to_invoice_countDirtyFlag())
            srfdomain.setSales_to_invoice_count(sales_to_invoice_count);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getUnassigned_leads_countDirtyFlag())
            srfdomain.setUnassigned_leads_count(unassigned_leads_count);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getDashboard_graph_group_posDirtyFlag())
            srfdomain.setDashboard_graph_group_pos(dashboard_graph_group_pos);
        if(getMember_idsDirtyFlag())
            srfdomain.setMember_ids(member_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getInvoiced_targetDirtyFlag())
            srfdomain.setInvoiced_target(invoiced_target);
        if(getDashboard_graph_groupDirtyFlag())
            srfdomain.setDashboard_graph_group(dashboard_graph_group);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getDashboard_graph_dataDirtyFlag())
            srfdomain.setDashboard_graph_data(dashboard_graph_data);
        if(getDashboard_graph_periodDirtyFlag())
            srfdomain.setDashboard_graph_period(dashboard_graph_period);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getPos_sessions_open_countDirtyFlag())
            srfdomain.setPos_sessions_open_count(pos_sessions_open_count);
        if(getUse_invoicesDirtyFlag())
            srfdomain.setUse_invoices(use_invoices);
        if(getDashboard_button_nameDirtyFlag())
            srfdomain.setDashboard_button_name(dashboard_button_name);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getDashboard_graph_group_pipelineDirtyFlag())
            srfdomain.setDashboard_graph_group_pipeline(dashboard_graph_group_pipeline);
        if(getPos_order_amount_totalDirtyFlag())
            srfdomain.setPos_order_amount_total(pos_order_amount_total);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getAlias_model_idDirtyFlag())
            srfdomain.setAlias_model_id(alias_model_id);
        if(getAlias_parent_model_idDirtyFlag())
            srfdomain.setAlias_parent_model_id(alias_parent_model_id);
        if(getAlias_contactDirtyFlag())
            srfdomain.setAlias_contact(alias_contact);
        if(getAlias_user_idDirtyFlag())
            srfdomain.setAlias_user_id(alias_user_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getAlias_defaultsDirtyFlag())
            srfdomain.setAlias_defaults(alias_defaults);
        if(getAlias_parent_thread_idDirtyFlag())
            srfdomain.setAlias_parent_thread_id(alias_parent_thread_id);
        if(getAlias_force_thread_idDirtyFlag())
            srfdomain.setAlias_force_thread_id(alias_force_thread_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getAlias_nameDirtyFlag())
            srfdomain.setAlias_name(alias_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Crm_team srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getOpportunities_amountDirtyFlag())
            this.setOpportunities_amount(srfdomain.getOpportunities_amount());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getUse_opportunitiesDirtyFlag())
            this.setUse_opportunities(srfdomain.getUse_opportunities());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getQuotations_countDirtyFlag())
            this.setQuotations_count(srfdomain.getQuotations_count());
        if(srfdomain.getDashboard_graph_typeDirtyFlag())
            this.setDashboard_graph_type(srfdomain.getDashboard_graph_type());
        if(srfdomain.getIs_favoriteDirtyFlag())
            this.setIs_favorite(srfdomain.getIs_favorite());
        if(srfdomain.getOpportunities_countDirtyFlag())
            this.setOpportunities_count(srfdomain.getOpportunities_count());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getUse_quotationsDirtyFlag())
            this.setUse_quotations(srfdomain.getUse_quotations());
        if(srfdomain.getReply_toDirtyFlag())
            this.setReply_to(srfdomain.getReply_to());
        if(srfdomain.getFavorite_user_idsDirtyFlag())
            this.setFavorite_user_ids(srfdomain.getFavorite_user_ids());
        if(srfdomain.getTeam_typeDirtyFlag())
            this.setTeam_type(srfdomain.getTeam_type());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getDashboard_graph_modelDirtyFlag())
            this.setDashboard_graph_model(srfdomain.getDashboard_graph_model());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getDashboard_graph_period_pipelineDirtyFlag())
            this.setDashboard_graph_period_pipeline(srfdomain.getDashboard_graph_period_pipeline());
        if(srfdomain.getAbandoned_carts_countDirtyFlag())
            this.setAbandoned_carts_count(srfdomain.getAbandoned_carts_count());
        if(srfdomain.getPos_config_idsDirtyFlag())
            this.setPos_config_ids(srfdomain.getPos_config_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getUse_leadsDirtyFlag())
            this.setUse_leads(srfdomain.getUse_leads());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getWebsite_idsDirtyFlag())
            this.setWebsite_ids(srfdomain.getWebsite_ids());
        if(srfdomain.getQuotations_amountDirtyFlag())
            this.setQuotations_amount(srfdomain.getQuotations_amount());
        if(srfdomain.getInvoicedDirtyFlag())
            this.setInvoiced(srfdomain.getInvoiced());
        if(srfdomain.getAbandoned_carts_amountDirtyFlag())
            this.setAbandoned_carts_amount(srfdomain.getAbandoned_carts_amount());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getSales_to_invoice_countDirtyFlag())
            this.setSales_to_invoice_count(srfdomain.getSales_to_invoice_count());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getUnassigned_leads_countDirtyFlag())
            this.setUnassigned_leads_count(srfdomain.getUnassigned_leads_count());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getDashboard_graph_group_posDirtyFlag())
            this.setDashboard_graph_group_pos(srfdomain.getDashboard_graph_group_pos());
        if(srfdomain.getMember_idsDirtyFlag())
            this.setMember_ids(srfdomain.getMember_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getInvoiced_targetDirtyFlag())
            this.setInvoiced_target(srfdomain.getInvoiced_target());
        if(srfdomain.getDashboard_graph_groupDirtyFlag())
            this.setDashboard_graph_group(srfdomain.getDashboard_graph_group());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getDashboard_graph_dataDirtyFlag())
            this.setDashboard_graph_data(srfdomain.getDashboard_graph_data());
        if(srfdomain.getDashboard_graph_periodDirtyFlag())
            this.setDashboard_graph_period(srfdomain.getDashboard_graph_period());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getPos_sessions_open_countDirtyFlag())
            this.setPos_sessions_open_count(srfdomain.getPos_sessions_open_count());
        if(srfdomain.getUse_invoicesDirtyFlag())
            this.setUse_invoices(srfdomain.getUse_invoices());
        if(srfdomain.getDashboard_button_nameDirtyFlag())
            this.setDashboard_button_name(srfdomain.getDashboard_button_name());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getDashboard_graph_group_pipelineDirtyFlag())
            this.setDashboard_graph_group_pipeline(srfdomain.getDashboard_graph_group_pipeline());
        if(srfdomain.getPos_order_amount_totalDirtyFlag())
            this.setPos_order_amount_total(srfdomain.getPos_order_amount_total());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getAlias_model_idDirtyFlag())
            this.setAlias_model_id(srfdomain.getAlias_model_id());
        if(srfdomain.getAlias_parent_model_idDirtyFlag())
            this.setAlias_parent_model_id(srfdomain.getAlias_parent_model_id());
        if(srfdomain.getAlias_contactDirtyFlag())
            this.setAlias_contact(srfdomain.getAlias_contact());
        if(srfdomain.getAlias_user_idDirtyFlag())
            this.setAlias_user_id(srfdomain.getAlias_user_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getAlias_defaultsDirtyFlag())
            this.setAlias_defaults(srfdomain.getAlias_defaults());
        if(srfdomain.getAlias_parent_thread_idDirtyFlag())
            this.setAlias_parent_thread_id(srfdomain.getAlias_parent_thread_id());
        if(srfdomain.getAlias_force_thread_idDirtyFlag())
            this.setAlias_force_thread_id(srfdomain.getAlias_force_thread_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getAlias_nameDirtyFlag())
            this.setAlias_name(srfdomain.getAlias_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Crm_teamDTO> fromDOPage(List<Crm_team> poPage)   {
        if(poPage == null)
            return null;
        List<Crm_teamDTO> dtos=new ArrayList<Crm_teamDTO>();
        for(Crm_team domain : poPage) {
            Crm_teamDTO dto = new Crm_teamDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


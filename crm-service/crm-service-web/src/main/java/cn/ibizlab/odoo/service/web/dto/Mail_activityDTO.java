package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_activity.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_activityDTO]
 */
public class Mail_activityDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_activityDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_IDS]
     *
     */
    @Mail_activityMail_template_idsDefault(info = "默认规则")
    private String mail_template_ids;

    @JsonIgnore
    private boolean mail_template_idsDirtyFlag;

    /**
     * 属性 [AUTOMATED]
     *
     */
    @Mail_activityAutomatedDefault(info = "默认规则")
    private String automated;

    @JsonIgnore
    private boolean automatedDirtyFlag;

    /**
     * 属性 [RES_NAME]
     *
     */
    @Mail_activityRes_nameDefault(info = "默认规则")
    private String res_name;

    @JsonIgnore
    private boolean res_nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mail_activityStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_activityIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_activityCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_activityWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @Mail_activityDate_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_activity__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [SUMMARY]
     *
     */
    @Mail_activitySummaryDefault(info = "默认规则")
    private String summary;

    @JsonIgnore
    private boolean summaryDirtyFlag;

    /**
     * 属性 [HAS_RECOMMENDED_ACTIVITIES]
     *
     */
    @Mail_activityHas_recommended_activitiesDefault(info = "默认规则")
    private String has_recommended_activities;

    @JsonIgnore
    private boolean has_recommended_activitiesDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Mail_activityRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Mail_activityNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [FEEDBACK]
     *
     */
    @Mail_activityFeedbackDefault(info = "默认规则")
    private String feedback;

    @JsonIgnore
    private boolean feedbackDirtyFlag;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @Mail_activityRes_model_idDefault(info = "默认规则")
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Mail_activityRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mail_activityUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [ACTIVITY_CATEGORY]
     *
     */
    @Mail_activityActivity_categoryDefault(info = "默认规则")
    private String activity_category;

    @JsonIgnore
    private boolean activity_categoryDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @Mail_activityActivity_type_id_textDefault(info = "默认规则")
    private String activity_type_id_text;

    @JsonIgnore
    private boolean activity_type_id_textDirtyFlag;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @Mail_activityPrevious_activity_type_id_textDefault(info = "默认规则")
    private String previous_activity_type_id_text;

    @JsonIgnore
    private boolean previous_activity_type_id_textDirtyFlag;

    /**
     * 属性 [NOTE_ID_TEXT]
     *
     */
    @Mail_activityNote_id_textDefault(info = "默认规则")
    private String note_id_text;

    @JsonIgnore
    private boolean note_id_textDirtyFlag;

    /**
     * 属性 [CREATE_USER_ID_TEXT]
     *
     */
    @Mail_activityCreate_user_id_textDefault(info = "默认规则")
    private String create_user_id_text;

    @JsonIgnore
    private boolean create_user_id_textDirtyFlag;

    /**
     * 属性 [ICON]
     *
     */
    @Mail_activityIconDefault(info = "默认规则")
    private String icon;

    @JsonIgnore
    private boolean iconDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_activityCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ACTIVITY_DECORATION]
     *
     */
    @Mail_activityActivity_decorationDefault(info = "默认规则")
    private String activity_decoration;

    @JsonIgnore
    private boolean activity_decorationDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_activityWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [FORCE_NEXT]
     *
     */
    @Mail_activityForce_nextDefault(info = "默认规则")
    private String force_next;

    @JsonIgnore
    private boolean force_nextDirtyFlag;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @Mail_activityRecommended_activity_type_id_textDefault(info = "默认规则")
    private String recommended_activity_type_id_text;

    @JsonIgnore
    private boolean recommended_activity_type_id_textDirtyFlag;

    /**
     * 属性 [CALENDAR_EVENT_ID_TEXT]
     *
     */
    @Mail_activityCalendar_event_id_textDefault(info = "默认规则")
    private String calendar_event_id_text;

    @JsonIgnore
    private boolean calendar_event_id_textDirtyFlag;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID]
     *
     */
    @Mail_activityRecommended_activity_type_idDefault(info = "默认规则")
    private Integer recommended_activity_type_id;

    @JsonIgnore
    private boolean recommended_activity_type_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Mail_activityActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [CREATE_USER_ID]
     *
     */
    @Mail_activityCreate_user_idDefault(info = "默认规则")
    private Integer create_user_id;

    @JsonIgnore
    private boolean create_user_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_activityCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mail_activityUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [NOTE_ID]
     *
     */
    @Mail_activityNote_idDefault(info = "默认规则")
    private Integer note_id;

    @JsonIgnore
    private boolean note_idDirtyFlag;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID]
     *
     */
    @Mail_activityPrevious_activity_type_idDefault(info = "默认规则")
    private Integer previous_activity_type_id;

    @JsonIgnore
    private boolean previous_activity_type_idDirtyFlag;

    /**
     * 属性 [CALENDAR_EVENT_ID]
     *
     */
    @Mail_activityCalendar_event_idDefault(info = "默认规则")
    private Integer calendar_event_id;

    @JsonIgnore
    private boolean calendar_event_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_activityWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_IDS]
     */
    @JsonProperty("mail_template_ids")
    public String getMail_template_ids(){
        return mail_template_ids ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_IDS]
     */
    @JsonProperty("mail_template_ids")
    public void setMail_template_ids(String  mail_template_ids){
        this.mail_template_ids = mail_template_ids ;
        this.mail_template_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idsDirtyFlag(){
        return mail_template_idsDirtyFlag ;
    }

    /**
     * 获取 [AUTOMATED]
     */
    @JsonProperty("automated")
    public String getAutomated(){
        return automated ;
    }

    /**
     * 设置 [AUTOMATED]
     */
    @JsonProperty("automated")
    public void setAutomated(String  automated){
        this.automated = automated ;
        this.automatedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTOMATED]脏标记
     */
    @JsonIgnore
    public boolean getAutomatedDirtyFlag(){
        return automatedDirtyFlag ;
    }

    /**
     * 获取 [RES_NAME]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return res_name ;
    }

    /**
     * 设置 [RES_NAME]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

    /**
     * 获取 [RES_NAME]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return res_nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return date_deadline ;
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [SUMMARY]
     */
    @JsonProperty("summary")
    public String getSummary(){
        return summary ;
    }

    /**
     * 设置 [SUMMARY]
     */
    @JsonProperty("summary")
    public void setSummary(String  summary){
        this.summary = summary ;
        this.summaryDirtyFlag = true ;
    }

    /**
     * 获取 [SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getSummaryDirtyFlag(){
        return summaryDirtyFlag ;
    }

    /**
     * 获取 [HAS_RECOMMENDED_ACTIVITIES]
     */
    @JsonProperty("has_recommended_activities")
    public String getHas_recommended_activities(){
        return has_recommended_activities ;
    }

    /**
     * 设置 [HAS_RECOMMENDED_ACTIVITIES]
     */
    @JsonProperty("has_recommended_activities")
    public void setHas_recommended_activities(String  has_recommended_activities){
        this.has_recommended_activities = has_recommended_activities ;
        this.has_recommended_activitiesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_RECOMMENDED_ACTIVITIES]脏标记
     */
    @JsonIgnore
    public boolean getHas_recommended_activitiesDirtyFlag(){
        return has_recommended_activitiesDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [FEEDBACK]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return feedback ;
    }

    /**
     * 设置 [FEEDBACK]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return feedbackDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return res_model_id ;
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return res_model_idDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_CATEGORY]
     */
    @JsonProperty("activity_category")
    public String getActivity_category(){
        return activity_category ;
    }

    /**
     * 设置 [ACTIVITY_CATEGORY]
     */
    @JsonProperty("activity_category")
    public void setActivity_category(String  activity_category){
        this.activity_category = activity_category ;
        this.activity_categoryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_CATEGORY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_categoryDirtyFlag(){
        return activity_categoryDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("activity_type_id_text")
    public String getActivity_type_id_text(){
        return activity_type_id_text ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("activity_type_id_text")
    public void setActivity_type_id_text(String  activity_type_id_text){
        this.activity_type_id_text = activity_type_id_text ;
        this.activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_id_textDirtyFlag(){
        return activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("previous_activity_type_id_text")
    public String getPrevious_activity_type_id_text(){
        return previous_activity_type_id_text ;
    }

    /**
     * 设置 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("previous_activity_type_id_text")
    public void setPrevious_activity_type_id_text(String  previous_activity_type_id_text){
        this.previous_activity_type_id_text = previous_activity_type_id_text ;
        this.previous_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_id_textDirtyFlag(){
        return previous_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [NOTE_ID_TEXT]
     */
    @JsonProperty("note_id_text")
    public String getNote_id_text(){
        return note_id_text ;
    }

    /**
     * 设置 [NOTE_ID_TEXT]
     */
    @JsonProperty("note_id_text")
    public void setNote_id_text(String  note_id_text){
        this.note_id_text = note_id_text ;
        this.note_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getNote_id_textDirtyFlag(){
        return note_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_USER_ID_TEXT]
     */
    @JsonProperty("create_user_id_text")
    public String getCreate_user_id_text(){
        return create_user_id_text ;
    }

    /**
     * 设置 [CREATE_USER_ID_TEXT]
     */
    @JsonProperty("create_user_id_text")
    public void setCreate_user_id_text(String  create_user_id_text){
        this.create_user_id_text = create_user_id_text ;
        this.create_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_id_textDirtyFlag(){
        return create_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [ICON]
     */
    @JsonProperty("icon")
    public String getIcon(){
        return icon ;
    }

    /**
     * 设置 [ICON]
     */
    @JsonProperty("icon")
    public void setIcon(String  icon){
        this.icon = icon ;
        this.iconDirtyFlag = true ;
    }

    /**
     * 获取 [ICON]脏标记
     */
    @JsonIgnore
    public boolean getIconDirtyFlag(){
        return iconDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DECORATION]
     */
    @JsonProperty("activity_decoration")
    public String getActivity_decoration(){
        return activity_decoration ;
    }

    /**
     * 设置 [ACTIVITY_DECORATION]
     */
    @JsonProperty("activity_decoration")
    public void setActivity_decoration(String  activity_decoration){
        this.activity_decoration = activity_decoration ;
        this.activity_decorationDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DECORATION]脏标记
     */
    @JsonIgnore
    public boolean getActivity_decorationDirtyFlag(){
        return activity_decorationDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [FORCE_NEXT]
     */
    @JsonProperty("force_next")
    public String getForce_next(){
        return force_next ;
    }

    /**
     * 设置 [FORCE_NEXT]
     */
    @JsonProperty("force_next")
    public void setForce_next(String  force_next){
        this.force_next = force_next ;
        this.force_nextDirtyFlag = true ;
    }

    /**
     * 获取 [FORCE_NEXT]脏标记
     */
    @JsonIgnore
    public boolean getForce_nextDirtyFlag(){
        return force_nextDirtyFlag ;
    }

    /**
     * 获取 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public String getRecommended_activity_type_id_text(){
        return recommended_activity_type_id_text ;
    }

    /**
     * 设置 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("recommended_activity_type_id_text")
    public void setRecommended_activity_type_id_text(String  recommended_activity_type_id_text){
        this.recommended_activity_type_id_text = recommended_activity_type_id_text ;
        this.recommended_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_id_textDirtyFlag(){
        return recommended_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_EVENT_ID_TEXT]
     */
    @JsonProperty("calendar_event_id_text")
    public String getCalendar_event_id_text(){
        return calendar_event_id_text ;
    }

    /**
     * 设置 [CALENDAR_EVENT_ID_TEXT]
     */
    @JsonProperty("calendar_event_id_text")
    public void setCalendar_event_id_text(String  calendar_event_id_text){
        this.calendar_event_id_text = calendar_event_id_text ;
        this.calendar_event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_EVENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_id_textDirtyFlag(){
        return calendar_event_id_textDirtyFlag ;
    }

    /**
     * 获取 [RECOMMENDED_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("recommended_activity_type_id")
    public Integer getRecommended_activity_type_id(){
        return recommended_activity_type_id ;
    }

    /**
     * 设置 [RECOMMENDED_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("recommended_activity_type_id")
    public void setRecommended_activity_type_id(Integer  recommended_activity_type_id){
        this.recommended_activity_type_id = recommended_activity_type_id ;
        this.recommended_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [RECOMMENDED_ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRecommended_activity_type_idDirtyFlag(){
        return recommended_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_USER_ID]
     */
    @JsonProperty("create_user_id")
    public Integer getCreate_user_id(){
        return create_user_id ;
    }

    /**
     * 设置 [CREATE_USER_ID]
     */
    @JsonProperty("create_user_id")
    public void setCreate_user_id(Integer  create_user_id){
        this.create_user_id = create_user_id ;
        this.create_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_user_idDirtyFlag(){
        return create_user_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [NOTE_ID]
     */
    @JsonProperty("note_id")
    public Integer getNote_id(){
        return note_id ;
    }

    /**
     * 设置 [NOTE_ID]
     */
    @JsonProperty("note_id")
    public void setNote_id(Integer  note_id){
        this.note_id = note_id ;
        this.note_idDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getNote_idDirtyFlag(){
        return note_idDirtyFlag ;
    }

    /**
     * 获取 [PREVIOUS_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("previous_activity_type_id")
    public Integer getPrevious_activity_type_id(){
        return previous_activity_type_id ;
    }

    /**
     * 设置 [PREVIOUS_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("previous_activity_type_id")
    public void setPrevious_activity_type_id(Integer  previous_activity_type_id){
        this.previous_activity_type_id = previous_activity_type_id ;
        this.previous_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PREVIOUS_ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_activity_type_idDirtyFlag(){
        return previous_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_EVENT_ID]
     */
    @JsonProperty("calendar_event_id")
    public Integer getCalendar_event_id(){
        return calendar_event_id ;
    }

    /**
     * 设置 [CALENDAR_EVENT_ID]
     */
    @JsonProperty("calendar_event_id")
    public void setCalendar_event_id(Integer  calendar_event_id){
        this.calendar_event_id = calendar_event_id ;
        this.calendar_event_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_EVENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_event_idDirtyFlag(){
        return calendar_event_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mail_activity toDO() {
        Mail_activity srfdomain = new Mail_activity();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMail_template_idsDirtyFlag())
            srfdomain.setMail_template_ids(mail_template_ids);
        if(getAutomatedDirtyFlag())
            srfdomain.setAutomated(automated);
        if(getRes_nameDirtyFlag())
            srfdomain.setRes_name(res_name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDate_deadlineDirtyFlag())
            srfdomain.setDate_deadline(date_deadline);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getSummaryDirtyFlag())
            srfdomain.setSummary(summary);
        if(getHas_recommended_activitiesDirtyFlag())
            srfdomain.setHas_recommended_activities(has_recommended_activities);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getFeedbackDirtyFlag())
            srfdomain.setFeedback(feedback);
        if(getRes_model_idDirtyFlag())
            srfdomain.setRes_model_id(res_model_id);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getActivity_categoryDirtyFlag())
            srfdomain.setActivity_category(activity_category);
        if(getActivity_type_id_textDirtyFlag())
            srfdomain.setActivity_type_id_text(activity_type_id_text);
        if(getPrevious_activity_type_id_textDirtyFlag())
            srfdomain.setPrevious_activity_type_id_text(previous_activity_type_id_text);
        if(getNote_id_textDirtyFlag())
            srfdomain.setNote_id_text(note_id_text);
        if(getCreate_user_id_textDirtyFlag())
            srfdomain.setCreate_user_id_text(create_user_id_text);
        if(getIconDirtyFlag())
            srfdomain.setIcon(icon);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getActivity_decorationDirtyFlag())
            srfdomain.setActivity_decoration(activity_decoration);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getForce_nextDirtyFlag())
            srfdomain.setForce_next(force_next);
        if(getRecommended_activity_type_id_textDirtyFlag())
            srfdomain.setRecommended_activity_type_id_text(recommended_activity_type_id_text);
        if(getCalendar_event_id_textDirtyFlag())
            srfdomain.setCalendar_event_id_text(calendar_event_id_text);
        if(getRecommended_activity_type_idDirtyFlag())
            srfdomain.setRecommended_activity_type_id(recommended_activity_type_id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getCreate_user_idDirtyFlag())
            srfdomain.setCreate_user_id(create_user_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getNote_idDirtyFlag())
            srfdomain.setNote_id(note_id);
        if(getPrevious_activity_type_idDirtyFlag())
            srfdomain.setPrevious_activity_type_id(previous_activity_type_id);
        if(getCalendar_event_idDirtyFlag())
            srfdomain.setCalendar_event_id(calendar_event_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mail_activity srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMail_template_idsDirtyFlag())
            this.setMail_template_ids(srfdomain.getMail_template_ids());
        if(srfdomain.getAutomatedDirtyFlag())
            this.setAutomated(srfdomain.getAutomated());
        if(srfdomain.getRes_nameDirtyFlag())
            this.setRes_name(srfdomain.getRes_name());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDate_deadlineDirtyFlag())
            this.setDate_deadline(srfdomain.getDate_deadline());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getSummaryDirtyFlag())
            this.setSummary(srfdomain.getSummary());
        if(srfdomain.getHas_recommended_activitiesDirtyFlag())
            this.setHas_recommended_activities(srfdomain.getHas_recommended_activities());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getFeedbackDirtyFlag())
            this.setFeedback(srfdomain.getFeedback());
        if(srfdomain.getRes_model_idDirtyFlag())
            this.setRes_model_id(srfdomain.getRes_model_id());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getActivity_categoryDirtyFlag())
            this.setActivity_category(srfdomain.getActivity_category());
        if(srfdomain.getActivity_type_id_textDirtyFlag())
            this.setActivity_type_id_text(srfdomain.getActivity_type_id_text());
        if(srfdomain.getPrevious_activity_type_id_textDirtyFlag())
            this.setPrevious_activity_type_id_text(srfdomain.getPrevious_activity_type_id_text());
        if(srfdomain.getNote_id_textDirtyFlag())
            this.setNote_id_text(srfdomain.getNote_id_text());
        if(srfdomain.getCreate_user_id_textDirtyFlag())
            this.setCreate_user_id_text(srfdomain.getCreate_user_id_text());
        if(srfdomain.getIconDirtyFlag())
            this.setIcon(srfdomain.getIcon());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getActivity_decorationDirtyFlag())
            this.setActivity_decoration(srfdomain.getActivity_decoration());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getForce_nextDirtyFlag())
            this.setForce_next(srfdomain.getForce_next());
        if(srfdomain.getRecommended_activity_type_id_textDirtyFlag())
            this.setRecommended_activity_type_id_text(srfdomain.getRecommended_activity_type_id_text());
        if(srfdomain.getCalendar_event_id_textDirtyFlag())
            this.setCalendar_event_id_text(srfdomain.getCalendar_event_id_text());
        if(srfdomain.getRecommended_activity_type_idDirtyFlag())
            this.setRecommended_activity_type_id(srfdomain.getRecommended_activity_type_id());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getCreate_user_idDirtyFlag())
            this.setCreate_user_id(srfdomain.getCreate_user_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getNote_idDirtyFlag())
            this.setNote_id(srfdomain.getNote_id());
        if(srfdomain.getPrevious_activity_type_idDirtyFlag())
            this.setPrevious_activity_type_id(srfdomain.getPrevious_activity_type_id());
        if(srfdomain.getCalendar_event_idDirtyFlag())
            this.setCalendar_event_id(srfdomain.getCalendar_event_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mail_activityDTO> fromDOPage(List<Mail_activity> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_activityDTO> dtos=new ArrayList<Mail_activityDTO>();
        for(Mail_activity domain : poPage) {
            Mail_activityDTO dto = new Mail_activityDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_pricelist_item.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_pricelist_itemDTO]
 */
public class Product_pricelist_itemDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_pricelist_itemDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [FIXED_PRICE]
     *
     */
    @Product_pricelist_itemFixed_priceDefault(info = "默认规则")
    private Double fixed_price;

    @JsonIgnore
    private boolean fixed_priceDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_pricelist_itemNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRICE]
     *
     */
    @Product_pricelist_itemPriceDefault(info = "默认规则")
    private String price;

    @JsonIgnore
    private boolean priceDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_pricelist_item__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [BASE]
     *
     */
    @Product_pricelist_itemBaseDefault(info = "默认规则")
    private String base;

    @JsonIgnore
    private boolean baseDirtyFlag;

    /**
     * 属性 [DATE_END]
     *
     */
    @Product_pricelist_itemDate_endDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_end;

    @JsonIgnore
    private boolean date_endDirtyFlag;

    /**
     * 属性 [PERCENT_PRICE]
     *
     */
    @Product_pricelist_itemPercent_priceDefault(info = "默认规则")
    private Double percent_price;

    @JsonIgnore
    private boolean percent_priceDirtyFlag;

    /**
     * 属性 [PRICE_ROUND]
     *
     */
    @Product_pricelist_itemPrice_roundDefault(info = "默认规则")
    private Double price_round;

    @JsonIgnore
    private boolean price_roundDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_pricelist_itemWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_pricelist_itemIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_pricelist_itemCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [COMPUTE_PRICE]
     *
     */
    @Product_pricelist_itemCompute_priceDefault(info = "默认规则")
    private String compute_price;

    @JsonIgnore
    private boolean compute_priceDirtyFlag;

    /**
     * 属性 [PRICE_MIN_MARGIN]
     *
     */
    @Product_pricelist_itemPrice_min_marginDefault(info = "默认规则")
    private Double price_min_margin;

    @JsonIgnore
    private boolean price_min_marginDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Product_pricelist_itemDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [PRICE_MAX_MARGIN]
     *
     */
    @Product_pricelist_itemPrice_max_marginDefault(info = "默认规则")
    private Double price_max_margin;

    @JsonIgnore
    private boolean price_max_marginDirtyFlag;

    /**
     * 属性 [APPLIED_ON]
     *
     */
    @Product_pricelist_itemApplied_onDefault(info = "默认规则")
    private String applied_on;

    @JsonIgnore
    private boolean applied_onDirtyFlag;

    /**
     * 属性 [MIN_QUANTITY]
     *
     */
    @Product_pricelist_itemMin_quantityDefault(info = "默认规则")
    private Integer min_quantity;

    @JsonIgnore
    private boolean min_quantityDirtyFlag;

    /**
     * 属性 [PRICE_SURCHARGE]
     *
     */
    @Product_pricelist_itemPrice_surchargeDefault(info = "默认规则")
    private Double price_surcharge;

    @JsonIgnore
    private boolean price_surchargeDirtyFlag;

    /**
     * 属性 [PRICE_DISCOUNT]
     *
     */
    @Product_pricelist_itemPrice_discountDefault(info = "默认规则")
    private Double price_discount;

    @JsonIgnore
    private boolean price_discountDirtyFlag;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @Product_pricelist_itemPricelist_id_textDefault(info = "默认规则")
    private String pricelist_id_text;

    @JsonIgnore
    private boolean pricelist_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Product_pricelist_itemCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @Product_pricelist_itemCateg_id_textDefault(info = "默认规则")
    private String categ_id_text;

    @JsonIgnore
    private boolean categ_id_textDirtyFlag;

    /**
     * 属性 [BASE_PRICELIST_ID_TEXT]
     *
     */
    @Product_pricelist_itemBase_pricelist_id_textDefault(info = "默认规则")
    private String base_pricelist_id_text;

    @JsonIgnore
    private boolean base_pricelist_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_pricelist_itemWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Product_pricelist_itemProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_pricelist_itemCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Product_pricelist_itemCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Product_pricelist_itemProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Product_pricelist_itemProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @Product_pricelist_itemPricelist_idDefault(info = "默认规则")
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_pricelist_itemWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Product_pricelist_itemCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [BASE_PRICELIST_ID]
     *
     */
    @Product_pricelist_itemBase_pricelist_idDefault(info = "默认规则")
    private Integer base_pricelist_id;

    @JsonIgnore
    private boolean base_pricelist_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Product_pricelist_itemCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @Product_pricelist_itemCateg_idDefault(info = "默认规则")
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_pricelist_itemCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Product_pricelist_itemProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [FIXED_PRICE]
     */
    @JsonProperty("fixed_price")
    public Double getFixed_price(){
        return fixed_price ;
    }

    /**
     * 设置 [FIXED_PRICE]
     */
    @JsonProperty("fixed_price")
    public void setFixed_price(Double  fixed_price){
        this.fixed_price = fixed_price ;
        this.fixed_priceDirtyFlag = true ;
    }

    /**
     * 获取 [FIXED_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getFixed_priceDirtyFlag(){
        return fixed_priceDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE]
     */
    @JsonProperty("price")
    public String getPrice(){
        return price ;
    }

    /**
     * 设置 [PRICE]
     */
    @JsonProperty("price")
    public void setPrice(String  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return priceDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [BASE]
     */
    @JsonProperty("base")
    public String getBase(){
        return base ;
    }

    /**
     * 设置 [BASE]
     */
    @JsonProperty("base")
    public void setBase(String  base){
        this.base = base ;
        this.baseDirtyFlag = true ;
    }

    /**
     * 获取 [BASE]脏标记
     */
    @JsonIgnore
    public boolean getBaseDirtyFlag(){
        return baseDirtyFlag ;
    }

    /**
     * 获取 [DATE_END]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return date_end ;
    }

    /**
     * 设置 [DATE_END]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return date_endDirtyFlag ;
    }

    /**
     * 获取 [PERCENT_PRICE]
     */
    @JsonProperty("percent_price")
    public Double getPercent_price(){
        return percent_price ;
    }

    /**
     * 设置 [PERCENT_PRICE]
     */
    @JsonProperty("percent_price")
    public void setPercent_price(Double  percent_price){
        this.percent_price = percent_price ;
        this.percent_priceDirtyFlag = true ;
    }

    /**
     * 获取 [PERCENT_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getPercent_priceDirtyFlag(){
        return percent_priceDirtyFlag ;
    }

    /**
     * 获取 [PRICE_ROUND]
     */
    @JsonProperty("price_round")
    public Double getPrice_round(){
        return price_round ;
    }

    /**
     * 设置 [PRICE_ROUND]
     */
    @JsonProperty("price_round")
    public void setPrice_round(Double  price_round){
        this.price_round = price_round ;
        this.price_roundDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_ROUND]脏标记
     */
    @JsonIgnore
    public boolean getPrice_roundDirtyFlag(){
        return price_roundDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [COMPUTE_PRICE]
     */
    @JsonProperty("compute_price")
    public String getCompute_price(){
        return compute_price ;
    }

    /**
     * 设置 [COMPUTE_PRICE]
     */
    @JsonProperty("compute_price")
    public void setCompute_price(String  compute_price){
        this.compute_price = compute_price ;
        this.compute_priceDirtyFlag = true ;
    }

    /**
     * 获取 [COMPUTE_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getCompute_priceDirtyFlag(){
        return compute_priceDirtyFlag ;
    }

    /**
     * 获取 [PRICE_MIN_MARGIN]
     */
    @JsonProperty("price_min_margin")
    public Double getPrice_min_margin(){
        return price_min_margin ;
    }

    /**
     * 设置 [PRICE_MIN_MARGIN]
     */
    @JsonProperty("price_min_margin")
    public void setPrice_min_margin(Double  price_min_margin){
        this.price_min_margin = price_min_margin ;
        this.price_min_marginDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_MIN_MARGIN]脏标记
     */
    @JsonIgnore
    public boolean getPrice_min_marginDirtyFlag(){
        return price_min_marginDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [PRICE_MAX_MARGIN]
     */
    @JsonProperty("price_max_margin")
    public Double getPrice_max_margin(){
        return price_max_margin ;
    }

    /**
     * 设置 [PRICE_MAX_MARGIN]
     */
    @JsonProperty("price_max_margin")
    public void setPrice_max_margin(Double  price_max_margin){
        this.price_max_margin = price_max_margin ;
        this.price_max_marginDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_MAX_MARGIN]脏标记
     */
    @JsonIgnore
    public boolean getPrice_max_marginDirtyFlag(){
        return price_max_marginDirtyFlag ;
    }

    /**
     * 获取 [APPLIED_ON]
     */
    @JsonProperty("applied_on")
    public String getApplied_on(){
        return applied_on ;
    }

    /**
     * 设置 [APPLIED_ON]
     */
    @JsonProperty("applied_on")
    public void setApplied_on(String  applied_on){
        this.applied_on = applied_on ;
        this.applied_onDirtyFlag = true ;
    }

    /**
     * 获取 [APPLIED_ON]脏标记
     */
    @JsonIgnore
    public boolean getApplied_onDirtyFlag(){
        return applied_onDirtyFlag ;
    }

    /**
     * 获取 [MIN_QUANTITY]
     */
    @JsonProperty("min_quantity")
    public Integer getMin_quantity(){
        return min_quantity ;
    }

    /**
     * 设置 [MIN_QUANTITY]
     */
    @JsonProperty("min_quantity")
    public void setMin_quantity(Integer  min_quantity){
        this.min_quantity = min_quantity ;
        this.min_quantityDirtyFlag = true ;
    }

    /**
     * 获取 [MIN_QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getMin_quantityDirtyFlag(){
        return min_quantityDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SURCHARGE]
     */
    @JsonProperty("price_surcharge")
    public Double getPrice_surcharge(){
        return price_surcharge ;
    }

    /**
     * 设置 [PRICE_SURCHARGE]
     */
    @JsonProperty("price_surcharge")
    public void setPrice_surcharge(Double  price_surcharge){
        this.price_surcharge = price_surcharge ;
        this.price_surchargeDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SURCHARGE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_surchargeDirtyFlag(){
        return price_surchargeDirtyFlag ;
    }

    /**
     * 获取 [PRICE_DISCOUNT]
     */
    @JsonProperty("price_discount")
    public Double getPrice_discount(){
        return price_discount ;
    }

    /**
     * 设置 [PRICE_DISCOUNT]
     */
    @JsonProperty("price_discount")
    public void setPrice_discount(Double  price_discount){
        this.price_discount = price_discount ;
        this.price_discountDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_DISCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_discountDirtyFlag(){
        return price_discountDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return pricelist_id_text ;
    }

    /**
     * 设置 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return categ_id_text ;
    }

    /**
     * 设置 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [BASE_PRICELIST_ID_TEXT]
     */
    @JsonProperty("base_pricelist_id_text")
    public String getBase_pricelist_id_text(){
        return base_pricelist_id_text ;
    }

    /**
     * 设置 [BASE_PRICELIST_ID_TEXT]
     */
    @JsonProperty("base_pricelist_id_text")
    public void setBase_pricelist_id_text(String  base_pricelist_id_text){
        this.base_pricelist_id_text = base_pricelist_id_text ;
        this.base_pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [BASE_PRICELIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_id_textDirtyFlag(){
        return base_pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return pricelist_id ;
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [BASE_PRICELIST_ID]
     */
    @JsonProperty("base_pricelist_id")
    public Integer getBase_pricelist_id(){
        return base_pricelist_id ;
    }

    /**
     * 设置 [BASE_PRICELIST_ID]
     */
    @JsonProperty("base_pricelist_id")
    public void setBase_pricelist_id(Integer  base_pricelist_id){
        this.base_pricelist_id = base_pricelist_id ;
        this.base_pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [BASE_PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_idDirtyFlag(){
        return base_pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return categ_id ;
    }

    /**
     * 设置 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return categ_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }



    public Product_pricelist_item toDO() {
        Product_pricelist_item srfdomain = new Product_pricelist_item();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getFixed_priceDirtyFlag())
            srfdomain.setFixed_price(fixed_price);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPriceDirtyFlag())
            srfdomain.setPrice(price);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getBaseDirtyFlag())
            srfdomain.setBase(base);
        if(getDate_endDirtyFlag())
            srfdomain.setDate_end(date_end);
        if(getPercent_priceDirtyFlag())
            srfdomain.setPercent_price(percent_price);
        if(getPrice_roundDirtyFlag())
            srfdomain.setPrice_round(price_round);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCompute_priceDirtyFlag())
            srfdomain.setCompute_price(compute_price);
        if(getPrice_min_marginDirtyFlag())
            srfdomain.setPrice_min_margin(price_min_margin);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(getPrice_max_marginDirtyFlag())
            srfdomain.setPrice_max_margin(price_max_margin);
        if(getApplied_onDirtyFlag())
            srfdomain.setApplied_on(applied_on);
        if(getMin_quantityDirtyFlag())
            srfdomain.setMin_quantity(min_quantity);
        if(getPrice_surchargeDirtyFlag())
            srfdomain.setPrice_surcharge(price_surcharge);
        if(getPrice_discountDirtyFlag())
            srfdomain.setPrice_discount(price_discount);
        if(getPricelist_id_textDirtyFlag())
            srfdomain.setPricelist_id_text(pricelist_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getCateg_id_textDirtyFlag())
            srfdomain.setCateg_id_text(categ_id_text);
        if(getBase_pricelist_id_textDirtyFlag())
            srfdomain.setBase_pricelist_id_text(base_pricelist_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getPricelist_idDirtyFlag())
            srfdomain.setPricelist_id(pricelist_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getBase_pricelist_idDirtyFlag())
            srfdomain.setBase_pricelist_id(base_pricelist_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCateg_idDirtyFlag())
            srfdomain.setCateg_id(categ_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);

        return srfdomain;
    }

    public void fromDO(Product_pricelist_item srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getFixed_priceDirtyFlag())
            this.setFixed_price(srfdomain.getFixed_price());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPriceDirtyFlag())
            this.setPrice(srfdomain.getPrice());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getBaseDirtyFlag())
            this.setBase(srfdomain.getBase());
        if(srfdomain.getDate_endDirtyFlag())
            this.setDate_end(srfdomain.getDate_end());
        if(srfdomain.getPercent_priceDirtyFlag())
            this.setPercent_price(srfdomain.getPercent_price());
        if(srfdomain.getPrice_roundDirtyFlag())
            this.setPrice_round(srfdomain.getPrice_round());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCompute_priceDirtyFlag())
            this.setCompute_price(srfdomain.getCompute_price());
        if(srfdomain.getPrice_min_marginDirtyFlag())
            this.setPrice_min_margin(srfdomain.getPrice_min_margin());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.getPrice_max_marginDirtyFlag())
            this.setPrice_max_margin(srfdomain.getPrice_max_margin());
        if(srfdomain.getApplied_onDirtyFlag())
            this.setApplied_on(srfdomain.getApplied_on());
        if(srfdomain.getMin_quantityDirtyFlag())
            this.setMin_quantity(srfdomain.getMin_quantity());
        if(srfdomain.getPrice_surchargeDirtyFlag())
            this.setPrice_surcharge(srfdomain.getPrice_surcharge());
        if(srfdomain.getPrice_discountDirtyFlag())
            this.setPrice_discount(srfdomain.getPrice_discount());
        if(srfdomain.getPricelist_id_textDirtyFlag())
            this.setPricelist_id_text(srfdomain.getPricelist_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getCateg_id_textDirtyFlag())
            this.setCateg_id_text(srfdomain.getCateg_id_text());
        if(srfdomain.getBase_pricelist_id_textDirtyFlag())
            this.setBase_pricelist_id_text(srfdomain.getBase_pricelist_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getPricelist_idDirtyFlag())
            this.setPricelist_id(srfdomain.getPricelist_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getBase_pricelist_idDirtyFlag())
            this.setBase_pricelist_id(srfdomain.getBase_pricelist_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCateg_idDirtyFlag())
            this.setCateg_id(srfdomain.getCateg_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());

    }

    public List<Product_pricelist_itemDTO> fromDOPage(List<Product_pricelist_item> poPage)   {
        if(poPage == null)
            return null;
        List<Product_pricelist_itemDTO> dtos=new ArrayList<Product_pricelist_itemDTO>();
        for(Product_pricelist_item domain : poPage) {
            Product_pricelist_itemDTO dto = new Product_pricelist_itemDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


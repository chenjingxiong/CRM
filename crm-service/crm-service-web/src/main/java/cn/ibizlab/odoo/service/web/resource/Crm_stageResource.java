package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_stageDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_stage" })
@RestController
@RequestMapping("")
public class Crm_stageResource {

    @Autowired
    private ICrm_stageService crm_stageService;

    public ICrm_stageService getCrm_stageService() {
        return this.crm_stageService;
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_stages")

    public ResponseEntity<Crm_stageDTO> create(@RequestBody Crm_stageDTO crm_stagedto) {
        Crm_stageDTO dto = new Crm_stageDTO();
        Crm_stage domain = crm_stagedto.toDO();
		crm_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_stage" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_stages/{crm_stage_id}/getdraft")

    public ResponseEntity<Crm_stageDTO> getDraft(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stageDTO crm_stagedto) {
        Crm_stage crm_stage = crm_stagedto.toDO();
    	crm_stage = crm_stageService.getDraft(crm_stage) ;
    	crm_stagedto.fromDO(crm_stage);
        return ResponseEntity.status(HttpStatus.OK).body(crm_stagedto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_stages/{crm_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_stage_id") Integer crm_stage_id) {
        Crm_stageDTO crm_stagedto = new Crm_stageDTO();
		Crm_stage domain = new Crm_stage();
		crm_stagedto.setId(crm_stage_id);
		domain.setId(crm_stage_id);
        Boolean rst = crm_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_stages/{crm_stage_id}")

    public ResponseEntity<Crm_stageDTO> update(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stageDTO crm_stagedto) {
		Crm_stage domain = crm_stagedto.toDO();
        domain.setId(crm_stage_id);
		crm_stageService.update(domain);
		Crm_stageDTO dto = new Crm_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_stages/{crm_stage_id}")
    public ResponseEntity<Crm_stageDTO> get(@PathVariable("crm_stage_id") Integer crm_stage_id) {
        Crm_stageDTO dto = new Crm_stageDTO();
        Crm_stage domain = crm_stageService.get(crm_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_stages/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_stage(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_stages/fetchdefault")
	public ResponseEntity<Page<Crm_stageDTO>> fetchDefault(Crm_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_stageDTO> list = new ArrayList<Crm_stageDTO>();
        
        Page<Crm_stage> domains = crm_stageService.searchDefault(context) ;
        for(Crm_stage crm_stage : domains.getContent()){
            Crm_stageDTO dto = new Crm_stageDTO();
            dto.fromDO(crm_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

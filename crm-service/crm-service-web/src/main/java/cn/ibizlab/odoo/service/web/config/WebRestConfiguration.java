package cn.ibizlab.odoo.service.web.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.web")
public class WebRestConfiguration {

}

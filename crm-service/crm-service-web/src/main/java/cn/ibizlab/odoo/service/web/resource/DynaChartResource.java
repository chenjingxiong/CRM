package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.DynaChartDTO;
import cn.ibizlab.odoo.core.r7rt_dyna.domain.DynaChart;
import cn.ibizlab.odoo.core.r7rt_dyna.service.IDynaChartService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.r7rt_dyna.filter.DynaChartSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"DynaChart" })
@RestController
@RequestMapping("")
public class DynaChartResource {

    @Autowired
    private IDynaChartService dynachartService;

    public IDynaChartService getDynachartService() {
        return this.dynachartService;
    }

    @ApiOperation(value = "Get", tags = {"DynaChart" },  notes = "Get")
	@RequestMapping(method = RequestMethod.GET, value = "/web/dynacharts/{dynachart_id}")
    public ResponseEntity<DynaChartDTO> get(@PathVariable("dynachart_id") String dynachart_id) {
        DynaChartDTO dto = new DynaChartDTO();
        DynaChart domain = dynachartService.get(dynachart_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "GetDraft", tags = {"DynaChart" },  notes = "GetDraft")
	@RequestMapping(method = RequestMethod.GET, value = "/web/dynacharts/{dynachart_id}/getdraft")

    public ResponseEntity<DynaChartDTO> getDraft(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChartDTO dynachartdto) {
        DynaChart dynachart = dynachartdto.toDO();
    	dynachart = dynachartService.getDraft(dynachart) ;
    	dynachartdto.fromDO(dynachart);
        return ResponseEntity.status(HttpStatus.OK).body(dynachartdto);
    }

    @ApiOperation(value = "CheckKey", tags = {"DynaChart" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody DynaChartDTO dynachartdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Update", tags = {"DynaChart" },  notes = "Update")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/dynacharts/{dynachart_id}")

    public ResponseEntity<DynaChartDTO> update(@PathVariable("dynachart_id") String dynachart_id, @RequestBody DynaChartDTO dynachartdto) {
		DynaChart domain = dynachartdto.toDO();
        domain.setDynaChartId(dynachart_id);
		dynachartService.update(domain);
		DynaChartDTO dto = new DynaChartDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Remove", tags = {"DynaChart" },  notes = "Remove")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/dynacharts/{dynachart_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("dynachart_id") String dynachart_id) {
        DynaChartDTO dynachartdto = new DynaChartDTO();
		DynaChart domain = new DynaChart();
		dynachartdto.setDynaChartId(dynachart_id);
		domain.setDynaChartId(dynachart_id);
        Boolean rst = dynachartService.remove(domain.getDynaChartId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "Create", tags = {"DynaChart" },  notes = "Create")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts")

    public ResponseEntity<DynaChartDTO> create(@RequestBody DynaChartDTO dynachartdto) {
        DynaChartDTO dto = new DynaChartDTO();
        DynaChart domain = dynachartdto.toDO();
		dynachartService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"DynaChart" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynacharts/{dynachart_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody DynaChartDTO dynachartdto) {
        DynaChart dynachart = dynachartdto.toDO();
    	Boolean b = dynachartService.save(dynachart) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@ApiOperation(value = "获取DEFAULT", tags = {"DynaChart" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/web/dynacharts/fetchdefault")
	public ResponseEntity<Page<DynaChartDTO>> fetchDefault(DynaChartSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<DynaChartDTO> list = new ArrayList<DynaChartDTO>();
        
        Page<DynaChart> domains = dynachartService.searchDefault(context) ;
        for(DynaChart dynachart : domains.getContent()){
            DynaChartDTO dto = new DynaChartDTO();
            dto.fromDO(dynachart);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_uom.valuerule.anno.uom_uom.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Uom_uomDTO]
 */
public class Uom_uomDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Uom_uomNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Uom_uomWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [FACTOR_INV]
     *
     */
    @Uom_uomFactor_invDefault(info = "默认规则")
    private Double factor_inv;

    @JsonIgnore
    private boolean factor_invDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Uom_uomActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Uom_uom__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ROUNDING]
     *
     */
    @Uom_uomRoundingDefault(info = "默认规则")
    private Double rounding;

    @JsonIgnore
    private boolean roundingDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Uom_uomDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [FACTOR]
     *
     */
    @Uom_uomFactorDefault(info = "默认规则")
    private Double factor;

    @JsonIgnore
    private boolean factorDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Uom_uomIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Uom_uomCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [UOM_TYPE]
     *
     */
    @Uom_uomUom_typeDefault(info = "默认规则")
    private String uom_type;

    @JsonIgnore
    private boolean uom_typeDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Uom_uomWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Uom_uomCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [IS_POS_GROUPABLE]
     *
     */
    @Uom_uomIs_pos_groupableDefault(info = "默认规则")
    private String is_pos_groupable;

    @JsonIgnore
    private boolean is_pos_groupableDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Uom_uomCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MEASURE_TYPE]
     *
     */
    @Uom_uomMeasure_typeDefault(info = "默认规则")
    private String measure_type;

    @JsonIgnore
    private boolean measure_typeDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Uom_uomCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Uom_uomCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Uom_uomWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [FACTOR_INV]
     */
    @JsonProperty("factor_inv")
    public Double getFactor_inv(){
        return factor_inv ;
    }

    /**
     * 设置 [FACTOR_INV]
     */
    @JsonProperty("factor_inv")
    public void setFactor_inv(Double  factor_inv){
        this.factor_inv = factor_inv ;
        this.factor_invDirtyFlag = true ;
    }

    /**
     * 获取 [FACTOR_INV]脏标记
     */
    @JsonIgnore
    public boolean getFactor_invDirtyFlag(){
        return factor_invDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ROUNDING]
     */
    @JsonProperty("rounding")
    public Double getRounding(){
        return rounding ;
    }

    /**
     * 设置 [ROUNDING]
     */
    @JsonProperty("rounding")
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.roundingDirtyFlag = true ;
    }

    /**
     * 获取 [ROUNDING]脏标记
     */
    @JsonIgnore
    public boolean getRoundingDirtyFlag(){
        return roundingDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [FACTOR]
     */
    @JsonProperty("factor")
    public Double getFactor(){
        return factor ;
    }

    /**
     * 设置 [FACTOR]
     */
    @JsonProperty("factor")
    public void setFactor(Double  factor){
        this.factor = factor ;
        this.factorDirtyFlag = true ;
    }

    /**
     * 获取 [FACTOR]脏标记
     */
    @JsonIgnore
    public boolean getFactorDirtyFlag(){
        return factorDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [UOM_TYPE]
     */
    @JsonProperty("uom_type")
    public String getUom_type(){
        return uom_type ;
    }

    /**
     * 设置 [UOM_TYPE]
     */
    @JsonProperty("uom_type")
    public void setUom_type(String  uom_type){
        this.uom_type = uom_type ;
        this.uom_typeDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getUom_typeDirtyFlag(){
        return uom_typeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [IS_POS_GROUPABLE]
     */
    @JsonProperty("is_pos_groupable")
    public String getIs_pos_groupable(){
        return is_pos_groupable ;
    }

    /**
     * 设置 [IS_POS_GROUPABLE]
     */
    @JsonProperty("is_pos_groupable")
    public void setIs_pos_groupable(String  is_pos_groupable){
        this.is_pos_groupable = is_pos_groupable ;
        this.is_pos_groupableDirtyFlag = true ;
    }

    /**
     * 获取 [IS_POS_GROUPABLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_pos_groupableDirtyFlag(){
        return is_pos_groupableDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MEASURE_TYPE]
     */
    @JsonProperty("measure_type")
    public String getMeasure_type(){
        return measure_type ;
    }

    /**
     * 设置 [MEASURE_TYPE]
     */
    @JsonProperty("measure_type")
    public void setMeasure_type(String  measure_type){
        this.measure_type = measure_type ;
        this.measure_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MEASURE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMeasure_typeDirtyFlag(){
        return measure_typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Uom_uom toDO() {
        Uom_uom srfdomain = new Uom_uom();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getFactor_invDirtyFlag())
            srfdomain.setFactor_inv(factor_inv);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getRoundingDirtyFlag())
            srfdomain.setRounding(rounding);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getFactorDirtyFlag())
            srfdomain.setFactor(factor);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getUom_typeDirtyFlag())
            srfdomain.setUom_type(uom_type);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getIs_pos_groupableDirtyFlag())
            srfdomain.setIs_pos_groupable(is_pos_groupable);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMeasure_typeDirtyFlag())
            srfdomain.setMeasure_type(measure_type);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Uom_uom srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getFactor_invDirtyFlag())
            this.setFactor_inv(srfdomain.getFactor_inv());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getRoundingDirtyFlag())
            this.setRounding(srfdomain.getRounding());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getFactorDirtyFlag())
            this.setFactor(srfdomain.getFactor());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getUom_typeDirtyFlag())
            this.setUom_type(srfdomain.getUom_type());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getIs_pos_groupableDirtyFlag())
            this.setIs_pos_groupable(srfdomain.getIs_pos_groupable());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMeasure_typeDirtyFlag())
            this.setMeasure_type(srfdomain.getMeasure_type());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Uom_uomDTO> fromDOPage(List<Uom_uom> poPage)   {
        if(poPage == null)
            return null;
        List<Uom_uomDTO> dtos=new ArrayList<Uom_uomDTO>();
        for(Uom_uom domain : poPage) {
            Uom_uomDTO dto = new Uom_uomDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


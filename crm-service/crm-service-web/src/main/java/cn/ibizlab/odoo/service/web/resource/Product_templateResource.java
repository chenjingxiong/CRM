package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Product_templateDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_templateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_template" })
@RestController
@RequestMapping("")
public class Product_templateResource {

    @Autowired
    private IProduct_templateService product_templateService;

    public IProduct_templateService getProduct_templateService() {
        return this.product_templateService;
    }

    @ApiOperation(value = "建立数据", tags = {"Product_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_templates")

    public ResponseEntity<Product_templateDTO> create(@RequestBody Product_templateDTO product_templatedto) {
        Product_templateDTO dto = new Product_templateDTO();
        Product_template domain = product_templatedto.toDO();
		product_templateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_template" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_templates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_templates/{product_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_template_id") Integer product_template_id) {
        Product_templateDTO product_templatedto = new Product_templateDTO();
		Product_template domain = new Product_template();
		product_templatedto.setId(product_template_id);
		domain.setId(product_template_id);
        Boolean rst = product_templateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Product_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_templates/{product_template_id}")
    public ResponseEntity<Product_templateDTO> get(@PathVariable("product_template_id") Integer product_template_id) {
        Product_templateDTO dto = new Product_templateDTO();
        Product_template domain = product_templateService.get(product_template_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Product_template" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_templates/{product_template_id}/getdraft")

    public ResponseEntity<Product_templateDTO> getDraft(@PathVariable("product_template_id") Integer product_template_id, @RequestBody Product_templateDTO product_templatedto) {
        Product_template product_template = product_templatedto.toDO();
    	product_template = product_templateService.getDraft(product_template) ;
    	product_templatedto.fromDO(product_template);
        return ResponseEntity.status(HttpStatus.OK).body(product_templatedto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_template" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_templates/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_template(@RequestBody List<Product_templateDTO> product_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_templates/{product_template_id}")

    public ResponseEntity<Product_templateDTO> update(@PathVariable("product_template_id") Integer product_template_id, @RequestBody Product_templateDTO product_templatedto) {
		Product_template domain = product_templatedto.toDO();
        domain.setId(product_template_id);
		product_templateService.update(domain);
		Product_templateDTO dto = new Product_templateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_template" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_templates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_template" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/product_templates/fetchdefault")
	public ResponseEntity<Page<Product_templateDTO>> fetchDefault(Product_templateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_templateDTO> list = new ArrayList<Product_templateDTO>();
        
        Page<Product_template> domains = product_templateService.searchDefault(context) ;
        for(Product_template product_template : domains.getContent()){
            Product_templateDTO dto = new Product_templateDTO();
            dto.fromDO(product_template);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

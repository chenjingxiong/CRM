package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_payment.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_paymentDTO]
 */
public class Account_paymentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Account_paymentInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Account_paymentMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_paymentDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Account_paymentMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [PAYMENT_TYPE]
     *
     */
    @Account_paymentPayment_typeDefault(info = "默认规则")
    private String payment_type;

    @JsonIgnore
    private boolean payment_typeDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Account_paymentMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [HAS_INVOICES]
     *
     */
    @Account_paymentHas_invoicesDefault(info = "默认规则")
    private String has_invoices;

    @JsonIgnore
    private boolean has_invoicesDirtyFlag;

    /**
     * 属性 [PAYMENT_DATE]
     *
     */
    @Account_paymentPayment_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp payment_date;

    @JsonIgnore
    private boolean payment_dateDirtyFlag;

    /**
     * 属性 [MULTI]
     *
     */
    @Account_paymentMultiDefault(info = "默认规则")
    private String multi;

    @JsonIgnore
    private boolean multiDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Account_paymentMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_paymentNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MOVE_RECONCILED]
     *
     */
    @Account_paymentMove_reconciledDefault(info = "默认规则")
    private String move_reconciled;

    @JsonIgnore
    private boolean move_reconciledDirtyFlag;

    /**
     * 属性 [DESTINATION_ACCOUNT_ID]
     *
     */
    @Account_paymentDestination_account_idDefault(info = "默认规则")
    private Integer destination_account_id;

    @JsonIgnore
    private boolean destination_account_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Account_paymentMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Account_paymentMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Account_paymentMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [PAYMENT_DIFFERENCE]
     *
     */
    @Account_paymentPayment_differenceDefault(info = "默认规则")
    private Double payment_difference;

    @JsonIgnore
    private boolean payment_differenceDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Account_paymentMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Account_paymentMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [PAYMENT_DIFFERENCE_HANDLING]
     *
     */
    @Account_paymentPayment_difference_handlingDefault(info = "默认规则")
    private String payment_difference_handling;

    @JsonIgnore
    private boolean payment_difference_handlingDirtyFlag;

    /**
     * 属性 [RECONCILED_INVOICE_IDS]
     *
     */
    @Account_paymentReconciled_invoice_idsDefault(info = "默认规则")
    private String reconciled_invoice_ids;

    @JsonIgnore
    private boolean reconciled_invoice_idsDirtyFlag;

    /**
     * 属性 [PAYMENT_REFERENCE]
     *
     */
    @Account_paymentPayment_referenceDefault(info = "默认规则")
    private String payment_reference;

    @JsonIgnore
    private boolean payment_referenceDirtyFlag;

    /**
     * 属性 [WRITEOFF_LABEL]
     *
     */
    @Account_paymentWriteoff_labelDefault(info = "默认规则")
    private String writeoff_label;

    @JsonIgnore
    private boolean writeoff_labelDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Account_paymentMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Account_paymentWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_paymentWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_paymentAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [SHOW_PARTNER_BANK_ACCOUNT]
     *
     */
    @Account_paymentShow_partner_bank_accountDefault(info = "默认规则")
    private String show_partner_bank_account;

    @JsonIgnore
    private boolean show_partner_bank_accountDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_paymentCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PARTNER_TYPE]
     *
     */
    @Account_paymentPartner_typeDefault(info = "默认规则")
    private String partner_type;

    @JsonIgnore
    private boolean partner_typeDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Account_paymentMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_payment__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COMMUNICATION]
     *
     */
    @Account_paymentCommunicationDefault(info = "默认规则")
    private String communication;

    @JsonIgnore
    private boolean communicationDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_paymentStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_paymentIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Account_paymentMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Account_paymentMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [HIDE_PAYMENT_METHOD]
     *
     */
    @Account_paymentHide_payment_methodDefault(info = "默认规则")
    private String hide_payment_method;

    @JsonIgnore
    private boolean hide_payment_methodDirtyFlag;

    /**
     * 属性 [MOVE_NAME]
     *
     */
    @Account_paymentMove_nameDefault(info = "默认规则")
    private String move_name;

    @JsonIgnore
    private boolean move_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Account_paymentMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [DESTINATION_JOURNAL_ID_TEXT]
     *
     */
    @Account_paymentDestination_journal_id_textDefault(info = "默认规则")
    private String destination_journal_id_text;

    @JsonIgnore
    private boolean destination_journal_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_CODE]
     *
     */
    @Account_paymentPayment_method_codeDefault(info = "默认规则")
    private String payment_method_code;

    @JsonIgnore
    private boolean payment_method_codeDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_paymentWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_paymentJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_paymentCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID_TEXT]
     *
     */
    @Account_paymentWriteoff_account_id_textDefault(info = "默认规则")
    private String writeoff_account_id_text;

    @JsonIgnore
    private boolean writeoff_account_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_paymentCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID_TEXT]
     *
     */
    @Account_paymentPayment_method_id_textDefault(info = "默认规则")
    private String payment_method_id_text;

    @JsonIgnore
    private boolean payment_method_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_ID_TEXT]
     *
     */
    @Account_paymentPayment_token_id_textDefault(info = "默认规则")
    private String payment_token_id_text;

    @JsonIgnore
    private boolean payment_token_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_paymentCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_paymentPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_paymentWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_ID]
     *
     */
    @Account_paymentPayment_token_idDefault(info = "默认规则")
    private Integer payment_token_id;

    @JsonIgnore
    private boolean payment_token_idDirtyFlag;

    /**
     * 属性 [PAYMENT_TRANSACTION_ID]
     *
     */
    @Account_paymentPayment_transaction_idDefault(info = "默认规则")
    private Integer payment_transaction_id;

    @JsonIgnore
    private boolean payment_transaction_idDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID]
     *
     */
    @Account_paymentWriteoff_account_idDefault(info = "默认规则")
    private Integer writeoff_account_id;

    @JsonIgnore
    private boolean writeoff_account_idDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ACCOUNT_ID]
     *
     */
    @Account_paymentPartner_bank_account_idDefault(info = "默认规则")
    private Integer partner_bank_account_id;

    @JsonIgnore
    private boolean partner_bank_account_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_paymentCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID]
     *
     */
    @Account_paymentPayment_method_idDefault(info = "默认规则")
    private Integer payment_method_id;

    @JsonIgnore
    private boolean payment_method_idDirtyFlag;

    /**
     * 属性 [DESTINATION_JOURNAL_ID]
     *
     */
    @Account_paymentDestination_journal_idDefault(info = "默认规则")
    private Integer destination_journal_id;

    @JsonIgnore
    private boolean destination_journal_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_paymentPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_paymentJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_paymentCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;


    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return payment_type ;
    }

    /**
     * 设置 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return payment_typeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [HAS_INVOICES]
     */
    @JsonProperty("has_invoices")
    public String getHas_invoices(){
        return has_invoices ;
    }

    /**
     * 设置 [HAS_INVOICES]
     */
    @JsonProperty("has_invoices")
    public void setHas_invoices(String  has_invoices){
        this.has_invoices = has_invoices ;
        this.has_invoicesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_INVOICES]脏标记
     */
    @JsonIgnore
    public boolean getHas_invoicesDirtyFlag(){
        return has_invoicesDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return payment_date ;
    }

    /**
     * 设置 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return payment_dateDirtyFlag ;
    }

    /**
     * 获取 [MULTI]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return multi ;
    }

    /**
     * 设置 [MULTI]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return multiDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MOVE_RECONCILED]
     */
    @JsonProperty("move_reconciled")
    public String getMove_reconciled(){
        return move_reconciled ;
    }

    /**
     * 设置 [MOVE_RECONCILED]
     */
    @JsonProperty("move_reconciled")
    public void setMove_reconciled(String  move_reconciled){
        this.move_reconciled = move_reconciled ;
        this.move_reconciledDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_RECONCILED]脏标记
     */
    @JsonIgnore
    public boolean getMove_reconciledDirtyFlag(){
        return move_reconciledDirtyFlag ;
    }

    /**
     * 获取 [DESTINATION_ACCOUNT_ID]
     */
    @JsonProperty("destination_account_id")
    public Integer getDestination_account_id(){
        return destination_account_id ;
    }

    /**
     * 设置 [DESTINATION_ACCOUNT_ID]
     */
    @JsonProperty("destination_account_id")
    public void setDestination_account_id(Integer  destination_account_id){
        this.destination_account_id = destination_account_id ;
        this.destination_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [DESTINATION_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDestination_account_idDirtyFlag(){
        return destination_account_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return payment_difference ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return payment_differenceDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return payment_difference_handling ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return payment_difference_handlingDirtyFlag ;
    }

    /**
     * 获取 [RECONCILED_INVOICE_IDS]
     */
    @JsonProperty("reconciled_invoice_ids")
    public String getReconciled_invoice_ids(){
        return reconciled_invoice_ids ;
    }

    /**
     * 设置 [RECONCILED_INVOICE_IDS]
     */
    @JsonProperty("reconciled_invoice_ids")
    public void setReconciled_invoice_ids(String  reconciled_invoice_ids){
        this.reconciled_invoice_ids = reconciled_invoice_ids ;
        this.reconciled_invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILED_INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getReconciled_invoice_idsDirtyFlag(){
        return reconciled_invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_REFERENCE]
     */
    @JsonProperty("payment_reference")
    public String getPayment_reference(){
        return payment_reference ;
    }

    /**
     * 设置 [PAYMENT_REFERENCE]
     */
    @JsonProperty("payment_reference")
    public void setPayment_reference(String  payment_reference){
        this.payment_reference = payment_reference ;
        this.payment_referenceDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_referenceDirtyFlag(){
        return payment_referenceDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return writeoff_label ;
    }

    /**
     * 设置 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return writeoff_labelDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return show_partner_bank_account ;
    }

    /**
     * 设置 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return show_partner_bank_accountDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return partner_type ;
    }

    /**
     * 设置 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return partner_typeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return communication ;
    }

    /**
     * 设置 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

    /**
     * 获取 [COMMUNICATION]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return communicationDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return hide_payment_method ;
    }

    /**
     * 设置 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return hide_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return move_name ;
    }

    /**
     * 设置 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return move_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [DESTINATION_JOURNAL_ID_TEXT]
     */
    @JsonProperty("destination_journal_id_text")
    public String getDestination_journal_id_text(){
        return destination_journal_id_text ;
    }

    /**
     * 设置 [DESTINATION_JOURNAL_ID_TEXT]
     */
    @JsonProperty("destination_journal_id_text")
    public void setDestination_journal_id_text(String  destination_journal_id_text){
        this.destination_journal_id_text = destination_journal_id_text ;
        this.destination_journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DESTINATION_JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDestination_journal_id_textDirtyFlag(){
        return destination_journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return payment_method_code ;
    }

    /**
     * 设置 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return payment_method_codeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return writeoff_account_id_text ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return writeoff_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return payment_method_id_text ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return payment_method_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID_TEXT]
     */
    @JsonProperty("payment_token_id_text")
    public String getPayment_token_id_text(){
        return payment_token_id_text ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_ID_TEXT]
     */
    @JsonProperty("payment_token_id_text")
    public void setPayment_token_id_text(String  payment_token_id_text){
        this.payment_token_id_text = payment_token_id_text ;
        this.payment_token_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_id_textDirtyFlag(){
        return payment_token_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID]
     */
    @JsonProperty("payment_token_id")
    public Integer getPayment_token_id(){
        return payment_token_id ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_ID]
     */
    @JsonProperty("payment_token_id")
    public void setPayment_token_id(Integer  payment_token_id){
        this.payment_token_id = payment_token_id ;
        this.payment_token_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idDirtyFlag(){
        return payment_token_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TRANSACTION_ID]
     */
    @JsonProperty("payment_transaction_id")
    public Integer getPayment_transaction_id(){
        return payment_transaction_id ;
    }

    /**
     * 设置 [PAYMENT_TRANSACTION_ID]
     */
    @JsonProperty("payment_transaction_id")
    public void setPayment_transaction_id(Integer  payment_transaction_id){
        this.payment_transaction_id = payment_transaction_id ;
        this.payment_transaction_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TRANSACTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_transaction_idDirtyFlag(){
        return payment_transaction_idDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return writeoff_account_id ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return writeoff_account_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return partner_bank_account_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return partner_bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return payment_method_id ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return payment_method_idDirtyFlag ;
    }

    /**
     * 获取 [DESTINATION_JOURNAL_ID]
     */
    @JsonProperty("destination_journal_id")
    public Integer getDestination_journal_id(){
        return destination_journal_id ;
    }

    /**
     * 设置 [DESTINATION_JOURNAL_ID]
     */
    @JsonProperty("destination_journal_id")
    public void setDestination_journal_id(Integer  destination_journal_id){
        this.destination_journal_id = destination_journal_id ;
        this.destination_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [DESTINATION_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getDestination_journal_idDirtyFlag(){
        return destination_journal_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }



    public Account_payment toDO() {
        Account_payment srfdomain = new Account_payment();
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getPayment_typeDirtyFlag())
            srfdomain.setPayment_type(payment_type);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getHas_invoicesDirtyFlag())
            srfdomain.setHas_invoices(has_invoices);
        if(getPayment_dateDirtyFlag())
            srfdomain.setPayment_date(payment_date);
        if(getMultiDirtyFlag())
            srfdomain.setMulti(multi);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMove_reconciledDirtyFlag())
            srfdomain.setMove_reconciled(move_reconciled);
        if(getDestination_account_idDirtyFlag())
            srfdomain.setDestination_account_id(destination_account_id);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getPayment_differenceDirtyFlag())
            srfdomain.setPayment_difference(payment_difference);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getPayment_difference_handlingDirtyFlag())
            srfdomain.setPayment_difference_handling(payment_difference_handling);
        if(getReconciled_invoice_idsDirtyFlag())
            srfdomain.setReconciled_invoice_ids(reconciled_invoice_ids);
        if(getPayment_referenceDirtyFlag())
            srfdomain.setPayment_reference(payment_reference);
        if(getWriteoff_labelDirtyFlag())
            srfdomain.setWriteoff_label(writeoff_label);
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getShow_partner_bank_accountDirtyFlag())
            srfdomain.setShow_partner_bank_account(show_partner_bank_account);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPartner_typeDirtyFlag())
            srfdomain.setPartner_type(partner_type);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCommunicationDirtyFlag())
            srfdomain.setCommunication(communication);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getHide_payment_methodDirtyFlag())
            srfdomain.setHide_payment_method(hide_payment_method);
        if(getMove_nameDirtyFlag())
            srfdomain.setMove_name(move_name);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getDestination_journal_id_textDirtyFlag())
            srfdomain.setDestination_journal_id_text(destination_journal_id_text);
        if(getPayment_method_codeDirtyFlag())
            srfdomain.setPayment_method_code(payment_method_code);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWriteoff_account_id_textDirtyFlag())
            srfdomain.setWriteoff_account_id_text(writeoff_account_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getPayment_method_id_textDirtyFlag())
            srfdomain.setPayment_method_id_text(payment_method_id_text);
        if(getPayment_token_id_textDirtyFlag())
            srfdomain.setPayment_token_id_text(payment_token_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPayment_token_idDirtyFlag())
            srfdomain.setPayment_token_id(payment_token_id);
        if(getPayment_transaction_idDirtyFlag())
            srfdomain.setPayment_transaction_id(payment_transaction_id);
        if(getWriteoff_account_idDirtyFlag())
            srfdomain.setWriteoff_account_id(writeoff_account_id);
        if(getPartner_bank_account_idDirtyFlag())
            srfdomain.setPartner_bank_account_id(partner_bank_account_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPayment_method_idDirtyFlag())
            srfdomain.setPayment_method_id(payment_method_id);
        if(getDestination_journal_idDirtyFlag())
            srfdomain.setDestination_journal_id(destination_journal_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);

        return srfdomain;
    }

    public void fromDO(Account_payment srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getPayment_typeDirtyFlag())
            this.setPayment_type(srfdomain.getPayment_type());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getHas_invoicesDirtyFlag())
            this.setHas_invoices(srfdomain.getHas_invoices());
        if(srfdomain.getPayment_dateDirtyFlag())
            this.setPayment_date(srfdomain.getPayment_date());
        if(srfdomain.getMultiDirtyFlag())
            this.setMulti(srfdomain.getMulti());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMove_reconciledDirtyFlag())
            this.setMove_reconciled(srfdomain.getMove_reconciled());
        if(srfdomain.getDestination_account_idDirtyFlag())
            this.setDestination_account_id(srfdomain.getDestination_account_id());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getPayment_differenceDirtyFlag())
            this.setPayment_difference(srfdomain.getPayment_difference());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getPayment_difference_handlingDirtyFlag())
            this.setPayment_difference_handling(srfdomain.getPayment_difference_handling());
        if(srfdomain.getReconciled_invoice_idsDirtyFlag())
            this.setReconciled_invoice_ids(srfdomain.getReconciled_invoice_ids());
        if(srfdomain.getPayment_referenceDirtyFlag())
            this.setPayment_reference(srfdomain.getPayment_reference());
        if(srfdomain.getWriteoff_labelDirtyFlag())
            this.setWriteoff_label(srfdomain.getWriteoff_label());
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getShow_partner_bank_accountDirtyFlag())
            this.setShow_partner_bank_account(srfdomain.getShow_partner_bank_account());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPartner_typeDirtyFlag())
            this.setPartner_type(srfdomain.getPartner_type());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCommunicationDirtyFlag())
            this.setCommunication(srfdomain.getCommunication());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getHide_payment_methodDirtyFlag())
            this.setHide_payment_method(srfdomain.getHide_payment_method());
        if(srfdomain.getMove_nameDirtyFlag())
            this.setMove_name(srfdomain.getMove_name());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getDestination_journal_id_textDirtyFlag())
            this.setDestination_journal_id_text(srfdomain.getDestination_journal_id_text());
        if(srfdomain.getPayment_method_codeDirtyFlag())
            this.setPayment_method_code(srfdomain.getPayment_method_code());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWriteoff_account_id_textDirtyFlag())
            this.setWriteoff_account_id_text(srfdomain.getWriteoff_account_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getPayment_method_id_textDirtyFlag())
            this.setPayment_method_id_text(srfdomain.getPayment_method_id_text());
        if(srfdomain.getPayment_token_id_textDirtyFlag())
            this.setPayment_token_id_text(srfdomain.getPayment_token_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPayment_token_idDirtyFlag())
            this.setPayment_token_id(srfdomain.getPayment_token_id());
        if(srfdomain.getPayment_transaction_idDirtyFlag())
            this.setPayment_transaction_id(srfdomain.getPayment_transaction_id());
        if(srfdomain.getWriteoff_account_idDirtyFlag())
            this.setWriteoff_account_id(srfdomain.getWriteoff_account_id());
        if(srfdomain.getPartner_bank_account_idDirtyFlag())
            this.setPartner_bank_account_id(srfdomain.getPartner_bank_account_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPayment_method_idDirtyFlag())
            this.setPayment_method_id(srfdomain.getPayment_method_id());
        if(srfdomain.getDestination_journal_idDirtyFlag())
            this.setDestination_journal_id(srfdomain.getDestination_journal_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());

    }

    public List<Account_paymentDTO> fromDOPage(List<Account_payment> poPage)   {
        if(poPage == null)
            return null;
        List<Account_paymentDTO> dtos=new ArrayList<Account_paymentDTO>();
        for(Account_payment domain : poPage) {
            Account_paymentDTO dto = new Account_paymentDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


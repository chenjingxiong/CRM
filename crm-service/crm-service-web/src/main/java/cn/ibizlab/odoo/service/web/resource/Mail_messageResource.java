package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Mail_messageDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_messageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_message" })
@RestController
@RequestMapping("")
public class Mail_messageResource {

    @Autowired
    private IMail_messageService mail_messageService;

    public IMail_messageService getMail_messageService() {
        return this.mail_messageService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages")

    public ResponseEntity<Mail_messageDTO> create(@RequestBody Mail_messageDTO mail_messagedto) {
        Mail_messageDTO dto = new Mail_messageDTO();
        Mail_message domain = mail_messagedto.toDO();
		mail_messageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_messages/{mail_message_id}")
    public ResponseEntity<Mail_messageDTO> get(@PathVariable("mail_message_id") Integer mail_message_id) {
        Mail_messageDTO dto = new Mail_messageDTO();
        Mail_message domain = mail_messageService.get(mail_message_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Mail_message" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_messageDTO mail_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_message" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_messages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Mail_message" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_messages/{mail_message_id}/getdraft")

    public ResponseEntity<Mail_messageDTO> getDraft(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message mail_message = mail_messagedto.toDO();
    	mail_message = mail_messageService.getDraft(mail_message) ;
    	mail_messagedto.fromDO(mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_messagedto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_message" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/createBatch")
    public ResponseEntity<Boolean> createBatchMail_message(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_message" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_messages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_messages/{mail_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_id") Integer mail_message_id) {
        Mail_messageDTO mail_messagedto = new Mail_messageDTO();
		Mail_message domain = new Mail_message();
		mail_messagedto.setId(mail_message_id);
		domain.setId(mail_message_id);
        Boolean rst = mail_messageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_messages/{mail_message_id}")

    public ResponseEntity<Mail_messageDTO> update(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
		Mail_message domain = mail_messagedto.toDO();
        domain.setId(mail_message_id);
		mail_messageService.update(domain);
		Mail_messageDTO dto = new Mail_messageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Mail_message" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/{mail_message_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message mail_message = mail_messagedto.toDO();
    	Boolean b = mail_messageService.save(mail_message) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_message" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/mail_messages/fetchdefault")
	public ResponseEntity<Page<Mail_messageDTO>> fetchDefault(Mail_messageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_messageDTO> list = new ArrayList<Mail_messageDTO>();
        
        Page<Mail_message> domains = mail_messageService.searchDefault(context) ;
        for(Mail_message mail_message : domains.getContent()){
            Mail_messageDTO dto = new Mail_messageDTO();
            dto.fromDO(mail_message);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

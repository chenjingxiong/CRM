package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_payment_term_line.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_payment_term_lineDTO]
 */
public class Account_payment_term_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DAYS]
     *
     */
    @Account_payment_term_lineDaysDefault(info = "默认规则")
    private Integer days;

    @JsonIgnore
    private boolean daysDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_payment_term_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_payment_term_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_payment_term_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [VALUE]
     *
     */
    @Account_payment_term_lineValueDefault(info = "默认规则")
    private String value;

    @JsonIgnore
    private boolean valueDirtyFlag;

    /**
     * 属性 [VALUE_AMOUNT]
     *
     */
    @Account_payment_term_lineValue_amountDefault(info = "默认规则")
    private Double value_amount;

    @JsonIgnore
    private boolean value_amountDirtyFlag;

    /**
     * 属性 [DAY_OF_THE_MONTH]
     *
     */
    @Account_payment_term_lineDay_of_the_monthDefault(info = "默认规则")
    private Integer day_of_the_month;

    @JsonIgnore
    private boolean day_of_the_monthDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_payment_term_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [OPTION]
     *
     */
    @Account_payment_term_lineOptionDefault(info = "默认规则")
    private String option;

    @JsonIgnore
    private boolean optionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_payment_term_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_payment_term_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @Account_payment_term_linePayment_id_textDefault(info = "默认规则")
    private String payment_id_text;

    @JsonIgnore
    private boolean payment_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_payment_term_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_payment_term_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_payment_term_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @Account_payment_term_linePayment_idDefault(info = "默认规则")
    private Integer payment_id;

    @JsonIgnore
    private boolean payment_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_payment_term_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [DAYS]
     */
    @JsonProperty("days")
    public Integer getDays(){
        return days ;
    }

    /**
     * 设置 [DAYS]
     */
    @JsonProperty("days")
    public void setDays(Integer  days){
        this.days = days ;
        this.daysDirtyFlag = true ;
    }

    /**
     * 获取 [DAYS]脏标记
     */
    @JsonIgnore
    public boolean getDaysDirtyFlag(){
        return daysDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [VALUE]
     */
    @JsonProperty("value")
    public String getValue(){
        return value ;
    }

    /**
     * 设置 [VALUE]
     */
    @JsonProperty("value")
    public void setValue(String  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return valueDirtyFlag ;
    }

    /**
     * 获取 [VALUE_AMOUNT]
     */
    @JsonProperty("value_amount")
    public Double getValue_amount(){
        return value_amount ;
    }

    /**
     * 设置 [VALUE_AMOUNT]
     */
    @JsonProperty("value_amount")
    public void setValue_amount(Double  value_amount){
        this.value_amount = value_amount ;
        this.value_amountDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getValue_amountDirtyFlag(){
        return value_amountDirtyFlag ;
    }

    /**
     * 获取 [DAY_OF_THE_MONTH]
     */
    @JsonProperty("day_of_the_month")
    public Integer getDay_of_the_month(){
        return day_of_the_month ;
    }

    /**
     * 设置 [DAY_OF_THE_MONTH]
     */
    @JsonProperty("day_of_the_month")
    public void setDay_of_the_month(Integer  day_of_the_month){
        this.day_of_the_month = day_of_the_month ;
        this.day_of_the_monthDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_OF_THE_MONTH]脏标记
     */
    @JsonIgnore
    public boolean getDay_of_the_monthDirtyFlag(){
        return day_of_the_monthDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [OPTION]
     */
    @JsonProperty("option")
    public String getOption(){
        return option ;
    }

    /**
     * 设置 [OPTION]
     */
    @JsonProperty("option")
    public void setOption(String  option){
        this.option = option ;
        this.optionDirtyFlag = true ;
    }

    /**
     * 获取 [OPTION]脏标记
     */
    @JsonIgnore
    public boolean getOptionDirtyFlag(){
        return optionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return payment_id_text ;
    }

    /**
     * 设置 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return payment_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return payment_id ;
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return payment_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Account_payment_term_line toDO() {
        Account_payment_term_line srfdomain = new Account_payment_term_line();
        if(getDaysDirtyFlag())
            srfdomain.setDays(days);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getValueDirtyFlag())
            srfdomain.setValue(value);
        if(getValue_amountDirtyFlag())
            srfdomain.setValue_amount(value_amount);
        if(getDay_of_the_monthDirtyFlag())
            srfdomain.setDay_of_the_month(day_of_the_month);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getOptionDirtyFlag())
            srfdomain.setOption(option);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPayment_id_textDirtyFlag())
            srfdomain.setPayment_id_text(payment_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPayment_idDirtyFlag())
            srfdomain.setPayment_id(payment_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Account_payment_term_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDaysDirtyFlag())
            this.setDays(srfdomain.getDays());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getValueDirtyFlag())
            this.setValue(srfdomain.getValue());
        if(srfdomain.getValue_amountDirtyFlag())
            this.setValue_amount(srfdomain.getValue_amount());
        if(srfdomain.getDay_of_the_monthDirtyFlag())
            this.setDay_of_the_month(srfdomain.getDay_of_the_month());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getOptionDirtyFlag())
            this.setOption(srfdomain.getOption());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPayment_id_textDirtyFlag())
            this.setPayment_id_text(srfdomain.getPayment_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPayment_idDirtyFlag())
            this.setPayment_id(srfdomain.getPayment_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Account_payment_term_lineDTO> fromDOPage(List<Account_payment_term_line> poPage)   {
        if(poPage == null)
            return null;
        List<Account_payment_term_lineDTO> dtos=new ArrayList<Account_payment_term_lineDTO>();
        for(Account_payment_term_line domain : poPage) {
            Account_payment_term_lineDTO dto = new Account_payment_term_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


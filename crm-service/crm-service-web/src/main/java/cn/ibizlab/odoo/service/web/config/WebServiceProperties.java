package cn.ibizlab.odoo.service.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.web")
@Data
public class WebServiceProperties {

	private boolean enabled;

	private boolean auth;


}
package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Payment_acquirerDTO;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Payment_acquirer" })
@RestController
@RequestMapping("")
public class Payment_acquirerResource {

    @Autowired
    private IPayment_acquirerService payment_acquirerService;

    public IPayment_acquirerService getPayment_acquirerService() {
        return this.payment_acquirerService;
    }

    @ApiOperation(value = "删除数据", tags = {"Payment_acquirer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/payment_acquirers/{payment_acquirer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id) {
        Payment_acquirerDTO payment_acquirerdto = new Payment_acquirerDTO();
		Payment_acquirer domain = new Payment_acquirer();
		payment_acquirerdto.setId(payment_acquirer_id);
		domain.setId(payment_acquirer_id);
        Boolean rst = payment_acquirerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Payment_acquirer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/payment_acquirers/{payment_acquirer_id}")

    public ResponseEntity<Payment_acquirerDTO> update(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id, @RequestBody Payment_acquirerDTO payment_acquirerdto) {
		Payment_acquirer domain = payment_acquirerdto.toDO();
        domain.setId(payment_acquirer_id);
		payment_acquirerService.update(domain);
		Payment_acquirerDTO dto = new Payment_acquirerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Payment_acquirer" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/payment_acquirers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Payment_acquirer" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/payment_acquirers/createBatch")
    public ResponseEntity<Boolean> createBatchPayment_acquirer(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Payment_acquirer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/payment_acquirers/{payment_acquirer_id}")
    public ResponseEntity<Payment_acquirerDTO> get(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id) {
        Payment_acquirerDTO dto = new Payment_acquirerDTO();
        Payment_acquirer domain = payment_acquirerService.get(payment_acquirer_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Payment_acquirer" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/payment_acquirers/{payment_acquirer_id}/getdraft")

    public ResponseEntity<Payment_acquirerDTO> getDraft(@PathVariable("payment_acquirer_id") Integer payment_acquirer_id, @RequestBody Payment_acquirerDTO payment_acquirerdto) {
        Payment_acquirer payment_acquirer = payment_acquirerdto.toDO();
    	payment_acquirer = payment_acquirerService.getDraft(payment_acquirer) ;
    	payment_acquirerdto.fromDO(payment_acquirer);
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerdto);
    }

    @ApiOperation(value = "建立数据", tags = {"Payment_acquirer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/payment_acquirers")

    public ResponseEntity<Payment_acquirerDTO> create(@RequestBody Payment_acquirerDTO payment_acquirerdto) {
        Payment_acquirerDTO dto = new Payment_acquirerDTO();
        Payment_acquirer domain = payment_acquirerdto.toDO();
		payment_acquirerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Payment_acquirer" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/payment_acquirers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Payment_acquirer" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/payment_acquirers/fetchdefault")
	public ResponseEntity<Page<Payment_acquirerDTO>> fetchDefault(Payment_acquirerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Payment_acquirerDTO> list = new ArrayList<Payment_acquirerDTO>();
        
        Page<Payment_acquirer> domains = payment_acquirerService.searchDefault(context) ;
        for(Payment_acquirer payment_acquirer : domains.getContent()){
            Payment_acquirerDTO dto = new Payment_acquirerDTO();
            dto.fromDO(payment_acquirer);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Product_categoryDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_category" })
@RestController
@RequestMapping("")
public class Product_categoryResource {

    @Autowired
    private IProduct_categoryService product_categoryService;

    public IProduct_categoryService getProduct_categoryService() {
        return this.product_categoryService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_categories/{product_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_category_id") Integer product_category_id) {
        Product_categoryDTO product_categorydto = new Product_categoryDTO();
		Product_category domain = new Product_category();
		product_categorydto.setId(product_category_id);
		domain.setId(product_category_id);
        Boolean rst = product_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Product_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories")

    public ResponseEntity<Product_categoryDTO> create(@RequestBody Product_categoryDTO product_categorydto) {
        Product_categoryDTO dto = new Product_categoryDTO();
        Product_category domain = product_categorydto.toDO();
		product_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_categories/{product_category_id}")

    public ResponseEntity<Product_categoryDTO> update(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_categoryDTO product_categorydto) {
		Product_category domain = product_categorydto.toDO();
        domain.setId(product_category_id);
		product_categoryService.update(domain);
		Product_categoryDTO dto = new Product_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Product_category" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/{product_category_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_categoryDTO product_categorydto) {
        Product_category product_category = product_categorydto.toDO();
    	Boolean b = product_categoryService.save(product_category) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Product_category" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_categories/{product_category_id}/getdraft")

    public ResponseEntity<Product_categoryDTO> getDraft(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_categoryDTO product_categorydto) {
        Product_category product_category = product_categorydto.toDO();
    	product_category = product_categoryService.getDraft(product_category) ;
    	product_categorydto.fromDO(product_category);
        return ResponseEntity.status(HttpStatus.OK).body(product_categorydto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_category(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "CheckKey", tags = {"Product_category" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_categoryDTO product_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_categories/{product_category_id}")
    public ResponseEntity<Product_categoryDTO> get(@PathVariable("product_category_id") Integer product_category_id) {
        Product_categoryDTO dto = new Product_categoryDTO();
        Product_category domain = product_categoryService.get(product_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/product_categories/fetchdefault")
	public ResponseEntity<Page<Product_categoryDTO>> fetchDefault(Product_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_categoryDTO> list = new ArrayList<Product_categoryDTO>();
        
        Page<Product_category> domains = product_categoryService.searchDefault(context) ;
        for(Product_category product_category : domains.getContent()){
            Product_categoryDTO dto = new Product_categoryDTO();
            dto.fromDO(product_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

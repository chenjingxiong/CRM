package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_stage.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Crm_stageDTO]
 */
public class Crm_stageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @Crm_stageProbabilityDefault(info = "默认规则")
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Crm_stageDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [REQUIREMENTS]
     *
     */
    @Crm_stageRequirementsDefault(info = "默认规则")
    private String requirements;

    @JsonIgnore
    private boolean requirementsDirtyFlag;

    /**
     * 属性 [FOLD]
     *
     */
    @Crm_stageFoldDefault(info = "默认规则")
    private String fold;

    @JsonIgnore
    private boolean foldDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Crm_stageSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [ON_CHANGE]
     *
     */
    @Crm_stageOn_changeDefault(info = "默认规则")
    private String on_change;

    @JsonIgnore
    private boolean on_changeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Crm_stageWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [LEGEND_PRIORITY]
     *
     */
    @Crm_stageLegend_priorityDefault(info = "默认规则")
    private String legend_priority;

    @JsonIgnore
    private boolean legend_priorityDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Crm_stageNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Crm_stageIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Crm_stageCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TEAM_COUNT]
     *
     */
    @Crm_stageTeam_countDefault(info = "默认规则")
    private Integer team_count;

    @JsonIgnore
    private boolean team_countDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Crm_stage__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Crm_stageCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Crm_stageWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Crm_stageTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Crm_stageCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Crm_stageTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Crm_stageWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [PROBABILITY]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return probability ;
    }

    /**
     * 设置 [PROBABILITY]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [PROBABILITY]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return probabilityDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public String getRequirements(){
        return requirements ;
    }

    /**
     * 设置 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.requirementsDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIREMENTS]脏标记
     */
    @JsonIgnore
    public boolean getRequirementsDirtyFlag(){
        return requirementsDirtyFlag ;
    }

    /**
     * 获取 [FOLD]
     */
    @JsonProperty("fold")
    public String getFold(){
        return fold ;
    }

    /**
     * 设置 [FOLD]
     */
    @JsonProperty("fold")
    public void setFold(String  fold){
        this.fold = fold ;
        this.foldDirtyFlag = true ;
    }

    /**
     * 获取 [FOLD]脏标记
     */
    @JsonIgnore
    public boolean getFoldDirtyFlag(){
        return foldDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [ON_CHANGE]
     */
    @JsonProperty("on_change")
    public String getOn_change(){
        return on_change ;
    }

    /**
     * 设置 [ON_CHANGE]
     */
    @JsonProperty("on_change")
    public void setOn_change(String  on_change){
        this.on_change = on_change ;
        this.on_changeDirtyFlag = true ;
    }

    /**
     * 获取 [ON_CHANGE]脏标记
     */
    @JsonIgnore
    public boolean getOn_changeDirtyFlag(){
        return on_changeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_PRIORITY]
     */
    @JsonProperty("legend_priority")
    public String getLegend_priority(){
        return legend_priority ;
    }

    /**
     * 设置 [LEGEND_PRIORITY]
     */
    @JsonProperty("legend_priority")
    public void setLegend_priority(String  legend_priority){
        this.legend_priority = legend_priority ;
        this.legend_priorityDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getLegend_priorityDirtyFlag(){
        return legend_priorityDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TEAM_COUNT]
     */
    @JsonProperty("team_count")
    public Integer getTeam_count(){
        return team_count ;
    }

    /**
     * 设置 [TEAM_COUNT]
     */
    @JsonProperty("team_count")
    public void setTeam_count(Integer  team_count){
        this.team_count = team_count ;
        this.team_countDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_countDirtyFlag(){
        return team_countDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Crm_stage toDO() {
        Crm_stage srfdomain = new Crm_stage();
        if(getProbabilityDirtyFlag())
            srfdomain.setProbability(probability);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getRequirementsDirtyFlag())
            srfdomain.setRequirements(requirements);
        if(getFoldDirtyFlag())
            srfdomain.setFold(fold);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getOn_changeDirtyFlag())
            srfdomain.setOn_change(on_change);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getLegend_priorityDirtyFlag())
            srfdomain.setLegend_priority(legend_priority);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTeam_countDirtyFlag())
            srfdomain.setTeam_count(team_count);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Crm_stage srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getProbabilityDirtyFlag())
            this.setProbability(srfdomain.getProbability());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getRequirementsDirtyFlag())
            this.setRequirements(srfdomain.getRequirements());
        if(srfdomain.getFoldDirtyFlag())
            this.setFold(srfdomain.getFold());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getOn_changeDirtyFlag())
            this.setOn_change(srfdomain.getOn_change());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getLegend_priorityDirtyFlag())
            this.setLegend_priority(srfdomain.getLegend_priority());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTeam_countDirtyFlag())
            this.setTeam_count(srfdomain.getTeam_count());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Crm_stageDTO> fromDOPage(List<Crm_stage> poPage)   {
        if(poPage == null)
            return null;
        List<Crm_stageDTO> dtos=new ArrayList<Crm_stageDTO>();
        for(Crm_stage domain : poPage) {
            Crm_stageDTO dto = new Crm_stageDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}


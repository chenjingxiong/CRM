package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_lead_tagDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lead_tag" })
@RestController
@RequestMapping("")
public class Crm_lead_tagResource {

    @Autowired
    private ICrm_lead_tagService crm_lead_tagService;

    public ICrm_lead_tagService getCrm_lead_tagService() {
        return this.crm_lead_tagService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lead_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead_tag" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_tags/{crm_lead_tag_id}/getdraft")

    public ResponseEntity<Crm_lead_tagDTO> getDraft(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        Crm_lead_tag crm_lead_tag = crm_lead_tagdto.toDO();
    	crm_lead_tag = crm_lead_tagService.getDraft(crm_lead_tag) ;
    	crm_lead_tagdto.fromDO(crm_lead_tag);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagdto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lead_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lead_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_tags/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lead_tag(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lead_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_tags/{crm_lead_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) {
        Crm_lead_tagDTO crm_lead_tagdto = new Crm_lead_tagDTO();
		Crm_lead_tag domain = new Crm_lead_tag();
		crm_lead_tagdto.setId(crm_lead_tag_id);
		domain.setId(crm_lead_tag_id);
        Boolean rst = crm_lead_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lead_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_tags/{crm_lead_tag_id}")

    public ResponseEntity<Crm_lead_tagDTO> update(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
		Crm_lead_tag domain = crm_lead_tagdto.toDO();
        domain.setId(crm_lead_tag_id);
		crm_lead_tagService.update(domain);
		Crm_lead_tagDTO dto = new Crm_lead_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lead_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_tags/{crm_lead_tag_id}")
    public ResponseEntity<Crm_lead_tagDTO> get(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) {
        Crm_lead_tagDTO dto = new Crm_lead_tagDTO();
        Crm_lead_tag domain = crm_lead_tagService.get(crm_lead_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lead_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_tags")

    public ResponseEntity<Crm_lead_tagDTO> create(@RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        Crm_lead_tagDTO dto = new Crm_lead_tagDTO();
        Crm_lead_tag domain = crm_lead_tagdto.toDO();
		crm_lead_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lead_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead_tags/fetchdefault")
	public ResponseEntity<Page<Crm_lead_tagDTO>> fetchDefault(Crm_lead_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_lead_tagDTO> list = new ArrayList<Crm_lead_tagDTO>();
        
        Page<Crm_lead_tag> domains = crm_lead_tagService.searchDefault(context) ;
        for(Crm_lead_tag crm_lead_tag : domains.getContent()){
            Crm_lead_tagDTO dto = new Crm_lead_tagDTO();
            dto.fromDO(crm_lead_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}

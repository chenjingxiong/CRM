package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Res_usersDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_users" })
@RestController
@RequestMapping("")
public class Res_usersResource {

    @Autowired
    private IRes_usersService res_usersService;

    public IRes_usersService getRes_usersService() {
        return this.res_usersService;
    }

    @ApiOperation(value = "更新数据", tags = {"Res_users" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain = res_usersdto.toDO();
        domain.setId(res_users_id);
		res_usersService.update(domain);
		Res_usersDTO dto = new Res_usersDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Res_users" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/{res_users_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_usersDTO res_usersdto) {
        Res_users res_users = res_usersdto.toDO();
    	Boolean b = res_usersService.save(res_users) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_users" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users")

    public ResponseEntity<Res_usersDTO> create(@RequestBody Res_usersDTO res_usersdto) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersdto.toDO();
		res_usersService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Res_users" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_usersDTO res_usersdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_users" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> get(@PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersService.get(res_users_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_users" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_users/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_users" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_users/createBatch")
    public ResponseEntity<Boolean> createBatchRes_users(@RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_users" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_users/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_users" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_users/{res_users_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO res_usersdto = new Res_usersDTO();
		Res_users domain = new Res_users();
		res_usersdto.setId(res_users_id);
		domain.setId(res_users_id);
        Boolean rst = res_usersService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Res_users" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_users/{res_users_id}/getdraft")

    public ResponseEntity<Res_usersDTO> getDraft(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users res_users = res_usersdto.toDO();
    	res_users = res_usersService.getDraft(res_users) ;
    	res_usersdto.fromDO(res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_usersdto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_users" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_users/fetchdefault")
	public ResponseEntity<Page<Res_usersDTO>> fetchDefault(Res_usersSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_usersDTO> list = new ArrayList<Res_usersDTO>();
        
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        for(Res_users res_users : domains.getContent()){
            Res_usersDTO dto = new Res_usersDTO();
            dto.fromDO(res_users);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}



    @ApiOperation(value = "更新数据", tags = {"Res_users" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> updateByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain = res_usersdto.toDO();
        domain.setSale_team_id(crm_team_id);
        domain.setId(res_users_id);
		res_usersService.update(domain);
		Res_usersDTO dto = new Res_usersDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Res_users" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/save")
    public ResponseEntity<Boolean> saveByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersdto.toDO();
        domain.setSale_team_id(crm_team_id);
    	Boolean b = res_usersService.save(domain) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_users" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users")

    public ResponseEntity<Res_usersDTO> createByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersdto.toDO();
        domain.setSale_team_id(crm_team_id);
		res_usersService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Res_users" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/checkkey")
    public ResponseEntity<Boolean> checkKeyByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Res_usersDTO res_usersdto) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_users" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> getByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersService.get(res_users_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_users" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}/res_users/updatebatch")
    public ResponseEntity<Boolean> updateBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_users" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/res_users/createbatch")
    public ResponseEntity<Boolean> createBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_users" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}/res_users/removebatch")
    public ResponseEntity<Boolean> removeBatchByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_users" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}/res_users/{res_users_id}")

    public ResponseEntity<Boolean> removeByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO res_usersdto = new Res_usersDTO();
		Res_users domain = new Res_users();
		res_usersdto.setId(res_users_id);
		domain.setId(res_users_id);
        Boolean rst = res_usersService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Res_users" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}/res_users/{res_usersid}/getdraft")

    public ResponseEntity<Res_usersDTO> getDraftByCrm_team(@PathVariable("crm_team_id") Integer crm_team_id, @PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users res_users = res_usersdto.toDO();
    	res_users = res_usersService.getDraft(res_users) ;
    	res_usersdto.fromDO(res_users);
        return ResponseEntity.status(HttpStatus.OK).body(res_usersdto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_users" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_teams/{crm_team_id}/res_users/fetchdefault")
	public ResponseEntity<Page<Res_usersDTO>> fetchRes_usersDefault(@PathVariable("crm_team_id") Integer crm_team_id,Res_usersSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_usersDTO> list = new ArrayList<Res_usersDTO>();
        context.setN_sale_team_id_eq(crm_team_id);
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        for(Res_users res_users : domains.getContent()){
            Res_usersDTO dto = new Res_usersDTO();
            dto.fromDO(res_users);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
